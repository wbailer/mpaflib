
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtImplInclude.h


#include "EbuCorevideoFormatType_width_CollectionType.h"
#include "EbuCorevideoFormatType_height_CollectionType.h"
#include "EbuCorerationalType.h"
#include "EbuCorevideoFormatType_aspectRatio_CollectionType.h"
#include "EbuCorevideoFormatType_videoEncoding_LocalType.h"
#include "EbuCorecodecType.h"
#include "EbuCorevideoFormatType_videoTrack_CollectionType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCorevideoFormatType_comment_CollectionType.h"
#include "EbuCorevideoFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorevideoFormatType_width_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:width
#include "EbuCorevideoFormatType_height_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:height
#include "EbuCoreaspectRatioType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:aspectRatio
#include "EbuCorevideoFormatType_videoTrack_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:videoTrack
#include "EbuCorecomment_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:comment

#include <assert.h>
IEbuCorevideoFormatType::IEbuCorevideoFormatType()
{

// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtPropInit.h

}

IEbuCorevideoFormatType::~IEbuCorevideoFormatType()
{
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtPropCleanup.h

}

EbuCorevideoFormatType::EbuCorevideoFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorevideoFormatType::~EbuCorevideoFormatType()
{
	Cleanup();
}

void EbuCorevideoFormatType::Init()
{

	// Init attributes
	m_videoFormatId = NULL; // String
	m_videoFormatId_Exist = false;
	m_videoFormatVersionId = NULL; // String
	m_videoFormatVersionId_Exist = false;
	m_videoFormatName = NULL; // String
	m_videoFormatName_Exist = false;
	m_videoFormatDefinition = NULL; // String
	m_videoFormatDefinition_Exist = false;
	m_videoPresenceFlag = false; // Value
	m_videoPresenceFlag_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_regionDelimX = (0); // Value

	m_regionDelimX_Exist = false;
	m_regionDelimY = (0); // Value

	m_regionDelimY_Exist = false;
	m_width = EbuCorevideoFormatType_width_CollectionPtr(); // Collection
	m_height = EbuCorevideoFormatType_height_CollectionPtr(); // Collection
	m_lines = (0); // Value

	m_lines_Exist = false;
	m_frameRate = EbuCorerationalPtr(); // Class with content 
	m_frameRate_Exist = false;
	m_aspectRatio = EbuCorevideoFormatType_aspectRatio_CollectionPtr(); // Collection
	m_videoEncoding = EbuCorevideoFormatType_videoEncoding_LocalPtr(); // Class
	m_videoEncoding_Exist = false;
	m_codec = EbuCorecodecPtr(); // Class
	m_codec_Exist = false;
	m_bitRate = (0); // Value

	m_bitRate_Exist = false;
	m_bitRateMax = (0); // Value

	m_bitRateMax_Exist = false;
	m_bitRateMode = EbuCorevideoFormatType_bitRateMode_LocalType::UninitializedEnumeration; // Enumeration
	m_bitRateMode_Exist = false;
	m_scanningFormat = EbuCorevideoFormatType_scanningFormat_LocalType::UninitializedEnumeration; // Enumeration
	m_scanningFormat_Exist = false;
	m_scanningOrder = EbuCorevideoFormatType_scanningOrder_LocalType::UninitializedEnumeration; // Enumeration
	m_scanningOrder_Exist = false;
	m_noiseFilter = (false); // Value

	m_noiseFilter_Exist = false;
	m_videoTrack = EbuCorevideoFormatType_videoTrack_CollectionPtr(); // Collection
	m_flag_3D = (false); // Value

	m_flag_3D_Exist = false;
	m_technicalAttributes = EbuCoretechnicalAttributesPtr(); // Class
	m_comment = EbuCorevideoFormatType_comment_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMyPropInit.h

}

void EbuCorevideoFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_videoFormatId); // String
	XMLString::release(&m_videoFormatVersionId); // String
	XMLString::release(&m_videoFormatName); // String
	XMLString::release(&m_videoFormatDefinition); // String
	// Dc1Factory::DeleteObject(m_width);
	// Dc1Factory::DeleteObject(m_height);
	// Dc1Factory::DeleteObject(m_frameRate);
	// Dc1Factory::DeleteObject(m_aspectRatio);
	// Dc1Factory::DeleteObject(m_videoEncoding);
	// Dc1Factory::DeleteObject(m_codec);
	// Dc1Factory::DeleteObject(m_videoTrack);
	// Dc1Factory::DeleteObject(m_technicalAttributes);
	// Dc1Factory::DeleteObject(m_comment);
}

void EbuCorevideoFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use videoFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IvideoFormatType
	const Dc1Ptr< EbuCorevideoFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_videoFormatId); // String
	if (tmp->ExistvideoFormatId())
	{
		this->SetvideoFormatId(XMLString::replicate(tmp->GetvideoFormatId()));
	}
	else
	{
		InvalidatevideoFormatId();
	}
	}
	{
	XMLString::release(&m_videoFormatVersionId); // String
	if (tmp->ExistvideoFormatVersionId())
	{
		this->SetvideoFormatVersionId(XMLString::replicate(tmp->GetvideoFormatVersionId()));
	}
	else
	{
		InvalidatevideoFormatVersionId();
	}
	}
	{
	XMLString::release(&m_videoFormatName); // String
	if (tmp->ExistvideoFormatName())
	{
		this->SetvideoFormatName(XMLString::replicate(tmp->GetvideoFormatName()));
	}
	else
	{
		InvalidatevideoFormatName();
	}
	}
	{
	XMLString::release(&m_videoFormatDefinition); // String
	if (tmp->ExistvideoFormatDefinition())
	{
		this->SetvideoFormatDefinition(XMLString::replicate(tmp->GetvideoFormatDefinition()));
	}
	else
	{
		InvalidatevideoFormatDefinition();
	}
	}
	{
	if (tmp->ExistvideoPresenceFlag())
	{
		this->SetvideoPresenceFlag(tmp->GetvideoPresenceFlag());
	}
	else
	{
		InvalidatevideoPresenceFlag();
	}
	}
	if (tmp->IsValidregionDelimX())
	{
		this->SetregionDelimX(tmp->GetregionDelimX());
	}
	else
	{
		InvalidateregionDelimX();
	}
	if (tmp->IsValidregionDelimY())
	{
		this->SetregionDelimY(tmp->GetregionDelimY());
	}
	else
	{
		InvalidateregionDelimY();
	}
		// Dc1Factory::DeleteObject(m_width);
		this->Setwidth(Dc1Factory::CloneObject(tmp->Getwidth()));
		// Dc1Factory::DeleteObject(m_height);
		this->Setheight(Dc1Factory::CloneObject(tmp->Getheight()));
	if (tmp->IsValidlines())
	{
		this->Setlines(tmp->Getlines());
	}
	else
	{
		Invalidatelines();
	}
	if (tmp->IsValidframeRate())
	{
		// Dc1Factory::DeleteObject(m_frameRate);
		this->SetframeRate(Dc1Factory::CloneObject(tmp->GetframeRate()));
	}
	else
	{
		InvalidateframeRate();
	}
		// Dc1Factory::DeleteObject(m_aspectRatio);
		this->SetaspectRatio(Dc1Factory::CloneObject(tmp->GetaspectRatio()));
	if (tmp->IsValidvideoEncoding())
	{
		// Dc1Factory::DeleteObject(m_videoEncoding);
		this->SetvideoEncoding(Dc1Factory::CloneObject(tmp->GetvideoEncoding()));
	}
	else
	{
		InvalidatevideoEncoding();
	}
	if (tmp->IsValidcodec())
	{
		// Dc1Factory::DeleteObject(m_codec);
		this->Setcodec(Dc1Factory::CloneObject(tmp->Getcodec()));
	}
	else
	{
		Invalidatecodec();
	}
	if (tmp->IsValidbitRate())
	{
		this->SetbitRate(tmp->GetbitRate());
	}
	else
	{
		InvalidatebitRate();
	}
	if (tmp->IsValidbitRateMax())
	{
		this->SetbitRateMax(tmp->GetbitRateMax());
	}
	else
	{
		InvalidatebitRateMax();
	}
	if (tmp->IsValidbitRateMode())
	{
		this->SetbitRateMode(tmp->GetbitRateMode());
	}
	else
	{
		InvalidatebitRateMode();
	}
	if (tmp->IsValidscanningFormat())
	{
		this->SetscanningFormat(tmp->GetscanningFormat());
	}
	else
	{
		InvalidatescanningFormat();
	}
	if (tmp->IsValidscanningOrder())
	{
		this->SetscanningOrder(tmp->GetscanningOrder());
	}
	else
	{
		InvalidatescanningOrder();
	}
	if (tmp->IsValidnoiseFilter())
	{
		this->SetnoiseFilter(tmp->GetnoiseFilter());
	}
	else
	{
		InvalidatenoiseFilter();
	}
		// Dc1Factory::DeleteObject(m_videoTrack);
		this->SetvideoTrack(Dc1Factory::CloneObject(tmp->GetvideoTrack()));
	if (tmp->IsValidflag_3D())
	{
		this->Setflag_3D(tmp->Getflag_3D());
	}
	else
	{
		Invalidateflag_3D();
	}
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		this->SettechnicalAttributes(Dc1Factory::CloneObject(tmp->GettechnicalAttributes()));
		// Dc1Factory::DeleteObject(m_comment);
		this->Setcomment(Dc1Factory::CloneObject(tmp->Getcomment()));
}

Dc1NodePtr EbuCorevideoFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorevideoFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorevideoFormatType::GetvideoFormatId() const
{
	return m_videoFormatId;
}

bool EbuCorevideoFormatType::ExistvideoFormatId() const
{
	return m_videoFormatId_Exist;
}
void EbuCorevideoFormatType::SetvideoFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoFormatId().");
	}
	m_videoFormatId = item;
	m_videoFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoFormatId()
{
	m_videoFormatId_Exist = false;
}
XMLCh * EbuCorevideoFormatType::GetvideoFormatVersionId() const
{
	return m_videoFormatVersionId;
}

bool EbuCorevideoFormatType::ExistvideoFormatVersionId() const
{
	return m_videoFormatVersionId_Exist;
}
void EbuCorevideoFormatType::SetvideoFormatVersionId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoFormatVersionId().");
	}
	m_videoFormatVersionId = item;
	m_videoFormatVersionId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoFormatVersionId()
{
	m_videoFormatVersionId_Exist = false;
}
XMLCh * EbuCorevideoFormatType::GetvideoFormatName() const
{
	return m_videoFormatName;
}

bool EbuCorevideoFormatType::ExistvideoFormatName() const
{
	return m_videoFormatName_Exist;
}
void EbuCorevideoFormatType::SetvideoFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoFormatName().");
	}
	m_videoFormatName = item;
	m_videoFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoFormatName()
{
	m_videoFormatName_Exist = false;
}
XMLCh * EbuCorevideoFormatType::GetvideoFormatDefinition() const
{
	return m_videoFormatDefinition;
}

bool EbuCorevideoFormatType::ExistvideoFormatDefinition() const
{
	return m_videoFormatDefinition_Exist;
}
void EbuCorevideoFormatType::SetvideoFormatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoFormatDefinition().");
	}
	m_videoFormatDefinition = item;
	m_videoFormatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoFormatDefinition()
{
	m_videoFormatDefinition_Exist = false;
}
bool EbuCorevideoFormatType::GetvideoPresenceFlag() const
{
	return m_videoPresenceFlag;
}

bool EbuCorevideoFormatType::ExistvideoPresenceFlag() const
{
	return m_videoPresenceFlag_Exist;
}
void EbuCorevideoFormatType::SetvideoPresenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoPresenceFlag().");
	}
	m_videoPresenceFlag = item;
	m_videoPresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoPresenceFlag()
{
	m_videoPresenceFlag_Exist = false;
}
unsigned EbuCorevideoFormatType::GetregionDelimX() const
{
		return m_regionDelimX;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidregionDelimX() const
{
	return m_regionDelimX_Exist;
}

unsigned EbuCorevideoFormatType::GetregionDelimY() const
{
		return m_regionDelimY;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidregionDelimY() const
{
	return m_regionDelimY_Exist;
}

EbuCorevideoFormatType_width_CollectionPtr EbuCorevideoFormatType::Getwidth() const
{
		return m_width;
}

EbuCorevideoFormatType_height_CollectionPtr EbuCorevideoFormatType::Getheight() const
{
		return m_height;
}

unsigned EbuCorevideoFormatType::Getlines() const
{
		return m_lines;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidlines() const
{
	return m_lines_Exist;
}

EbuCorerationalPtr EbuCorevideoFormatType::GetframeRate() const
{
		return m_frameRate;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidframeRate() const
{
	return m_frameRate_Exist;
}

EbuCorevideoFormatType_aspectRatio_CollectionPtr EbuCorevideoFormatType::GetaspectRatio() const
{
		return m_aspectRatio;
}

EbuCorevideoFormatType_videoEncoding_LocalPtr EbuCorevideoFormatType::GetvideoEncoding() const
{
		return m_videoEncoding;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidvideoEncoding() const
{
	return m_videoEncoding_Exist;
}

EbuCorecodecPtr EbuCorevideoFormatType::Getcodec() const
{
		return m_codec;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidcodec() const
{
	return m_codec_Exist;
}

unsigned EbuCorevideoFormatType::GetbitRate() const
{
		return m_bitRate;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidbitRate() const
{
	return m_bitRate_Exist;
}

unsigned EbuCorevideoFormatType::GetbitRateMax() const
{
		return m_bitRateMax;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidbitRateMax() const
{
	return m_bitRateMax_Exist;
}

EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration EbuCorevideoFormatType::GetbitRateMode() const
{
		return m_bitRateMode;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidbitRateMode() const
{
	return m_bitRateMode_Exist;
}

EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration EbuCorevideoFormatType::GetscanningFormat() const
{
		return m_scanningFormat;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidscanningFormat() const
{
	return m_scanningFormat_Exist;
}

EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration EbuCorevideoFormatType::GetscanningOrder() const
{
		return m_scanningOrder;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidscanningOrder() const
{
	return m_scanningOrder_Exist;
}

bool EbuCorevideoFormatType::GetnoiseFilter() const
{
		return m_noiseFilter;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidnoiseFilter() const
{
	return m_noiseFilter_Exist;
}

EbuCorevideoFormatType_videoTrack_CollectionPtr EbuCorevideoFormatType::GetvideoTrack() const
{
		return m_videoTrack;
}

bool EbuCorevideoFormatType::Getflag_3D() const
{
		return m_flag_3D;
}

// Element is optional
bool EbuCorevideoFormatType::IsValidflag_3D() const
{
	return m_flag_3D_Exist;
}

EbuCoretechnicalAttributesPtr EbuCorevideoFormatType::GettechnicalAttributes() const
{
		return m_technicalAttributes;
}

EbuCorevideoFormatType_comment_CollectionPtr EbuCorevideoFormatType::Getcomment() const
{
		return m_comment;
}

void EbuCorevideoFormatType::SetregionDelimX(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetregionDelimX().");
	}
	if (m_regionDelimX != item || m_regionDelimX_Exist == false)
	{
		m_regionDelimX = item;
		m_regionDelimX_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidateregionDelimX()
{
	m_regionDelimX_Exist = false;
}
void EbuCorevideoFormatType::SetregionDelimY(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetregionDelimY().");
	}
	if (m_regionDelimY != item || m_regionDelimY_Exist == false)
	{
		m_regionDelimY = item;
		m_regionDelimY_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidateregionDelimY()
{
	m_regionDelimY_Exist = false;
}
void EbuCorevideoFormatType::Setwidth(const EbuCorevideoFormatType_width_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setwidth().");
	}
	if (m_width != item)
	{
		// Dc1Factory::DeleteObject(m_width);
		m_width = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_width) m_width->SetParent(m_myself.getPointer());
		if(m_width != EbuCorevideoFormatType_width_CollectionPtr())
		{
			m_width->SetContentName(XMLString::transcode("width"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Setheight(const EbuCorevideoFormatType_height_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setheight().");
	}
	if (m_height != item)
	{
		// Dc1Factory::DeleteObject(m_height);
		m_height = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_height) m_height->SetParent(m_myself.getPointer());
		if(m_height != EbuCorevideoFormatType_height_CollectionPtr())
		{
			m_height->SetContentName(XMLString::transcode("height"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Setlines(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setlines().");
	}
	if (m_lines != item || m_lines_Exist == false)
	{
		m_lines = item;
		m_lines_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Invalidatelines()
{
	m_lines_Exist = false;
}
void EbuCorevideoFormatType::SetframeRate(const EbuCorerationalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetframeRate().");
	}
	if (m_frameRate != item || m_frameRate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_frameRate);
		m_frameRate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_frameRate) m_frameRate->SetParent(m_myself.getPointer());
		if (m_frameRate && m_frameRate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rationalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_frameRate->UseTypeAttribute = true;
		}
		m_frameRate_Exist = true;
		if(m_frameRate != EbuCorerationalPtr())
		{
			m_frameRate->SetContentName(XMLString::transcode("frameRate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidateframeRate()
{
	m_frameRate_Exist = false;
}
void EbuCorevideoFormatType::SetaspectRatio(const EbuCorevideoFormatType_aspectRatio_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetaspectRatio().");
	}
	if (m_aspectRatio != item)
	{
		// Dc1Factory::DeleteObject(m_aspectRatio);
		m_aspectRatio = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_aspectRatio) m_aspectRatio->SetParent(m_myself.getPointer());
		if(m_aspectRatio != EbuCorevideoFormatType_aspectRatio_CollectionPtr())
		{
			m_aspectRatio->SetContentName(XMLString::transcode("aspectRatio"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::SetvideoEncoding(const EbuCorevideoFormatType_videoEncoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoEncoding().");
	}
	if (m_videoEncoding != item || m_videoEncoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_videoEncoding);
		m_videoEncoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_videoEncoding) m_videoEncoding->SetParent(m_myself.getPointer());
		if (m_videoEncoding && m_videoEncoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoEncoding_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_videoEncoding->UseTypeAttribute = true;
		}
		m_videoEncoding_Exist = true;
		if(m_videoEncoding != EbuCorevideoFormatType_videoEncoding_LocalPtr())
		{
			m_videoEncoding->SetContentName(XMLString::transcode("videoEncoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatevideoEncoding()
{
	m_videoEncoding_Exist = false;
}
void EbuCorevideoFormatType::Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setcodec().");
	}
	if (m_codec != item || m_codec_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_codec);
		m_codec = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_codec) m_codec->SetParent(m_myself.getPointer());
		if (m_codec && m_codec->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_codec->UseTypeAttribute = true;
		}
		m_codec_Exist = true;
		if(m_codec != EbuCorecodecPtr())
		{
			m_codec->SetContentName(XMLString::transcode("codec"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Invalidatecodec()
{
	m_codec_Exist = false;
}
void EbuCorevideoFormatType::SetbitRate(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetbitRate().");
	}
	if (m_bitRate != item || m_bitRate_Exist == false)
	{
		m_bitRate = item;
		m_bitRate_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatebitRate()
{
	m_bitRate_Exist = false;
}
void EbuCorevideoFormatType::SetbitRateMax(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetbitRateMax().");
	}
	if (m_bitRateMax != item || m_bitRateMax_Exist == false)
	{
		m_bitRateMax = item;
		m_bitRateMax_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatebitRateMax()
{
	m_bitRateMax_Exist = false;
}
void EbuCorevideoFormatType::SetbitRateMode(EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetbitRateMode().");
	}
	if (m_bitRateMode != item || m_bitRateMode_Exist == false)
	{
		// nothing to free here, hopefully
		m_bitRateMode = item;
		m_bitRateMode_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatebitRateMode()
{
	m_bitRateMode_Exist = false;
}
void EbuCorevideoFormatType::SetscanningFormat(EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetscanningFormat().");
	}
	if (m_scanningFormat != item || m_scanningFormat_Exist == false)
	{
		// nothing to free here, hopefully
		m_scanningFormat = item;
		m_scanningFormat_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatescanningFormat()
{
	m_scanningFormat_Exist = false;
}
void EbuCorevideoFormatType::SetscanningOrder(EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetscanningOrder().");
	}
	if (m_scanningOrder != item || m_scanningOrder_Exist == false)
	{
		// nothing to free here, hopefully
		m_scanningOrder = item;
		m_scanningOrder_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatescanningOrder()
{
	m_scanningOrder_Exist = false;
}
void EbuCorevideoFormatType::SetnoiseFilter(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetnoiseFilter().");
	}
	if (m_noiseFilter != item || m_noiseFilter_Exist == false)
	{
		m_noiseFilter = item;
		m_noiseFilter_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::InvalidatenoiseFilter()
{
	m_noiseFilter_Exist = false;
}
void EbuCorevideoFormatType::SetvideoTrack(const EbuCorevideoFormatType_videoTrack_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SetvideoTrack().");
	}
	if (m_videoTrack != item)
	{
		// Dc1Factory::DeleteObject(m_videoTrack);
		m_videoTrack = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_videoTrack) m_videoTrack->SetParent(m_myself.getPointer());
		if(m_videoTrack != EbuCorevideoFormatType_videoTrack_CollectionPtr())
		{
			m_videoTrack->SetContentName(XMLString::transcode("videoTrack"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Setflag_3D(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setflag_3D().");
	}
	if (m_flag_3D != item || m_flag_3D_Exist == false)
	{
		m_flag_3D = item;
		m_flag_3D_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Invalidateflag_3D()
{
	m_flag_3D_Exist = false;
}
void EbuCorevideoFormatType::SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::SettechnicalAttributes().");
	}
	if (m_technicalAttributes != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		m_technicalAttributes = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributes) m_technicalAttributes->SetParent(m_myself.getPointer());
		if (m_technicalAttributes && m_technicalAttributes->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_technicalAttributes->UseTypeAttribute = true;
		}
		if(m_technicalAttributes != EbuCoretechnicalAttributesPtr())
		{
			m_technicalAttributes->SetContentName(XMLString::transcode("technicalAttributes"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType::Setcomment(const EbuCorevideoFormatType_comment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType::Setcomment().");
	}
	if (m_comment != item)
	{
		// Dc1Factory::DeleteObject(m_comment);
		m_comment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_comment) m_comment->SetParent(m_myself.getPointer());
		if(m_comment != EbuCorevideoFormatType_comment_CollectionPtr())
		{
			m_comment->SetContentName(XMLString::transcode("comment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorevideoFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("videoFormatId")) == 0)
	{
		// videoFormatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("videoFormatVersionId")) == 0)
	{
		// videoFormatVersionId is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("videoFormatName")) == 0)
	{
		// videoFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("videoFormatDefinition")) == 0)
	{
		// videoFormatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("videoPresenceFlag")) == 0)
	{
		// videoPresenceFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("width")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:width is item of type videoFormatType_width_LocalType
		// in element collection videoFormatType_width_CollectionType
		
		context->Found = true;
		EbuCorevideoFormatType_width_CollectionPtr coll = Getwidth();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatevideoFormatType_width_CollectionType; // FTT, check this
				Setwidth(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_width_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorevideoFormatType_width_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("height")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:height is item of type videoFormatType_height_LocalType
		// in element collection videoFormatType_height_CollectionType
		
		context->Found = true;
		EbuCorevideoFormatType_height_CollectionPtr coll = Getheight();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatevideoFormatType_height_CollectionType; // FTT, check this
				Setheight(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_height_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorevideoFormatType_height_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("frameRate")) == 0)
	{
		// frameRate is simple element rationalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetframeRate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rationalType")))) != empty)
			{
				// Is type allowed
				EbuCorerationalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetframeRate(p, client);
					if((p = GetframeRate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("aspectRatio")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:aspectRatio is item of type aspectRatioType
		// in element collection videoFormatType_aspectRatio_CollectionType
		
		context->Found = true;
		EbuCorevideoFormatType_aspectRatio_CollectionPtr coll = GetaspectRatio();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatevideoFormatType_aspectRatio_CollectionType; // FTT, check this
				SetaspectRatio(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:aspectRatioType")))) != empty)
			{
				// Is type allowed
				EbuCoreaspectRatioPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("videoEncoding")) == 0)
	{
		// videoEncoding is simple element videoFormatType_videoEncoding_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetvideoEncoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoEncoding_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorevideoFormatType_videoEncoding_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetvideoEncoding(p, client);
					if((p = GetvideoEncoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("codec")) == 0)
	{
		// codec is simple element codecType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcodec()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType")))) != empty)
			{
				// Is type allowed
				EbuCorecodecPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcodec(p, client);
					if((p = Getcodec()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("videoTrack")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:videoTrack is item of type videoFormatType_videoTrack_LocalType
		// in element collection videoFormatType_videoTrack_CollectionType
		
		context->Found = true;
		EbuCorevideoFormatType_videoTrack_CollectionPtr coll = GetvideoTrack();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatevideoFormatType_videoTrack_CollectionType; // FTT, check this
				SetvideoTrack(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoTrack_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorevideoFormatType_videoTrack_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributes")) == 0)
	{
		// technicalAttributes is simple element technicalAttributes
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettechnicalAttributes()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettechnicalAttributes(p, client);
					if((p = GettechnicalAttributes()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("comment")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:comment is item of type comment_LocalType
		// in element collection videoFormatType_comment_CollectionType
		
		context->Found = true;
		EbuCorevideoFormatType_comment_CollectionPtr coll = Getcomment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatevideoFormatType_comment_CollectionType; // FTT, check this
				Setcomment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecomment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for videoFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "videoFormatType");
	}
	return result;
}

XMLCh * EbuCorevideoFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorevideoFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorevideoFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("videoFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_videoFormatId_Exist)
	{
	// String
	if(m_videoFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("videoFormatId"), m_videoFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_videoFormatVersionId_Exist)
	{
	// String
	if(m_videoFormatVersionId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("videoFormatVersionId"), m_videoFormatVersionId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_videoFormatName_Exist)
	{
	// String
	if(m_videoFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("videoFormatName"), m_videoFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_videoFormatDefinition_Exist)
	{
	// String
	if(m_videoFormatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("videoFormatDefinition"), m_videoFormatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_videoPresenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_videoPresenceFlag);
		element->setAttributeNS(X(""), X("videoPresenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_regionDelimX_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_regionDelimX);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("regionDelimX"), true);
		XMLString::release(&tmp);
	}
	if(m_regionDelimY_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_regionDelimY);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("regionDelimY"), true);
		XMLString::release(&tmp);
	}
	if (m_width != EbuCorevideoFormatType_width_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_width->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_height != EbuCorevideoFormatType_height_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_height->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_lines_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_lines);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("lines"), true);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_frameRate != EbuCorerationalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_frameRate->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("frameRate"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_frameRate->Serialize(doc, element, &elem);
	}
	if (m_aspectRatio != EbuCorevideoFormatType_aspectRatio_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_aspectRatio->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_videoEncoding != EbuCorevideoFormatType_videoEncoding_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_videoEncoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("videoEncoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_videoEncoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_codec != EbuCorecodecPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_codec->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("codec"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_codec->Serialize(doc, element, &elem);
	}
	if(m_bitRate_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_bitRate);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRate"), true);
		XMLString::release(&tmp);
	}
	if(m_bitRateMax_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_bitRateMax);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRateMax"), true);
		XMLString::release(&tmp);
	}
	if(m_bitRateMode != EbuCorevideoFormatType_bitRateMode_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCorevideoFormatType_bitRateMode_LocalType::ToText(m_bitRateMode);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRateMode"), true);
		XMLString::release(&tmp);
	}
	if(m_scanningFormat != EbuCorevideoFormatType_scanningFormat_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCorevideoFormatType_scanningFormat_LocalType::ToText(m_scanningFormat);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("scanningFormat"), true);
		XMLString::release(&tmp);
	}
	if(m_scanningOrder != EbuCorevideoFormatType_scanningOrder_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCorevideoFormatType_scanningOrder_LocalType::ToText(m_scanningOrder);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("scanningOrder"), true);
		XMLString::release(&tmp);
	}
	if(m_noiseFilter_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_noiseFilter);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("noiseFilter"), true);
		XMLString::release(&tmp);
	}
	if (m_videoTrack != EbuCorevideoFormatType_videoTrack_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_videoTrack->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_flag_3D_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_flag_3D);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("flag_3D"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_technicalAttributes != EbuCoretechnicalAttributesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_technicalAttributes->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_technicalAttributes->Serialize(doc, element, &elem);
	}
	if (m_comment != EbuCorevideoFormatType_comment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_comment->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorevideoFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("videoFormatId")))
	{
		// Deserialize string type
		this->SetvideoFormatId(Dc1Convert::TextToString(parent->getAttribute(X("videoFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("videoFormatVersionId")))
	{
		// Deserialize string type
		this->SetvideoFormatVersionId(Dc1Convert::TextToString(parent->getAttribute(X("videoFormatVersionId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("videoFormatName")))
	{
		// Deserialize string type
		this->SetvideoFormatName(Dc1Convert::TextToString(parent->getAttribute(X("videoFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("videoFormatDefinition")))
	{
		// Deserialize string type
		this->SetvideoFormatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("videoFormatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("videoPresenceFlag")))
	{
		// deserialize value type
		this->SetvideoPresenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("videoPresenceFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("regionDelimX"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetregionDelimX(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("regionDelimY"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetregionDelimY(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("width"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("width")) == 0))
		{
			// Deserialize factory type
			EbuCorevideoFormatType_width_CollectionPtr tmp = CreatevideoFormatType_width_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setwidth(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("height"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("height")) == 0))
		{
			// Deserialize factory type
			EbuCorevideoFormatType_height_CollectionPtr tmp = CreatevideoFormatType_height_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setheight(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("lines"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setlines(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("frameRate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("frameRate")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreaterationalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetframeRate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("aspectRatio"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("aspectRatio")) == 0))
		{
			// Deserialize factory type
			EbuCorevideoFormatType_aspectRatio_CollectionPtr tmp = CreatevideoFormatType_aspectRatio_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaspectRatio(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("videoEncoding"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("videoEncoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatevideoFormatType_videoEncoding_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetvideoEncoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("codec"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("codec")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecodecType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcodec(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("bitRate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetbitRate(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("bitRateMax"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetbitRateMax(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("bitRateMode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("bitRateMode")) == 0))
		{
			EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration tmp = EbuCorevideoFormatType_bitRateMode_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCorevideoFormatType_bitRateMode_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetbitRateMode(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("scanningFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("scanningFormat")) == 0))
		{
			EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration tmp = EbuCorevideoFormatType_scanningFormat_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCorevideoFormatType_scanningFormat_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetscanningFormat(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("scanningOrder"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("scanningOrder")) == 0))
		{
			EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration tmp = EbuCorevideoFormatType_scanningOrder_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCorevideoFormatType_scanningOrder_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetscanningOrder(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("noiseFilter"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetnoiseFilter(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("videoTrack"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("videoTrack")) == 0))
		{
			// Deserialize factory type
			EbuCorevideoFormatType_videoTrack_CollectionPtr tmp = CreatevideoFormatType_videoTrack_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetvideoTrack(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("flag_3D"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setflag_3D(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("technicalAttributes"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("technicalAttributes")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetechnicalAttributes; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettechnicalAttributes(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("comment"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("comment")) == 0))
		{
			// Deserialize factory type
			EbuCorevideoFormatType_comment_CollectionPtr tmp = CreatevideoFormatType_comment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcomment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorevideoFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("width")) == 0))
  {
	EbuCorevideoFormatType_width_CollectionPtr tmp = CreatevideoFormatType_width_CollectionType; // FTT, check this
	this->Setwidth(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("height")) == 0))
  {
	EbuCorevideoFormatType_height_CollectionPtr tmp = CreatevideoFormatType_height_CollectionType; // FTT, check this
	this->Setheight(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("frameRate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreaterationalType; // FTT, check this
	}
	this->SetframeRate(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("aspectRatio")) == 0))
  {
	EbuCorevideoFormatType_aspectRatio_CollectionPtr tmp = CreatevideoFormatType_aspectRatio_CollectionType; // FTT, check this
	this->SetaspectRatio(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("videoEncoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatevideoFormatType_videoEncoding_LocalType; // FTT, check this
	}
	this->SetvideoEncoding(child);
  }
  if (XMLString::compareString(elementname, X("codec")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecodecType; // FTT, check this
	}
	this->Setcodec(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("videoTrack")) == 0))
  {
	EbuCorevideoFormatType_videoTrack_CollectionPtr tmp = CreatevideoFormatType_videoTrack_CollectionType; // FTT, check this
	this->SetvideoTrack(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("technicalAttributes")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetechnicalAttributes; // FTT, check this
	}
	this->SettechnicalAttributes(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("comment")) == 0))
  {
	EbuCorevideoFormatType_comment_CollectionPtr tmp = CreatevideoFormatType_comment_CollectionType; // FTT, check this
	this->Setcomment(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorevideoFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getwidth() != Dc1NodePtr())
		result.Insert(Getwidth());
	if (Getheight() != Dc1NodePtr())
		result.Insert(Getheight());
	if (GetframeRate() != Dc1NodePtr())
		result.Insert(GetframeRate());
	if (GetaspectRatio() != Dc1NodePtr())
		result.Insert(GetaspectRatio());
	if (GetvideoEncoding() != Dc1NodePtr())
		result.Insert(GetvideoEncoding());
	if (Getcodec() != Dc1NodePtr())
		result.Insert(Getcodec());
	if (GetvideoTrack() != Dc1NodePtr())
		result.Insert(GetvideoTrack());
	if (GettechnicalAttributes() != Dc1NodePtr())
		result.Insert(GettechnicalAttributes());
	if (Getcomment() != Dc1NodePtr())
		result.Insert(Getcomment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMethodImpl.h


