
// Extension library class
// NodeRefCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <stdio.h>
#include "EbuCorecoreMetadataType_isPartOf_CollectionType.h"
#include "Dc1FactoryDefines.h"
 
#include "EbuCoreFactoryDefines.h"

#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtImplInclude.h


#include "EbuCorerelationType.h" // Collection itemtype



EbuCorecoreMetadataType_isPartOf_CollectionType::EbuCorecoreMetadataType_isPartOf_CollectionType()
{
	m_Item = new Dc1ValueVectorOf<Dc1NodePtr>(0); // XXX
	dontSerializeWithContentName = true; // HACK this is beta, next time make it better!

// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtPropInit.h


}

EbuCorecoreMetadataType_isPartOf_CollectionType::~EbuCorecoreMetadataType_isPartOf_CollectionType()
{
// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtPropCleanup.h


	delete m_Item;
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::DeepCopy(const Dc1NodePtr &original)
{
	const EbuCorecoreMetadataType_isPartOf_CollectionPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: passed argument of wrong type

	removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		Dc1NodePtr dummy = Dc1Factory::CloneObject(tmp->elementAt(i));
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (dummy) dummy->SetParent(m_myself.getPointer());
		m_Item->addElement(dummy);
	}
}

// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtMethodImpl.h


void EbuCorecoreMetadataType_isPartOf_CollectionType::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::addElement(const Dc1NodePtr &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_isPartOf_CollectionType::addElement().");
	}
	m_Item->addElement(toAdd);
	if (toAdd) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toAdd->SetParent(m_myself.getPointer());
		if (!toAdd->GetContentName()) {
// OLD			toAdd->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("urn:ebu:metadata-schema:ebuCore_2014:isPartOf"));
			toAdd->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("isPartOf"));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_ADDED,
		client, toAdd);
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_isPartOf_CollectionType::setElementAt().");
	}
	Dc1NodePtr previous = m_Item->elementAt(setAt);
	if (previous) previous->SetParent(Dc1NodePtr());
	m_Item->setElementAt(toSet, setAt);

	if (toSet) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toSet->SetParent(m_myself.getPointer());
		if (!toSet->GetContentName()) {
// OLD			toSet->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("urn:ebu:metadata-schema:ebuCore_2014:isPartOf"));
			toSet->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("isPartOf"));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, toSet, setAt);
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_isPartOf_CollectionType::insertElementAt().");
	}
	m_Item->insertElementAt(toInsert, insertAt);
	if (toInsert) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toInsert->SetParent(m_myself.getPointer());
		if (!toInsert->GetContentName()) {
// OLD			toInsert->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("urn:ebu:metadata-schema:ebuCore_2014:isPartOf"));
			toInsert->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("isPartOf"));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_INSERTED,
		client, toInsert, insertAt);
}

/*
Dc1NodePtr EbuCorecoreMetadataType_isPartOf_CollectionType::orphanElementAt(unsigned int orphanAt)
{
	return m_Item->orphanElementAt(orphanAt);
}
*/

void EbuCorecoreMetadataType_isPartOf_CollectionType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_isPartOf_CollectionType::removeAllElements().");
	}

	for (unsigned int i = 0; i < m_Item->size(); i++) {
		// FIXME: handle elementAt(removeAt) == NULL
		m_Item->elementAt(i)->SetParent(Dc1NodePtr());
	}

	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_isPartOf_CollectionType::removeElementAt().");
	}
	// FIXME: handle elementAt(removeAt) == NULL
	m_Item->elementAt(removeAt)->SetParent(Dc1NodePtr());
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool EbuCorecoreMetadataType_isPartOf_CollectionType::containsElement(const Dc1NodePtr &toCheck)
{
	return m_Item->containsElement(toCheck);
}

unsigned int EbuCorecoreMetadataType_isPartOf_CollectionType::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const Dc1NodePtr EbuCorecoreMetadataType_isPartOf_CollectionType::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

Dc1NodePtr EbuCorecoreMetadataType_isPartOf_CollectionType::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

Dc1NodeEnum EbuCorecoreMetadataType_isPartOf_CollectionType::GetElementsOfType(unsigned int id) const
{
	Dc1Enumerator< Dc1NodePtr > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		if (elementAt(i)->GetClassId() == id)
		{
			result.Insert(elementAt(i));
		}
	}
	return result;
}

Dc1NodeEnum EbuCorecoreMetadataType_isPartOf_CollectionType::GetElements() const
{
	Dc1Enumerator< Dc1Ptr<Dc1Node> > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		result.Insert(elementAt(i));
	}
	return result;
}

unsigned int EbuCorecoreMetadataType_isPartOf_CollectionType::size() const
{
	return (unsigned int)m_Item->size();
}

void EbuCorecoreMetadataType_isPartOf_CollectionType::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

// JRS: addition to Xerces collection
int EbuCorecoreMetadataType_isPartOf_CollectionType::elementIndexOf(const Dc1NodePtr &toCheck) const
{
	for(unsigned int i = 0; i < size(); i++)
	{
		if(elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1; // not found
}

XMLCh * EbuCorecoreMetadataType_isPartOf_CollectionType::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < size(); i++)
	{
		XMLCh * tmp = (elementAt(i))->ToText();
		Dc1Util::CatString(&buf, tmp);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		if(i < size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer
	return buf;
}

bool EbuCorecoreMetadataType_isPartOf_CollectionType::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
		
	// This is not a simpletype list ItemType and intentionally unimplemented for collections
	return false;
}

/** 
 * Item type is ElementOnly. 
 */
void EbuCorecoreMetadataType_isPartOf_CollectionType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
	} else {
		element = doc->getDocumentElement();
	}


// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtPreSerialize.h


	for(unsigned int i = 0; i < size(); i++)
	{
		// Collection is not SimpleTypeList -> serialize elements
		if(this->GetContentName() != NULL && !dontSerializeWithContentName)
		{
// OLD			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			*newElem = elem;
			(elementAt(i))->Serialize(doc, elem, newElem);
			element->appendChild(elem);
		}
		else
		{
			// Let element create a node if it wants
			(elementAt(i))->Serialize(doc, element);
		}
		if(i < size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
	}
}



bool EbuCorecoreMetadataType_isPartOf_CollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(parent == NULL)
	{
		return false;
	}
	* current = parent; // Init with first element - check this!

	// Deserialize collection
	// Itemtype = Empty | ElementOnly
	DOMElement * ignore = NULL;
	while((parent != NULL) && (
		Dc1Util::HasNodeName(parent, X("isPartOf"), X("urn:ebu:metadata-schema:ebuCore_2014"))
// OLD3		(XMLString::compareString(parent->getNodeName(), X("urn:ebu:metadata-schema:ebuCore_2014:isPartOf")) == 0)
	))
	{
		Dc1NodePtr item = EbuCorerelationPtr();

		NamespaceHelper xsitype;
		if(Dc1Util::GetXsiType(parent, doc, &xsitype))
//OLD		if(parent->hasAttribute(X("xsi:type")))
		{
			XMLCh * tname = Dc1Util::QualifiedName(&xsitype);
//OLD			unsigned int id = Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type")));
			unsigned int id = Dc1Factory::GetTypeIndex(tname);
			if(tname) Dc1Util::deallocate(&tname);
			item = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject(id));
			item->UseTypeAttribute = true;
		}
		else
		{
			item = CreaterelationType; // FTT, check this
		}
		// not Sequence or Choice -- use "own" node
		item->Deserialize(doc, parent, &ignore);
		XMLCh * contentname = XMLString::replicate(parent->getNodeName());
		item->SetContentName(contentname);
		* current = parent;
		addElement(item);
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}


// no includefile for extension defined 
// file EbuCorecoreMetadataType_isPartOf_CollectionType_ExtPostDeserialize.h


	return true;
}

inline
bool am_in_collection(int next_slash, int next_bracket, int next_close_bracket)
{
	return next_bracket >= 0
		&& next_close_bracket > next_bracket + 1 // at least one digit
		&& (next_slash < 0		// either at end of string ...
			|| next_slash > next_close_bracket); // ... or collection
												 // is in current part
}

Dc1NodePtr EbuCorecoreMetadataType_isPartOf_CollectionType::NodeFromXPath(const XMLCh *xpath) const
{
	const XMLCh *work_string = xpath;
	const XMLCh *content_name = GetContentName();
	Dc1NodePtr me = m_myself.getPointer();
	Dc1NodeCollectionPtr me_as_collection = me;
	Dc1NodePtr parent = GetParent();

	if (XMLString::startsWith(work_string, X("/"))) {
		if (!parent)
			work_string++;		// consume leading slash
		else {
			Dc1NodePtr root = parent;
			while (root->GetParent())
				root = root->GetParent();
			return root->NodeFromXPath(work_string); // EXIT: parse from root
		}
	}

	if (XMLString::stringLen(work_string) == 0)
		return me;

	if (XMLString::startsWith(work_string, X("..")))
		return UpwardWithXPath(work_string);

	XMLCh slash = X("/")[0];
	XMLCh bracket = X("[")[0];
	XMLCh close_bracket = X("]")[0];
	int next_slash = XMLString::indexOf(work_string, slash);
	int next_bracket = XMLString::indexOf(work_string, bracket);
	int next_close_bracket = XMLString::indexOf(work_string, close_bracket);

	// We're a collection -- find n, nth element, parse from there
	int n;
	if (!am_in_collection(next_slash, next_bracket, next_close_bracket))
	{
		if (!content_name) {
			// A collection can be a a choice too; there's a valid
			// test case where we needed to descend into a
			// collection for the xpath string "Name".  The
			// "!content_name" clause is just a futile attempt to
			// cut back the lossage a bit; I'm assuming that none
			// of these freak collections has it set.
			Dc1NodeEnum children = me_as_collection->GetElements();
			while (children.HasNext()) {
				Dc1NodePtr result = children.GetNext()->NodeFromXPath(xpath);
				if (result != Dc1NodePtr())
					return result;	// EXIT: success!
			}
		}
		// fall-through case: no collection index found, malformed
		// xpath string or xpath string didn't match the children.
		return Dc1NodePtr(); // EXIT
	}
	XMLCh *n_str = Dc1Util::allocate(next_close_bracket - next_bracket + 1);
	XMLString::moveChars(n_str, work_string + next_bracket + 1,
		next_close_bracket - next_bracket - 1);
	n = XMLString::parseInt(n_str) - 1;	// XPath indices start
	// with 1, ours start with
	// 0 -- adjust
	XMLString::release(&n_str);
	if (n >= 0 && n < (int)me_as_collection->size()) {
		// if the parent is a LocalType, our ContentName has to
		// match; otherwise, it's allowed to be unset.  if it's
		// set, it has to match.
		if ((content_name
				&& XMLString::startsWith(work_string, content_name)
				&& !XMLString::isAlphaNum(
					work_string[XMLString::stringLen(content_name)])))
		{
			if (next_slash != -1)
				if (dontSerializeWithContentName)
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string);
				else
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string + next_slash + 1);
			else
				// EXIT: success!  (xpath ended with ".../elemName[n]")
				return me_as_collection->elementAt(n);
		}
		else if (!content_name)
		{
			// It's probably a local collection (collection ->
			// LocalType -> Type) -- the name will be picked up by
			// the Type node, so pass in full work string here.
			return me_as_collection->elementAt(n)->NodeFromXPath(
				work_string); // EXIT
		}
		else
			return Dc1NodePtr(); // EXIT: wrong content name
	} else
	return Dc1NodePtr();	// EXIT: index out of range

}

