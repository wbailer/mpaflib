
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtImplInclude.h


#include "EbuCoreaudioBlockFormatType_speakerLabel_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioBlockFormatType_speakerLabel_LocalType::IEbuCoreaudioBlockFormatType_speakerLabel_LocalType()
{

// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtPropInit.h

}

IEbuCoreaudioBlockFormatType_speakerLabel_LocalType::~IEbuCoreaudioBlockFormatType_speakerLabel_LocalType()
{
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtPropCleanup.h

}

EbuCoreaudioBlockFormatType_speakerLabel_LocalType::EbuCoreaudioBlockFormatType_speakerLabel_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioBlockFormatType_speakerLabel_LocalType::~EbuCoreaudioBlockFormatType_speakerLabel_LocalType()
{
	Cleanup();
}

void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Init()
{
	// Init base
	m_Base = XMLString::transcode("");

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;



// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtMyPropInit.h

}

void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_Base);
	XMLString::release(&m_lang); // String
}

void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioBlockFormatType_speakerLabel_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioBlockFormatType_speakerLabel_LocalType
	const Dc1Ptr< EbuCoreaudioBlockFormatType_speakerLabel_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	XMLString::release(&m_Base);
	m_Base = XMLString::replicate(tmp->GetContent());
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
}

Dc1NodePtr EbuCoreaudioBlockFormatType_speakerLabel_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioBlockFormatType_speakerLabel_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType_speakerLabel_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		XMLString::release(&m_Base);
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCoreaudioBlockFormatType_speakerLabel_LocalType::GetContent() const
{
	return m_Base;
}
XMLCh * EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Getlang() const
{
	return m_lang;
}

bool EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Existlang() const
{
	return m_lang_Exist;
}
void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Invalidatelang()
{
	m_lang_Exist = false;
}

Dc1NodeEnum EbuCoreaudioBlockFormatType_speakerLabel_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioBlockFormatType_speakerLabel_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioBlockFormatType_speakerLabel_LocalType");
	}
	return result;
}

XMLCh * EbuCoreaudioBlockFormatType_speakerLabel_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCoreaudioBlockFormatType_speakerLabel_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// EbuCoreaudioBlockFormatType_speakerLabel_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* EbuCoreaudioBlockFormatType_speakerLabel_LocalType::ContentToString() const
{
	// EbuCoreaudioBlockFormatType_speakerLabel_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	element->appendChild(doc->createTextNode(m_Base));

	assert(element);
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioBlockFormatType_speakerLabel_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCoreaudioBlockFormatType_speakerLabel_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	XMLCh * tmp = Dc1Convert::TextToString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		XMLString::release(&m_Base);
		m_Base = tmp;
		found = true;
	}
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioBlockFormatType_speakerLabel_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCoreaudioBlockFormatType_speakerLabel_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_speakerLabel_LocalType_ExtMethodImpl.h


