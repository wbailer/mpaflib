
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtImplInclude.h


#include "EbuCorerelationType.h"
#include "EbuCorealternativeTitleType.h"
#include "EbuCoretimeType.h"
#include "Dc1elementType.h"
#include "EbuCoreidentifierType.h"
#include "EbuCorecoreMetadataType_hasTrackPart_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorecoreMetadataType_hasTrackPart_LocalType::IEbuCorecoreMetadataType_hasTrackPart_LocalType()
{

// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtPropInit.h

}

IEbuCorecoreMetadataType_hasTrackPart_LocalType::~IEbuCorecoreMetadataType_hasTrackPart_LocalType()
{
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtPropCleanup.h

}

EbuCorecoreMetadataType_hasTrackPart_LocalType::EbuCorecoreMetadataType_hasTrackPart_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecoreMetadataType_hasTrackPart_LocalType::~EbuCorecoreMetadataType_hasTrackPart_LocalType()
{
	Cleanup();
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::Init()
{
	// Init base
	m_Base = CreaterelationType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_trackPartTitle = EbuCorealternativeTitlePtr(); // Class
	m_destinationId = NULL; // Optional String
	m_destinationId_Exist = false;
	m_destinationStart = EbuCoretimePtr(); // Class
	m_destinationStart_Exist = false;
	m_destinationEnd = EbuCoretimePtr(); // Class
	m_destinationEnd_Exist = false;
	m_sourceId = NULL; // Optional String
	m_sourceId_Exist = false;
	m_sourceStart = EbuCoretimePtr(); // Class
	m_sourceStart_Exist = false;
	m_sourceEnd = EbuCoretimePtr(); // Class
	m_sourceEnd_Exist = false;


// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMyPropInit.h

}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_trackPartTitle);
	XMLString::release(&this->m_destinationId);
	this->m_destinationId = NULL;
	// Dc1Factory::DeleteObject(m_destinationStart);
	// Dc1Factory::DeleteObject(m_destinationEnd);
	XMLString::release(&this->m_sourceId);
	this->m_sourceId = NULL;
	// Dc1Factory::DeleteObject(m_sourceStart);
	// Dc1Factory::DeleteObject(m_sourceEnd);
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use coreMetadataType_hasTrackPart_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IcoreMetadataType_hasTrackPart_LocalType
	const Dc1Ptr< EbuCorecoreMetadataType_hasTrackPart_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = EbuCorerelationPtr(Dc1Factory::CloneObject((dynamic_cast< const EbuCorecoreMetadataType_hasTrackPart_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_trackPartTitle);
		this->SettrackPartTitle(Dc1Factory::CloneObject(tmp->GettrackPartTitle()));
	if (tmp->IsValiddestinationId())
	{
		this->SetdestinationId(XMLString::replicate(tmp->GetdestinationId()));
	}
	else
	{
		InvalidatedestinationId();
	}
	if (tmp->IsValiddestinationStart())
	{
		// Dc1Factory::DeleteObject(m_destinationStart);
		this->SetdestinationStart(Dc1Factory::CloneObject(tmp->GetdestinationStart()));
	}
	else
	{
		InvalidatedestinationStart();
	}
	if (tmp->IsValiddestinationEnd())
	{
		// Dc1Factory::DeleteObject(m_destinationEnd);
		this->SetdestinationEnd(Dc1Factory::CloneObject(tmp->GetdestinationEnd()));
	}
	else
	{
		InvalidatedestinationEnd();
	}
	if (tmp->IsValidsourceId())
	{
		this->SetsourceId(XMLString::replicate(tmp->GetsourceId()));
	}
	else
	{
		InvalidatesourceId();
	}
	if (tmp->IsValidsourceStart())
	{
		// Dc1Factory::DeleteObject(m_sourceStart);
		this->SetsourceStart(Dc1Factory::CloneObject(tmp->GetsourceStart()));
	}
	else
	{
		InvalidatesourceStart();
	}
	if (tmp->IsValidsourceEnd())
	{
		// Dc1Factory::DeleteObject(m_sourceEnd);
		this->SetsourceEnd(Dc1Factory::CloneObject(tmp->GetsourceEnd()));
	}
	else
	{
		InvalidatesourceEnd();
	}
}

Dc1NodePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< EbuCorerelationType > EbuCorecoreMetadataType_hasTrackPart_LocalType::GetBase() const
{
	return m_Base;
}

EbuCorealternativeTitlePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GettrackPartTitle() const
{
		return m_trackPartTitle;
}

XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GetdestinationId() const
{
		return m_destinationId;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValiddestinationId() const
{
	return m_destinationId_Exist;
}

EbuCoretimePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetdestinationStart() const
{
		return m_destinationStart;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValiddestinationStart() const
{
	return m_destinationStart_Exist;
}

EbuCoretimePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetdestinationEnd() const
{
		return m_destinationEnd;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValiddestinationEnd() const
{
	return m_destinationEnd_Exist;
}

XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GetsourceId() const
{
		return m_sourceId;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValidsourceId() const
{
	return m_sourceId_Exist;
}

EbuCoretimePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetsourceStart() const
{
		return m_sourceStart;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValidsourceStart() const
{
	return m_sourceStart_Exist;
}

EbuCoretimePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetsourceEnd() const
{
		return m_sourceEnd;
}

// Element is optional
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::IsValidsourceEnd() const
{
	return m_sourceEnd_Exist;
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettrackPartTitle(const EbuCorealternativeTitlePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettrackPartTitle().");
	}
	if (m_trackPartTitle != item)
	{
		// Dc1Factory::DeleteObject(m_trackPartTitle);
		m_trackPartTitle = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_trackPartTitle) m_trackPartTitle->SetParent(m_myself.getPointer());
		if (m_trackPartTitle && m_trackPartTitle->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_trackPartTitle->UseTypeAttribute = true;
		}
		if(m_trackPartTitle != EbuCorealternativeTitlePtr())
		{
			m_trackPartTitle->SetContentName(XMLString::transcode("trackPartTitle"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationId().");
	}
	if (m_destinationId != item || m_destinationId_Exist == false)
	{
		XMLString::release(&m_destinationId);
		m_destinationId = item;
		m_destinationId_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatedestinationId()
{
	m_destinationId_Exist = false;
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationStart(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationStart().");
	}
	if (m_destinationStart != item || m_destinationStart_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_destinationStart);
		m_destinationStart = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_destinationStart) m_destinationStart->SetParent(m_myself.getPointer());
		if (m_destinationStart && m_destinationStart->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_destinationStart->UseTypeAttribute = true;
		}
		m_destinationStart_Exist = true;
		if(m_destinationStart != EbuCoretimePtr())
		{
			m_destinationStart->SetContentName(XMLString::transcode("destinationStart"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatedestinationStart()
{
	m_destinationStart_Exist = false;
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationEnd(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetdestinationEnd().");
	}
	if (m_destinationEnd != item || m_destinationEnd_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_destinationEnd);
		m_destinationEnd = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_destinationEnd) m_destinationEnd->SetParent(m_myself.getPointer());
		if (m_destinationEnd && m_destinationEnd->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_destinationEnd->UseTypeAttribute = true;
		}
		m_destinationEnd_Exist = true;
		if(m_destinationEnd != EbuCoretimePtr())
		{
			m_destinationEnd->SetContentName(XMLString::transcode("destinationEnd"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatedestinationEnd()
{
	m_destinationEnd_Exist = false;
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceId().");
	}
	if (m_sourceId != item || m_sourceId_Exist == false)
	{
		XMLString::release(&m_sourceId);
		m_sourceId = item;
		m_sourceId_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatesourceId()
{
	m_sourceId_Exist = false;
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceStart(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceStart().");
	}
	if (m_sourceStart != item || m_sourceStart_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_sourceStart);
		m_sourceStart = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_sourceStart) m_sourceStart->SetParent(m_myself.getPointer());
		if (m_sourceStart && m_sourceStart->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_sourceStart->UseTypeAttribute = true;
		}
		m_sourceStart_Exist = true;
		if(m_sourceStart != EbuCoretimePtr())
		{
			m_sourceStart->SetContentName(XMLString::transcode("sourceStart"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatesourceStart()
{
	m_sourceStart_Exist = false;
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceEnd(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetsourceEnd().");
	}
	if (m_sourceEnd != item || m_sourceEnd_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_sourceEnd);
		m_sourceEnd = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_sourceEnd) m_sourceEnd->SetParent(m_myself.getPointer());
		if (m_sourceEnd && m_sourceEnd->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_sourceEnd->UseTypeAttribute = true;
		}
		m_sourceEnd_Exist = true;
		if(m_sourceEnd != EbuCoretimePtr())
		{
			m_sourceEnd->SetContentName(XMLString::transcode("sourceEnd"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatesourceEnd()
{
	m_sourceEnd_Exist = false;
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GettypeLabel() const
{
	return GetBase()->GettypeLabel();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttypeLabel() const
{
	return GetBase()->ExisttypeLabel();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLabel().");
	}
	GetBase()->SettypeLabel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetypeLabel()
{
	GetBase()->InvalidatetypeLabel();
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GettypeDefinition() const
{
	return GetBase()->GettypeDefinition();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttypeDefinition() const
{
	return GetBase()->ExisttypeDefinition();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeDefinition().");
	}
	GetBase()->SettypeDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetypeDefinition()
{
	GetBase()->InvalidatetypeDefinition();
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GettypeLink() const
{
	return GetBase()->GettypeLink();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttypeLink() const
{
	return GetBase()->ExisttypeLink();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLink().");
	}
	GetBase()->SettypeLink(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetypeLink()
{
	GetBase()->InvalidatetypeLink();
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GettypeSource() const
{
	return GetBase()->GettypeSource();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttypeSource() const
{
	return GetBase()->ExisttypeSource();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeSource().");
	}
	GetBase()->SettypeSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetypeSource()
{
	GetBase()->InvalidatetypeSource();
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GettypeLanguage() const
{
	return GetBase()->GettypeLanguage();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttypeLanguage() const
{
	return GetBase()->ExisttypeLanguage();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettypeLanguage().");
	}
	GetBase()->SettypeLanguage(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetypeLanguage()
{
	GetBase()->InvalidatetypeLanguage();
}
int EbuCorecoreMetadataType_hasTrackPart_LocalType::GetrunningOrderNumber() const
{
	return GetBase()->GetrunningOrderNumber();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExistrunningOrderNumber() const
{
	return GetBase()->ExistrunningOrderNumber();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrunningOrderNumber(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrunningOrderNumber().");
	}
	GetBase()->SetrunningOrderNumber(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidaterunningOrderNumber()
{
	GetBase()->InvalidaterunningOrderNumber();
}
int EbuCorecoreMetadataType_hasTrackPart_LocalType::GettotalNumberOfGroupMembers() const
{
	return GetBase()->GettotalNumberOfGroupMembers();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExisttotalNumberOfGroupMembers() const
{
	return GetBase()->ExisttotalNumberOfGroupMembers();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SettotalNumberOfGroupMembers(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SettotalNumberOfGroupMembers().");
	}
	GetBase()->SettotalNumberOfGroupMembers(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidatetotalNumberOfGroupMembers()
{
	GetBase()->InvalidatetotalNumberOfGroupMembers();
}
bool EbuCorecoreMetadataType_hasTrackPart_LocalType::GetorderedGroupFlag() const
{
	return GetBase()->GetorderedGroupFlag();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::ExistorderedGroupFlag() const
{
	return GetBase()->ExistorderedGroupFlag();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetorderedGroupFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetorderedGroupFlag().");
	}
	GetBase()->SetorderedGroupFlag(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::InvalidateorderedGroupFlag()
{
	GetBase()->InvalidateorderedGroupFlag();
}
XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::Getnote() const
{
	return GetBase()->Getnote();
}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::Existnote() const
{
	return GetBase()->Existnote();
}
void EbuCorecoreMetadataType_hasTrackPart_LocalType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::Setnote().");
	}
	GetBase()->Setnote(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType_hasTrackPart_LocalType::Invalidatenote()
{
	GetBase()->Invalidatenote();
}
Dc1elementPtr EbuCorecoreMetadataType_hasTrackPart_LocalType::Getrelation() const
{
	return GetBase()->Getrelation();
}

EbuCoreidentifierPtr EbuCorecoreMetadataType_hasTrackPart_LocalType::GetrelationIdentifier() const
{
	return GetBase()->GetrelationIdentifier();
}

XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::GetrelationLink() const
{
	return GetBase()->GetrelationLink();
}

// implementing setter for choice 
/* element */
void EbuCorecoreMetadataType_hasTrackPart_LocalType::Setrelation(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::Setrelation().");
	}
	GetBase()->Setrelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrelationIdentifier(const EbuCoreidentifierPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrelationIdentifier().");
	}
	GetBase()->SetrelationIdentifier(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrelationLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType_hasTrackPart_LocalType::SetrelationLink().");
	}
	GetBase()->SetrelationLink(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorecoreMetadataType_hasTrackPart_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("trackPartTitle")) == 0)
	{
		// trackPartTitle is simple element alternativeTitleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettrackPartTitle()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType")))) != empty)
			{
				// Is type allowed
				EbuCorealternativeTitlePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettrackPartTitle(p, client);
					if((p = GettrackPartTitle()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("destinationStart")) == 0)
	{
		// destinationStart is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdestinationStart()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdestinationStart(p, client);
					if((p = GetdestinationStart()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("destinationEnd")) == 0)
	{
		// destinationEnd is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdestinationEnd()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdestinationEnd(p, client);
					if((p = GetdestinationEnd()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("sourceStart")) == 0)
	{
		// sourceStart is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetsourceStart()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetsourceStart(p, client);
					if((p = GetsourceStart()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("sourceEnd")) == 0)
	{
		// sourceEnd is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetsourceEnd()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetsourceEnd(p, client);
					if((p = GetsourceEnd()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for coreMetadataType_hasTrackPart_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "coreMetadataType_hasTrackPart_LocalType");
	}
	return result;
}

XMLCh * EbuCorecoreMetadataType_hasTrackPart_LocalType::ToText() const
{
	return m_Base->ToText();
}


bool EbuCorecoreMetadataType_hasTrackPart_LocalType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void EbuCorecoreMetadataType_hasTrackPart_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("coreMetadataType_hasTrackPart_LocalType"));
	// Element serialization:
	// Class
	
	if (m_trackPartTitle != EbuCorealternativeTitlePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_trackPartTitle->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("trackPartTitle"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_trackPartTitle->Serialize(doc, element, &elem);
	}
	 // String
	if(m_destinationId != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_destinationId, X("urn:ebu:metadata-schema:ebuCore_2014"), X("destinationId"), true);
	// Class
	
	if (m_destinationStart != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_destinationStart->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("destinationStart"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_destinationStart->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_destinationEnd != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_destinationEnd->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("destinationEnd"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_destinationEnd->Serialize(doc, element, &elem);
	}
	 // String
	if(m_sourceId != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_sourceId, X("urn:ebu:metadata-schema:ebuCore_2014"), X("sourceId"), true);
	// Class
	
	if (m_sourceStart != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_sourceStart->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("sourceStart"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_sourceStart->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_sourceEnd != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_sourceEnd->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("sourceEnd"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_sourceEnd->Serialize(doc, element, &elem);
	}

}

bool EbuCorecoreMetadataType_hasTrackPart_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is EbuCorerelationType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("trackPartTitle"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("trackPartTitle")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatealternativeTitleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettrackPartTitle(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("destinationId"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetdestinationId(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetdestinationId(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("destinationStart"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("destinationStart")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetdestinationStart(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("destinationEnd"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("destinationEnd")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetdestinationEnd(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("sourceId"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetsourceId(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetsourceId(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("sourceStart"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("sourceStart")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetsourceStart(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("sourceEnd"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("sourceEnd")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetsourceEnd(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecoreMetadataType_hasTrackPart_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("trackPartTitle")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatealternativeTitleType; // FTT, check this
	}
	this->SettrackPartTitle(child);
  }
  if (XMLString::compareString(elementname, X("destinationStart")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->SetdestinationStart(child);
  }
  if (XMLString::compareString(elementname, X("destinationEnd")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->SetdestinationEnd(child);
  }
  if (XMLString::compareString(elementname, X("sourceStart")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->SetsourceStart(child);
  }
  if (XMLString::compareString(elementname, X("sourceEnd")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->SetsourceEnd(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorecoreMetadataType_hasTrackPart_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GettrackPartTitle() != Dc1NodePtr())
		result.Insert(GettrackPartTitle());
	if (GetdestinationStart() != Dc1NodePtr())
		result.Insert(GetdestinationStart());
	if (GetdestinationEnd() != Dc1NodePtr())
		result.Insert(GetdestinationEnd());
	if (GetsourceStart() != Dc1NodePtr())
		result.Insert(GetsourceStart());
	if (GetsourceEnd() != Dc1NodePtr())
		result.Insert(GetsourceEnd());
	if (Getrelation() != Dc1NodePtr())
		result.Insert(Getrelation());
	if (GetrelationIdentifier() != Dc1NodePtr())
		result.Insert(GetrelationIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMethodImpl.h


