
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtImplInclude.h


#include "EbuCoreaudioFormatType_audioEncoding_LocalType.h"
#include "EbuCorecodecType.h"
#include "EbuCoreaudioFormatType_audioTrackConfiguration_LocalType.h"
#include "EbuCoreaudioFormatType_audioTrack_CollectionType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCoreaudioFormatType_comment_CollectionType.h"
#include "EbuCoreaudioFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreaudioFormatType_audioTrack_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioTrack
#include "EbuCorecomment_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:comment

#include <assert.h>
IEbuCoreaudioFormatType::IEbuCoreaudioFormatType()
{

// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtPropInit.h

}

IEbuCoreaudioFormatType::~IEbuCoreaudioFormatType()
{
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtPropCleanup.h

}

EbuCoreaudioFormatType::EbuCoreaudioFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioFormatType::~EbuCoreaudioFormatType()
{
	Cleanup();
}

void EbuCoreaudioFormatType::Init()
{

	// Init attributes
	m_audioFormatId = NULL; // String
	m_audioFormatId_Exist = false;
	m_audioFormatVersionId = NULL; // String
	m_audioFormatVersionId_Exist = false;
	m_audioFormatName = NULL; // String
	m_audioFormatName_Exist = false;
	m_audioFormatDefinition = NULL; // String
	m_audioFormatDefinition_Exist = false;
	m_audioPresenceFlag = false; // Value
	m_audioPresenceFlag_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioEncoding = EbuCoreaudioFormatType_audioEncoding_LocalPtr(); // Class
	m_audioEncoding_Exist = false;
	m_codec = EbuCorecodecPtr(); // Class
	m_codec_Exist = false;
	m_audioTrackConfiguration = EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr(); // Class
	m_audioTrackConfiguration_Exist = false;
	m_samplingRate = (0); // Value

	m_samplingRate_Exist = false;
	m_sampleSize = (0); // Value

	m_sampleSize_Exist = false;
	m_sampleType = EbuCoreaudioFormatType_sampleType_LocalType::UninitializedEnumeration; // Enumeration
	m_sampleType_Exist = false;
	m_bitRate = (0); // Value

	m_bitRate_Exist = false;
	m_bitRateMax = (0); // Value

	m_bitRateMax_Exist = false;
	m_bitRateMode = EbuCoreaudioFormatType_bitRateMode_LocalType::UninitializedEnumeration; // Enumeration
	m_bitRateMode_Exist = false;
	m_audioTrack = EbuCoreaudioFormatType_audioTrack_CollectionPtr(); // Collection
	m_channels = (0); // Value

	m_channels_Exist = false;
	m_technicalAttributes = EbuCoretechnicalAttributesPtr(); // Class
	m_comment = EbuCoreaudioFormatType_comment_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMyPropInit.h

}

void EbuCoreaudioFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_audioFormatId); // String
	XMLString::release(&m_audioFormatVersionId); // String
	XMLString::release(&m_audioFormatName); // String
	XMLString::release(&m_audioFormatDefinition); // String
	// Dc1Factory::DeleteObject(m_audioEncoding);
	// Dc1Factory::DeleteObject(m_codec);
	// Dc1Factory::DeleteObject(m_audioTrackConfiguration);
	// Dc1Factory::DeleteObject(m_audioTrack);
	// Dc1Factory::DeleteObject(m_technicalAttributes);
	// Dc1Factory::DeleteObject(m_comment);
}

void EbuCoreaudioFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioFormatType
	const Dc1Ptr< EbuCoreaudioFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioFormatId); // String
	if (tmp->ExistaudioFormatId())
	{
		this->SetaudioFormatId(XMLString::replicate(tmp->GetaudioFormatId()));
	}
	else
	{
		InvalidateaudioFormatId();
	}
	}
	{
	XMLString::release(&m_audioFormatVersionId); // String
	if (tmp->ExistaudioFormatVersionId())
	{
		this->SetaudioFormatVersionId(XMLString::replicate(tmp->GetaudioFormatVersionId()));
	}
	else
	{
		InvalidateaudioFormatVersionId();
	}
	}
	{
	XMLString::release(&m_audioFormatName); // String
	if (tmp->ExistaudioFormatName())
	{
		this->SetaudioFormatName(XMLString::replicate(tmp->GetaudioFormatName()));
	}
	else
	{
		InvalidateaudioFormatName();
	}
	}
	{
	XMLString::release(&m_audioFormatDefinition); // String
	if (tmp->ExistaudioFormatDefinition())
	{
		this->SetaudioFormatDefinition(XMLString::replicate(tmp->GetaudioFormatDefinition()));
	}
	else
	{
		InvalidateaudioFormatDefinition();
	}
	}
	{
	if (tmp->ExistaudioPresenceFlag())
	{
		this->SetaudioPresenceFlag(tmp->GetaudioPresenceFlag());
	}
	else
	{
		InvalidateaudioPresenceFlag();
	}
	}
	if (tmp->IsValidaudioEncoding())
	{
		// Dc1Factory::DeleteObject(m_audioEncoding);
		this->SetaudioEncoding(Dc1Factory::CloneObject(tmp->GetaudioEncoding()));
	}
	else
	{
		InvalidateaudioEncoding();
	}
	if (tmp->IsValidcodec())
	{
		// Dc1Factory::DeleteObject(m_codec);
		this->Setcodec(Dc1Factory::CloneObject(tmp->Getcodec()));
	}
	else
	{
		Invalidatecodec();
	}
	if (tmp->IsValidaudioTrackConfiguration())
	{
		// Dc1Factory::DeleteObject(m_audioTrackConfiguration);
		this->SetaudioTrackConfiguration(Dc1Factory::CloneObject(tmp->GetaudioTrackConfiguration()));
	}
	else
	{
		InvalidateaudioTrackConfiguration();
	}
	if (tmp->IsValidsamplingRate())
	{
		this->SetsamplingRate(tmp->GetsamplingRate());
	}
	else
	{
		InvalidatesamplingRate();
	}
	if (tmp->IsValidsampleSize())
	{
		this->SetsampleSize(tmp->GetsampleSize());
	}
	else
	{
		InvalidatesampleSize();
	}
	if (tmp->IsValidsampleType())
	{
		this->SetsampleType(tmp->GetsampleType());
	}
	else
	{
		InvalidatesampleType();
	}
	if (tmp->IsValidbitRate())
	{
		this->SetbitRate(tmp->GetbitRate());
	}
	else
	{
		InvalidatebitRate();
	}
	if (tmp->IsValidbitRateMax())
	{
		this->SetbitRateMax(tmp->GetbitRateMax());
	}
	else
	{
		InvalidatebitRateMax();
	}
	if (tmp->IsValidbitRateMode())
	{
		this->SetbitRateMode(tmp->GetbitRateMode());
	}
	else
	{
		InvalidatebitRateMode();
	}
		// Dc1Factory::DeleteObject(m_audioTrack);
		this->SetaudioTrack(Dc1Factory::CloneObject(tmp->GetaudioTrack()));
	if (tmp->IsValidchannels())
	{
		this->Setchannels(tmp->Getchannels());
	}
	else
	{
		Invalidatechannels();
	}
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		this->SettechnicalAttributes(Dc1Factory::CloneObject(tmp->GettechnicalAttributes()));
		// Dc1Factory::DeleteObject(m_comment);
		this->Setcomment(Dc1Factory::CloneObject(tmp->Getcomment()));
}

Dc1NodePtr EbuCoreaudioFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioFormatType::GetaudioFormatId() const
{
	return m_audioFormatId;
}

bool EbuCoreaudioFormatType::ExistaudioFormatId() const
{
	return m_audioFormatId_Exist;
}
void EbuCoreaudioFormatType::SetaudioFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioFormatId().");
	}
	m_audioFormatId = item;
	m_audioFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioFormatId()
{
	m_audioFormatId_Exist = false;
}
XMLCh * EbuCoreaudioFormatType::GetaudioFormatVersionId() const
{
	return m_audioFormatVersionId;
}

bool EbuCoreaudioFormatType::ExistaudioFormatVersionId() const
{
	return m_audioFormatVersionId_Exist;
}
void EbuCoreaudioFormatType::SetaudioFormatVersionId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioFormatVersionId().");
	}
	m_audioFormatVersionId = item;
	m_audioFormatVersionId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioFormatVersionId()
{
	m_audioFormatVersionId_Exist = false;
}
XMLCh * EbuCoreaudioFormatType::GetaudioFormatName() const
{
	return m_audioFormatName;
}

bool EbuCoreaudioFormatType::ExistaudioFormatName() const
{
	return m_audioFormatName_Exist;
}
void EbuCoreaudioFormatType::SetaudioFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioFormatName().");
	}
	m_audioFormatName = item;
	m_audioFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioFormatName()
{
	m_audioFormatName_Exist = false;
}
XMLCh * EbuCoreaudioFormatType::GetaudioFormatDefinition() const
{
	return m_audioFormatDefinition;
}

bool EbuCoreaudioFormatType::ExistaudioFormatDefinition() const
{
	return m_audioFormatDefinition_Exist;
}
void EbuCoreaudioFormatType::SetaudioFormatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioFormatDefinition().");
	}
	m_audioFormatDefinition = item;
	m_audioFormatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioFormatDefinition()
{
	m_audioFormatDefinition_Exist = false;
}
bool EbuCoreaudioFormatType::GetaudioPresenceFlag() const
{
	return m_audioPresenceFlag;
}

bool EbuCoreaudioFormatType::ExistaudioPresenceFlag() const
{
	return m_audioPresenceFlag_Exist;
}
void EbuCoreaudioFormatType::SetaudioPresenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioPresenceFlag().");
	}
	m_audioPresenceFlag = item;
	m_audioPresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioPresenceFlag()
{
	m_audioPresenceFlag_Exist = false;
}
EbuCoreaudioFormatType_audioEncoding_LocalPtr EbuCoreaudioFormatType::GetaudioEncoding() const
{
		return m_audioEncoding;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidaudioEncoding() const
{
	return m_audioEncoding_Exist;
}

EbuCorecodecPtr EbuCoreaudioFormatType::Getcodec() const
{
		return m_codec;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidcodec() const
{
	return m_codec_Exist;
}

EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr EbuCoreaudioFormatType::GetaudioTrackConfiguration() const
{
		return m_audioTrackConfiguration;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidaudioTrackConfiguration() const
{
	return m_audioTrackConfiguration_Exist;
}

int EbuCoreaudioFormatType::GetsamplingRate() const
{
		return m_samplingRate;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidsamplingRate() const
{
	return m_samplingRate_Exist;
}

unsigned EbuCoreaudioFormatType::GetsampleSize() const
{
		return m_sampleSize;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidsampleSize() const
{
	return m_sampleSize_Exist;
}

EbuCoreaudioFormatType_sampleType_LocalType::Enumeration EbuCoreaudioFormatType::GetsampleType() const
{
		return m_sampleType;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidsampleType() const
{
	return m_sampleType_Exist;
}

unsigned EbuCoreaudioFormatType::GetbitRate() const
{
		return m_bitRate;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidbitRate() const
{
	return m_bitRate_Exist;
}

unsigned EbuCoreaudioFormatType::GetbitRateMax() const
{
		return m_bitRateMax;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidbitRateMax() const
{
	return m_bitRateMax_Exist;
}

EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration EbuCoreaudioFormatType::GetbitRateMode() const
{
		return m_bitRateMode;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidbitRateMode() const
{
	return m_bitRateMode_Exist;
}

EbuCoreaudioFormatType_audioTrack_CollectionPtr EbuCoreaudioFormatType::GetaudioTrack() const
{
		return m_audioTrack;
}

unsigned EbuCoreaudioFormatType::Getchannels() const
{
		return m_channels;
}

// Element is optional
bool EbuCoreaudioFormatType::IsValidchannels() const
{
	return m_channels_Exist;
}

EbuCoretechnicalAttributesPtr EbuCoreaudioFormatType::GettechnicalAttributes() const
{
		return m_technicalAttributes;
}

EbuCoreaudioFormatType_comment_CollectionPtr EbuCoreaudioFormatType::Getcomment() const
{
		return m_comment;
}

void EbuCoreaudioFormatType::SetaudioEncoding(const EbuCoreaudioFormatType_audioEncoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioEncoding().");
	}
	if (m_audioEncoding != item || m_audioEncoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_audioEncoding);
		m_audioEncoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioEncoding) m_audioEncoding->SetParent(m_myself.getPointer());
		if (m_audioEncoding && m_audioEncoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioEncoding_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_audioEncoding->UseTypeAttribute = true;
		}
		m_audioEncoding_Exist = true;
		if(m_audioEncoding != EbuCoreaudioFormatType_audioEncoding_LocalPtr())
		{
			m_audioEncoding->SetContentName(XMLString::transcode("audioEncoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioEncoding()
{
	m_audioEncoding_Exist = false;
}
void EbuCoreaudioFormatType::Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::Setcodec().");
	}
	if (m_codec != item || m_codec_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_codec);
		m_codec = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_codec) m_codec->SetParent(m_myself.getPointer());
		if (m_codec && m_codec->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_codec->UseTypeAttribute = true;
		}
		m_codec_Exist = true;
		if(m_codec != EbuCorecodecPtr())
		{
			m_codec->SetContentName(XMLString::transcode("codec"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::Invalidatecodec()
{
	m_codec_Exist = false;
}
void EbuCoreaudioFormatType::SetaudioTrackConfiguration(const EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioTrackConfiguration().");
	}
	if (m_audioTrackConfiguration != item || m_audioTrackConfiguration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_audioTrackConfiguration);
		m_audioTrackConfiguration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrackConfiguration) m_audioTrackConfiguration->SetParent(m_myself.getPointer());
		if (m_audioTrackConfiguration && m_audioTrackConfiguration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrackConfiguration_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_audioTrackConfiguration->UseTypeAttribute = true;
		}
		m_audioTrackConfiguration_Exist = true;
		if(m_audioTrackConfiguration != EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr())
		{
			m_audioTrackConfiguration->SetContentName(XMLString::transcode("audioTrackConfiguration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidateaudioTrackConfiguration()
{
	m_audioTrackConfiguration_Exist = false;
}
void EbuCoreaudioFormatType::SetsamplingRate(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetsamplingRate().");
	}
	if (m_samplingRate != item || m_samplingRate_Exist == false)
	{
		m_samplingRate = item;
		m_samplingRate_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatesamplingRate()
{
	m_samplingRate_Exist = false;
}
void EbuCoreaudioFormatType::SetsampleSize(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetsampleSize().");
	}
	if (m_sampleSize != item || m_sampleSize_Exist == false)
	{
		m_sampleSize = item;
		m_sampleSize_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatesampleSize()
{
	m_sampleSize_Exist = false;
}
void EbuCoreaudioFormatType::SetsampleType(EbuCoreaudioFormatType_sampleType_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetsampleType().");
	}
	if (m_sampleType != item || m_sampleType_Exist == false)
	{
		// nothing to free here, hopefully
		m_sampleType = item;
		m_sampleType_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatesampleType()
{
	m_sampleType_Exist = false;
}
void EbuCoreaudioFormatType::SetbitRate(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetbitRate().");
	}
	if (m_bitRate != item || m_bitRate_Exist == false)
	{
		m_bitRate = item;
		m_bitRate_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatebitRate()
{
	m_bitRate_Exist = false;
}
void EbuCoreaudioFormatType::SetbitRateMax(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetbitRateMax().");
	}
	if (m_bitRateMax != item || m_bitRateMax_Exist == false)
	{
		m_bitRateMax = item;
		m_bitRateMax_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatebitRateMax()
{
	m_bitRateMax_Exist = false;
}
void EbuCoreaudioFormatType::SetbitRateMode(EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetbitRateMode().");
	}
	if (m_bitRateMode != item || m_bitRateMode_Exist == false)
	{
		// nothing to free here, hopefully
		m_bitRateMode = item;
		m_bitRateMode_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::InvalidatebitRateMode()
{
	m_bitRateMode_Exist = false;
}
void EbuCoreaudioFormatType::SetaudioTrack(const EbuCoreaudioFormatType_audioTrack_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SetaudioTrack().");
	}
	if (m_audioTrack != item)
	{
		// Dc1Factory::DeleteObject(m_audioTrack);
		m_audioTrack = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrack) m_audioTrack->SetParent(m_myself.getPointer());
		if(m_audioTrack != EbuCoreaudioFormatType_audioTrack_CollectionPtr())
		{
			m_audioTrack->SetContentName(XMLString::transcode("audioTrack"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::Setchannels(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::Setchannels().");
	}
	if (m_channels != item || m_channels_Exist == false)
	{
		m_channels = item;
		m_channels_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::Invalidatechannels()
{
	m_channels_Exist = false;
}
void EbuCoreaudioFormatType::SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::SettechnicalAttributes().");
	}
	if (m_technicalAttributes != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		m_technicalAttributes = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributes) m_technicalAttributes->SetParent(m_myself.getPointer());
		if (m_technicalAttributes && m_technicalAttributes->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_technicalAttributes->UseTypeAttribute = true;
		}
		if(m_technicalAttributes != EbuCoretechnicalAttributesPtr())
		{
			m_technicalAttributes->SetContentName(XMLString::transcode("technicalAttributes"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatType::Setcomment(const EbuCoreaudioFormatType_comment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatType::Setcomment().");
	}
	if (m_comment != item)
	{
		// Dc1Factory::DeleteObject(m_comment);
		m_comment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_comment) m_comment->SetParent(m_myself.getPointer());
		if(m_comment != EbuCoreaudioFormatType_comment_CollectionPtr())
		{
			m_comment->SetContentName(XMLString::transcode("comment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatId")) == 0)
	{
		// audioFormatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatVersionId")) == 0)
	{
		// audioFormatVersionId is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatName")) == 0)
	{
		// audioFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatDefinition")) == 0)
	{
		// audioFormatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioPresenceFlag")) == 0)
	{
		// audioPresenceFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioEncoding")) == 0)
	{
		// audioEncoding is simple element audioFormatType_audioEncoding_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaudioEncoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioEncoding_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioFormatType_audioEncoding_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaudioEncoding(p, client);
					if((p = GetaudioEncoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("codec")) == 0)
	{
		// codec is simple element codecType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcodec()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType")))) != empty)
			{
				// Is type allowed
				EbuCorecodecPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcodec(p, client);
					if((p = Getcodec()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrackConfiguration")) == 0)
	{
		// audioTrackConfiguration is simple element audioFormatType_audioTrackConfiguration_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaudioTrackConfiguration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrackConfiguration_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaudioTrackConfiguration(p, client);
					if((p = GetaudioTrackConfiguration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrack")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioTrack is item of type audioFormatType_audioTrack_LocalType
		// in element collection audioFormatType_audioTrack_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatType_audioTrack_CollectionPtr coll = GetaudioTrack();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatType_audioTrack_CollectionType; // FTT, check this
				SetaudioTrack(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrack_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioFormatType_audioTrack_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributes")) == 0)
	{
		// technicalAttributes is simple element technicalAttributes
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettechnicalAttributes()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettechnicalAttributes(p, client);
					if((p = GettechnicalAttributes()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("comment")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:comment is item of type comment_LocalType
		// in element collection audioFormatType_comment_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatType_comment_CollectionPtr coll = Getcomment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatType_comment_CollectionType; // FTT, check this
				Setcomment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecomment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioFormatType");
	}
	return result;
}

XMLCh * EbuCoreaudioFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatId_Exist)
	{
	// String
	if(m_audioFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatId"), m_audioFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatVersionId_Exist)
	{
	// String
	if(m_audioFormatVersionId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatVersionId"), m_audioFormatVersionId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatName_Exist)
	{
	// String
	if(m_audioFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatName"), m_audioFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatDefinition_Exist)
	{
	// String
	if(m_audioFormatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatDefinition"), m_audioFormatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioPresenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_audioPresenceFlag);
		element->setAttributeNS(X(""), X("audioPresenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_audioEncoding != EbuCoreaudioFormatType_audioEncoding_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_audioEncoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioEncoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_audioEncoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_codec != EbuCorecodecPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_codec->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("codec"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_codec->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_audioTrackConfiguration != EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_audioTrackConfiguration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioTrackConfiguration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_audioTrackConfiguration->Serialize(doc, element, &elem);
	}
	if(m_samplingRate_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_samplingRate);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("samplingRate"), true);
		XMLString::release(&tmp);
	}
	if(m_sampleSize_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_sampleSize);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("sampleSize"), true);
		XMLString::release(&tmp);
	}
	if(m_sampleType != EbuCoreaudioFormatType_sampleType_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCoreaudioFormatType_sampleType_LocalType::ToText(m_sampleType);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("sampleType"), true);
		XMLString::release(&tmp);
	}
	if(m_bitRate_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_bitRate);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRate"), true);
		XMLString::release(&tmp);
	}
	if(m_bitRateMax_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_bitRateMax);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRateMax"), true);
		XMLString::release(&tmp);
	}
	if(m_bitRateMode != EbuCoreaudioFormatType_bitRateMode_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCoreaudioFormatType_bitRateMode_LocalType::ToText(m_bitRateMode);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("bitRateMode"), true);
		XMLString::release(&tmp);
	}
	if (m_audioTrack != EbuCoreaudioFormatType_audioTrack_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioTrack->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_channels_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_channels);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("channels"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_technicalAttributes != EbuCoretechnicalAttributesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_technicalAttributes->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_technicalAttributes->Serialize(doc, element, &elem);
	}
	if (m_comment != EbuCoreaudioFormatType_comment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_comment->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatId")))
	{
		// Deserialize string type
		this->SetaudioFormatId(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatVersionId")))
	{
		// Deserialize string type
		this->SetaudioFormatVersionId(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatVersionId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatName")))
	{
		// Deserialize string type
		this->SetaudioFormatName(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatDefinition")))
	{
		// Deserialize string type
		this->SetaudioFormatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioPresenceFlag")))
	{
		// deserialize value type
		this->SetaudioPresenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("audioPresenceFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("audioEncoding"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("audioEncoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaudioFormatType_audioEncoding_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaudioEncoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("codec"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("codec")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecodecType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcodec(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("audioTrackConfiguration"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("audioTrackConfiguration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaudioFormatType_audioTrackConfiguration_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaudioTrackConfiguration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("samplingRate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetsamplingRate(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("sampleSize"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetsampleSize(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("sampleType"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("sampleType")) == 0))
		{
			EbuCoreaudioFormatType_sampleType_LocalType::Enumeration tmp = EbuCoreaudioFormatType_sampleType_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCoreaudioFormatType_sampleType_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetsampleType(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("bitRate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetbitRate(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("bitRateMax"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetbitRateMax(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("bitRateMode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("bitRateMode")) == 0))
		{
			EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration tmp = EbuCoreaudioFormatType_bitRateMode_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCoreaudioFormatType_bitRateMode_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetbitRateMode(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioTrack"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioTrack")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatType_audioTrack_CollectionPtr tmp = CreateaudioFormatType_audioTrack_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioTrack(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("channels"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setchannels(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("technicalAttributes"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("technicalAttributes")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetechnicalAttributes; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettechnicalAttributes(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("comment"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("comment")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatType_comment_CollectionPtr tmp = CreateaudioFormatType_comment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcomment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("audioEncoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaudioFormatType_audioEncoding_LocalType; // FTT, check this
	}
	this->SetaudioEncoding(child);
  }
  if (XMLString::compareString(elementname, X("codec")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecodecType; // FTT, check this
	}
	this->Setcodec(child);
  }
  if (XMLString::compareString(elementname, X("audioTrackConfiguration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaudioFormatType_audioTrackConfiguration_LocalType; // FTT, check this
	}
	this->SetaudioTrackConfiguration(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioTrack")) == 0))
  {
	EbuCoreaudioFormatType_audioTrack_CollectionPtr tmp = CreateaudioFormatType_audioTrack_CollectionType; // FTT, check this
	this->SetaudioTrack(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("technicalAttributes")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetechnicalAttributes; // FTT, check this
	}
	this->SettechnicalAttributes(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("comment")) == 0))
  {
	EbuCoreaudioFormatType_comment_CollectionPtr tmp = CreateaudioFormatType_comment_CollectionType; // FTT, check this
	this->Setcomment(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioEncoding() != Dc1NodePtr())
		result.Insert(GetaudioEncoding());
	if (Getcodec() != Dc1NodePtr())
		result.Insert(Getcodec());
	if (GetaudioTrackConfiguration() != Dc1NodePtr())
		result.Insert(GetaudioTrackConfiguration());
	if (GetaudioTrack() != Dc1NodePtr())
		result.Insert(GetaudioTrack());
	if (GettechnicalAttributes() != Dc1NodePtr())
		result.Insert(GettechnicalAttributes());
	if (Getcomment() != Dc1NodePtr())
		result.Insert(Getcomment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMethodImpl.h


