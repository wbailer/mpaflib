
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreentityType_ExtImplInclude.h


#include "EbuCoreentityType_contactDetails_CollectionType.h"
#include "EbuCoreentityType_organisationDetails_CollectionType.h"
#include "EbuCoreentityType_role_CollectionType.h"
#include "EbuCoreentityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorecontactDetailsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:contactDetails
#include "EbuCoreorganisationDetailsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:organisationDetails
#include "EbuCoreentityType_role_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:role

#include <assert.h>
IEbuCoreentityType::IEbuCoreentityType()
{

// no includefile for extension defined 
// file EbuCoreentityType_ExtPropInit.h

}

IEbuCoreentityType::~IEbuCoreentityType()
{
// no includefile for extension defined 
// file EbuCoreentityType_ExtPropCleanup.h

}

EbuCoreentityType::EbuCoreentityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreentityType::~EbuCoreentityType()
{
	Cleanup();
}

void EbuCoreentityType::Init()
{

	// Init attributes
	m_entityId = NULL; // String
	m_entityId_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_contactDetails = EbuCoreentityType_contactDetails_CollectionPtr(); // Collection
	m_organisationDetails = EbuCoreentityType_organisationDetails_CollectionPtr(); // Collection
	m_role = EbuCoreentityType_role_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreentityType_ExtMyPropInit.h

}

void EbuCoreentityType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreentityType_ExtMyPropCleanup.h


	XMLString::release(&m_entityId); // String
	// Dc1Factory::DeleteObject(m_contactDetails);
	// Dc1Factory::DeleteObject(m_organisationDetails);
	// Dc1Factory::DeleteObject(m_role);
}

void EbuCoreentityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use entityTypePtr, since we
	// might need GetBase(), which isn't defined in IentityType
	const Dc1Ptr< EbuCoreentityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_entityId); // String
	if (tmp->ExistentityId())
	{
		this->SetentityId(XMLString::replicate(tmp->GetentityId()));
	}
	else
	{
		InvalidateentityId();
	}
	}
		// Dc1Factory::DeleteObject(m_contactDetails);
		this->SetcontactDetails(Dc1Factory::CloneObject(tmp->GetcontactDetails()));
		// Dc1Factory::DeleteObject(m_organisationDetails);
		this->SetorganisationDetails(Dc1Factory::CloneObject(tmp->GetorganisationDetails()));
		// Dc1Factory::DeleteObject(m_role);
		this->Setrole(Dc1Factory::CloneObject(tmp->Getrole()));
}

Dc1NodePtr EbuCoreentityType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreentityType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreentityType::GetentityId() const
{
	return m_entityId;
}

bool EbuCoreentityType::ExistentityId() const
{
	return m_entityId_Exist;
}
void EbuCoreentityType::SetentityId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreentityType::SetentityId().");
	}
	m_entityId = item;
	m_entityId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreentityType::InvalidateentityId()
{
	m_entityId_Exist = false;
}
EbuCoreentityType_contactDetails_CollectionPtr EbuCoreentityType::GetcontactDetails() const
{
		return m_contactDetails;
}

EbuCoreentityType_organisationDetails_CollectionPtr EbuCoreentityType::GetorganisationDetails() const
{
		return m_organisationDetails;
}

EbuCoreentityType_role_CollectionPtr EbuCoreentityType::Getrole() const
{
		return m_role;
}

void EbuCoreentityType::SetcontactDetails(const EbuCoreentityType_contactDetails_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreentityType::SetcontactDetails().");
	}
	if (m_contactDetails != item)
	{
		// Dc1Factory::DeleteObject(m_contactDetails);
		m_contactDetails = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_contactDetails) m_contactDetails->SetParent(m_myself.getPointer());
		if(m_contactDetails != EbuCoreentityType_contactDetails_CollectionPtr())
		{
			m_contactDetails->SetContentName(XMLString::transcode("contactDetails"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreentityType::SetorganisationDetails(const EbuCoreentityType_organisationDetails_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreentityType::SetorganisationDetails().");
	}
	if (m_organisationDetails != item)
	{
		// Dc1Factory::DeleteObject(m_organisationDetails);
		m_organisationDetails = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_organisationDetails) m_organisationDetails->SetParent(m_myself.getPointer());
		if(m_organisationDetails != EbuCoreentityType_organisationDetails_CollectionPtr())
		{
			m_organisationDetails->SetContentName(XMLString::transcode("organisationDetails"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreentityType::Setrole(const EbuCoreentityType_role_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreentityType::Setrole().");
	}
	if (m_role != item)
	{
		// Dc1Factory::DeleteObject(m_role);
		m_role = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_role) m_role->SetParent(m_myself.getPointer());
		if(m_role != EbuCoreentityType_role_CollectionPtr())
		{
			m_role->SetContentName(XMLString::transcode("role"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreentityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("entityId")) == 0)
	{
		// entityId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("contactDetails")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:contactDetails is item of type contactDetailsType
		// in element collection entityType_contactDetails_CollectionType
		
		context->Found = true;
		EbuCoreentityType_contactDetails_CollectionPtr coll = GetcontactDetails();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateentityType_contactDetails_CollectionType; // FTT, check this
				SetcontactDetails(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType")))) != empty)
			{
				// Is type allowed
				EbuCorecontactDetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("organisationDetails")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:organisationDetails is item of type organisationDetailsType
		// in element collection entityType_organisationDetails_CollectionType
		
		context->Found = true;
		EbuCoreentityType_organisationDetails_CollectionPtr coll = GetorganisationDetails();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateentityType_organisationDetails_CollectionType; // FTT, check this
				SetorganisationDetails(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType")))) != empty)
			{
				// Is type allowed
				EbuCoreorganisationDetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("role")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:role is item of type entityType_role_LocalType
		// in element collection entityType_role_CollectionType
		
		context->Found = true;
		EbuCoreentityType_role_CollectionPtr coll = Getrole();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateentityType_role_CollectionType; // FTT, check this
				Setrole(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType_role_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityType_role_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for entityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "entityType");
	}
	return result;
}

XMLCh * EbuCoreentityType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreentityType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreentityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("entityType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_entityId_Exist)
	{
	// String
	if(m_entityId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("entityId"), m_entityId);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_contactDetails != EbuCoreentityType_contactDetails_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_contactDetails->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_organisationDetails != EbuCoreentityType_organisationDetails_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_organisationDetails->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_role != EbuCoreentityType_role_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_role->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreentityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("entityId")))
	{
		// Deserialize string type
		this->SetentityId(Dc1Convert::TextToString(parent->getAttribute(X("entityId"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("contactDetails"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("contactDetails")) == 0))
		{
			// Deserialize factory type
			EbuCoreentityType_contactDetails_CollectionPtr tmp = CreateentityType_contactDetails_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetcontactDetails(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("organisationDetails"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("organisationDetails")) == 0))
		{
			// Deserialize factory type
			EbuCoreentityType_organisationDetails_CollectionPtr tmp = CreateentityType_organisationDetails_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetorganisationDetails(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("role"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("role")) == 0))
		{
			// Deserialize factory type
			EbuCoreentityType_role_CollectionPtr tmp = CreateentityType_role_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrole(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreentityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreentityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("contactDetails")) == 0))
  {
	EbuCoreentityType_contactDetails_CollectionPtr tmp = CreateentityType_contactDetails_CollectionType; // FTT, check this
	this->SetcontactDetails(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("organisationDetails")) == 0))
  {
	EbuCoreentityType_organisationDetails_CollectionPtr tmp = CreateentityType_organisationDetails_CollectionType; // FTT, check this
	this->SetorganisationDetails(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("role")) == 0))
  {
	EbuCoreentityType_role_CollectionPtr tmp = CreateentityType_role_CollectionType; // FTT, check this
	this->Setrole(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreentityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetcontactDetails() != Dc1NodePtr())
		result.Insert(GetcontactDetails());
	if (GetorganisationDetails() != Dc1NodePtr())
		result.Insert(GetorganisationDetails());
	if (Getrole() != Dc1NodePtr())
		result.Insert(Getrole());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreentityType_ExtMethodImpl.h


