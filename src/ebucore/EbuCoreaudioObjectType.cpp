
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtImplInclude.h


#include "EbuCoretimecodeType.h"
#include "EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioObjectType_audioObjectIDRef_CollectionType.h"
#include "EbuCoreaudioObjectType_audioTrackUIDRef_CollectionType.h"
#include "EbuCoreaudioObjectType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioObjectType::IEbuCoreaudioObjectType()
{

// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtPropInit.h

}

IEbuCoreaudioObjectType::~IEbuCoreaudioObjectType()
{
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtPropCleanup.h

}

EbuCoreaudioObjectType::EbuCoreaudioObjectType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioObjectType::~EbuCoreaudioObjectType()
{
	Cleanup();
}

void EbuCoreaudioObjectType::Init()
{

	// Init attributes
	m_audioObjectID = NULL; // String
	m_audioObjectID_Exist = false;
	m_audioObjectName = NULL; // String
	m_audioObjectName_Exist = false;
	m_start = EbuCoretimecodePtr(); // Pattern
	m_start_Exist = false;
	m_duration = EbuCoretimecodePtr(); // Pattern
	m_duration_Exist = false;
	m_dialogue = 0; // Value
	m_dialogue_Default = 0; // Default value
	m_dialogue_Exist = false;
	m_importance = 0; // Value
	m_importance_Exist = false;
	m_interact = false; // Value
	m_interact_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioPackFormatIDRef = EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr(); // Collection
	m_audioObjectIDRef = EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr(); // Collection
	m_audioTrackUIDRef = EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMyPropInit.h

}

void EbuCoreaudioObjectType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMyPropCleanup.h


	XMLString::release(&m_audioObjectID); // String
	XMLString::release(&m_audioObjectName); // String
	// Dc1Factory::DeleteObject(m_start); // Pattern
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
	// Dc1Factory::DeleteObject(m_audioObjectIDRef);
	// Dc1Factory::DeleteObject(m_audioTrackUIDRef);
}

void EbuCoreaudioObjectType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioObjectTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioObjectType
	const Dc1Ptr< EbuCoreaudioObjectType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioObjectID); // String
	if (tmp->ExistaudioObjectID())
	{
		this->SetaudioObjectID(XMLString::replicate(tmp->GetaudioObjectID()));
	}
	else
	{
		InvalidateaudioObjectID();
	}
	}
	{
	XMLString::release(&m_audioObjectName); // String
	if (tmp->ExistaudioObjectName())
	{
		this->SetaudioObjectName(XMLString::replicate(tmp->GetaudioObjectName()));
	}
	else
	{
		InvalidateaudioObjectName();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_start); // Pattern
	if (tmp->Existstart())
	{
		this->Setstart(Dc1Factory::CloneObject(tmp->Getstart()));
	}
	else
	{
		Invalidatestart();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	if (tmp->Existduration())
	{
		this->Setduration(Dc1Factory::CloneObject(tmp->Getduration()));
	}
	else
	{
		Invalidateduration();
	}
	}
	{
	if (tmp->Existdialogue())
	{
		this->Setdialogue(tmp->Getdialogue());
	}
	else
	{
		Invalidatedialogue();
	}
	}
	{
	if (tmp->Existimportance())
	{
		this->Setimportance(tmp->Getimportance());
	}
	else
	{
		Invalidateimportance();
	}
	}
	{
	if (tmp->Existinteract())
	{
		this->Setinteract(tmp->Getinteract());
	}
	else
	{
		Invalidateinteract();
	}
	}
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		this->SetaudioPackFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioPackFormatIDRef()));
		// Dc1Factory::DeleteObject(m_audioObjectIDRef);
		this->SetaudioObjectIDRef(Dc1Factory::CloneObject(tmp->GetaudioObjectIDRef()));
		// Dc1Factory::DeleteObject(m_audioTrackUIDRef);
		this->SetaudioTrackUIDRef(Dc1Factory::CloneObject(tmp->GetaudioTrackUIDRef()));
}

Dc1NodePtr EbuCoreaudioObjectType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioObjectType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioObjectType::GetaudioObjectID() const
{
	return m_audioObjectID;
}

bool EbuCoreaudioObjectType::ExistaudioObjectID() const
{
	return m_audioObjectID_Exist;
}
void EbuCoreaudioObjectType::SetaudioObjectID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::SetaudioObjectID().");
	}
	m_audioObjectID = item;
	m_audioObjectID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::InvalidateaudioObjectID()
{
	m_audioObjectID_Exist = false;
}
XMLCh * EbuCoreaudioObjectType::GetaudioObjectName() const
{
	return m_audioObjectName;
}

bool EbuCoreaudioObjectType::ExistaudioObjectName() const
{
	return m_audioObjectName_Exist;
}
void EbuCoreaudioObjectType::SetaudioObjectName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::SetaudioObjectName().");
	}
	m_audioObjectName = item;
	m_audioObjectName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::InvalidateaudioObjectName()
{
	m_audioObjectName_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioObjectType::Getstart() const
{
	return m_start;
}

bool EbuCoreaudioObjectType::Existstart() const
{
	return m_start_Exist;
}
void EbuCoreaudioObjectType::Setstart(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::Setstart().");
	}
	m_start = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_start) m_start->SetParent(m_myself.getPointer());
	m_start_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::Invalidatestart()
{
	m_start_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioObjectType::Getduration() const
{
	return m_duration;
}

bool EbuCoreaudioObjectType::Existduration() const
{
	return m_duration_Exist;
}
void EbuCoreaudioObjectType::Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::Setduration().");
	}
	m_duration = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_duration) m_duration->SetParent(m_myself.getPointer());
	m_duration_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::Invalidateduration()
{
	m_duration_Exist = false;
}
int EbuCoreaudioObjectType::Getdialogue() const
{
	if (this->Existdialogue()) {
		return m_dialogue;
	} else {
		return m_dialogue_Default;
	}
}

bool EbuCoreaudioObjectType::Existdialogue() const
{
	return m_dialogue_Exist;
}
void EbuCoreaudioObjectType::Setdialogue(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::Setdialogue().");
	}
	m_dialogue = item;
	m_dialogue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::Invalidatedialogue()
{
	m_dialogue_Exist = false;
}
int EbuCoreaudioObjectType::Getimportance() const
{
	return m_importance;
}

bool EbuCoreaudioObjectType::Existimportance() const
{
	return m_importance_Exist;
}
void EbuCoreaudioObjectType::Setimportance(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::Setimportance().");
	}
	m_importance = item;
	m_importance_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::Invalidateimportance()
{
	m_importance_Exist = false;
}
bool EbuCoreaudioObjectType::Getinteract() const
{
	return m_interact;
}

bool EbuCoreaudioObjectType::Existinteract() const
{
	return m_interact_Exist;
}
void EbuCoreaudioObjectType::Setinteract(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::Setinteract().");
	}
	m_interact = item;
	m_interact_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::Invalidateinteract()
{
	m_interact_Exist = false;
}
EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr EbuCoreaudioObjectType::GetaudioPackFormatIDRef() const
{
		return m_audioPackFormatIDRef;
}

EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr EbuCoreaudioObjectType::GetaudioObjectIDRef() const
{
		return m_audioObjectIDRef;
}

EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr EbuCoreaudioObjectType::GetaudioTrackUIDRef() const
{
		return m_audioTrackUIDRef;
}

void EbuCoreaudioObjectType::SetaudioPackFormatIDRef(const EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::SetaudioPackFormatIDRef().");
	}
	if (m_audioPackFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		m_audioPackFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioPackFormatIDRef) m_audioPackFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioPackFormatIDRef != EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr())
		{
			m_audioPackFormatIDRef->SetContentName(XMLString::transcode("audioPackFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::SetaudioObjectIDRef(const EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::SetaudioObjectIDRef().");
	}
	if (m_audioObjectIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioObjectIDRef);
		m_audioObjectIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioObjectIDRef) m_audioObjectIDRef->SetParent(m_myself.getPointer());
		if(m_audioObjectIDRef != EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr())
		{
			m_audioObjectIDRef->SetContentName(XMLString::transcode("audioObjectIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioObjectType::SetaudioTrackUIDRef(const EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioObjectType::SetaudioTrackUIDRef().");
	}
	if (m_audioTrackUIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioTrackUIDRef);
		m_audioTrackUIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrackUIDRef) m_audioTrackUIDRef->SetParent(m_myself.getPointer());
		if(m_audioTrackUIDRef != EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr())
		{
			m_audioTrackUIDRef->SetContentName(XMLString::transcode("audioTrackUIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioObjectType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioObjectID")) == 0)
	{
		// audioObjectID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioObjectName")) == 0)
	{
		// audioObjectName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("duration")) == 0)
	{
		// duration is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dialogue")) == 0)
	{
		// dialogue is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("importance")) == 0)
	{
		// importance is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("interact")) == 0)
	{
		// interact is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioPackFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioObjectIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrackUIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioObjectType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioObjectType");
	}
	return result;
}

XMLCh * EbuCoreaudioObjectType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioObjectType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioObjectType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioObjectType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioObjectID_Exist)
	{
	// String
	if(m_audioObjectID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioObjectID"), m_audioObjectID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioObjectName_Exist)
	{
	// String
	if(m_audioObjectName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioObjectName"), m_audioObjectName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_start_Exist)
	{
	// Pattern
	if (m_start != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_start->ToText();
		element->setAttributeNS(X(""), X("start"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_duration_Exist)
	{
	// Pattern
	if (m_duration != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_duration->ToText();
		element->setAttributeNS(X(""), X("duration"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dialogue_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_dialogue);
		element->setAttributeNS(X(""), X("dialogue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_importance_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_importance);
		element->setAttributeNS(X(""), X("importance"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_interact_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_interact);
		element->setAttributeNS(X(""), X("interact"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioPackFormatIDRef != EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioPackFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioObjectIDRef != EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioObjectIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioTrackUIDRef != EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioTrackUIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioObjectType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioObjectID")))
	{
		// Deserialize string type
		this->SetaudioObjectID(Dc1Convert::TextToString(parent->getAttribute(X("audioObjectID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioObjectName")))
	{
		// Deserialize string type
		this->SetaudioObjectName(Dc1Convert::TextToString(parent->getAttribute(X("audioObjectName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("start")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("start")));
		this->Setstart(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("duration")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("duration")));
		this->Setduration(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dialogue")))
	{
		// deserialize value type
		this->Setdialogue(Dc1Convert::TextToInt(parent->getAttribute(X("dialogue"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("importance")))
	{
		// deserialize value type
		this->Setimportance(Dc1Convert::TextToInt(parent->getAttribute(X("importance"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("interact")))
	{
		// deserialize value type
		this->Setinteract(Dc1Convert::TextToBool(parent->getAttribute(X("interact"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioPackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioPackFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioObjectType_audioPackFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioPackFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioObjectIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioObjectIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr tmp = CreateaudioObjectType_audioObjectIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioObjectIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioTrackUIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioTrackUIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr tmp = CreateaudioObjectType_audioTrackUIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioTrackUIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioObjectType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioPackFormatIDRef")) == 0))
  {
	EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioObjectType_audioPackFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioPackFormatIDRef(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioObjectIDRef")) == 0))
  {
	EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr tmp = CreateaudioObjectType_audioObjectIDRef_CollectionType; // FTT, check this
	this->SetaudioObjectIDRef(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioTrackUIDRef")) == 0))
  {
	EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr tmp = CreateaudioObjectType_audioTrackUIDRef_CollectionType; // FTT, check this
	this->SetaudioTrackUIDRef(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioObjectType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioPackFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioPackFormatIDRef());
	if (GetaudioObjectIDRef() != Dc1NodePtr())
		result.Insert(GetaudioObjectIDRef());
	if (GetaudioTrackUIDRef() != Dc1NodePtr())
		result.Insert(GetaudioTrackUIDRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMethodImpl.h


