
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecoefficientType_ExtImplInclude.h


#include "EbuCorecoefficientType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorecoefficientType::IEbuCorecoefficientType()
{

// no includefile for extension defined 
// file EbuCorecoefficientType_ExtPropInit.h

}

IEbuCorecoefficientType::~IEbuCorecoefficientType()
{
// no includefile for extension defined 
// file EbuCorecoefficientType_ExtPropCleanup.h

}

EbuCorecoefficientType::EbuCorecoefficientType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecoefficientType::~EbuCorecoefficientType()
{
	Cleanup();
}

void EbuCorecoefficientType::Init()
{
	// Init base
	m_Base = XMLString::transcode("");

	// Init attributes
	m_gain = 0.0f; // Value
	m_gain_Exist = false;
	m_gainVar = NULL; // String
	m_gainVar_Exist = false;
	m_phase = 0.0f; // Value
	m_phase_Exist = false;
	m_phaseVar = NULL; // String
	m_phaseVar_Exist = false;



// no includefile for extension defined 
// file EbuCorecoefficientType_ExtMyPropInit.h

}

void EbuCorecoefficientType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecoefficientType_ExtMyPropCleanup.h


	XMLString::release(&m_Base);
	XMLString::release(&m_gainVar); // String
	XMLString::release(&m_phaseVar); // String
}

void EbuCorecoefficientType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use coefficientTypePtr, since we
	// might need GetBase(), which isn't defined in IcoefficientType
	const Dc1Ptr< EbuCorecoefficientType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	XMLString::release(&m_Base);
	m_Base = XMLString::replicate(tmp->GetContent());
	{
	if (tmp->Existgain())
	{
		this->Setgain(tmp->Getgain());
	}
	else
	{
		Invalidategain();
	}
	}
	{
	XMLString::release(&m_gainVar); // String
	if (tmp->ExistgainVar())
	{
		this->SetgainVar(XMLString::replicate(tmp->GetgainVar()));
	}
	else
	{
		InvalidategainVar();
	}
	}
	{
	if (tmp->Existphase())
	{
		this->Setphase(tmp->Getphase());
	}
	else
	{
		Invalidatephase();
	}
	}
	{
	XMLString::release(&m_phaseVar); // String
	if (tmp->ExistphaseVar())
	{
		this->SetphaseVar(XMLString::replicate(tmp->GetphaseVar()));
	}
	else
	{
		InvalidatephaseVar();
	}
	}
}

Dc1NodePtr EbuCorecoefficientType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorecoefficientType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void EbuCorecoefficientType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoefficientType::SetContent().");
	}
	if (m_Base != item)
	{
		XMLString::release(&m_Base);
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCorecoefficientType::GetContent() const
{
	return m_Base;
}
float EbuCorecoefficientType::Getgain() const
{
	return m_gain;
}

bool EbuCorecoefficientType::Existgain() const
{
	return m_gain_Exist;
}
void EbuCorecoefficientType::Setgain(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoefficientType::Setgain().");
	}
	m_gain = item;
	m_gain_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoefficientType::Invalidategain()
{
	m_gain_Exist = false;
}
XMLCh * EbuCorecoefficientType::GetgainVar() const
{
	return m_gainVar;
}

bool EbuCorecoefficientType::ExistgainVar() const
{
	return m_gainVar_Exist;
}
void EbuCorecoefficientType::SetgainVar(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoefficientType::SetgainVar().");
	}
	m_gainVar = item;
	m_gainVar_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoefficientType::InvalidategainVar()
{
	m_gainVar_Exist = false;
}
float EbuCorecoefficientType::Getphase() const
{
	return m_phase;
}

bool EbuCorecoefficientType::Existphase() const
{
	return m_phase_Exist;
}
void EbuCorecoefficientType::Setphase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoefficientType::Setphase().");
	}
	m_phase = item;
	m_phase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoefficientType::Invalidatephase()
{
	m_phase_Exist = false;
}
XMLCh * EbuCorecoefficientType::GetphaseVar() const
{
	return m_phaseVar;
}

bool EbuCorecoefficientType::ExistphaseVar() const
{
	return m_phaseVar_Exist;
}
void EbuCorecoefficientType::SetphaseVar(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoefficientType::SetphaseVar().");
	}
	m_phaseVar = item;
	m_phaseVar_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoefficientType::InvalidatephaseVar()
{
	m_phaseVar_Exist = false;
}

Dc1NodeEnum EbuCorecoefficientType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("gain")) == 0)
	{
		// gain is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("gainVar")) == 0)
	{
		// gainVar is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phase")) == 0)
	{
		// phase is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phaseVar")) == 0)
	{
		// phaseVar is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for coefficientType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "coefficientType");
	}
	return result;
}

XMLCh * EbuCorecoefficientType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorecoefficientType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCorecoefficientType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// EbuCorecoefficientType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* EbuCorecoefficientType::ContentToString() const
{
	// EbuCorecoefficientType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void EbuCorecoefficientType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	element->appendChild(doc->createTextNode(m_Base));

	assert(element);
// no includefile for extension defined 
// file EbuCorecoefficientType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("coefficientType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_gain_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_gain);
		element->setAttributeNS(X(""), X("gain"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_gainVar_Exist)
	{
	// String
	if(m_gainVar != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("gainVar"), m_gainVar);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phase_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_phase);
		element->setAttributeNS(X(""), X("phase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phaseVar_Exist)
	{
	// String
	if(m_phaseVar != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("phaseVar"), m_phaseVar);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCorecoefficientType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("gain")))
	{
		// deserialize value type
		this->Setgain(Dc1Convert::TextToFloat(parent->getAttribute(X("gain"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("gainVar")))
	{
		// Deserialize string type
		this->SetgainVar(Dc1Convert::TextToString(parent->getAttribute(X("gainVar"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phase")))
	{
		// deserialize value type
		this->Setphase(Dc1Convert::TextToFloat(parent->getAttribute(X("phase"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phaseVar")))
	{
		// Deserialize string type
		this->SetphaseVar(Dc1Convert::TextToString(parent->getAttribute(X("phaseVar"))));
		* current = parent;
	}

	XMLCh * tmp = Dc1Convert::TextToString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		XMLString::release(&m_Base);
		m_Base = tmp;
		found = true;
	}
// no includefile for extension defined 
// file EbuCorecoefficientType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecoefficientType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCorecoefficientType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecoefficientType_ExtMethodImpl.h


