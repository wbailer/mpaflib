
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCorecoreMetadataType.h"
#include "EbuCoreentityType.h"
#include "EbuCoreebuCoreMainType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreebuCoreMainType::IEbuCoreebuCoreMainType()
{

// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtPropInit.h

}

IEbuCoreebuCoreMainType::~IEbuCoreebuCoreMainType()
{
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtPropCleanup.h

}

EbuCoreebuCoreMainType::EbuCoreebuCoreMainType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreebuCoreMainType::~EbuCoreebuCoreMainType()
{
	Cleanup();
}

void EbuCoreebuCoreMainType::Init()
{

	// Init attributes
	m_schema = NULL; // String
	m_schema_Exist = false;
	m_version = NULL; // String
	m_version_Default = XMLString::transcode("1.5"); // Default value
	m_version_Exist = false;
	m_dateLastModified = W3CdatePtr(); // Pattern
	m_dateLastModified_Exist = false;
	m_timeLastModified = W3CtimePtr(); // Pattern
	m_timeLastModified_Exist = false;
	m_documentId = NULL; // String
	m_documentId_Exist = false;
	m_documentLocation = NULL; // String
	m_documentLocation_Exist = false;
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_coreMetadata = EbuCorecoreMetadataPtr(); // Class
	m_metadataProvider = EbuCoreentityPtr(); // Class
	m_metadataProvider_Exist = false;


// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMyPropInit.h

}

void EbuCoreebuCoreMainType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMyPropCleanup.h


	XMLString::release(&m_schema); // String
	XMLString::release(&m_version); // String
	XMLString::release(&m_version_Default); // Default value
	// Dc1Factory::DeleteObject(m_dateLastModified); // Pattern
	// Dc1Factory::DeleteObject(m_timeLastModified); // Pattern
	XMLString::release(&m_documentId); // String
	XMLString::release(&m_documentLocation); // String
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_coreMetadata);
	// Dc1Factory::DeleteObject(m_metadataProvider);
}

void EbuCoreebuCoreMainType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ebuCoreMainTypePtr, since we
	// might need GetBase(), which isn't defined in IebuCoreMainType
	const Dc1Ptr< EbuCoreebuCoreMainType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_schema); // String
	if (tmp->Existschema())
	{
		this->Setschema(XMLString::replicate(tmp->Getschema()));
	}
	else
	{
		Invalidateschema();
	}
	}
	{
	XMLString::release(&m_version); // String
	XMLString::release(&m_version_Default); // Default value
	if (tmp->Existversion())
	{
		this->Setversion(XMLString::replicate(tmp->Getversion()));
	}
	else
	{
		Invalidateversion();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_dateLastModified); // Pattern
	if (tmp->ExistdateLastModified())
	{
		this->SetdateLastModified(Dc1Factory::CloneObject(tmp->GetdateLastModified()));
	}
	else
	{
		InvalidatedateLastModified();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeLastModified); // Pattern
	if (tmp->ExisttimeLastModified())
	{
		this->SettimeLastModified(Dc1Factory::CloneObject(tmp->GettimeLastModified()));
	}
	else
	{
		InvalidatetimeLastModified();
	}
	}
	{
	XMLString::release(&m_documentId); // String
	if (tmp->ExistdocumentId())
	{
		this->SetdocumentId(XMLString::replicate(tmp->GetdocumentId()));
	}
	else
	{
		InvalidatedocumentId();
	}
	}
	{
	XMLString::release(&m_documentLocation); // String
	if (tmp->ExistdocumentLocation())
	{
		this->SetdocumentLocation(XMLString::replicate(tmp->GetdocumentLocation()));
	}
	else
	{
		InvalidatedocumentLocation();
	}
	}
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_coreMetadata);
		this->SetcoreMetadata(Dc1Factory::CloneObject(tmp->GetcoreMetadata()));
	if (tmp->IsValidmetadataProvider())
	{
		// Dc1Factory::DeleteObject(m_metadataProvider);
		this->SetmetadataProvider(Dc1Factory::CloneObject(tmp->GetmetadataProvider()));
	}
	else
	{
		InvalidatemetadataProvider();
	}
}

Dc1NodePtr EbuCoreebuCoreMainType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreebuCoreMainType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreebuCoreMainType::Getschema() const
{
	return m_schema;
}

bool EbuCoreebuCoreMainType::Existschema() const
{
	return m_schema_Exist;
}
void EbuCoreebuCoreMainType::Setschema(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::Setschema().");
	}
	m_schema = item;
	m_schema_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::Invalidateschema()
{
	m_schema_Exist = false;
}
XMLCh * EbuCoreebuCoreMainType::Getversion() const
{
	if (this->Existversion()) {
		return m_version;
	} else {
		return m_version_Default;
	}
}

bool EbuCoreebuCoreMainType::Existversion() const
{
	return m_version_Exist;
}
void EbuCoreebuCoreMainType::Setversion(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::Setversion().");
	}
	m_version = item;
	m_version_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::Invalidateversion()
{
	m_version_Exist = false;
}
W3CdatePtr EbuCoreebuCoreMainType::GetdateLastModified() const
{
	return m_dateLastModified;
}

bool EbuCoreebuCoreMainType::ExistdateLastModified() const
{
	return m_dateLastModified_Exist;
}
void EbuCoreebuCoreMainType::SetdateLastModified(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SetdateLastModified().");
	}
	m_dateLastModified = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dateLastModified) m_dateLastModified->SetParent(m_myself.getPointer());
	m_dateLastModified_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::InvalidatedateLastModified()
{
	m_dateLastModified_Exist = false;
}
W3CtimePtr EbuCoreebuCoreMainType::GettimeLastModified() const
{
	return m_timeLastModified;
}

bool EbuCoreebuCoreMainType::ExisttimeLastModified() const
{
	return m_timeLastModified_Exist;
}
void EbuCoreebuCoreMainType::SettimeLastModified(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SettimeLastModified().");
	}
	m_timeLastModified = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeLastModified) m_timeLastModified->SetParent(m_myself.getPointer());
	m_timeLastModified_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::InvalidatetimeLastModified()
{
	m_timeLastModified_Exist = false;
}
XMLCh * EbuCoreebuCoreMainType::GetdocumentId() const
{
	return m_documentId;
}

bool EbuCoreebuCoreMainType::ExistdocumentId() const
{
	return m_documentId_Exist;
}
void EbuCoreebuCoreMainType::SetdocumentId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SetdocumentId().");
	}
	m_documentId = item;
	m_documentId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::InvalidatedocumentId()
{
	m_documentId_Exist = false;
}
XMLCh * EbuCoreebuCoreMainType::GetdocumentLocation() const
{
	return m_documentLocation;
}

bool EbuCoreebuCoreMainType::ExistdocumentLocation() const
{
	return m_documentLocation_Exist;
}
void EbuCoreebuCoreMainType::SetdocumentLocation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SetdocumentLocation().");
	}
	m_documentLocation = item;
	m_documentLocation_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::InvalidatedocumentLocation()
{
	m_documentLocation_Exist = false;
}
XMLCh * EbuCoreebuCoreMainType::Getlang() const
{
	return m_lang;
}

bool EbuCoreebuCoreMainType::Existlang() const
{
	return m_lang_Exist;
}
void EbuCoreebuCoreMainType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::Invalidatelang()
{
	m_lang_Exist = false;
}
EbuCorecoreMetadataPtr EbuCoreebuCoreMainType::GetcoreMetadata() const
{
		return m_coreMetadata;
}

EbuCoreentityPtr EbuCoreebuCoreMainType::GetmetadataProvider() const
{
		return m_metadataProvider;
}

// Element is optional
bool EbuCoreebuCoreMainType::IsValidmetadataProvider() const
{
	return m_metadataProvider_Exist;
}

void EbuCoreebuCoreMainType::SetcoreMetadata(const EbuCorecoreMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SetcoreMetadata().");
	}
	if (m_coreMetadata != item)
	{
		// Dc1Factory::DeleteObject(m_coreMetadata);
		m_coreMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_coreMetadata) m_coreMetadata->SetParent(m_myself.getPointer());
		if (m_coreMetadata && m_coreMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_coreMetadata->UseTypeAttribute = true;
		}
		if(m_coreMetadata != EbuCorecoreMetadataPtr())
		{
			m_coreMetadata->SetContentName(XMLString::transcode("coreMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::SetmetadataProvider(const EbuCoreentityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreebuCoreMainType::SetmetadataProvider().");
	}
	if (m_metadataProvider != item || m_metadataProvider_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_metadataProvider);
		m_metadataProvider = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_metadataProvider) m_metadataProvider->SetParent(m_myself.getPointer());
		if (m_metadataProvider && m_metadataProvider->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_metadataProvider->UseTypeAttribute = true;
		}
		m_metadataProvider_Exist = true;
		if(m_metadataProvider != EbuCoreentityPtr())
		{
			m_metadataProvider->SetContentName(XMLString::transcode("metadataProvider"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreebuCoreMainType::InvalidatemetadataProvider()
{
	m_metadataProvider_Exist = false;
}

Dc1NodeEnum EbuCoreebuCoreMainType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("schema")) == 0)
	{
		// schema is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("version")) == 0)
	{
		// version is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dateLastModified")) == 0)
	{
		// dateLastModified is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeLastModified")) == 0)
	{
		// timeLastModified is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("documentId")) == 0)
	{
		// documentId is simple attribute NMTOKEN
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("documentLocation")) == 0)
	{
		// documentLocation is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("coreMetadata")) == 0)
	{
		// coreMetadata is simple element coreMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetcoreMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType")))) != empty)
			{
				// Is type allowed
				EbuCorecoreMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetcoreMetadata(p, client);
					if((p = GetcoreMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("metadataProvider")) == 0)
	{
		// metadataProvider is simple element entityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetmetadataProvider()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetmetadataProvider(p, client);
					if((p = GetmetadataProvider()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ebuCoreMainType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ebuCoreMainType");
	}
	return result;
}

XMLCh * EbuCoreebuCoreMainType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreebuCoreMainType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreebuCoreMainType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("ebuCoreMainType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_schema_Exist)
	{
	// String
	if(m_schema != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("schema"), m_schema);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_version_Exist)
	{
	// String
	if(m_version != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("version"), m_version);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dateLastModified_Exist)
	{
	// Pattern
	if (m_dateLastModified != W3CdatePtr())
	{
		XMLCh * tmp = m_dateLastModified->ToText();
		element->setAttributeNS(X(""), X("dateLastModified"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeLastModified_Exist)
	{
	// Pattern
	if (m_timeLastModified != W3CtimePtr())
	{
		XMLCh * tmp = m_timeLastModified->ToText();
		element->setAttributeNS(X(""), X("timeLastModified"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_documentId_Exist)
	{
	// String
	if(m_documentId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("documentId"), m_documentId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_documentLocation_Exist)
	{
	// String
	if(m_documentLocation != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("documentLocation"), m_documentLocation);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_coreMetadata != EbuCorecoreMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_coreMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("coreMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_coreMetadata->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_metadataProvider != EbuCoreentityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_metadataProvider->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("metadataProvider"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_metadataProvider->Serialize(doc, element, &elem);
	}

}

bool EbuCoreebuCoreMainType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("schema")))
	{
		// Deserialize string type
		this->Setschema(Dc1Convert::TextToString(parent->getAttribute(X("schema"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("version")))
	{
		// Deserialize string type
		this->Setversion(Dc1Convert::TextToString(parent->getAttribute(X("version"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dateLastModified")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("dateLastModified")));
		this->SetdateLastModified(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeLastModified")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeLastModified")));
		this->SettimeLastModified(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("documentId")))
	{
		// Deserialize string type
		this->SetdocumentId(Dc1Convert::TextToString(parent->getAttribute(X("documentId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("documentLocation")))
	{
		// Deserialize string type
		this->SetdocumentLocation(Dc1Convert::TextToString(parent->getAttribute(X("documentLocation"))));
		* current = parent;
	}

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("coreMetadata"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("coreMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecoreMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetcoreMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("metadataProvider"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("metadataProvider")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateentityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetmetadataProvider(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreebuCoreMainType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("coreMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecoreMetadataType; // FTT, check this
	}
	this->SetcoreMetadata(child);
  }
  if (XMLString::compareString(elementname, X("metadataProvider")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateentityType; // FTT, check this
	}
	this->SetmetadataProvider(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreebuCoreMainType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetcoreMetadata() != Dc1NodePtr())
		result.Insert(GetcoreMetadata());
	if (GetmetadataProvider() != Dc1NodePtr())
		result.Insert(GetmetadataProvider());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMethodImpl.h


