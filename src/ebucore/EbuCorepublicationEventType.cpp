
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCorepublicationServiceType.h"
#include "EbuCorepublicationMediumType.h"
#include "EbuCorepublicationChannelType.h"
#include "EbuCorepublicationEventType_publicationRegion_CollectionType.h"
#include "EbuCorepublicationEventType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreregionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:publicationRegion

#include <assert.h>
IEbuCorepublicationEventType::IEbuCorepublicationEventType()
{

// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtPropInit.h

}

IEbuCorepublicationEventType::~IEbuCorepublicationEventType()
{
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtPropCleanup.h

}

EbuCorepublicationEventType::EbuCorepublicationEventType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorepublicationEventType::~EbuCorepublicationEventType()
{
	Cleanup();
}

void EbuCorepublicationEventType::Init()
{

	// Init attributes
	m_publicationEventId = NULL; // String
	m_publicationEventId_Exist = false;
	m_publicationEventName = NULL; // String
	m_publicationEventName_Exist = false;
	m_firstShowing = false; // Value
	m_firstShowing_Exist = false;
	m_lastShowing = false; // Value
	m_lastShowing_Exist = false;
	m_live = false; // Value
	m_live_Exist = false;
	m_free = false; // Value
	m_free_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;
	m_formatIdRef = NULL; // String
	m_formatIdRef_Exist = false;
	m_rightsIDRefs = NULL; // String
	m_rightsIDRefs_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_publicationDate = W3CdatePtr(); // Pattern
	m_publicationDate_Exist = false;
	m_publicationTime = W3CtimePtr(); // Pattern
	m_publicationTime_Exist = false;
	m_scheduleDate = W3CdatePtr(); // Pattern
	m_scheduleDate_Exist = false;
	m_publicationService = EbuCorepublicationServicePtr(); // Class
	m_publicationService_Exist = false;
	m_publicationMedium = EbuCorepublicationMediumPtr(); // Class
	m_publicationMedium_Exist = false;
	m_publicationChannel = EbuCorepublicationChannelPtr(); // Class
	m_publicationChannel_Exist = false;
	m_publicationRegion = EbuCorepublicationEventType_publicationRegion_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMyPropInit.h

}

void EbuCorepublicationEventType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMyPropCleanup.h


	XMLString::release(&m_publicationEventId); // String
	XMLString::release(&m_publicationEventName); // String
	XMLString::release(&m_note); // String
	XMLString::release(&m_formatIdRef); // String
	XMLString::release(&m_rightsIDRefs); // String
	// Dc1Factory::DeleteObject(m_publicationDate);
	// Dc1Factory::DeleteObject(m_publicationTime);
	// Dc1Factory::DeleteObject(m_scheduleDate);
	// Dc1Factory::DeleteObject(m_publicationService);
	// Dc1Factory::DeleteObject(m_publicationMedium);
	// Dc1Factory::DeleteObject(m_publicationChannel);
	// Dc1Factory::DeleteObject(m_publicationRegion);
}

void EbuCorepublicationEventType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use publicationEventTypePtr, since we
	// might need GetBase(), which isn't defined in IpublicationEventType
	const Dc1Ptr< EbuCorepublicationEventType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_publicationEventId); // String
	if (tmp->ExistpublicationEventId())
	{
		this->SetpublicationEventId(XMLString::replicate(tmp->GetpublicationEventId()));
	}
	else
	{
		InvalidatepublicationEventId();
	}
	}
	{
	XMLString::release(&m_publicationEventName); // String
	if (tmp->ExistpublicationEventName())
	{
		this->SetpublicationEventName(XMLString::replicate(tmp->GetpublicationEventName()));
	}
	else
	{
		InvalidatepublicationEventName();
	}
	}
	{
	if (tmp->ExistfirstShowing())
	{
		this->SetfirstShowing(tmp->GetfirstShowing());
	}
	else
	{
		InvalidatefirstShowing();
	}
	}
	{
	if (tmp->ExistlastShowing())
	{
		this->SetlastShowing(tmp->GetlastShowing());
	}
	else
	{
		InvalidatelastShowing();
	}
	}
	{
	if (tmp->Existlive())
	{
		this->Setlive(tmp->Getlive());
	}
	else
	{
		Invalidatelive();
	}
	}
	{
	if (tmp->Existfree())
	{
		this->Setfree(tmp->Getfree());
	}
	else
	{
		Invalidatefree();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
	{
	XMLString::release(&m_formatIdRef); // String
	if (tmp->ExistformatIdRef())
	{
		this->SetformatIdRef(XMLString::replicate(tmp->GetformatIdRef()));
	}
	else
	{
		InvalidateformatIdRef();
	}
	}
	{
	XMLString::release(&m_rightsIDRefs); // String
	if (tmp->ExistrightsIDRefs())
	{
		this->SetrightsIDRefs(XMLString::replicate(tmp->GetrightsIDRefs()));
	}
	else
	{
		InvalidaterightsIDRefs();
	}
	}
	if (tmp->IsValidpublicationDate())
	{
		// Dc1Factory::DeleteObject(m_publicationDate);
		this->SetpublicationDate(Dc1Factory::CloneObject(tmp->GetpublicationDate()));
	}
	else
	{
		InvalidatepublicationDate();
	}
	if (tmp->IsValidpublicationTime())
	{
		// Dc1Factory::DeleteObject(m_publicationTime);
		this->SetpublicationTime(Dc1Factory::CloneObject(tmp->GetpublicationTime()));
	}
	else
	{
		InvalidatepublicationTime();
	}
	if (tmp->IsValidscheduleDate())
	{
		// Dc1Factory::DeleteObject(m_scheduleDate);
		this->SetscheduleDate(Dc1Factory::CloneObject(tmp->GetscheduleDate()));
	}
	else
	{
		InvalidatescheduleDate();
	}
	if (tmp->IsValidpublicationService())
	{
		// Dc1Factory::DeleteObject(m_publicationService);
		this->SetpublicationService(Dc1Factory::CloneObject(tmp->GetpublicationService()));
	}
	else
	{
		InvalidatepublicationService();
	}
	if (tmp->IsValidpublicationMedium())
	{
		// Dc1Factory::DeleteObject(m_publicationMedium);
		this->SetpublicationMedium(Dc1Factory::CloneObject(tmp->GetpublicationMedium()));
	}
	else
	{
		InvalidatepublicationMedium();
	}
	if (tmp->IsValidpublicationChannel())
	{
		// Dc1Factory::DeleteObject(m_publicationChannel);
		this->SetpublicationChannel(Dc1Factory::CloneObject(tmp->GetpublicationChannel()));
	}
	else
	{
		InvalidatepublicationChannel();
	}
		// Dc1Factory::DeleteObject(m_publicationRegion);
		this->SetpublicationRegion(Dc1Factory::CloneObject(tmp->GetpublicationRegion()));
}

Dc1NodePtr EbuCorepublicationEventType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorepublicationEventType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorepublicationEventType::GetpublicationEventId() const
{
	return m_publicationEventId;
}

bool EbuCorepublicationEventType::ExistpublicationEventId() const
{
	return m_publicationEventId_Exist;
}
void EbuCorepublicationEventType::SetpublicationEventId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationEventId().");
	}
	m_publicationEventId = item;
	m_publicationEventId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationEventId()
{
	m_publicationEventId_Exist = false;
}
XMLCh * EbuCorepublicationEventType::GetpublicationEventName() const
{
	return m_publicationEventName;
}

bool EbuCorepublicationEventType::ExistpublicationEventName() const
{
	return m_publicationEventName_Exist;
}
void EbuCorepublicationEventType::SetpublicationEventName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationEventName().");
	}
	m_publicationEventName = item;
	m_publicationEventName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationEventName()
{
	m_publicationEventName_Exist = false;
}
bool EbuCorepublicationEventType::GetfirstShowing() const
{
	return m_firstShowing;
}

bool EbuCorepublicationEventType::ExistfirstShowing() const
{
	return m_firstShowing_Exist;
}
void EbuCorepublicationEventType::SetfirstShowing(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetfirstShowing().");
	}
	m_firstShowing = item;
	m_firstShowing_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatefirstShowing()
{
	m_firstShowing_Exist = false;
}
bool EbuCorepublicationEventType::GetlastShowing() const
{
	return m_lastShowing;
}

bool EbuCorepublicationEventType::ExistlastShowing() const
{
	return m_lastShowing_Exist;
}
void EbuCorepublicationEventType::SetlastShowing(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetlastShowing().");
	}
	m_lastShowing = item;
	m_lastShowing_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatelastShowing()
{
	m_lastShowing_Exist = false;
}
bool EbuCorepublicationEventType::Getlive() const
{
	return m_live;
}

bool EbuCorepublicationEventType::Existlive() const
{
	return m_live_Exist;
}
void EbuCorepublicationEventType::Setlive(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::Setlive().");
	}
	m_live = item;
	m_live_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::Invalidatelive()
{
	m_live_Exist = false;
}
bool EbuCorepublicationEventType::Getfree() const
{
	return m_free;
}

bool EbuCorepublicationEventType::Existfree() const
{
	return m_free_Exist;
}
void EbuCorepublicationEventType::Setfree(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::Setfree().");
	}
	m_free = item;
	m_free_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::Invalidatefree()
{
	m_free_Exist = false;
}
XMLCh * EbuCorepublicationEventType::Getnote() const
{
	return m_note;
}

bool EbuCorepublicationEventType::Existnote() const
{
	return m_note_Exist;
}
void EbuCorepublicationEventType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::Invalidatenote()
{
	m_note_Exist = false;
}
XMLCh * EbuCorepublicationEventType::GetformatIdRef() const
{
	return m_formatIdRef;
}

bool EbuCorepublicationEventType::ExistformatIdRef() const
{
	return m_formatIdRef_Exist;
}
void EbuCorepublicationEventType::SetformatIdRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetformatIdRef().");
	}
	m_formatIdRef = item;
	m_formatIdRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidateformatIdRef()
{
	m_formatIdRef_Exist = false;
}
XMLCh * EbuCorepublicationEventType::GetrightsIDRefs() const
{
	return m_rightsIDRefs;
}

bool EbuCorepublicationEventType::ExistrightsIDRefs() const
{
	return m_rightsIDRefs_Exist;
}
void EbuCorepublicationEventType::SetrightsIDRefs(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetrightsIDRefs().");
	}
	m_rightsIDRefs = item;
	m_rightsIDRefs_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidaterightsIDRefs()
{
	m_rightsIDRefs_Exist = false;
}
W3CdatePtr EbuCorepublicationEventType::GetpublicationDate() const
{
		return m_publicationDate;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidpublicationDate() const
{
	return m_publicationDate_Exist;
}

W3CtimePtr EbuCorepublicationEventType::GetpublicationTime() const
{
		return m_publicationTime;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidpublicationTime() const
{
	return m_publicationTime_Exist;
}

W3CdatePtr EbuCorepublicationEventType::GetscheduleDate() const
{
		return m_scheduleDate;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidscheduleDate() const
{
	return m_scheduleDate_Exist;
}

EbuCorepublicationServicePtr EbuCorepublicationEventType::GetpublicationService() const
{
		return m_publicationService;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidpublicationService() const
{
	return m_publicationService_Exist;
}

EbuCorepublicationMediumPtr EbuCorepublicationEventType::GetpublicationMedium() const
{
		return m_publicationMedium;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidpublicationMedium() const
{
	return m_publicationMedium_Exist;
}

EbuCorepublicationChannelPtr EbuCorepublicationEventType::GetpublicationChannel() const
{
		return m_publicationChannel;
}

// Element is optional
bool EbuCorepublicationEventType::IsValidpublicationChannel() const
{
	return m_publicationChannel_Exist;
}

EbuCorepublicationEventType_publicationRegion_CollectionPtr EbuCorepublicationEventType::GetpublicationRegion() const
{
		return m_publicationRegion;
}

void EbuCorepublicationEventType::SetpublicationDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationDate().");
	}
	if (m_publicationDate != item || m_publicationDate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationDate);
		m_publicationDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationDate) m_publicationDate->SetParent(m_myself.getPointer());
		if (m_publicationDate && m_publicationDate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationDate->UseTypeAttribute = true;
		}
		m_publicationDate_Exist = true;
		if(m_publicationDate != W3CdatePtr())
		{
			m_publicationDate->SetContentName(XMLString::transcode("publicationDate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationDate()
{
	m_publicationDate_Exist = false;
}
void EbuCorepublicationEventType::SetpublicationTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationTime().");
	}
	if (m_publicationTime != item || m_publicationTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationTime);
		m_publicationTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationTime) m_publicationTime->SetParent(m_myself.getPointer());
		if (m_publicationTime && m_publicationTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationTime->UseTypeAttribute = true;
		}
		m_publicationTime_Exist = true;
		if(m_publicationTime != W3CtimePtr())
		{
			m_publicationTime->SetContentName(XMLString::transcode("publicationTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationTime()
{
	m_publicationTime_Exist = false;
}
void EbuCorepublicationEventType::SetscheduleDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetscheduleDate().");
	}
	if (m_scheduleDate != item || m_scheduleDate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_scheduleDate);
		m_scheduleDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_scheduleDate) m_scheduleDate->SetParent(m_myself.getPointer());
		if (m_scheduleDate && m_scheduleDate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_scheduleDate->UseTypeAttribute = true;
		}
		m_scheduleDate_Exist = true;
		if(m_scheduleDate != W3CdatePtr())
		{
			m_scheduleDate->SetContentName(XMLString::transcode("scheduleDate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatescheduleDate()
{
	m_scheduleDate_Exist = false;
}
void EbuCorepublicationEventType::SetpublicationService(const EbuCorepublicationServicePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationService().");
	}
	if (m_publicationService != item || m_publicationService_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationService);
		m_publicationService = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationService) m_publicationService->SetParent(m_myself.getPointer());
		if (m_publicationService && m_publicationService->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationServiceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationService->UseTypeAttribute = true;
		}
		m_publicationService_Exist = true;
		if(m_publicationService != EbuCorepublicationServicePtr())
		{
			m_publicationService->SetContentName(XMLString::transcode("publicationService"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationService()
{
	m_publicationService_Exist = false;
}
void EbuCorepublicationEventType::SetpublicationMedium(const EbuCorepublicationMediumPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationMedium().");
	}
	if (m_publicationMedium != item || m_publicationMedium_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationMedium);
		m_publicationMedium = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationMedium) m_publicationMedium->SetParent(m_myself.getPointer());
		if (m_publicationMedium && m_publicationMedium->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationMediumType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationMedium->UseTypeAttribute = true;
		}
		m_publicationMedium_Exist = true;
		if(m_publicationMedium != EbuCorepublicationMediumPtr())
		{
			m_publicationMedium->SetContentName(XMLString::transcode("publicationMedium"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationMedium()
{
	m_publicationMedium_Exist = false;
}
void EbuCorepublicationEventType::SetpublicationChannel(const EbuCorepublicationChannelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationChannel().");
	}
	if (m_publicationChannel != item || m_publicationChannel_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationChannel);
		m_publicationChannel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationChannel) m_publicationChannel->SetParent(m_myself.getPointer());
		if (m_publicationChannel && m_publicationChannel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationChannelType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationChannel->UseTypeAttribute = true;
		}
		m_publicationChannel_Exist = true;
		if(m_publicationChannel != EbuCorepublicationChannelPtr())
		{
			m_publicationChannel->SetContentName(XMLString::transcode("publicationChannel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationEventType::InvalidatepublicationChannel()
{
	m_publicationChannel_Exist = false;
}
void EbuCorepublicationEventType::SetpublicationRegion(const EbuCorepublicationEventType_publicationRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationEventType::SetpublicationRegion().");
	}
	if (m_publicationRegion != item)
	{
		// Dc1Factory::DeleteObject(m_publicationRegion);
		m_publicationRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationRegion) m_publicationRegion->SetParent(m_myself.getPointer());
		if(m_publicationRegion != EbuCorepublicationEventType_publicationRegion_CollectionPtr())
		{
			m_publicationRegion->SetContentName(XMLString::transcode("publicationRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorepublicationEventType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  9, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("publicationEventId")) == 0)
	{
		// publicationEventId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("publicationEventName")) == 0)
	{
		// publicationEventName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("firstShowing")) == 0)
	{
		// firstShowing is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lastShowing")) == 0)
	{
		// lastShowing is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("live")) == 0)
	{
		// live is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("free")) == 0)
	{
		// free is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatIdRef")) == 0)
	{
		// formatIdRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("rightsIDRefs")) == 0)
	{
		// rightsIDRefs is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationDate")) == 0)
	{
		// publicationDate is simple element date
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))) != empty)
			{
				// Is type allowed
				W3CdatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationDate(p, client);
					if((p = GetpublicationDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationTime")) == 0)
	{
		// publicationTime is simple element time
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time")))) != empty)
			{
				// Is type allowed
				W3CtimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationTime(p, client);
					if((p = GetpublicationTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("scheduleDate")) == 0)
	{
		// scheduleDate is simple element date
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetscheduleDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))) != empty)
			{
				// Is type allowed
				W3CdatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetscheduleDate(p, client);
					if((p = GetscheduleDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationService")) == 0)
	{
		// publicationService is simple element publicationServiceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationService()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationServiceType")))) != empty)
			{
				// Is type allowed
				EbuCorepublicationServicePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationService(p, client);
					if((p = GetpublicationService()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationMedium")) == 0)
	{
		// publicationMedium is simple element publicationMediumType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationMedium()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationMediumType")))) != empty)
			{
				// Is type allowed
				EbuCorepublicationMediumPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationMedium(p, client);
					if((p = GetpublicationMedium()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationChannel")) == 0)
	{
		// publicationChannel is simple element publicationChannelType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationChannel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationChannelType")))) != empty)
			{
				// Is type allowed
				EbuCorepublicationChannelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationChannel(p, client);
					if((p = GetpublicationChannel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationRegion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:publicationRegion is item of type regionType
		// in element collection publicationEventType_publicationRegion_CollectionType
		
		context->Found = true;
		EbuCorepublicationEventType_publicationRegion_CollectionPtr coll = GetpublicationRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatepublicationEventType_publicationRegion_CollectionType; // FTT, check this
				SetpublicationRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))) != empty)
			{
				// Is type allowed
				EbuCoreregionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for publicationEventType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "publicationEventType");
	}
	return result;
}

XMLCh * EbuCorepublicationEventType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorepublicationEventType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorepublicationEventType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationEventType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_publicationEventId_Exist)
	{
	// String
	if(m_publicationEventId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("publicationEventId"), m_publicationEventId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_publicationEventName_Exist)
	{
	// String
	if(m_publicationEventName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("publicationEventName"), m_publicationEventName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_firstShowing_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_firstShowing);
		element->setAttributeNS(X(""), X("firstShowing"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lastShowing_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_lastShowing);
		element->setAttributeNS(X(""), X("lastShowing"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_live_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_live);
		element->setAttributeNS(X(""), X("live"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_free_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_free);
		element->setAttributeNS(X(""), X("free"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatIdRef_Exist)
	{
	// String
	if(m_formatIdRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatIdRef"), m_formatIdRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_rightsIDRefs_Exist)
	{
	// String
	if(m_rightsIDRefs != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("rightsIDRefs"), m_rightsIDRefs);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_publicationDate != W3CdatePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("publicationDate");
//		m_publicationDate->SetContentName(contentname);
		m_publicationDate->UseTypeAttribute = this->UseTypeAttribute;
		m_publicationDate->Serialize(doc, element);
	}
	if(m_publicationTime != W3CtimePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("publicationTime");
//		m_publicationTime->SetContentName(contentname);
		m_publicationTime->UseTypeAttribute = this->UseTypeAttribute;
		m_publicationTime->Serialize(doc, element);
	}
	if(m_scheduleDate != W3CdatePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("scheduleDate");
//		m_scheduleDate->SetContentName(contentname);
		m_scheduleDate->UseTypeAttribute = this->UseTypeAttribute;
		m_scheduleDate->Serialize(doc, element);
	}
	// Class
	
	if (m_publicationService != EbuCorepublicationServicePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_publicationService->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationService"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_publicationService->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_publicationMedium != EbuCorepublicationMediumPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_publicationMedium->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationMedium"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_publicationMedium->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_publicationChannel != EbuCorepublicationChannelPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_publicationChannel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationChannel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_publicationChannel->Serialize(doc, element, &elem);
	}
	if (m_publicationRegion != EbuCorepublicationEventType_publicationRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_publicationRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorepublicationEventType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("publicationEventId")))
	{
		// Deserialize string type
		this->SetpublicationEventId(Dc1Convert::TextToString(parent->getAttribute(X("publicationEventId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("publicationEventName")))
	{
		// Deserialize string type
		this->SetpublicationEventName(Dc1Convert::TextToString(parent->getAttribute(X("publicationEventName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("firstShowing")))
	{
		// deserialize value type
		this->SetfirstShowing(Dc1Convert::TextToBool(parent->getAttribute(X("firstShowing"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("lastShowing")))
	{
		// deserialize value type
		this->SetlastShowing(Dc1Convert::TextToBool(parent->getAttribute(X("lastShowing"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("live")))
	{
		// deserialize value type
		this->Setlive(Dc1Convert::TextToBool(parent->getAttribute(X("live"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("free")))
	{
		// deserialize value type
		this->Setfree(Dc1Convert::TextToBool(parent->getAttribute(X("free"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatIdRef")))
	{
		// Deserialize string type
		this->SetformatIdRef(Dc1Convert::TextToString(parent->getAttribute(X("formatIdRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("rightsIDRefs")))
	{
		// Deserialize string type
		this->SetrightsIDRefs(Dc1Convert::TextToString(parent->getAttribute(X("rightsIDRefs"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("publicationDate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationDate")) == 0))
		{
			W3CdatePtr tmp = Createdate; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpublicationDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("publicationTime"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationTime")) == 0))
		{
			W3CtimePtr tmp = Createtime; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpublicationTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("scheduleDate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("scheduleDate")) == 0))
		{
			W3CdatePtr tmp = Createdate; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetscheduleDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("publicationService"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationService")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatepublicationServiceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpublicationService(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("publicationMedium"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationMedium")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatepublicationMediumType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpublicationMedium(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("publicationChannel"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationChannel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatepublicationChannelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpublicationChannel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("publicationRegion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("publicationRegion")) == 0))
		{
			// Deserialize factory type
			EbuCorepublicationEventType_publicationRegion_CollectionPtr tmp = CreatepublicationEventType_publicationRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpublicationRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorepublicationEventType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("publicationDate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createdate; // FTT, check this
	}
	this->SetpublicationDate(child);
  }
  if (XMLString::compareString(elementname, X("publicationTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createtime; // FTT, check this
	}
	this->SetpublicationTime(child);
  }
  if (XMLString::compareString(elementname, X("scheduleDate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createdate; // FTT, check this
	}
	this->SetscheduleDate(child);
  }
  if (XMLString::compareString(elementname, X("publicationService")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatepublicationServiceType; // FTT, check this
	}
	this->SetpublicationService(child);
  }
  if (XMLString::compareString(elementname, X("publicationMedium")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatepublicationMediumType; // FTT, check this
	}
	this->SetpublicationMedium(child);
  }
  if (XMLString::compareString(elementname, X("publicationChannel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatepublicationChannelType; // FTT, check this
	}
	this->SetpublicationChannel(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("publicationRegion")) == 0))
  {
	EbuCorepublicationEventType_publicationRegion_CollectionPtr tmp = CreatepublicationEventType_publicationRegion_CollectionType; // FTT, check this
	this->SetpublicationRegion(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorepublicationEventType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetpublicationDate() != Dc1NodePtr())
		result.Insert(GetpublicationDate());
	if (GetpublicationTime() != Dc1NodePtr())
		result.Insert(GetpublicationTime());
	if (GetscheduleDate() != Dc1NodePtr())
		result.Insert(GetscheduleDate());
	if (GetpublicationService() != Dc1NodePtr())
		result.Insert(GetpublicationService());
	if (GetpublicationMedium() != Dc1NodePtr())
		result.Insert(GetpublicationMedium());
	if (GetpublicationChannel() != Dc1NodePtr())
		result.Insert(GetpublicationChannel());
	if (GetpublicationRegion() != Dc1NodePtr())
		result.Insert(GetpublicationRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMethodImpl.h


