
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoretitleType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCoretitleType_title_CollectionType.h"
#include "EbuCoretitleType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:title

#include <assert.h>
IEbuCoretitleType::IEbuCoretitleType()
{

// no includefile for extension defined 
// file EbuCoretitleType_ExtPropInit.h

}

IEbuCoretitleType::~IEbuCoretitleType()
{
// no includefile for extension defined 
// file EbuCoretitleType_ExtPropCleanup.h

}

EbuCoretitleType::EbuCoretitleType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoretitleType::~EbuCoretitleType()
{
	Cleanup();
}

void EbuCoretitleType::Init()
{

	// Init attributes
	m_startYear = 2015; // Value
	m_startYear_Exist = false;
	m_startDate = W3CdatePtr(); // Pattern
	m_startDate_Exist = false;
	m_startTime = W3CtimePtr(); // Pattern
	m_startTime_Exist = false;
	m_endYear = 2015; // Value
	m_endYear_Exist = false;
	m_endDate = W3CdatePtr(); // Pattern
	m_endDate_Exist = false;
	m_endTime = W3CtimePtr(); // Pattern
	m_endTime_Exist = false;
	m_period = NULL; // String
	m_period_Exist = false;
	m_length = 1; // Value
	m_length_Exist = false;
	m_geographicalScope = NULL; // String
	m_geographicalScope_Exist = false;
	m_geographicalExclusionScope = NULL; // String
	m_geographicalExclusionScope_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_title = EbuCoretitleType_title_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoretitleType_ExtMyPropInit.h

}

void EbuCoretitleType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretitleType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	XMLString::release(&m_period); // String
	XMLString::release(&m_geographicalScope); // String
	XMLString::release(&m_geographicalExclusionScope); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_title);
}

void EbuCoretitleType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use titleTypePtr, since we
	// might need GetBase(), which isn't defined in ItitleType
	const Dc1Ptr< EbuCoretitleType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExiststartYear())
	{
		this->SetstartYear(tmp->GetstartYear());
	}
	else
	{
		InvalidatestartYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	if (tmp->ExiststartDate())
	{
		this->SetstartDate(Dc1Factory::CloneObject(tmp->GetstartDate()));
	}
	else
	{
		InvalidatestartDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	if (tmp->ExiststartTime())
	{
		this->SetstartTime(Dc1Factory::CloneObject(tmp->GetstartTime()));
	}
	else
	{
		InvalidatestartTime();
	}
	}
	{
	if (tmp->ExistendYear())
	{
		this->SetendYear(tmp->GetendYear());
	}
	else
	{
		InvalidateendYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	if (tmp->ExistendDate())
	{
		this->SetendDate(Dc1Factory::CloneObject(tmp->GetendDate()));
	}
	else
	{
		InvalidateendDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	if (tmp->ExistendTime())
	{
		this->SetendTime(Dc1Factory::CloneObject(tmp->GetendTime()));
	}
	else
	{
		InvalidateendTime();
	}
	}
	{
	XMLString::release(&m_period); // String
	if (tmp->Existperiod())
	{
		this->Setperiod(XMLString::replicate(tmp->Getperiod()));
	}
	else
	{
		Invalidateperiod();
	}
	}
	{
	if (tmp->Existlength())
	{
		this->Setlength(tmp->Getlength());
	}
	else
	{
		Invalidatelength();
	}
	}
	{
	XMLString::release(&m_geographicalScope); // String
	if (tmp->ExistgeographicalScope())
	{
		this->SetgeographicalScope(XMLString::replicate(tmp->GetgeographicalScope()));
	}
	else
	{
		InvalidategeographicalScope();
	}
	}
	{
	XMLString::release(&m_geographicalExclusionScope); // String
	if (tmp->ExistgeographicalExclusionScope())
	{
		this->SetgeographicalExclusionScope(XMLString::replicate(tmp->GetgeographicalExclusionScope()));
	}
	else
	{
		InvalidategeographicalExclusionScope();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_title);
		this->Settitle(Dc1Factory::CloneObject(tmp->Gettitle()));
}

Dc1NodePtr EbuCoretitleType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretitleType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


int EbuCoretitleType::GetstartYear() const
{
	return m_startYear;
}

bool EbuCoretitleType::ExiststartYear() const
{
	return m_startYear_Exist;
}
void EbuCoretitleType::SetstartYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetstartYear().");
	}
	m_startYear = item;
	m_startYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatestartYear()
{
	m_startYear_Exist = false;
}
W3CdatePtr EbuCoretitleType::GetstartDate() const
{
	return m_startDate;
}

bool EbuCoretitleType::ExiststartDate() const
{
	return m_startDate_Exist;
}
void EbuCoretitleType::SetstartDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetstartDate().");
	}
	m_startDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startDate) m_startDate->SetParent(m_myself.getPointer());
	m_startDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatestartDate()
{
	m_startDate_Exist = false;
}
W3CtimePtr EbuCoretitleType::GetstartTime() const
{
	return m_startTime;
}

bool EbuCoretitleType::ExiststartTime() const
{
	return m_startTime_Exist;
}
void EbuCoretitleType::SetstartTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetstartTime().");
	}
	m_startTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startTime) m_startTime->SetParent(m_myself.getPointer());
	m_startTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatestartTime()
{
	m_startTime_Exist = false;
}
int EbuCoretitleType::GetendYear() const
{
	return m_endYear;
}

bool EbuCoretitleType::ExistendYear() const
{
	return m_endYear_Exist;
}
void EbuCoretitleType::SetendYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetendYear().");
	}
	m_endYear = item;
	m_endYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidateendYear()
{
	m_endYear_Exist = false;
}
W3CdatePtr EbuCoretitleType::GetendDate() const
{
	return m_endDate;
}

bool EbuCoretitleType::ExistendDate() const
{
	return m_endDate_Exist;
}
void EbuCoretitleType::SetendDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetendDate().");
	}
	m_endDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endDate) m_endDate->SetParent(m_myself.getPointer());
	m_endDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidateendDate()
{
	m_endDate_Exist = false;
}
W3CtimePtr EbuCoretitleType::GetendTime() const
{
	return m_endTime;
}

bool EbuCoretitleType::ExistendTime() const
{
	return m_endTime_Exist;
}
void EbuCoretitleType::SetendTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetendTime().");
	}
	m_endTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endTime) m_endTime->SetParent(m_myself.getPointer());
	m_endTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidateendTime()
{
	m_endTime_Exist = false;
}
XMLCh * EbuCoretitleType::Getperiod() const
{
	return m_period;
}

bool EbuCoretitleType::Existperiod() const
{
	return m_period_Exist;
}
void EbuCoretitleType::Setperiod(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::Setperiod().");
	}
	m_period = item;
	m_period_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::Invalidateperiod()
{
	m_period_Exist = false;
}
unsigned EbuCoretitleType::Getlength() const
{
	return m_length;
}

bool EbuCoretitleType::Existlength() const
{
	return m_length_Exist;
}
void EbuCoretitleType::Setlength(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::Setlength().");
	}
	m_length = item;
	m_length_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::Invalidatelength()
{
	m_length_Exist = false;
}
XMLCh * EbuCoretitleType::GetgeographicalScope() const
{
	return m_geographicalScope;
}

bool EbuCoretitleType::ExistgeographicalScope() const
{
	return m_geographicalScope_Exist;
}
void EbuCoretitleType::SetgeographicalScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetgeographicalScope().");
	}
	m_geographicalScope = item;
	m_geographicalScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidategeographicalScope()
{
	m_geographicalScope_Exist = false;
}
XMLCh * EbuCoretitleType::GetgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope;
}

bool EbuCoretitleType::ExistgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope_Exist;
}
void EbuCoretitleType::SetgeographicalExclusionScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SetgeographicalExclusionScope().");
	}
	m_geographicalExclusionScope = item;
	m_geographicalExclusionScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidategeographicalExclusionScope()
{
	m_geographicalExclusionScope_Exist = false;
}
XMLCh * EbuCoretitleType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoretitleType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoretitleType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoretitleType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoretitleType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoretitleType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoretitleType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoretitleType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoretitleType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoretitleType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoretitleType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoretitleType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoretitleType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoretitleType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoretitleType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoretitleType::Getnote() const
{
	return m_note;
}

bool EbuCoretitleType::Existnote() const
{
	return m_note_Exist;
}
void EbuCoretitleType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretitleType::Invalidatenote()
{
	m_note_Exist = false;
}
EbuCoretitleType_title_CollectionPtr EbuCoretitleType::Gettitle() const
{
		return m_title;
}

void EbuCoretitleType::Settitle(const EbuCoretitleType_title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretitleType::Settitle().");
	}
	if (m_title != item)
	{
		// Dc1Factory::DeleteObject(m_title);
		m_title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_title) m_title->SetParent(m_myself.getPointer());
		if(m_title != EbuCoretitleType_title_CollectionPtr())
		{
			m_title->SetContentName(XMLString::transcode("title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoretitleType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  16, 16 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startYear")) == 0)
	{
		// startYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startDate")) == 0)
	{
		// startDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startTime")) == 0)
	{
		// startTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endYear")) == 0)
	{
		// endYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endDate")) == 0)
	{
		// endDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endTime")) == 0)
	{
		// endTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("period")) == 0)
	{
		// period is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("length")) == 0)
	{
		// length is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalScope")) == 0)
	{
		// geographicalScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalExclusionScope")) == 0)
	{
		// geographicalExclusionScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("title")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:title is item of type elementType
		// in element collection titleType_title_CollectionType
		
		context->Found = true;
		EbuCoretitleType_title_CollectionPtr coll = Gettitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetitleType_title_CollectionType; // FTT, check this
				Settitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for titleType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "titleType");
	}
	return result;
}

XMLCh * EbuCoretitleType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoretitleType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoretitleType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("titleType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_startYear);
		element->setAttributeNS(X(""), X("startYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startDate_Exist)
	{
	// Pattern
	if (m_startDate != W3CdatePtr())
	{
		XMLCh * tmp = m_startDate->ToText();
		element->setAttributeNS(X(""), X("startDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startTime_Exist)
	{
	// Pattern
	if (m_startTime != W3CtimePtr())
	{
		XMLCh * tmp = m_startTime->ToText();
		element->setAttributeNS(X(""), X("startTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_endYear);
		element->setAttributeNS(X(""), X("endYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endDate_Exist)
	{
	// Pattern
	if (m_endDate != W3CdatePtr())
	{
		XMLCh * tmp = m_endDate->ToText();
		element->setAttributeNS(X(""), X("endDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endTime_Exist)
	{
	// Pattern
	if (m_endTime != W3CtimePtr())
	{
		XMLCh * tmp = m_endTime->ToText();
		element->setAttributeNS(X(""), X("endTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_period_Exist)
	{
	// String
	if(m_period != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("period"), m_period);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_length_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_length);
		element->setAttributeNS(X(""), X("length"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalScope_Exist)
	{
	// String
	if(m_geographicalScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalScope"), m_geographicalScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalExclusionScope_Exist)
	{
	// String
	if(m_geographicalExclusionScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalExclusionScope"), m_geographicalExclusionScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_title != EbuCoretitleType_title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_title->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoretitleType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startYear")))
	{
		// deserialize value type
		this->SetstartYear(Dc1Convert::TextToInt(parent->getAttribute(X("startYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startDate")));
		this->SetstartDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startTime")));
		this->SetstartTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endYear")))
	{
		// deserialize value type
		this->SetendYear(Dc1Convert::TextToInt(parent->getAttribute(X("endYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endDate")));
		this->SetendDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endTime")));
		this->SetendTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("period")))
	{
		// Deserialize string type
		this->Setperiod(Dc1Convert::TextToString(parent->getAttribute(X("period"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("length")))
	{
		// deserialize value type
		this->Setlength(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("length"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalScope")))
	{
		// Deserialize string type
		this->SetgeographicalScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalExclusionScope")))
	{
		// Deserialize string type
		this->SetgeographicalExclusionScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalExclusionScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("title"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("title")) == 0))
		{
			// Deserialize factory type
			EbuCoretitleType_title_CollectionPtr tmp = CreatetitleType_title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoretitleType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoretitleType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("title")) == 0))
  {
	EbuCoretitleType_title_CollectionPtr tmp = CreatetitleType_title_CollectionType; // FTT, check this
	this->Settitle(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoretitleType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettitle() != Dc1NodePtr())
		result.Insert(Gettitle());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoretitleType_ExtMethodImpl.h


