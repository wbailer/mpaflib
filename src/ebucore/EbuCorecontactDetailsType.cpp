
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "EbuCorecontactDetailsType_name_CollectionType.h"
#include "EbuCoreelementType.h"
#include "EbuCorecontactDetailsType_otherGivenName_CollectionType.h"
#include "EbuCorecontactDetailsType_username_CollectionType.h"
#include "EbuCorecontactDetailsType_nickname_CollectionType.h"
#include "EbuCorecontactDetailsType_details_CollectionType.h"
#include "EbuCorecontactDetailsType_stageName_CollectionType.h"
#include "EbuCorecontactDetailsType_relatedInformationLink_CollectionType.h"
#include "EbuCorecontactDetailsType_relatedContacts_CollectionType.h"
#include "EbuCorecontactDetailsType_skill_CollectionType.h"
#include "EbuCorecontactDetailsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorecompoundNameType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:name
#include "EbuCoredetailsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:details
#include "EbuCorecontactDetailsType_relatedInformationLink_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink
#include "EbuCoreentityType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:relatedContacts

#include <assert.h>
IEbuCorecontactDetailsType::IEbuCorecontactDetailsType()
{

// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtPropInit.h

}

IEbuCorecontactDetailsType::~IEbuCorecontactDetailsType()
{
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtPropCleanup.h

}

EbuCorecontactDetailsType::EbuCorecontactDetailsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecontactDetailsType::~EbuCorecontactDetailsType()
{
	Cleanup();
}

void EbuCorecontactDetailsType::Init()
{

	// Init attributes
	m_contactId = NULL; // String
	m_contactId_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_lastUpdate = W3CdatePtr(); // Pattern
	m_lastUpdate_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_name = EbuCorecontactDetailsType_name_CollectionPtr(); // Collection
	m_givenName = EbuCoreelementPtr(); // Class
	m_givenName_Exist = false;
	m_familyName = EbuCoreelementPtr(); // Class
	m_familyName_Exist = false;
	m_otherGivenName = EbuCorecontactDetailsType_otherGivenName_CollectionPtr(); // Collection
	m_suffix = EbuCoreelementPtr(); // Class
	m_suffix_Exist = false;
	m_salutation = EbuCoreelementPtr(); // Class
	m_salutation_Exist = false;
	m_birthDate = W3CdatePtr(); // Pattern
	m_deathDate = W3CdatePtr(); // Pattern
	m_username = EbuCorecontactDetailsType_username_CollectionPtr(); // Collection
	m_nickname = EbuCorecontactDetailsType_nickname_CollectionPtr(); // Collection
	m_occupation = EbuCoreelementPtr(); // Class
	m_occupation_Exist = false;
	m_details = EbuCorecontactDetailsType_details_CollectionPtr(); // Collection
	m_stageName = EbuCorecontactDetailsType_stageName_CollectionPtr(); // Collection
	m_guest = (false); // Value

	m_guest_Exist = false;
	m_gender = EbuCoreelementPtr(); // Class
	m_gender_Exist = false;
	m_relatedInformationLink = EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr(); // Collection
	m_relatedContacts = EbuCorecontactDetailsType_relatedContacts_CollectionPtr(); // Collection
	m_skill = EbuCorecontactDetailsType_skill_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMyPropInit.h

}

void EbuCorecontactDetailsType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMyPropCleanup.h


	XMLString::release(&m_contactId); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_lastUpdate); // Pattern
	// Dc1Factory::DeleteObject(m_name);
	// Dc1Factory::DeleteObject(m_givenName);
	// Dc1Factory::DeleteObject(m_familyName);
	// Dc1Factory::DeleteObject(m_otherGivenName);
	// Dc1Factory::DeleteObject(m_suffix);
	// Dc1Factory::DeleteObject(m_salutation);
	// Dc1Factory::DeleteObject(m_birthDate);
	// Dc1Factory::DeleteObject(m_deathDate);
	// Dc1Factory::DeleteObject(m_username);
	// Dc1Factory::DeleteObject(m_nickname);
	// Dc1Factory::DeleteObject(m_occupation);
	// Dc1Factory::DeleteObject(m_details);
	// Dc1Factory::DeleteObject(m_stageName);
	// Dc1Factory::DeleteObject(m_gender);
	// Dc1Factory::DeleteObject(m_relatedInformationLink);
	// Dc1Factory::DeleteObject(m_relatedContacts);
	// Dc1Factory::DeleteObject(m_skill);
}

void EbuCorecontactDetailsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use contactDetailsTypePtr, since we
	// might need GetBase(), which isn't defined in IcontactDetailsType
	const Dc1Ptr< EbuCorecontactDetailsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_contactId); // String
	if (tmp->ExistcontactId())
	{
		this->SetcontactId(XMLString::replicate(tmp->GetcontactId()));
	}
	else
	{
		InvalidatecontactId();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_lastUpdate); // Pattern
	if (tmp->ExistlastUpdate())
	{
		this->SetlastUpdate(Dc1Factory::CloneObject(tmp->GetlastUpdate()));
	}
	else
	{
		InvalidatelastUpdate();
	}
	}
		// Dc1Factory::DeleteObject(m_name);
		this->Setname(Dc1Factory::CloneObject(tmp->Getname()));
	if (tmp->IsValidgivenName())
	{
		// Dc1Factory::DeleteObject(m_givenName);
		this->SetgivenName(Dc1Factory::CloneObject(tmp->GetgivenName()));
	}
	else
	{
		InvalidategivenName();
	}
	if (tmp->IsValidfamilyName())
	{
		// Dc1Factory::DeleteObject(m_familyName);
		this->SetfamilyName(Dc1Factory::CloneObject(tmp->GetfamilyName()));
	}
	else
	{
		InvalidatefamilyName();
	}
		// Dc1Factory::DeleteObject(m_otherGivenName);
		this->SetotherGivenName(Dc1Factory::CloneObject(tmp->GetotherGivenName()));
	if (tmp->IsValidsuffix())
	{
		// Dc1Factory::DeleteObject(m_suffix);
		this->Setsuffix(Dc1Factory::CloneObject(tmp->Getsuffix()));
	}
	else
	{
		Invalidatesuffix();
	}
	if (tmp->IsValidsalutation())
	{
		// Dc1Factory::DeleteObject(m_salutation);
		this->Setsalutation(Dc1Factory::CloneObject(tmp->Getsalutation()));
	}
	else
	{
		Invalidatesalutation();
	}
		// Dc1Factory::DeleteObject(m_birthDate);
		this->SetbirthDate(Dc1Factory::CloneObject(tmp->GetbirthDate()));
		// Dc1Factory::DeleteObject(m_deathDate);
		this->SetdeathDate(Dc1Factory::CloneObject(tmp->GetdeathDate()));
		// Dc1Factory::DeleteObject(m_username);
		this->Setusername(Dc1Factory::CloneObject(tmp->Getusername()));
		// Dc1Factory::DeleteObject(m_nickname);
		this->Setnickname(Dc1Factory::CloneObject(tmp->Getnickname()));
	if (tmp->IsValidoccupation())
	{
		// Dc1Factory::DeleteObject(m_occupation);
		this->Setoccupation(Dc1Factory::CloneObject(tmp->Getoccupation()));
	}
	else
	{
		Invalidateoccupation();
	}
		// Dc1Factory::DeleteObject(m_details);
		this->Setdetails(Dc1Factory::CloneObject(tmp->Getdetails()));
		// Dc1Factory::DeleteObject(m_stageName);
		this->SetstageName(Dc1Factory::CloneObject(tmp->GetstageName()));
	if (tmp->IsValidguest())
	{
		this->Setguest(tmp->Getguest());
	}
	else
	{
		Invalidateguest();
	}
	if (tmp->IsValidgender())
	{
		// Dc1Factory::DeleteObject(m_gender);
		this->Setgender(Dc1Factory::CloneObject(tmp->Getgender()));
	}
	else
	{
		Invalidategender();
	}
		// Dc1Factory::DeleteObject(m_relatedInformationLink);
		this->SetrelatedInformationLink(Dc1Factory::CloneObject(tmp->GetrelatedInformationLink()));
		// Dc1Factory::DeleteObject(m_relatedContacts);
		this->SetrelatedContacts(Dc1Factory::CloneObject(tmp->GetrelatedContacts()));
		// Dc1Factory::DeleteObject(m_skill);
		this->Setskill(Dc1Factory::CloneObject(tmp->Getskill()));
}

Dc1NodePtr EbuCorecontactDetailsType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorecontactDetailsType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorecontactDetailsType::GetcontactId() const
{
	return m_contactId;
}

bool EbuCorecontactDetailsType::ExistcontactId() const
{
	return m_contactId_Exist;
}
void EbuCorecontactDetailsType::SetcontactId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetcontactId().");
	}
	m_contactId = item;
	m_contactId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatecontactId()
{
	m_contactId_Exist = false;
}
XMLCh * EbuCorecontactDetailsType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorecontactDetailsType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorecontactDetailsType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorecontactDetailsType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorecontactDetailsType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorecontactDetailsType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorecontactDetailsType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorecontactDetailsType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorecontactDetailsType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorecontactDetailsType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorecontactDetailsType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorecontactDetailsType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorecontactDetailsType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorecontactDetailsType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorecontactDetailsType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
W3CdatePtr EbuCorecontactDetailsType::GetlastUpdate() const
{
	return m_lastUpdate;
}

bool EbuCorecontactDetailsType::ExistlastUpdate() const
{
	return m_lastUpdate_Exist;
}
void EbuCorecontactDetailsType::SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetlastUpdate().");
	}
	m_lastUpdate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_lastUpdate) m_lastUpdate->SetParent(m_myself.getPointer());
	m_lastUpdate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatelastUpdate()
{
	m_lastUpdate_Exist = false;
}
EbuCorecontactDetailsType_name_CollectionPtr EbuCorecontactDetailsType::Getname() const
{
		return m_name;
}

EbuCoreelementPtr EbuCorecontactDetailsType::GetgivenName() const
{
		return m_givenName;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidgivenName() const
{
	return m_givenName_Exist;
}

EbuCoreelementPtr EbuCorecontactDetailsType::GetfamilyName() const
{
		return m_familyName;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidfamilyName() const
{
	return m_familyName_Exist;
}

EbuCorecontactDetailsType_otherGivenName_CollectionPtr EbuCorecontactDetailsType::GetotherGivenName() const
{
		return m_otherGivenName;
}

EbuCoreelementPtr EbuCorecontactDetailsType::Getsuffix() const
{
		return m_suffix;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidsuffix() const
{
	return m_suffix_Exist;
}

EbuCoreelementPtr EbuCorecontactDetailsType::Getsalutation() const
{
		return m_salutation;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidsalutation() const
{
	return m_salutation_Exist;
}

W3CdatePtr EbuCorecontactDetailsType::GetbirthDate() const
{
		return m_birthDate;
}

W3CdatePtr EbuCorecontactDetailsType::GetdeathDate() const
{
		return m_deathDate;
}

EbuCorecontactDetailsType_username_CollectionPtr EbuCorecontactDetailsType::Getusername() const
{
		return m_username;
}

EbuCorecontactDetailsType_nickname_CollectionPtr EbuCorecontactDetailsType::Getnickname() const
{
		return m_nickname;
}

EbuCoreelementPtr EbuCorecontactDetailsType::Getoccupation() const
{
		return m_occupation;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidoccupation() const
{
	return m_occupation_Exist;
}

EbuCorecontactDetailsType_details_CollectionPtr EbuCorecontactDetailsType::Getdetails() const
{
		return m_details;
}

EbuCorecontactDetailsType_stageName_CollectionPtr EbuCorecontactDetailsType::GetstageName() const
{
		return m_stageName;
}

bool EbuCorecontactDetailsType::Getguest() const
{
		return m_guest;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidguest() const
{
	return m_guest_Exist;
}

EbuCoreelementPtr EbuCorecontactDetailsType::Getgender() const
{
		return m_gender;
}

// Element is optional
bool EbuCorecontactDetailsType::IsValidgender() const
{
	return m_gender_Exist;
}

EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr EbuCorecontactDetailsType::GetrelatedInformationLink() const
{
		return m_relatedInformationLink;
}

EbuCorecontactDetailsType_relatedContacts_CollectionPtr EbuCorecontactDetailsType::GetrelatedContacts() const
{
		return m_relatedContacts;
}

EbuCorecontactDetailsType_skill_CollectionPtr EbuCorecontactDetailsType::Getskill() const
{
		return m_skill;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void EbuCorecontactDetailsType::Setname(const EbuCorecontactDetailsType_name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setname().");
	}
	if (m_name != item)
	{
		// Dc1Factory::DeleteObject(m_name);
		m_name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_name) m_name->SetParent(m_myself.getPointer());
		if(m_name != EbuCorecontactDetailsType_name_CollectionPtr())
		{
			m_name->SetContentName(XMLString::transcode("name"));
		}
	// Dc1Factory::DeleteObject(m_givenName);
	m_givenName = EbuCoreelementPtr();
	// Dc1Factory::DeleteObject(m_familyName);
	m_familyName = EbuCoreelementPtr();
	// Dc1Factory::DeleteObject(m_otherGivenName);
	m_otherGivenName = EbuCorecontactDetailsType_otherGivenName_CollectionPtr();
	// Dc1Factory::DeleteObject(m_suffix);
	m_suffix = EbuCoreelementPtr();
	// Dc1Factory::DeleteObject(m_salutation);
	m_salutation = EbuCoreelementPtr();
	// Dc1Factory::DeleteObject(m_birthDate);
	m_birthDate = W3CdatePtr();
	// Dc1Factory::DeleteObject(m_deathDate);
	m_deathDate = W3CdatePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void EbuCorecontactDetailsType::SetgivenName(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetgivenName().");
	}
	if (m_givenName != item || m_givenName_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_givenName);
		m_givenName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_givenName) m_givenName->SetParent(m_myself.getPointer());
		if (m_givenName && m_givenName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_givenName->UseTypeAttribute = true;
		}
		m_givenName_Exist = true;
		if(m_givenName != EbuCoreelementPtr())
		{
			m_givenName->SetContentName(XMLString::transcode("givenName"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidategivenName()
{
	m_givenName_Exist = false;
}
void EbuCorecontactDetailsType::SetfamilyName(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetfamilyName().");
	}
	if (m_familyName != item || m_familyName_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_familyName);
		m_familyName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_familyName) m_familyName->SetParent(m_myself.getPointer());
		if (m_familyName && m_familyName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_familyName->UseTypeAttribute = true;
		}
		m_familyName_Exist = true;
		if(m_familyName != EbuCoreelementPtr())
		{
			m_familyName->SetContentName(XMLString::transcode("familyName"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::InvalidatefamilyName()
{
	m_familyName_Exist = false;
}
void EbuCorecontactDetailsType::SetotherGivenName(const EbuCorecontactDetailsType_otherGivenName_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetotherGivenName().");
	}
	if (m_otherGivenName != item)
	{
		// Dc1Factory::DeleteObject(m_otherGivenName);
		m_otherGivenName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_otherGivenName) m_otherGivenName->SetParent(m_myself.getPointer());
		if(m_otherGivenName != EbuCorecontactDetailsType_otherGivenName_CollectionPtr())
		{
			m_otherGivenName->SetContentName(XMLString::transcode("otherGivenName"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setsuffix(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setsuffix().");
	}
	if (m_suffix != item || m_suffix_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_suffix);
		m_suffix = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_suffix) m_suffix->SetParent(m_myself.getPointer());
		if (m_suffix && m_suffix->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_suffix->UseTypeAttribute = true;
		}
		m_suffix_Exist = true;
		if(m_suffix != EbuCoreelementPtr())
		{
			m_suffix->SetContentName(XMLString::transcode("suffix"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Invalidatesuffix()
{
	m_suffix_Exist = false;
}
void EbuCorecontactDetailsType::Setsalutation(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setsalutation().");
	}
	if (m_salutation != item || m_salutation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_salutation);
		m_salutation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_salutation) m_salutation->SetParent(m_myself.getPointer());
		if (m_salutation && m_salutation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_salutation->UseTypeAttribute = true;
		}
		m_salutation_Exist = true;
		if(m_salutation != EbuCoreelementPtr())
		{
			m_salutation->SetContentName(XMLString::transcode("salutation"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Invalidatesalutation()
{
	m_salutation_Exist = false;
}
void EbuCorecontactDetailsType::SetbirthDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetbirthDate().");
	}
	if (m_birthDate != item)
	{
		// Dc1Factory::DeleteObject(m_birthDate);
		m_birthDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_birthDate) m_birthDate->SetParent(m_myself.getPointer());
		if (m_birthDate && m_birthDate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_birthDate->UseTypeAttribute = true;
		}
		if(m_birthDate != W3CdatePtr())
		{
			m_birthDate->SetContentName(XMLString::transcode("birthDate"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::SetdeathDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetdeathDate().");
	}
	if (m_deathDate != item)
	{
		// Dc1Factory::DeleteObject(m_deathDate);
		m_deathDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_deathDate) m_deathDate->SetParent(m_myself.getPointer());
		if (m_deathDate && m_deathDate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_deathDate->UseTypeAttribute = true;
		}
		if(m_deathDate != W3CdatePtr())
		{
			m_deathDate->SetContentName(XMLString::transcode("deathDate"));
		}
	// Dc1Factory::DeleteObject(m_name);
	m_name = EbuCorecontactDetailsType_name_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setusername(const EbuCorecontactDetailsType_username_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setusername().");
	}
	if (m_username != item)
	{
		// Dc1Factory::DeleteObject(m_username);
		m_username = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_username) m_username->SetParent(m_myself.getPointer());
		if(m_username != EbuCorecontactDetailsType_username_CollectionPtr())
		{
			m_username->SetContentName(XMLString::transcode("username"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setnickname(const EbuCorecontactDetailsType_nickname_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setnickname().");
	}
	if (m_nickname != item)
	{
		// Dc1Factory::DeleteObject(m_nickname);
		m_nickname = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_nickname) m_nickname->SetParent(m_myself.getPointer());
		if(m_nickname != EbuCorecontactDetailsType_nickname_CollectionPtr())
		{
			m_nickname->SetContentName(XMLString::transcode("nickname"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setoccupation(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setoccupation().");
	}
	if (m_occupation != item || m_occupation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_occupation);
		m_occupation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_occupation) m_occupation->SetParent(m_myself.getPointer());
		if (m_occupation && m_occupation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_occupation->UseTypeAttribute = true;
		}
		m_occupation_Exist = true;
		if(m_occupation != EbuCoreelementPtr())
		{
			m_occupation->SetContentName(XMLString::transcode("occupation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Invalidateoccupation()
{
	m_occupation_Exist = false;
}
void EbuCorecontactDetailsType::Setdetails(const EbuCorecontactDetailsType_details_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setdetails().");
	}
	if (m_details != item)
	{
		// Dc1Factory::DeleteObject(m_details);
		m_details = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_details) m_details->SetParent(m_myself.getPointer());
		if(m_details != EbuCorecontactDetailsType_details_CollectionPtr())
		{
			m_details->SetContentName(XMLString::transcode("details"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::SetstageName(const EbuCorecontactDetailsType_stageName_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetstageName().");
	}
	if (m_stageName != item)
	{
		// Dc1Factory::DeleteObject(m_stageName);
		m_stageName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_stageName) m_stageName->SetParent(m_myself.getPointer());
		if(m_stageName != EbuCorecontactDetailsType_stageName_CollectionPtr())
		{
			m_stageName->SetContentName(XMLString::transcode("stageName"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setguest(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setguest().");
	}
	if (m_guest != item || m_guest_Exist == false)
	{
		m_guest = item;
		m_guest_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Invalidateguest()
{
	m_guest_Exist = false;
}
void EbuCorecontactDetailsType::Setgender(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setgender().");
	}
	if (m_gender != item || m_gender_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_gender);
		m_gender = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_gender) m_gender->SetParent(m_myself.getPointer());
		if (m_gender && m_gender->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_gender->UseTypeAttribute = true;
		}
		m_gender_Exist = true;
		if(m_gender != EbuCoreelementPtr())
		{
			m_gender->SetContentName(XMLString::transcode("gender"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Invalidategender()
{
	m_gender_Exist = false;
}
void EbuCorecontactDetailsType::SetrelatedInformationLink(const EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetrelatedInformationLink().");
	}
	if (m_relatedInformationLink != item)
	{
		// Dc1Factory::DeleteObject(m_relatedInformationLink);
		m_relatedInformationLink = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relatedInformationLink) m_relatedInformationLink->SetParent(m_myself.getPointer());
		if(m_relatedInformationLink != EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr())
		{
			m_relatedInformationLink->SetContentName(XMLString::transcode("relatedInformationLink"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::SetrelatedContacts(const EbuCorecontactDetailsType_relatedContacts_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::SetrelatedContacts().");
	}
	if (m_relatedContacts != item)
	{
		// Dc1Factory::DeleteObject(m_relatedContacts);
		m_relatedContacts = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relatedContacts) m_relatedContacts->SetParent(m_myself.getPointer());
		if(m_relatedContacts != EbuCorecontactDetailsType_relatedContacts_CollectionPtr())
		{
			m_relatedContacts->SetContentName(XMLString::transcode("relatedContacts"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecontactDetailsType::Setskill(const EbuCorecontactDetailsType_skill_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecontactDetailsType::Setskill().");
	}
	if (m_skill != item)
	{
		// Dc1Factory::DeleteObject(m_skill);
		m_skill = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_skill) m_skill->SetParent(m_myself.getPointer());
		if(m_skill != EbuCorecontactDetailsType_skill_CollectionPtr())
		{
			m_skill->SetContentName(XMLString::transcode("skill"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorecontactDetailsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("contactId")) == 0)
	{
		// contactId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lastUpdate")) == 0)
	{
		// lastUpdate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("name")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:name is item of type compoundNameType
		// in element collection contactDetailsType_name_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_name_CollectionPtr coll = Getname();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_name_CollectionType; // FTT, check this
				Setname(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:compoundNameType")))) != empty)
			{
				// Is type allowed
				EbuCorecompoundNamePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("givenName")) == 0)
	{
		// givenName is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetgivenName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetgivenName(p, client);
					if((p = GetgivenName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("familyName")) == 0)
	{
		// familyName is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetfamilyName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetfamilyName(p, client);
					if((p = GetfamilyName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("otherGivenName")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:otherGivenName is item of type elementType
		// in element collection contactDetailsType_otherGivenName_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_otherGivenName_CollectionPtr coll = GetotherGivenName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_otherGivenName_CollectionType; // FTT, check this
				SetotherGivenName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("suffix")) == 0)
	{
		// suffix is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getsuffix()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setsuffix(p, client);
					if((p = Getsuffix()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("salutation")) == 0)
	{
		// salutation is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getsalutation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setsalutation(p, client);
					if((p = Getsalutation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("birthDate")) == 0)
	{
		// birthDate is simple element date
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetbirthDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))) != empty)
			{
				// Is type allowed
				W3CdatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetbirthDate(p, client);
					if((p = GetbirthDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("deathDate")) == 0)
	{
		// deathDate is simple element date
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdeathDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))) != empty)
			{
				// Is type allowed
				W3CdatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdeathDate(p, client);
					if((p = GetdeathDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("username")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:username is item of type elementType
		// in element collection contactDetailsType_username_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_username_CollectionPtr coll = Getusername();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_username_CollectionType; // FTT, check this
				Setusername(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("nickname")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:nickname is item of type elementType
		// in element collection contactDetailsType_nickname_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_nickname_CollectionPtr coll = Getnickname();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_nickname_CollectionType; // FTT, check this
				Setnickname(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("occupation")) == 0)
	{
		// occupation is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getoccupation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setoccupation(p, client);
					if((p = Getoccupation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("details")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:details is item of type detailsType
		// in element collection contactDetailsType_details_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_details_CollectionPtr coll = Getdetails();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_details_CollectionType; // FTT, check this
				Setdetails(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:detailsType")))) != empty)
			{
				// Is type allowed
				EbuCoredetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("stageName")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:stageName is item of type elementType
		// in element collection contactDetailsType_stageName_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_stageName_CollectionPtr coll = GetstageName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_stageName_CollectionType; // FTT, check this
				SetstageName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("gender")) == 0)
	{
		// gender is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getgender()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setgender(p, client);
					if((p = Getgender()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relatedInformationLink")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink is item of type contactDetailsType_relatedInformationLink_LocalType
		// in element collection contactDetailsType_relatedInformationLink_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr coll = GetrelatedInformationLink();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_relatedInformationLink_CollectionType; // FTT, check this
				SetrelatedInformationLink(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedInformationLink_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecontactDetailsType_relatedInformationLink_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relatedContacts")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:relatedContacts is item of type entityType
		// in element collection contactDetailsType_relatedContacts_CollectionType
		
		context->Found = true;
		EbuCorecontactDetailsType_relatedContacts_CollectionPtr coll = GetrelatedContacts();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecontactDetailsType_relatedContacts_CollectionType; // FTT, check this
				SetrelatedContacts(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("skill")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for contactDetailsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "contactDetailsType");
	}
	return result;
}

XMLCh * EbuCorecontactDetailsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorecontactDetailsType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorecontactDetailsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("contactDetailsType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_contactId_Exist)
	{
	// String
	if(m_contactId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("contactId"), m_contactId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lastUpdate_Exist)
	{
	// Pattern
	if (m_lastUpdate != W3CdatePtr())
	{
		XMLCh * tmp = m_lastUpdate->ToText();
		element->setAttributeNS(X(""), X("lastUpdate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_name != EbuCorecontactDetailsType_name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_givenName != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_givenName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("givenName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_givenName->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_familyName != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_familyName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("familyName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_familyName->Serialize(doc, element, &elem);
	}
	if (m_otherGivenName != EbuCorecontactDetailsType_otherGivenName_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_otherGivenName->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_suffix != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_suffix->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("suffix"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_suffix->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_salutation != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_salutation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("salutation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_salutation->Serialize(doc, element, &elem);
	}
	if(m_birthDate != W3CdatePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("birthDate");
//		m_birthDate->SetContentName(contentname);
		m_birthDate->UseTypeAttribute = this->UseTypeAttribute;
		m_birthDate->Serialize(doc, element);
	}
	if(m_deathDate != W3CdatePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("deathDate");
//		m_deathDate->SetContentName(contentname);
		m_deathDate->UseTypeAttribute = this->UseTypeAttribute;
		m_deathDate->Serialize(doc, element);
	}
	if (m_username != EbuCorecontactDetailsType_username_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_username->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_nickname != EbuCorecontactDetailsType_nickname_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_nickname->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_occupation != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_occupation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("occupation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_occupation->Serialize(doc, element, &elem);
	}
	if (m_details != EbuCorecontactDetailsType_details_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_details->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_stageName != EbuCorecontactDetailsType_stageName_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_stageName->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_guest_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_guest);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("guest"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_gender != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_gender->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("gender"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_gender->Serialize(doc, element, &elem);
	}
	if (m_relatedInformationLink != EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_relatedInformationLink->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_relatedContacts != EbuCorecontactDetailsType_relatedContacts_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_relatedContacts->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_skill != EbuCorecontactDetailsType_skill_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_skill->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorecontactDetailsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("contactId")))
	{
		// Deserialize string type
		this->SetcontactId(Dc1Convert::TextToString(parent->getAttribute(X("contactId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("lastUpdate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("lastUpdate")));
		this->SetlastUpdate(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("name"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("name")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_name_CollectionPtr tmp = CreatecontactDetailsType_name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setname(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("givenName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("givenName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetgivenName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("familyName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("familyName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetfamilyName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("otherGivenName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("otherGivenName")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_otherGivenName_CollectionPtr tmp = CreatecontactDetailsType_otherGivenName_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetotherGivenName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("suffix"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("suffix")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setsuffix(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("salutation"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("salutation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setsalutation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("birthDate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("birthDate")) == 0))
		{
			W3CdatePtr tmp = Createdate; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetbirthDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("deathDate"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("deathDate")) == 0))
		{
			W3CdatePtr tmp = Createdate; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetdeathDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("username"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("username")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_username_CollectionPtr tmp = CreatecontactDetailsType_username_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setusername(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("nickname"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("nickname")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_nickname_CollectionPtr tmp = CreatecontactDetailsType_nickname_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setnickname(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("occupation"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("occupation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setoccupation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("details"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("details")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_details_CollectionPtr tmp = CreatecontactDetailsType_details_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdetails(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("stageName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("stageName")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_stageName_CollectionPtr tmp = CreatecontactDetailsType_stageName_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetstageName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("guest"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setguest(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("gender"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("gender")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setgender(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("relatedInformationLink"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("relatedInformationLink")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr tmp = CreatecontactDetailsType_relatedInformationLink_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetrelatedInformationLink(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("relatedContacts"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("relatedContacts")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_relatedContacts_CollectionPtr tmp = CreatecontactDetailsType_relatedContacts_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetrelatedContacts(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("skill"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("skill")) == 0))
		{
			// Deserialize factory type
			EbuCorecontactDetailsType_skill_CollectionPtr tmp = CreatecontactDetailsType_skill_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setskill(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecontactDetailsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("name")) == 0))
  {
	EbuCorecontactDetailsType_name_CollectionPtr tmp = CreatecontactDetailsType_name_CollectionType; // FTT, check this
	this->Setname(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("givenName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->SetgivenName(child);
  }
  if (XMLString::compareString(elementname, X("familyName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->SetfamilyName(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("otherGivenName")) == 0))
  {
	EbuCorecontactDetailsType_otherGivenName_CollectionPtr tmp = CreatecontactDetailsType_otherGivenName_CollectionType; // FTT, check this
	this->SetotherGivenName(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("suffix")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->Setsuffix(child);
  }
  if (XMLString::compareString(elementname, X("salutation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->Setsalutation(child);
  }
  if (XMLString::compareString(elementname, X("birthDate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createdate; // FTT, check this
	}
	this->SetbirthDate(child);
  }
  if (XMLString::compareString(elementname, X("deathDate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createdate; // FTT, check this
	}
	this->SetdeathDate(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("username")) == 0))
  {
	EbuCorecontactDetailsType_username_CollectionPtr tmp = CreatecontactDetailsType_username_CollectionType; // FTT, check this
	this->Setusername(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("nickname")) == 0))
  {
	EbuCorecontactDetailsType_nickname_CollectionPtr tmp = CreatecontactDetailsType_nickname_CollectionType; // FTT, check this
	this->Setnickname(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("occupation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->Setoccupation(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("details")) == 0))
  {
	EbuCorecontactDetailsType_details_CollectionPtr tmp = CreatecontactDetailsType_details_CollectionType; // FTT, check this
	this->Setdetails(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("stageName")) == 0))
  {
	EbuCorecontactDetailsType_stageName_CollectionPtr tmp = CreatecontactDetailsType_stageName_CollectionType; // FTT, check this
	this->SetstageName(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("gender")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->Setgender(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("relatedInformationLink")) == 0))
  {
	EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr tmp = CreatecontactDetailsType_relatedInformationLink_CollectionType; // FTT, check this
	this->SetrelatedInformationLink(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("relatedContacts")) == 0))
  {
	EbuCorecontactDetailsType_relatedContacts_CollectionPtr tmp = CreatecontactDetailsType_relatedContacts_CollectionType; // FTT, check this
	this->SetrelatedContacts(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("skill")) == 0))
  {
	EbuCorecontactDetailsType_skill_CollectionPtr tmp = CreatecontactDetailsType_skill_CollectionType; // FTT, check this
	this->Setskill(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorecontactDetailsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getusername() != Dc1NodePtr())
		result.Insert(Getusername());
	if (Getnickname() != Dc1NodePtr())
		result.Insert(Getnickname());
	if (Getoccupation() != Dc1NodePtr())
		result.Insert(Getoccupation());
	if (Getdetails() != Dc1NodePtr())
		result.Insert(Getdetails());
	if (GetstageName() != Dc1NodePtr())
		result.Insert(GetstageName());
	if (Getgender() != Dc1NodePtr())
		result.Insert(Getgender());
	if (GetrelatedInformationLink() != Dc1NodePtr())
		result.Insert(GetrelatedInformationLink());
	if (GetrelatedContacts() != Dc1NodePtr())
		result.Insert(GetrelatedContacts());
	if (Getskill() != Dc1NodePtr())
		result.Insert(Getskill());
	if (Getname() != Dc1NodePtr())
		result.Insert(Getname());
	if (GetgivenName() != Dc1NodePtr())
		result.Insert(GetgivenName());
	if (GetfamilyName() != Dc1NodePtr())
		result.Insert(GetfamilyName());
	if (GetotherGivenName() != Dc1NodePtr())
		result.Insert(GetotherGivenName());
	if (Getsuffix() != Dc1NodePtr())
		result.Insert(Getsuffix());
	if (Getsalutation() != Dc1NodePtr())
		result.Insert(Getsalutation());
	if (GetbirthDate() != Dc1NodePtr())
		result.Insert(GetbirthDate());
	if (GetdeathDate() != Dc1NodePtr())
		result.Insert(GetdeathDate());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMethodImpl.h


