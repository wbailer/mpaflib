
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtImplInclude.h


#include "EbuCorevideoFormatType_videoTrack_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorevideoFormatType_videoTrack_LocalType::IEbuCorevideoFormatType_videoTrack_LocalType()
{

// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtPropInit.h

}

IEbuCorevideoFormatType_videoTrack_LocalType::~IEbuCorevideoFormatType_videoTrack_LocalType()
{
// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtPropCleanup.h

}

EbuCorevideoFormatType_videoTrack_LocalType::EbuCorevideoFormatType_videoTrack_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorevideoFormatType_videoTrack_LocalType::~EbuCorevideoFormatType_videoTrack_LocalType()
{
	Cleanup();
}

void EbuCorevideoFormatType_videoTrack_LocalType::Init()
{

	// Init attributes
	m_trackId = NULL; // String
	m_trackId_Exist = false;
	m_trackName = NULL; // String
	m_trackName_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;



// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtMyPropInit.h

}

void EbuCorevideoFormatType_videoTrack_LocalType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_trackId); // String
	XMLString::release(&m_trackName); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
}

void EbuCorevideoFormatType_videoTrack_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use videoFormatType_videoTrack_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IvideoFormatType_videoTrack_LocalType
	const Dc1Ptr< EbuCorevideoFormatType_videoTrack_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_trackId); // String
	if (tmp->ExisttrackId())
	{
		this->SettrackId(XMLString::replicate(tmp->GettrackId()));
	}
	else
	{
		InvalidatetrackId();
	}
	}
	{
	XMLString::release(&m_trackName); // String
	if (tmp->ExisttrackName())
	{
		this->SettrackName(XMLString::replicate(tmp->GettrackName()));
	}
	else
	{
		InvalidatetrackName();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
}

Dc1NodePtr EbuCorevideoFormatType_videoTrack_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorevideoFormatType_videoTrack_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettrackId() const
{
	return m_trackId;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttrackId() const
{
	return m_trackId_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettrackId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettrackId().");
	}
	m_trackId = item;
	m_trackId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetrackId()
{
	m_trackId_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettrackName() const
{
	return m_trackName;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttrackName() const
{
	return m_trackName_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettrackName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettrackName().");
	}
	m_trackName = item;
	m_trackName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetrackName()
{
	m_trackName_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorevideoFormatType_videoTrack_LocalType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorevideoFormatType_videoTrack_LocalType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorevideoFormatType_videoTrack_LocalType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorevideoFormatType_videoTrack_LocalType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}

Dc1NodeEnum EbuCorevideoFormatType_videoTrack_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * EbuCorevideoFormatType_videoTrack_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorevideoFormatType_videoTrack_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void EbuCorevideoFormatType_videoTrack_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("videoFormatType_videoTrack_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_trackId_Exist)
	{
	// String
	if(m_trackId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("trackId"), m_trackId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_trackName_Exist)
	{
	// String
	if(m_trackName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("trackName"), m_trackName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCorevideoFormatType_videoTrack_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("trackId")))
	{
		// Deserialize string type
		this->SettrackId(Dc1Convert::TextToString(parent->getAttribute(X("trackId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("trackName")))
	{
		// Deserialize string type
		this->SettrackName(Dc1Convert::TextToString(parent->getAttribute(X("trackName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorevideoFormatType_videoTrack_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCorevideoFormatType_videoTrack_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorevideoFormatType_videoTrack_LocalType_ExtMethodImpl.h


