
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtImplInclude.h


#include "EbuCoredimensionType.h"
#include "EbuCoreaspectRatioType.h"
#include "EbuCoreimageFormatType_imageEncoding_LocalType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCoreimageFormatType_comment_CollectionType.h"
#include "EbuCoreimageFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorecomment_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:comment

#include <assert.h>
IEbuCoreimageFormatType::IEbuCoreimageFormatType()
{

// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtPropInit.h

}

IEbuCoreimageFormatType::~IEbuCoreimageFormatType()
{
// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtPropCleanup.h

}

EbuCoreimageFormatType::EbuCoreimageFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreimageFormatType::~EbuCoreimageFormatType()
{
	Cleanup();
}

void EbuCoreimageFormatType::Init()
{

	// Init attributes
	m_imageFormatId = NULL; // String
	m_imageFormatId_Exist = false;
	m_imageFormatVersionId = NULL; // String
	m_imageFormatVersionId_Exist = false;
	m_imageFormatName = NULL; // String
	m_imageFormatName_Exist = false;
	m_imageFormatDefinition = NULL; // String
	m_imageFormatDefinition_Exist = false;
	m_imagePresenceFlag = false; // Value
	m_imagePresenceFlag_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_regionDelimX = (0); // Value

	m_regionDelimX_Exist = false;
	m_regionDelimY = (0); // Value

	m_regionDelimY_Exist = false;
	m_width = EbuCoredimensionPtr(); // Class with content 
	m_width_Exist = false;
	m_height = EbuCoredimensionPtr(); // Class with content 
	m_height_Exist = false;
	m_orientation = EbuCoreimageFormatType_orientation_LocalType::UninitializedEnumeration; // Enumeration
	m_orientation_Exist = false;
	m_aspectRatio = EbuCoreaspectRatioPtr(); // Class
	m_aspectRatio_Exist = false;
	m_imageEncoding = EbuCoreimageFormatType_imageEncoding_LocalPtr(); // Class
	m_imageEncoding_Exist = false;
	m_technicalAttributes = EbuCoretechnicalAttributesPtr(); // Class
	m_comment = EbuCoreimageFormatType_comment_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtMyPropInit.h

}

void EbuCoreimageFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_imageFormatId); // String
	XMLString::release(&m_imageFormatVersionId); // String
	XMLString::release(&m_imageFormatName); // String
	XMLString::release(&m_imageFormatDefinition); // String
	// Dc1Factory::DeleteObject(m_width);
	// Dc1Factory::DeleteObject(m_height);
	// Dc1Factory::DeleteObject(m_aspectRatio);
	// Dc1Factory::DeleteObject(m_imageEncoding);
	// Dc1Factory::DeleteObject(m_technicalAttributes);
	// Dc1Factory::DeleteObject(m_comment);
}

void EbuCoreimageFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use imageFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IimageFormatType
	const Dc1Ptr< EbuCoreimageFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_imageFormatId); // String
	if (tmp->ExistimageFormatId())
	{
		this->SetimageFormatId(XMLString::replicate(tmp->GetimageFormatId()));
	}
	else
	{
		InvalidateimageFormatId();
	}
	}
	{
	XMLString::release(&m_imageFormatVersionId); // String
	if (tmp->ExistimageFormatVersionId())
	{
		this->SetimageFormatVersionId(XMLString::replicate(tmp->GetimageFormatVersionId()));
	}
	else
	{
		InvalidateimageFormatVersionId();
	}
	}
	{
	XMLString::release(&m_imageFormatName); // String
	if (tmp->ExistimageFormatName())
	{
		this->SetimageFormatName(XMLString::replicate(tmp->GetimageFormatName()));
	}
	else
	{
		InvalidateimageFormatName();
	}
	}
	{
	XMLString::release(&m_imageFormatDefinition); // String
	if (tmp->ExistimageFormatDefinition())
	{
		this->SetimageFormatDefinition(XMLString::replicate(tmp->GetimageFormatDefinition()));
	}
	else
	{
		InvalidateimageFormatDefinition();
	}
	}
	{
	if (tmp->ExistimagePresenceFlag())
	{
		this->SetimagePresenceFlag(tmp->GetimagePresenceFlag());
	}
	else
	{
		InvalidateimagePresenceFlag();
	}
	}
	if (tmp->IsValidregionDelimX())
	{
		this->SetregionDelimX(tmp->GetregionDelimX());
	}
	else
	{
		InvalidateregionDelimX();
	}
	if (tmp->IsValidregionDelimY())
	{
		this->SetregionDelimY(tmp->GetregionDelimY());
	}
	else
	{
		InvalidateregionDelimY();
	}
	if (tmp->IsValidwidth())
	{
		// Dc1Factory::DeleteObject(m_width);
		this->Setwidth(Dc1Factory::CloneObject(tmp->Getwidth()));
	}
	else
	{
		Invalidatewidth();
	}
	if (tmp->IsValidheight())
	{
		// Dc1Factory::DeleteObject(m_height);
		this->Setheight(Dc1Factory::CloneObject(tmp->Getheight()));
	}
	else
	{
		Invalidateheight();
	}
	if (tmp->IsValidorientation())
	{
		this->Setorientation(tmp->Getorientation());
	}
	else
	{
		Invalidateorientation();
	}
	if (tmp->IsValidaspectRatio())
	{
		// Dc1Factory::DeleteObject(m_aspectRatio);
		this->SetaspectRatio(Dc1Factory::CloneObject(tmp->GetaspectRatio()));
	}
	else
	{
		InvalidateaspectRatio();
	}
	if (tmp->IsValidimageEncoding())
	{
		// Dc1Factory::DeleteObject(m_imageEncoding);
		this->SetimageEncoding(Dc1Factory::CloneObject(tmp->GetimageEncoding()));
	}
	else
	{
		InvalidateimageEncoding();
	}
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		this->SettechnicalAttributes(Dc1Factory::CloneObject(tmp->GettechnicalAttributes()));
		// Dc1Factory::DeleteObject(m_comment);
		this->Setcomment(Dc1Factory::CloneObject(tmp->Getcomment()));
}

Dc1NodePtr EbuCoreimageFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreimageFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreimageFormatType::GetimageFormatId() const
{
	return m_imageFormatId;
}

bool EbuCoreimageFormatType::ExistimageFormatId() const
{
	return m_imageFormatId_Exist;
}
void EbuCoreimageFormatType::SetimageFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimageFormatId().");
	}
	m_imageFormatId = item;
	m_imageFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimageFormatId()
{
	m_imageFormatId_Exist = false;
}
XMLCh * EbuCoreimageFormatType::GetimageFormatVersionId() const
{
	return m_imageFormatVersionId;
}

bool EbuCoreimageFormatType::ExistimageFormatVersionId() const
{
	return m_imageFormatVersionId_Exist;
}
void EbuCoreimageFormatType::SetimageFormatVersionId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimageFormatVersionId().");
	}
	m_imageFormatVersionId = item;
	m_imageFormatVersionId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimageFormatVersionId()
{
	m_imageFormatVersionId_Exist = false;
}
XMLCh * EbuCoreimageFormatType::GetimageFormatName() const
{
	return m_imageFormatName;
}

bool EbuCoreimageFormatType::ExistimageFormatName() const
{
	return m_imageFormatName_Exist;
}
void EbuCoreimageFormatType::SetimageFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimageFormatName().");
	}
	m_imageFormatName = item;
	m_imageFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimageFormatName()
{
	m_imageFormatName_Exist = false;
}
XMLCh * EbuCoreimageFormatType::GetimageFormatDefinition() const
{
	return m_imageFormatDefinition;
}

bool EbuCoreimageFormatType::ExistimageFormatDefinition() const
{
	return m_imageFormatDefinition_Exist;
}
void EbuCoreimageFormatType::SetimageFormatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimageFormatDefinition().");
	}
	m_imageFormatDefinition = item;
	m_imageFormatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimageFormatDefinition()
{
	m_imageFormatDefinition_Exist = false;
}
bool EbuCoreimageFormatType::GetimagePresenceFlag() const
{
	return m_imagePresenceFlag;
}

bool EbuCoreimageFormatType::ExistimagePresenceFlag() const
{
	return m_imagePresenceFlag_Exist;
}
void EbuCoreimageFormatType::SetimagePresenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimagePresenceFlag().");
	}
	m_imagePresenceFlag = item;
	m_imagePresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimagePresenceFlag()
{
	m_imagePresenceFlag_Exist = false;
}
unsigned EbuCoreimageFormatType::GetregionDelimX() const
{
		return m_regionDelimX;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidregionDelimX() const
{
	return m_regionDelimX_Exist;
}

unsigned EbuCoreimageFormatType::GetregionDelimY() const
{
		return m_regionDelimY;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidregionDelimY() const
{
	return m_regionDelimY_Exist;
}

EbuCoredimensionPtr EbuCoreimageFormatType::Getwidth() const
{
		return m_width;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidwidth() const
{
	return m_width_Exist;
}

EbuCoredimensionPtr EbuCoreimageFormatType::Getheight() const
{
		return m_height;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidheight() const
{
	return m_height_Exist;
}

EbuCoreimageFormatType_orientation_LocalType::Enumeration EbuCoreimageFormatType::Getorientation() const
{
		return m_orientation;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidorientation() const
{
	return m_orientation_Exist;
}

EbuCoreaspectRatioPtr EbuCoreimageFormatType::GetaspectRatio() const
{
		return m_aspectRatio;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidaspectRatio() const
{
	return m_aspectRatio_Exist;
}

EbuCoreimageFormatType_imageEncoding_LocalPtr EbuCoreimageFormatType::GetimageEncoding() const
{
		return m_imageEncoding;
}

// Element is optional
bool EbuCoreimageFormatType::IsValidimageEncoding() const
{
	return m_imageEncoding_Exist;
}

EbuCoretechnicalAttributesPtr EbuCoreimageFormatType::GettechnicalAttributes() const
{
		return m_technicalAttributes;
}

EbuCoreimageFormatType_comment_CollectionPtr EbuCoreimageFormatType::Getcomment() const
{
		return m_comment;
}

void EbuCoreimageFormatType::SetregionDelimX(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetregionDelimX().");
	}
	if (m_regionDelimX != item || m_regionDelimX_Exist == false)
	{
		m_regionDelimX = item;
		m_regionDelimX_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateregionDelimX()
{
	m_regionDelimX_Exist = false;
}
void EbuCoreimageFormatType::SetregionDelimY(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetregionDelimY().");
	}
	if (m_regionDelimY != item || m_regionDelimY_Exist == false)
	{
		m_regionDelimY = item;
		m_regionDelimY_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateregionDelimY()
{
	m_regionDelimY_Exist = false;
}
void EbuCoreimageFormatType::Setwidth(const EbuCoredimensionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::Setwidth().");
	}
	if (m_width != item || m_width_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_width);
		m_width = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_width) m_width->SetParent(m_myself.getPointer());
		if (m_width && m_width->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dimensionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_width->UseTypeAttribute = true;
		}
		m_width_Exist = true;
		if(m_width != EbuCoredimensionPtr())
		{
			m_width->SetContentName(XMLString::transcode("width"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::Invalidatewidth()
{
	m_width_Exist = false;
}
void EbuCoreimageFormatType::Setheight(const EbuCoredimensionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::Setheight().");
	}
	if (m_height != item || m_height_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_height);
		m_height = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_height) m_height->SetParent(m_myself.getPointer());
		if (m_height && m_height->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dimensionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_height->UseTypeAttribute = true;
		}
		m_height_Exist = true;
		if(m_height != EbuCoredimensionPtr())
		{
			m_height->SetContentName(XMLString::transcode("height"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::Invalidateheight()
{
	m_height_Exist = false;
}
void EbuCoreimageFormatType::Setorientation(EbuCoreimageFormatType_orientation_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::Setorientation().");
	}
	if (m_orientation != item || m_orientation_Exist == false)
	{
		// nothing to free here, hopefully
		m_orientation = item;
		m_orientation_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::Invalidateorientation()
{
	m_orientation_Exist = false;
}
void EbuCoreimageFormatType::SetaspectRatio(const EbuCoreaspectRatioPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetaspectRatio().");
	}
	if (m_aspectRatio != item || m_aspectRatio_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_aspectRatio);
		m_aspectRatio = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_aspectRatio) m_aspectRatio->SetParent(m_myself.getPointer());
		if (m_aspectRatio && m_aspectRatio->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:aspectRatioType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_aspectRatio->UseTypeAttribute = true;
		}
		m_aspectRatio_Exist = true;
		if(m_aspectRatio != EbuCoreaspectRatioPtr())
		{
			m_aspectRatio->SetContentName(XMLString::transcode("aspectRatio"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateaspectRatio()
{
	m_aspectRatio_Exist = false;
}
void EbuCoreimageFormatType::SetimageEncoding(const EbuCoreimageFormatType_imageEncoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SetimageEncoding().");
	}
	if (m_imageEncoding != item || m_imageEncoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_imageEncoding);
		m_imageEncoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_imageEncoding) m_imageEncoding->SetParent(m_myself.getPointer());
		if (m_imageEncoding && m_imageEncoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_imageEncoding_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_imageEncoding->UseTypeAttribute = true;
		}
		m_imageEncoding_Exist = true;
		if(m_imageEncoding != EbuCoreimageFormatType_imageEncoding_LocalPtr())
		{
			m_imageEncoding->SetContentName(XMLString::transcode("imageEncoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::InvalidateimageEncoding()
{
	m_imageEncoding_Exist = false;
}
void EbuCoreimageFormatType::SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::SettechnicalAttributes().");
	}
	if (m_technicalAttributes != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		m_technicalAttributes = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributes) m_technicalAttributes->SetParent(m_myself.getPointer());
		if (m_technicalAttributes && m_technicalAttributes->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_technicalAttributes->UseTypeAttribute = true;
		}
		if(m_technicalAttributes != EbuCoretechnicalAttributesPtr())
		{
			m_technicalAttributes->SetContentName(XMLString::transcode("technicalAttributes"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreimageFormatType::Setcomment(const EbuCoreimageFormatType_comment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreimageFormatType::Setcomment().");
	}
	if (m_comment != item)
	{
		// Dc1Factory::DeleteObject(m_comment);
		m_comment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_comment) m_comment->SetParent(m_myself.getPointer());
		if(m_comment != EbuCoreimageFormatType_comment_CollectionPtr())
		{
			m_comment->SetContentName(XMLString::transcode("comment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreimageFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("imageFormatId")) == 0)
	{
		// imageFormatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("imageFormatVersionId")) == 0)
	{
		// imageFormatVersionId is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("imageFormatName")) == 0)
	{
		// imageFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("imageFormatDefinition")) == 0)
	{
		// imageFormatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("imagePresenceFlag")) == 0)
	{
		// imagePresenceFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("width")) == 0)
	{
		// width is simple element dimensionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getwidth()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dimensionType")))) != empty)
			{
				// Is type allowed
				EbuCoredimensionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setwidth(p, client);
					if((p = Getwidth()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("height")) == 0)
	{
		// height is simple element dimensionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getheight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dimensionType")))) != empty)
			{
				// Is type allowed
				EbuCoredimensionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setheight(p, client);
					if((p = Getheight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("aspectRatio")) == 0)
	{
		// aspectRatio is simple element aspectRatioType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaspectRatio()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:aspectRatioType")))) != empty)
			{
				// Is type allowed
				EbuCoreaspectRatioPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaspectRatio(p, client);
					if((p = GetaspectRatio()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("imageEncoding")) == 0)
	{
		// imageEncoding is simple element imageFormatType_imageEncoding_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetimageEncoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_imageEncoding_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreimageFormatType_imageEncoding_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetimageEncoding(p, client);
					if((p = GetimageEncoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributes")) == 0)
	{
		// technicalAttributes is simple element technicalAttributes
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettechnicalAttributes()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettechnicalAttributes(p, client);
					if((p = GettechnicalAttributes()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("comment")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:comment is item of type comment_LocalType
		// in element collection imageFormatType_comment_CollectionType
		
		context->Found = true;
		EbuCoreimageFormatType_comment_CollectionPtr coll = Getcomment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateimageFormatType_comment_CollectionType; // FTT, check this
				Setcomment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecomment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for imageFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "imageFormatType");
	}
	return result;
}

XMLCh * EbuCoreimageFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreimageFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreimageFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("imageFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_imageFormatId_Exist)
	{
	// String
	if(m_imageFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("imageFormatId"), m_imageFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_imageFormatVersionId_Exist)
	{
	// String
	if(m_imageFormatVersionId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("imageFormatVersionId"), m_imageFormatVersionId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_imageFormatName_Exist)
	{
	// String
	if(m_imageFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("imageFormatName"), m_imageFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_imageFormatDefinition_Exist)
	{
	// String
	if(m_imageFormatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("imageFormatDefinition"), m_imageFormatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_imagePresenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_imagePresenceFlag);
		element->setAttributeNS(X(""), X("imagePresenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_regionDelimX_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_regionDelimX);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("regionDelimX"), true);
		XMLString::release(&tmp);
	}
	if(m_regionDelimY_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_regionDelimY);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("regionDelimY"), true);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_width != EbuCoredimensionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_width->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("width"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_width->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_height != EbuCoredimensionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_height->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("height"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_height->Serialize(doc, element, &elem);
	}
	if(m_orientation != EbuCoreimageFormatType_orientation_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = EbuCoreimageFormatType_orientation_LocalType::ToText(m_orientation);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("orientation"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_aspectRatio != EbuCoreaspectRatioPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_aspectRatio->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("aspectRatio"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_aspectRatio->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_imageEncoding != EbuCoreimageFormatType_imageEncoding_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_imageEncoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("imageEncoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_imageEncoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_technicalAttributes != EbuCoretechnicalAttributesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_technicalAttributes->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_technicalAttributes->Serialize(doc, element, &elem);
	}
	if (m_comment != EbuCoreimageFormatType_comment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_comment->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreimageFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("imageFormatId")))
	{
		// Deserialize string type
		this->SetimageFormatId(Dc1Convert::TextToString(parent->getAttribute(X("imageFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("imageFormatVersionId")))
	{
		// Deserialize string type
		this->SetimageFormatVersionId(Dc1Convert::TextToString(parent->getAttribute(X("imageFormatVersionId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("imageFormatName")))
	{
		// Deserialize string type
		this->SetimageFormatName(Dc1Convert::TextToString(parent->getAttribute(X("imageFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("imageFormatDefinition")))
	{
		// Deserialize string type
		this->SetimageFormatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("imageFormatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("imagePresenceFlag")))
	{
		// deserialize value type
		this->SetimagePresenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("imagePresenceFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("regionDelimX"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetregionDelimX(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("regionDelimY"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetregionDelimY(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("width"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("width")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedimensionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setwidth(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("height"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("height")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedimensionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setheight(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("orientation"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("orientation")) == 0))
		{
			EbuCoreimageFormatType_orientation_LocalType::Enumeration tmp = EbuCoreimageFormatType_orientation_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == EbuCoreimageFormatType_orientation_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->Setorientation(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("aspectRatio"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("aspectRatio")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaspectRatioType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaspectRatio(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("imageEncoding"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("imageEncoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateimageFormatType_imageEncoding_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetimageEncoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("technicalAttributes"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("technicalAttributes")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetechnicalAttributes; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettechnicalAttributes(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("comment"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("comment")) == 0))
		{
			// Deserialize factory type
			EbuCoreimageFormatType_comment_CollectionPtr tmp = CreateimageFormatType_comment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcomment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreimageFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("width")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedimensionType; // FTT, check this
	}
	this->Setwidth(child);
  }
  if (XMLString::compareString(elementname, X("height")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedimensionType; // FTT, check this
	}
	this->Setheight(child);
  }
  if (XMLString::compareString(elementname, X("aspectRatio")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaspectRatioType; // FTT, check this
	}
	this->SetaspectRatio(child);
  }
  if (XMLString::compareString(elementname, X("imageEncoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateimageFormatType_imageEncoding_LocalType; // FTT, check this
	}
	this->SetimageEncoding(child);
  }
  if (XMLString::compareString(elementname, X("technicalAttributes")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetechnicalAttributes; // FTT, check this
	}
	this->SettechnicalAttributes(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("comment")) == 0))
  {
	EbuCoreimageFormatType_comment_CollectionPtr tmp = CreateimageFormatType_comment_CollectionType; // FTT, check this
	this->Setcomment(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreimageFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getwidth() != Dc1NodePtr())
		result.Insert(Getwidth());
	if (Getheight() != Dc1NodePtr())
		result.Insert(Getheight());
	if (GetaspectRatio() != Dc1NodePtr())
		result.Insert(GetaspectRatio());
	if (GetimageEncoding() != Dc1NodePtr())
		result.Insert(GetimageEncoding());
	if (GettechnicalAttributes() != Dc1NodePtr())
		result.Insert(GettechnicalAttributes());
	if (Getcomment() != Dc1NodePtr())
		result.Insert(Getcomment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreimageFormatType_ExtMethodImpl.h


