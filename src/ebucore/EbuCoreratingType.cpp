
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreratingType_ExtImplInclude.h


#include "EbuCoreratingType_ratingValue_CollectionType.h"
#include "EbuCoreratingType_ratingLink_CollectionType.h"
#include "EbuCoreratingType_ratingScaleMaxValue_CollectionType.h"
#include "EbuCoreratingType_ratingScaleMinValue_CollectionType.h"
#include "EbuCoreentityType.h"
#include "EbuCoreratingType_ratingRegion_CollectionType.h"
#include "EbuCoreratingType_ratingExclusionRegion_CollectionType.h"
#include "EbuCoreratingType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreelementType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:ratingValue
#include "EbuCoreregionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:ratingRegion

#include <assert.h>
IEbuCoreratingType::IEbuCoreratingType()
{

// no includefile for extension defined 
// file EbuCoreratingType_ExtPropInit.h

}

IEbuCoreratingType::~IEbuCoreratingType()
{
// no includefile for extension defined 
// file EbuCoreratingType_ExtPropCleanup.h

}

EbuCoreratingType::EbuCoreratingType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreratingType::~EbuCoreratingType()
{
	Cleanup();
}

void EbuCoreratingType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;
	m_reason = NULL; // String
	m_reason_Exist = false;
	m_linkToLogo = NULL; // String
	m_linkToLogo_Exist = false;
	m_notRated = false; // Value
	m_notRated_Exist = false;
	m_adultContent = false; // Value
	m_adultContent_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_ratingValue = EbuCoreratingType_ratingValue_CollectionPtr(); // Collection
	m_ratingLink = EbuCoreratingType_ratingLink_CollectionPtr(); // Collection
	m_ratingScaleMaxValue = EbuCoreratingType_ratingScaleMaxValue_CollectionPtr(); // Collection
	m_ratingScaleMinValue = EbuCoreratingType_ratingScaleMinValue_CollectionPtr(); // Collection
	m_ratingProvider = EbuCoreentityPtr(); // Class
	m_ratingProvider_Exist = false;
	m_ratingRegion = EbuCoreratingType_ratingRegion_CollectionPtr(); // Collection
	m_ratingExclusionRegion = EbuCoreratingType_ratingExclusionRegion_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreratingType_ExtMyPropInit.h

}

void EbuCoreratingType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreratingType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
	XMLString::release(&m_reason); // String
	XMLString::release(&m_linkToLogo); // String
	// Dc1Factory::DeleteObject(m_ratingValue);
	// Dc1Factory::DeleteObject(m_ratingLink);
	// Dc1Factory::DeleteObject(m_ratingScaleMaxValue);
	// Dc1Factory::DeleteObject(m_ratingScaleMinValue);
	// Dc1Factory::DeleteObject(m_ratingProvider);
	// Dc1Factory::DeleteObject(m_ratingRegion);
	// Dc1Factory::DeleteObject(m_ratingExclusionRegion);
}

void EbuCoreratingType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ratingTypePtr, since we
	// might need GetBase(), which isn't defined in IratingType
	const Dc1Ptr< EbuCoreratingType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
	{
	XMLString::release(&m_reason); // String
	if (tmp->Existreason())
	{
		this->Setreason(XMLString::replicate(tmp->Getreason()));
	}
	else
	{
		Invalidatereason();
	}
	}
	{
	XMLString::release(&m_linkToLogo); // String
	if (tmp->ExistlinkToLogo())
	{
		this->SetlinkToLogo(XMLString::replicate(tmp->GetlinkToLogo()));
	}
	else
	{
		InvalidatelinkToLogo();
	}
	}
	{
	if (tmp->ExistnotRated())
	{
		this->SetnotRated(tmp->GetnotRated());
	}
	else
	{
		InvalidatenotRated();
	}
	}
	{
	if (tmp->ExistadultContent())
	{
		this->SetadultContent(tmp->GetadultContent());
	}
	else
	{
		InvalidateadultContent();
	}
	}
		// Dc1Factory::DeleteObject(m_ratingValue);
		this->SetratingValue(Dc1Factory::CloneObject(tmp->GetratingValue()));
		// Dc1Factory::DeleteObject(m_ratingLink);
		this->SetratingLink(Dc1Factory::CloneObject(tmp->GetratingLink()));
		// Dc1Factory::DeleteObject(m_ratingScaleMaxValue);
		this->SetratingScaleMaxValue(Dc1Factory::CloneObject(tmp->GetratingScaleMaxValue()));
		// Dc1Factory::DeleteObject(m_ratingScaleMinValue);
		this->SetratingScaleMinValue(Dc1Factory::CloneObject(tmp->GetratingScaleMinValue()));
	if (tmp->IsValidratingProvider())
	{
		// Dc1Factory::DeleteObject(m_ratingProvider);
		this->SetratingProvider(Dc1Factory::CloneObject(tmp->GetratingProvider()));
	}
	else
	{
		InvalidateratingProvider();
	}
		// Dc1Factory::DeleteObject(m_ratingRegion);
		this->SetratingRegion(Dc1Factory::CloneObject(tmp->GetratingRegion()));
		// Dc1Factory::DeleteObject(m_ratingExclusionRegion);
		this->SetratingExclusionRegion(Dc1Factory::CloneObject(tmp->GetratingExclusionRegion()));
}

Dc1NodePtr EbuCoreratingType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreratingType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreratingType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreratingType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreratingType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreratingType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreratingType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreratingType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreratingType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreratingType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreratingType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreratingType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreratingType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreratingType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreratingType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreratingType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreratingType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoreratingType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCoreratingType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCoreratingType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCoreratingType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCoreratingType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCoreratingType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCoreratingType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCoreratingType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCoreratingType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCoreratingType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCoreratingType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCoreratingType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCoreratingType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCoreratingType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCoreratingType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
XMLCh * EbuCoreratingType::Getreason() const
{
	return m_reason;
}

bool EbuCoreratingType::Existreason() const
{
	return m_reason_Exist;
}
void EbuCoreratingType::Setreason(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::Setreason().");
	}
	m_reason = item;
	m_reason_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::Invalidatereason()
{
	m_reason_Exist = false;
}
XMLCh * EbuCoreratingType::GetlinkToLogo() const
{
	return m_linkToLogo;
}

bool EbuCoreratingType::ExistlinkToLogo() const
{
	return m_linkToLogo_Exist;
}
void EbuCoreratingType::SetlinkToLogo(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetlinkToLogo().");
	}
	m_linkToLogo = item;
	m_linkToLogo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatelinkToLogo()
{
	m_linkToLogo_Exist = false;
}
bool EbuCoreratingType::GetnotRated() const
{
	return m_notRated;
}

bool EbuCoreratingType::ExistnotRated() const
{
	return m_notRated_Exist;
}
void EbuCoreratingType::SetnotRated(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetnotRated().");
	}
	m_notRated = item;
	m_notRated_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidatenotRated()
{
	m_notRated_Exist = false;
}
bool EbuCoreratingType::GetadultContent() const
{
	return m_adultContent;
}

bool EbuCoreratingType::ExistadultContent() const
{
	return m_adultContent_Exist;
}
void EbuCoreratingType::SetadultContent(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetadultContent().");
	}
	m_adultContent = item;
	m_adultContent_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateadultContent()
{
	m_adultContent_Exist = false;
}
EbuCoreratingType_ratingValue_CollectionPtr EbuCoreratingType::GetratingValue() const
{
		return m_ratingValue;
}

EbuCoreratingType_ratingLink_CollectionPtr EbuCoreratingType::GetratingLink() const
{
		return m_ratingLink;
}

EbuCoreratingType_ratingScaleMaxValue_CollectionPtr EbuCoreratingType::GetratingScaleMaxValue() const
{
		return m_ratingScaleMaxValue;
}

EbuCoreratingType_ratingScaleMinValue_CollectionPtr EbuCoreratingType::GetratingScaleMinValue() const
{
		return m_ratingScaleMinValue;
}

EbuCoreentityPtr EbuCoreratingType::GetratingProvider() const
{
		return m_ratingProvider;
}

// Element is optional
bool EbuCoreratingType::IsValidratingProvider() const
{
	return m_ratingProvider_Exist;
}

EbuCoreratingType_ratingRegion_CollectionPtr EbuCoreratingType::GetratingRegion() const
{
		return m_ratingRegion;
}

EbuCoreratingType_ratingExclusionRegion_CollectionPtr EbuCoreratingType::GetratingExclusionRegion() const
{
		return m_ratingExclusionRegion;
}

void EbuCoreratingType::SetratingValue(const EbuCoreratingType_ratingValue_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingValue().");
	}
	if (m_ratingValue != item)
	{
		// Dc1Factory::DeleteObject(m_ratingValue);
		m_ratingValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingValue) m_ratingValue->SetParent(m_myself.getPointer());
		if(m_ratingValue != EbuCoreratingType_ratingValue_CollectionPtr())
		{
			m_ratingValue->SetContentName(XMLString::transcode("ratingValue"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::SetratingLink(const EbuCoreratingType_ratingLink_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingLink().");
	}
	if (m_ratingLink != item)
	{
		// Dc1Factory::DeleteObject(m_ratingLink);
		m_ratingLink = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingLink) m_ratingLink->SetParent(m_myself.getPointer());
		if(m_ratingLink != EbuCoreratingType_ratingLink_CollectionPtr())
		{
			m_ratingLink->SetContentName(XMLString::transcode("ratingLink"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::SetratingScaleMaxValue(const EbuCoreratingType_ratingScaleMaxValue_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingScaleMaxValue().");
	}
	if (m_ratingScaleMaxValue != item)
	{
		// Dc1Factory::DeleteObject(m_ratingScaleMaxValue);
		m_ratingScaleMaxValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingScaleMaxValue) m_ratingScaleMaxValue->SetParent(m_myself.getPointer());
		if(m_ratingScaleMaxValue != EbuCoreratingType_ratingScaleMaxValue_CollectionPtr())
		{
			m_ratingScaleMaxValue->SetContentName(XMLString::transcode("ratingScaleMaxValue"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::SetratingScaleMinValue(const EbuCoreratingType_ratingScaleMinValue_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingScaleMinValue().");
	}
	if (m_ratingScaleMinValue != item)
	{
		// Dc1Factory::DeleteObject(m_ratingScaleMinValue);
		m_ratingScaleMinValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingScaleMinValue) m_ratingScaleMinValue->SetParent(m_myself.getPointer());
		if(m_ratingScaleMinValue != EbuCoreratingType_ratingScaleMinValue_CollectionPtr())
		{
			m_ratingScaleMinValue->SetContentName(XMLString::transcode("ratingScaleMinValue"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::SetratingProvider(const EbuCoreentityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingProvider().");
	}
	if (m_ratingProvider != item || m_ratingProvider_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ratingProvider);
		m_ratingProvider = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingProvider) m_ratingProvider->SetParent(m_myself.getPointer());
		if (m_ratingProvider && m_ratingProvider->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ratingProvider->UseTypeAttribute = true;
		}
		m_ratingProvider_Exist = true;
		if(m_ratingProvider != EbuCoreentityPtr())
		{
			m_ratingProvider->SetContentName(XMLString::transcode("ratingProvider"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::InvalidateratingProvider()
{
	m_ratingProvider_Exist = false;
}
void EbuCoreratingType::SetratingRegion(const EbuCoreratingType_ratingRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingRegion().");
	}
	if (m_ratingRegion != item)
	{
		// Dc1Factory::DeleteObject(m_ratingRegion);
		m_ratingRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingRegion) m_ratingRegion->SetParent(m_myself.getPointer());
		if(m_ratingRegion != EbuCoreratingType_ratingRegion_CollectionPtr())
		{
			m_ratingRegion->SetContentName(XMLString::transcode("ratingRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreratingType::SetratingExclusionRegion(const EbuCoreratingType_ratingExclusionRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreratingType::SetratingExclusionRegion().");
	}
	if (m_ratingExclusionRegion != item)
	{
		// Dc1Factory::DeleteObject(m_ratingExclusionRegion);
		m_ratingExclusionRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ratingExclusionRegion) m_ratingExclusionRegion->SetParent(m_myself.getPointer());
		if(m_ratingExclusionRegion != EbuCoreratingType_ratingExclusionRegion_CollectionPtr())
		{
			m_ratingExclusionRegion->SetContentName(XMLString::transcode("ratingExclusionRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreratingType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  14, 14 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLabel")) == 0)
	{
		// formatLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatDefinition")) == 0)
	{
		// formatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLink")) == 0)
	{
		// formatLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatSource")) == 0)
	{
		// formatSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLanguage")) == 0)
	{
		// formatLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("reason")) == 0)
	{
		// reason is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("linkToLogo")) == 0)
	{
		// linkToLogo is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("notRated")) == 0)
	{
		// notRated is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("adultContent")) == 0)
	{
		// adultContent is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingValue")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ratingValue is item of type elementType
		// in element collection ratingType_ratingValue_CollectionType
		
		context->Found = true;
		EbuCoreratingType_ratingValue_CollectionPtr coll = GetratingValue();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateratingType_ratingValue_CollectionType; // FTT, check this
				SetratingValue(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingLink")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingScaleMaxValue")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ratingScaleMaxValue is item of type elementType
		// in element collection ratingType_ratingScaleMaxValue_CollectionType
		
		context->Found = true;
		EbuCoreratingType_ratingScaleMaxValue_CollectionPtr coll = GetratingScaleMaxValue();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateratingType_ratingScaleMaxValue_CollectionType; // FTT, check this
				SetratingScaleMaxValue(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingScaleMinValue")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ratingScaleMinValue is item of type elementType
		// in element collection ratingType_ratingScaleMinValue_CollectionType
		
		context->Found = true;
		EbuCoreratingType_ratingScaleMinValue_CollectionPtr coll = GetratingScaleMinValue();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateratingType_ratingScaleMinValue_CollectionType; // FTT, check this
				SetratingScaleMinValue(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingProvider")) == 0)
	{
		// ratingProvider is simple element entityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetratingProvider()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetratingProvider(p, client);
					if((p = GetratingProvider()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingRegion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ratingRegion is item of type regionType
		// in element collection ratingType_ratingRegion_CollectionType
		
		context->Found = true;
		EbuCoreratingType_ratingRegion_CollectionPtr coll = GetratingRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateratingType_ratingRegion_CollectionType; // FTT, check this
				SetratingRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))) != empty)
			{
				// Is type allowed
				EbuCoreregionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ratingExclusionRegion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ratingExclusionRegion is item of type regionType
		// in element collection ratingType_ratingExclusionRegion_CollectionType
		
		context->Found = true;
		EbuCoreratingType_ratingExclusionRegion_CollectionPtr coll = GetratingExclusionRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateratingType_ratingExclusionRegion_CollectionType; // FTT, check this
				SetratingExclusionRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))) != empty)
			{
				// Is type allowed
				EbuCoreregionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ratingType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ratingType");
	}
	return result;
}

XMLCh * EbuCoreratingType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreratingType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreratingType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("ratingType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_reason_Exist)
	{
	// String
	if(m_reason != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("reason"), m_reason);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_linkToLogo_Exist)
	{
	// String
	if(m_linkToLogo != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("linkToLogo"), m_linkToLogo);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_notRated_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_notRated);
		element->setAttributeNS(X(""), X("notRated"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_adultContent_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_adultContent);
		element->setAttributeNS(X(""), X("adultContent"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_ratingValue != EbuCoreratingType_ratingValue_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingValue->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ratingLink != EbuCoreratingType_ratingLink_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingLink->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ratingScaleMaxValue != EbuCoreratingType_ratingScaleMaxValue_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingScaleMaxValue->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ratingScaleMinValue != EbuCoreratingType_ratingScaleMinValue_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingScaleMinValue->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_ratingProvider != EbuCoreentityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ratingProvider->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("ratingProvider"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ratingProvider->Serialize(doc, element, &elem);
	}
	if (m_ratingRegion != EbuCoreratingType_ratingRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ratingExclusionRegion != EbuCoreratingType_ratingExclusionRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ratingExclusionRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreratingType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("reason")))
	{
		// Deserialize string type
		this->Setreason(Dc1Convert::TextToString(parent->getAttribute(X("reason"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("linkToLogo")))
	{
		// Deserialize string type
		this->SetlinkToLogo(Dc1Convert::TextToString(parent->getAttribute(X("linkToLogo"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("notRated")))
	{
		// deserialize value type
		this->SetnotRated(Dc1Convert::TextToBool(parent->getAttribute(X("notRated"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("adultContent")))
	{
		// deserialize value type
		this->SetadultContent(Dc1Convert::TextToBool(parent->getAttribute(X("adultContent"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingValue"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingValue")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingValue_CollectionPtr tmp = CreateratingType_ratingValue_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingValue(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingLink"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingLink")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingLink_CollectionPtr tmp = CreateratingType_ratingLink_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingLink(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingScaleMaxValue"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingScaleMaxValue")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingScaleMaxValue_CollectionPtr tmp = CreateratingType_ratingScaleMaxValue_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingScaleMaxValue(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingScaleMinValue"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingScaleMinValue")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingScaleMinValue_CollectionPtr tmp = CreateratingType_ratingScaleMinValue_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingScaleMinValue(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ratingProvider"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ratingProvider")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateentityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetratingProvider(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingRegion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingRegion")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingRegion_CollectionPtr tmp = CreateratingType_ratingRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ratingExclusionRegion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ratingExclusionRegion")) == 0))
		{
			// Deserialize factory type
			EbuCoreratingType_ratingExclusionRegion_CollectionPtr tmp = CreateratingType_ratingExclusionRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetratingExclusionRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreratingType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreratingType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingValue")) == 0))
  {
	EbuCoreratingType_ratingValue_CollectionPtr tmp = CreateratingType_ratingValue_CollectionType; // FTT, check this
	this->SetratingValue(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingLink")) == 0))
  {
	EbuCoreratingType_ratingLink_CollectionPtr tmp = CreateratingType_ratingLink_CollectionType; // FTT, check this
	this->SetratingLink(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingScaleMaxValue")) == 0))
  {
	EbuCoreratingType_ratingScaleMaxValue_CollectionPtr tmp = CreateratingType_ratingScaleMaxValue_CollectionType; // FTT, check this
	this->SetratingScaleMaxValue(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingScaleMinValue")) == 0))
  {
	EbuCoreratingType_ratingScaleMinValue_CollectionPtr tmp = CreateratingType_ratingScaleMinValue_CollectionType; // FTT, check this
	this->SetratingScaleMinValue(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("ratingProvider")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateentityType; // FTT, check this
	}
	this->SetratingProvider(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingRegion")) == 0))
  {
	EbuCoreratingType_ratingRegion_CollectionPtr tmp = CreateratingType_ratingRegion_CollectionType; // FTT, check this
	this->SetratingRegion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ratingExclusionRegion")) == 0))
  {
	EbuCoreratingType_ratingExclusionRegion_CollectionPtr tmp = CreateratingType_ratingExclusionRegion_CollectionType; // FTT, check this
	this->SetratingExclusionRegion(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreratingType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetratingValue() != Dc1NodePtr())
		result.Insert(GetratingValue());
	if (GetratingLink() != Dc1NodePtr())
		result.Insert(GetratingLink());
	if (GetratingScaleMaxValue() != Dc1NodePtr())
		result.Insert(GetratingScaleMaxValue());
	if (GetratingScaleMinValue() != Dc1NodePtr())
		result.Insert(GetratingScaleMinValue());
	if (GetratingProvider() != Dc1NodePtr())
		result.Insert(GetratingProvider());
	if (GetratingRegion() != Dc1NodePtr())
		result.Insert(GetratingRegion());
	if (GetratingExclusionRegion() != Dc1NodePtr())
		result.Insert(GetratingExclusionRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreratingType_ExtMethodImpl.h


