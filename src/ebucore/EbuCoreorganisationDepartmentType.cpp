
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtImplInclude.h


#include "EbuCoreelementType.h"
#include "EbuCoreorganisationDepartmentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreorganisationDepartmentType::IEbuCoreorganisationDepartmentType()
{

// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtPropInit.h

}

IEbuCoreorganisationDepartmentType::~IEbuCoreorganisationDepartmentType()
{
// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtPropCleanup.h

}

EbuCoreorganisationDepartmentType::EbuCoreorganisationDepartmentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreorganisationDepartmentType::~EbuCoreorganisationDepartmentType()
{
	Cleanup();
}

void EbuCoreorganisationDepartmentType::Init()
{
	// Init base
	m_Base = CreateEbuCoreelementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_departmentId = NULL; // String
	m_departmentId_Exist = false;



// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtMyPropInit.h

}

void EbuCoreorganisationDepartmentType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_departmentId); // String
}

void EbuCoreorganisationDepartmentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use organisationDepartmentTypePtr, since we
	// might need GetBase(), which isn't defined in IorganisationDepartmentType
	const Dc1Ptr< EbuCoreorganisationDepartmentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = EbuCoreelementPtr(Dc1Factory::CloneObject((dynamic_cast< const EbuCoreorganisationDepartmentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_departmentId); // String
	if (tmp->ExistdepartmentId())
	{
		this->SetdepartmentId(XMLString::replicate(tmp->GetdepartmentId()));
	}
	else
	{
		InvalidatedepartmentId();
	}
	}
}

Dc1NodePtr EbuCoreorganisationDepartmentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr EbuCoreorganisationDepartmentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< EbuCoreelementType > EbuCoreorganisationDepartmentType::GetBase() const
{
	return m_Base;
}

void EbuCoreorganisationDepartmentType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::SetContent().");
	}
	GetBase()->GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCoreorganisationDepartmentType::GetContent() const
{
	return GetBase()->GetBase()->GetContent();
}
XMLCh * EbuCoreorganisationDepartmentType::GetdepartmentId() const
{
	return m_departmentId;
}

bool EbuCoreorganisationDepartmentType::ExistdepartmentId() const
{
	return m_departmentId_Exist;
}
void EbuCoreorganisationDepartmentType::SetdepartmentId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::SetdepartmentId().");
	}
	m_departmentId = item;
	m_departmentId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDepartmentType::InvalidatedepartmentId()
{
	m_departmentId_Exist = false;
}
XMLCh * EbuCoreorganisationDepartmentType::Getcharset() const
{
	return GetBase()->Getcharset();
}

bool EbuCoreorganisationDepartmentType::Existcharset() const
{
	return GetBase()->Existcharset();
}
void EbuCoreorganisationDepartmentType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::Setcharset().");
	}
	GetBase()->Setcharset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDepartmentType::Invalidatecharset()
{
	GetBase()->Invalidatecharset();
}
XMLCh * EbuCoreorganisationDepartmentType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool EbuCoreorganisationDepartmentType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void EbuCoreorganisationDepartmentType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDepartmentType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
XMLCh * EbuCoreorganisationDepartmentType::Gettranscription() const
{
	return GetBase()->Gettranscription();
}

bool EbuCoreorganisationDepartmentType::Existtranscription() const
{
	return GetBase()->Existtranscription();
}
void EbuCoreorganisationDepartmentType::Settranscription(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::Settranscription().");
	}
	GetBase()->Settranscription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDepartmentType::Invalidatetranscription()
{
	GetBase()->Invalidatetranscription();
}
XMLCh * EbuCoreorganisationDepartmentType::Getlang() const
{
	return GetBase()->GetBase()->Getlang();
}

bool EbuCoreorganisationDepartmentType::Existlang() const
{
	return GetBase()->GetBase()->Existlang();
}
void EbuCoreorganisationDepartmentType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDepartmentType::Setlang().");
	}
	GetBase()->GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDepartmentType::Invalidatelang()
{
	GetBase()->GetBase()->Invalidatelang();
}

Dc1NodeEnum EbuCoreorganisationDepartmentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("departmentId")) == 0)
	{
		// departmentId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for organisationDepartmentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "organisationDepartmentType");
	}
	return result;
}

XMLCh * EbuCoreorganisationDepartmentType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreorganisationDepartmentType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCoreorganisationDepartmentType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* EbuCoreorganisationDepartmentType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void EbuCoreorganisationDepartmentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("organisationDepartmentType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_departmentId_Exist)
	{
	// String
	if(m_departmentId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("departmentId"), m_departmentId);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCoreorganisationDepartmentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("departmentId")))
	{
		// Deserialize string type
		this->SetdepartmentId(Dc1Convert::TextToString(parent->getAttribute(X("departmentId"))));
		* current = parent;
	}

	// Extensionbase is EbuCoreelementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreorganisationDepartmentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreorganisationDepartmentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreorganisationDepartmentType_ExtMethodImpl.h


