
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtImplInclude.h


#include "EbuCorecaptioningFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorecaptioningFormatType::IEbuCorecaptioningFormatType()
{

// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtPropInit.h

}

IEbuCorecaptioningFormatType::~IEbuCorecaptioningFormatType()
{
// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtPropCleanup.h

}

EbuCorecaptioningFormatType::EbuCorecaptioningFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecaptioningFormatType::~EbuCorecaptioningFormatType()
{
	Cleanup();
}

void EbuCorecaptioningFormatType::Init()
{

	// Init attributes
	m_captioningFormatId = NULL; // String
	m_captioningFormatId_Exist = false;
	m_captioningFormatName = NULL; // String
	m_captioningFormatName_Exist = false;
	m_trackId = NULL; // String
	m_trackId_Exist = false;
	m_trackName = NULL; // String
	m_trackName_Exist = false;
	m_captioningSourceUri = NULL; // String
	m_captioningSourceUri_Exist = false;
	m_language = NULL; // String
	m_language_Exist = false;
	m_closed = false; // Value
	m_closed_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;
	m_captioningPresenceFlag = false; // Value
	m_captioningPresenceFlag_Exist = false;



// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtMyPropInit.h

}

void EbuCorecaptioningFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_captioningFormatId); // String
	XMLString::release(&m_captioningFormatName); // String
	XMLString::release(&m_trackId); // String
	XMLString::release(&m_trackName); // String
	XMLString::release(&m_captioningSourceUri); // String
	XMLString::release(&m_language); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
}

void EbuCorecaptioningFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use captioningFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IcaptioningFormatType
	const Dc1Ptr< EbuCorecaptioningFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_captioningFormatId); // String
	if (tmp->ExistcaptioningFormatId())
	{
		this->SetcaptioningFormatId(XMLString::replicate(tmp->GetcaptioningFormatId()));
	}
	else
	{
		InvalidatecaptioningFormatId();
	}
	}
	{
	XMLString::release(&m_captioningFormatName); // String
	if (tmp->ExistcaptioningFormatName())
	{
		this->SetcaptioningFormatName(XMLString::replicate(tmp->GetcaptioningFormatName()));
	}
	else
	{
		InvalidatecaptioningFormatName();
	}
	}
	{
	XMLString::release(&m_trackId); // String
	if (tmp->ExisttrackId())
	{
		this->SettrackId(XMLString::replicate(tmp->GettrackId()));
	}
	else
	{
		InvalidatetrackId();
	}
	}
	{
	XMLString::release(&m_trackName); // String
	if (tmp->ExisttrackName())
	{
		this->SettrackName(XMLString::replicate(tmp->GettrackName()));
	}
	else
	{
		InvalidatetrackName();
	}
	}
	{
	XMLString::release(&m_captioningSourceUri); // String
	if (tmp->ExistcaptioningSourceUri())
	{
		this->SetcaptioningSourceUri(XMLString::replicate(tmp->GetcaptioningSourceUri()));
	}
	else
	{
		InvalidatecaptioningSourceUri();
	}
	}
	{
	XMLString::release(&m_language); // String
	if (tmp->Existlanguage())
	{
		this->Setlanguage(XMLString::replicate(tmp->Getlanguage()));
	}
	else
	{
		Invalidatelanguage();
	}
	}
	{
	if (tmp->Existclosed())
	{
		this->Setclosed(tmp->Getclosed());
	}
	else
	{
		Invalidateclosed();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
	{
	if (tmp->ExistcaptioningPresenceFlag())
	{
		this->SetcaptioningPresenceFlag(tmp->GetcaptioningPresenceFlag());
	}
	else
	{
		InvalidatecaptioningPresenceFlag();
	}
	}
}

Dc1NodePtr EbuCorecaptioningFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorecaptioningFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorecaptioningFormatType::GetcaptioningFormatId() const
{
	return m_captioningFormatId;
}

bool EbuCorecaptioningFormatType::ExistcaptioningFormatId() const
{
	return m_captioningFormatId_Exist;
}
void EbuCorecaptioningFormatType::SetcaptioningFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetcaptioningFormatId().");
	}
	m_captioningFormatId = item;
	m_captioningFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatecaptioningFormatId()
{
	m_captioningFormatId_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetcaptioningFormatName() const
{
	return m_captioningFormatName;
}

bool EbuCorecaptioningFormatType::ExistcaptioningFormatName() const
{
	return m_captioningFormatName_Exist;
}
void EbuCorecaptioningFormatType::SetcaptioningFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetcaptioningFormatName().");
	}
	m_captioningFormatName = item;
	m_captioningFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatecaptioningFormatName()
{
	m_captioningFormatName_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettrackId() const
{
	return m_trackId;
}

bool EbuCorecaptioningFormatType::ExisttrackId() const
{
	return m_trackId_Exist;
}
void EbuCorecaptioningFormatType::SettrackId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettrackId().");
	}
	m_trackId = item;
	m_trackId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetrackId()
{
	m_trackId_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettrackName() const
{
	return m_trackName;
}

bool EbuCorecaptioningFormatType::ExisttrackName() const
{
	return m_trackName_Exist;
}
void EbuCorecaptioningFormatType::SettrackName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettrackName().");
	}
	m_trackName = item;
	m_trackName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetrackName()
{
	m_trackName_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetcaptioningSourceUri() const
{
	return m_captioningSourceUri;
}

bool EbuCorecaptioningFormatType::ExistcaptioningSourceUri() const
{
	return m_captioningSourceUri_Exist;
}
void EbuCorecaptioningFormatType::SetcaptioningSourceUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetcaptioningSourceUri().");
	}
	m_captioningSourceUri = item;
	m_captioningSourceUri_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatecaptioningSourceUri()
{
	m_captioningSourceUri_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::Getlanguage() const
{
	return m_language;
}

bool EbuCorecaptioningFormatType::Existlanguage() const
{
	return m_language_Exist;
}
void EbuCorecaptioningFormatType::Setlanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::Setlanguage().");
	}
	m_language = item;
	m_language_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::Invalidatelanguage()
{
	m_language_Exist = false;
}
bool EbuCorecaptioningFormatType::Getclosed() const
{
	return m_closed;
}

bool EbuCorecaptioningFormatType::Existclosed() const
{
	return m_closed_Exist;
}
void EbuCorecaptioningFormatType::Setclosed(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::Setclosed().");
	}
	m_closed = item;
	m_closed_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::Invalidateclosed()
{
	m_closed_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorecaptioningFormatType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorecaptioningFormatType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorecaptioningFormatType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorecaptioningFormatType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorecaptioningFormatType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorecaptioningFormatType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorecaptioningFormatType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorecaptioningFormatType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorecaptioningFormatType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorecaptioningFormatType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCorecaptioningFormatType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCorecaptioningFormatType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCorecaptioningFormatType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCorecaptioningFormatType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCorecaptioningFormatType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCorecaptioningFormatType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCorecaptioningFormatType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCorecaptioningFormatType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCorecaptioningFormatType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCorecaptioningFormatType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCorecaptioningFormatType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
bool EbuCorecaptioningFormatType::GetcaptioningPresenceFlag() const
{
	return m_captioningPresenceFlag;
}

bool EbuCorecaptioningFormatType::ExistcaptioningPresenceFlag() const
{
	return m_captioningPresenceFlag_Exist;
}
void EbuCorecaptioningFormatType::SetcaptioningPresenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecaptioningFormatType::SetcaptioningPresenceFlag().");
	}
	m_captioningPresenceFlag = item;
	m_captioningPresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecaptioningFormatType::InvalidatecaptioningPresenceFlag()
{
	m_captioningPresenceFlag_Exist = false;
}

Dc1NodeEnum EbuCorecaptioningFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  18, 18 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * EbuCorecaptioningFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorecaptioningFormatType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void EbuCorecaptioningFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("captioningFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_captioningFormatId_Exist)
	{
	// String
	if(m_captioningFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("captioningFormatId"), m_captioningFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_captioningFormatName_Exist)
	{
	// String
	if(m_captioningFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("captioningFormatName"), m_captioningFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_trackId_Exist)
	{
	// String
	if(m_trackId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("trackId"), m_trackId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_trackName_Exist)
	{
	// String
	if(m_trackName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("trackName"), m_trackName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_captioningSourceUri_Exist)
	{
	// String
	if(m_captioningSourceUri != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("captioningSourceUri"), m_captioningSourceUri);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_language_Exist)
	{
	// String
	if(m_language != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("language"), m_language);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_closed_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_closed);
		element->setAttributeNS(X(""), X("closed"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_captioningPresenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_captioningPresenceFlag);
		element->setAttributeNS(X(""), X("captioningPresenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCorecaptioningFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("captioningFormatId")))
	{
		// Deserialize string type
		this->SetcaptioningFormatId(Dc1Convert::TextToString(parent->getAttribute(X("captioningFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("captioningFormatName")))
	{
		// Deserialize string type
		this->SetcaptioningFormatName(Dc1Convert::TextToString(parent->getAttribute(X("captioningFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("trackId")))
	{
		// Deserialize string type
		this->SettrackId(Dc1Convert::TextToString(parent->getAttribute(X("trackId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("trackName")))
	{
		// Deserialize string type
		this->SettrackName(Dc1Convert::TextToString(parent->getAttribute(X("trackName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("captioningSourceUri")))
	{
		// Deserialize string type
		this->SetcaptioningSourceUri(Dc1Convert::TextToString(parent->getAttribute(X("captioningSourceUri"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("language")))
	{
		// Deserialize string type
		this->Setlanguage(Dc1Convert::TextToString(parent->getAttribute(X("language"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("closed")))
	{
		// deserialize value type
		this->Setclosed(Dc1Convert::TextToBool(parent->getAttribute(X("closed"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("captioningPresenceFlag")))
	{
		// deserialize value type
		this->SetcaptioningPresenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("captioningPresenceFlag"))));
		* current = parent;
	}

// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecaptioningFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCorecaptioningFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecaptioningFormatType_ExtMethodImpl.h


