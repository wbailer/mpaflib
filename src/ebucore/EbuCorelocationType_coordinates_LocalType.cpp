
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtImplInclude.h


#include "EbuCorelocationType_coordinates_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorelocationType_coordinates_LocalType::IEbuCorelocationType_coordinates_LocalType()
{

// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtPropInit.h

}

IEbuCorelocationType_coordinates_LocalType::~IEbuCorelocationType_coordinates_LocalType()
{
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtPropCleanup.h

}

EbuCorelocationType_coordinates_LocalType::EbuCorelocationType_coordinates_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorelocationType_coordinates_LocalType::~EbuCorelocationType_coordinates_LocalType()
{
	Cleanup();
}

void EbuCorelocationType_coordinates_LocalType::Init()
{

	// Init attributes
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_posy = (0.0f); // Value

	m_posx = (0.0f); // Value



// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMyPropInit.h

}

void EbuCorelocationType_coordinates_LocalType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
}

void EbuCorelocationType_coordinates_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use locationType_coordinates_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IlocationType_coordinates_LocalType
	const Dc1Ptr< EbuCorelocationType_coordinates_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
		this->Setposy(tmp->Getposy());
		this->Setposx(tmp->Getposx());
}

Dc1NodePtr EbuCorelocationType_coordinates_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorelocationType_coordinates_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorelocationType_coordinates_LocalType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCorelocationType_coordinates_LocalType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCorelocationType_coordinates_LocalType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCorelocationType_coordinates_LocalType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCorelocationType_coordinates_LocalType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCorelocationType_coordinates_LocalType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCorelocationType_coordinates_LocalType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCorelocationType_coordinates_LocalType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCorelocationType_coordinates_LocalType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCorelocationType_coordinates_LocalType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCorelocationType_coordinates_LocalType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCorelocationType_coordinates_LocalType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCorelocationType_coordinates_LocalType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCorelocationType_coordinates_LocalType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCorelocationType_coordinates_LocalType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
float EbuCorelocationType_coordinates_LocalType::Getposy() const
{
		return m_posy;
}

float EbuCorelocationType_coordinates_LocalType::Getposx() const
{
		return m_posx;
}

void EbuCorelocationType_coordinates_LocalType::Setposy(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::Setposy().");
	}
	if (m_posy != item)
	{
		m_posy = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorelocationType_coordinates_LocalType::Setposx(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorelocationType_coordinates_LocalType::Setposx().");
	}
	if (m_posx != item)
	{
		m_posx = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorelocationType_coordinates_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * EbuCorelocationType_coordinates_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorelocationType_coordinates_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorelocationType_coordinates_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("locationType_coordinates_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_posy);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("posy"), false);
		XMLString::release(&tmp);
	}
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_posx);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("posx"), false);
		XMLString::release(&tmp);
	}

}

bool EbuCorelocationType_coordinates_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("posy"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setposy(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("posx"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setposx(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorelocationType_coordinates_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCorelocationType_coordinates_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMethodImpl.h


