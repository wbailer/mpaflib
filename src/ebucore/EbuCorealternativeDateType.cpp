
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCorealternativeDateType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorealternativeDateType::IEbuCorealternativeDateType()
{

// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtPropInit.h

}

IEbuCorealternativeDateType::~IEbuCorealternativeDateType()
{
// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtPropCleanup.h

}

EbuCorealternativeDateType::EbuCorealternativeDateType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorealternativeDateType::~EbuCorealternativeDateType()
{
	Cleanup();
}

void EbuCorealternativeDateType::Init()
{

	// Init attributes
	m_startYear = 2015; // Value
	m_startYear_Exist = false;
	m_startDate = W3CdatePtr(); // Pattern
	m_startDate_Exist = false;
	m_startTime = W3CtimePtr(); // Pattern
	m_startTime_Exist = false;
	m_endYear = 2015; // Value
	m_endYear_Exist = false;
	m_endDate = W3CdatePtr(); // Pattern
	m_endDate_Exist = false;
	m_endTime = W3CtimePtr(); // Pattern
	m_endTime_Exist = false;
	m_period = NULL; // String
	m_period_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;



// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtMyPropInit.h

}

void EbuCorealternativeDateType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	XMLString::release(&m_period); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
}

void EbuCorealternativeDateType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use alternativeDateTypePtr, since we
	// might need GetBase(), which isn't defined in IalternativeDateType
	const Dc1Ptr< EbuCorealternativeDateType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExiststartYear())
	{
		this->SetstartYear(tmp->GetstartYear());
	}
	else
	{
		InvalidatestartYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	if (tmp->ExiststartDate())
	{
		this->SetstartDate(Dc1Factory::CloneObject(tmp->GetstartDate()));
	}
	else
	{
		InvalidatestartDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	if (tmp->ExiststartTime())
	{
		this->SetstartTime(Dc1Factory::CloneObject(tmp->GetstartTime()));
	}
	else
	{
		InvalidatestartTime();
	}
	}
	{
	if (tmp->ExistendYear())
	{
		this->SetendYear(tmp->GetendYear());
	}
	else
	{
		InvalidateendYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	if (tmp->ExistendDate())
	{
		this->SetendDate(Dc1Factory::CloneObject(tmp->GetendDate()));
	}
	else
	{
		InvalidateendDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	if (tmp->ExistendTime())
	{
		this->SetendTime(Dc1Factory::CloneObject(tmp->GetendTime()));
	}
	else
	{
		InvalidateendTime();
	}
	}
	{
	XMLString::release(&m_period); // String
	if (tmp->Existperiod())
	{
		this->Setperiod(XMLString::replicate(tmp->Getperiod()));
	}
	else
	{
		Invalidateperiod();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
}

Dc1NodePtr EbuCorealternativeDateType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorealternativeDateType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


int EbuCorealternativeDateType::GetstartYear() const
{
	return m_startYear;
}

bool EbuCorealternativeDateType::ExiststartYear() const
{
	return m_startYear_Exist;
}
void EbuCorealternativeDateType::SetstartYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetstartYear().");
	}
	m_startYear = item;
	m_startYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatestartYear()
{
	m_startYear_Exist = false;
}
W3CdatePtr EbuCorealternativeDateType::GetstartDate() const
{
	return m_startDate;
}

bool EbuCorealternativeDateType::ExiststartDate() const
{
	return m_startDate_Exist;
}
void EbuCorealternativeDateType::SetstartDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetstartDate().");
	}
	m_startDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startDate) m_startDate->SetParent(m_myself.getPointer());
	m_startDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatestartDate()
{
	m_startDate_Exist = false;
}
W3CtimePtr EbuCorealternativeDateType::GetstartTime() const
{
	return m_startTime;
}

bool EbuCorealternativeDateType::ExiststartTime() const
{
	return m_startTime_Exist;
}
void EbuCorealternativeDateType::SetstartTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetstartTime().");
	}
	m_startTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startTime) m_startTime->SetParent(m_myself.getPointer());
	m_startTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatestartTime()
{
	m_startTime_Exist = false;
}
int EbuCorealternativeDateType::GetendYear() const
{
	return m_endYear;
}

bool EbuCorealternativeDateType::ExistendYear() const
{
	return m_endYear_Exist;
}
void EbuCorealternativeDateType::SetendYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetendYear().");
	}
	m_endYear = item;
	m_endYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidateendYear()
{
	m_endYear_Exist = false;
}
W3CdatePtr EbuCorealternativeDateType::GetendDate() const
{
	return m_endDate;
}

bool EbuCorealternativeDateType::ExistendDate() const
{
	return m_endDate_Exist;
}
void EbuCorealternativeDateType::SetendDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetendDate().");
	}
	m_endDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endDate) m_endDate->SetParent(m_myself.getPointer());
	m_endDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidateendDate()
{
	m_endDate_Exist = false;
}
W3CtimePtr EbuCorealternativeDateType::GetendTime() const
{
	return m_endTime;
}

bool EbuCorealternativeDateType::ExistendTime() const
{
	return m_endTime_Exist;
}
void EbuCorealternativeDateType::SetendTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SetendTime().");
	}
	m_endTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endTime) m_endTime->SetParent(m_myself.getPointer());
	m_endTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidateendTime()
{
	m_endTime_Exist = false;
}
XMLCh * EbuCorealternativeDateType::Getperiod() const
{
	return m_period;
}

bool EbuCorealternativeDateType::Existperiod() const
{
	return m_period_Exist;
}
void EbuCorealternativeDateType::Setperiod(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::Setperiod().");
	}
	m_period = item;
	m_period_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::Invalidateperiod()
{
	m_period_Exist = false;
}
XMLCh * EbuCorealternativeDateType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorealternativeDateType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorealternativeDateType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorealternativeDateType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorealternativeDateType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorealternativeDateType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorealternativeDateType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorealternativeDateType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorealternativeDateType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorealternativeDateType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorealternativeDateType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorealternativeDateType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorealternativeDateType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorealternativeDateType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorealternativeDateType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeDateType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeDateType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}

Dc1NodeEnum EbuCorealternativeDateType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  12, 12 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * EbuCorealternativeDateType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorealternativeDateType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void EbuCorealternativeDateType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("alternativeDateType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_startYear);
		element->setAttributeNS(X(""), X("startYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startDate_Exist)
	{
	// Pattern
	if (m_startDate != W3CdatePtr())
	{
		XMLCh * tmp = m_startDate->ToText();
		element->setAttributeNS(X(""), X("startDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startTime_Exist)
	{
	// Pattern
	if (m_startTime != W3CtimePtr())
	{
		XMLCh * tmp = m_startTime->ToText();
		element->setAttributeNS(X(""), X("startTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_endYear);
		element->setAttributeNS(X(""), X("endYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endDate_Exist)
	{
	// Pattern
	if (m_endDate != W3CdatePtr())
	{
		XMLCh * tmp = m_endDate->ToText();
		element->setAttributeNS(X(""), X("endDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endTime_Exist)
	{
	// Pattern
	if (m_endTime != W3CtimePtr())
	{
		XMLCh * tmp = m_endTime->ToText();
		element->setAttributeNS(X(""), X("endTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_period_Exist)
	{
	// String
	if(m_period != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("period"), m_period);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCorealternativeDateType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startYear")))
	{
		// deserialize value type
		this->SetstartYear(Dc1Convert::TextToInt(parent->getAttribute(X("startYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startDate")));
		this->SetstartDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startTime")));
		this->SetstartTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endYear")))
	{
		// deserialize value type
		this->SetendYear(Dc1Convert::TextToInt(parent->getAttribute(X("endYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endDate")));
		this->SetendDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endTime")));
		this->SetendTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("period")))
	{
		// Deserialize string type
		this->Setperiod(Dc1Convert::TextToString(parent->getAttribute(X("period"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorealternativeDateType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCorealternativeDateType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorealternativeDateType_ExtMethodImpl.h


