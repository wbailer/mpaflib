
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreformatType_ExtImplInclude.h


#include "Dc1elementType.h"
#include "EbuCoreformatType_medium_CollectionType.h"
#include "EbuCoreformatType_imageFormat_CollectionType.h"
#include "EbuCoreformatType_videoFormat_CollectionType.h"
#include "EbuCoreformatType_audioFormat_CollectionType.h"
#include "EbuCoreformatType_audioFormatExtended_CollectionType.h"
#include "EbuCoreformatType_containerFormat_CollectionType.h"
#include "EbuCoresigningFormatType.h"
#include "EbuCoreformatType_dataFormat_CollectionType.h"
#include "EbuCoretimeType.h"
#include "EbuCoredurationType.h"
#include "EbuCorefileInfo.h"
#include "EbuCoredocumentFormatType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCoreformatType_dateCreated_LocalType.h"
#include "EbuCoreformatType_dateModified_LocalType.h"
#include "EbuCoreformatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreformatType_medium_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:medium
#include "EbuCoreimageFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:imageFormat
#include "EbuCorevideoFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:videoFormat
#include "EbuCoreaudioFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioFormat
#include "EbuCoreaudioFormatExtendedType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtended
#include "EbuCorecontainerFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:containerFormat
#include "EbuCoredataFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:dataFormat

#include <assert.h>
IEbuCoreformatType::IEbuCoreformatType()
{

// no includefile for extension defined 
// file EbuCoreformatType_ExtPropInit.h

}

IEbuCoreformatType::~IEbuCoreformatType()
{
// no includefile for extension defined 
// file EbuCoreformatType_ExtPropCleanup.h

}

EbuCoreformatType::EbuCoreformatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreformatType::~EbuCoreformatType()
{
	Cleanup();
}

void EbuCoreformatType::Init()
{

	// Init attributes
	m_formatId = NULL; // String
	m_formatId_Exist = false;
	m_formatVersionId = NULL; // String
	m_formatVersionId_Exist = false;
	m_formatName = NULL; // String
	m_formatName_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_format = Dc1elementPtr(); // Class
	m_format_Exist = false;
	m_medium = EbuCoreformatType_medium_CollectionPtr(); // Collection
	m_imageFormat = EbuCoreformatType_imageFormat_CollectionPtr(); // Collection
	m_videoFormat = EbuCoreformatType_videoFormat_CollectionPtr(); // Collection
	m_audioFormat = EbuCoreformatType_audioFormat_CollectionPtr(); // Collection
	m_audioFormatExtended = EbuCoreformatType_audioFormatExtended_CollectionPtr(); // Collection
	m_containerFormat = EbuCoreformatType_containerFormat_CollectionPtr(); // Collection
	m_signingFormat = EbuCoresigningFormatPtr(); // Class
	m_signingFormat_Exist = false;
	m_dataFormat = EbuCoreformatType_dataFormat_CollectionPtr(); // Collection
	m_start = EbuCoretimePtr(); // Class
	m_start_Exist = false;
	m_end = EbuCoretimePtr(); // Class
	m_end_Exist = false;
	m_duration = EbuCoredurationPtr(); // Class
	m_duration_Exist = false;
	m_fileInfo = EbuCorefileInfoPtr(); // Class
	m_documentFormat = EbuCoredocumentFormatPtr(); // Class
	m_documentFormat_Exist = false;
	m_technicalAttributes = EbuCoretechnicalAttributesPtr(); // Class
	m_dateCreated = EbuCoreformatType_dateCreated_LocalPtr(); // Class
	m_dateCreated_Exist = false;
	m_dateModified = EbuCoreformatType_dateModified_LocalPtr(); // Class
	m_dateModified_Exist = false;


// no includefile for extension defined 
// file EbuCoreformatType_ExtMyPropInit.h

}

void EbuCoreformatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreformatType_ExtMyPropCleanup.h


	XMLString::release(&m_formatId); // String
	XMLString::release(&m_formatVersionId); // String
	XMLString::release(&m_formatName); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_format);
	// Dc1Factory::DeleteObject(m_medium);
	// Dc1Factory::DeleteObject(m_imageFormat);
	// Dc1Factory::DeleteObject(m_videoFormat);
	// Dc1Factory::DeleteObject(m_audioFormat);
	// Dc1Factory::DeleteObject(m_audioFormatExtended);
	// Dc1Factory::DeleteObject(m_containerFormat);
	// Dc1Factory::DeleteObject(m_signingFormat);
	// Dc1Factory::DeleteObject(m_dataFormat);
	// Dc1Factory::DeleteObject(m_start);
	// Dc1Factory::DeleteObject(m_end);
	// Dc1Factory::DeleteObject(m_duration);
	// Dc1Factory::DeleteObject(m_fileInfo);
	// Dc1Factory::DeleteObject(m_documentFormat);
	// Dc1Factory::DeleteObject(m_technicalAttributes);
	// Dc1Factory::DeleteObject(m_dateCreated);
	// Dc1Factory::DeleteObject(m_dateModified);
}

void EbuCoreformatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use formatTypePtr, since we
	// might need GetBase(), which isn't defined in IformatType
	const Dc1Ptr< EbuCoreformatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_formatId); // String
	if (tmp->ExistformatId())
	{
		this->SetformatId(XMLString::replicate(tmp->GetformatId()));
	}
	else
	{
		InvalidateformatId();
	}
	}
	{
	XMLString::release(&m_formatVersionId); // String
	if (tmp->ExistformatVersionId())
	{
		this->SetformatVersionId(XMLString::replicate(tmp->GetformatVersionId()));
	}
	else
	{
		InvalidateformatVersionId();
	}
	}
	{
	XMLString::release(&m_formatName); // String
	if (tmp->ExistformatName())
	{
		this->SetformatName(XMLString::replicate(tmp->GetformatName()));
	}
	else
	{
		InvalidateformatName();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	if (tmp->IsValidformat())
	{
		// Dc1Factory::DeleteObject(m_format);
		this->Setformat(Dc1Factory::CloneObject(tmp->Getformat()));
	}
	else
	{
		Invalidateformat();
	}
		// Dc1Factory::DeleteObject(m_medium);
		this->Setmedium(Dc1Factory::CloneObject(tmp->Getmedium()));
		// Dc1Factory::DeleteObject(m_imageFormat);
		this->SetimageFormat(Dc1Factory::CloneObject(tmp->GetimageFormat()));
		// Dc1Factory::DeleteObject(m_videoFormat);
		this->SetvideoFormat(Dc1Factory::CloneObject(tmp->GetvideoFormat()));
		// Dc1Factory::DeleteObject(m_audioFormat);
		this->SetaudioFormat(Dc1Factory::CloneObject(tmp->GetaudioFormat()));
		// Dc1Factory::DeleteObject(m_audioFormatExtended);
		this->SetaudioFormatExtended(Dc1Factory::CloneObject(tmp->GetaudioFormatExtended()));
		// Dc1Factory::DeleteObject(m_containerFormat);
		this->SetcontainerFormat(Dc1Factory::CloneObject(tmp->GetcontainerFormat()));
	if (tmp->IsValidsigningFormat())
	{
		// Dc1Factory::DeleteObject(m_signingFormat);
		this->SetsigningFormat(Dc1Factory::CloneObject(tmp->GetsigningFormat()));
	}
	else
	{
		InvalidatesigningFormat();
	}
		// Dc1Factory::DeleteObject(m_dataFormat);
		this->SetdataFormat(Dc1Factory::CloneObject(tmp->GetdataFormat()));
	if (tmp->IsValidstart())
	{
		// Dc1Factory::DeleteObject(m_start);
		this->Setstart(Dc1Factory::CloneObject(tmp->Getstart()));
	}
	else
	{
		Invalidatestart();
	}
	if (tmp->IsValidend())
	{
		// Dc1Factory::DeleteObject(m_end);
		this->Setend(Dc1Factory::CloneObject(tmp->Getend()));
	}
	else
	{
		Invalidateend();
	}
	if (tmp->IsValidduration())
	{
		// Dc1Factory::DeleteObject(m_duration);
		this->Setduration(Dc1Factory::CloneObject(tmp->Getduration()));
	}
	else
	{
		Invalidateduration();
	}
		// Dc1Factory::DeleteObject(m_fileInfo);
		this->SetfileInfo(Dc1Factory::CloneObject(tmp->GetfileInfo()));
	if (tmp->IsValiddocumentFormat())
	{
		// Dc1Factory::DeleteObject(m_documentFormat);
		this->SetdocumentFormat(Dc1Factory::CloneObject(tmp->GetdocumentFormat()));
	}
	else
	{
		InvalidatedocumentFormat();
	}
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		this->SettechnicalAttributes(Dc1Factory::CloneObject(tmp->GettechnicalAttributes()));
	if (tmp->IsValiddateCreated())
	{
		// Dc1Factory::DeleteObject(m_dateCreated);
		this->SetdateCreated(Dc1Factory::CloneObject(tmp->GetdateCreated()));
	}
	else
	{
		InvalidatedateCreated();
	}
	if (tmp->IsValiddateModified())
	{
		// Dc1Factory::DeleteObject(m_dateModified);
		this->SetdateModified(Dc1Factory::CloneObject(tmp->GetdateModified()));
	}
	else
	{
		InvalidatedateModified();
	}
}

Dc1NodePtr EbuCoreformatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreformatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreformatType::GetformatId() const
{
	return m_formatId;
}

bool EbuCoreformatType::ExistformatId() const
{
	return m_formatId_Exist;
}
void EbuCoreformatType::SetformatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetformatId().");
	}
	m_formatId = item;
	m_formatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidateformatId()
{
	m_formatId_Exist = false;
}
XMLCh * EbuCoreformatType::GetformatVersionId() const
{
	return m_formatVersionId;
}

bool EbuCoreformatType::ExistformatVersionId() const
{
	return m_formatVersionId_Exist;
}
void EbuCoreformatType::SetformatVersionId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetformatVersionId().");
	}
	m_formatVersionId = item;
	m_formatVersionId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidateformatVersionId()
{
	m_formatVersionId_Exist = false;
}
XMLCh * EbuCoreformatType::GetformatName() const
{
	return m_formatName;
}

bool EbuCoreformatType::ExistformatName() const
{
	return m_formatName_Exist;
}
void EbuCoreformatType::SetformatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetformatName().");
	}
	m_formatName = item;
	m_formatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidateformatName()
{
	m_formatName_Exist = false;
}
XMLCh * EbuCoreformatType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCoreformatType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCoreformatType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCoreformatType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreformatType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreformatType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreformatType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreformatType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreformatType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreformatType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreformatType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreformatType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreformatType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreformatType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreformatType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreformatType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreformatType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreformatType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
Dc1elementPtr EbuCoreformatType::Getformat() const
{
		return m_format;
}

// Element is optional
bool EbuCoreformatType::IsValidformat() const
{
	return m_format_Exist;
}

EbuCoreformatType_medium_CollectionPtr EbuCoreformatType::Getmedium() const
{
		return m_medium;
}

EbuCoreformatType_imageFormat_CollectionPtr EbuCoreformatType::GetimageFormat() const
{
		return m_imageFormat;
}

EbuCoreformatType_videoFormat_CollectionPtr EbuCoreformatType::GetvideoFormat() const
{
		return m_videoFormat;
}

EbuCoreformatType_audioFormat_CollectionPtr EbuCoreformatType::GetaudioFormat() const
{
		return m_audioFormat;
}

EbuCoreformatType_audioFormatExtended_CollectionPtr EbuCoreformatType::GetaudioFormatExtended() const
{
		return m_audioFormatExtended;
}

EbuCoreformatType_containerFormat_CollectionPtr EbuCoreformatType::GetcontainerFormat() const
{
		return m_containerFormat;
}

EbuCoresigningFormatPtr EbuCoreformatType::GetsigningFormat() const
{
		return m_signingFormat;
}

// Element is optional
bool EbuCoreformatType::IsValidsigningFormat() const
{
	return m_signingFormat_Exist;
}

EbuCoreformatType_dataFormat_CollectionPtr EbuCoreformatType::GetdataFormat() const
{
		return m_dataFormat;
}

EbuCoretimePtr EbuCoreformatType::Getstart() const
{
		return m_start;
}

// Element is optional
bool EbuCoreformatType::IsValidstart() const
{
	return m_start_Exist;
}

EbuCoretimePtr EbuCoreformatType::Getend() const
{
		return m_end;
}

// Element is optional
bool EbuCoreformatType::IsValidend() const
{
	return m_end_Exist;
}

EbuCoredurationPtr EbuCoreformatType::Getduration() const
{
		return m_duration;
}

// Element is optional
bool EbuCoreformatType::IsValidduration() const
{
	return m_duration_Exist;
}

EbuCorefileInfoPtr EbuCoreformatType::GetfileInfo() const
{
		return m_fileInfo;
}

EbuCoredocumentFormatPtr EbuCoreformatType::GetdocumentFormat() const
{
		return m_documentFormat;
}

// Element is optional
bool EbuCoreformatType::IsValiddocumentFormat() const
{
	return m_documentFormat_Exist;
}

EbuCoretechnicalAttributesPtr EbuCoreformatType::GettechnicalAttributes() const
{
		return m_technicalAttributes;
}

EbuCoreformatType_dateCreated_LocalPtr EbuCoreformatType::GetdateCreated() const
{
		return m_dateCreated;
}

// Element is optional
bool EbuCoreformatType::IsValiddateCreated() const
{
	return m_dateCreated_Exist;
}

EbuCoreformatType_dateModified_LocalPtr EbuCoreformatType::GetdateModified() const
{
		return m_dateModified;
}

// Element is optional
bool EbuCoreformatType::IsValiddateModified() const
{
	return m_dateModified_Exist;
}

void EbuCoreformatType::Setformat(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::Setformat().");
	}
	if (m_format != item || m_format_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_format);
		m_format = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_format) m_format->SetParent(m_myself.getPointer());
		if (m_format && m_format->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_format->UseTypeAttribute = true;
		}
		m_format_Exist = true;
		if(m_format != Dc1elementPtr())
		{
			m_format->SetContentName(XMLString::transcode("format"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::Invalidateformat()
{
	m_format_Exist = false;
}
void EbuCoreformatType::Setmedium(const EbuCoreformatType_medium_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::Setmedium().");
	}
	if (m_medium != item)
	{
		// Dc1Factory::DeleteObject(m_medium);
		m_medium = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_medium) m_medium->SetParent(m_myself.getPointer());
		if(m_medium != EbuCoreformatType_medium_CollectionPtr())
		{
			m_medium->SetContentName(XMLString::transcode("medium"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetimageFormat(const EbuCoreformatType_imageFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetimageFormat().");
	}
	if (m_imageFormat != item)
	{
		// Dc1Factory::DeleteObject(m_imageFormat);
		m_imageFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_imageFormat) m_imageFormat->SetParent(m_myself.getPointer());
		if(m_imageFormat != EbuCoreformatType_imageFormat_CollectionPtr())
		{
			m_imageFormat->SetContentName(XMLString::transcode("imageFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetvideoFormat(const EbuCoreformatType_videoFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetvideoFormat().");
	}
	if (m_videoFormat != item)
	{
		// Dc1Factory::DeleteObject(m_videoFormat);
		m_videoFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_videoFormat) m_videoFormat->SetParent(m_myself.getPointer());
		if(m_videoFormat != EbuCoreformatType_videoFormat_CollectionPtr())
		{
			m_videoFormat->SetContentName(XMLString::transcode("videoFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetaudioFormat(const EbuCoreformatType_audioFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetaudioFormat().");
	}
	if (m_audioFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioFormat);
		m_audioFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioFormat) m_audioFormat->SetParent(m_myself.getPointer());
		if(m_audioFormat != EbuCoreformatType_audioFormat_CollectionPtr())
		{
			m_audioFormat->SetContentName(XMLString::transcode("audioFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetaudioFormatExtended(const EbuCoreformatType_audioFormatExtended_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetaudioFormatExtended().");
	}
	if (m_audioFormatExtended != item)
	{
		// Dc1Factory::DeleteObject(m_audioFormatExtended);
		m_audioFormatExtended = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioFormatExtended) m_audioFormatExtended->SetParent(m_myself.getPointer());
		if(m_audioFormatExtended != EbuCoreformatType_audioFormatExtended_CollectionPtr())
		{
			m_audioFormatExtended->SetContentName(XMLString::transcode("audioFormatExtended"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetcontainerFormat(const EbuCoreformatType_containerFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetcontainerFormat().");
	}
	if (m_containerFormat != item)
	{
		// Dc1Factory::DeleteObject(m_containerFormat);
		m_containerFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_containerFormat) m_containerFormat->SetParent(m_myself.getPointer());
		if(m_containerFormat != EbuCoreformatType_containerFormat_CollectionPtr())
		{
			m_containerFormat->SetContentName(XMLString::transcode("containerFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetsigningFormat(const EbuCoresigningFormatPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetsigningFormat().");
	}
	if (m_signingFormat != item || m_signingFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_signingFormat);
		m_signingFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_signingFormat) m_signingFormat->SetParent(m_myself.getPointer());
		if (m_signingFormat && m_signingFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:signingFormatType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_signingFormat->UseTypeAttribute = true;
		}
		m_signingFormat_Exist = true;
		if(m_signingFormat != EbuCoresigningFormatPtr())
		{
			m_signingFormat->SetContentName(XMLString::transcode("signingFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatesigningFormat()
{
	m_signingFormat_Exist = false;
}
void EbuCoreformatType::SetdataFormat(const EbuCoreformatType_dataFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetdataFormat().");
	}
	if (m_dataFormat != item)
	{
		// Dc1Factory::DeleteObject(m_dataFormat);
		m_dataFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_dataFormat) m_dataFormat->SetParent(m_myself.getPointer());
		if(m_dataFormat != EbuCoreformatType_dataFormat_CollectionPtr())
		{
			m_dataFormat->SetContentName(XMLString::transcode("dataFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::Setstart(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::Setstart().");
	}
	if (m_start != item || m_start_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_start);
		m_start = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_start) m_start->SetParent(m_myself.getPointer());
		if (m_start && m_start->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_start->UseTypeAttribute = true;
		}
		m_start_Exist = true;
		if(m_start != EbuCoretimePtr())
		{
			m_start->SetContentName(XMLString::transcode("start"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::Invalidatestart()
{
	m_start_Exist = false;
}
void EbuCoreformatType::Setend(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::Setend().");
	}
	if (m_end != item || m_end_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_end);
		m_end = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_end) m_end->SetParent(m_myself.getPointer());
		if (m_end && m_end->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_end->UseTypeAttribute = true;
		}
		m_end_Exist = true;
		if(m_end != EbuCoretimePtr())
		{
			m_end->SetContentName(XMLString::transcode("end"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::Invalidateend()
{
	m_end_Exist = false;
}
void EbuCoreformatType::Setduration(const EbuCoredurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::Setduration().");
	}
	if (m_duration != item || m_duration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_duration);
		m_duration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_duration) m_duration->SetParent(m_myself.getPointer());
		if (m_duration && m_duration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_duration->UseTypeAttribute = true;
		}
		m_duration_Exist = true;
		if(m_duration != EbuCoredurationPtr())
		{
			m_duration->SetContentName(XMLString::transcode("duration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::Invalidateduration()
{
	m_duration_Exist = false;
}
void EbuCoreformatType::SetfileInfo(const EbuCorefileInfoPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetfileInfo().");
	}
	if (m_fileInfo != item)
	{
		// Dc1Factory::DeleteObject(m_fileInfo);
		m_fileInfo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_fileInfo) m_fileInfo->SetParent(m_myself.getPointer());
		if (m_fileInfo && m_fileInfo->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_fileInfo->UseTypeAttribute = true;
		}
		if(m_fileInfo != EbuCorefileInfoPtr())
		{
			m_fileInfo->SetContentName(XMLString::transcode("fileInfo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetdocumentFormat(const EbuCoredocumentFormatPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetdocumentFormat().");
	}
	if (m_documentFormat != item || m_documentFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_documentFormat);
		m_documentFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_documentFormat) m_documentFormat->SetParent(m_myself.getPointer());
		if (m_documentFormat && m_documentFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_documentFormat->UseTypeAttribute = true;
		}
		m_documentFormat_Exist = true;
		if(m_documentFormat != EbuCoredocumentFormatPtr())
		{
			m_documentFormat->SetContentName(XMLString::transcode("documentFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatedocumentFormat()
{
	m_documentFormat_Exist = false;
}
void EbuCoreformatType::SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SettechnicalAttributes().");
	}
	if (m_technicalAttributes != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		m_technicalAttributes = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributes) m_technicalAttributes->SetParent(m_myself.getPointer());
		if (m_technicalAttributes && m_technicalAttributes->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_technicalAttributes->UseTypeAttribute = true;
		}
		if(m_technicalAttributes != EbuCoretechnicalAttributesPtr())
		{
			m_technicalAttributes->SetContentName(XMLString::transcode("technicalAttributes"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::SetdateCreated(const EbuCoreformatType_dateCreated_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetdateCreated().");
	}
	if (m_dateCreated != item || m_dateCreated_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_dateCreated);
		m_dateCreated = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_dateCreated) m_dateCreated->SetParent(m_myself.getPointer());
		if (m_dateCreated && m_dateCreated->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateCreated_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_dateCreated->UseTypeAttribute = true;
		}
		m_dateCreated_Exist = true;
		if(m_dateCreated != EbuCoreformatType_dateCreated_LocalPtr())
		{
			m_dateCreated->SetContentName(XMLString::transcode("dateCreated"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatedateCreated()
{
	m_dateCreated_Exist = false;
}
void EbuCoreformatType::SetdateModified(const EbuCoreformatType_dateModified_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreformatType::SetdateModified().");
	}
	if (m_dateModified != item || m_dateModified_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_dateModified);
		m_dateModified = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_dateModified) m_dateModified->SetParent(m_myself.getPointer());
		if (m_dateModified && m_dateModified->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateModified_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_dateModified->UseTypeAttribute = true;
		}
		m_dateModified_Exist = true;
		if(m_dateModified != EbuCoreformatType_dateModified_LocalPtr())
		{
			m_dateModified->SetContentName(XMLString::transcode("dateModified"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreformatType::InvalidatedateModified()
{
	m_dateModified_Exist = false;
}

Dc1NodeEnum EbuCoreformatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  9, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatId")) == 0)
	{
		// formatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatVersionId")) == 0)
	{
		// formatVersionId is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatName")) == 0)
	{
		// formatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatDefinition")) == 0)
	{
		// formatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("format")) == 0)
	{
		// format is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getformat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setformat(p, client);
					if((p = Getformat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("medium")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:medium is item of type formatType_medium_LocalType
		// in element collection formatType_medium_CollectionType
		
		context->Found = true;
		EbuCoreformatType_medium_CollectionPtr coll = Getmedium();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_medium_CollectionType; // FTT, check this
				Setmedium(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_medium_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreformatType_medium_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("imageFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:imageFormat is item of type imageFormatType
		// in element collection formatType_imageFormat_CollectionType
		
		context->Found = true;
		EbuCoreformatType_imageFormat_CollectionPtr coll = GetimageFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_imageFormat_CollectionType; // FTT, check this
				SetimageFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreimageFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("videoFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:videoFormat is item of type videoFormatType
		// in element collection formatType_videoFormat_CollectionType
		
		context->Found = true;
		EbuCoreformatType_videoFormat_CollectionPtr coll = GetvideoFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_videoFormat_CollectionType; // FTT, check this
				SetvideoFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType")))) != empty)
			{
				// Is type allowed
				EbuCorevideoFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioFormat is item of type audioFormatType
		// in element collection formatType_audioFormat_CollectionType
		
		context->Found = true;
		EbuCoreformatType_audioFormat_CollectionPtr coll = GetaudioFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_audioFormat_CollectionType; // FTT, check this
				SetaudioFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioFormatExtended")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtended is item of type audioFormatExtendedType
		// in element collection formatType_audioFormatExtended_CollectionType
		
		context->Found = true;
		EbuCoreformatType_audioFormatExtended_CollectionPtr coll = GetaudioFormatExtended();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_audioFormatExtended_CollectionType; // FTT, check this
				SetaudioFormatExtended(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioFormatExtendedPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("containerFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:containerFormat is item of type containerFormatType
		// in element collection formatType_containerFormat_CollectionType
		
		context->Found = true;
		EbuCoreformatType_containerFormat_CollectionPtr coll = GetcontainerFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_containerFormat_CollectionType; // FTT, check this
				SetcontainerFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:containerFormatType")))) != empty)
			{
				// Is type allowed
				EbuCorecontainerFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("signingFormat")) == 0)
	{
		// signingFormat is simple element signingFormatType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetsigningFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:signingFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoresigningFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetsigningFormat(p, client);
					if((p = GetsigningFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("dataFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:dataFormat is item of type dataFormatType
		// in element collection formatType_dataFormat_CollectionType
		
		context->Found = true;
		EbuCoreformatType_dataFormat_CollectionPtr coll = GetdataFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateformatType_dataFormat_CollectionType; // FTT, check this
				SetdataFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoredataFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getstart()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setstart(p, client);
					if((p = Getstart()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("end")) == 0)
	{
		// end is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getend()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setend(p, client);
					if((p = Getend()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("duration")) == 0)
	{
		// duration is simple element durationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getduration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType")))) != empty)
			{
				// Is type allowed
				EbuCoredurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setduration(p, client);
					if((p = Getduration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("fileInfo")) == 0)
	{
		// fileInfo is simple element fileInfo
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetfileInfo()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo")))) != empty)
			{
				// Is type allowed
				EbuCorefileInfoPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetfileInfo(p, client);
					if((p = GetfileInfo()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("documentFormat")) == 0)
	{
		// documentFormat is simple element documentFormatType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdocumentFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoredocumentFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdocumentFormat(p, client);
					if((p = GetdocumentFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributes")) == 0)
	{
		// technicalAttributes is simple element technicalAttributes
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettechnicalAttributes()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettechnicalAttributes(p, client);
					if((p = GettechnicalAttributes()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("dateCreated")) == 0)
	{
		// dateCreated is simple element formatType_dateCreated_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdateCreated()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateCreated_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreformatType_dateCreated_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdateCreated(p, client);
					if((p = GetdateCreated()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("dateModified")) == 0)
	{
		// dateModified is simple element formatType_dateModified_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetdateModified()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateModified_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreformatType_dateModified_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetdateModified(p, client);
					if((p = GetdateModified()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for formatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "formatType");
	}
	return result;
}

XMLCh * EbuCoreformatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreformatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreformatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("formatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatId_Exist)
	{
	// String
	if(m_formatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatId"), m_formatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatVersionId_Exist)
	{
	// String
	if(m_formatVersionId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatVersionId"), m_formatVersionId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatName_Exist)
	{
	// String
	if(m_formatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatName"), m_formatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_format != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_format->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("format"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_format->Serialize(doc, element, &elem);
	}
	if (m_medium != EbuCoreformatType_medium_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_medium->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_imageFormat != EbuCoreformatType_imageFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_imageFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_videoFormat != EbuCoreformatType_videoFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_videoFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioFormat != EbuCoreformatType_audioFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioFormatExtended != EbuCoreformatType_audioFormatExtended_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioFormatExtended->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_containerFormat != EbuCoreformatType_containerFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_containerFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_signingFormat != EbuCoresigningFormatPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_signingFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("signingFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_signingFormat->Serialize(doc, element, &elem);
	}
	if (m_dataFormat != EbuCoreformatType_dataFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_dataFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_start != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_start->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("start"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_start->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_end != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_end->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("end"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_end->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_duration != EbuCoredurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_duration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("duration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_duration->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_fileInfo != EbuCorefileInfoPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_fileInfo->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("fileInfo"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_fileInfo->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_documentFormat != EbuCoredocumentFormatPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_documentFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("documentFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_documentFormat->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_technicalAttributes != EbuCoretechnicalAttributesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_technicalAttributes->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_technicalAttributes->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_dateCreated != EbuCoreformatType_dateCreated_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_dateCreated->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("dateCreated"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_dateCreated->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_dateModified != EbuCoreformatType_dateModified_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_dateModified->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("dateModified"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_dateModified->Serialize(doc, element, &elem);
	}

}

bool EbuCoreformatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatId")))
	{
		// Deserialize string type
		this->SetformatId(Dc1Convert::TextToString(parent->getAttribute(X("formatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatVersionId")))
	{
		// Deserialize string type
		this->SetformatVersionId(Dc1Convert::TextToString(parent->getAttribute(X("formatVersionId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatName")))
	{
		// Deserialize string type
		this->SetformatName(Dc1Convert::TextToString(parent->getAttribute(X("formatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("format"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("format")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setformat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("medium"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("medium")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_medium_CollectionPtr tmp = CreateformatType_medium_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setmedium(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("imageFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("imageFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_imageFormat_CollectionPtr tmp = CreateformatType_imageFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetimageFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("videoFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("videoFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_videoFormat_CollectionPtr tmp = CreateformatType_videoFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetvideoFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_audioFormat_CollectionPtr tmp = CreateformatType_audioFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioFormatExtended"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioFormatExtended")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_audioFormatExtended_CollectionPtr tmp = CreateformatType_audioFormatExtended_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioFormatExtended(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("containerFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("containerFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_containerFormat_CollectionPtr tmp = CreateformatType_containerFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetcontainerFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("signingFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("signingFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatesigningFormatType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetsigningFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("dataFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("dataFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreformatType_dataFormat_CollectionPtr tmp = CreateformatType_dataFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetdataFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("start"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("start")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setstart(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("end"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("end")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setend(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("duration"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("duration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setduration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("fileInfo"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("fileInfo")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatefileInfo; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetfileInfo(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("documentFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("documentFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedocumentFormatType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetdocumentFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("technicalAttributes"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("technicalAttributes")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetechnicalAttributes; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettechnicalAttributes(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("dateCreated"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("dateCreated")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateformatType_dateCreated_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetdateCreated(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("dateModified"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("dateModified")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateformatType_dateModified_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetdateModified(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreformatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreformatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("format")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setformat(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("medium")) == 0))
  {
	EbuCoreformatType_medium_CollectionPtr tmp = CreateformatType_medium_CollectionType; // FTT, check this
	this->Setmedium(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("imageFormat")) == 0))
  {
	EbuCoreformatType_imageFormat_CollectionPtr tmp = CreateformatType_imageFormat_CollectionType; // FTT, check this
	this->SetimageFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("videoFormat")) == 0))
  {
	EbuCoreformatType_videoFormat_CollectionPtr tmp = CreateformatType_videoFormat_CollectionType; // FTT, check this
	this->SetvideoFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioFormat")) == 0))
  {
	EbuCoreformatType_audioFormat_CollectionPtr tmp = CreateformatType_audioFormat_CollectionType; // FTT, check this
	this->SetaudioFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioFormatExtended")) == 0))
  {
	EbuCoreformatType_audioFormatExtended_CollectionPtr tmp = CreateformatType_audioFormatExtended_CollectionType; // FTT, check this
	this->SetaudioFormatExtended(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("containerFormat")) == 0))
  {
	EbuCoreformatType_containerFormat_CollectionPtr tmp = CreateformatType_containerFormat_CollectionType; // FTT, check this
	this->SetcontainerFormat(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("signingFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatesigningFormatType; // FTT, check this
	}
	this->SetsigningFormat(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("dataFormat")) == 0))
  {
	EbuCoreformatType_dataFormat_CollectionPtr tmp = CreateformatType_dataFormat_CollectionType; // FTT, check this
	this->SetdataFormat(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("start")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->Setstart(child);
  }
  if (XMLString::compareString(elementname, X("end")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->Setend(child);
  }
  if (XMLString::compareString(elementname, X("duration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedurationType; // FTT, check this
	}
	this->Setduration(child);
  }
  if (XMLString::compareString(elementname, X("fileInfo")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatefileInfo; // FTT, check this
	}
	this->SetfileInfo(child);
  }
  if (XMLString::compareString(elementname, X("documentFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedocumentFormatType; // FTT, check this
	}
	this->SetdocumentFormat(child);
  }
  if (XMLString::compareString(elementname, X("technicalAttributes")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetechnicalAttributes; // FTT, check this
	}
	this->SettechnicalAttributes(child);
  }
  if (XMLString::compareString(elementname, X("dateCreated")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateformatType_dateCreated_LocalType; // FTT, check this
	}
	this->SetdateCreated(child);
  }
  if (XMLString::compareString(elementname, X("dateModified")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateformatType_dateModified_LocalType; // FTT, check this
	}
	this->SetdateModified(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreformatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getformat() != Dc1NodePtr())
		result.Insert(Getformat());
	if (Getmedium() != Dc1NodePtr())
		result.Insert(Getmedium());
	if (GetimageFormat() != Dc1NodePtr())
		result.Insert(GetimageFormat());
	if (GetvideoFormat() != Dc1NodePtr())
		result.Insert(GetvideoFormat());
	if (GetaudioFormat() != Dc1NodePtr())
		result.Insert(GetaudioFormat());
	if (GetaudioFormatExtended() != Dc1NodePtr())
		result.Insert(GetaudioFormatExtended());
	if (GetcontainerFormat() != Dc1NodePtr())
		result.Insert(GetcontainerFormat());
	if (GetsigningFormat() != Dc1NodePtr())
		result.Insert(GetsigningFormat());
	if (GetdataFormat() != Dc1NodePtr())
		result.Insert(GetdataFormat());
	if (Getstart() != Dc1NodePtr())
		result.Insert(Getstart());
	if (Getend() != Dc1NodePtr())
		result.Insert(Getend());
	if (Getduration() != Dc1NodePtr())
		result.Insert(Getduration());
	if (GetfileInfo() != Dc1NodePtr())
		result.Insert(GetfileInfo());
	if (GetdocumentFormat() != Dc1NodePtr())
		result.Insert(GetdocumentFormat());
	if (GettechnicalAttributes() != Dc1NodePtr())
		result.Insert(GettechnicalAttributes());
	if (GetdateCreated() != Dc1NodePtr())
		result.Insert(GetdateCreated());
	if (GetdateModified() != Dc1NodePtr())
		result.Insert(GetdateModified());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreformatType_ExtMethodImpl.h


