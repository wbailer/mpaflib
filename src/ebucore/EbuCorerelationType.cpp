
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorerelationType_ExtImplInclude.h


#include "Dc1elementType.h"
#include "EbuCoreidentifierType.h"
#include "EbuCorerelationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorerelationType::IEbuCorerelationType()
{

// no includefile for extension defined 
// file EbuCorerelationType_ExtPropInit.h

}

IEbuCorerelationType::~IEbuCorerelationType()
{
// no includefile for extension defined 
// file EbuCorerelationType_ExtPropCleanup.h

}

EbuCorerelationType::EbuCorerelationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorerelationType::~EbuCorerelationType()
{
	Cleanup();
}

void EbuCorerelationType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_runningOrderNumber = 0; // Value
	m_runningOrderNumber_Exist = false;
	m_totalNumberOfGroupMembers = 0; // Value
	m_totalNumberOfGroupMembers_Exist = false;
	m_orderedGroupFlag = false; // Value
	m_orderedGroupFlag_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_relation = Dc1elementPtr(); // Class
	m_relationIdentifier = EbuCoreidentifierPtr(); // Class
	m_relationLink = XMLString::transcode(""); //  Mandatory String


// no includefile for extension defined 
// file EbuCorerelationType_ExtMyPropInit.h

}

void EbuCorerelationType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorerelationType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_relation);
	// Dc1Factory::DeleteObject(m_relationIdentifier);
	XMLString::release(&this->m_relationLink);
	this->m_relationLink = NULL;
}

void EbuCorerelationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use relationTypePtr, since we
	// might need GetBase(), which isn't defined in IrelationType
	const Dc1Ptr< EbuCorerelationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	if (tmp->ExistrunningOrderNumber())
	{
		this->SetrunningOrderNumber(tmp->GetrunningOrderNumber());
	}
	else
	{
		InvalidaterunningOrderNumber();
	}
	}
	{
	if (tmp->ExisttotalNumberOfGroupMembers())
	{
		this->SettotalNumberOfGroupMembers(tmp->GettotalNumberOfGroupMembers());
	}
	else
	{
		InvalidatetotalNumberOfGroupMembers();
	}
	}
	{
	if (tmp->ExistorderedGroupFlag())
	{
		this->SetorderedGroupFlag(tmp->GetorderedGroupFlag());
	}
	else
	{
		InvalidateorderedGroupFlag();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_relation);
		this->Setrelation(Dc1Factory::CloneObject(tmp->Getrelation()));
		// Dc1Factory::DeleteObject(m_relationIdentifier);
		this->SetrelationIdentifier(Dc1Factory::CloneObject(tmp->GetrelationIdentifier()));
		this->SetrelationLink(XMLString::replicate(tmp->GetrelationLink()));
}

Dc1NodePtr EbuCorerelationType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorerelationType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorerelationType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorerelationType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorerelationType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorerelationType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorerelationType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorerelationType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorerelationType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorerelationType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorerelationType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorerelationType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorerelationType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorerelationType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorerelationType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorerelationType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorerelationType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
int EbuCorerelationType::GetrunningOrderNumber() const
{
	return m_runningOrderNumber;
}

bool EbuCorerelationType::ExistrunningOrderNumber() const
{
	return m_runningOrderNumber_Exist;
}
void EbuCorerelationType::SetrunningOrderNumber(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SetrunningOrderNumber().");
	}
	m_runningOrderNumber = item;
	m_runningOrderNumber_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidaterunningOrderNumber()
{
	m_runningOrderNumber_Exist = false;
}
int EbuCorerelationType::GettotalNumberOfGroupMembers() const
{
	return m_totalNumberOfGroupMembers;
}

bool EbuCorerelationType::ExisttotalNumberOfGroupMembers() const
{
	return m_totalNumberOfGroupMembers_Exist;
}
void EbuCorerelationType::SettotalNumberOfGroupMembers(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SettotalNumberOfGroupMembers().");
	}
	m_totalNumberOfGroupMembers = item;
	m_totalNumberOfGroupMembers_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidatetotalNumberOfGroupMembers()
{
	m_totalNumberOfGroupMembers_Exist = false;
}
bool EbuCorerelationType::GetorderedGroupFlag() const
{
	return m_orderedGroupFlag;
}

bool EbuCorerelationType::ExistorderedGroupFlag() const
{
	return m_orderedGroupFlag_Exist;
}
void EbuCorerelationType::SetorderedGroupFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SetorderedGroupFlag().");
	}
	m_orderedGroupFlag = item;
	m_orderedGroupFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::InvalidateorderedGroupFlag()
{
	m_orderedGroupFlag_Exist = false;
}
XMLCh * EbuCorerelationType::Getnote() const
{
	return m_note;
}

bool EbuCorerelationType::Existnote() const
{
	return m_note_Exist;
}
void EbuCorerelationType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerelationType::Invalidatenote()
{
	m_note_Exist = false;
}
Dc1elementPtr EbuCorerelationType::Getrelation() const
{
		return m_relation;
}

EbuCoreidentifierPtr EbuCorerelationType::GetrelationIdentifier() const
{
		return m_relationIdentifier;
}

XMLCh * EbuCorerelationType::GetrelationLink() const
{
		return m_relationLink;
}

// implementing setter for choice 
/* element */
void EbuCorerelationType::Setrelation(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::Setrelation().");
	}
	if (m_relation != item)
	{
		// Dc1Factory::DeleteObject(m_relation);
		m_relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relation) m_relation->SetParent(m_myself.getPointer());
		if (m_relation && m_relation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_relation->UseTypeAttribute = true;
		}
		if(m_relation != Dc1elementPtr())
		{
			m_relation->SetContentName(XMLString::transcode("relation"));
		}
	// Dc1Factory::DeleteObject(m_relationIdentifier);
	m_relationIdentifier = EbuCoreidentifierPtr();
	XMLString::release(&this->m_relationLink);
	m_relationLink = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCorerelationType::SetrelationIdentifier(const EbuCoreidentifierPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SetrelationIdentifier().");
	}
	if (m_relationIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_relationIdentifier);
		m_relationIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relationIdentifier) m_relationIdentifier->SetParent(m_myself.getPointer());
		if (m_relationIdentifier && m_relationIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_relationIdentifier->UseTypeAttribute = true;
		}
		if(m_relationIdentifier != EbuCoreidentifierPtr())
		{
			m_relationIdentifier->SetContentName(XMLString::transcode("relationIdentifier"));
		}
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	XMLString::release(&this->m_relationLink);
	m_relationLink = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCorerelationType::SetrelationLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerelationType::SetrelationLink().");
	}
	if (m_relationLink != item)
	{
		XMLString::release(&m_relationLink);
		m_relationLink = item;
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relationIdentifier);
	m_relationIdentifier = EbuCoreidentifierPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorerelationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  9, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("runningOrderNumber")) == 0)
	{
		// runningOrderNumber is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("totalNumberOfGroupMembers")) == 0)
	{
		// totalNumberOfGroupMembers is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("orderedGroupFlag")) == 0)
	{
		// orderedGroupFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relation")) == 0)
	{
		// relation is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getrelation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setrelation(p, client);
					if((p = Getrelation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relationIdentifier")) == 0)
	{
		// relationIdentifier is simple element identifierType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetrelationIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))) != empty)
			{
				// Is type allowed
				EbuCoreidentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetrelationIdentifier(p, client);
					if((p = GetrelationIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for relationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "relationType");
	}
	return result;
}

XMLCh * EbuCorerelationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorerelationType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorerelationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("relationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_runningOrderNumber_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_runningOrderNumber);
		element->setAttributeNS(X(""), X("runningOrderNumber"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_totalNumberOfGroupMembers_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_totalNumberOfGroupMembers);
		element->setAttributeNS(X(""), X("totalNumberOfGroupMembers"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_orderedGroupFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_orderedGroupFlag);
		element->setAttributeNS(X(""), X("orderedGroupFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_relation != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_relation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("relation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_relation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_relationIdentifier != EbuCoreidentifierPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_relationIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("relationIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_relationIdentifier->Serialize(doc, element, &elem);
	}
	 // String
	if(m_relationLink != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_relationLink, X("urn:ebu:metadata-schema:ebuCore_2014"), X("relationLink"), false);

}

bool EbuCorerelationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("runningOrderNumber")))
	{
		// deserialize value type
		this->SetrunningOrderNumber(Dc1Convert::TextToInt(parent->getAttribute(X("runningOrderNumber"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("totalNumberOfGroupMembers")))
	{
		// deserialize value type
		this->SettotalNumberOfGroupMembers(Dc1Convert::TextToInt(parent->getAttribute(X("totalNumberOfGroupMembers"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("orderedGroupFlag")))
	{
		// deserialize value type
		this->SetorderedGroupFlag(Dc1Convert::TextToBool(parent->getAttribute(X("orderedGroupFlag"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("relation"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("relation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setrelation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("relationIdentifier"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("relationIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateidentifierType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetrelationIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("relationLink"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetrelationLink(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetrelationLink(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	} while(false);
// no includefile for extension defined 
// file EbuCorerelationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorerelationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("relation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setrelation(child);
  }
  if (XMLString::compareString(elementname, X("relationIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateidentifierType; // FTT, check this
	}
	this->SetrelationIdentifier(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorerelationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getrelation() != Dc1NodePtr())
		result.Insert(Getrelation());
	if (GetrelationIdentifier() != Dc1NodePtr())
		result.Insert(GetrelationIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorerelationType_ExtMethodImpl.h


