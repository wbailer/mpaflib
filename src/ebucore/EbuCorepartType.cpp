
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorepartType_ExtImplInclude.h


#include "EbuCorecoreMetadataType.h"
#include "EbuCoretimeType.h"
#include "EbuCoredurationType.h"
#include "EbuCorecoreMetadataType_title_CollectionType.h"
#include "EbuCorecoreMetadataType_alternativeTitle_CollectionType.h"
#include "EbuCorecoreMetadataType_creator_CollectionType.h"
#include "EbuCorecoreMetadataType_subject_CollectionType.h"
#include "EbuCorecoreMetadataType_description_CollectionType.h"
#include "EbuCorecoreMetadataType_publisher_CollectionType.h"
#include "EbuCorecoreMetadataType_contributor_CollectionType.h"
#include "EbuCorecoreMetadataType_date_CollectionType.h"
#include "EbuCorecoreMetadataType_type_CollectionType.h"
#include "EbuCorecoreMetadataType_format_CollectionType.h"
#include "EbuCorecoreMetadataType_identifier_CollectionType.h"
#include "EbuCorecoreMetadataType_source_CollectionType.h"
#include "EbuCorecoreMetadataType_language_CollectionType.h"
#include "EbuCorecoreMetadataType_relation_CollectionType.h"
#include "EbuCorecoreMetadataType_isRelatedTo_CollectionType.h"
#include "EbuCorecoreMetadataType_isNextInSequence_CollectionType.h"
#include "EbuCorecoreMetadataType_isVersionOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasVersion_CollectionType.h"
#include "EbuCorecoreMetadataType_isReplacedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_replaces_CollectionType.h"
#include "EbuCorecoreMetadataType_isRequiredBy_CollectionType.h"
#include "EbuCorecoreMetadataType_requires_CollectionType.h"
#include "EbuCorecoreMetadataType_isPartOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasPart_CollectionType.h"
#include "EbuCorecoreMetadataType_hasTrackPart_CollectionType.h"
#include "EbuCorecoreMetadataType_isReferencedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_references_CollectionType.h"
#include "EbuCorecoreMetadataType_isFormatOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasFormat_CollectionType.h"
#include "EbuCorecoreMetadataType_isEpisodeOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isSeasonOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasEpisode_CollectionType.h"
#include "EbuCorecoreMetadataType_hasSeason_CollectionType.h"
#include "EbuCorecoreMetadataType_isMemberOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasMember_CollectionType.h"
#include "EbuCorecoreMetadataType_sameAs_CollectionType.h"
#include "EbuCorecoreMetadataType_coverage_CollectionType.h"
#include "EbuCorecoreMetadataType_rights_CollectionType.h"
#include "EbuCorecoreMetadataType_version_CollectionType.h"
#include "EbuCorecoreMetadataType_publicationHistory_CollectionType.h"
#include "EbuCorecoreMetadataType_planning_CollectionType.h"
#include "EbuCorecoreMetadataType_rating_CollectionType.h"
#include "EbuCorecoreMetadataType_part_CollectionType.h"
#include "EbuCorepartType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoretitleType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:title
#include "EbuCorealternativeTitleType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:alternativeTitle
#include "EbuCoreentityType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:creator
#include "EbuCoresubjectType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:subject
#include "EbuCoredescriptionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:description
#include "EbuCoredateType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:date
#include "EbuCoretypeType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:type
#include "EbuCoreformatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:format
#include "EbuCoreidentifierType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:identifier
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:source
#include "EbuCorelanguageType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:language
#include "EbuCorerelationType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:relation
#include "EbuCorecoreMetadataType_hasTrackPart_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:hasTrackPart
#include "EbuCorecoverageType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:coverage
#include "EbuCorerightsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rights
#include "EbuCoreversionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:version
#include "EbuCorepublicationHistoryType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:publicationHistory
#include "EbuCoreplanningType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:planning
#include "EbuCoreratingType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rating
#include "EbuCorepartType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:part

#include <assert.h>
IEbuCorepartType::IEbuCorepartType()
{

// no includefile for extension defined 
// file EbuCorepartType_ExtPropInit.h

}

IEbuCorepartType::~IEbuCorepartType()
{
// no includefile for extension defined 
// file EbuCorepartType_ExtPropCleanup.h

}

EbuCorepartType::EbuCorepartType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorepartType::~EbuCorepartType()
{
	Cleanup();
}

void EbuCorepartType::Init()
{
	// Init base
	m_Base = CreatecoreMetadataType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_partId = NULL; // String
	m_partId_Exist = false;
	m_partName = NULL; // String
	m_partName_Exist = false;
	m_partDefinition = NULL; // String
	m_partDefinition_Exist = false;
	m_partNumber = 0; // Value
	m_partNumber_Exist = false;
	m_partTotalNumber = 0; // Value
	m_partTotalNumber_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_partStartTime = EbuCoretimePtr(); // Class
	m_partStartTime_Exist = false;
	m_partDuration = EbuCoredurationPtr(); // Class
	m_partDuration_Exist = false;


// no includefile for extension defined 
// file EbuCorepartType_ExtMyPropInit.h

}

void EbuCorepartType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorepartType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_partId); // String
	XMLString::release(&m_partName); // String
	XMLString::release(&m_partDefinition); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_partStartTime);
	// Dc1Factory::DeleteObject(m_partDuration);
}

void EbuCorepartType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use partTypePtr, since we
	// might need GetBase(), which isn't defined in IpartType
	const Dc1Ptr< EbuCorepartType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = EbuCorecoreMetadataPtr(Dc1Factory::CloneObject((dynamic_cast< const EbuCorepartType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_partId); // String
	if (tmp->ExistpartId())
	{
		this->SetpartId(XMLString::replicate(tmp->GetpartId()));
	}
	else
	{
		InvalidatepartId();
	}
	}
	{
	XMLString::release(&m_partName); // String
	if (tmp->ExistpartName())
	{
		this->SetpartName(XMLString::replicate(tmp->GetpartName()));
	}
	else
	{
		InvalidatepartName();
	}
	}
	{
	XMLString::release(&m_partDefinition); // String
	if (tmp->ExistpartDefinition())
	{
		this->SetpartDefinition(XMLString::replicate(tmp->GetpartDefinition()));
	}
	else
	{
		InvalidatepartDefinition();
	}
	}
	{
	if (tmp->ExistpartNumber())
	{
		this->SetpartNumber(tmp->GetpartNumber());
	}
	else
	{
		InvalidatepartNumber();
	}
	}
	{
	if (tmp->ExistpartTotalNumber())
	{
		this->SetpartTotalNumber(tmp->GetpartTotalNumber());
	}
	else
	{
		InvalidatepartTotalNumber();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	if (tmp->IsValidpartStartTime())
	{
		// Dc1Factory::DeleteObject(m_partStartTime);
		this->SetpartStartTime(Dc1Factory::CloneObject(tmp->GetpartStartTime()));
	}
	else
	{
		InvalidatepartStartTime();
	}
	if (tmp->IsValidpartDuration())
	{
		// Dc1Factory::DeleteObject(m_partDuration);
		this->SetpartDuration(Dc1Factory::CloneObject(tmp->GetpartDuration()));
	}
	else
	{
		InvalidatepartDuration();
	}
}

Dc1NodePtr EbuCorepartType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr EbuCorepartType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< EbuCorecoreMetadataType > EbuCorepartType::GetBase() const
{
	return m_Base;
}

XMLCh * EbuCorepartType::GetpartId() const
{
	return m_partId;
}

bool EbuCorepartType::ExistpartId() const
{
	return m_partId_Exist;
}
void EbuCorepartType::SetpartId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartId().");
	}
	m_partId = item;
	m_partId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartId()
{
	m_partId_Exist = false;
}
XMLCh * EbuCorepartType::GetpartName() const
{
	return m_partName;
}

bool EbuCorepartType::ExistpartName() const
{
	return m_partName_Exist;
}
void EbuCorepartType::SetpartName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartName().");
	}
	m_partName = item;
	m_partName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartName()
{
	m_partName_Exist = false;
}
XMLCh * EbuCorepartType::GetpartDefinition() const
{
	return m_partDefinition;
}

bool EbuCorepartType::ExistpartDefinition() const
{
	return m_partDefinition_Exist;
}
void EbuCorepartType::SetpartDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartDefinition().");
	}
	m_partDefinition = item;
	m_partDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartDefinition()
{
	m_partDefinition_Exist = false;
}
int EbuCorepartType::GetpartNumber() const
{
	return m_partNumber;
}

bool EbuCorepartType::ExistpartNumber() const
{
	return m_partNumber_Exist;
}
void EbuCorepartType::SetpartNumber(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartNumber().");
	}
	m_partNumber = item;
	m_partNumber_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartNumber()
{
	m_partNumber_Exist = false;
}
int EbuCorepartType::GetpartTotalNumber() const
{
	return m_partTotalNumber;
}

bool EbuCorepartType::ExistpartTotalNumber() const
{
	return m_partTotalNumber_Exist;
}
void EbuCorepartType::SetpartTotalNumber(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartTotalNumber().");
	}
	m_partTotalNumber = item;
	m_partTotalNumber_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartTotalNumber()
{
	m_partTotalNumber_Exist = false;
}
XMLCh * EbuCorepartType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorepartType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorepartType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorepartType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorepartType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorepartType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorepartType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorepartType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorepartType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorepartType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorepartType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorepartType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorepartType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorepartType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorepartType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
EbuCoretimePtr EbuCorepartType::GetpartStartTime() const
{
		return m_partStartTime;
}

// Element is optional
bool EbuCorepartType::IsValidpartStartTime() const
{
	return m_partStartTime_Exist;
}

EbuCoredurationPtr EbuCorepartType::GetpartDuration() const
{
		return m_partDuration;
}

// Element is optional
bool EbuCorepartType::IsValidpartDuration() const
{
	return m_partDuration_Exist;
}

void EbuCorepartType::SetpartStartTime(const EbuCoretimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartStartTime().");
	}
	if (m_partStartTime != item || m_partStartTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_partStartTime);
		m_partStartTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_partStartTime) m_partStartTime->SetParent(m_myself.getPointer());
		if (m_partStartTime && m_partStartTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_partStartTime->UseTypeAttribute = true;
		}
		m_partStartTime_Exist = true;
		if(m_partStartTime != EbuCoretimePtr())
		{
			m_partStartTime->SetContentName(XMLString::transcode("partStartTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartStartTime()
{
	m_partStartTime_Exist = false;
}
void EbuCorepartType::SetpartDuration(const EbuCoredurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpartDuration().");
	}
	if (m_partDuration != item || m_partDuration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_partDuration);
		m_partDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_partDuration) m_partDuration->SetParent(m_myself.getPointer());
		if (m_partDuration && m_partDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_partDuration->UseTypeAttribute = true;
		}
		m_partDuration_Exist = true;
		if(m_partDuration != EbuCoredurationPtr())
		{
			m_partDuration->SetContentName(XMLString::transcode("partDuration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::InvalidatepartDuration()
{
	m_partDuration_Exist = false;
}
EbuCorecoreMetadataType_title_CollectionPtr EbuCorepartType::Gettitle() const
{
	return GetBase()->Gettitle();
}

EbuCorecoreMetadataType_alternativeTitle_CollectionPtr EbuCorepartType::GetalternativeTitle() const
{
	return GetBase()->GetalternativeTitle();
}

EbuCorecoreMetadataType_creator_CollectionPtr EbuCorepartType::Getcreator() const
{
	return GetBase()->Getcreator();
}

EbuCorecoreMetadataType_subject_CollectionPtr EbuCorepartType::Getsubject() const
{
	return GetBase()->Getsubject();
}

EbuCorecoreMetadataType_description_CollectionPtr EbuCorepartType::Getdescription() const
{
	return GetBase()->Getdescription();
}

EbuCorecoreMetadataType_publisher_CollectionPtr EbuCorepartType::Getpublisher() const
{
	return GetBase()->Getpublisher();
}

EbuCorecoreMetadataType_contributor_CollectionPtr EbuCorepartType::Getcontributor() const
{
	return GetBase()->Getcontributor();
}

EbuCorecoreMetadataType_date_CollectionPtr EbuCorepartType::Getdate() const
{
	return GetBase()->Getdate();
}

EbuCorecoreMetadataType_type_CollectionPtr EbuCorepartType::Gettype() const
{
	return GetBase()->Gettype();
}

EbuCorecoreMetadataType_format_CollectionPtr EbuCorepartType::Getformat() const
{
	return GetBase()->Getformat();
}

EbuCorecoreMetadataType_identifier_CollectionPtr EbuCorepartType::Getidentifier() const
{
	return GetBase()->Getidentifier();
}

EbuCorecoreMetadataType_source_CollectionPtr EbuCorepartType::Getsource() const
{
	return GetBase()->Getsource();
}

EbuCorecoreMetadataType_language_CollectionPtr EbuCorepartType::Getlanguage() const
{
	return GetBase()->Getlanguage();
}

EbuCorecoreMetadataType_relation_CollectionPtr EbuCorepartType::Getrelation() const
{
	return GetBase()->Getrelation();
}

EbuCorecoreMetadataType_isRelatedTo_CollectionPtr EbuCorepartType::GetisRelatedTo() const
{
	return GetBase()->GetisRelatedTo();
}

EbuCorecoreMetadataType_isNextInSequence_CollectionPtr EbuCorepartType::GetisNextInSequence() const
{
	return GetBase()->GetisNextInSequence();
}

EbuCorecoreMetadataType_isVersionOf_CollectionPtr EbuCorepartType::GetisVersionOf() const
{
	return GetBase()->GetisVersionOf();
}

EbuCorecoreMetadataType_hasVersion_CollectionPtr EbuCorepartType::GethasVersion() const
{
	return GetBase()->GethasVersion();
}

EbuCorecoreMetadataType_isReplacedBy_CollectionPtr EbuCorepartType::GetisReplacedBy() const
{
	return GetBase()->GetisReplacedBy();
}

EbuCorecoreMetadataType_replaces_CollectionPtr EbuCorepartType::Getreplaces() const
{
	return GetBase()->Getreplaces();
}

EbuCorecoreMetadataType_isRequiredBy_CollectionPtr EbuCorepartType::GetisRequiredBy() const
{
	return GetBase()->GetisRequiredBy();
}

EbuCorecoreMetadataType_requires_CollectionPtr EbuCorepartType::Getrequires() const
{
	return GetBase()->Getrequires();
}

EbuCorecoreMetadataType_isPartOf_CollectionPtr EbuCorepartType::GetisPartOf() const
{
	return GetBase()->GetisPartOf();
}

EbuCorecoreMetadataType_hasPart_CollectionPtr EbuCorepartType::GethasPart() const
{
	return GetBase()->GethasPart();
}

EbuCorecoreMetadataType_hasTrackPart_CollectionPtr EbuCorepartType::GethasTrackPart() const
{
	return GetBase()->GethasTrackPart();
}

EbuCorecoreMetadataType_isReferencedBy_CollectionPtr EbuCorepartType::GetisReferencedBy() const
{
	return GetBase()->GetisReferencedBy();
}

EbuCorecoreMetadataType_references_CollectionPtr EbuCorepartType::Getreferences() const
{
	return GetBase()->Getreferences();
}

EbuCorecoreMetadataType_isFormatOf_CollectionPtr EbuCorepartType::GetisFormatOf() const
{
	return GetBase()->GetisFormatOf();
}

EbuCorecoreMetadataType_hasFormat_CollectionPtr EbuCorepartType::GethasFormat() const
{
	return GetBase()->GethasFormat();
}

EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr EbuCorepartType::GetisEpisodeOf() const
{
	return GetBase()->GetisEpisodeOf();
}

EbuCorecoreMetadataType_isSeasonOf_CollectionPtr EbuCorepartType::GetisSeasonOf() const
{
	return GetBase()->GetisSeasonOf();
}

EbuCorecoreMetadataType_hasEpisode_CollectionPtr EbuCorepartType::GethasEpisode() const
{
	return GetBase()->GethasEpisode();
}

EbuCorecoreMetadataType_hasSeason_CollectionPtr EbuCorepartType::GethasSeason() const
{
	return GetBase()->GethasSeason();
}

EbuCorecoreMetadataType_isMemberOf_CollectionPtr EbuCorepartType::GetisMemberOf() const
{
	return GetBase()->GetisMemberOf();
}

EbuCorecoreMetadataType_hasMember_CollectionPtr EbuCorepartType::GethasMember() const
{
	return GetBase()->GethasMember();
}

EbuCorecoreMetadataType_sameAs_CollectionPtr EbuCorepartType::GetsameAs() const
{
	return GetBase()->GetsameAs();
}

EbuCorecoreMetadataType_coverage_CollectionPtr EbuCorepartType::Getcoverage() const
{
	return GetBase()->Getcoverage();
}

EbuCorecoreMetadataType_rights_CollectionPtr EbuCorepartType::Getrights() const
{
	return GetBase()->Getrights();
}

EbuCorecoreMetadataType_version_CollectionPtr EbuCorepartType::Getversion() const
{
	return GetBase()->Getversion();
}

EbuCorecoreMetadataType_publicationHistory_CollectionPtr EbuCorepartType::GetpublicationHistory() const
{
	return GetBase()->GetpublicationHistory();
}

EbuCorecoreMetadataType_planning_CollectionPtr EbuCorepartType::Getplanning() const
{
	return GetBase()->Getplanning();
}

EbuCorecoreMetadataType_rating_CollectionPtr EbuCorepartType::Getrating() const
{
	return GetBase()->Getrating();
}

EbuCorecoreMetadataType_part_CollectionPtr EbuCorepartType::Getpart() const
{
	return GetBase()->Getpart();
}

void EbuCorepartType::Settitle(const EbuCorecoreMetadataType_title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Settitle().");
	}
	GetBase()->Settitle(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetalternativeTitle(const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetalternativeTitle().");
	}
	GetBase()->SetalternativeTitle(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setcreator(const EbuCorecoreMetadataType_creator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setcreator().");
	}
	GetBase()->Setcreator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setsubject(const EbuCorecoreMetadataType_subject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setsubject().");
	}
	GetBase()->Setsubject(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setdescription(const EbuCorecoreMetadataType_description_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setdescription().");
	}
	GetBase()->Setdescription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setpublisher(const EbuCorecoreMetadataType_publisher_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setpublisher().");
	}
	GetBase()->Setpublisher(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setcontributor(const EbuCorecoreMetadataType_contributor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setcontributor().");
	}
	GetBase()->Setcontributor(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setdate(const EbuCorecoreMetadataType_date_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setdate().");
	}
	GetBase()->Setdate(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Settype(const EbuCorecoreMetadataType_type_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setformat(const EbuCorecoreMetadataType_format_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setformat().");
	}
	GetBase()->Setformat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setidentifier(const EbuCorecoreMetadataType_identifier_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setidentifier().");
	}
	GetBase()->Setidentifier(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setsource(const EbuCorecoreMetadataType_source_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setsource().");
	}
	GetBase()->Setsource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setlanguage(const EbuCorecoreMetadataType_language_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setlanguage().");
	}
	GetBase()->Setlanguage(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setrelation(const EbuCorecoreMetadataType_relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setrelation().");
	}
	GetBase()->Setrelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisRelatedTo(const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisRelatedTo().");
	}
	GetBase()->SetisRelatedTo(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisNextInSequence(const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisNextInSequence().");
	}
	GetBase()->SetisNextInSequence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisVersionOf(const EbuCorecoreMetadataType_isVersionOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisVersionOf().");
	}
	GetBase()->SetisVersionOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasVersion(const EbuCorecoreMetadataType_hasVersion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasVersion().");
	}
	GetBase()->SethasVersion(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisReplacedBy(const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisReplacedBy().");
	}
	GetBase()->SetisReplacedBy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setreplaces(const EbuCorecoreMetadataType_replaces_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setreplaces().");
	}
	GetBase()->Setreplaces(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisRequiredBy(const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisRequiredBy().");
	}
	GetBase()->SetisRequiredBy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setrequires(const EbuCorecoreMetadataType_requires_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setrequires().");
	}
	GetBase()->Setrequires(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisPartOf(const EbuCorecoreMetadataType_isPartOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisPartOf().");
	}
	GetBase()->SetisPartOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasPart(const EbuCorecoreMetadataType_hasPart_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasPart().");
	}
	GetBase()->SethasPart(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasTrackPart(const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasTrackPart().");
	}
	GetBase()->SethasTrackPart(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisReferencedBy(const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisReferencedBy().");
	}
	GetBase()->SetisReferencedBy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setreferences(const EbuCorecoreMetadataType_references_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setreferences().");
	}
	GetBase()->Setreferences(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisFormatOf(const EbuCorecoreMetadataType_isFormatOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisFormatOf().");
	}
	GetBase()->SetisFormatOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasFormat(const EbuCorecoreMetadataType_hasFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasFormat().");
	}
	GetBase()->SethasFormat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisEpisodeOf(const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisEpisodeOf().");
	}
	GetBase()->SetisEpisodeOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisSeasonOf(const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisSeasonOf().");
	}
	GetBase()->SetisSeasonOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasEpisode(const EbuCorecoreMetadataType_hasEpisode_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasEpisode().");
	}
	GetBase()->SethasEpisode(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasSeason(const EbuCorecoreMetadataType_hasSeason_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasSeason().");
	}
	GetBase()->SethasSeason(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetisMemberOf(const EbuCorecoreMetadataType_isMemberOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetisMemberOf().");
	}
	GetBase()->SetisMemberOf(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SethasMember(const EbuCorecoreMetadataType_hasMember_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SethasMember().");
	}
	GetBase()->SethasMember(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetsameAs(const EbuCorecoreMetadataType_sameAs_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetsameAs().");
	}
	GetBase()->SetsameAs(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setcoverage(const EbuCorecoreMetadataType_coverage_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setcoverage().");
	}
	GetBase()->Setcoverage(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setrights(const EbuCorecoreMetadataType_rights_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setrights().");
	}
	GetBase()->Setrights(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setversion(const EbuCorecoreMetadataType_version_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setversion().");
	}
	GetBase()->Setversion(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::SetpublicationHistory(const EbuCorecoreMetadataType_publicationHistory_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::SetpublicationHistory().");
	}
	GetBase()->SetpublicationHistory(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setplanning(const EbuCorecoreMetadataType_planning_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setplanning().");
	}
	GetBase()->Setplanning(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setrating(const EbuCorecoreMetadataType_rating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setrating().");
	}
	GetBase()->Setrating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepartType::Setpart(const EbuCorecoreMetadataType_part_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepartType::Setpart().");
	}
	GetBase()->Setpart(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorepartType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  10, 10 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("partId")) == 0)
	{
		// partId is simple attribute NMTOKEN
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("partName")) == 0)
	{
		// partName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("partDefinition")) == 0)
	{
		// partDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("partNumber")) == 0)
	{
		// partNumber is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("partTotalNumber")) == 0)
	{
		// partTotalNumber is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("partStartTime")) == 0)
	{
		// partStartTime is simple element timeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpartStartTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpartStartTime(p, client);
					if((p = GetpartStartTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("partDuration")) == 0)
	{
		// partDuration is simple element durationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpartDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType")))) != empty)
			{
				// Is type allowed
				EbuCoredurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpartDuration(p, client);
					if((p = GetpartDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for partType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "partType");
	}
	return result;
}

XMLCh * EbuCorepartType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorepartType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorepartType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file EbuCorepartType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("partType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_partId_Exist)
	{
	// String
	if(m_partId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("partId"), m_partId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_partName_Exist)
	{
	// String
	if(m_partName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("partName"), m_partName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_partDefinition_Exist)
	{
	// String
	if(m_partDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("partDefinition"), m_partDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_partNumber_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_partNumber);
		element->setAttributeNS(X(""), X("partNumber"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_partTotalNumber_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_partTotalNumber);
		element->setAttributeNS(X(""), X("partTotalNumber"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_partStartTime != EbuCoretimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_partStartTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("partStartTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_partStartTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_partDuration != EbuCoredurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_partDuration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("partDuration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_partDuration->Serialize(doc, element, &elem);
	}

}

bool EbuCorepartType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("partId")))
	{
		// Deserialize string type
		this->SetpartId(Dc1Convert::TextToString(parent->getAttribute(X("partId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("partName")))
	{
		// Deserialize string type
		this->SetpartName(Dc1Convert::TextToString(parent->getAttribute(X("partName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("partDefinition")))
	{
		// Deserialize string type
		this->SetpartDefinition(Dc1Convert::TextToString(parent->getAttribute(X("partDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("partNumber")))
	{
		// deserialize value type
		this->SetpartNumber(Dc1Convert::TextToInt(parent->getAttribute(X("partNumber"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("partTotalNumber")))
	{
		// deserialize value type
		this->SetpartTotalNumber(Dc1Convert::TextToInt(parent->getAttribute(X("partTotalNumber"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Extensionbase is EbuCorecoreMetadataType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("partStartTime"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("partStartTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpartStartTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("partDuration"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("partDuration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpartDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorepartType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorepartType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("partStartTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType; // FTT, check this
	}
	this->SetpartStartTime(child);
  }
  if (XMLString::compareString(elementname, X("partDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedurationType; // FTT, check this
	}
	this->SetpartDuration(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorepartType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetpartStartTime() != Dc1NodePtr())
		result.Insert(GetpartStartTime());
	if (GetpartDuration() != Dc1NodePtr())
		result.Insert(GetpartDuration());
	if (Gettitle() != Dc1NodePtr())
		result.Insert(Gettitle());
	if (GetalternativeTitle() != Dc1NodePtr())
		result.Insert(GetalternativeTitle());
	if (Getcreator() != Dc1NodePtr())
		result.Insert(Getcreator());
	if (Getsubject() != Dc1NodePtr())
		result.Insert(Getsubject());
	if (Getdescription() != Dc1NodePtr())
		result.Insert(Getdescription());
	if (Getpublisher() != Dc1NodePtr())
		result.Insert(Getpublisher());
	if (Getcontributor() != Dc1NodePtr())
		result.Insert(Getcontributor());
	if (Getdate() != Dc1NodePtr())
		result.Insert(Getdate());
	if (Gettype() != Dc1NodePtr())
		result.Insert(Gettype());
	if (Getformat() != Dc1NodePtr())
		result.Insert(Getformat());
	if (Getidentifier() != Dc1NodePtr())
		result.Insert(Getidentifier());
	if (Getsource() != Dc1NodePtr())
		result.Insert(Getsource());
	if (Getlanguage() != Dc1NodePtr())
		result.Insert(Getlanguage());
	if (Getrelation() != Dc1NodePtr())
		result.Insert(Getrelation());
	if (GetisRelatedTo() != Dc1NodePtr())
		result.Insert(GetisRelatedTo());
	if (GetisNextInSequence() != Dc1NodePtr())
		result.Insert(GetisNextInSequence());
	if (GetisVersionOf() != Dc1NodePtr())
		result.Insert(GetisVersionOf());
	if (GethasVersion() != Dc1NodePtr())
		result.Insert(GethasVersion());
	if (GetisReplacedBy() != Dc1NodePtr())
		result.Insert(GetisReplacedBy());
	if (Getreplaces() != Dc1NodePtr())
		result.Insert(Getreplaces());
	if (GetisRequiredBy() != Dc1NodePtr())
		result.Insert(GetisRequiredBy());
	if (Getrequires() != Dc1NodePtr())
		result.Insert(Getrequires());
	if (GetisPartOf() != Dc1NodePtr())
		result.Insert(GetisPartOf());
	if (GethasPart() != Dc1NodePtr())
		result.Insert(GethasPart());
	if (GethasTrackPart() != Dc1NodePtr())
		result.Insert(GethasTrackPart());
	if (GetisReferencedBy() != Dc1NodePtr())
		result.Insert(GetisReferencedBy());
	if (Getreferences() != Dc1NodePtr())
		result.Insert(Getreferences());
	if (GetisFormatOf() != Dc1NodePtr())
		result.Insert(GetisFormatOf());
	if (GethasFormat() != Dc1NodePtr())
		result.Insert(GethasFormat());
	if (GetisEpisodeOf() != Dc1NodePtr())
		result.Insert(GetisEpisodeOf());
	if (GetisSeasonOf() != Dc1NodePtr())
		result.Insert(GetisSeasonOf());
	if (GethasEpisode() != Dc1NodePtr())
		result.Insert(GethasEpisode());
	if (GethasSeason() != Dc1NodePtr())
		result.Insert(GethasSeason());
	if (GetisMemberOf() != Dc1NodePtr())
		result.Insert(GetisMemberOf());
	if (GethasMember() != Dc1NodePtr())
		result.Insert(GethasMember());
	if (GetsameAs() != Dc1NodePtr())
		result.Insert(GetsameAs());
	if (Getcoverage() != Dc1NodePtr())
		result.Insert(Getcoverage());
	if (Getrights() != Dc1NodePtr())
		result.Insert(Getrights());
	if (Getversion() != Dc1NodePtr())
		result.Insert(Getversion());
	if (GetpublicationHistory() != Dc1NodePtr())
		result.Insert(GetpublicationHistory());
	if (Getplanning() != Dc1NodePtr())
		result.Insert(Getplanning());
	if (Getrating() != Dc1NodePtr())
		result.Insert(Getrating());
	if (Getpart() != Dc1NodePtr())
		result.Insert(Getpart());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorepartType_ExtMethodImpl.h


