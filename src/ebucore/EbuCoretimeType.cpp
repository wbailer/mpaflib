
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoretimeType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "EbuCoretimecodeType.h"
#include "W3Ctime.h"
#include "EbuCoreeditUnitNumberType.h"
#include "EbuCoretimeType_time_LocalType.h"
#include "EbuCoretimeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoretimeType::IEbuCoretimeType()
{

// no includefile for extension defined 
// file EbuCoretimeType_ExtPropInit.h

}

IEbuCoretimeType::~IEbuCoretimeType()
{
// no includefile for extension defined 
// file EbuCoretimeType_ExtPropCleanup.h

}

EbuCoretimeType::EbuCoretimeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoretimeType::~EbuCoretimeType()
{
	Cleanup();
}

void EbuCoretimeType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_timecode = EbuCoretimecodePtr(); // Pattern
	m_normalPlayTime = W3CtimePtr(); // Pattern
	m_editUnitNumber = EbuCoreeditUnitNumberPtr(); // Class with content 
	m_time = EbuCoretimeType_time_LocalPtr(); // Class


// no includefile for extension defined 
// file EbuCoretimeType_ExtMyPropInit.h

}

void EbuCoretimeType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretimeType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_timecode);
	// Dc1Factory::DeleteObject(m_normalPlayTime);
	// Dc1Factory::DeleteObject(m_editUnitNumber);
	// Dc1Factory::DeleteObject(m_time);
}

void EbuCoretimeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use timeTypePtr, since we
	// might need GetBase(), which isn't defined in ItimeType
	const Dc1Ptr< EbuCoretimeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
		// Dc1Factory::DeleteObject(m_timecode);
		this->Settimecode(Dc1Factory::CloneObject(tmp->Gettimecode()));
		// Dc1Factory::DeleteObject(m_normalPlayTime);
		this->SetnormalPlayTime(Dc1Factory::CloneObject(tmp->GetnormalPlayTime()));
		// Dc1Factory::DeleteObject(m_editUnitNumber);
		this->SeteditUnitNumber(Dc1Factory::CloneObject(tmp->GeteditUnitNumber()));
		// Dc1Factory::DeleteObject(m_time);
		this->Settime(Dc1Factory::CloneObject(tmp->Gettime()));
}

Dc1NodePtr EbuCoretimeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretimeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoretimeType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoretimeType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoretimeType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretimeType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoretimeType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoretimeType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoretimeType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretimeType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoretimeType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoretimeType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoretimeType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretimeType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoretimeType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoretimeType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoretimeType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretimeType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoretimeType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoretimeType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoretimeType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretimeType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
EbuCoretimecodePtr EbuCoretimeType::Gettimecode() const
{
		return m_timecode;
}

W3CtimePtr EbuCoretimeType::GetnormalPlayTime() const
{
		return m_normalPlayTime;
}

EbuCoreeditUnitNumberPtr EbuCoretimeType::GeteditUnitNumber() const
{
		return m_editUnitNumber;
}

EbuCoretimeType_time_LocalPtr EbuCoretimeType::Gettime() const
{
		return m_time;
}

// implementing setter for choice 
/* element */
void EbuCoretimeType::Settimecode(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::Settimecode().");
	}
	if (m_timecode != item)
	{
		// Dc1Factory::DeleteObject(m_timecode);
		m_timecode = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_timecode) m_timecode->SetParent(m_myself.getPointer());
		if (m_timecode && m_timecode->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timecodeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_timecode->UseTypeAttribute = true;
		}
		if(m_timecode != EbuCoretimecodePtr())
		{
			m_timecode->SetContentName(XMLString::transcode("timecode"));
		}
	// Dc1Factory::DeleteObject(m_normalPlayTime);
	m_normalPlayTime = W3CtimePtr();
	// Dc1Factory::DeleteObject(m_editUnitNumber);
	m_editUnitNumber = EbuCoreeditUnitNumberPtr();
	// Dc1Factory::DeleteObject(m_time);
	m_time = EbuCoretimeType_time_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCoretimeType::SetnormalPlayTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SetnormalPlayTime().");
	}
	if (m_normalPlayTime != item)
	{
		// Dc1Factory::DeleteObject(m_normalPlayTime);
		m_normalPlayTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_normalPlayTime) m_normalPlayTime->SetParent(m_myself.getPointer());
		if (m_normalPlayTime && m_normalPlayTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_normalPlayTime->UseTypeAttribute = true;
		}
		if(m_normalPlayTime != W3CtimePtr())
		{
			m_normalPlayTime->SetContentName(XMLString::transcode("normalPlayTime"));
		}
	// Dc1Factory::DeleteObject(m_timecode);
	m_timecode = EbuCoretimecodePtr();
	// Dc1Factory::DeleteObject(m_editUnitNumber);
	m_editUnitNumber = EbuCoreeditUnitNumberPtr();
	// Dc1Factory::DeleteObject(m_time);
	m_time = EbuCoretimeType_time_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCoretimeType::SeteditUnitNumber(const EbuCoreeditUnitNumberPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::SeteditUnitNumber().");
	}
	if (m_editUnitNumber != item)
	{
		// Dc1Factory::DeleteObject(m_editUnitNumber);
		m_editUnitNumber = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_editUnitNumber) m_editUnitNumber->SetParent(m_myself.getPointer());
		if (m_editUnitNumber && m_editUnitNumber->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:editUnitNumberType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_editUnitNumber->UseTypeAttribute = true;
		}
		if(m_editUnitNumber != EbuCoreeditUnitNumberPtr())
		{
			m_editUnitNumber->SetContentName(XMLString::transcode("editUnitNumber"));
		}
	// Dc1Factory::DeleteObject(m_timecode);
	m_timecode = EbuCoretimecodePtr();
	// Dc1Factory::DeleteObject(m_normalPlayTime);
	m_normalPlayTime = W3CtimePtr();
	// Dc1Factory::DeleteObject(m_time);
	m_time = EbuCoretimeType_time_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void EbuCoretimeType::Settime(const EbuCoretimeType_time_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimeType::Settime().");
	}
	if (m_time != item)
	{
		// Dc1Factory::DeleteObject(m_time);
		m_time = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_time) m_time->SetParent(m_myself.getPointer());
		if (m_time && m_time->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType_time_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_time->UseTypeAttribute = true;
		}
		if(m_time != EbuCoretimeType_time_LocalPtr())
		{
			m_time->SetContentName(XMLString::transcode("time"));
		}
	// Dc1Factory::DeleteObject(m_timecode);
	m_timecode = EbuCoretimecodePtr();
	// Dc1Factory::DeleteObject(m_normalPlayTime);
	m_normalPlayTime = W3CtimePtr();
	// Dc1Factory::DeleteObject(m_editUnitNumber);
	m_editUnitNumber = EbuCoreeditUnitNumberPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoretimeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("timecode")) == 0)
	{
		// timecode is simple element timecodeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Gettimecode()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timecodeType")))) != empty)
			{
				// Is type allowed
				EbuCoretimecodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Settimecode(p, client);
					if((p = Gettimecode()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("normalPlayTime")) == 0)
	{
		// normalPlayTime is simple element time
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetnormalPlayTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time")))) != empty)
			{
				// Is type allowed
				W3CtimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetnormalPlayTime(p, client);
					if((p = GetnormalPlayTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("editUnitNumber")) == 0)
	{
		// editUnitNumber is simple element editUnitNumberType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GeteditUnitNumber()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:editUnitNumberType")))) != empty)
			{
				// Is type allowed
				EbuCoreeditUnitNumberPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SeteditUnitNumber(p, client);
					if((p = GeteditUnitNumber()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("time")) == 0)
	{
		// time is simple element timeType_time_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Gettime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType_time_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoretimeType_time_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Settime(p, client);
					if((p = Gettime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for timeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "timeType");
	}
	return result;
}

XMLCh * EbuCoretimeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoretimeType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoretimeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("timeType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_timecode != EbuCoretimecodePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("timecode");
//		m_timecode->SetContentName(contentname);
		m_timecode->UseTypeAttribute = this->UseTypeAttribute;
		m_timecode->Serialize(doc, element);
	}
	if(m_normalPlayTime != W3CtimePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("normalPlayTime");
//		m_normalPlayTime->SetContentName(contentname);
		m_normalPlayTime->UseTypeAttribute = this->UseTypeAttribute;
		m_normalPlayTime->Serialize(doc, element);
	}
	// Class with content		
	
	if (m_editUnitNumber != EbuCoreeditUnitNumberPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_editUnitNumber->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("editUnitNumber"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_editUnitNumber->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_time != EbuCoretimeType_time_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_time->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("time"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_time->Serialize(doc, element, &elem);
	}

}

bool EbuCoretimeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("timecode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("timecode")) == 0))
		{
			EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settimecode(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("normalPlayTime"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("normalPlayTime")) == 0))
		{
			W3CtimePtr tmp = Createtime; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetnormalPlayTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("editUnitNumber"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("editUnitNumber")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateeditUnitNumberType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SeteditUnitNumber(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("time"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("time")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetimeType_time_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Settime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file EbuCoretimeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoretimeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("timecode")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimecodeType; // FTT, check this
	}
	this->Settimecode(child);
  }
  if (XMLString::compareString(elementname, X("normalPlayTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createtime; // FTT, check this
	}
	this->SetnormalPlayTime(child);
  }
  if (XMLString::compareString(elementname, X("editUnitNumber")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateeditUnitNumberType; // FTT, check this
	}
	this->SeteditUnitNumber(child);
  }
  if (XMLString::compareString(elementname, X("time")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimeType_time_LocalType; // FTT, check this
	}
	this->Settime(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoretimeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettimecode() != Dc1NodePtr())
		result.Insert(Gettimecode());
	if (GetnormalPlayTime() != Dc1NodePtr())
		result.Insert(GetnormalPlayTime());
	if (GeteditUnitNumber() != Dc1NodePtr())
		result.Insert(GeteditUnitNumber());
	if (Gettime() != Dc1NodePtr())
		result.Insert(Gettime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoretimeType_ExtMethodImpl.h


