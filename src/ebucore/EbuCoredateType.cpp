
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoredateType_ExtImplInclude.h


#include "EbuCoredateType_date_CollectionType.h"
#include "EbuCoredateType_created_LocalType.h"
#include "EbuCoredateType_issued_LocalType.h"
#include "EbuCoredateType_modified_LocalType.h"
#include "EbuCoredateType_digitised_LocalType.h"
#include "EbuCoredateType_released_LocalType.h"
#include "EbuCoredateType_copyrighted_LocalType.h"
#include "EbuCoredateType_alternative_CollectionType.h"
#include "EbuCoredateType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:date
#include "EbuCorealternativeDateType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:alternative

#include <assert.h>
IEbuCoredateType::IEbuCoredateType()
{

// no includefile for extension defined 
// file EbuCoredateType_ExtPropInit.h

}

IEbuCoredateType::~IEbuCoredateType()
{
// no includefile for extension defined 
// file EbuCoredateType_ExtPropCleanup.h

}

EbuCoredateType::EbuCoredateType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoredateType::~EbuCoredateType()
{
	Cleanup();
}

void EbuCoredateType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_precision = NULL; // String
	m_precision_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_date = EbuCoredateType_date_CollectionPtr(); // Collection
	m_created = EbuCoredateType_created_LocalPtr(); // Class
	m_created_Exist = false;
	m_issued = EbuCoredateType_issued_LocalPtr(); // Class
	m_issued_Exist = false;
	m_modified = EbuCoredateType_modified_LocalPtr(); // Class
	m_modified_Exist = false;
	m_digitised = EbuCoredateType_digitised_LocalPtr(); // Class
	m_digitised_Exist = false;
	m_released = EbuCoredateType_released_LocalPtr(); // Class
	m_released_Exist = false;
	m_copyrighted = EbuCoredateType_copyrighted_LocalPtr(); // Class
	m_copyrighted_Exist = false;
	m_alternative = EbuCoredateType_alternative_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoredateType_ExtMyPropInit.h

}

void EbuCoredateType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoredateType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_precision); // String
	// Dc1Factory::DeleteObject(m_date);
	// Dc1Factory::DeleteObject(m_created);
	// Dc1Factory::DeleteObject(m_issued);
	// Dc1Factory::DeleteObject(m_modified);
	// Dc1Factory::DeleteObject(m_digitised);
	// Dc1Factory::DeleteObject(m_released);
	// Dc1Factory::DeleteObject(m_copyrighted);
	// Dc1Factory::DeleteObject(m_alternative);
}

void EbuCoredateType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use dateTypePtr, since we
	// might need GetBase(), which isn't defined in IdateType
	const Dc1Ptr< EbuCoredateType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_precision); // String
	if (tmp->Existprecision())
	{
		this->Setprecision(XMLString::replicate(tmp->Getprecision()));
	}
	else
	{
		Invalidateprecision();
	}
	}
		// Dc1Factory::DeleteObject(m_date);
		this->Setdate(Dc1Factory::CloneObject(tmp->Getdate()));
	if (tmp->IsValidcreated())
	{
		// Dc1Factory::DeleteObject(m_created);
		this->Setcreated(Dc1Factory::CloneObject(tmp->Getcreated()));
	}
	else
	{
		Invalidatecreated();
	}
	if (tmp->IsValidissued())
	{
		// Dc1Factory::DeleteObject(m_issued);
		this->Setissued(Dc1Factory::CloneObject(tmp->Getissued()));
	}
	else
	{
		Invalidateissued();
	}
	if (tmp->IsValidmodified())
	{
		// Dc1Factory::DeleteObject(m_modified);
		this->Setmodified(Dc1Factory::CloneObject(tmp->Getmodified()));
	}
	else
	{
		Invalidatemodified();
	}
	if (tmp->IsValiddigitised())
	{
		// Dc1Factory::DeleteObject(m_digitised);
		this->Setdigitised(Dc1Factory::CloneObject(tmp->Getdigitised()));
	}
	else
	{
		Invalidatedigitised();
	}
	if (tmp->IsValidreleased())
	{
		// Dc1Factory::DeleteObject(m_released);
		this->Setreleased(Dc1Factory::CloneObject(tmp->Getreleased()));
	}
	else
	{
		Invalidatereleased();
	}
	if (tmp->IsValidcopyrighted())
	{
		// Dc1Factory::DeleteObject(m_copyrighted);
		this->Setcopyrighted(Dc1Factory::CloneObject(tmp->Getcopyrighted()));
	}
	else
	{
		Invalidatecopyrighted();
	}
		// Dc1Factory::DeleteObject(m_alternative);
		this->Setalternative(Dc1Factory::CloneObject(tmp->Getalternative()));
}

Dc1NodePtr EbuCoredateType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoredateType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoredateType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoredateType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoredateType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoredateType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoredateType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoredateType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoredateType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoredateType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoredateType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoredateType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoredateType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoredateType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoredateType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoredateType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoredateType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoredateType::Getprecision() const
{
	return m_precision;
}

bool EbuCoredateType::Existprecision() const
{
	return m_precision_Exist;
}
void EbuCoredateType::Setprecision(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setprecision().");
	}
	m_precision = item;
	m_precision_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidateprecision()
{
	m_precision_Exist = false;
}
EbuCoredateType_date_CollectionPtr EbuCoredateType::Getdate() const
{
		return m_date;
}

EbuCoredateType_created_LocalPtr EbuCoredateType::Getcreated() const
{
		return m_created;
}

// Element is optional
bool EbuCoredateType::IsValidcreated() const
{
	return m_created_Exist;
}

EbuCoredateType_issued_LocalPtr EbuCoredateType::Getissued() const
{
		return m_issued;
}

// Element is optional
bool EbuCoredateType::IsValidissued() const
{
	return m_issued_Exist;
}

EbuCoredateType_modified_LocalPtr EbuCoredateType::Getmodified() const
{
		return m_modified;
}

// Element is optional
bool EbuCoredateType::IsValidmodified() const
{
	return m_modified_Exist;
}

EbuCoredateType_digitised_LocalPtr EbuCoredateType::Getdigitised() const
{
		return m_digitised;
}

// Element is optional
bool EbuCoredateType::IsValiddigitised() const
{
	return m_digitised_Exist;
}

EbuCoredateType_released_LocalPtr EbuCoredateType::Getreleased() const
{
		return m_released;
}

// Element is optional
bool EbuCoredateType::IsValidreleased() const
{
	return m_released_Exist;
}

EbuCoredateType_copyrighted_LocalPtr EbuCoredateType::Getcopyrighted() const
{
		return m_copyrighted;
}

// Element is optional
bool EbuCoredateType::IsValidcopyrighted() const
{
	return m_copyrighted_Exist;
}

EbuCoredateType_alternative_CollectionPtr EbuCoredateType::Getalternative() const
{
		return m_alternative;
}

void EbuCoredateType::Setdate(const EbuCoredateType_date_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setdate().");
	}
	if (m_date != item)
	{
		// Dc1Factory::DeleteObject(m_date);
		m_date = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_date) m_date->SetParent(m_myself.getPointer());
		if(m_date != EbuCoredateType_date_CollectionPtr())
		{
			m_date->SetContentName(XMLString::transcode("date"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Setcreated(const EbuCoredateType_created_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setcreated().");
	}
	if (m_created != item || m_created_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_created);
		m_created = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_created) m_created->SetParent(m_myself.getPointer());
		if (m_created && m_created->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_created_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_created->UseTypeAttribute = true;
		}
		m_created_Exist = true;
		if(m_created != EbuCoredateType_created_LocalPtr())
		{
			m_created->SetContentName(XMLString::transcode("created"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidatecreated()
{
	m_created_Exist = false;
}
void EbuCoredateType::Setissued(const EbuCoredateType_issued_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setissued().");
	}
	if (m_issued != item || m_issued_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_issued);
		m_issued = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_issued) m_issued->SetParent(m_myself.getPointer());
		if (m_issued && m_issued->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_issued_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_issued->UseTypeAttribute = true;
		}
		m_issued_Exist = true;
		if(m_issued != EbuCoredateType_issued_LocalPtr())
		{
			m_issued->SetContentName(XMLString::transcode("issued"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidateissued()
{
	m_issued_Exist = false;
}
void EbuCoredateType::Setmodified(const EbuCoredateType_modified_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setmodified().");
	}
	if (m_modified != item || m_modified_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_modified);
		m_modified = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_modified) m_modified->SetParent(m_myself.getPointer());
		if (m_modified && m_modified->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_modified_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_modified->UseTypeAttribute = true;
		}
		m_modified_Exist = true;
		if(m_modified != EbuCoredateType_modified_LocalPtr())
		{
			m_modified->SetContentName(XMLString::transcode("modified"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidatemodified()
{
	m_modified_Exist = false;
}
void EbuCoredateType::Setdigitised(const EbuCoredateType_digitised_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setdigitised().");
	}
	if (m_digitised != item || m_digitised_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_digitised);
		m_digitised = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_digitised) m_digitised->SetParent(m_myself.getPointer());
		if (m_digitised && m_digitised->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_digitised_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_digitised->UseTypeAttribute = true;
		}
		m_digitised_Exist = true;
		if(m_digitised != EbuCoredateType_digitised_LocalPtr())
		{
			m_digitised->SetContentName(XMLString::transcode("digitised"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidatedigitised()
{
	m_digitised_Exist = false;
}
void EbuCoredateType::Setreleased(const EbuCoredateType_released_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setreleased().");
	}
	if (m_released != item || m_released_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_released);
		m_released = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_released) m_released->SetParent(m_myself.getPointer());
		if (m_released && m_released->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_released_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_released->UseTypeAttribute = true;
		}
		m_released_Exist = true;
		if(m_released != EbuCoredateType_released_LocalPtr())
		{
			m_released->SetContentName(XMLString::transcode("released"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidatereleased()
{
	m_released_Exist = false;
}
void EbuCoredateType::Setcopyrighted(const EbuCoredateType_copyrighted_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setcopyrighted().");
	}
	if (m_copyrighted != item || m_copyrighted_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_copyrighted);
		m_copyrighted = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_copyrighted) m_copyrighted->SetParent(m_myself.getPointer());
		if (m_copyrighted && m_copyrighted->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_copyrighted_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_copyrighted->UseTypeAttribute = true;
		}
		m_copyrighted_Exist = true;
		if(m_copyrighted != EbuCoredateType_copyrighted_LocalPtr())
		{
			m_copyrighted->SetContentName(XMLString::transcode("copyrighted"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredateType::Invalidatecopyrighted()
{
	m_copyrighted_Exist = false;
}
void EbuCoredateType::Setalternative(const EbuCoredateType_alternative_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredateType::Setalternative().");
	}
	if (m_alternative != item)
	{
		// Dc1Factory::DeleteObject(m_alternative);
		m_alternative = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_alternative) m_alternative->SetParent(m_myself.getPointer());
		if(m_alternative != EbuCoredateType_alternative_CollectionPtr())
		{
			m_alternative->SetContentName(XMLString::transcode("alternative"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoredateType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  6, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("precision")) == 0)
	{
		// precision is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("date")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:date is item of type elementType
		// in element collection dateType_date_CollectionType
		
		context->Found = true;
		EbuCoredateType_date_CollectionPtr coll = Getdate();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedateType_date_CollectionType; // FTT, check this
				Setdate(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("created")) == 0)
	{
		// created is simple element dateType_created_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcreated()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_created_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_created_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcreated(p, client);
					if((p = Getcreated()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("issued")) == 0)
	{
		// issued is simple element dateType_issued_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getissued()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_issued_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_issued_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setissued(p, client);
					if((p = Getissued()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("modified")) == 0)
	{
		// modified is simple element dateType_modified_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getmodified()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_modified_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_modified_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setmodified(p, client);
					if((p = Getmodified()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("digitised")) == 0)
	{
		// digitised is simple element dateType_digitised_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getdigitised()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_digitised_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_digitised_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setdigitised(p, client);
					if((p = Getdigitised()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("released")) == 0)
	{
		// released is simple element dateType_released_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getreleased()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_released_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_released_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setreleased(p, client);
					if((p = Getreleased()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("copyrighted")) == 0)
	{
		// copyrighted is simple element dateType_copyrighted_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcopyrighted()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_copyrighted_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoredateType_copyrighted_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcopyrighted(p, client);
					if((p = Getcopyrighted()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("alternative")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:alternative is item of type alternativeDateType
		// in element collection dateType_alternative_CollectionType
		
		context->Found = true;
		EbuCoredateType_alternative_CollectionPtr coll = Getalternative();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedateType_alternative_CollectionType; // FTT, check this
				Setalternative(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeDateType")))) != empty)
			{
				// Is type allowed
				EbuCorealternativeDatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for dateType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "dateType");
	}
	return result;
}

XMLCh * EbuCoredateType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoredateType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoredateType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("dateType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_precision_Exist)
	{
	// String
	if(m_precision != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("precision"), m_precision);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_date != EbuCoredateType_date_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_date->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_created != EbuCoredateType_created_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_created->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("created"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_created->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_issued != EbuCoredateType_issued_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_issued->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("issued"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_issued->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_modified != EbuCoredateType_modified_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_modified->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("modified"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_modified->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_digitised != EbuCoredateType_digitised_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_digitised->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("digitised"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_digitised->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_released != EbuCoredateType_released_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_released->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("released"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_released->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_copyrighted != EbuCoredateType_copyrighted_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_copyrighted->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("copyrighted"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_copyrighted->Serialize(doc, element, &elem);
	}
	if (m_alternative != EbuCoredateType_alternative_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_alternative->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoredateType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("precision")))
	{
		// Deserialize string type
		this->Setprecision(Dc1Convert::TextToString(parent->getAttribute(X("precision"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("date"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("date")) == 0))
		{
			// Deserialize factory type
			EbuCoredateType_date_CollectionPtr tmp = CreatedateType_date_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdate(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("created"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("created")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_created_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcreated(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("issued"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("issued")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_issued_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setissued(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("modified"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("modified")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_modified_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setmodified(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("digitised"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("digitised")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_digitised_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setdigitised(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("released"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("released")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_released_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setreleased(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("copyrighted"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("copyrighted")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatedateType_copyrighted_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcopyrighted(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("alternative"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("alternative")) == 0))
		{
			// Deserialize factory type
			EbuCoredateType_alternative_CollectionPtr tmp = CreatedateType_alternative_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setalternative(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoredateType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoredateType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("date")) == 0))
  {
	EbuCoredateType_date_CollectionPtr tmp = CreatedateType_date_CollectionType; // FTT, check this
	this->Setdate(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("created")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_created_LocalType; // FTT, check this
	}
	this->Setcreated(child);
  }
  if (XMLString::compareString(elementname, X("issued")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_issued_LocalType; // FTT, check this
	}
	this->Setissued(child);
  }
  if (XMLString::compareString(elementname, X("modified")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_modified_LocalType; // FTT, check this
	}
	this->Setmodified(child);
  }
  if (XMLString::compareString(elementname, X("digitised")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_digitised_LocalType; // FTT, check this
	}
	this->Setdigitised(child);
  }
  if (XMLString::compareString(elementname, X("released")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_released_LocalType; // FTT, check this
	}
	this->Setreleased(child);
  }
  if (XMLString::compareString(elementname, X("copyrighted")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateType_copyrighted_LocalType; // FTT, check this
	}
	this->Setcopyrighted(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("alternative")) == 0))
  {
	EbuCoredateType_alternative_CollectionPtr tmp = CreatedateType_alternative_CollectionType; // FTT, check this
	this->Setalternative(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoredateType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getdate() != Dc1NodePtr())
		result.Insert(Getdate());
	if (Getcreated() != Dc1NodePtr())
		result.Insert(Getcreated());
	if (Getissued() != Dc1NodePtr())
		result.Insert(Getissued());
	if (Getmodified() != Dc1NodePtr())
		result.Insert(Getmodified());
	if (Getdigitised() != Dc1NodePtr())
		result.Insert(Getdigitised());
	if (Getreleased() != Dc1NodePtr())
		result.Insert(Getreleased());
	if (Getcopyrighted() != Dc1NodePtr())
		result.Insert(Getcopyrighted());
	if (Getalternative() != Dc1NodePtr())
		result.Insert(Getalternative());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoredateType_ExtMethodImpl.h


