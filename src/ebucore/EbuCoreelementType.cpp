
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreelementType_ExtImplInclude.h


#include "Dc1elementType.h"
#include "EbuCoreelementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreelementType::IEbuCoreelementType()
{

// no includefile for extension defined 
// file EbuCoreelementType_ExtPropInit.h

}

IEbuCoreelementType::~IEbuCoreelementType()
{
// no includefile for extension defined 
// file EbuCoreelementType_ExtPropCleanup.h

}

EbuCoreelementType::EbuCoreelementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreelementType::~EbuCoreelementType()
{
	Cleanup();
}

void EbuCoreelementType::Init()
{
	// Init base
	m_Base = CreateelementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_charset = NULL; // String
	m_charset_Exist = false;
	m_encoding = NULL; // String
	m_encoding_Exist = false;
	m_transcription = NULL; // String
	m_transcription_Exist = false;



// no includefile for extension defined 
// file EbuCoreelementType_ExtMyPropInit.h

}

void EbuCoreelementType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreelementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_charset); // String
	XMLString::release(&m_encoding); // String
	XMLString::release(&m_transcription); // String
}

void EbuCoreelementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use elementTypePtr, since we
	// might need GetBase(), which isn't defined in IelementType
	const Dc1Ptr< EbuCoreelementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Dc1elementPtr(Dc1Factory::CloneObject((dynamic_cast< const EbuCoreelementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_charset); // String
	if (tmp->Existcharset())
	{
		this->Setcharset(XMLString::replicate(tmp->Getcharset()));
	}
	else
	{
		Invalidatecharset();
	}
	}
	{
	XMLString::release(&m_encoding); // String
	if (tmp->Existencoding())
	{
		this->Setencoding(XMLString::replicate(tmp->Getencoding()));
	}
	else
	{
		Invalidateencoding();
	}
	}
	{
	XMLString::release(&m_transcription); // String
	if (tmp->Existtranscription())
	{
		this->Settranscription(XMLString::replicate(tmp->Gettranscription()));
	}
	else
	{
		Invalidatetranscription();
	}
	}
}

Dc1NodePtr EbuCoreelementType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr EbuCoreelementType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Dc1elementType > EbuCoreelementType::GetBase() const
{
	return m_Base;
}

void EbuCoreelementType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreelementType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCoreelementType::GetContent() const
{
	return GetBase()->GetContent();
}
XMLCh * EbuCoreelementType::Getcharset() const
{
	return m_charset;
}

bool EbuCoreelementType::Existcharset() const
{
	return m_charset_Exist;
}
void EbuCoreelementType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreelementType::Setcharset().");
	}
	m_charset = item;
	m_charset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreelementType::Invalidatecharset()
{
	m_charset_Exist = false;
}
XMLCh * EbuCoreelementType::Getencoding() const
{
	return m_encoding;
}

bool EbuCoreelementType::Existencoding() const
{
	return m_encoding_Exist;
}
void EbuCoreelementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreelementType::Setencoding().");
	}
	m_encoding = item;
	m_encoding_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreelementType::Invalidateencoding()
{
	m_encoding_Exist = false;
}
XMLCh * EbuCoreelementType::Gettranscription() const
{
	return m_transcription;
}

bool EbuCoreelementType::Existtranscription() const
{
	return m_transcription_Exist;
}
void EbuCoreelementType::Settranscription(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreelementType::Settranscription().");
	}
	m_transcription = item;
	m_transcription_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreelementType::Invalidatetranscription()
{
	m_transcription_Exist = false;
}
XMLCh * EbuCoreelementType::Getlang() const
{
	return GetBase()->Getlang();
}

bool EbuCoreelementType::Existlang() const
{
	return GetBase()->Existlang();
}
void EbuCoreelementType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreelementType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreelementType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}

Dc1NodeEnum EbuCoreelementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("charset")) == 0)
	{
		// charset is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("encoding")) == 0)
	{
		// encoding is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("transcription")) == 0)
	{
		// transcription is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for elementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "elementType");
	}
	return result;
}

XMLCh * EbuCoreelementType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreelementType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCoreelementType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* EbuCoreelementType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void EbuCoreelementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file EbuCoreelementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("elementType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_charset_Exist)
	{
	// String
	if(m_charset != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("charset"), m_charset);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_encoding_Exist)
	{
	// String
	if(m_encoding != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("encoding"), m_encoding);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_transcription_Exist)
	{
	// String
	if(m_transcription != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("transcription"), m_transcription);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCoreelementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("charset")))
	{
		// Deserialize string type
		this->Setcharset(Dc1Convert::TextToString(parent->getAttribute(X("charset"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("encoding")))
	{
		// Deserialize string type
		this->Setencoding(Dc1Convert::TextToString(parent->getAttribute(X("encoding"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("transcription")))
	{
		// Deserialize string type
		this->Settranscription(Dc1Convert::TextToString(parent->getAttribute(X("transcription"))));
		* current = parent;
	}

	// Extensionbase is Dc1elementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreelementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreelementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreelementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreelementType_ExtMethodImpl.h


