
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtImplInclude.h


#include "EbuCoredataFormatType_captioningFormat_CollectionType.h"
#include "EbuCoredataFormatType_subtitlingFormat_CollectionType.h"
#include "EbuCoredataFormatType_ancillaryDataFormat_CollectionType.h"
#include "EbuCorecodecType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCoredataFormatType_comment_CollectionType.h"
#include "EbuCoredataFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorecaptioningFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:captioningFormat
#include "EbuCoresubtitlingFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormat
#include "EbuCoreancillaryDataFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormat
#include "EbuCorecomment_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:comment

#include <assert.h>
IEbuCoredataFormatType::IEbuCoredataFormatType()
{

// no includefile for extension defined 
// file EbuCoredataFormatType_ExtPropInit.h

}

IEbuCoredataFormatType::~IEbuCoredataFormatType()
{
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtPropCleanup.h

}

EbuCoredataFormatType::EbuCoredataFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoredataFormatType::~EbuCoredataFormatType()
{
	Cleanup();
}

void EbuCoredataFormatType::Init()
{

	// Init attributes
	m_dataFormatId = NULL; // String
	m_dataFormatId_Exist = false;
	m_dataFormatVersionId = NULL; // String
	m_dataFormatVersionId_Exist = false;
	m_dataFormatName = NULL; // String
	m_dataFormatName_Exist = false;
	m_dataFormatDefinition = NULL; // String
	m_dataFormatDefinition_Exist = false;
	m_dataTrackId = NULL; // String
	m_dataTrackId_Exist = false;
	m_dataTrackName = NULL; // String
	m_dataTrackName_Exist = false;
	m_dataTrackLanguage = NULL; // String
	m_dataTrackLanguage_Exist = false;
	m_dataPresenceFlag = false; // Value
	m_dataPresenceFlag_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_captioningFormat = EbuCoredataFormatType_captioningFormat_CollectionPtr(); // Collection
	m_subtitlingFormat = EbuCoredataFormatType_subtitlingFormat_CollectionPtr(); // Collection
	m_ancillaryDataFormat = EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr(); // Collection
	m_codec = EbuCorecodecPtr(); // Class
	m_codec_Exist = false;
	m_technicalAttributes = EbuCoretechnicalAttributesPtr(); // Class
	m_comment = EbuCoredataFormatType_comment_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMyPropInit.h

}

void EbuCoredataFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_dataFormatId); // String
	XMLString::release(&m_dataFormatVersionId); // String
	XMLString::release(&m_dataFormatName); // String
	XMLString::release(&m_dataFormatDefinition); // String
	XMLString::release(&m_dataTrackId); // String
	XMLString::release(&m_dataTrackName); // String
	XMLString::release(&m_dataTrackLanguage); // String
	// Dc1Factory::DeleteObject(m_captioningFormat);
	// Dc1Factory::DeleteObject(m_subtitlingFormat);
	// Dc1Factory::DeleteObject(m_ancillaryDataFormat);
	// Dc1Factory::DeleteObject(m_codec);
	// Dc1Factory::DeleteObject(m_technicalAttributes);
	// Dc1Factory::DeleteObject(m_comment);
}

void EbuCoredataFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use dataFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IdataFormatType
	const Dc1Ptr< EbuCoredataFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_dataFormatId); // String
	if (tmp->ExistdataFormatId())
	{
		this->SetdataFormatId(XMLString::replicate(tmp->GetdataFormatId()));
	}
	else
	{
		InvalidatedataFormatId();
	}
	}
	{
	XMLString::release(&m_dataFormatVersionId); // String
	if (tmp->ExistdataFormatVersionId())
	{
		this->SetdataFormatVersionId(XMLString::replicate(tmp->GetdataFormatVersionId()));
	}
	else
	{
		InvalidatedataFormatVersionId();
	}
	}
	{
	XMLString::release(&m_dataFormatName); // String
	if (tmp->ExistdataFormatName())
	{
		this->SetdataFormatName(XMLString::replicate(tmp->GetdataFormatName()));
	}
	else
	{
		InvalidatedataFormatName();
	}
	}
	{
	XMLString::release(&m_dataFormatDefinition); // String
	if (tmp->ExistdataFormatDefinition())
	{
		this->SetdataFormatDefinition(XMLString::replicate(tmp->GetdataFormatDefinition()));
	}
	else
	{
		InvalidatedataFormatDefinition();
	}
	}
	{
	XMLString::release(&m_dataTrackId); // String
	if (tmp->ExistdataTrackId())
	{
		this->SetdataTrackId(XMLString::replicate(tmp->GetdataTrackId()));
	}
	else
	{
		InvalidatedataTrackId();
	}
	}
	{
	XMLString::release(&m_dataTrackName); // String
	if (tmp->ExistdataTrackName())
	{
		this->SetdataTrackName(XMLString::replicate(tmp->GetdataTrackName()));
	}
	else
	{
		InvalidatedataTrackName();
	}
	}
	{
	XMLString::release(&m_dataTrackLanguage); // String
	if (tmp->ExistdataTrackLanguage())
	{
		this->SetdataTrackLanguage(XMLString::replicate(tmp->GetdataTrackLanguage()));
	}
	else
	{
		InvalidatedataTrackLanguage();
	}
	}
	{
	if (tmp->ExistdataPresenceFlag())
	{
		this->SetdataPresenceFlag(tmp->GetdataPresenceFlag());
	}
	else
	{
		InvalidatedataPresenceFlag();
	}
	}
		// Dc1Factory::DeleteObject(m_captioningFormat);
		this->SetcaptioningFormat(Dc1Factory::CloneObject(tmp->GetcaptioningFormat()));
		// Dc1Factory::DeleteObject(m_subtitlingFormat);
		this->SetsubtitlingFormat(Dc1Factory::CloneObject(tmp->GetsubtitlingFormat()));
		// Dc1Factory::DeleteObject(m_ancillaryDataFormat);
		this->SetancillaryDataFormat(Dc1Factory::CloneObject(tmp->GetancillaryDataFormat()));
	if (tmp->IsValidcodec())
	{
		// Dc1Factory::DeleteObject(m_codec);
		this->Setcodec(Dc1Factory::CloneObject(tmp->Getcodec()));
	}
	else
	{
		Invalidatecodec();
	}
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		this->SettechnicalAttributes(Dc1Factory::CloneObject(tmp->GettechnicalAttributes()));
		// Dc1Factory::DeleteObject(m_comment);
		this->Setcomment(Dc1Factory::CloneObject(tmp->Getcomment()));
}

Dc1NodePtr EbuCoredataFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoredataFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoredataFormatType::GetdataFormatId() const
{
	return m_dataFormatId;
}

bool EbuCoredataFormatType::ExistdataFormatId() const
{
	return m_dataFormatId_Exist;
}
void EbuCoredataFormatType::SetdataFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataFormatId().");
	}
	m_dataFormatId = item;
	m_dataFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataFormatId()
{
	m_dataFormatId_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataFormatVersionId() const
{
	return m_dataFormatVersionId;
}

bool EbuCoredataFormatType::ExistdataFormatVersionId() const
{
	return m_dataFormatVersionId_Exist;
}
void EbuCoredataFormatType::SetdataFormatVersionId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataFormatVersionId().");
	}
	m_dataFormatVersionId = item;
	m_dataFormatVersionId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataFormatVersionId()
{
	m_dataFormatVersionId_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataFormatName() const
{
	return m_dataFormatName;
}

bool EbuCoredataFormatType::ExistdataFormatName() const
{
	return m_dataFormatName_Exist;
}
void EbuCoredataFormatType::SetdataFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataFormatName().");
	}
	m_dataFormatName = item;
	m_dataFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataFormatName()
{
	m_dataFormatName_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataFormatDefinition() const
{
	return m_dataFormatDefinition;
}

bool EbuCoredataFormatType::ExistdataFormatDefinition() const
{
	return m_dataFormatDefinition_Exist;
}
void EbuCoredataFormatType::SetdataFormatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataFormatDefinition().");
	}
	m_dataFormatDefinition = item;
	m_dataFormatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataFormatDefinition()
{
	m_dataFormatDefinition_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataTrackId() const
{
	return m_dataTrackId;
}

bool EbuCoredataFormatType::ExistdataTrackId() const
{
	return m_dataTrackId_Exist;
}
void EbuCoredataFormatType::SetdataTrackId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataTrackId().");
	}
	m_dataTrackId = item;
	m_dataTrackId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataTrackId()
{
	m_dataTrackId_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataTrackName() const
{
	return m_dataTrackName;
}

bool EbuCoredataFormatType::ExistdataTrackName() const
{
	return m_dataTrackName_Exist;
}
void EbuCoredataFormatType::SetdataTrackName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataTrackName().");
	}
	m_dataTrackName = item;
	m_dataTrackName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataTrackName()
{
	m_dataTrackName_Exist = false;
}
XMLCh * EbuCoredataFormatType::GetdataTrackLanguage() const
{
	return m_dataTrackLanguage;
}

bool EbuCoredataFormatType::ExistdataTrackLanguage() const
{
	return m_dataTrackLanguage_Exist;
}
void EbuCoredataFormatType::SetdataTrackLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataTrackLanguage().");
	}
	m_dataTrackLanguage = item;
	m_dataTrackLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataTrackLanguage()
{
	m_dataTrackLanguage_Exist = false;
}
bool EbuCoredataFormatType::GetdataPresenceFlag() const
{
	return m_dataPresenceFlag;
}

bool EbuCoredataFormatType::ExistdataPresenceFlag() const
{
	return m_dataPresenceFlag_Exist;
}
void EbuCoredataFormatType::SetdataPresenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetdataPresenceFlag().");
	}
	m_dataPresenceFlag = item;
	m_dataPresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::InvalidatedataPresenceFlag()
{
	m_dataPresenceFlag_Exist = false;
}
EbuCoredataFormatType_captioningFormat_CollectionPtr EbuCoredataFormatType::GetcaptioningFormat() const
{
		return m_captioningFormat;
}

EbuCoredataFormatType_subtitlingFormat_CollectionPtr EbuCoredataFormatType::GetsubtitlingFormat() const
{
		return m_subtitlingFormat;
}

EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr EbuCoredataFormatType::GetancillaryDataFormat() const
{
		return m_ancillaryDataFormat;
}

EbuCorecodecPtr EbuCoredataFormatType::Getcodec() const
{
		return m_codec;
}

// Element is optional
bool EbuCoredataFormatType::IsValidcodec() const
{
	return m_codec_Exist;
}

EbuCoretechnicalAttributesPtr EbuCoredataFormatType::GettechnicalAttributes() const
{
		return m_technicalAttributes;
}

EbuCoredataFormatType_comment_CollectionPtr EbuCoredataFormatType::Getcomment() const
{
		return m_comment;
}

void EbuCoredataFormatType::SetcaptioningFormat(const EbuCoredataFormatType_captioningFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetcaptioningFormat().");
	}
	if (m_captioningFormat != item)
	{
		// Dc1Factory::DeleteObject(m_captioningFormat);
		m_captioningFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_captioningFormat) m_captioningFormat->SetParent(m_myself.getPointer());
		if(m_captioningFormat != EbuCoredataFormatType_captioningFormat_CollectionPtr())
		{
			m_captioningFormat->SetContentName(XMLString::transcode("captioningFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::SetsubtitlingFormat(const EbuCoredataFormatType_subtitlingFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetsubtitlingFormat().");
	}
	if (m_subtitlingFormat != item)
	{
		// Dc1Factory::DeleteObject(m_subtitlingFormat);
		m_subtitlingFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_subtitlingFormat) m_subtitlingFormat->SetParent(m_myself.getPointer());
		if(m_subtitlingFormat != EbuCoredataFormatType_subtitlingFormat_CollectionPtr())
		{
			m_subtitlingFormat->SetContentName(XMLString::transcode("subtitlingFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::SetancillaryDataFormat(const EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SetancillaryDataFormat().");
	}
	if (m_ancillaryDataFormat != item)
	{
		// Dc1Factory::DeleteObject(m_ancillaryDataFormat);
		m_ancillaryDataFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ancillaryDataFormat) m_ancillaryDataFormat->SetParent(m_myself.getPointer());
		if(m_ancillaryDataFormat != EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr())
		{
			m_ancillaryDataFormat->SetContentName(XMLString::transcode("ancillaryDataFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::Setcodec().");
	}
	if (m_codec != item || m_codec_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_codec);
		m_codec = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_codec) m_codec->SetParent(m_myself.getPointer());
		if (m_codec && m_codec->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_codec->UseTypeAttribute = true;
		}
		m_codec_Exist = true;
		if(m_codec != EbuCorecodecPtr())
		{
			m_codec->SetContentName(XMLString::transcode("codec"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::Invalidatecodec()
{
	m_codec_Exist = false;
}
void EbuCoredataFormatType::SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::SettechnicalAttributes().");
	}
	if (m_technicalAttributes != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributes);
		m_technicalAttributes = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributes) m_technicalAttributes->SetParent(m_myself.getPointer());
		if (m_technicalAttributes && m_technicalAttributes->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_technicalAttributes->UseTypeAttribute = true;
		}
		if(m_technicalAttributes != EbuCoretechnicalAttributesPtr())
		{
			m_technicalAttributes->SetContentName(XMLString::transcode("technicalAttributes"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredataFormatType::Setcomment(const EbuCoredataFormatType_comment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredataFormatType::Setcomment().");
	}
	if (m_comment != item)
	{
		// Dc1Factory::DeleteObject(m_comment);
		m_comment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_comment) m_comment->SetParent(m_myself.getPointer());
		if(m_comment != EbuCoredataFormatType_comment_CollectionPtr())
		{
			m_comment->SetContentName(XMLString::transcode("comment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoredataFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  8, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataFormatId")) == 0)
	{
		// dataFormatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataFormatVersionId")) == 0)
	{
		// dataFormatVersionId is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataFormatName")) == 0)
	{
		// dataFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataFormatDefinition")) == 0)
	{
		// dataFormatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataTrackId")) == 0)
	{
		// dataTrackId is simple attribute NMTOKEN
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataTrackName")) == 0)
	{
		// dataTrackName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataTrackLanguage")) == 0)
	{
		// dataTrackLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataPresenceFlag")) == 0)
	{
		// dataPresenceFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("captioningFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:captioningFormat is item of type captioningFormatType
		// in element collection dataFormatType_captioningFormat_CollectionType
		
		context->Found = true;
		EbuCoredataFormatType_captioningFormat_CollectionPtr coll = GetcaptioningFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedataFormatType_captioningFormat_CollectionType; // FTT, check this
				SetcaptioningFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:captioningFormatType")))) != empty)
			{
				// Is type allowed
				EbuCorecaptioningFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("subtitlingFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormat is item of type subtitlingFormatType
		// in element collection dataFormatType_subtitlingFormat_CollectionType
		
		context->Found = true;
		EbuCoredataFormatType_subtitlingFormat_CollectionPtr coll = GetsubtitlingFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedataFormatType_subtitlingFormat_CollectionType; // FTT, check this
				SetsubtitlingFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoresubtitlingFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ancillaryDataFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormat is item of type ancillaryDataFormatType
		// in element collection dataFormatType_ancillaryDataFormat_CollectionType
		
		context->Found = true;
		EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr coll = GetancillaryDataFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedataFormatType_ancillaryDataFormat_CollectionType; // FTT, check this
				SetancillaryDataFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreancillaryDataFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("codec")) == 0)
	{
		// codec is simple element codecType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcodec()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType")))) != empty)
			{
				// Is type allowed
				EbuCorecodecPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcodec(p, client);
					if((p = Getcodec()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributes")) == 0)
	{
		// technicalAttributes is simple element technicalAttributes
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettechnicalAttributes()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettechnicalAttributes(p, client);
					if((p = GettechnicalAttributes()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("comment")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:comment is item of type comment_LocalType
		// in element collection dataFormatType_comment_CollectionType
		
		context->Found = true;
		EbuCoredataFormatType_comment_CollectionPtr coll = Getcomment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedataFormatType_comment_CollectionType; // FTT, check this
				Setcomment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecomment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for dataFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "dataFormatType");
	}
	return result;
}

XMLCh * EbuCoredataFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoredataFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoredataFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("dataFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataFormatId_Exist)
	{
	// String
	if(m_dataFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataFormatId"), m_dataFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataFormatVersionId_Exist)
	{
	// String
	if(m_dataFormatVersionId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataFormatVersionId"), m_dataFormatVersionId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataFormatName_Exist)
	{
	// String
	if(m_dataFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataFormatName"), m_dataFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataFormatDefinition_Exist)
	{
	// String
	if(m_dataFormatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataFormatDefinition"), m_dataFormatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataTrackId_Exist)
	{
	// String
	if(m_dataTrackId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataTrackId"), m_dataTrackId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataTrackName_Exist)
	{
	// String
	if(m_dataTrackName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataTrackName"), m_dataTrackName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataTrackLanguage_Exist)
	{
	// String
	if(m_dataTrackLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataTrackLanguage"), m_dataTrackLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataPresenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_dataPresenceFlag);
		element->setAttributeNS(X(""), X("dataPresenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_captioningFormat != EbuCoredataFormatType_captioningFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_captioningFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_subtitlingFormat != EbuCoredataFormatType_subtitlingFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_subtitlingFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ancillaryDataFormat != EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ancillaryDataFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_codec != EbuCorecodecPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_codec->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("codec"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_codec->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_technicalAttributes != EbuCoretechnicalAttributesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_technicalAttributes->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_technicalAttributes->Serialize(doc, element, &elem);
	}
	if (m_comment != EbuCoredataFormatType_comment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_comment->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoredataFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataFormatId")))
	{
		// Deserialize string type
		this->SetdataFormatId(Dc1Convert::TextToString(parent->getAttribute(X("dataFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataFormatVersionId")))
	{
		// Deserialize string type
		this->SetdataFormatVersionId(Dc1Convert::TextToString(parent->getAttribute(X("dataFormatVersionId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataFormatName")))
	{
		// Deserialize string type
		this->SetdataFormatName(Dc1Convert::TextToString(parent->getAttribute(X("dataFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataFormatDefinition")))
	{
		// Deserialize string type
		this->SetdataFormatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("dataFormatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataTrackId")))
	{
		// Deserialize string type
		this->SetdataTrackId(Dc1Convert::TextToString(parent->getAttribute(X("dataTrackId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataTrackName")))
	{
		// Deserialize string type
		this->SetdataTrackName(Dc1Convert::TextToString(parent->getAttribute(X("dataTrackName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataTrackLanguage")))
	{
		// Deserialize string type
		this->SetdataTrackLanguage(Dc1Convert::TextToString(parent->getAttribute(X("dataTrackLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataPresenceFlag")))
	{
		// deserialize value type
		this->SetdataPresenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("dataPresenceFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("captioningFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("captioningFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoredataFormatType_captioningFormat_CollectionPtr tmp = CreatedataFormatType_captioningFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetcaptioningFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("subtitlingFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("subtitlingFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoredataFormatType_subtitlingFormat_CollectionPtr tmp = CreatedataFormatType_subtitlingFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetsubtitlingFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ancillaryDataFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ancillaryDataFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr tmp = CreatedataFormatType_ancillaryDataFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetancillaryDataFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("codec"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("codec")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecodecType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcodec(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("technicalAttributes"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("technicalAttributes")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetechnicalAttributes; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettechnicalAttributes(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("comment"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("comment")) == 0))
		{
			// Deserialize factory type
			EbuCoredataFormatType_comment_CollectionPtr tmp = CreatedataFormatType_comment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcomment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoredataFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("captioningFormat")) == 0))
  {
	EbuCoredataFormatType_captioningFormat_CollectionPtr tmp = CreatedataFormatType_captioningFormat_CollectionType; // FTT, check this
	this->SetcaptioningFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("subtitlingFormat")) == 0))
  {
	EbuCoredataFormatType_subtitlingFormat_CollectionPtr tmp = CreatedataFormatType_subtitlingFormat_CollectionType; // FTT, check this
	this->SetsubtitlingFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ancillaryDataFormat")) == 0))
  {
	EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr tmp = CreatedataFormatType_ancillaryDataFormat_CollectionType; // FTT, check this
	this->SetancillaryDataFormat(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("codec")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecodecType; // FTT, check this
	}
	this->Setcodec(child);
  }
  if (XMLString::compareString(elementname, X("technicalAttributes")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetechnicalAttributes; // FTT, check this
	}
	this->SettechnicalAttributes(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("comment")) == 0))
  {
	EbuCoredataFormatType_comment_CollectionPtr tmp = CreatedataFormatType_comment_CollectionType; // FTT, check this
	this->Setcomment(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoredataFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetcaptioningFormat() != Dc1NodePtr())
		result.Insert(GetcaptioningFormat());
	if (GetsubtitlingFormat() != Dc1NodePtr())
		result.Insert(GetsubtitlingFormat());
	if (GetancillaryDataFormat() != Dc1NodePtr())
		result.Insert(GetancillaryDataFormat());
	if (Getcodec() != Dc1NodePtr())
		result.Insert(Getcodec());
	if (GettechnicalAttributes() != Dc1NodePtr())
		result.Insert(GettechnicalAttributes());
	if (Getcomment() != Dc1NodePtr())
		result.Insert(Getcomment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMethodImpl.h


