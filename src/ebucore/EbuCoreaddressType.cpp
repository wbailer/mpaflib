
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaddressType_ExtImplInclude.h


#include "EbuCoreaddressType_addressLine_CollectionType.h"
#include "EbuCoreelementType.h"
#include "EbuCoreaddressType_country_LocalType.h"
#include "EbuCoreaddressType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaddressType::IEbuCoreaddressType()
{

// no includefile for extension defined 
// file EbuCoreaddressType_ExtPropInit.h

}

IEbuCoreaddressType::~IEbuCoreaddressType()
{
// no includefile for extension defined 
// file EbuCoreaddressType_ExtPropCleanup.h

}

EbuCoreaddressType::EbuCoreaddressType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaddressType::~EbuCoreaddressType()
{
	Cleanup();
}

void EbuCoreaddressType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_addressLine = EbuCoreaddressType_addressLine_CollectionPtr(); // Collection
	m_addressTownCity = EbuCoreelementPtr(); // Class
	m_addressTownCity_Exist = false;
	m_addressCountyState = EbuCoreelementPtr(); // Class
	m_addressCountyState_Exist = false;
	m_addressDeliveryCode = NULL; // Optional String
	m_addressDeliveryCode_Exist = false;
	m_country = EbuCoreaddressType_country_LocalPtr(); // Class
	m_country_Exist = false;


// no includefile for extension defined 
// file EbuCoreaddressType_ExtMyPropInit.h

}

void EbuCoreaddressType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaddressType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_addressLine);
	// Dc1Factory::DeleteObject(m_addressTownCity);
	// Dc1Factory::DeleteObject(m_addressCountyState);
	XMLString::release(&this->m_addressDeliveryCode);
	this->m_addressDeliveryCode = NULL;
	// Dc1Factory::DeleteObject(m_country);
}

void EbuCoreaddressType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use addressTypePtr, since we
	// might need GetBase(), which isn't defined in IaddressType
	const Dc1Ptr< EbuCoreaddressType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_addressLine);
		this->SetaddressLine(Dc1Factory::CloneObject(tmp->GetaddressLine()));
	if (tmp->IsValidaddressTownCity())
	{
		// Dc1Factory::DeleteObject(m_addressTownCity);
		this->SetaddressTownCity(Dc1Factory::CloneObject(tmp->GetaddressTownCity()));
	}
	else
	{
		InvalidateaddressTownCity();
	}
	if (tmp->IsValidaddressCountyState())
	{
		// Dc1Factory::DeleteObject(m_addressCountyState);
		this->SetaddressCountyState(Dc1Factory::CloneObject(tmp->GetaddressCountyState()));
	}
	else
	{
		InvalidateaddressCountyState();
	}
	if (tmp->IsValidaddressDeliveryCode())
	{
		this->SetaddressDeliveryCode(XMLString::replicate(tmp->GetaddressDeliveryCode()));
	}
	else
	{
		InvalidateaddressDeliveryCode();
	}
	if (tmp->IsValidcountry())
	{
		// Dc1Factory::DeleteObject(m_country);
		this->Setcountry(Dc1Factory::CloneObject(tmp->Getcountry()));
	}
	else
	{
		Invalidatecountry();
	}
}

Dc1NodePtr EbuCoreaddressType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaddressType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


EbuCoreaddressType_addressLine_CollectionPtr EbuCoreaddressType::GetaddressLine() const
{
		return m_addressLine;
}

EbuCoreelementPtr EbuCoreaddressType::GetaddressTownCity() const
{
		return m_addressTownCity;
}

// Element is optional
bool EbuCoreaddressType::IsValidaddressTownCity() const
{
	return m_addressTownCity_Exist;
}

EbuCoreelementPtr EbuCoreaddressType::GetaddressCountyState() const
{
		return m_addressCountyState;
}

// Element is optional
bool EbuCoreaddressType::IsValidaddressCountyState() const
{
	return m_addressCountyState_Exist;
}

XMLCh * EbuCoreaddressType::GetaddressDeliveryCode() const
{
		return m_addressDeliveryCode;
}

// Element is optional
bool EbuCoreaddressType::IsValidaddressDeliveryCode() const
{
	return m_addressDeliveryCode_Exist;
}

EbuCoreaddressType_country_LocalPtr EbuCoreaddressType::Getcountry() const
{
		return m_country;
}

// Element is optional
bool EbuCoreaddressType::IsValidcountry() const
{
	return m_country_Exist;
}

void EbuCoreaddressType::SetaddressLine(const EbuCoreaddressType_addressLine_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaddressType::SetaddressLine().");
	}
	if (m_addressLine != item)
	{
		// Dc1Factory::DeleteObject(m_addressLine);
		m_addressLine = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_addressLine) m_addressLine->SetParent(m_myself.getPointer());
		if(m_addressLine != EbuCoreaddressType_addressLine_CollectionPtr())
		{
			m_addressLine->SetContentName(XMLString::transcode("addressLine"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaddressType::SetaddressTownCity(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaddressType::SetaddressTownCity().");
	}
	if (m_addressTownCity != item || m_addressTownCity_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_addressTownCity);
		m_addressTownCity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_addressTownCity) m_addressTownCity->SetParent(m_myself.getPointer());
		if (m_addressTownCity && m_addressTownCity->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_addressTownCity->UseTypeAttribute = true;
		}
		m_addressTownCity_Exist = true;
		if(m_addressTownCity != EbuCoreelementPtr())
		{
			m_addressTownCity->SetContentName(XMLString::transcode("addressTownCity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaddressType::InvalidateaddressTownCity()
{
	m_addressTownCity_Exist = false;
}
void EbuCoreaddressType::SetaddressCountyState(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaddressType::SetaddressCountyState().");
	}
	if (m_addressCountyState != item || m_addressCountyState_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_addressCountyState);
		m_addressCountyState = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_addressCountyState) m_addressCountyState->SetParent(m_myself.getPointer());
		if (m_addressCountyState && m_addressCountyState->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_addressCountyState->UseTypeAttribute = true;
		}
		m_addressCountyState_Exist = true;
		if(m_addressCountyState != EbuCoreelementPtr())
		{
			m_addressCountyState->SetContentName(XMLString::transcode("addressCountyState"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaddressType::InvalidateaddressCountyState()
{
	m_addressCountyState_Exist = false;
}
void EbuCoreaddressType::SetaddressDeliveryCode(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaddressType::SetaddressDeliveryCode().");
	}
	if (m_addressDeliveryCode != item || m_addressDeliveryCode_Exist == false)
	{
		XMLString::release(&m_addressDeliveryCode);
		m_addressDeliveryCode = item;
		m_addressDeliveryCode_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaddressType::InvalidateaddressDeliveryCode()
{
	m_addressDeliveryCode_Exist = false;
}
void EbuCoreaddressType::Setcountry(const EbuCoreaddressType_country_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaddressType::Setcountry().");
	}
	if (m_country != item || m_country_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_country);
		m_country = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_country) m_country->SetParent(m_myself.getPointer());
		if (m_country && m_country->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType_country_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_country->UseTypeAttribute = true;
		}
		m_country_Exist = true;
		if(m_country != EbuCoreaddressType_country_LocalPtr())
		{
			m_country->SetContentName(XMLString::transcode("country"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaddressType::Invalidatecountry()
{
	m_country_Exist = false;
}

Dc1NodeEnum EbuCoreaddressType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("addressLine")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:addressLine is item of type elementType
		// in element collection addressType_addressLine_CollectionType
		
		context->Found = true;
		EbuCoreaddressType_addressLine_CollectionPtr coll = GetaddressLine();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaddressType_addressLine_CollectionType; // FTT, check this
				SetaddressLine(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("addressTownCity")) == 0)
	{
		// addressTownCity is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaddressTownCity()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaddressTownCity(p, client);
					if((p = GetaddressTownCity()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("addressCountyState")) == 0)
	{
		// addressCountyState is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaddressCountyState()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaddressCountyState(p, client);
					if((p = GetaddressCountyState()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("country")) == 0)
	{
		// country is simple element addressType_country_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcountry()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType_country_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreaddressType_country_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcountry(p, client);
					if((p = Getcountry()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for addressType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "addressType");
	}
	return result;
}

XMLCh * EbuCoreaddressType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaddressType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaddressType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("addressType"));
	// Element serialization:
	if (m_addressLine != EbuCoreaddressType_addressLine_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_addressLine->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_addressTownCity != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_addressTownCity->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("addressTownCity"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_addressTownCity->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_addressCountyState != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_addressCountyState->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("addressCountyState"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_addressCountyState->Serialize(doc, element, &elem);
	}
	 // String
	if(m_addressDeliveryCode != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_addressDeliveryCode, X("urn:ebu:metadata-schema:ebuCore_2014"), X("addressDeliveryCode"), true);
	// Class
	
	if (m_country != EbuCoreaddressType_country_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_country->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("country"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_country->Serialize(doc, element, &elem);
	}

}

bool EbuCoreaddressType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("addressLine"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("addressLine")) == 0))
		{
			// Deserialize factory type
			EbuCoreaddressType_addressLine_CollectionPtr tmp = CreateaddressType_addressLine_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaddressLine(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("addressTownCity"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("addressTownCity")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaddressTownCity(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("addressCountyState"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("addressCountyState")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaddressCountyState(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("addressDeliveryCode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetaddressDeliveryCode(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetaddressDeliveryCode(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("country"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("country")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaddressType_country_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcountry(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaddressType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaddressType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("addressLine")) == 0))
  {
	EbuCoreaddressType_addressLine_CollectionPtr tmp = CreateaddressType_addressLine_CollectionType; // FTT, check this
	this->SetaddressLine(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("addressTownCity")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->SetaddressTownCity(child);
  }
  if (XMLString::compareString(elementname, X("addressCountyState")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->SetaddressCountyState(child);
  }
  if (XMLString::compareString(elementname, X("country")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaddressType_country_LocalType; // FTT, check this
	}
	this->Setcountry(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaddressType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaddressLine() != Dc1NodePtr())
		result.Insert(GetaddressLine());
	if (GetaddressTownCity() != Dc1NodePtr())
		result.Insert(GetaddressTownCity());
	if (GetaddressCountyState() != Dc1NodePtr())
		result.Insert(GetaddressCountyState());
	if (Getcountry() != Dc1NodePtr())
		result.Insert(Getcountry());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaddressType_ExtMethodImpl.h


