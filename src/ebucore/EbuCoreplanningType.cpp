
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreplanningType_ExtImplInclude.h


#include "EbuCoreplanningType_publicationEvent_CollectionType.h"
#include "EbuCoreplanningType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorepublicationEventType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:publicationEvent

#include <assert.h>
IEbuCoreplanningType::IEbuCoreplanningType()
{

// no includefile for extension defined 
// file EbuCoreplanningType_ExtPropInit.h

}

IEbuCoreplanningType::~IEbuCoreplanningType()
{
// no includefile for extension defined 
// file EbuCoreplanningType_ExtPropCleanup.h

}

EbuCoreplanningType::EbuCoreplanningType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreplanningType::~EbuCoreplanningType()
{
	Cleanup();
}

void EbuCoreplanningType::Init()
{

	// Init attributes
	m_planningId = NULL; // String
	m_planningId_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_publicationEvent = EbuCoreplanningType_publicationEvent_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreplanningType_ExtMyPropInit.h

}

void EbuCoreplanningType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreplanningType_ExtMyPropCleanup.h


	XMLString::release(&m_planningId); // String
	// Dc1Factory::DeleteObject(m_publicationEvent);
}

void EbuCoreplanningType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use planningTypePtr, since we
	// might need GetBase(), which isn't defined in IplanningType
	const Dc1Ptr< EbuCoreplanningType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_planningId); // String
	if (tmp->ExistplanningId())
	{
		this->SetplanningId(XMLString::replicate(tmp->GetplanningId()));
	}
	else
	{
		InvalidateplanningId();
	}
	}
		// Dc1Factory::DeleteObject(m_publicationEvent);
		this->SetpublicationEvent(Dc1Factory::CloneObject(tmp->GetpublicationEvent()));
}

Dc1NodePtr EbuCoreplanningType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreplanningType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreplanningType::GetplanningId() const
{
	return m_planningId;
}

bool EbuCoreplanningType::ExistplanningId() const
{
	return m_planningId_Exist;
}
void EbuCoreplanningType::SetplanningId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreplanningType::SetplanningId().");
	}
	m_planningId = item;
	m_planningId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreplanningType::InvalidateplanningId()
{
	m_planningId_Exist = false;
}
EbuCoreplanningType_publicationEvent_CollectionPtr EbuCoreplanningType::GetpublicationEvent() const
{
		return m_publicationEvent;
}

void EbuCoreplanningType::SetpublicationEvent(const EbuCoreplanningType_publicationEvent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreplanningType::SetpublicationEvent().");
	}
	if (m_publicationEvent != item)
	{
		// Dc1Factory::DeleteObject(m_publicationEvent);
		m_publicationEvent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationEvent) m_publicationEvent->SetParent(m_myself.getPointer());
		if(m_publicationEvent != EbuCoreplanningType_publicationEvent_CollectionPtr())
		{
			m_publicationEvent->SetContentName(XMLString::transcode("publicationEvent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreplanningType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("planningId")) == 0)
	{
		// planningId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("publicationEvent")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:publicationEvent is item of type publicationEventType
		// in element collection planningType_publicationEvent_CollectionType
		
		context->Found = true;
		EbuCoreplanningType_publicationEvent_CollectionPtr coll = GetpublicationEvent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateplanningType_publicationEvent_CollectionType; // FTT, check this
				SetpublicationEvent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationEventType")))) != empty)
			{
				// Is type allowed
				EbuCorepublicationEventPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for planningType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "planningType");
	}
	return result;
}

XMLCh * EbuCoreplanningType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreplanningType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreplanningType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("planningType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_planningId_Exist)
	{
	// String
	if(m_planningId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("planningId"), m_planningId);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_publicationEvent != EbuCoreplanningType_publicationEvent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_publicationEvent->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreplanningType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("planningId")))
	{
		// Deserialize string type
		this->SetplanningId(Dc1Convert::TextToString(parent->getAttribute(X("planningId"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("publicationEvent"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("publicationEvent")) == 0))
		{
			// Deserialize factory type
			EbuCoreplanningType_publicationEvent_CollectionPtr tmp = CreateplanningType_publicationEvent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpublicationEvent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreplanningType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreplanningType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("publicationEvent")) == 0))
  {
	EbuCoreplanningType_publicationEvent_CollectionPtr tmp = CreateplanningType_publicationEvent_CollectionType; // FTT, check this
	this->SetpublicationEvent(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreplanningType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetpublicationEvent() != Dc1NodePtr())
		result.Insert(GetpublicationEvent());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreplanningType_ExtMethodImpl.h


