
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoresubjectType_ExtImplInclude.h


#include "EbuCoresubjectType_subject_CollectionType.h"
#include "EbuCoresubjectType_subjectDefinition_CollectionType.h"
#include "EbuCoreentityType.h"
#include "EbuCoresubjectType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:subject
#include "EbuCoreelementType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:subjectDefinition

#include <assert.h>
IEbuCoresubjectType::IEbuCoresubjectType()
{

// no includefile for extension defined 
// file EbuCoresubjectType_ExtPropInit.h

}

IEbuCoresubjectType::~IEbuCoresubjectType()
{
// no includefile for extension defined 
// file EbuCoresubjectType_ExtPropCleanup.h

}

EbuCoresubjectType::EbuCoresubjectType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoresubjectType::~EbuCoresubjectType()
{
	Cleanup();
}

void EbuCoresubjectType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_subject = EbuCoresubjectType_subject_CollectionPtr(); // Collection
	m_subjectCode = NULL; // Optional String
	m_subjectCode_Exist = false;
	m_subjectDefinition = EbuCoresubjectType_subjectDefinition_CollectionPtr(); // Collection
	m_attributor = EbuCoreentityPtr(); // Class
	m_attributor_Exist = false;


// no includefile for extension defined 
// file EbuCoresubjectType_ExtMyPropInit.h

}

void EbuCoresubjectType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoresubjectType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_subject);
	XMLString::release(&this->m_subjectCode);
	this->m_subjectCode = NULL;
	// Dc1Factory::DeleteObject(m_subjectDefinition);
	// Dc1Factory::DeleteObject(m_attributor);
}

void EbuCoresubjectType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use subjectTypePtr, since we
	// might need GetBase(), which isn't defined in IsubjectType
	const Dc1Ptr< EbuCoresubjectType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_subject);
		this->Setsubject(Dc1Factory::CloneObject(tmp->Getsubject()));
	if (tmp->IsValidsubjectCode())
	{
		this->SetsubjectCode(XMLString::replicate(tmp->GetsubjectCode()));
	}
	else
	{
		InvalidatesubjectCode();
	}
		// Dc1Factory::DeleteObject(m_subjectDefinition);
		this->SetsubjectDefinition(Dc1Factory::CloneObject(tmp->GetsubjectDefinition()));
	if (tmp->IsValidattributor())
	{
		// Dc1Factory::DeleteObject(m_attributor);
		this->Setattributor(Dc1Factory::CloneObject(tmp->Getattributor()));
	}
	else
	{
		Invalidateattributor();
	}
}

Dc1NodePtr EbuCoresubjectType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoresubjectType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoresubjectType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoresubjectType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoresubjectType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoresubjectType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoresubjectType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoresubjectType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoresubjectType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoresubjectType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoresubjectType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoresubjectType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoresubjectType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoresubjectType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoresubjectType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoresubjectType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoresubjectType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoresubjectType::Getnote() const
{
	return m_note;
}

bool EbuCoresubjectType::Existnote() const
{
	return m_note_Exist;
}
void EbuCoresubjectType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::Invalidatenote()
{
	m_note_Exist = false;
}
EbuCoresubjectType_subject_CollectionPtr EbuCoresubjectType::Getsubject() const
{
		return m_subject;
}

XMLCh * EbuCoresubjectType::GetsubjectCode() const
{
		return m_subjectCode;
}

// Element is optional
bool EbuCoresubjectType::IsValidsubjectCode() const
{
	return m_subjectCode_Exist;
}

EbuCoresubjectType_subjectDefinition_CollectionPtr EbuCoresubjectType::GetsubjectDefinition() const
{
		return m_subjectDefinition;
}

EbuCoreentityPtr EbuCoresubjectType::Getattributor() const
{
		return m_attributor;
}

// Element is optional
bool EbuCoresubjectType::IsValidattributor() const
{
	return m_attributor_Exist;
}

void EbuCoresubjectType::Setsubject(const EbuCoresubjectType_subject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::Setsubject().");
	}
	if (m_subject != item)
	{
		// Dc1Factory::DeleteObject(m_subject);
		m_subject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_subject) m_subject->SetParent(m_myself.getPointer());
		if(m_subject != EbuCoresubjectType_subject_CollectionPtr())
		{
			m_subject->SetContentName(XMLString::transcode("subject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::SetsubjectCode(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SetsubjectCode().");
	}
	if (m_subjectCode != item || m_subjectCode_Exist == false)
	{
		XMLString::release(&m_subjectCode);
		m_subjectCode = item;
		m_subjectCode_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::InvalidatesubjectCode()
{
	m_subjectCode_Exist = false;
}
void EbuCoresubjectType::SetsubjectDefinition(const EbuCoresubjectType_subjectDefinition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::SetsubjectDefinition().");
	}
	if (m_subjectDefinition != item)
	{
		// Dc1Factory::DeleteObject(m_subjectDefinition);
		m_subjectDefinition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_subjectDefinition) m_subjectDefinition->SetParent(m_myself.getPointer());
		if(m_subjectDefinition != EbuCoresubjectType_subjectDefinition_CollectionPtr())
		{
			m_subjectDefinition->SetContentName(XMLString::transcode("subjectDefinition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::Setattributor(const EbuCoreentityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoresubjectType::Setattributor().");
	}
	if (m_attributor != item || m_attributor_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_attributor);
		m_attributor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_attributor) m_attributor->SetParent(m_myself.getPointer());
		if (m_attributor && m_attributor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_attributor->UseTypeAttribute = true;
		}
		m_attributor_Exist = true;
		if(m_attributor != EbuCoreentityPtr())
		{
			m_attributor->SetContentName(XMLString::transcode("attributor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoresubjectType::Invalidateattributor()
{
	m_attributor_Exist = false;
}

Dc1NodeEnum EbuCoresubjectType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  6, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("subject")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:subject is item of type elementType
		// in element collection subjectType_subject_CollectionType
		
		context->Found = true;
		EbuCoresubjectType_subject_CollectionPtr coll = Getsubject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatesubjectType_subject_CollectionType; // FTT, check this
				Setsubject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("subjectDefinition")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:subjectDefinition is item of type elementType
		// in element collection subjectType_subjectDefinition_CollectionType
		
		context->Found = true;
		EbuCoresubjectType_subjectDefinition_CollectionPtr coll = GetsubjectDefinition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatesubjectType_subjectDefinition_CollectionType; // FTT, check this
				SetsubjectDefinition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("attributor")) == 0)
	{
		// attributor is simple element entityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getattributor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setattributor(p, client);
					if((p = Getattributor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for subjectType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "subjectType");
	}
	return result;
}

XMLCh * EbuCoresubjectType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoresubjectType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoresubjectType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("subjectType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_subject != EbuCoresubjectType_subject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_subject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	 // String
	if(m_subjectCode != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_subjectCode, X("urn:ebu:metadata-schema:ebuCore_2014"), X("subjectCode"), true);
	if (m_subjectDefinition != EbuCoresubjectType_subjectDefinition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_subjectDefinition->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_attributor != EbuCoreentityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_attributor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("attributor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_attributor->Serialize(doc, element, &elem);
	}

}

bool EbuCoresubjectType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("subject"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("subject")) == 0))
		{
			// Deserialize factory type
			EbuCoresubjectType_subject_CollectionPtr tmp = CreatesubjectType_subject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setsubject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("subjectCode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetsubjectCode(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetsubjectCode(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("subjectDefinition"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("subjectDefinition")) == 0))
		{
			// Deserialize factory type
			EbuCoresubjectType_subjectDefinition_CollectionPtr tmp = CreatesubjectType_subjectDefinition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetsubjectDefinition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("attributor"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("attributor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateentityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setattributor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoresubjectType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoresubjectType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("subject")) == 0))
  {
	EbuCoresubjectType_subject_CollectionPtr tmp = CreatesubjectType_subject_CollectionType; // FTT, check this
	this->Setsubject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("subjectDefinition")) == 0))
  {
	EbuCoresubjectType_subjectDefinition_CollectionPtr tmp = CreatesubjectType_subjectDefinition_CollectionType; // FTT, check this
	this->SetsubjectDefinition(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("attributor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateentityType; // FTT, check this
	}
	this->Setattributor(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoresubjectType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getsubject() != Dc1NodePtr())
		result.Insert(Getsubject());
	if (GetsubjectDefinition() != Dc1NodePtr())
		result.Insert(GetsubjectDefinition());
	if (Getattributor() != Dc1NodePtr())
		result.Insert(Getattributor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoresubjectType_ExtMethodImpl.h


