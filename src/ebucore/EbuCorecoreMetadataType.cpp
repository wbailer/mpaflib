
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtImplInclude.h


#include "EbuCorecoreMetadataType_title_CollectionType.h"
#include "EbuCorecoreMetadataType_alternativeTitle_CollectionType.h"
#include "EbuCorecoreMetadataType_creator_CollectionType.h"
#include "EbuCorecoreMetadataType_subject_CollectionType.h"
#include "EbuCorecoreMetadataType_description_CollectionType.h"
#include "EbuCorecoreMetadataType_publisher_CollectionType.h"
#include "EbuCorecoreMetadataType_contributor_CollectionType.h"
#include "EbuCorecoreMetadataType_date_CollectionType.h"
#include "EbuCorecoreMetadataType_type_CollectionType.h"
#include "EbuCorecoreMetadataType_format_CollectionType.h"
#include "EbuCorecoreMetadataType_identifier_CollectionType.h"
#include "EbuCorecoreMetadataType_source_CollectionType.h"
#include "EbuCorecoreMetadataType_language_CollectionType.h"
#include "EbuCorecoreMetadataType_relation_CollectionType.h"
#include "EbuCorecoreMetadataType_isRelatedTo_CollectionType.h"
#include "EbuCorecoreMetadataType_isNextInSequence_CollectionType.h"
#include "EbuCorecoreMetadataType_isVersionOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasVersion_CollectionType.h"
#include "EbuCorecoreMetadataType_isReplacedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_replaces_CollectionType.h"
#include "EbuCorecoreMetadataType_isRequiredBy_CollectionType.h"
#include "EbuCorecoreMetadataType_requires_CollectionType.h"
#include "EbuCorecoreMetadataType_isPartOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasPart_CollectionType.h"
#include "EbuCorecoreMetadataType_hasTrackPart_CollectionType.h"
#include "EbuCorecoreMetadataType_isReferencedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_references_CollectionType.h"
#include "EbuCorecoreMetadataType_isFormatOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasFormat_CollectionType.h"
#include "EbuCorecoreMetadataType_isEpisodeOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isSeasonOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasEpisode_CollectionType.h"
#include "EbuCorecoreMetadataType_hasSeason_CollectionType.h"
#include "EbuCorecoreMetadataType_isMemberOf_CollectionType.h"
#include "EbuCorecoreMetadataType_hasMember_CollectionType.h"
#include "EbuCorecoreMetadataType_sameAs_CollectionType.h"
#include "EbuCorecoreMetadataType_coverage_CollectionType.h"
#include "EbuCorecoreMetadataType_rights_CollectionType.h"
#include "EbuCorecoreMetadataType_version_CollectionType.h"
#include "EbuCorecoreMetadataType_publicationHistory_CollectionType.h"
#include "EbuCorecoreMetadataType_planning_CollectionType.h"
#include "EbuCorecoreMetadataType_rating_CollectionType.h"
#include "EbuCorecoreMetadataType_part_CollectionType.h"
#include "EbuCorecoreMetadataType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoretitleType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:title
#include "EbuCorealternativeTitleType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:alternativeTitle
#include "EbuCoreentityType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:creator
#include "EbuCoresubjectType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:subject
#include "EbuCoredescriptionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:description
#include "EbuCoredateType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:date
#include "EbuCoretypeType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:type
#include "EbuCoreformatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:format
#include "EbuCoreidentifierType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:identifier
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:source
#include "EbuCorelanguageType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:language
#include "EbuCorerelationType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:relation
#include "EbuCorecoreMetadataType_hasTrackPart_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:hasTrackPart
#include "EbuCorecoverageType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:coverage
#include "EbuCorerightsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rights
#include "EbuCoreversionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:version
#include "EbuCorepublicationHistoryType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:publicationHistory
#include "EbuCoreplanningType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:planning
#include "EbuCoreratingType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rating
#include "EbuCorepartType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:part

#include <assert.h>
IEbuCorecoreMetadataType::IEbuCorecoreMetadataType()
{

// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtPropInit.h

}

IEbuCorecoreMetadataType::~IEbuCorecoreMetadataType()
{
// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtPropCleanup.h

}

EbuCorecoreMetadataType::EbuCorecoreMetadataType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecoreMetadataType::~EbuCorecoreMetadataType()
{
	Cleanup();
}

void EbuCorecoreMetadataType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_title = EbuCorecoreMetadataType_title_CollectionPtr(); // Collection
	m_alternativeTitle = EbuCorecoreMetadataType_alternativeTitle_CollectionPtr(); // Collection
	m_creator = EbuCorecoreMetadataType_creator_CollectionPtr(); // Collection
	m_subject = EbuCorecoreMetadataType_subject_CollectionPtr(); // Collection
	m_description = EbuCorecoreMetadataType_description_CollectionPtr(); // Collection
	m_publisher = EbuCorecoreMetadataType_publisher_CollectionPtr(); // Collection
	m_contributor = EbuCorecoreMetadataType_contributor_CollectionPtr(); // Collection
	m_date = EbuCorecoreMetadataType_date_CollectionPtr(); // Collection
	m_type = EbuCorecoreMetadataType_type_CollectionPtr(); // Collection
	m_format = EbuCorecoreMetadataType_format_CollectionPtr(); // Collection
	m_identifier = EbuCorecoreMetadataType_identifier_CollectionPtr(); // Collection
	m_source = EbuCorecoreMetadataType_source_CollectionPtr(); // Collection
	m_language = EbuCorecoreMetadataType_language_CollectionPtr(); // Collection
	m_relation = EbuCorecoreMetadataType_relation_CollectionPtr(); // Collection
	m_isRelatedTo = EbuCorecoreMetadataType_isRelatedTo_CollectionPtr(); // Collection
	m_isNextInSequence = EbuCorecoreMetadataType_isNextInSequence_CollectionPtr(); // Collection
	m_isVersionOf = EbuCorecoreMetadataType_isVersionOf_CollectionPtr(); // Collection
	m_hasVersion = EbuCorecoreMetadataType_hasVersion_CollectionPtr(); // Collection
	m_isReplacedBy = EbuCorecoreMetadataType_isReplacedBy_CollectionPtr(); // Collection
	m_replaces = EbuCorecoreMetadataType_replaces_CollectionPtr(); // Collection
	m_isRequiredBy = EbuCorecoreMetadataType_isRequiredBy_CollectionPtr(); // Collection
	m_requires = EbuCorecoreMetadataType_requires_CollectionPtr(); // Collection
	m_isPartOf = EbuCorecoreMetadataType_isPartOf_CollectionPtr(); // Collection
	m_hasPart = EbuCorecoreMetadataType_hasPart_CollectionPtr(); // Collection
	m_hasTrackPart = EbuCorecoreMetadataType_hasTrackPart_CollectionPtr(); // Collection
	m_isReferencedBy = EbuCorecoreMetadataType_isReferencedBy_CollectionPtr(); // Collection
	m_references = EbuCorecoreMetadataType_references_CollectionPtr(); // Collection
	m_isFormatOf = EbuCorecoreMetadataType_isFormatOf_CollectionPtr(); // Collection
	m_hasFormat = EbuCorecoreMetadataType_hasFormat_CollectionPtr(); // Collection
	m_isEpisodeOf = EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr(); // Collection
	m_isSeasonOf = EbuCorecoreMetadataType_isSeasonOf_CollectionPtr(); // Collection
	m_hasEpisode = EbuCorecoreMetadataType_hasEpisode_CollectionPtr(); // Collection
	m_hasSeason = EbuCorecoreMetadataType_hasSeason_CollectionPtr(); // Collection
	m_isMemberOf = EbuCorecoreMetadataType_isMemberOf_CollectionPtr(); // Collection
	m_hasMember = EbuCorecoreMetadataType_hasMember_CollectionPtr(); // Collection
	m_sameAs = EbuCorecoreMetadataType_sameAs_CollectionPtr(); // Collection
	m_coverage = EbuCorecoreMetadataType_coverage_CollectionPtr(); // Collection
	m_rights = EbuCorecoreMetadataType_rights_CollectionPtr(); // Collection
	m_version = EbuCorecoreMetadataType_version_CollectionPtr(); // Collection
	m_publicationHistory = EbuCorecoreMetadataType_publicationHistory_CollectionPtr(); // Collection
	m_planning = EbuCorecoreMetadataType_planning_CollectionPtr(); // Collection
	m_rating = EbuCorecoreMetadataType_rating_CollectionPtr(); // Collection
	m_part = EbuCorecoreMetadataType_part_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtMyPropInit.h

}

void EbuCorecoreMetadataType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_title);
	// Dc1Factory::DeleteObject(m_alternativeTitle);
	// Dc1Factory::DeleteObject(m_creator);
	// Dc1Factory::DeleteObject(m_subject);
	// Dc1Factory::DeleteObject(m_description);
	// Dc1Factory::DeleteObject(m_publisher);
	// Dc1Factory::DeleteObject(m_contributor);
	// Dc1Factory::DeleteObject(m_date);
	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_format);
	// Dc1Factory::DeleteObject(m_identifier);
	// Dc1Factory::DeleteObject(m_source);
	// Dc1Factory::DeleteObject(m_language);
	// Dc1Factory::DeleteObject(m_relation);
	// Dc1Factory::DeleteObject(m_isRelatedTo);
	// Dc1Factory::DeleteObject(m_isNextInSequence);
	// Dc1Factory::DeleteObject(m_isVersionOf);
	// Dc1Factory::DeleteObject(m_hasVersion);
	// Dc1Factory::DeleteObject(m_isReplacedBy);
	// Dc1Factory::DeleteObject(m_replaces);
	// Dc1Factory::DeleteObject(m_isRequiredBy);
	// Dc1Factory::DeleteObject(m_requires);
	// Dc1Factory::DeleteObject(m_isPartOf);
	// Dc1Factory::DeleteObject(m_hasPart);
	// Dc1Factory::DeleteObject(m_hasTrackPart);
	// Dc1Factory::DeleteObject(m_isReferencedBy);
	// Dc1Factory::DeleteObject(m_references);
	// Dc1Factory::DeleteObject(m_isFormatOf);
	// Dc1Factory::DeleteObject(m_hasFormat);
	// Dc1Factory::DeleteObject(m_isEpisodeOf);
	// Dc1Factory::DeleteObject(m_isSeasonOf);
	// Dc1Factory::DeleteObject(m_hasEpisode);
	// Dc1Factory::DeleteObject(m_hasSeason);
	// Dc1Factory::DeleteObject(m_isMemberOf);
	// Dc1Factory::DeleteObject(m_hasMember);
	// Dc1Factory::DeleteObject(m_sameAs);
	// Dc1Factory::DeleteObject(m_coverage);
	// Dc1Factory::DeleteObject(m_rights);
	// Dc1Factory::DeleteObject(m_version);
	// Dc1Factory::DeleteObject(m_publicationHistory);
	// Dc1Factory::DeleteObject(m_planning);
	// Dc1Factory::DeleteObject(m_rating);
	// Dc1Factory::DeleteObject(m_part);
}

void EbuCorecoreMetadataType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use coreMetadataTypePtr, since we
	// might need GetBase(), which isn't defined in IcoreMetadataType
	const Dc1Ptr< EbuCorecoreMetadataType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_title);
		this->Settitle(Dc1Factory::CloneObject(tmp->Gettitle()));
		// Dc1Factory::DeleteObject(m_alternativeTitle);
		this->SetalternativeTitle(Dc1Factory::CloneObject(tmp->GetalternativeTitle()));
		// Dc1Factory::DeleteObject(m_creator);
		this->Setcreator(Dc1Factory::CloneObject(tmp->Getcreator()));
		// Dc1Factory::DeleteObject(m_subject);
		this->Setsubject(Dc1Factory::CloneObject(tmp->Getsubject()));
		// Dc1Factory::DeleteObject(m_description);
		this->Setdescription(Dc1Factory::CloneObject(tmp->Getdescription()));
		// Dc1Factory::DeleteObject(m_publisher);
		this->Setpublisher(Dc1Factory::CloneObject(tmp->Getpublisher()));
		// Dc1Factory::DeleteObject(m_contributor);
		this->Setcontributor(Dc1Factory::CloneObject(tmp->Getcontributor()));
		// Dc1Factory::DeleteObject(m_date);
		this->Setdate(Dc1Factory::CloneObject(tmp->Getdate()));
		// Dc1Factory::DeleteObject(m_type);
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
		// Dc1Factory::DeleteObject(m_format);
		this->Setformat(Dc1Factory::CloneObject(tmp->Getformat()));
		// Dc1Factory::DeleteObject(m_identifier);
		this->Setidentifier(Dc1Factory::CloneObject(tmp->Getidentifier()));
		// Dc1Factory::DeleteObject(m_source);
		this->Setsource(Dc1Factory::CloneObject(tmp->Getsource()));
		// Dc1Factory::DeleteObject(m_language);
		this->Setlanguage(Dc1Factory::CloneObject(tmp->Getlanguage()));
		// Dc1Factory::DeleteObject(m_relation);
		this->Setrelation(Dc1Factory::CloneObject(tmp->Getrelation()));
		// Dc1Factory::DeleteObject(m_isRelatedTo);
		this->SetisRelatedTo(Dc1Factory::CloneObject(tmp->GetisRelatedTo()));
		// Dc1Factory::DeleteObject(m_isNextInSequence);
		this->SetisNextInSequence(Dc1Factory::CloneObject(tmp->GetisNextInSequence()));
		// Dc1Factory::DeleteObject(m_isVersionOf);
		this->SetisVersionOf(Dc1Factory::CloneObject(tmp->GetisVersionOf()));
		// Dc1Factory::DeleteObject(m_hasVersion);
		this->SethasVersion(Dc1Factory::CloneObject(tmp->GethasVersion()));
		// Dc1Factory::DeleteObject(m_isReplacedBy);
		this->SetisReplacedBy(Dc1Factory::CloneObject(tmp->GetisReplacedBy()));
		// Dc1Factory::DeleteObject(m_replaces);
		this->Setreplaces(Dc1Factory::CloneObject(tmp->Getreplaces()));
		// Dc1Factory::DeleteObject(m_isRequiredBy);
		this->SetisRequiredBy(Dc1Factory::CloneObject(tmp->GetisRequiredBy()));
		// Dc1Factory::DeleteObject(m_requires);
		this->Setrequires(Dc1Factory::CloneObject(tmp->Getrequires()));
		// Dc1Factory::DeleteObject(m_isPartOf);
		this->SetisPartOf(Dc1Factory::CloneObject(tmp->GetisPartOf()));
		// Dc1Factory::DeleteObject(m_hasPart);
		this->SethasPart(Dc1Factory::CloneObject(tmp->GethasPart()));
		// Dc1Factory::DeleteObject(m_hasTrackPart);
		this->SethasTrackPart(Dc1Factory::CloneObject(tmp->GethasTrackPart()));
		// Dc1Factory::DeleteObject(m_isReferencedBy);
		this->SetisReferencedBy(Dc1Factory::CloneObject(tmp->GetisReferencedBy()));
		// Dc1Factory::DeleteObject(m_references);
		this->Setreferences(Dc1Factory::CloneObject(tmp->Getreferences()));
		// Dc1Factory::DeleteObject(m_isFormatOf);
		this->SetisFormatOf(Dc1Factory::CloneObject(tmp->GetisFormatOf()));
		// Dc1Factory::DeleteObject(m_hasFormat);
		this->SethasFormat(Dc1Factory::CloneObject(tmp->GethasFormat()));
		// Dc1Factory::DeleteObject(m_isEpisodeOf);
		this->SetisEpisodeOf(Dc1Factory::CloneObject(tmp->GetisEpisodeOf()));
		// Dc1Factory::DeleteObject(m_isSeasonOf);
		this->SetisSeasonOf(Dc1Factory::CloneObject(tmp->GetisSeasonOf()));
		// Dc1Factory::DeleteObject(m_hasEpisode);
		this->SethasEpisode(Dc1Factory::CloneObject(tmp->GethasEpisode()));
		// Dc1Factory::DeleteObject(m_hasSeason);
		this->SethasSeason(Dc1Factory::CloneObject(tmp->GethasSeason()));
		// Dc1Factory::DeleteObject(m_isMemberOf);
		this->SetisMemberOf(Dc1Factory::CloneObject(tmp->GetisMemberOf()));
		// Dc1Factory::DeleteObject(m_hasMember);
		this->SethasMember(Dc1Factory::CloneObject(tmp->GethasMember()));
		// Dc1Factory::DeleteObject(m_sameAs);
		this->SetsameAs(Dc1Factory::CloneObject(tmp->GetsameAs()));
		// Dc1Factory::DeleteObject(m_coverage);
		this->Setcoverage(Dc1Factory::CloneObject(tmp->Getcoverage()));
		// Dc1Factory::DeleteObject(m_rights);
		this->Setrights(Dc1Factory::CloneObject(tmp->Getrights()));
		// Dc1Factory::DeleteObject(m_version);
		this->Setversion(Dc1Factory::CloneObject(tmp->Getversion()));
		// Dc1Factory::DeleteObject(m_publicationHistory);
		this->SetpublicationHistory(Dc1Factory::CloneObject(tmp->GetpublicationHistory()));
		// Dc1Factory::DeleteObject(m_planning);
		this->Setplanning(Dc1Factory::CloneObject(tmp->Getplanning()));
		// Dc1Factory::DeleteObject(m_rating);
		this->Setrating(Dc1Factory::CloneObject(tmp->Getrating()));
		// Dc1Factory::DeleteObject(m_part);
		this->Setpart(Dc1Factory::CloneObject(tmp->Getpart()));
}

Dc1NodePtr EbuCorecoreMetadataType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorecoreMetadataType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


EbuCorecoreMetadataType_title_CollectionPtr EbuCorecoreMetadataType::Gettitle() const
{
		return m_title;
}

EbuCorecoreMetadataType_alternativeTitle_CollectionPtr EbuCorecoreMetadataType::GetalternativeTitle() const
{
		return m_alternativeTitle;
}

EbuCorecoreMetadataType_creator_CollectionPtr EbuCorecoreMetadataType::Getcreator() const
{
		return m_creator;
}

EbuCorecoreMetadataType_subject_CollectionPtr EbuCorecoreMetadataType::Getsubject() const
{
		return m_subject;
}

EbuCorecoreMetadataType_description_CollectionPtr EbuCorecoreMetadataType::Getdescription() const
{
		return m_description;
}

EbuCorecoreMetadataType_publisher_CollectionPtr EbuCorecoreMetadataType::Getpublisher() const
{
		return m_publisher;
}

EbuCorecoreMetadataType_contributor_CollectionPtr EbuCorecoreMetadataType::Getcontributor() const
{
		return m_contributor;
}

EbuCorecoreMetadataType_date_CollectionPtr EbuCorecoreMetadataType::Getdate() const
{
		return m_date;
}

EbuCorecoreMetadataType_type_CollectionPtr EbuCorecoreMetadataType::Gettype() const
{
		return m_type;
}

EbuCorecoreMetadataType_format_CollectionPtr EbuCorecoreMetadataType::Getformat() const
{
		return m_format;
}

EbuCorecoreMetadataType_identifier_CollectionPtr EbuCorecoreMetadataType::Getidentifier() const
{
		return m_identifier;
}

EbuCorecoreMetadataType_source_CollectionPtr EbuCorecoreMetadataType::Getsource() const
{
		return m_source;
}

EbuCorecoreMetadataType_language_CollectionPtr EbuCorecoreMetadataType::Getlanguage() const
{
		return m_language;
}

EbuCorecoreMetadataType_relation_CollectionPtr EbuCorecoreMetadataType::Getrelation() const
{
		return m_relation;
}

EbuCorecoreMetadataType_isRelatedTo_CollectionPtr EbuCorecoreMetadataType::GetisRelatedTo() const
{
		return m_isRelatedTo;
}

EbuCorecoreMetadataType_isNextInSequence_CollectionPtr EbuCorecoreMetadataType::GetisNextInSequence() const
{
		return m_isNextInSequence;
}

EbuCorecoreMetadataType_isVersionOf_CollectionPtr EbuCorecoreMetadataType::GetisVersionOf() const
{
		return m_isVersionOf;
}

EbuCorecoreMetadataType_hasVersion_CollectionPtr EbuCorecoreMetadataType::GethasVersion() const
{
		return m_hasVersion;
}

EbuCorecoreMetadataType_isReplacedBy_CollectionPtr EbuCorecoreMetadataType::GetisReplacedBy() const
{
		return m_isReplacedBy;
}

EbuCorecoreMetadataType_replaces_CollectionPtr EbuCorecoreMetadataType::Getreplaces() const
{
		return m_replaces;
}

EbuCorecoreMetadataType_isRequiredBy_CollectionPtr EbuCorecoreMetadataType::GetisRequiredBy() const
{
		return m_isRequiredBy;
}

EbuCorecoreMetadataType_requires_CollectionPtr EbuCorecoreMetadataType::Getrequires() const
{
		return m_requires;
}

EbuCorecoreMetadataType_isPartOf_CollectionPtr EbuCorecoreMetadataType::GetisPartOf() const
{
		return m_isPartOf;
}

EbuCorecoreMetadataType_hasPart_CollectionPtr EbuCorecoreMetadataType::GethasPart() const
{
		return m_hasPart;
}

EbuCorecoreMetadataType_hasTrackPart_CollectionPtr EbuCorecoreMetadataType::GethasTrackPart() const
{
		return m_hasTrackPart;
}

EbuCorecoreMetadataType_isReferencedBy_CollectionPtr EbuCorecoreMetadataType::GetisReferencedBy() const
{
		return m_isReferencedBy;
}

EbuCorecoreMetadataType_references_CollectionPtr EbuCorecoreMetadataType::Getreferences() const
{
		return m_references;
}

EbuCorecoreMetadataType_isFormatOf_CollectionPtr EbuCorecoreMetadataType::GetisFormatOf() const
{
		return m_isFormatOf;
}

EbuCorecoreMetadataType_hasFormat_CollectionPtr EbuCorecoreMetadataType::GethasFormat() const
{
		return m_hasFormat;
}

EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr EbuCorecoreMetadataType::GetisEpisodeOf() const
{
		return m_isEpisodeOf;
}

EbuCorecoreMetadataType_isSeasonOf_CollectionPtr EbuCorecoreMetadataType::GetisSeasonOf() const
{
		return m_isSeasonOf;
}

EbuCorecoreMetadataType_hasEpisode_CollectionPtr EbuCorecoreMetadataType::GethasEpisode() const
{
		return m_hasEpisode;
}

EbuCorecoreMetadataType_hasSeason_CollectionPtr EbuCorecoreMetadataType::GethasSeason() const
{
		return m_hasSeason;
}

EbuCorecoreMetadataType_isMemberOf_CollectionPtr EbuCorecoreMetadataType::GetisMemberOf() const
{
		return m_isMemberOf;
}

EbuCorecoreMetadataType_hasMember_CollectionPtr EbuCorecoreMetadataType::GethasMember() const
{
		return m_hasMember;
}

EbuCorecoreMetadataType_sameAs_CollectionPtr EbuCorecoreMetadataType::GetsameAs() const
{
		return m_sameAs;
}

EbuCorecoreMetadataType_coverage_CollectionPtr EbuCorecoreMetadataType::Getcoverage() const
{
		return m_coverage;
}

EbuCorecoreMetadataType_rights_CollectionPtr EbuCorecoreMetadataType::Getrights() const
{
		return m_rights;
}

EbuCorecoreMetadataType_version_CollectionPtr EbuCorecoreMetadataType::Getversion() const
{
		return m_version;
}

EbuCorecoreMetadataType_publicationHistory_CollectionPtr EbuCorecoreMetadataType::GetpublicationHistory() const
{
		return m_publicationHistory;
}

EbuCorecoreMetadataType_planning_CollectionPtr EbuCorecoreMetadataType::Getplanning() const
{
		return m_planning;
}

EbuCorecoreMetadataType_rating_CollectionPtr EbuCorecoreMetadataType::Getrating() const
{
		return m_rating;
}

EbuCorecoreMetadataType_part_CollectionPtr EbuCorecoreMetadataType::Getpart() const
{
		return m_part;
}

void EbuCorecoreMetadataType::Settitle(const EbuCorecoreMetadataType_title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Settitle().");
	}
	if (m_title != item)
	{
		// Dc1Factory::DeleteObject(m_title);
		m_title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_title) m_title->SetParent(m_myself.getPointer());
		if(m_title != EbuCorecoreMetadataType_title_CollectionPtr())
		{
			m_title->SetContentName(XMLString::transcode("title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetalternativeTitle(const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetalternativeTitle().");
	}
	if (m_alternativeTitle != item)
	{
		// Dc1Factory::DeleteObject(m_alternativeTitle);
		m_alternativeTitle = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_alternativeTitle) m_alternativeTitle->SetParent(m_myself.getPointer());
		if(m_alternativeTitle != EbuCorecoreMetadataType_alternativeTitle_CollectionPtr())
		{
			m_alternativeTitle->SetContentName(XMLString::transcode("alternativeTitle"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setcreator(const EbuCorecoreMetadataType_creator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setcreator().");
	}
	if (m_creator != item)
	{
		// Dc1Factory::DeleteObject(m_creator);
		m_creator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_creator) m_creator->SetParent(m_myself.getPointer());
		if(m_creator != EbuCorecoreMetadataType_creator_CollectionPtr())
		{
			m_creator->SetContentName(XMLString::transcode("creator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setsubject(const EbuCorecoreMetadataType_subject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setsubject().");
	}
	if (m_subject != item)
	{
		// Dc1Factory::DeleteObject(m_subject);
		m_subject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_subject) m_subject->SetParent(m_myself.getPointer());
		if(m_subject != EbuCorecoreMetadataType_subject_CollectionPtr())
		{
			m_subject->SetContentName(XMLString::transcode("subject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setdescription(const EbuCorecoreMetadataType_description_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setdescription().");
	}
	if (m_description != item)
	{
		// Dc1Factory::DeleteObject(m_description);
		m_description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_description) m_description->SetParent(m_myself.getPointer());
		if(m_description != EbuCorecoreMetadataType_description_CollectionPtr())
		{
			m_description->SetContentName(XMLString::transcode("description"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setpublisher(const EbuCorecoreMetadataType_publisher_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setpublisher().");
	}
	if (m_publisher != item)
	{
		// Dc1Factory::DeleteObject(m_publisher);
		m_publisher = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publisher) m_publisher->SetParent(m_myself.getPointer());
		if(m_publisher != EbuCorecoreMetadataType_publisher_CollectionPtr())
		{
			m_publisher->SetContentName(XMLString::transcode("publisher"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setcontributor(const EbuCorecoreMetadataType_contributor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setcontributor().");
	}
	if (m_contributor != item)
	{
		// Dc1Factory::DeleteObject(m_contributor);
		m_contributor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_contributor) m_contributor->SetParent(m_myself.getPointer());
		if(m_contributor != EbuCorecoreMetadataType_contributor_CollectionPtr())
		{
			m_contributor->SetContentName(XMLString::transcode("contributor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setdate(const EbuCorecoreMetadataType_date_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setdate().");
	}
	if (m_date != item)
	{
		// Dc1Factory::DeleteObject(m_date);
		m_date = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_date) m_date->SetParent(m_myself.getPointer());
		if(m_date != EbuCorecoreMetadataType_date_CollectionPtr())
		{
			m_date->SetContentName(XMLString::transcode("date"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Settype(const EbuCorecoreMetadataType_type_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Settype().");
	}
	if (m_type != item)
	{
		// Dc1Factory::DeleteObject(m_type);
		m_type = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_type) m_type->SetParent(m_myself.getPointer());
		if(m_type != EbuCorecoreMetadataType_type_CollectionPtr())
		{
			m_type->SetContentName(XMLString::transcode("type"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setformat(const EbuCorecoreMetadataType_format_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setformat().");
	}
	if (m_format != item)
	{
		// Dc1Factory::DeleteObject(m_format);
		m_format = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_format) m_format->SetParent(m_myself.getPointer());
		if(m_format != EbuCorecoreMetadataType_format_CollectionPtr())
		{
			m_format->SetContentName(XMLString::transcode("format"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setidentifier(const EbuCorecoreMetadataType_identifier_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setidentifier().");
	}
	if (m_identifier != item)
	{
		// Dc1Factory::DeleteObject(m_identifier);
		m_identifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_identifier) m_identifier->SetParent(m_myself.getPointer());
		if(m_identifier != EbuCorecoreMetadataType_identifier_CollectionPtr())
		{
			m_identifier->SetContentName(XMLString::transcode("identifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setsource(const EbuCorecoreMetadataType_source_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setsource().");
	}
	if (m_source != item)
	{
		// Dc1Factory::DeleteObject(m_source);
		m_source = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_source) m_source->SetParent(m_myself.getPointer());
		if(m_source != EbuCorecoreMetadataType_source_CollectionPtr())
		{
			m_source->SetContentName(XMLString::transcode("source"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setlanguage(const EbuCorecoreMetadataType_language_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setlanguage().");
	}
	if (m_language != item)
	{
		// Dc1Factory::DeleteObject(m_language);
		m_language = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_language) m_language->SetParent(m_myself.getPointer());
		if(m_language != EbuCorecoreMetadataType_language_CollectionPtr())
		{
			m_language->SetContentName(XMLString::transcode("language"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setrelation(const EbuCorecoreMetadataType_relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setrelation().");
	}
	if (m_relation != item)
	{
		// Dc1Factory::DeleteObject(m_relation);
		m_relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relation) m_relation->SetParent(m_myself.getPointer());
		if(m_relation != EbuCorecoreMetadataType_relation_CollectionPtr())
		{
			m_relation->SetContentName(XMLString::transcode("relation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisRelatedTo(const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisRelatedTo().");
	}
	if (m_isRelatedTo != item)
	{
		// Dc1Factory::DeleteObject(m_isRelatedTo);
		m_isRelatedTo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isRelatedTo) m_isRelatedTo->SetParent(m_myself.getPointer());
		if(m_isRelatedTo != EbuCorecoreMetadataType_isRelatedTo_CollectionPtr())
		{
			m_isRelatedTo->SetContentName(XMLString::transcode("isRelatedTo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisNextInSequence(const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisNextInSequence().");
	}
	if (m_isNextInSequence != item)
	{
		// Dc1Factory::DeleteObject(m_isNextInSequence);
		m_isNextInSequence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isNextInSequence) m_isNextInSequence->SetParent(m_myself.getPointer());
		if(m_isNextInSequence != EbuCorecoreMetadataType_isNextInSequence_CollectionPtr())
		{
			m_isNextInSequence->SetContentName(XMLString::transcode("isNextInSequence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisVersionOf(const EbuCorecoreMetadataType_isVersionOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisVersionOf().");
	}
	if (m_isVersionOf != item)
	{
		// Dc1Factory::DeleteObject(m_isVersionOf);
		m_isVersionOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isVersionOf) m_isVersionOf->SetParent(m_myself.getPointer());
		if(m_isVersionOf != EbuCorecoreMetadataType_isVersionOf_CollectionPtr())
		{
			m_isVersionOf->SetContentName(XMLString::transcode("isVersionOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasVersion(const EbuCorecoreMetadataType_hasVersion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasVersion().");
	}
	if (m_hasVersion != item)
	{
		// Dc1Factory::DeleteObject(m_hasVersion);
		m_hasVersion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasVersion) m_hasVersion->SetParent(m_myself.getPointer());
		if(m_hasVersion != EbuCorecoreMetadataType_hasVersion_CollectionPtr())
		{
			m_hasVersion->SetContentName(XMLString::transcode("hasVersion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisReplacedBy(const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisReplacedBy().");
	}
	if (m_isReplacedBy != item)
	{
		// Dc1Factory::DeleteObject(m_isReplacedBy);
		m_isReplacedBy = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isReplacedBy) m_isReplacedBy->SetParent(m_myself.getPointer());
		if(m_isReplacedBy != EbuCorecoreMetadataType_isReplacedBy_CollectionPtr())
		{
			m_isReplacedBy->SetContentName(XMLString::transcode("isReplacedBy"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setreplaces(const EbuCorecoreMetadataType_replaces_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setreplaces().");
	}
	if (m_replaces != item)
	{
		// Dc1Factory::DeleteObject(m_replaces);
		m_replaces = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_replaces) m_replaces->SetParent(m_myself.getPointer());
		if(m_replaces != EbuCorecoreMetadataType_replaces_CollectionPtr())
		{
			m_replaces->SetContentName(XMLString::transcode("replaces"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisRequiredBy(const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisRequiredBy().");
	}
	if (m_isRequiredBy != item)
	{
		// Dc1Factory::DeleteObject(m_isRequiredBy);
		m_isRequiredBy = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isRequiredBy) m_isRequiredBy->SetParent(m_myself.getPointer());
		if(m_isRequiredBy != EbuCorecoreMetadataType_isRequiredBy_CollectionPtr())
		{
			m_isRequiredBy->SetContentName(XMLString::transcode("isRequiredBy"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setrequires(const EbuCorecoreMetadataType_requires_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setrequires().");
	}
	if (m_requires != item)
	{
		// Dc1Factory::DeleteObject(m_requires);
		m_requires = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_requires) m_requires->SetParent(m_myself.getPointer());
		if(m_requires != EbuCorecoreMetadataType_requires_CollectionPtr())
		{
			m_requires->SetContentName(XMLString::transcode("requires"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisPartOf(const EbuCorecoreMetadataType_isPartOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisPartOf().");
	}
	if (m_isPartOf != item)
	{
		// Dc1Factory::DeleteObject(m_isPartOf);
		m_isPartOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isPartOf) m_isPartOf->SetParent(m_myself.getPointer());
		if(m_isPartOf != EbuCorecoreMetadataType_isPartOf_CollectionPtr())
		{
			m_isPartOf->SetContentName(XMLString::transcode("isPartOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasPart(const EbuCorecoreMetadataType_hasPart_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasPart().");
	}
	if (m_hasPart != item)
	{
		// Dc1Factory::DeleteObject(m_hasPart);
		m_hasPart = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasPart) m_hasPart->SetParent(m_myself.getPointer());
		if(m_hasPart != EbuCorecoreMetadataType_hasPart_CollectionPtr())
		{
			m_hasPart->SetContentName(XMLString::transcode("hasPart"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasTrackPart(const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasTrackPart().");
	}
	if (m_hasTrackPart != item)
	{
		// Dc1Factory::DeleteObject(m_hasTrackPart);
		m_hasTrackPart = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasTrackPart) m_hasTrackPart->SetParent(m_myself.getPointer());
		if(m_hasTrackPart != EbuCorecoreMetadataType_hasTrackPart_CollectionPtr())
		{
			m_hasTrackPart->SetContentName(XMLString::transcode("hasTrackPart"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisReferencedBy(const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisReferencedBy().");
	}
	if (m_isReferencedBy != item)
	{
		// Dc1Factory::DeleteObject(m_isReferencedBy);
		m_isReferencedBy = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isReferencedBy) m_isReferencedBy->SetParent(m_myself.getPointer());
		if(m_isReferencedBy != EbuCorecoreMetadataType_isReferencedBy_CollectionPtr())
		{
			m_isReferencedBy->SetContentName(XMLString::transcode("isReferencedBy"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setreferences(const EbuCorecoreMetadataType_references_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setreferences().");
	}
	if (m_references != item)
	{
		// Dc1Factory::DeleteObject(m_references);
		m_references = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_references) m_references->SetParent(m_myself.getPointer());
		if(m_references != EbuCorecoreMetadataType_references_CollectionPtr())
		{
			m_references->SetContentName(XMLString::transcode("references"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisFormatOf(const EbuCorecoreMetadataType_isFormatOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisFormatOf().");
	}
	if (m_isFormatOf != item)
	{
		// Dc1Factory::DeleteObject(m_isFormatOf);
		m_isFormatOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isFormatOf) m_isFormatOf->SetParent(m_myself.getPointer());
		if(m_isFormatOf != EbuCorecoreMetadataType_isFormatOf_CollectionPtr())
		{
			m_isFormatOf->SetContentName(XMLString::transcode("isFormatOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasFormat(const EbuCorecoreMetadataType_hasFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasFormat().");
	}
	if (m_hasFormat != item)
	{
		// Dc1Factory::DeleteObject(m_hasFormat);
		m_hasFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasFormat) m_hasFormat->SetParent(m_myself.getPointer());
		if(m_hasFormat != EbuCorecoreMetadataType_hasFormat_CollectionPtr())
		{
			m_hasFormat->SetContentName(XMLString::transcode("hasFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisEpisodeOf(const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisEpisodeOf().");
	}
	if (m_isEpisodeOf != item)
	{
		// Dc1Factory::DeleteObject(m_isEpisodeOf);
		m_isEpisodeOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isEpisodeOf) m_isEpisodeOf->SetParent(m_myself.getPointer());
		if(m_isEpisodeOf != EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr())
		{
			m_isEpisodeOf->SetContentName(XMLString::transcode("isEpisodeOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisSeasonOf(const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisSeasonOf().");
	}
	if (m_isSeasonOf != item)
	{
		// Dc1Factory::DeleteObject(m_isSeasonOf);
		m_isSeasonOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isSeasonOf) m_isSeasonOf->SetParent(m_myself.getPointer());
		if(m_isSeasonOf != EbuCorecoreMetadataType_isSeasonOf_CollectionPtr())
		{
			m_isSeasonOf->SetContentName(XMLString::transcode("isSeasonOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasEpisode(const EbuCorecoreMetadataType_hasEpisode_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasEpisode().");
	}
	if (m_hasEpisode != item)
	{
		// Dc1Factory::DeleteObject(m_hasEpisode);
		m_hasEpisode = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasEpisode) m_hasEpisode->SetParent(m_myself.getPointer());
		if(m_hasEpisode != EbuCorecoreMetadataType_hasEpisode_CollectionPtr())
		{
			m_hasEpisode->SetContentName(XMLString::transcode("hasEpisode"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasSeason(const EbuCorecoreMetadataType_hasSeason_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasSeason().");
	}
	if (m_hasSeason != item)
	{
		// Dc1Factory::DeleteObject(m_hasSeason);
		m_hasSeason = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasSeason) m_hasSeason->SetParent(m_myself.getPointer());
		if(m_hasSeason != EbuCorecoreMetadataType_hasSeason_CollectionPtr())
		{
			m_hasSeason->SetContentName(XMLString::transcode("hasSeason"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetisMemberOf(const EbuCorecoreMetadataType_isMemberOf_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetisMemberOf().");
	}
	if (m_isMemberOf != item)
	{
		// Dc1Factory::DeleteObject(m_isMemberOf);
		m_isMemberOf = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_isMemberOf) m_isMemberOf->SetParent(m_myself.getPointer());
		if(m_isMemberOf != EbuCorecoreMetadataType_isMemberOf_CollectionPtr())
		{
			m_isMemberOf->SetContentName(XMLString::transcode("isMemberOf"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SethasMember(const EbuCorecoreMetadataType_hasMember_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SethasMember().");
	}
	if (m_hasMember != item)
	{
		// Dc1Factory::DeleteObject(m_hasMember);
		m_hasMember = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hasMember) m_hasMember->SetParent(m_myself.getPointer());
		if(m_hasMember != EbuCorecoreMetadataType_hasMember_CollectionPtr())
		{
			m_hasMember->SetContentName(XMLString::transcode("hasMember"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetsameAs(const EbuCorecoreMetadataType_sameAs_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetsameAs().");
	}
	if (m_sameAs != item)
	{
		// Dc1Factory::DeleteObject(m_sameAs);
		m_sameAs = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_sameAs) m_sameAs->SetParent(m_myself.getPointer());
		if(m_sameAs != EbuCorecoreMetadataType_sameAs_CollectionPtr())
		{
			m_sameAs->SetContentName(XMLString::transcode("sameAs"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setcoverage(const EbuCorecoreMetadataType_coverage_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setcoverage().");
	}
	if (m_coverage != item)
	{
		// Dc1Factory::DeleteObject(m_coverage);
		m_coverage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_coverage) m_coverage->SetParent(m_myself.getPointer());
		if(m_coverage != EbuCorecoreMetadataType_coverage_CollectionPtr())
		{
			m_coverage->SetContentName(XMLString::transcode("coverage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setrights(const EbuCorecoreMetadataType_rights_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setrights().");
	}
	if (m_rights != item)
	{
		// Dc1Factory::DeleteObject(m_rights);
		m_rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rights) m_rights->SetParent(m_myself.getPointer());
		if(m_rights != EbuCorecoreMetadataType_rights_CollectionPtr())
		{
			m_rights->SetContentName(XMLString::transcode("rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setversion(const EbuCorecoreMetadataType_version_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setversion().");
	}
	if (m_version != item)
	{
		// Dc1Factory::DeleteObject(m_version);
		m_version = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_version) m_version->SetParent(m_myself.getPointer());
		if(m_version != EbuCorecoreMetadataType_version_CollectionPtr())
		{
			m_version->SetContentName(XMLString::transcode("version"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::SetpublicationHistory(const EbuCorecoreMetadataType_publicationHistory_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::SetpublicationHistory().");
	}
	if (m_publicationHistory != item)
	{
		// Dc1Factory::DeleteObject(m_publicationHistory);
		m_publicationHistory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationHistory) m_publicationHistory->SetParent(m_myself.getPointer());
		if(m_publicationHistory != EbuCorecoreMetadataType_publicationHistory_CollectionPtr())
		{
			m_publicationHistory->SetContentName(XMLString::transcode("publicationHistory"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setplanning(const EbuCorecoreMetadataType_planning_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setplanning().");
	}
	if (m_planning != item)
	{
		// Dc1Factory::DeleteObject(m_planning);
		m_planning = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_planning) m_planning->SetParent(m_myself.getPointer());
		if(m_planning != EbuCorecoreMetadataType_planning_CollectionPtr())
		{
			m_planning->SetContentName(XMLString::transcode("planning"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setrating(const EbuCorecoreMetadataType_rating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setrating().");
	}
	if (m_rating != item)
	{
		// Dc1Factory::DeleteObject(m_rating);
		m_rating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rating) m_rating->SetParent(m_myself.getPointer());
		if(m_rating != EbuCorecoreMetadataType_rating_CollectionPtr())
		{
			m_rating->SetContentName(XMLString::transcode("rating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecoreMetadataType::Setpart(const EbuCorecoreMetadataType_part_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecoreMetadataType::Setpart().");
	}
	if (m_part != item)
	{
		// Dc1Factory::DeleteObject(m_part);
		m_part = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_part) m_part->SetParent(m_myself.getPointer());
		if(m_part != EbuCorecoreMetadataType_part_CollectionPtr())
		{
			m_part->SetContentName(XMLString::transcode("part"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorecoreMetadataType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("title")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:title is item of type titleType
		// in element collection coreMetadataType_title_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_title_CollectionPtr coll = Gettitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_title_CollectionType; // FTT, check this
				Settitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:titleType")))) != empty)
			{
				// Is type allowed
				EbuCoretitlePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("alternativeTitle")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:alternativeTitle is item of type alternativeTitleType
		// in element collection coreMetadataType_alternativeTitle_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_alternativeTitle_CollectionPtr coll = GetalternativeTitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_alternativeTitle_CollectionType; // FTT, check this
				SetalternativeTitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType")))) != empty)
			{
				// Is type allowed
				EbuCorealternativeTitlePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("creator")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:creator is item of type entityType
		// in element collection coreMetadataType_creator_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_creator_CollectionPtr coll = Getcreator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_creator_CollectionType; // FTT, check this
				Setcreator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("subject")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:subject is item of type subjectType
		// in element collection coreMetadataType_subject_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_subject_CollectionPtr coll = Getsubject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_subject_CollectionType; // FTT, check this
				Setsubject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subjectType")))) != empty)
			{
				// Is type allowed
				EbuCoresubjectPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("description")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:description is item of type descriptionType
		// in element collection coreMetadataType_description_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_description_CollectionPtr coll = Getdescription();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_description_CollectionType; // FTT, check this
				Setdescription(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:descriptionType")))) != empty)
			{
				// Is type allowed
				EbuCoredescriptionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publisher")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:publisher is item of type entityType
		// in element collection coreMetadataType_publisher_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_publisher_CollectionPtr coll = Getpublisher();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_publisher_CollectionType; // FTT, check this
				Setpublisher(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("contributor")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:contributor is item of type entityType
		// in element collection coreMetadataType_contributor_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_contributor_CollectionPtr coll = Getcontributor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_contributor_CollectionType; // FTT, check this
				Setcontributor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("date")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:date is item of type dateType
		// in element collection coreMetadataType_date_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_date_CollectionPtr coll = Getdate();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_date_CollectionType; // FTT, check this
				Setdate(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType")))) != empty)
			{
				// Is type allowed
				EbuCoredatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:type is item of type typeType
		// in element collection coreMetadataType_type_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_type_CollectionPtr coll = Gettype();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_type_CollectionType; // FTT, check this
				Settype(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType")))) != empty)
			{
				// Is type allowed
				EbuCoretypePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("format")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:format is item of type formatType
		// in element collection coreMetadataType_format_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_format_CollectionPtr coll = Getformat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_format_CollectionType; // FTT, check this
				Setformat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType")))) != empty)
			{
				// Is type allowed
				EbuCoreformatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("identifier")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:identifier is item of type identifierType
		// in element collection coreMetadataType_identifier_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_identifier_CollectionPtr coll = Getidentifier();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_identifier_CollectionType; // FTT, check this
				Setidentifier(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))) != empty)
			{
				// Is type allowed
				EbuCoreidentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("source")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:source is item of type elementType
		// in element collection coreMetadataType_source_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_source_CollectionPtr coll = Getsource();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_source_CollectionType; // FTT, check this
				Setsource(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("language")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:language is item of type languageType
		// in element collection coreMetadataType_language_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_language_CollectionPtr coll = Getlanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_language_CollectionType; // FTT, check this
				Setlanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:languageType")))) != empty)
			{
				// Is type allowed
				EbuCorelanguagePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relation")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:relation is item of type relationType
		// in element collection coreMetadataType_relation_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_relation_CollectionPtr coll = Getrelation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_relation_CollectionType; // FTT, check this
				Setrelation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isRelatedTo")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isRelatedTo is item of type relationType
		// in element collection coreMetadataType_isRelatedTo_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isRelatedTo_CollectionPtr coll = GetisRelatedTo();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isRelatedTo_CollectionType; // FTT, check this
				SetisRelatedTo(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isNextInSequence")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isNextInSequence is item of type relationType
		// in element collection coreMetadataType_isNextInSequence_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isNextInSequence_CollectionPtr coll = GetisNextInSequence();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isNextInSequence_CollectionType; // FTT, check this
				SetisNextInSequence(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isVersionOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isVersionOf is item of type relationType
		// in element collection coreMetadataType_isVersionOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isVersionOf_CollectionPtr coll = GetisVersionOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isVersionOf_CollectionType; // FTT, check this
				SetisVersionOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasVersion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasVersion is item of type relationType
		// in element collection coreMetadataType_hasVersion_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasVersion_CollectionPtr coll = GethasVersion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasVersion_CollectionType; // FTT, check this
				SethasVersion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isReplacedBy")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isReplacedBy is item of type relationType
		// in element collection coreMetadataType_isReplacedBy_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isReplacedBy_CollectionPtr coll = GetisReplacedBy();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isReplacedBy_CollectionType; // FTT, check this
				SetisReplacedBy(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("replaces")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:replaces is item of type relationType
		// in element collection coreMetadataType_replaces_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_replaces_CollectionPtr coll = Getreplaces();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_replaces_CollectionType; // FTT, check this
				Setreplaces(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isRequiredBy")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isRequiredBy is item of type relationType
		// in element collection coreMetadataType_isRequiredBy_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isRequiredBy_CollectionPtr coll = GetisRequiredBy();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isRequiredBy_CollectionType; // FTT, check this
				SetisRequiredBy(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("requires")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:requires is item of type relationType
		// in element collection coreMetadataType_requires_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_requires_CollectionPtr coll = Getrequires();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_requires_CollectionType; // FTT, check this
				Setrequires(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isPartOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isPartOf is item of type relationType
		// in element collection coreMetadataType_isPartOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isPartOf_CollectionPtr coll = GetisPartOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isPartOf_CollectionType; // FTT, check this
				SetisPartOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasPart")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasPart is item of type relationType
		// in element collection coreMetadataType_hasPart_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasPart_CollectionPtr coll = GethasPart();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasPart_CollectionType; // FTT, check this
				SethasPart(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasTrackPart")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasTrackPart is item of type coreMetadataType_hasTrackPart_LocalType
		// in element collection coreMetadataType_hasTrackPart_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasTrackPart_CollectionPtr coll = GethasTrackPart();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasTrackPart_CollectionType; // FTT, check this
				SethasTrackPart(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasTrackPart_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorecoreMetadataType_hasTrackPart_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isReferencedBy")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isReferencedBy is item of type relationType
		// in element collection coreMetadataType_isReferencedBy_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isReferencedBy_CollectionPtr coll = GetisReferencedBy();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isReferencedBy_CollectionType; // FTT, check this
				SetisReferencedBy(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("references")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:references is item of type relationType
		// in element collection coreMetadataType_references_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_references_CollectionPtr coll = Getreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_references_CollectionType; // FTT, check this
				Setreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isFormatOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isFormatOf is item of type relationType
		// in element collection coreMetadataType_isFormatOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isFormatOf_CollectionPtr coll = GetisFormatOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isFormatOf_CollectionType; // FTT, check this
				SetisFormatOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasFormat is item of type relationType
		// in element collection coreMetadataType_hasFormat_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasFormat_CollectionPtr coll = GethasFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasFormat_CollectionType; // FTT, check this
				SethasFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isEpisodeOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isEpisodeOf is item of type relationType
		// in element collection coreMetadataType_isEpisodeOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr coll = GetisEpisodeOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isEpisodeOf_CollectionType; // FTT, check this
				SetisEpisodeOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isSeasonOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isSeasonOf is item of type relationType
		// in element collection coreMetadataType_isSeasonOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isSeasonOf_CollectionPtr coll = GetisSeasonOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isSeasonOf_CollectionType; // FTT, check this
				SetisSeasonOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasEpisode")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasEpisode is item of type relationType
		// in element collection coreMetadataType_hasEpisode_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasEpisode_CollectionPtr coll = GethasEpisode();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasEpisode_CollectionType; // FTT, check this
				SethasEpisode(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasSeason")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasSeason is item of type relationType
		// in element collection coreMetadataType_hasSeason_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasSeason_CollectionPtr coll = GethasSeason();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasSeason_CollectionType; // FTT, check this
				SethasSeason(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("isMemberOf")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:isMemberOf is item of type relationType
		// in element collection coreMetadataType_isMemberOf_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_isMemberOf_CollectionPtr coll = GetisMemberOf();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_isMemberOf_CollectionType; // FTT, check this
				SetisMemberOf(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hasMember")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:hasMember is item of type relationType
		// in element collection coreMetadataType_hasMember_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_hasMember_CollectionPtr coll = GethasMember();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_hasMember_CollectionType; // FTT, check this
				SethasMember(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("sameAs")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:sameAs is item of type relationType
		// in element collection coreMetadataType_sameAs_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_sameAs_CollectionPtr coll = GetsameAs();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_sameAs_CollectionType; // FTT, check this
				SetsameAs(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))) != empty)
			{
				// Is type allowed
				EbuCorerelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("coverage")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:coverage is item of type coverageType
		// in element collection coreMetadataType_coverage_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_coverage_CollectionPtr coll = Getcoverage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_coverage_CollectionType; // FTT, check this
				Setcoverage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coverageType")))) != empty)
			{
				// Is type allowed
				EbuCorecoveragePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rights")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:rights is item of type rightsType
		// in element collection coreMetadataType_rights_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_rights_CollectionPtr coll = Getrights();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_rights_CollectionType; // FTT, check this
				Setrights(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType")))) != empty)
			{
				// Is type allowed
				EbuCorerightsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("version")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:version is item of type versionType
		// in element collection coreMetadataType_version_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_version_CollectionPtr coll = Getversion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_version_CollectionType; // FTT, check this
				Setversion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:versionType")))) != empty)
			{
				// Is type allowed
				EbuCoreversionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publicationHistory")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:publicationHistory is item of type publicationHistoryType
		// in element collection coreMetadataType_publicationHistory_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_publicationHistory_CollectionPtr coll = GetpublicationHistory();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_publicationHistory_CollectionType; // FTT, check this
				SetpublicationHistory(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationHistoryType")))) != empty)
			{
				// Is type allowed
				EbuCorepublicationHistoryPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("planning")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:planning is item of type planningType
		// in element collection coreMetadataType_planning_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_planning_CollectionPtr coll = Getplanning();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_planning_CollectionType; // FTT, check this
				Setplanning(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:planningType")))) != empty)
			{
				// Is type allowed
				EbuCoreplanningPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rating")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:rating is item of type ratingType
		// in element collection coreMetadataType_rating_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_rating_CollectionPtr coll = Getrating();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_rating_CollectionType; // FTT, check this
				Setrating(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType")))) != empty)
			{
				// Is type allowed
				EbuCoreratingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("part")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:part is item of type partType
		// in element collection coreMetadataType_part_CollectionType
		
		context->Found = true;
		EbuCorecoreMetadataType_part_CollectionPtr coll = Getpart();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatecoreMetadataType_part_CollectionType; // FTT, check this
				Setpart(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:partType")))) != empty)
			{
				// Is type allowed
				EbuCorepartPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for coreMetadataType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "coreMetadataType");
	}
	return result;
}

XMLCh * EbuCorecoreMetadataType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorecoreMetadataType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorecoreMetadataType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("coreMetadataType"));
	// Element serialization:
	if (m_title != EbuCorecoreMetadataType_title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_title->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_alternativeTitle != EbuCorecoreMetadataType_alternativeTitle_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_alternativeTitle->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_creator != EbuCorecoreMetadataType_creator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_creator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_subject != EbuCorecoreMetadataType_subject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_subject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_description != EbuCorecoreMetadataType_description_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_description->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_publisher != EbuCorecoreMetadataType_publisher_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_publisher->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_contributor != EbuCorecoreMetadataType_contributor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_contributor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_date != EbuCorecoreMetadataType_date_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_date->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_type != EbuCorecoreMetadataType_type_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_type->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_format != EbuCorecoreMetadataType_format_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_format->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_identifier != EbuCorecoreMetadataType_identifier_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_identifier->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_source != EbuCorecoreMetadataType_source_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_source->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_language != EbuCorecoreMetadataType_language_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_language->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_relation != EbuCorecoreMetadataType_relation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_relation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isRelatedTo != EbuCorecoreMetadataType_isRelatedTo_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isRelatedTo->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isNextInSequence != EbuCorecoreMetadataType_isNextInSequence_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isNextInSequence->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isVersionOf != EbuCorecoreMetadataType_isVersionOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isVersionOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasVersion != EbuCorecoreMetadataType_hasVersion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasVersion->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isReplacedBy != EbuCorecoreMetadataType_isReplacedBy_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isReplacedBy->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_replaces != EbuCorecoreMetadataType_replaces_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_replaces->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isRequiredBy != EbuCorecoreMetadataType_isRequiredBy_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isRequiredBy->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_requires != EbuCorecoreMetadataType_requires_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_requires->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isPartOf != EbuCorecoreMetadataType_isPartOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isPartOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasPart != EbuCorecoreMetadataType_hasPart_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasPart->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasTrackPart != EbuCorecoreMetadataType_hasTrackPart_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasTrackPart->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isReferencedBy != EbuCorecoreMetadataType_isReferencedBy_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isReferencedBy->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_references != EbuCorecoreMetadataType_references_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_references->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isFormatOf != EbuCorecoreMetadataType_isFormatOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isFormatOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasFormat != EbuCorecoreMetadataType_hasFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isEpisodeOf != EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isEpisodeOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isSeasonOf != EbuCorecoreMetadataType_isSeasonOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isSeasonOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasEpisode != EbuCorecoreMetadataType_hasEpisode_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasEpisode->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasSeason != EbuCorecoreMetadataType_hasSeason_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasSeason->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_isMemberOf != EbuCorecoreMetadataType_isMemberOf_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_isMemberOf->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_hasMember != EbuCorecoreMetadataType_hasMember_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_hasMember->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_sameAs != EbuCorecoreMetadataType_sameAs_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_sameAs->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_coverage != EbuCorecoreMetadataType_coverage_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_coverage->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_rights != EbuCorecoreMetadataType_rights_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_rights->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_version != EbuCorecoreMetadataType_version_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_version->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_publicationHistory != EbuCorecoreMetadataType_publicationHistory_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_publicationHistory->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_planning != EbuCorecoreMetadataType_planning_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_planning->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_rating != EbuCorecoreMetadataType_rating_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_rating->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_part != EbuCorecoreMetadataType_part_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_part->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorecoreMetadataType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("title"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("title")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_title_CollectionPtr tmp = CreatecoreMetadataType_title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("alternativeTitle"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("alternativeTitle")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_alternativeTitle_CollectionPtr tmp = CreatecoreMetadataType_alternativeTitle_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetalternativeTitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("creator"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("creator")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_creator_CollectionPtr tmp = CreatecoreMetadataType_creator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcreator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("subject"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("subject")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_subject_CollectionPtr tmp = CreatecoreMetadataType_subject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setsubject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("description"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("description")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_description_CollectionPtr tmp = CreatecoreMetadataType_description_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdescription(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("publisher"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("publisher")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_publisher_CollectionPtr tmp = CreatecoreMetadataType_publisher_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setpublisher(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("contributor"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("contributor")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_contributor_CollectionPtr tmp = CreatecoreMetadataType_contributor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcontributor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("date"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("date")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_date_CollectionPtr tmp = CreatecoreMetadataType_date_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdate(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("type"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("type")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_type_CollectionPtr tmp = CreatecoreMetadataType_type_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settype(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("format"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("format")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_format_CollectionPtr tmp = CreatecoreMetadataType_format_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setformat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("identifier"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("identifier")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_identifier_CollectionPtr tmp = CreatecoreMetadataType_identifier_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setidentifier(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("source"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("source")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_source_CollectionPtr tmp = CreatecoreMetadataType_source_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setsource(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("language"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("language")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_language_CollectionPtr tmp = CreatecoreMetadataType_language_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setlanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("relation"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("relation")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_relation_CollectionPtr tmp = CreatecoreMetadataType_relation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrelation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isRelatedTo"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isRelatedTo")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isRelatedTo_CollectionPtr tmp = CreatecoreMetadataType_isRelatedTo_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisRelatedTo(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isNextInSequence"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isNextInSequence")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isNextInSequence_CollectionPtr tmp = CreatecoreMetadataType_isNextInSequence_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisNextInSequence(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isVersionOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isVersionOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isVersionOf_CollectionPtr tmp = CreatecoreMetadataType_isVersionOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisVersionOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasVersion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasVersion")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasVersion_CollectionPtr tmp = CreatecoreMetadataType_hasVersion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasVersion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isReplacedBy"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isReplacedBy")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isReplacedBy_CollectionPtr tmp = CreatecoreMetadataType_isReplacedBy_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisReplacedBy(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("replaces"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("replaces")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_replaces_CollectionPtr tmp = CreatecoreMetadataType_replaces_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setreplaces(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isRequiredBy"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isRequiredBy")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isRequiredBy_CollectionPtr tmp = CreatecoreMetadataType_isRequiredBy_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisRequiredBy(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("requires"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("requires")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_requires_CollectionPtr tmp = CreatecoreMetadataType_requires_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrequires(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isPartOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isPartOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isPartOf_CollectionPtr tmp = CreatecoreMetadataType_isPartOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisPartOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasPart"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasPart")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasPart_CollectionPtr tmp = CreatecoreMetadataType_hasPart_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasPart(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasTrackPart"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasTrackPart")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasTrackPart_CollectionPtr tmp = CreatecoreMetadataType_hasTrackPart_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasTrackPart(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isReferencedBy"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isReferencedBy")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isReferencedBy_CollectionPtr tmp = CreatecoreMetadataType_isReferencedBy_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisReferencedBy(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("references"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("references")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_references_CollectionPtr tmp = CreatecoreMetadataType_references_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isFormatOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isFormatOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isFormatOf_CollectionPtr tmp = CreatecoreMetadataType_isFormatOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisFormatOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasFormat")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasFormat_CollectionPtr tmp = CreatecoreMetadataType_hasFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isEpisodeOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isEpisodeOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr tmp = CreatecoreMetadataType_isEpisodeOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisEpisodeOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isSeasonOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isSeasonOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isSeasonOf_CollectionPtr tmp = CreatecoreMetadataType_isSeasonOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisSeasonOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasEpisode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasEpisode")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasEpisode_CollectionPtr tmp = CreatecoreMetadataType_hasEpisode_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasEpisode(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasSeason"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasSeason")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasSeason_CollectionPtr tmp = CreatecoreMetadataType_hasSeason_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasSeason(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("isMemberOf"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("isMemberOf")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_isMemberOf_CollectionPtr tmp = CreatecoreMetadataType_isMemberOf_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetisMemberOf(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("hasMember"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("hasMember")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_hasMember_CollectionPtr tmp = CreatecoreMetadataType_hasMember_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SethasMember(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("sameAs"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("sameAs")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_sameAs_CollectionPtr tmp = CreatecoreMetadataType_sameAs_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetsameAs(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("coverage"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("coverage")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_coverage_CollectionPtr tmp = CreatecoreMetadataType_coverage_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcoverage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("rights"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("rights")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_rights_CollectionPtr tmp = CreatecoreMetadataType_rights_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrights(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("version"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("version")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_version_CollectionPtr tmp = CreatecoreMetadataType_version_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setversion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("publicationHistory"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("publicationHistory")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_publicationHistory_CollectionPtr tmp = CreatecoreMetadataType_publicationHistory_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpublicationHistory(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("planning"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("planning")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_planning_CollectionPtr tmp = CreatecoreMetadataType_planning_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setplanning(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("rating"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("rating")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_rating_CollectionPtr tmp = CreatecoreMetadataType_rating_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrating(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("part"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("part")) == 0))
		{
			// Deserialize factory type
			EbuCorecoreMetadataType_part_CollectionPtr tmp = CreatecoreMetadataType_part_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setpart(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecoreMetadataType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("title")) == 0))
  {
	EbuCorecoreMetadataType_title_CollectionPtr tmp = CreatecoreMetadataType_title_CollectionType; // FTT, check this
	this->Settitle(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("alternativeTitle")) == 0))
  {
	EbuCorecoreMetadataType_alternativeTitle_CollectionPtr tmp = CreatecoreMetadataType_alternativeTitle_CollectionType; // FTT, check this
	this->SetalternativeTitle(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("creator")) == 0))
  {
	EbuCorecoreMetadataType_creator_CollectionPtr tmp = CreatecoreMetadataType_creator_CollectionType; // FTT, check this
	this->Setcreator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("subject")) == 0))
  {
	EbuCorecoreMetadataType_subject_CollectionPtr tmp = CreatecoreMetadataType_subject_CollectionType; // FTT, check this
	this->Setsubject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("description")) == 0))
  {
	EbuCorecoreMetadataType_description_CollectionPtr tmp = CreatecoreMetadataType_description_CollectionType; // FTT, check this
	this->Setdescription(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("publisher")) == 0))
  {
	EbuCorecoreMetadataType_publisher_CollectionPtr tmp = CreatecoreMetadataType_publisher_CollectionType; // FTT, check this
	this->Setpublisher(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("contributor")) == 0))
  {
	EbuCorecoreMetadataType_contributor_CollectionPtr tmp = CreatecoreMetadataType_contributor_CollectionType; // FTT, check this
	this->Setcontributor(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("date")) == 0))
  {
	EbuCorecoreMetadataType_date_CollectionPtr tmp = CreatecoreMetadataType_date_CollectionType; // FTT, check this
	this->Setdate(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("type")) == 0))
  {
	EbuCorecoreMetadataType_type_CollectionPtr tmp = CreatecoreMetadataType_type_CollectionType; // FTT, check this
	this->Settype(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("format")) == 0))
  {
	EbuCorecoreMetadataType_format_CollectionPtr tmp = CreatecoreMetadataType_format_CollectionType; // FTT, check this
	this->Setformat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("identifier")) == 0))
  {
	EbuCorecoreMetadataType_identifier_CollectionPtr tmp = CreatecoreMetadataType_identifier_CollectionType; // FTT, check this
	this->Setidentifier(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("source")) == 0))
  {
	EbuCorecoreMetadataType_source_CollectionPtr tmp = CreatecoreMetadataType_source_CollectionType; // FTT, check this
	this->Setsource(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("language")) == 0))
  {
	EbuCorecoreMetadataType_language_CollectionPtr tmp = CreatecoreMetadataType_language_CollectionType; // FTT, check this
	this->Setlanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("relation")) == 0))
  {
	EbuCorecoreMetadataType_relation_CollectionPtr tmp = CreatecoreMetadataType_relation_CollectionType; // FTT, check this
	this->Setrelation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isRelatedTo")) == 0))
  {
	EbuCorecoreMetadataType_isRelatedTo_CollectionPtr tmp = CreatecoreMetadataType_isRelatedTo_CollectionType; // FTT, check this
	this->SetisRelatedTo(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isNextInSequence")) == 0))
  {
	EbuCorecoreMetadataType_isNextInSequence_CollectionPtr tmp = CreatecoreMetadataType_isNextInSequence_CollectionType; // FTT, check this
	this->SetisNextInSequence(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isVersionOf")) == 0))
  {
	EbuCorecoreMetadataType_isVersionOf_CollectionPtr tmp = CreatecoreMetadataType_isVersionOf_CollectionType; // FTT, check this
	this->SetisVersionOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasVersion")) == 0))
  {
	EbuCorecoreMetadataType_hasVersion_CollectionPtr tmp = CreatecoreMetadataType_hasVersion_CollectionType; // FTT, check this
	this->SethasVersion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isReplacedBy")) == 0))
  {
	EbuCorecoreMetadataType_isReplacedBy_CollectionPtr tmp = CreatecoreMetadataType_isReplacedBy_CollectionType; // FTT, check this
	this->SetisReplacedBy(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("replaces")) == 0))
  {
	EbuCorecoreMetadataType_replaces_CollectionPtr tmp = CreatecoreMetadataType_replaces_CollectionType; // FTT, check this
	this->Setreplaces(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isRequiredBy")) == 0))
  {
	EbuCorecoreMetadataType_isRequiredBy_CollectionPtr tmp = CreatecoreMetadataType_isRequiredBy_CollectionType; // FTT, check this
	this->SetisRequiredBy(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("requires")) == 0))
  {
	EbuCorecoreMetadataType_requires_CollectionPtr tmp = CreatecoreMetadataType_requires_CollectionType; // FTT, check this
	this->Setrequires(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isPartOf")) == 0))
  {
	EbuCorecoreMetadataType_isPartOf_CollectionPtr tmp = CreatecoreMetadataType_isPartOf_CollectionType; // FTT, check this
	this->SetisPartOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasPart")) == 0))
  {
	EbuCorecoreMetadataType_hasPart_CollectionPtr tmp = CreatecoreMetadataType_hasPart_CollectionType; // FTT, check this
	this->SethasPart(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasTrackPart")) == 0))
  {
	EbuCorecoreMetadataType_hasTrackPart_CollectionPtr tmp = CreatecoreMetadataType_hasTrackPart_CollectionType; // FTT, check this
	this->SethasTrackPart(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isReferencedBy")) == 0))
  {
	EbuCorecoreMetadataType_isReferencedBy_CollectionPtr tmp = CreatecoreMetadataType_isReferencedBy_CollectionType; // FTT, check this
	this->SetisReferencedBy(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("references")) == 0))
  {
	EbuCorecoreMetadataType_references_CollectionPtr tmp = CreatecoreMetadataType_references_CollectionType; // FTT, check this
	this->Setreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isFormatOf")) == 0))
  {
	EbuCorecoreMetadataType_isFormatOf_CollectionPtr tmp = CreatecoreMetadataType_isFormatOf_CollectionType; // FTT, check this
	this->SetisFormatOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasFormat")) == 0))
  {
	EbuCorecoreMetadataType_hasFormat_CollectionPtr tmp = CreatecoreMetadataType_hasFormat_CollectionType; // FTT, check this
	this->SethasFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isEpisodeOf")) == 0))
  {
	EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr tmp = CreatecoreMetadataType_isEpisodeOf_CollectionType; // FTT, check this
	this->SetisEpisodeOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isSeasonOf")) == 0))
  {
	EbuCorecoreMetadataType_isSeasonOf_CollectionPtr tmp = CreatecoreMetadataType_isSeasonOf_CollectionType; // FTT, check this
	this->SetisSeasonOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasEpisode")) == 0))
  {
	EbuCorecoreMetadataType_hasEpisode_CollectionPtr tmp = CreatecoreMetadataType_hasEpisode_CollectionType; // FTT, check this
	this->SethasEpisode(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasSeason")) == 0))
  {
	EbuCorecoreMetadataType_hasSeason_CollectionPtr tmp = CreatecoreMetadataType_hasSeason_CollectionType; // FTT, check this
	this->SethasSeason(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("isMemberOf")) == 0))
  {
	EbuCorecoreMetadataType_isMemberOf_CollectionPtr tmp = CreatecoreMetadataType_isMemberOf_CollectionType; // FTT, check this
	this->SetisMemberOf(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("hasMember")) == 0))
  {
	EbuCorecoreMetadataType_hasMember_CollectionPtr tmp = CreatecoreMetadataType_hasMember_CollectionType; // FTT, check this
	this->SethasMember(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("sameAs")) == 0))
  {
	EbuCorecoreMetadataType_sameAs_CollectionPtr tmp = CreatecoreMetadataType_sameAs_CollectionType; // FTT, check this
	this->SetsameAs(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("coverage")) == 0))
  {
	EbuCorecoreMetadataType_coverage_CollectionPtr tmp = CreatecoreMetadataType_coverage_CollectionType; // FTT, check this
	this->Setcoverage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("rights")) == 0))
  {
	EbuCorecoreMetadataType_rights_CollectionPtr tmp = CreatecoreMetadataType_rights_CollectionType; // FTT, check this
	this->Setrights(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("version")) == 0))
  {
	EbuCorecoreMetadataType_version_CollectionPtr tmp = CreatecoreMetadataType_version_CollectionType; // FTT, check this
	this->Setversion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("publicationHistory")) == 0))
  {
	EbuCorecoreMetadataType_publicationHistory_CollectionPtr tmp = CreatecoreMetadataType_publicationHistory_CollectionType; // FTT, check this
	this->SetpublicationHistory(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("planning")) == 0))
  {
	EbuCorecoreMetadataType_planning_CollectionPtr tmp = CreatecoreMetadataType_planning_CollectionType; // FTT, check this
	this->Setplanning(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("rating")) == 0))
  {
	EbuCorecoreMetadataType_rating_CollectionPtr tmp = CreatecoreMetadataType_rating_CollectionType; // FTT, check this
	this->Setrating(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("part")) == 0))
  {
	EbuCorecoreMetadataType_part_CollectionPtr tmp = CreatecoreMetadataType_part_CollectionType; // FTT, check this
	this->Setpart(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorecoreMetadataType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettitle() != Dc1NodePtr())
		result.Insert(Gettitle());
	if (GetalternativeTitle() != Dc1NodePtr())
		result.Insert(GetalternativeTitle());
	if (Getcreator() != Dc1NodePtr())
		result.Insert(Getcreator());
	if (Getsubject() != Dc1NodePtr())
		result.Insert(Getsubject());
	if (Getdescription() != Dc1NodePtr())
		result.Insert(Getdescription());
	if (Getpublisher() != Dc1NodePtr())
		result.Insert(Getpublisher());
	if (Getcontributor() != Dc1NodePtr())
		result.Insert(Getcontributor());
	if (Getdate() != Dc1NodePtr())
		result.Insert(Getdate());
	if (Gettype() != Dc1NodePtr())
		result.Insert(Gettype());
	if (Getformat() != Dc1NodePtr())
		result.Insert(Getformat());
	if (Getidentifier() != Dc1NodePtr())
		result.Insert(Getidentifier());
	if (Getsource() != Dc1NodePtr())
		result.Insert(Getsource());
	if (Getlanguage() != Dc1NodePtr())
		result.Insert(Getlanguage());
	if (Getrelation() != Dc1NodePtr())
		result.Insert(Getrelation());
	if (GetisRelatedTo() != Dc1NodePtr())
		result.Insert(GetisRelatedTo());
	if (GetisNextInSequence() != Dc1NodePtr())
		result.Insert(GetisNextInSequence());
	if (GetisVersionOf() != Dc1NodePtr())
		result.Insert(GetisVersionOf());
	if (GethasVersion() != Dc1NodePtr())
		result.Insert(GethasVersion());
	if (GetisReplacedBy() != Dc1NodePtr())
		result.Insert(GetisReplacedBy());
	if (Getreplaces() != Dc1NodePtr())
		result.Insert(Getreplaces());
	if (GetisRequiredBy() != Dc1NodePtr())
		result.Insert(GetisRequiredBy());
	if (Getrequires() != Dc1NodePtr())
		result.Insert(Getrequires());
	if (GetisPartOf() != Dc1NodePtr())
		result.Insert(GetisPartOf());
	if (GethasPart() != Dc1NodePtr())
		result.Insert(GethasPart());
	if (GethasTrackPart() != Dc1NodePtr())
		result.Insert(GethasTrackPart());
	if (GetisReferencedBy() != Dc1NodePtr())
		result.Insert(GetisReferencedBy());
	if (Getreferences() != Dc1NodePtr())
		result.Insert(Getreferences());
	if (GetisFormatOf() != Dc1NodePtr())
		result.Insert(GetisFormatOf());
	if (GethasFormat() != Dc1NodePtr())
		result.Insert(GethasFormat());
	if (GetisEpisodeOf() != Dc1NodePtr())
		result.Insert(GetisEpisodeOf());
	if (GetisSeasonOf() != Dc1NodePtr())
		result.Insert(GetisSeasonOf());
	if (GethasEpisode() != Dc1NodePtr())
		result.Insert(GethasEpisode());
	if (GethasSeason() != Dc1NodePtr())
		result.Insert(GethasSeason());
	if (GetisMemberOf() != Dc1NodePtr())
		result.Insert(GetisMemberOf());
	if (GethasMember() != Dc1NodePtr())
		result.Insert(GethasMember());
	if (GetsameAs() != Dc1NodePtr())
		result.Insert(GetsameAs());
	if (Getcoverage() != Dc1NodePtr())
		result.Insert(Getcoverage());
	if (Getrights() != Dc1NodePtr())
		result.Insert(Getrights());
	if (Getversion() != Dc1NodePtr())
		result.Insert(Getversion());
	if (GetpublicationHistory() != Dc1NodePtr())
		result.Insert(GetpublicationHistory());
	if (Getplanning() != Dc1NodePtr())
		result.Insert(Getplanning());
	if (Getrating() != Dc1NodePtr())
		result.Insert(Getrating());
	if (Getpart() != Dc1NodePtr())
		result.Insert(Getpart());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecoreMetadataType_ExtMethodImpl.h


