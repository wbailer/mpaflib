
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtImplInclude.h


#include "EbuCoreorganisationDetailsType.h"
#include "EbuCorepublicationServiceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorepublicationServiceType::IEbuCorepublicationServiceType()
{

// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtPropInit.h

}

IEbuCorepublicationServiceType::~IEbuCorepublicationServiceType()
{
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtPropCleanup.h

}

EbuCorepublicationServiceType::EbuCorepublicationServiceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorepublicationServiceType::~EbuCorepublicationServiceType()
{
	Cleanup();
}

void EbuCorepublicationServiceType::Init()
{

	// Init attributes
	m_serviceId = NULL; // String
	m_serviceId_Exist = false;
	m_linkToLogo = NULL; // String
	m_linkToLogo_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_publicationServiceName = NULL; // Optional String
	m_publicationServiceName_Exist = false;
	m_publicationSource = EbuCoreorganisationDetailsPtr(); // Class
	m_publicationSource_Exist = false;


// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMyPropInit.h

}

void EbuCorepublicationServiceType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMyPropCleanup.h


	XMLString::release(&m_serviceId); // String
	XMLString::release(&m_linkToLogo); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&this->m_publicationServiceName);
	this->m_publicationServiceName = NULL;
	// Dc1Factory::DeleteObject(m_publicationSource);
}

void EbuCorepublicationServiceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use publicationServiceTypePtr, since we
	// might need GetBase(), which isn't defined in IpublicationServiceType
	const Dc1Ptr< EbuCorepublicationServiceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_serviceId); // String
	if (tmp->ExistserviceId())
	{
		this->SetserviceId(XMLString::replicate(tmp->GetserviceId()));
	}
	else
	{
		InvalidateserviceId();
	}
	}
	{
	XMLString::release(&m_linkToLogo); // String
	if (tmp->ExistlinkToLogo())
	{
		this->SetlinkToLogo(XMLString::replicate(tmp->GetlinkToLogo()));
	}
	else
	{
		InvalidatelinkToLogo();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	if (tmp->IsValidpublicationServiceName())
	{
		this->SetpublicationServiceName(XMLString::replicate(tmp->GetpublicationServiceName()));
	}
	else
	{
		InvalidatepublicationServiceName();
	}
	if (tmp->IsValidpublicationSource())
	{
		// Dc1Factory::DeleteObject(m_publicationSource);
		this->SetpublicationSource(Dc1Factory::CloneObject(tmp->GetpublicationSource()));
	}
	else
	{
		InvalidatepublicationSource();
	}
}

Dc1NodePtr EbuCorepublicationServiceType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorepublicationServiceType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorepublicationServiceType::GetserviceId() const
{
	return m_serviceId;
}

bool EbuCorepublicationServiceType::ExistserviceId() const
{
	return m_serviceId_Exist;
}
void EbuCorepublicationServiceType::SetserviceId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SetserviceId().");
	}
	m_serviceId = item;
	m_serviceId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidateserviceId()
{
	m_serviceId_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GetlinkToLogo() const
{
	return m_linkToLogo;
}

bool EbuCorepublicationServiceType::ExistlinkToLogo() const
{
	return m_linkToLogo_Exist;
}
void EbuCorepublicationServiceType::SetlinkToLogo(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SetlinkToLogo().");
	}
	m_linkToLogo = item;
	m_linkToLogo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatelinkToLogo()
{
	m_linkToLogo_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorepublicationServiceType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorepublicationServiceType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorepublicationServiceType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorepublicationServiceType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorepublicationServiceType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorepublicationServiceType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorepublicationServiceType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorepublicationServiceType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorepublicationServiceType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorepublicationServiceType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCorepublicationServiceType::GetpublicationServiceName() const
{
		return m_publicationServiceName;
}

// Element is optional
bool EbuCorepublicationServiceType::IsValidpublicationServiceName() const
{
	return m_publicationServiceName_Exist;
}

EbuCoreorganisationDetailsPtr EbuCorepublicationServiceType::GetpublicationSource() const
{
		return m_publicationSource;
}

// Element is optional
bool EbuCorepublicationServiceType::IsValidpublicationSource() const
{
	return m_publicationSource_Exist;
}

void EbuCorepublicationServiceType::SetpublicationServiceName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SetpublicationServiceName().");
	}
	if (m_publicationServiceName != item || m_publicationServiceName_Exist == false)
	{
		XMLString::release(&m_publicationServiceName);
		m_publicationServiceName = item;
		m_publicationServiceName_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatepublicationServiceName()
{
	m_publicationServiceName_Exist = false;
}
void EbuCorepublicationServiceType::SetpublicationSource(const EbuCoreorganisationDetailsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorepublicationServiceType::SetpublicationSource().");
	}
	if (m_publicationSource != item || m_publicationSource_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_publicationSource);
		m_publicationSource = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publicationSource) m_publicationSource->SetParent(m_myself.getPointer());
		if (m_publicationSource && m_publicationSource->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publicationSource->UseTypeAttribute = true;
		}
		m_publicationSource_Exist = true;
		if(m_publicationSource != EbuCoreorganisationDetailsPtr())
		{
			m_publicationSource->SetContentName(XMLString::transcode("publicationSource"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorepublicationServiceType::InvalidatepublicationSource()
{
	m_publicationSource_Exist = false;
}

Dc1NodeEnum EbuCorepublicationServiceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("serviceId")) == 0)
	{
		// serviceId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("linkToLogo")) == 0)
	{
		// linkToLogo is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("publicationSource")) == 0)
	{
		// publicationSource is simple element organisationDetailsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpublicationSource()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType")))) != empty)
			{
				// Is type allowed
				EbuCoreorganisationDetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpublicationSource(p, client);
					if((p = GetpublicationSource()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for publicationServiceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "publicationServiceType");
	}
	return result;
}

XMLCh * EbuCorepublicationServiceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorepublicationServiceType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorepublicationServiceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationServiceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_serviceId_Exist)
	{
	// String
	if(m_serviceId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("serviceId"), m_serviceId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_linkToLogo_Exist)
	{
	// String
	if(m_linkToLogo != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("linkToLogo"), m_linkToLogo);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	 // String
	if(m_publicationServiceName != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_publicationServiceName, X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationServiceName"), true);
	// Class
	
	if (m_publicationSource != EbuCoreorganisationDetailsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_publicationSource->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("publicationSource"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_publicationSource->Serialize(doc, element, &elem);
	}

}

bool EbuCorepublicationServiceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("serviceId")))
	{
		// Deserialize string type
		this->SetserviceId(Dc1Convert::TextToString(parent->getAttribute(X("serviceId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("linkToLogo")))
	{
		// Deserialize string type
		this->SetlinkToLogo(Dc1Convert::TextToString(parent->getAttribute(X("linkToLogo"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("publicationServiceName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetpublicationServiceName(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetpublicationServiceName(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("publicationSource"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publicationSource")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateorganisationDetailsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetpublicationSource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorepublicationServiceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("publicationSource")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateorganisationDetailsType; // FTT, check this
	}
	this->SetpublicationSource(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorepublicationServiceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetpublicationSource() != Dc1NodePtr())
		result.Insert(GetpublicationSource());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMethodImpl.h


