
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtImplInclude.h


#include "EbuCoreancillaryDataFormatType_lineNumber_CollectionType.h"
#include "EbuCoreancillaryDataFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreancillaryDataFormatType::IEbuCoreancillaryDataFormatType()
{

// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtPropInit.h

}

IEbuCoreancillaryDataFormatType::~IEbuCoreancillaryDataFormatType()
{
// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtPropCleanup.h

}

EbuCoreancillaryDataFormatType::EbuCoreancillaryDataFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreancillaryDataFormatType::~EbuCoreancillaryDataFormatType()
{
	Cleanup();
}

void EbuCoreancillaryDataFormatType::Init()
{

	// Init attributes
	m_ancillaryDataFormatId = NULL; // String
	m_ancillaryDataFormatId_Exist = false;
	m_ancillaryDataFormatName = NULL; // String
	m_ancillaryDataFormatName_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_DID = (0); // Value

	m_DID_Exist = false;
	m_SDID = (0); // Value

	m_SDID_Exist = false;
	m_lineNumber = EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr(); // Collection
	m_wrappingType = (0); // Value

	m_wrappingType_Exist = false;


// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtMyPropInit.h

}

void EbuCoreancillaryDataFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_ancillaryDataFormatId); // String
	XMLString::release(&m_ancillaryDataFormatName); // String
	// Dc1Factory::DeleteObject(m_lineNumber);
}

void EbuCoreancillaryDataFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ancillaryDataFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IancillaryDataFormatType
	const Dc1Ptr< EbuCoreancillaryDataFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_ancillaryDataFormatId); // String
	if (tmp->ExistancillaryDataFormatId())
	{
		this->SetancillaryDataFormatId(XMLString::replicate(tmp->GetancillaryDataFormatId()));
	}
	else
	{
		InvalidateancillaryDataFormatId();
	}
	}
	{
	XMLString::release(&m_ancillaryDataFormatName); // String
	if (tmp->ExistancillaryDataFormatName())
	{
		this->SetancillaryDataFormatName(XMLString::replicate(tmp->GetancillaryDataFormatName()));
	}
	else
	{
		InvalidateancillaryDataFormatName();
	}
	}
	if (tmp->IsValidDID())
	{
		this->SetDID(tmp->GetDID());
	}
	else
	{
		InvalidateDID();
	}
	if (tmp->IsValidSDID())
	{
		this->SetSDID(tmp->GetSDID());
	}
	else
	{
		InvalidateSDID();
	}
		// Dc1Factory::DeleteObject(m_lineNumber);
		this->SetlineNumber(Dc1Factory::CloneObject(tmp->GetlineNumber()));
	if (tmp->IsValidwrappingType())
	{
		this->SetwrappingType(tmp->GetwrappingType());
	}
	else
	{
		InvalidatewrappingType();
	}
}

Dc1NodePtr EbuCoreancillaryDataFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreancillaryDataFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreancillaryDataFormatType::GetancillaryDataFormatId() const
{
	return m_ancillaryDataFormatId;
}

bool EbuCoreancillaryDataFormatType::ExistancillaryDataFormatId() const
{
	return m_ancillaryDataFormatId_Exist;
}
void EbuCoreancillaryDataFormatType::SetancillaryDataFormatId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetancillaryDataFormatId().");
	}
	m_ancillaryDataFormatId = item;
	m_ancillaryDataFormatId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::InvalidateancillaryDataFormatId()
{
	m_ancillaryDataFormatId_Exist = false;
}
XMLCh * EbuCoreancillaryDataFormatType::GetancillaryDataFormatName() const
{
	return m_ancillaryDataFormatName;
}

bool EbuCoreancillaryDataFormatType::ExistancillaryDataFormatName() const
{
	return m_ancillaryDataFormatName_Exist;
}
void EbuCoreancillaryDataFormatType::SetancillaryDataFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetancillaryDataFormatName().");
	}
	m_ancillaryDataFormatName = item;
	m_ancillaryDataFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::InvalidateancillaryDataFormatName()
{
	m_ancillaryDataFormatName_Exist = false;
}
int EbuCoreancillaryDataFormatType::GetDID() const
{
		return m_DID;
}

// Element is optional
bool EbuCoreancillaryDataFormatType::IsValidDID() const
{
	return m_DID_Exist;
}

int EbuCoreancillaryDataFormatType::GetSDID() const
{
		return m_SDID;
}

// Element is optional
bool EbuCoreancillaryDataFormatType::IsValidSDID() const
{
	return m_SDID_Exist;
}

EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr EbuCoreancillaryDataFormatType::GetlineNumber() const
{
		return m_lineNumber;
}

int EbuCoreancillaryDataFormatType::GetwrappingType() const
{
		return m_wrappingType;
}

// Element is optional
bool EbuCoreancillaryDataFormatType::IsValidwrappingType() const
{
	return m_wrappingType_Exist;
}

void EbuCoreancillaryDataFormatType::SetDID(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetDID().");
	}
	if (m_DID != item || m_DID_Exist == false)
	{
		m_DID = item;
		m_DID_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::InvalidateDID()
{
	m_DID_Exist = false;
}
void EbuCoreancillaryDataFormatType::SetSDID(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetSDID().");
	}
	if (m_SDID != item || m_SDID_Exist == false)
	{
		m_SDID = item;
		m_SDID_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::InvalidateSDID()
{
	m_SDID_Exist = false;
}
void EbuCoreancillaryDataFormatType::SetlineNumber(const EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetlineNumber().");
	}
	if (m_lineNumber != item)
	{
		// Dc1Factory::DeleteObject(m_lineNumber);
		m_lineNumber = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_lineNumber) m_lineNumber->SetParent(m_myself.getPointer());
		if(m_lineNumber != EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr())
		{
			m_lineNumber->SetContentName(XMLString::transcode("lineNumber"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::SetwrappingType(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreancillaryDataFormatType::SetwrappingType().");
	}
	if (m_wrappingType != item || m_wrappingType_Exist == false)
	{
		m_wrappingType = item;
		m_wrappingType_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreancillaryDataFormatType::InvalidatewrappingType()
{
	m_wrappingType_Exist = false;
}

Dc1NodeEnum EbuCoreancillaryDataFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("ancillaryDataFormatId")) == 0)
	{
		// ancillaryDataFormatId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("ancillaryDataFormatName")) == 0)
	{
		// ancillaryDataFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("lineNumber")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "int");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ancillaryDataFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ancillaryDataFormatType");
	}
	return result;
}

XMLCh * EbuCoreancillaryDataFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreancillaryDataFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreancillaryDataFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("ancillaryDataFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_ancillaryDataFormatId_Exist)
	{
	// String
	if(m_ancillaryDataFormatId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("ancillaryDataFormatId"), m_ancillaryDataFormatId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_ancillaryDataFormatName_Exist)
	{
	// String
	if(m_ancillaryDataFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("ancillaryDataFormatName"), m_ancillaryDataFormatName);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_DID_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_DID);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("DID"), true);
		XMLString::release(&tmp);
	}
	if(m_SDID_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_SDID);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("SDID"), true);
		XMLString::release(&tmp);
	}
	if (m_lineNumber != EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_lineNumber->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_wrappingType_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_wrappingType);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("wrappingType"), true);
		XMLString::release(&tmp);
	}

}

bool EbuCoreancillaryDataFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("ancillaryDataFormatId")))
	{
		// Deserialize string type
		this->SetancillaryDataFormatId(Dc1Convert::TextToString(parent->getAttribute(X("ancillaryDataFormatId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("ancillaryDataFormatName")))
	{
		// Deserialize string type
		this->SetancillaryDataFormatName(Dc1Convert::TextToString(parent->getAttribute(X("ancillaryDataFormatName"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("DID"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetDID(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("SDID"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetSDID(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("lineNumber"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("lineNumber")) == 0))
		{
			// Deserialize factory type
			EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr tmp = CreateancillaryDataFormatType_lineNumber_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetlineNumber(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("wrappingType"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetwrappingType(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreancillaryDataFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("lineNumber")) == 0))
  {
	EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr tmp = CreateancillaryDataFormatType_lineNumber_CollectionType; // FTT, check this
	this->SetlineNumber(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreancillaryDataFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetlineNumber() != Dc1NodePtr())
		result.Insert(GetlineNumber());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreancillaryDataFormatType_ExtMethodImpl.h


