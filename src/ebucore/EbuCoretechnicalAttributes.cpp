
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtImplInclude.h


#include "EbuCoretechnicalAttributes_technicalAttributeString_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeByte_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeShort_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeLong_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeRational_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUri_CollectionType.h"
#include "EbuCoretechnicalAttributes.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreString.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeString
#include "EbuCoreInt8.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeByte
#include "EbuCoreInt16.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeShort
#include "EbuCoreInt32.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeInteger
#include "EbuCoreInt64.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeLong
#include "EbuCoreUInt8.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedByte
#include "EbuCoreUInt16.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedShort
#include "EbuCoreUInt32.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedInteger
#include "EbuCoreUInt64.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedLong
#include "EbuCoreBoolean.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeBoolean
#include "EbuCoreFloat.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeFloat
#include "EbuCoretechnicalAttributeRationalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRational
#include "EbuCoretechnicalAttributeUriType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUri

#include <assert.h>
IEbuCoretechnicalAttributes::IEbuCoretechnicalAttributes()
{

// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtPropInit.h

}

IEbuCoretechnicalAttributes::~IEbuCoretechnicalAttributes()
{
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtPropCleanup.h

}

EbuCoretechnicalAttributes::EbuCoretechnicalAttributes()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoretechnicalAttributes::~EbuCoretechnicalAttributes()
{
	Cleanup();
}

void EbuCoretechnicalAttributes::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_technicalAttributeString = EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr(); // Collection
	m_technicalAttributeByte = EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr(); // Collection
	m_technicalAttributeShort = EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr(); // Collection
	m_technicalAttributeInteger = EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr(); // Collection
	m_technicalAttributeLong = EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr(); // Collection
	m_technicalAttributeUnsignedByte = EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr(); // Collection
	m_technicalAttributeUnsignedShort = EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr(); // Collection
	m_technicalAttributeUnsignedInteger = EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr(); // Collection
	m_technicalAttributeUnsignedLong = EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr(); // Collection
	m_technicalAttributeBoolean = EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr(); // Collection
	m_technicalAttributeFloat = EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr(); // Collection
	m_technicalAttributeRational = EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr(); // Collection
	m_technicalAttributeUri = EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMyPropInit.h

}

void EbuCoretechnicalAttributes::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_technicalAttributeString);
	// Dc1Factory::DeleteObject(m_technicalAttributeByte);
	// Dc1Factory::DeleteObject(m_technicalAttributeShort);
	// Dc1Factory::DeleteObject(m_technicalAttributeInteger);
	// Dc1Factory::DeleteObject(m_technicalAttributeLong);
	// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedByte);
	// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedShort);
	// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedInteger);
	// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedLong);
	// Dc1Factory::DeleteObject(m_technicalAttributeBoolean);
	// Dc1Factory::DeleteObject(m_technicalAttributeFloat);
	// Dc1Factory::DeleteObject(m_technicalAttributeRational);
	// Dc1Factory::DeleteObject(m_technicalAttributeUri);
}

void EbuCoretechnicalAttributes::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use technicalAttributesPtr, since we
	// might need GetBase(), which isn't defined in ItechnicalAttributes
	const Dc1Ptr< EbuCoretechnicalAttributes > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_technicalAttributeString);
		this->SettechnicalAttributeString(Dc1Factory::CloneObject(tmp->GettechnicalAttributeString()));
		// Dc1Factory::DeleteObject(m_technicalAttributeByte);
		this->SettechnicalAttributeByte(Dc1Factory::CloneObject(tmp->GettechnicalAttributeByte()));
		// Dc1Factory::DeleteObject(m_technicalAttributeShort);
		this->SettechnicalAttributeShort(Dc1Factory::CloneObject(tmp->GettechnicalAttributeShort()));
		// Dc1Factory::DeleteObject(m_technicalAttributeInteger);
		this->SettechnicalAttributeInteger(Dc1Factory::CloneObject(tmp->GettechnicalAttributeInteger()));
		// Dc1Factory::DeleteObject(m_technicalAttributeLong);
		this->SettechnicalAttributeLong(Dc1Factory::CloneObject(tmp->GettechnicalAttributeLong()));
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedByte);
		this->SettechnicalAttributeUnsignedByte(Dc1Factory::CloneObject(tmp->GettechnicalAttributeUnsignedByte()));
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedShort);
		this->SettechnicalAttributeUnsignedShort(Dc1Factory::CloneObject(tmp->GettechnicalAttributeUnsignedShort()));
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedInteger);
		this->SettechnicalAttributeUnsignedInteger(Dc1Factory::CloneObject(tmp->GettechnicalAttributeUnsignedInteger()));
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedLong);
		this->SettechnicalAttributeUnsignedLong(Dc1Factory::CloneObject(tmp->GettechnicalAttributeUnsignedLong()));
		// Dc1Factory::DeleteObject(m_technicalAttributeBoolean);
		this->SettechnicalAttributeBoolean(Dc1Factory::CloneObject(tmp->GettechnicalAttributeBoolean()));
		// Dc1Factory::DeleteObject(m_technicalAttributeFloat);
		this->SettechnicalAttributeFloat(Dc1Factory::CloneObject(tmp->GettechnicalAttributeFloat()));
		// Dc1Factory::DeleteObject(m_technicalAttributeRational);
		this->SettechnicalAttributeRational(Dc1Factory::CloneObject(tmp->GettechnicalAttributeRational()));
		// Dc1Factory::DeleteObject(m_technicalAttributeUri);
		this->SettechnicalAttributeUri(Dc1Factory::CloneObject(tmp->GettechnicalAttributeUri()));
}

Dc1NodePtr EbuCoretechnicalAttributes::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretechnicalAttributes::GetBaseRoot() const
{
	return m_myself.getPointer();
}


EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeString() const
{
		return m_technicalAttributeString;
}

EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeByte() const
{
		return m_technicalAttributeByte;
}

EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeShort() const
{
		return m_technicalAttributeShort;
}

EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeInteger() const
{
		return m_technicalAttributeInteger;
}

EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeLong() const
{
		return m_technicalAttributeLong;
}

EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeUnsignedByte() const
{
		return m_technicalAttributeUnsignedByte;
}

EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeUnsignedShort() const
{
		return m_technicalAttributeUnsignedShort;
}

EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeUnsignedInteger() const
{
		return m_technicalAttributeUnsignedInteger;
}

EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeUnsignedLong() const
{
		return m_technicalAttributeUnsignedLong;
}

EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeBoolean() const
{
		return m_technicalAttributeBoolean;
}

EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeFloat() const
{
		return m_technicalAttributeFloat;
}

EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeRational() const
{
		return m_technicalAttributeRational;
}

EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr EbuCoretechnicalAttributes::GettechnicalAttributeUri() const
{
		return m_technicalAttributeUri;
}

void EbuCoretechnicalAttributes::SettechnicalAttributeString(const EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeString().");
	}
	if (m_technicalAttributeString != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeString);
		m_technicalAttributeString = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeString) m_technicalAttributeString->SetParent(m_myself.getPointer());
		if(m_technicalAttributeString != EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr())
		{
			m_technicalAttributeString->SetContentName(XMLString::transcode("technicalAttributeString"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeByte(const EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeByte().");
	}
	if (m_technicalAttributeByte != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeByte);
		m_technicalAttributeByte = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeByte) m_technicalAttributeByte->SetParent(m_myself.getPointer());
		if(m_technicalAttributeByte != EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr())
		{
			m_technicalAttributeByte->SetContentName(XMLString::transcode("technicalAttributeByte"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeShort(const EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeShort().");
	}
	if (m_technicalAttributeShort != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeShort);
		m_technicalAttributeShort = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeShort) m_technicalAttributeShort->SetParent(m_myself.getPointer());
		if(m_technicalAttributeShort != EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr())
		{
			m_technicalAttributeShort->SetContentName(XMLString::transcode("technicalAttributeShort"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeInteger(const EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeInteger().");
	}
	if (m_technicalAttributeInteger != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeInteger);
		m_technicalAttributeInteger = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeInteger) m_technicalAttributeInteger->SetParent(m_myself.getPointer());
		if(m_technicalAttributeInteger != EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr())
		{
			m_technicalAttributeInteger->SetContentName(XMLString::transcode("technicalAttributeInteger"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeLong(const EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeLong().");
	}
	if (m_technicalAttributeLong != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeLong);
		m_technicalAttributeLong = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeLong) m_technicalAttributeLong->SetParent(m_myself.getPointer());
		if(m_technicalAttributeLong != EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr())
		{
			m_technicalAttributeLong->SetContentName(XMLString::transcode("technicalAttributeLong"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedByte(const EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedByte().");
	}
	if (m_technicalAttributeUnsignedByte != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedByte);
		m_technicalAttributeUnsignedByte = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeUnsignedByte) m_technicalAttributeUnsignedByte->SetParent(m_myself.getPointer());
		if(m_technicalAttributeUnsignedByte != EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr())
		{
			m_technicalAttributeUnsignedByte->SetContentName(XMLString::transcode("technicalAttributeUnsignedByte"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedShort(const EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedShort().");
	}
	if (m_technicalAttributeUnsignedShort != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedShort);
		m_technicalAttributeUnsignedShort = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeUnsignedShort) m_technicalAttributeUnsignedShort->SetParent(m_myself.getPointer());
		if(m_technicalAttributeUnsignedShort != EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr())
		{
			m_technicalAttributeUnsignedShort->SetContentName(XMLString::transcode("technicalAttributeUnsignedShort"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedInteger(const EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedInteger().");
	}
	if (m_technicalAttributeUnsignedInteger != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedInteger);
		m_technicalAttributeUnsignedInteger = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeUnsignedInteger) m_technicalAttributeUnsignedInteger->SetParent(m_myself.getPointer());
		if(m_technicalAttributeUnsignedInteger != EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr())
		{
			m_technicalAttributeUnsignedInteger->SetContentName(XMLString::transcode("technicalAttributeUnsignedInteger"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedLong(const EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeUnsignedLong().");
	}
	if (m_technicalAttributeUnsignedLong != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeUnsignedLong);
		m_technicalAttributeUnsignedLong = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeUnsignedLong) m_technicalAttributeUnsignedLong->SetParent(m_myself.getPointer());
		if(m_technicalAttributeUnsignedLong != EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr())
		{
			m_technicalAttributeUnsignedLong->SetContentName(XMLString::transcode("technicalAttributeUnsignedLong"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeBoolean(const EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeBoolean().");
	}
	if (m_technicalAttributeBoolean != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeBoolean);
		m_technicalAttributeBoolean = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeBoolean) m_technicalAttributeBoolean->SetParent(m_myself.getPointer());
		if(m_technicalAttributeBoolean != EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr())
		{
			m_technicalAttributeBoolean->SetContentName(XMLString::transcode("technicalAttributeBoolean"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeFloat(const EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeFloat().");
	}
	if (m_technicalAttributeFloat != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeFloat);
		m_technicalAttributeFloat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeFloat) m_technicalAttributeFloat->SetParent(m_myself.getPointer());
		if(m_technicalAttributeFloat != EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr())
		{
			m_technicalAttributeFloat->SetContentName(XMLString::transcode("technicalAttributeFloat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeRational(const EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeRational().");
	}
	if (m_technicalAttributeRational != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeRational);
		m_technicalAttributeRational = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeRational) m_technicalAttributeRational->SetParent(m_myself.getPointer());
		if(m_technicalAttributeRational != EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr())
		{
			m_technicalAttributeRational->SetContentName(XMLString::transcode("technicalAttributeRational"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretechnicalAttributes::SettechnicalAttributeUri(const EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretechnicalAttributes::SettechnicalAttributeUri().");
	}
	if (m_technicalAttributeUri != item)
	{
		// Dc1Factory::DeleteObject(m_technicalAttributeUri);
		m_technicalAttributeUri = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_technicalAttributeUri) m_technicalAttributeUri->SetParent(m_myself.getPointer());
		if(m_technicalAttributeUri != EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr())
		{
			m_technicalAttributeUri->SetContentName(XMLString::transcode("technicalAttributeUri"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoretechnicalAttributes::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("technicalAttributeString")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeString is item of type String
		// in element collection technicalAttributes_technicalAttributeString_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr coll = GettechnicalAttributeString();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeString_CollectionType; // FTT, check this
				SettechnicalAttributeString(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:String")))) != empty)
			{
				// Is type allowed
				EbuCoreStringPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeByte")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeByte is item of type Int8
		// in element collection technicalAttributes_technicalAttributeByte_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr coll = GettechnicalAttributeByte();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeByte_CollectionType; // FTT, check this
				SettechnicalAttributeByte(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int8")))) != empty)
			{
				// Is type allowed
				EbuCoreInt8Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeShort")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeShort is item of type Int16
		// in element collection technicalAttributes_technicalAttributeShort_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr coll = GettechnicalAttributeShort();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeShort_CollectionType; // FTT, check this
				SettechnicalAttributeShort(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int16")))) != empty)
			{
				// Is type allowed
				EbuCoreInt16Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeInteger")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeInteger is item of type Int32
		// in element collection technicalAttributes_technicalAttributeInteger_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr coll = GettechnicalAttributeInteger();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeInteger_CollectionType; // FTT, check this
				SettechnicalAttributeInteger(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int32")))) != empty)
			{
				// Is type allowed
				EbuCoreInt32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeLong")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeLong is item of type Int64
		// in element collection technicalAttributes_technicalAttributeLong_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr coll = GettechnicalAttributeLong();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeLong_CollectionType; // FTT, check this
				SettechnicalAttributeLong(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int64")))) != empty)
			{
				// Is type allowed
				EbuCoreInt64Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeUnsignedByte")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedByte is item of type UInt8
		// in element collection technicalAttributes_technicalAttributeUnsignedByte_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr coll = GettechnicalAttributeUnsignedByte();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeUnsignedByte_CollectionType; // FTT, check this
				SettechnicalAttributeUnsignedByte(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt8")))) != empty)
			{
				// Is type allowed
				EbuCoreUInt8Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeUnsignedShort")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedShort is item of type UInt16
		// in element collection technicalAttributes_technicalAttributeUnsignedShort_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr coll = GettechnicalAttributeUnsignedShort();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeUnsignedShort_CollectionType; // FTT, check this
				SettechnicalAttributeUnsignedShort(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt16")))) != empty)
			{
				// Is type allowed
				EbuCoreUInt16Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeUnsignedInteger")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedInteger is item of type UInt32
		// in element collection technicalAttributes_technicalAttributeUnsignedInteger_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr coll = GettechnicalAttributeUnsignedInteger();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType; // FTT, check this
				SettechnicalAttributeUnsignedInteger(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt32")))) != empty)
			{
				// Is type allowed
				EbuCoreUInt32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeUnsignedLong")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedLong is item of type UInt64
		// in element collection technicalAttributes_technicalAttributeUnsignedLong_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr coll = GettechnicalAttributeUnsignedLong();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeUnsignedLong_CollectionType; // FTT, check this
				SettechnicalAttributeUnsignedLong(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt64")))) != empty)
			{
				// Is type allowed
				EbuCoreUInt64Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeBoolean")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeBoolean is item of type Boolean
		// in element collection technicalAttributes_technicalAttributeBoolean_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr coll = GettechnicalAttributeBoolean();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeBoolean_CollectionType; // FTT, check this
				SettechnicalAttributeBoolean(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Boolean")))) != empty)
			{
				// Is type allowed
				EbuCoreBooleanPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeFloat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeFloat is item of type Float
		// in element collection technicalAttributes_technicalAttributeFloat_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr coll = GettechnicalAttributeFloat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeFloat_CollectionType; // FTT, check this
				SettechnicalAttributeFloat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Float")))) != empty)
			{
				// Is type allowed
				EbuCoreFloatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeRational")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRational is item of type technicalAttributeRationalType
		// in element collection technicalAttributes_technicalAttributeRational_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr coll = GettechnicalAttributeRational();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeRational_CollectionType; // FTT, check this
				SettechnicalAttributeRational(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRationalType")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributeRationalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("technicalAttributeUri")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUri is item of type technicalAttributeUriType
		// in element collection technicalAttributes_technicalAttributeUri_CollectionType
		
		context->Found = true;
		EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr coll = GettechnicalAttributeUri();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetechnicalAttributes_technicalAttributeUri_CollectionType; // FTT, check this
				SettechnicalAttributeUri(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUriType")))) != empty)
			{
				// Is type allowed
				EbuCoretechnicalAttributeUriPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for technicalAttributes
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "technicalAttributes");
	}
	return result;
}

XMLCh * EbuCoretechnicalAttributes::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoretechnicalAttributes::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoretechnicalAttributes::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("technicalAttributes"));
	// Element serialization:
	if (m_technicalAttributeString != EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeString->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeByte != EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeByte->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeShort != EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeShort->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeInteger != EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeInteger->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeLong != EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeLong->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeUnsignedByte != EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeUnsignedByte->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeUnsignedShort != EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeUnsignedShort->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeUnsignedInteger != EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeUnsignedInteger->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeUnsignedLong != EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeUnsignedLong->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeBoolean != EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeBoolean->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeFloat != EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeFloat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeRational != EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeRational->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_technicalAttributeUri != EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_technicalAttributeUri->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoretechnicalAttributes::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeString"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeString")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeString_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeString(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeByte"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeByte")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeByte_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeByte(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeShort"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeShort")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeShort_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeShort(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeInteger"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeInteger")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeInteger_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeInteger(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeLong"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeLong")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeLong_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeLong(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeUnsignedByte"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeUnsignedByte")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedByte_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeUnsignedByte(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeUnsignedShort"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeUnsignedShort")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedShort_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeUnsignedShort(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeUnsignedInteger"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeUnsignedInteger")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeUnsignedInteger(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeUnsignedLong"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeUnsignedLong")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedLong_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeUnsignedLong(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeBoolean"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeBoolean")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeBoolean_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeBoolean(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeFloat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeFloat")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeFloat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeFloat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeRational"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeRational")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeRational_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeRational(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("technicalAttributeUri"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("technicalAttributeUri")) == 0))
		{
			// Deserialize factory type
			EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUri_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettechnicalAttributeUri(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoretechnicalAttributes::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeString")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeString_CollectionType; // FTT, check this
	this->SettechnicalAttributeString(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeByte")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeByte_CollectionType; // FTT, check this
	this->SettechnicalAttributeByte(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeShort")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeShort_CollectionType; // FTT, check this
	this->SettechnicalAttributeShort(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeInteger")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeInteger_CollectionType; // FTT, check this
	this->SettechnicalAttributeInteger(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeLong")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeLong_CollectionType; // FTT, check this
	this->SettechnicalAttributeLong(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeUnsignedByte")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedByte_CollectionType; // FTT, check this
	this->SettechnicalAttributeUnsignedByte(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeUnsignedShort")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedShort_CollectionType; // FTT, check this
	this->SettechnicalAttributeUnsignedShort(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeUnsignedInteger")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType; // FTT, check this
	this->SettechnicalAttributeUnsignedInteger(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeUnsignedLong")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUnsignedLong_CollectionType; // FTT, check this
	this->SettechnicalAttributeUnsignedLong(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeBoolean")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeBoolean_CollectionType; // FTT, check this
	this->SettechnicalAttributeBoolean(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeFloat")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeFloat_CollectionType; // FTT, check this
	this->SettechnicalAttributeFloat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeRational")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeRational_CollectionType; // FTT, check this
	this->SettechnicalAttributeRational(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("technicalAttributeUri")) == 0))
  {
	EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr tmp = CreatetechnicalAttributes_technicalAttributeUri_CollectionType; // FTT, check this
	this->SettechnicalAttributeUri(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoretechnicalAttributes::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GettechnicalAttributeString() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeString());
	if (GettechnicalAttributeByte() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeByte());
	if (GettechnicalAttributeShort() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeShort());
	if (GettechnicalAttributeInteger() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeInteger());
	if (GettechnicalAttributeLong() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeLong());
	if (GettechnicalAttributeUnsignedByte() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeUnsignedByte());
	if (GettechnicalAttributeUnsignedShort() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeUnsignedShort());
	if (GettechnicalAttributeUnsignedInteger() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeUnsignedInteger());
	if (GettechnicalAttributeUnsignedLong() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeUnsignedLong());
	if (GettechnicalAttributeBoolean() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeBoolean());
	if (GettechnicalAttributeFloat() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeFloat());
	if (GettechnicalAttributeRational() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeRational());
	if (GettechnicalAttributeUri() != Dc1NodePtr())
		result.Insert(GettechnicalAttributeUri());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMethodImpl.h


