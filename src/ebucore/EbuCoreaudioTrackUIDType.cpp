
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtImplInclude.h


#include "EbuCoreaudioMXFLookUpType.h"
#include "EbuCoreaudioTrackUIDType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioTrackUIDType::IEbuCoreaudioTrackUIDType()
{

// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtPropInit.h

}

IEbuCoreaudioTrackUIDType::~IEbuCoreaudioTrackUIDType()
{
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtPropCleanup.h

}

EbuCoreaudioTrackUIDType::EbuCoreaudioTrackUIDType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioTrackUIDType::~EbuCoreaudioTrackUIDType()
{
	Cleanup();
}

void EbuCoreaudioTrackUIDType::Init()
{

	// Init attributes
	m_UID = NULL; // String
	m_UID_Exist = false;
	m_sampleRate = 0; // Value
	m_sampleRate_Exist = false;
	m_bitDepth = 0; // Value
	m_bitDepth_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioMXFLookUp = EbuCoreaudioMXFLookUpPtr(); // Class
	m_audioMXFLookUp_Exist = false;
	m_audioTrackFormatIDRef = NULL; // Optional String
	m_audioTrackFormatIDRef_Exist = false;
	m_audioPackFormatIDRef = NULL; // Optional String
	m_audioPackFormatIDRef_Exist = false;


// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMyPropInit.h

}

void EbuCoreaudioTrackUIDType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMyPropCleanup.h


	XMLString::release(&m_UID); // String
	// Dc1Factory::DeleteObject(m_audioMXFLookUp);
	XMLString::release(&this->m_audioTrackFormatIDRef);
	this->m_audioTrackFormatIDRef = NULL;
	XMLString::release(&this->m_audioPackFormatIDRef);
	this->m_audioPackFormatIDRef = NULL;
}

void EbuCoreaudioTrackUIDType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioTrackUIDTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioTrackUIDType
	const Dc1Ptr< EbuCoreaudioTrackUIDType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_UID); // String
	if (tmp->ExistUID())
	{
		this->SetUID(XMLString::replicate(tmp->GetUID()));
	}
	else
	{
		InvalidateUID();
	}
	}
	{
	if (tmp->ExistsampleRate())
	{
		this->SetsampleRate(tmp->GetsampleRate());
	}
	else
	{
		InvalidatesampleRate();
	}
	}
	{
	if (tmp->ExistbitDepth())
	{
		this->SetbitDepth(tmp->GetbitDepth());
	}
	else
	{
		InvalidatebitDepth();
	}
	}
	if (tmp->IsValidaudioMXFLookUp())
	{
		// Dc1Factory::DeleteObject(m_audioMXFLookUp);
		this->SetaudioMXFLookUp(Dc1Factory::CloneObject(tmp->GetaudioMXFLookUp()));
	}
	else
	{
		InvalidateaudioMXFLookUp();
	}
	if (tmp->IsValidaudioTrackFormatIDRef())
	{
		this->SetaudioTrackFormatIDRef(XMLString::replicate(tmp->GetaudioTrackFormatIDRef()));
	}
	else
	{
		InvalidateaudioTrackFormatIDRef();
	}
	if (tmp->IsValidaudioPackFormatIDRef())
	{
		this->SetaudioPackFormatIDRef(XMLString::replicate(tmp->GetaudioPackFormatIDRef()));
	}
	else
	{
		InvalidateaudioPackFormatIDRef();
	}
}

Dc1NodePtr EbuCoreaudioTrackUIDType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioTrackUIDType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioTrackUIDType::GetUID() const
{
	return m_UID;
}

bool EbuCoreaudioTrackUIDType::ExistUID() const
{
	return m_UID_Exist;
}
void EbuCoreaudioTrackUIDType::SetUID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetUID().");
	}
	m_UID = item;
	m_UID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidateUID()
{
	m_UID_Exist = false;
}
int EbuCoreaudioTrackUIDType::GetsampleRate() const
{
	return m_sampleRate;
}

bool EbuCoreaudioTrackUIDType::ExistsampleRate() const
{
	return m_sampleRate_Exist;
}
void EbuCoreaudioTrackUIDType::SetsampleRate(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetsampleRate().");
	}
	m_sampleRate = item;
	m_sampleRate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidatesampleRate()
{
	m_sampleRate_Exist = false;
}
int EbuCoreaudioTrackUIDType::GetbitDepth() const
{
	return m_bitDepth;
}

bool EbuCoreaudioTrackUIDType::ExistbitDepth() const
{
	return m_bitDepth_Exist;
}
void EbuCoreaudioTrackUIDType::SetbitDepth(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetbitDepth().");
	}
	m_bitDepth = item;
	m_bitDepth_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidatebitDepth()
{
	m_bitDepth_Exist = false;
}
EbuCoreaudioMXFLookUpPtr EbuCoreaudioTrackUIDType::GetaudioMXFLookUp() const
{
		return m_audioMXFLookUp;
}

// Element is optional
bool EbuCoreaudioTrackUIDType::IsValidaudioMXFLookUp() const
{
	return m_audioMXFLookUp_Exist;
}

XMLCh * EbuCoreaudioTrackUIDType::GetaudioTrackFormatIDRef() const
{
		return m_audioTrackFormatIDRef;
}

// Element is optional
bool EbuCoreaudioTrackUIDType::IsValidaudioTrackFormatIDRef() const
{
	return m_audioTrackFormatIDRef_Exist;
}

XMLCh * EbuCoreaudioTrackUIDType::GetaudioPackFormatIDRef() const
{
		return m_audioPackFormatIDRef;
}

// Element is optional
bool EbuCoreaudioTrackUIDType::IsValidaudioPackFormatIDRef() const
{
	return m_audioPackFormatIDRef_Exist;
}

void EbuCoreaudioTrackUIDType::SetaudioMXFLookUp(const EbuCoreaudioMXFLookUpPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetaudioMXFLookUp().");
	}
	if (m_audioMXFLookUp != item || m_audioMXFLookUp_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_audioMXFLookUp);
		m_audioMXFLookUp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioMXFLookUp) m_audioMXFLookUp->SetParent(m_myself.getPointer());
		if (m_audioMXFLookUp && m_audioMXFLookUp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioMXFLookUpType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_audioMXFLookUp->UseTypeAttribute = true;
		}
		m_audioMXFLookUp_Exist = true;
		if(m_audioMXFLookUp != EbuCoreaudioMXFLookUpPtr())
		{
			m_audioMXFLookUp->SetContentName(XMLString::transcode("audioMXFLookUp"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidateaudioMXFLookUp()
{
	m_audioMXFLookUp_Exist = false;
}
void EbuCoreaudioTrackUIDType::SetaudioTrackFormatIDRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetaudioTrackFormatIDRef().");
	}
	if (m_audioTrackFormatIDRef != item || m_audioTrackFormatIDRef_Exist == false)
	{
		XMLString::release(&m_audioTrackFormatIDRef);
		m_audioTrackFormatIDRef = item;
		m_audioTrackFormatIDRef_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidateaudioTrackFormatIDRef()
{
	m_audioTrackFormatIDRef_Exist = false;
}
void EbuCoreaudioTrackUIDType::SetaudioPackFormatIDRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioTrackUIDType::SetaudioPackFormatIDRef().");
	}
	if (m_audioPackFormatIDRef != item || m_audioPackFormatIDRef_Exist == false)
	{
		XMLString::release(&m_audioPackFormatIDRef);
		m_audioPackFormatIDRef = item;
		m_audioPackFormatIDRef_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioTrackUIDType::InvalidateaudioPackFormatIDRef()
{
	m_audioPackFormatIDRef_Exist = false;
}

Dc1NodeEnum EbuCoreaudioTrackUIDType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("UID")) == 0)
	{
		// UID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("sampleRate")) == 0)
	{
		// sampleRate is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("bitDepth")) == 0)
	{
		// bitDepth is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("audioMXFLookUp")) == 0)
	{
		// audioMXFLookUp is simple element audioMXFLookUpType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetaudioMXFLookUp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioMXFLookUpType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioMXFLookUpPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetaudioMXFLookUp(p, client);
					if((p = GetaudioMXFLookUp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioTrackUIDType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioTrackUIDType");
	}
	return result;
}

XMLCh * EbuCoreaudioTrackUIDType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioTrackUIDType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioTrackUIDType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioTrackUIDType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_UID_Exist)
	{
	// String
	if(m_UID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("UID"), m_UID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_sampleRate_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_sampleRate);
		element->setAttributeNS(X(""), X("sampleRate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_bitDepth_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_bitDepth);
		element->setAttributeNS(X(""), X("bitDepth"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_audioMXFLookUp != EbuCoreaudioMXFLookUpPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_audioMXFLookUp->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioMXFLookUp"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_audioMXFLookUp->Serialize(doc, element, &elem);
	}
	 // String
	if(m_audioTrackFormatIDRef != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_audioTrackFormatIDRef, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioTrackFormatIDRef"), true);
	 // String
	if(m_audioPackFormatIDRef != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_audioPackFormatIDRef, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioPackFormatIDRef"), true);

}

bool EbuCoreaudioTrackUIDType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("UID")))
	{
		// Deserialize string type
		this->SetUID(Dc1Convert::TextToString(parent->getAttribute(X("UID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("sampleRate")))
	{
		// deserialize value type
		this->SetsampleRate(Dc1Convert::TextToInt(parent->getAttribute(X("sampleRate"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("bitDepth")))
	{
		// deserialize value type
		this->SetbitDepth(Dc1Convert::TextToInt(parent->getAttribute(X("bitDepth"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("audioMXFLookUp"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("audioMXFLookUp")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaudioMXFLookUpType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetaudioMXFLookUp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("audioTrackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetaudioTrackFormatIDRef(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetaudioTrackFormatIDRef(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("audioPackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetaudioPackFormatIDRef(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetaudioPackFormatIDRef(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioTrackUIDType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("audioMXFLookUp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaudioMXFLookUpType; // FTT, check this
	}
	this->SetaudioMXFLookUp(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioTrackUIDType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioMXFLookUp() != Dc1NodePtr())
		result.Insert(GetaudioMXFLookUp());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMethodImpl.h


