
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtImplInclude.h


#include "EbuCoretimecodeType.h"
#include "EbuCoreaudioBlockFormatType_speakerLabel_CollectionType.h"
#include "EbuCoreaudioBlockFormatType_position_CollectionType.h"
#include "EbuCorematrixType.h"
#include "EbuCoreaudioBlockFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreaudioBlockFormatType_speakerLabel_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:speakerLabel
#include "EbuCoreposition_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:position

#include <assert.h>
IEbuCoreaudioBlockFormatType::IEbuCoreaudioBlockFormatType()
{

// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtPropInit.h

}

IEbuCoreaudioBlockFormatType::~IEbuCoreaudioBlockFormatType()
{
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtPropCleanup.h

}

EbuCoreaudioBlockFormatType::EbuCoreaudioBlockFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioBlockFormatType::~EbuCoreaudioBlockFormatType()
{
	Cleanup();
}

void EbuCoreaudioBlockFormatType::Init()
{

	// Init attributes
	m_audioBlockFormatID = NULL; // String
	m_audioBlockFormatID_Exist = false;
	m_rtime = EbuCoretimecodePtr(); // Pattern
	m_rtime_Exist = false;
	m_duration = EbuCoretimecodePtr(); // Pattern
	m_duration_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_speakerLabel = EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr(); // Collection
	m_position = EbuCoreaudioBlockFormatType_position_CollectionPtr(); // Collection
	m_matrix = EbuCorematrixPtr(); // Class
	m_matrix_Exist = false;
	m_gain = (0.0f); // Value

	m_gain_Exist = false;
	m_diffuse = (false); // Value

	m_diffuse_Exist = false;
	m_width = (0.0f); // Value

	m_width_Exist = false;
	m_height = (0.0f); // Value

	m_height_Exist = false;
	m_depth = (0.0f); // Value

	m_depth_Exist = false;
	m_channelLock = (false); // Value

	m_channelLock_Exist = false;
	m_jumpPosition = (false); // Value

	m_jumpPosition_Exist = false;
	m_equation = NULL; // Optional String
	m_equation_Exist = false;
	m_degree = (0); // Value

	m_degree_Exist = false;
	m_order = (0); // Value

	m_order_Exist = false;


// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMyPropInit.h

}

void EbuCoreaudioBlockFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_audioBlockFormatID); // String
	// Dc1Factory::DeleteObject(m_rtime); // Pattern
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	// Dc1Factory::DeleteObject(m_speakerLabel);
	// Dc1Factory::DeleteObject(m_position);
	// Dc1Factory::DeleteObject(m_matrix);
	XMLString::release(&this->m_equation);
	this->m_equation = NULL;
}

void EbuCoreaudioBlockFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioBlockFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioBlockFormatType
	const Dc1Ptr< EbuCoreaudioBlockFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioBlockFormatID); // String
	if (tmp->ExistaudioBlockFormatID())
	{
		this->SetaudioBlockFormatID(XMLString::replicate(tmp->GetaudioBlockFormatID()));
	}
	else
	{
		InvalidateaudioBlockFormatID();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_rtime); // Pattern
	if (tmp->Existrtime())
	{
		this->Setrtime(Dc1Factory::CloneObject(tmp->Getrtime()));
	}
	else
	{
		Invalidatertime();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	if (tmp->Existduration())
	{
		this->Setduration(Dc1Factory::CloneObject(tmp->Getduration()));
	}
	else
	{
		Invalidateduration();
	}
	}
		// Dc1Factory::DeleteObject(m_speakerLabel);
		this->SetspeakerLabel(Dc1Factory::CloneObject(tmp->GetspeakerLabel()));
		// Dc1Factory::DeleteObject(m_position);
		this->Setposition(Dc1Factory::CloneObject(tmp->Getposition()));
	if (tmp->IsValidmatrix())
	{
		// Dc1Factory::DeleteObject(m_matrix);
		this->Setmatrix(Dc1Factory::CloneObject(tmp->Getmatrix()));
	}
	else
	{
		Invalidatematrix();
	}
	if (tmp->IsValidgain())
	{
		this->Setgain(tmp->Getgain());
	}
	else
	{
		Invalidategain();
	}
	if (tmp->IsValiddiffuse())
	{
		this->Setdiffuse(tmp->Getdiffuse());
	}
	else
	{
		Invalidatediffuse();
	}
	if (tmp->IsValidwidth())
	{
		this->Setwidth(tmp->Getwidth());
	}
	else
	{
		Invalidatewidth();
	}
	if (tmp->IsValidheight())
	{
		this->Setheight(tmp->Getheight());
	}
	else
	{
		Invalidateheight();
	}
	if (tmp->IsValiddepth())
	{
		this->Setdepth(tmp->Getdepth());
	}
	else
	{
		Invalidatedepth();
	}
	if (tmp->IsValidchannelLock())
	{
		this->SetchannelLock(tmp->GetchannelLock());
	}
	else
	{
		InvalidatechannelLock();
	}
	if (tmp->IsValidjumpPosition())
	{
		this->SetjumpPosition(tmp->GetjumpPosition());
	}
	else
	{
		InvalidatejumpPosition();
	}
	if (tmp->IsValidequation())
	{
		this->Setequation(XMLString::replicate(tmp->Getequation()));
	}
	else
	{
		Invalidateequation();
	}
	if (tmp->IsValiddegree())
	{
		this->Setdegree(tmp->Getdegree());
	}
	else
	{
		Invalidatedegree();
	}
	if (tmp->IsValidorder())
	{
		this->Setorder(tmp->Getorder());
	}
	else
	{
		Invalidateorder();
	}
}

Dc1NodePtr EbuCoreaudioBlockFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioBlockFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioBlockFormatType::GetaudioBlockFormatID() const
{
	return m_audioBlockFormatID;
}

bool EbuCoreaudioBlockFormatType::ExistaudioBlockFormatID() const
{
	return m_audioBlockFormatID_Exist;
}
void EbuCoreaudioBlockFormatType::SetaudioBlockFormatID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::SetaudioBlockFormatID().");
	}
	m_audioBlockFormatID = item;
	m_audioBlockFormatID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::InvalidateaudioBlockFormatID()
{
	m_audioBlockFormatID_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioBlockFormatType::Getrtime() const
{
	return m_rtime;
}

bool EbuCoreaudioBlockFormatType::Existrtime() const
{
	return m_rtime_Exist;
}
void EbuCoreaudioBlockFormatType::Setrtime(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setrtime().");
	}
	m_rtime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_rtime) m_rtime->SetParent(m_myself.getPointer());
	m_rtime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatertime()
{
	m_rtime_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioBlockFormatType::Getduration() const
{
	return m_duration;
}

bool EbuCoreaudioBlockFormatType::Existduration() const
{
	return m_duration_Exist;
}
void EbuCoreaudioBlockFormatType::Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setduration().");
	}
	m_duration = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_duration) m_duration->SetParent(m_myself.getPointer());
	m_duration_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidateduration()
{
	m_duration_Exist = false;
}
EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr EbuCoreaudioBlockFormatType::GetspeakerLabel() const
{
		return m_speakerLabel;
}

EbuCoreaudioBlockFormatType_position_CollectionPtr EbuCoreaudioBlockFormatType::Getposition() const
{
		return m_position;
}

EbuCorematrixPtr EbuCoreaudioBlockFormatType::Getmatrix() const
{
		return m_matrix;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidmatrix() const
{
	return m_matrix_Exist;
}

float EbuCoreaudioBlockFormatType::Getgain() const
{
		return m_gain;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidgain() const
{
	return m_gain_Exist;
}

bool EbuCoreaudioBlockFormatType::Getdiffuse() const
{
		return m_diffuse;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValiddiffuse() const
{
	return m_diffuse_Exist;
}

float EbuCoreaudioBlockFormatType::Getwidth() const
{
		return m_width;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidwidth() const
{
	return m_width_Exist;
}

float EbuCoreaudioBlockFormatType::Getheight() const
{
		return m_height;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidheight() const
{
	return m_height_Exist;
}

float EbuCoreaudioBlockFormatType::Getdepth() const
{
		return m_depth;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValiddepth() const
{
	return m_depth_Exist;
}

bool EbuCoreaudioBlockFormatType::GetchannelLock() const
{
		return m_channelLock;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidchannelLock() const
{
	return m_channelLock_Exist;
}

bool EbuCoreaudioBlockFormatType::GetjumpPosition() const
{
		return m_jumpPosition;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidjumpPosition() const
{
	return m_jumpPosition_Exist;
}

XMLCh * EbuCoreaudioBlockFormatType::Getequation() const
{
		return m_equation;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidequation() const
{
	return m_equation_Exist;
}

int EbuCoreaudioBlockFormatType::Getdegree() const
{
		return m_degree;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValiddegree() const
{
	return m_degree_Exist;
}

int EbuCoreaudioBlockFormatType::Getorder() const
{
		return m_order;
}

// Element is optional
bool EbuCoreaudioBlockFormatType::IsValidorder() const
{
	return m_order_Exist;
}

void EbuCoreaudioBlockFormatType::SetspeakerLabel(const EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::SetspeakerLabel().");
	}
	if (m_speakerLabel != item)
	{
		// Dc1Factory::DeleteObject(m_speakerLabel);
		m_speakerLabel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_speakerLabel) m_speakerLabel->SetParent(m_myself.getPointer());
		if(m_speakerLabel != EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr())
		{
			m_speakerLabel->SetContentName(XMLString::transcode("speakerLabel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Setposition(const EbuCoreaudioBlockFormatType_position_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setposition().");
	}
	if (m_position != item)
	{
		// Dc1Factory::DeleteObject(m_position);
		m_position = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_position) m_position->SetParent(m_myself.getPointer());
		if(m_position != EbuCoreaudioBlockFormatType_position_CollectionPtr())
		{
			m_position->SetContentName(XMLString::transcode("position"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Setmatrix(const EbuCorematrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setmatrix().");
	}
	if (m_matrix != item || m_matrix_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_matrix);
		m_matrix = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_matrix) m_matrix->SetParent(m_myself.getPointer());
		if (m_matrix && m_matrix->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:matrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_matrix->UseTypeAttribute = true;
		}
		m_matrix_Exist = true;
		if(m_matrix != EbuCorematrixPtr())
		{
			m_matrix->SetContentName(XMLString::transcode("matrix"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatematrix()
{
	m_matrix_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setgain(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setgain().");
	}
	if (m_gain != item || m_gain_Exist == false)
	{
		m_gain = item;
		m_gain_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidategain()
{
	m_gain_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setdiffuse(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setdiffuse().");
	}
	if (m_diffuse != item || m_diffuse_Exist == false)
	{
		m_diffuse = item;
		m_diffuse_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatediffuse()
{
	m_diffuse_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setwidth(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setwidth().");
	}
	if (m_width != item || m_width_Exist == false)
	{
		m_width = item;
		m_width_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatewidth()
{
	m_width_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setheight(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setheight().");
	}
	if (m_height != item || m_height_Exist == false)
	{
		m_height = item;
		m_height_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidateheight()
{
	m_height_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setdepth(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setdepth().");
	}
	if (m_depth != item || m_depth_Exist == false)
	{
		m_depth = item;
		m_depth_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatedepth()
{
	m_depth_Exist = false;
}
void EbuCoreaudioBlockFormatType::SetchannelLock(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::SetchannelLock().");
	}
	if (m_channelLock != item || m_channelLock_Exist == false)
	{
		m_channelLock = item;
		m_channelLock_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::InvalidatechannelLock()
{
	m_channelLock_Exist = false;
}
void EbuCoreaudioBlockFormatType::SetjumpPosition(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::SetjumpPosition().");
	}
	if (m_jumpPosition != item || m_jumpPosition_Exist == false)
	{
		m_jumpPosition = item;
		m_jumpPosition_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::InvalidatejumpPosition()
{
	m_jumpPosition_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setequation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setequation().");
	}
	if (m_equation != item || m_equation_Exist == false)
	{
		XMLString::release(&m_equation);
		m_equation = item;
		m_equation_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidateequation()
{
	m_equation_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setdegree(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setdegree().");
	}
	if (m_degree != item || m_degree_Exist == false)
	{
		m_degree = item;
		m_degree_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidatedegree()
{
	m_degree_Exist = false;
}
void EbuCoreaudioBlockFormatType::Setorder(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioBlockFormatType::Setorder().");
	}
	if (m_order != item || m_order_Exist == false)
	{
		m_order = item;
		m_order_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioBlockFormatType::Invalidateorder()
{
	m_order_Exist = false;
}

Dc1NodeEnum EbuCoreaudioBlockFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioBlockFormatID")) == 0)
	{
		// audioBlockFormatID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("rtime")) == 0)
	{
		// rtime is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("duration")) == 0)
	{
		// duration is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("speakerLabel")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:speakerLabel is item of type audioBlockFormatType_speakerLabel_LocalType
		// in element collection audioBlockFormatType_speakerLabel_CollectionType
		
		context->Found = true;
		EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr coll = GetspeakerLabel();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioBlockFormatType_speakerLabel_CollectionType; // FTT, check this
				SetspeakerLabel(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_speakerLabel_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioBlockFormatType_speakerLabel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("position")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:position is item of type position_LocalType
		// in element collection audioBlockFormatType_position_CollectionType
		
		context->Found = true;
		EbuCoreaudioBlockFormatType_position_CollectionPtr coll = Getposition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioBlockFormatType_position_CollectionType; // FTT, check this
				Setposition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:position_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreposition_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("matrix")) == 0)
	{
		// matrix is simple element matrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getmatrix()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:matrixType")))) != empty)
			{
				// Is type allowed
				EbuCorematrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setmatrix(p, client);
					if((p = Getmatrix()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioBlockFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioBlockFormatType");
	}
	return result;
}

XMLCh * EbuCoreaudioBlockFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioBlockFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioBlockFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioBlockFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioBlockFormatID_Exist)
	{
	// String
	if(m_audioBlockFormatID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioBlockFormatID"), m_audioBlockFormatID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_rtime_Exist)
	{
	// Pattern
	if (m_rtime != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_rtime->ToText();
		element->setAttributeNS(X(""), X("rtime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_duration_Exist)
	{
	// Pattern
	if (m_duration != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_duration->ToText();
		element->setAttributeNS(X(""), X("duration"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_speakerLabel != EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_speakerLabel->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_position != EbuCoreaudioBlockFormatType_position_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_position->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_matrix != EbuCorematrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_matrix->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("matrix"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_matrix->Serialize(doc, element, &elem);
	}
	if(m_gain_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_gain);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("gain"), true);
		XMLString::release(&tmp);
	}
	if(m_diffuse_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_diffuse);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("diffuse"), true);
		XMLString::release(&tmp);
	}
	if(m_width_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_width);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("width"), true);
		XMLString::release(&tmp);
	}
	if(m_height_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_height);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("height"), true);
		XMLString::release(&tmp);
	}
	if(m_depth_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_depth);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("depth"), true);
		XMLString::release(&tmp);
	}
	if(m_channelLock_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_channelLock);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("channelLock"), true);
		XMLString::release(&tmp);
	}
	if(m_jumpPosition_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_jumpPosition);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("jumpPosition"), true);
		XMLString::release(&tmp);
	}
	 // String
	if(m_equation != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_equation, X("urn:ebu:metadata-schema:ebuCore_2014"), X("equation"), true);
	if(m_degree_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_degree);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("degree"), true);
		XMLString::release(&tmp);
	}
	if(m_order_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_order);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("order"), true);
		XMLString::release(&tmp);
	}

}

bool EbuCoreaudioBlockFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioBlockFormatID")))
	{
		// Deserialize string type
		this->SetaudioBlockFormatID(Dc1Convert::TextToString(parent->getAttribute(X("audioBlockFormatID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("rtime")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("rtime")));
		this->Setrtime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("duration")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("duration")));
		this->Setduration(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("speakerLabel"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("speakerLabel")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr tmp = CreateaudioBlockFormatType_speakerLabel_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetspeakerLabel(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("position"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("position")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioBlockFormatType_position_CollectionPtr tmp = CreateaudioBlockFormatType_position_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setposition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("matrix"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("matrix")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatematrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setmatrix(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("gain"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setgain(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("diffuse"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setdiffuse(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("width"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setwidth(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("height"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setheight(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("depth"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setdepth(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("channelLock"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetchannelLock(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("jumpPosition"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetjumpPosition(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("equation"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->Setequation(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setequation(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("degree"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setdegree(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("order"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->Setorder(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioBlockFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("speakerLabel")) == 0))
  {
	EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr tmp = CreateaudioBlockFormatType_speakerLabel_CollectionType; // FTT, check this
	this->SetspeakerLabel(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("position")) == 0))
  {
	EbuCoreaudioBlockFormatType_position_CollectionPtr tmp = CreateaudioBlockFormatType_position_CollectionType; // FTT, check this
	this->Setposition(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("matrix")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatematrixType; // FTT, check this
	}
	this->Setmatrix(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioBlockFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetspeakerLabel() != Dc1NodePtr())
		result.Insert(GetspeakerLabel());
	if (Getposition() != Dc1NodePtr())
		result.Insert(Getposition());
	if (Getmatrix() != Dc1NodePtr())
		result.Insert(Getmatrix());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMethodImpl.h


