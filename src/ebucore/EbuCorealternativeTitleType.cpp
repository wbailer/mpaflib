
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCorealternativeTitleType_title_CollectionType.h"
#include "EbuCorealternativeTitleType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:title

#include <assert.h>
IEbuCorealternativeTitleType::IEbuCorealternativeTitleType()
{

// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtPropInit.h

}

IEbuCorealternativeTitleType::~IEbuCorealternativeTitleType()
{
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtPropCleanup.h

}

EbuCorealternativeTitleType::EbuCorealternativeTitleType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorealternativeTitleType::~EbuCorealternativeTitleType()
{
	Cleanup();
}

void EbuCorealternativeTitleType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_statusLabel = NULL; // String
	m_statusLabel_Exist = false;
	m_statusDefinition = NULL; // String
	m_statusDefinition_Exist = false;
	m_statusLink = NULL; // String
	m_statusLink_Exist = false;
	m_statusSource = NULL; // String
	m_statusSource_Exist = false;
	m_statusLanguage = NULL; // String
	m_statusLanguage_Exist = false;
	m_startYear = 2015; // Value
	m_startYear_Exist = false;
	m_startDate = W3CdatePtr(); // Pattern
	m_startDate_Exist = false;
	m_startTime = W3CtimePtr(); // Pattern
	m_startTime_Exist = false;
	m_endYear = 2015; // Value
	m_endYear_Exist = false;
	m_endDate = W3CdatePtr(); // Pattern
	m_endDate_Exist = false;
	m_endTime = W3CtimePtr(); // Pattern
	m_endTime_Exist = false;
	m_period = NULL; // String
	m_period_Exist = false;
	m_length = 1; // Value
	m_length_Exist = false;
	m_geographicalScope = NULL; // String
	m_geographicalScope_Exist = false;
	m_geographicalExclusionScope = NULL; // String
	m_geographicalExclusionScope_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_title = EbuCorealternativeTitleType_title_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMyPropInit.h

}

void EbuCorealternativeTitleType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_statusLabel); // String
	XMLString::release(&m_statusDefinition); // String
	XMLString::release(&m_statusLink); // String
	XMLString::release(&m_statusSource); // String
	XMLString::release(&m_statusLanguage); // String
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	XMLString::release(&m_period); // String
	XMLString::release(&m_geographicalScope); // String
	XMLString::release(&m_geographicalExclusionScope); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_title);
}

void EbuCorealternativeTitleType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use alternativeTitleTypePtr, since we
	// might need GetBase(), which isn't defined in IalternativeTitleType
	const Dc1Ptr< EbuCorealternativeTitleType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_statusLabel); // String
	if (tmp->ExiststatusLabel())
	{
		this->SetstatusLabel(XMLString::replicate(tmp->GetstatusLabel()));
	}
	else
	{
		InvalidatestatusLabel();
	}
	}
	{
	XMLString::release(&m_statusDefinition); // String
	if (tmp->ExiststatusDefinition())
	{
		this->SetstatusDefinition(XMLString::replicate(tmp->GetstatusDefinition()));
	}
	else
	{
		InvalidatestatusDefinition();
	}
	}
	{
	XMLString::release(&m_statusLink); // String
	if (tmp->ExiststatusLink())
	{
		this->SetstatusLink(XMLString::replicate(tmp->GetstatusLink()));
	}
	else
	{
		InvalidatestatusLink();
	}
	}
	{
	XMLString::release(&m_statusSource); // String
	if (tmp->ExiststatusSource())
	{
		this->SetstatusSource(XMLString::replicate(tmp->GetstatusSource()));
	}
	else
	{
		InvalidatestatusSource();
	}
	}
	{
	XMLString::release(&m_statusLanguage); // String
	if (tmp->ExiststatusLanguage())
	{
		this->SetstatusLanguage(XMLString::replicate(tmp->GetstatusLanguage()));
	}
	else
	{
		InvalidatestatusLanguage();
	}
	}
	{
	if (tmp->ExiststartYear())
	{
		this->SetstartYear(tmp->GetstartYear());
	}
	else
	{
		InvalidatestartYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	if (tmp->ExiststartDate())
	{
		this->SetstartDate(Dc1Factory::CloneObject(tmp->GetstartDate()));
	}
	else
	{
		InvalidatestartDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	if (tmp->ExiststartTime())
	{
		this->SetstartTime(Dc1Factory::CloneObject(tmp->GetstartTime()));
	}
	else
	{
		InvalidatestartTime();
	}
	}
	{
	if (tmp->ExistendYear())
	{
		this->SetendYear(tmp->GetendYear());
	}
	else
	{
		InvalidateendYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	if (tmp->ExistendDate())
	{
		this->SetendDate(Dc1Factory::CloneObject(tmp->GetendDate()));
	}
	else
	{
		InvalidateendDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	if (tmp->ExistendTime())
	{
		this->SetendTime(Dc1Factory::CloneObject(tmp->GetendTime()));
	}
	else
	{
		InvalidateendTime();
	}
	}
	{
	XMLString::release(&m_period); // String
	if (tmp->Existperiod())
	{
		this->Setperiod(XMLString::replicate(tmp->Getperiod()));
	}
	else
	{
		Invalidateperiod();
	}
	}
	{
	if (tmp->Existlength())
	{
		this->Setlength(tmp->Getlength());
	}
	else
	{
		Invalidatelength();
	}
	}
	{
	XMLString::release(&m_geographicalScope); // String
	if (tmp->ExistgeographicalScope())
	{
		this->SetgeographicalScope(XMLString::replicate(tmp->GetgeographicalScope()));
	}
	else
	{
		InvalidategeographicalScope();
	}
	}
	{
	XMLString::release(&m_geographicalExclusionScope); // String
	if (tmp->ExistgeographicalExclusionScope())
	{
		this->SetgeographicalExclusionScope(XMLString::replicate(tmp->GetgeographicalExclusionScope()));
	}
	else
	{
		InvalidategeographicalExclusionScope();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_title);
		this->Settitle(Dc1Factory::CloneObject(tmp->Gettitle()));
}

Dc1NodePtr EbuCorealternativeTitleType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorealternativeTitleType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorealternativeTitleType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorealternativeTitleType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorealternativeTitleType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorealternativeTitleType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorealternativeTitleType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorealternativeTitleType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorealternativeTitleType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorealternativeTitleType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorealternativeTitleType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorealternativeTitleType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorealternativeTitleType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetstatusLabel() const
{
	return m_statusLabel;
}

bool EbuCorealternativeTitleType::ExiststatusLabel() const
{
	return m_statusLabel_Exist;
}
void EbuCorealternativeTitleType::SetstatusLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstatusLabel().");
	}
	m_statusLabel = item;
	m_statusLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestatusLabel()
{
	m_statusLabel_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetstatusDefinition() const
{
	return m_statusDefinition;
}

bool EbuCorealternativeTitleType::ExiststatusDefinition() const
{
	return m_statusDefinition_Exist;
}
void EbuCorealternativeTitleType::SetstatusDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstatusDefinition().");
	}
	m_statusDefinition = item;
	m_statusDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestatusDefinition()
{
	m_statusDefinition_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetstatusLink() const
{
	return m_statusLink;
}

bool EbuCorealternativeTitleType::ExiststatusLink() const
{
	return m_statusLink_Exist;
}
void EbuCorealternativeTitleType::SetstatusLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstatusLink().");
	}
	m_statusLink = item;
	m_statusLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestatusLink()
{
	m_statusLink_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetstatusSource() const
{
	return m_statusSource;
}

bool EbuCorealternativeTitleType::ExiststatusSource() const
{
	return m_statusSource_Exist;
}
void EbuCorealternativeTitleType::SetstatusSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstatusSource().");
	}
	m_statusSource = item;
	m_statusSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestatusSource()
{
	m_statusSource_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetstatusLanguage() const
{
	return m_statusLanguage;
}

bool EbuCorealternativeTitleType::ExiststatusLanguage() const
{
	return m_statusLanguage_Exist;
}
void EbuCorealternativeTitleType::SetstatusLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstatusLanguage().");
	}
	m_statusLanguage = item;
	m_statusLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestatusLanguage()
{
	m_statusLanguage_Exist = false;
}
int EbuCorealternativeTitleType::GetstartYear() const
{
	return m_startYear;
}

bool EbuCorealternativeTitleType::ExiststartYear() const
{
	return m_startYear_Exist;
}
void EbuCorealternativeTitleType::SetstartYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstartYear().");
	}
	m_startYear = item;
	m_startYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestartYear()
{
	m_startYear_Exist = false;
}
W3CdatePtr EbuCorealternativeTitleType::GetstartDate() const
{
	return m_startDate;
}

bool EbuCorealternativeTitleType::ExiststartDate() const
{
	return m_startDate_Exist;
}
void EbuCorealternativeTitleType::SetstartDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstartDate().");
	}
	m_startDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startDate) m_startDate->SetParent(m_myself.getPointer());
	m_startDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestartDate()
{
	m_startDate_Exist = false;
}
W3CtimePtr EbuCorealternativeTitleType::GetstartTime() const
{
	return m_startTime;
}

bool EbuCorealternativeTitleType::ExiststartTime() const
{
	return m_startTime_Exist;
}
void EbuCorealternativeTitleType::SetstartTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetstartTime().");
	}
	m_startTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startTime) m_startTime->SetParent(m_myself.getPointer());
	m_startTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidatestartTime()
{
	m_startTime_Exist = false;
}
int EbuCorealternativeTitleType::GetendYear() const
{
	return m_endYear;
}

bool EbuCorealternativeTitleType::ExistendYear() const
{
	return m_endYear_Exist;
}
void EbuCorealternativeTitleType::SetendYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetendYear().");
	}
	m_endYear = item;
	m_endYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidateendYear()
{
	m_endYear_Exist = false;
}
W3CdatePtr EbuCorealternativeTitleType::GetendDate() const
{
	return m_endDate;
}

bool EbuCorealternativeTitleType::ExistendDate() const
{
	return m_endDate_Exist;
}
void EbuCorealternativeTitleType::SetendDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetendDate().");
	}
	m_endDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endDate) m_endDate->SetParent(m_myself.getPointer());
	m_endDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidateendDate()
{
	m_endDate_Exist = false;
}
W3CtimePtr EbuCorealternativeTitleType::GetendTime() const
{
	return m_endTime;
}

bool EbuCorealternativeTitleType::ExistendTime() const
{
	return m_endTime_Exist;
}
void EbuCorealternativeTitleType::SetendTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetendTime().");
	}
	m_endTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endTime) m_endTime->SetParent(m_myself.getPointer());
	m_endTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidateendTime()
{
	m_endTime_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::Getperiod() const
{
	return m_period;
}

bool EbuCorealternativeTitleType::Existperiod() const
{
	return m_period_Exist;
}
void EbuCorealternativeTitleType::Setperiod(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::Setperiod().");
	}
	m_period = item;
	m_period_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::Invalidateperiod()
{
	m_period_Exist = false;
}
unsigned EbuCorealternativeTitleType::Getlength() const
{
	return m_length;
}

bool EbuCorealternativeTitleType::Existlength() const
{
	return m_length_Exist;
}
void EbuCorealternativeTitleType::Setlength(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::Setlength().");
	}
	m_length = item;
	m_length_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::Invalidatelength()
{
	m_length_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetgeographicalScope() const
{
	return m_geographicalScope;
}

bool EbuCorealternativeTitleType::ExistgeographicalScope() const
{
	return m_geographicalScope_Exist;
}
void EbuCorealternativeTitleType::SetgeographicalScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetgeographicalScope().");
	}
	m_geographicalScope = item;
	m_geographicalScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidategeographicalScope()
{
	m_geographicalScope_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::GetgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope;
}

bool EbuCorealternativeTitleType::ExistgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope_Exist;
}
void EbuCorealternativeTitleType::SetgeographicalExclusionScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::SetgeographicalExclusionScope().");
	}
	m_geographicalExclusionScope = item;
	m_geographicalExclusionScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::InvalidategeographicalExclusionScope()
{
	m_geographicalExclusionScope_Exist = false;
}
XMLCh * EbuCorealternativeTitleType::Getnote() const
{
	return m_note;
}

bool EbuCorealternativeTitleType::Existnote() const
{
	return m_note_Exist;
}
void EbuCorealternativeTitleType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorealternativeTitleType::Invalidatenote()
{
	m_note_Exist = false;
}
EbuCorealternativeTitleType_title_CollectionPtr EbuCorealternativeTitleType::Gettitle() const
{
		return m_title;
}

void EbuCorealternativeTitleType::Settitle(const EbuCorealternativeTitleType_title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorealternativeTitleType::Settitle().");
	}
	if (m_title != item)
	{
		// Dc1Factory::DeleteObject(m_title);
		m_title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_title) m_title->SetParent(m_myself.getPointer());
		if(m_title != EbuCorealternativeTitleType_title_CollectionPtr())
		{
			m_title->SetContentName(XMLString::transcode("title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorealternativeTitleType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  21, 21 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("statusLabel")) == 0)
	{
		// statusLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("statusDefinition")) == 0)
	{
		// statusDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("statusLink")) == 0)
	{
		// statusLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("statusSource")) == 0)
	{
		// statusSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("statusLanguage")) == 0)
	{
		// statusLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startYear")) == 0)
	{
		// startYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startDate")) == 0)
	{
		// startDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startTime")) == 0)
	{
		// startTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endYear")) == 0)
	{
		// endYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endDate")) == 0)
	{
		// endDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endTime")) == 0)
	{
		// endTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("period")) == 0)
	{
		// period is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("length")) == 0)
	{
		// length is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalScope")) == 0)
	{
		// geographicalScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalExclusionScope")) == 0)
	{
		// geographicalExclusionScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("title")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:title is item of type elementType
		// in element collection alternativeTitleType_title_CollectionType
		
		context->Found = true;
		EbuCorealternativeTitleType_title_CollectionPtr coll = Gettitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatealternativeTitleType_title_CollectionType; // FTT, check this
				Settitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for alternativeTitleType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "alternativeTitleType");
	}
	return result;
}

XMLCh * EbuCorealternativeTitleType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorealternativeTitleType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorealternativeTitleType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("alternativeTitleType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_statusLabel_Exist)
	{
	// String
	if(m_statusLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("statusLabel"), m_statusLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_statusDefinition_Exist)
	{
	// String
	if(m_statusDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("statusDefinition"), m_statusDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_statusLink_Exist)
	{
	// String
	if(m_statusLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("statusLink"), m_statusLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_statusSource_Exist)
	{
	// String
	if(m_statusSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("statusSource"), m_statusSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_statusLanguage_Exist)
	{
	// String
	if(m_statusLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("statusLanguage"), m_statusLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_startYear);
		element->setAttributeNS(X(""), X("startYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startDate_Exist)
	{
	// Pattern
	if (m_startDate != W3CdatePtr())
	{
		XMLCh * tmp = m_startDate->ToText();
		element->setAttributeNS(X(""), X("startDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startTime_Exist)
	{
	// Pattern
	if (m_startTime != W3CtimePtr())
	{
		XMLCh * tmp = m_startTime->ToText();
		element->setAttributeNS(X(""), X("startTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_endYear);
		element->setAttributeNS(X(""), X("endYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endDate_Exist)
	{
	// Pattern
	if (m_endDate != W3CdatePtr())
	{
		XMLCh * tmp = m_endDate->ToText();
		element->setAttributeNS(X(""), X("endDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endTime_Exist)
	{
	// Pattern
	if (m_endTime != W3CtimePtr())
	{
		XMLCh * tmp = m_endTime->ToText();
		element->setAttributeNS(X(""), X("endTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_period_Exist)
	{
	// String
	if(m_period != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("period"), m_period);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_length_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_length);
		element->setAttributeNS(X(""), X("length"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalScope_Exist)
	{
	// String
	if(m_geographicalScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalScope"), m_geographicalScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalExclusionScope_Exist)
	{
	// String
	if(m_geographicalExclusionScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalExclusionScope"), m_geographicalExclusionScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_title != EbuCorealternativeTitleType_title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_title->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorealternativeTitleType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("statusLabel")))
	{
		// Deserialize string type
		this->SetstatusLabel(Dc1Convert::TextToString(parent->getAttribute(X("statusLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("statusDefinition")))
	{
		// Deserialize string type
		this->SetstatusDefinition(Dc1Convert::TextToString(parent->getAttribute(X("statusDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("statusLink")))
	{
		// Deserialize string type
		this->SetstatusLink(Dc1Convert::TextToString(parent->getAttribute(X("statusLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("statusSource")))
	{
		// Deserialize string type
		this->SetstatusSource(Dc1Convert::TextToString(parent->getAttribute(X("statusSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("statusLanguage")))
	{
		// Deserialize string type
		this->SetstatusLanguage(Dc1Convert::TextToString(parent->getAttribute(X("statusLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startYear")))
	{
		// deserialize value type
		this->SetstartYear(Dc1Convert::TextToInt(parent->getAttribute(X("startYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startDate")));
		this->SetstartDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startTime")));
		this->SetstartTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endYear")))
	{
		// deserialize value type
		this->SetendYear(Dc1Convert::TextToInt(parent->getAttribute(X("endYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endDate")));
		this->SetendDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endTime")));
		this->SetendTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("period")))
	{
		// Deserialize string type
		this->Setperiod(Dc1Convert::TextToString(parent->getAttribute(X("period"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("length")))
	{
		// deserialize value type
		this->Setlength(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("length"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalScope")))
	{
		// Deserialize string type
		this->SetgeographicalScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalExclusionScope")))
	{
		// Deserialize string type
		this->SetgeographicalExclusionScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalExclusionScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("title"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("title")) == 0))
		{
			// Deserialize factory type
			EbuCorealternativeTitleType_title_CollectionPtr tmp = CreatealternativeTitleType_title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorealternativeTitleType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("title")) == 0))
  {
	EbuCorealternativeTitleType_title_CollectionPtr tmp = CreatealternativeTitleType_title_CollectionType; // FTT, check this
	this->Settitle(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorealternativeTitleType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettitle() != Dc1NodePtr())
		result.Insert(Gettitle());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMethodImpl.h


