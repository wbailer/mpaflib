
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtImplInclude.h


#include "EbuCorePackageIDType.h"
#include "EbuCoreaudioMXFLookUpType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioMXFLookUpType::IEbuCoreaudioMXFLookUpType()
{

// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtPropInit.h

}

IEbuCoreaudioMXFLookUpType::~IEbuCoreaudioMXFLookUpType()
{
// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtPropCleanup.h

}

EbuCoreaudioMXFLookUpType::EbuCoreaudioMXFLookUpType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioMXFLookUpType::~EbuCoreaudioMXFLookUpType()
{
	Cleanup();
}

void EbuCoreaudioMXFLookUpType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_packageUIDRef = EbuCorePackageIDPtr(); // Pattern
	m_trackIDRef = XMLString::transcode(""); //  Mandatory String
	m_channelIDRef = XMLString::transcode(""); //  Mandatory String


// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtMyPropInit.h

}

void EbuCoreaudioMXFLookUpType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_packageUIDRef);
	XMLString::release(&this->m_trackIDRef);
	this->m_trackIDRef = NULL;
	XMLString::release(&this->m_channelIDRef);
	this->m_channelIDRef = NULL;
}

void EbuCoreaudioMXFLookUpType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioMXFLookUpTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioMXFLookUpType
	const Dc1Ptr< EbuCoreaudioMXFLookUpType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_packageUIDRef);
		this->SetpackageUIDRef(Dc1Factory::CloneObject(tmp->GetpackageUIDRef()));
		this->SettrackIDRef(XMLString::replicate(tmp->GettrackIDRef()));
		this->SetchannelIDRef(XMLString::replicate(tmp->GetchannelIDRef()));
}

Dc1NodePtr EbuCoreaudioMXFLookUpType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioMXFLookUpType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


EbuCorePackageIDPtr EbuCoreaudioMXFLookUpType::GetpackageUIDRef() const
{
		return m_packageUIDRef;
}

XMLCh * EbuCoreaudioMXFLookUpType::GettrackIDRef() const
{
		return m_trackIDRef;
}

XMLCh * EbuCoreaudioMXFLookUpType::GetchannelIDRef() const
{
		return m_channelIDRef;
}

void EbuCoreaudioMXFLookUpType::SetpackageUIDRef(const EbuCorePackageIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioMXFLookUpType::SetpackageUIDRef().");
	}
	if (m_packageUIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_packageUIDRef);
		m_packageUIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_packageUIDRef) m_packageUIDRef->SetParent(m_myself.getPointer());
		if (m_packageUIDRef && m_packageUIDRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:PackageIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_packageUIDRef->UseTypeAttribute = true;
		}
		if(m_packageUIDRef != EbuCorePackageIDPtr())
		{
			m_packageUIDRef->SetContentName(XMLString::transcode("packageUIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioMXFLookUpType::SettrackIDRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioMXFLookUpType::SettrackIDRef().");
	}
	if (m_trackIDRef != item)
	{
		XMLString::release(&m_trackIDRef);
		m_trackIDRef = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioMXFLookUpType::SetchannelIDRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioMXFLookUpType::SetchannelIDRef().");
	}
	if (m_channelIDRef != item)
	{
		XMLString::release(&m_channelIDRef);
		m_channelIDRef = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioMXFLookUpType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("packageUIDRef")) == 0)
	{
		// packageUIDRef is simple element PackageIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetpackageUIDRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:PackageIDType")))) != empty)
			{
				// Is type allowed
				EbuCorePackageIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetpackageUIDRef(p, client);
					if((p = GetpackageUIDRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioMXFLookUpType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioMXFLookUpType");
	}
	return result;
}

XMLCh * EbuCoreaudioMXFLookUpType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioMXFLookUpType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioMXFLookUpType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioMXFLookUpType"));
	// Element serialization:
	if(m_packageUIDRef != EbuCorePackageIDPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("packageUIDRef");
//		m_packageUIDRef->SetContentName(contentname);
		m_packageUIDRef->UseTypeAttribute = this->UseTypeAttribute;
		m_packageUIDRef->Serialize(doc, element);
	}
	 // String
	if(m_trackIDRef != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_trackIDRef, X("urn:ebu:metadata-schema:ebuCore_2014"), X("trackIDRef"), false);
	 // String
	if(m_channelIDRef != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_channelIDRef, X("urn:ebu:metadata-schema:ebuCore_2014"), X("channelIDRef"), false);

}

bool EbuCoreaudioMXFLookUpType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("packageUIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("packageUIDRef")) == 0))
		{
			EbuCorePackageIDPtr tmp = CreatePackageIDType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetpackageUIDRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("trackIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SettrackIDRef(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SettrackIDRef(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("channelIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetchannelIDRef(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetchannelIDRef(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioMXFLookUpType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("packageUIDRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePackageIDType; // FTT, check this
	}
	this->SetpackageUIDRef(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioMXFLookUpType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetpackageUIDRef() != Dc1NodePtr())
		result.Insert(GetpackageUIDRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioMXFLookUpType_ExtMethodImpl.h


