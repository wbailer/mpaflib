
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoretypeType_ExtImplInclude.h


#include "EbuCoretypeType_type_CollectionType.h"
#include "EbuCoretypeType_genre_CollectionType.h"
#include "EbuCoretypeType_objectType_CollectionType.h"
#include "EbuCoretypeType_targetAudience_CollectionType.h"
#include "EbuCoretypeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:type
#include "EbuCoretypeType_genre_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:genre
#include "EbuCoretypeType_objectType_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:objectType
#include "EbuCoretargetAudienceType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:targetAudience

#include <assert.h>
IEbuCoretypeType::IEbuCoretypeType()
{

// no includefile for extension defined 
// file EbuCoretypeType_ExtPropInit.h

}

IEbuCoretypeType::~IEbuCoretypeType()
{
// no includefile for extension defined 
// file EbuCoretypeType_ExtPropCleanup.h

}

EbuCoretypeType::EbuCoretypeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoretypeType::~EbuCoretypeType()
{
	Cleanup();
}

void EbuCoretypeType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_type = EbuCoretypeType_type_CollectionPtr(); // Collection
	m_genre = EbuCoretypeType_genre_CollectionPtr(); // Collection
	m_objectType = EbuCoretypeType_objectType_CollectionPtr(); // Collection
	m_targetAudience = EbuCoretypeType_targetAudience_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoretypeType_ExtMyPropInit.h

}

void EbuCoretypeType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretypeType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_genre);
	// Dc1Factory::DeleteObject(m_objectType);
	// Dc1Factory::DeleteObject(m_targetAudience);
}

void EbuCoretypeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use typeTypePtr, since we
	// might need GetBase(), which isn't defined in ItypeType
	const Dc1Ptr< EbuCoretypeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_type);
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
		// Dc1Factory::DeleteObject(m_genre);
		this->Setgenre(Dc1Factory::CloneObject(tmp->Getgenre()));
		// Dc1Factory::DeleteObject(m_objectType);
		this->SetobjectType(Dc1Factory::CloneObject(tmp->GetobjectType()));
		// Dc1Factory::DeleteObject(m_targetAudience);
		this->SettargetAudience(Dc1Factory::CloneObject(tmp->GettargetAudience()));
}

Dc1NodePtr EbuCoretypeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretypeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoretypeType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoretypeType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoretypeType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoretypeType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoretypeType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoretypeType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoretypeType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoretypeType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoretypeType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoretypeType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoretypeType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoretypeType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoretypeType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoretypeType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoretypeType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoretypeType::Getnote() const
{
	return m_note;
}

bool EbuCoretypeType::Existnote() const
{
	return m_note_Exist;
}
void EbuCoretypeType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::Invalidatenote()
{
	m_note_Exist = false;
}
EbuCoretypeType_type_CollectionPtr EbuCoretypeType::Gettype() const
{
		return m_type;
}

EbuCoretypeType_genre_CollectionPtr EbuCoretypeType::Getgenre() const
{
		return m_genre;
}

EbuCoretypeType_objectType_CollectionPtr EbuCoretypeType::GetobjectType() const
{
		return m_objectType;
}

EbuCoretypeType_targetAudience_CollectionPtr EbuCoretypeType::GettargetAudience() const
{
		return m_targetAudience;
}

void EbuCoretypeType::Settype(const EbuCoretypeType_type_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::Settype().");
	}
	if (m_type != item)
	{
		// Dc1Factory::DeleteObject(m_type);
		m_type = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_type) m_type->SetParent(m_myself.getPointer());
		if(m_type != EbuCoretypeType_type_CollectionPtr())
		{
			m_type->SetContentName(XMLString::transcode("type"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::Setgenre(const EbuCoretypeType_genre_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::Setgenre().");
	}
	if (m_genre != item)
	{
		// Dc1Factory::DeleteObject(m_genre);
		m_genre = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_genre) m_genre->SetParent(m_myself.getPointer());
		if(m_genre != EbuCoretypeType_genre_CollectionPtr())
		{
			m_genre->SetContentName(XMLString::transcode("genre"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::SetobjectType(const EbuCoretypeType_objectType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SetobjectType().");
	}
	if (m_objectType != item)
	{
		// Dc1Factory::DeleteObject(m_objectType);
		m_objectType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_objectType) m_objectType->SetParent(m_myself.getPointer());
		if(m_objectType != EbuCoretypeType_objectType_CollectionPtr())
		{
			m_objectType->SetContentName(XMLString::transcode("objectType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretypeType::SettargetAudience(const EbuCoretypeType_targetAudience_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretypeType::SettargetAudience().");
	}
	if (m_targetAudience != item)
	{
		// Dc1Factory::DeleteObject(m_targetAudience);
		m_targetAudience = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_targetAudience) m_targetAudience->SetParent(m_myself.getPointer());
		if(m_targetAudience != EbuCoretypeType_targetAudience_CollectionPtr())
		{
			m_targetAudience->SetContentName(XMLString::transcode("targetAudience"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoretypeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  6, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:type is item of type elementType
		// in element collection typeType_type_CollectionType
		
		context->Found = true;
		EbuCoretypeType_type_CollectionPtr coll = Gettype();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetypeType_type_CollectionType; // FTT, check this
				Settype(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("genre")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:genre is item of type typeType_genre_LocalType
		// in element collection typeType_genre_CollectionType
		
		context->Found = true;
		EbuCoretypeType_genre_CollectionPtr coll = Getgenre();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetypeType_genre_CollectionType; // FTT, check this
				Setgenre(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_genre_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoretypeType_genre_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("objectType")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:objectType is item of type typeType_objectType_LocalType
		// in element collection typeType_objectType_CollectionType
		
		context->Found = true;
		EbuCoretypeType_objectType_CollectionPtr coll = GetobjectType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetypeType_objectType_CollectionType; // FTT, check this
				SetobjectType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_objectType_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoretypeType_objectType_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("targetAudience")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:targetAudience is item of type targetAudienceType
		// in element collection typeType_targetAudience_CollectionType
		
		context->Found = true;
		EbuCoretypeType_targetAudience_CollectionPtr coll = GettargetAudience();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetypeType_targetAudience_CollectionType; // FTT, check this
				SettargetAudience(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType")))) != empty)
			{
				// Is type allowed
				EbuCoretargetAudiencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for typeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "typeType");
	}
	return result;
}

XMLCh * EbuCoretypeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoretypeType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoretypeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("typeType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_type != EbuCoretypeType_type_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_type->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_genre != EbuCoretypeType_genre_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_genre->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_objectType != EbuCoretypeType_objectType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_objectType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_targetAudience != EbuCoretypeType_targetAudience_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_targetAudience->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoretypeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("type"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("type")) == 0))
		{
			// Deserialize factory type
			EbuCoretypeType_type_CollectionPtr tmp = CreatetypeType_type_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Settype(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("genre"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("genre")) == 0))
		{
			// Deserialize factory type
			EbuCoretypeType_genre_CollectionPtr tmp = CreatetypeType_genre_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setgenre(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("objectType"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("objectType")) == 0))
		{
			// Deserialize factory type
			EbuCoretypeType_objectType_CollectionPtr tmp = CreatetypeType_objectType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetobjectType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("targetAudience"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("targetAudience")) == 0))
		{
			// Deserialize factory type
			EbuCoretypeType_targetAudience_CollectionPtr tmp = CreatetypeType_targetAudience_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettargetAudience(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoretypeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoretypeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("type")) == 0))
  {
	EbuCoretypeType_type_CollectionPtr tmp = CreatetypeType_type_CollectionType; // FTT, check this
	this->Settype(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("genre")) == 0))
  {
	EbuCoretypeType_genre_CollectionPtr tmp = CreatetypeType_genre_CollectionType; // FTT, check this
	this->Setgenre(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("objectType")) == 0))
  {
	EbuCoretypeType_objectType_CollectionPtr tmp = CreatetypeType_objectType_CollectionType; // FTT, check this
	this->SetobjectType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("targetAudience")) == 0))
  {
	EbuCoretypeType_targetAudience_CollectionPtr tmp = CreatetypeType_targetAudience_CollectionType; // FTT, check this
	this->SettargetAudience(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoretypeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettype() != Dc1NodePtr())
		result.Insert(Gettype());
	if (Getgenre() != Dc1NodePtr())
		result.Insert(Getgenre());
	if (GetobjectType() != Dc1NodePtr())
		result.Insert(GetobjectType());
	if (GettargetAudience() != Dc1NodePtr())
		result.Insert(GettargetAudience());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoretypeType_ExtMethodImpl.h


