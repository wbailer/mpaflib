
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtImplInclude.h


#include "EbuCoretimecodeType.h"
#include "EbuCoreaudioProgrammeType_audioContentIDRef_CollectionType.h"
#include "EbuCoreloudnessMetadataType.h"
#include "EbuCoreaudioProgrammeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioProgrammeType::IEbuCoreaudioProgrammeType()
{

// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtPropInit.h

}

IEbuCoreaudioProgrammeType::~IEbuCoreaudioProgrammeType()
{
// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtPropCleanup.h

}

EbuCoreaudioProgrammeType::EbuCoreaudioProgrammeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioProgrammeType::~EbuCoreaudioProgrammeType()
{
	Cleanup();
}

void EbuCoreaudioProgrammeType::Init()
{

	// Init attributes
	m_audioProgrammeID = NULL; // String
	m_audioProgrammeName = NULL; // String
	m_audioProgrammeName_Exist = false;
	m_audioProgrammeLanguage = NULL; // String
	m_audioProgrammeLanguage_Exist = false;
	m_start = EbuCoretimecodePtr(); // Pattern
	m_start_Exist = false;
	m_end = EbuCoretimecodePtr(); // Pattern
	m_end_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioContentIDRef = EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr(); // Collection
	m_loudnessMetadata = EbuCoreloudnessMetadataPtr(); // Class
	m_loudnessMetadata_Exist = false;


// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtMyPropInit.h

}

void EbuCoreaudioProgrammeType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtMyPropCleanup.h


	XMLString::release(&m_audioProgrammeID); // String
	XMLString::release(&m_audioProgrammeName); // String
	XMLString::release(&m_audioProgrammeLanguage); // String
	// Dc1Factory::DeleteObject(m_start); // Pattern
	// Dc1Factory::DeleteObject(m_end); // Pattern
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
	// Dc1Factory::DeleteObject(m_audioContentIDRef);
	// Dc1Factory::DeleteObject(m_loudnessMetadata);
}

void EbuCoreaudioProgrammeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioProgrammeTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioProgrammeType
	const Dc1Ptr< EbuCoreaudioProgrammeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioProgrammeID); // String
		this->SetaudioProgrammeID(XMLString::replicate(tmp->GetaudioProgrammeID()));
	}
	{
	XMLString::release(&m_audioProgrammeName); // String
	if (tmp->ExistaudioProgrammeName())
	{
		this->SetaudioProgrammeName(XMLString::replicate(tmp->GetaudioProgrammeName()));
	}
	else
	{
		InvalidateaudioProgrammeName();
	}
	}
	{
	XMLString::release(&m_audioProgrammeLanguage); // String
	if (tmp->ExistaudioProgrammeLanguage())
	{
		this->SetaudioProgrammeLanguage(XMLString::replicate(tmp->GetaudioProgrammeLanguage()));
	}
	else
	{
		InvalidateaudioProgrammeLanguage();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_start); // Pattern
	if (tmp->Existstart())
	{
		this->Setstart(Dc1Factory::CloneObject(tmp->Getstart()));
	}
	else
	{
		Invalidatestart();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_end); // Pattern
	if (tmp->Existend())
	{
		this->Setend(Dc1Factory::CloneObject(tmp->Getend()));
	}
	else
	{
		Invalidateend();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
		// Dc1Factory::DeleteObject(m_audioContentIDRef);
		this->SetaudioContentIDRef(Dc1Factory::CloneObject(tmp->GetaudioContentIDRef()));
	if (tmp->IsValidloudnessMetadata())
	{
		// Dc1Factory::DeleteObject(m_loudnessMetadata);
		this->SetloudnessMetadata(Dc1Factory::CloneObject(tmp->GetloudnessMetadata()));
	}
	else
	{
		InvalidateloudnessMetadata();
	}
}

Dc1NodePtr EbuCoreaudioProgrammeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioProgrammeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioProgrammeType::GetaudioProgrammeID() const
{
	return m_audioProgrammeID;
}

void EbuCoreaudioProgrammeType::SetaudioProgrammeID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetaudioProgrammeID().");
	}
	m_audioProgrammeID = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCoreaudioProgrammeType::GetaudioProgrammeName() const
{
	return m_audioProgrammeName;
}

bool EbuCoreaudioProgrammeType::ExistaudioProgrammeName() const
{
	return m_audioProgrammeName_Exist;
}
void EbuCoreaudioProgrammeType::SetaudioProgrammeName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetaudioProgrammeName().");
	}
	m_audioProgrammeName = item;
	m_audioProgrammeName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateaudioProgrammeName()
{
	m_audioProgrammeName_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetaudioProgrammeLanguage() const
{
	return m_audioProgrammeLanguage;
}

bool EbuCoreaudioProgrammeType::ExistaudioProgrammeLanguage() const
{
	return m_audioProgrammeLanguage_Exist;
}
void EbuCoreaudioProgrammeType::SetaudioProgrammeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetaudioProgrammeLanguage().");
	}
	m_audioProgrammeLanguage = item;
	m_audioProgrammeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateaudioProgrammeLanguage()
{
	m_audioProgrammeLanguage_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioProgrammeType::Getstart() const
{
	return m_start;
}

bool EbuCoreaudioProgrammeType::Existstart() const
{
	return m_start_Exist;
}
void EbuCoreaudioProgrammeType::Setstart(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::Setstart().");
	}
	m_start = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_start) m_start->SetParent(m_myself.getPointer());
	m_start_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::Invalidatestart()
{
	m_start_Exist = false;
}
EbuCoretimecodePtr EbuCoreaudioProgrammeType::Getend() const
{
	return m_end;
}

bool EbuCoreaudioProgrammeType::Existend() const
{
	return m_end_Exist;
}
void EbuCoreaudioProgrammeType::Setend(const EbuCoretimecodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::Setend().");
	}
	m_end = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_end) m_end->SetParent(m_myself.getPointer());
	m_end_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::Invalidateend()
{
	m_end_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreaudioProgrammeType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreaudioProgrammeType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreaudioProgrammeType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreaudioProgrammeType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreaudioProgrammeType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreaudioProgrammeType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreaudioProgrammeType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreaudioProgrammeType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreaudioProgrammeType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreaudioProgrammeType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCoreaudioProgrammeType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCoreaudioProgrammeType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCoreaudioProgrammeType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCoreaudioProgrammeType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCoreaudioProgrammeType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCoreaudioProgrammeType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCoreaudioProgrammeType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCoreaudioProgrammeType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCoreaudioProgrammeType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCoreaudioProgrammeType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCoreaudioProgrammeType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr EbuCoreaudioProgrammeType::GetaudioContentIDRef() const
{
		return m_audioContentIDRef;
}

EbuCoreloudnessMetadataPtr EbuCoreaudioProgrammeType::GetloudnessMetadata() const
{
		return m_loudnessMetadata;
}

// Element is optional
bool EbuCoreaudioProgrammeType::IsValidloudnessMetadata() const
{
	return m_loudnessMetadata_Exist;
}

void EbuCoreaudioProgrammeType::SetaudioContentIDRef(const EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetaudioContentIDRef().");
	}
	if (m_audioContentIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioContentIDRef);
		m_audioContentIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioContentIDRef) m_audioContentIDRef->SetParent(m_myself.getPointer());
		if(m_audioContentIDRef != EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr())
		{
			m_audioContentIDRef->SetContentName(XMLString::transcode("audioContentIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::SetloudnessMetadata(const EbuCoreloudnessMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioProgrammeType::SetloudnessMetadata().");
	}
	if (m_loudnessMetadata != item || m_loudnessMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_loudnessMetadata);
		m_loudnessMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_loudnessMetadata) m_loudnessMetadata->SetParent(m_myself.getPointer());
		if (m_loudnessMetadata && m_loudnessMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_loudnessMetadata->UseTypeAttribute = true;
		}
		m_loudnessMetadata_Exist = true;
		if(m_loudnessMetadata != EbuCoreloudnessMetadataPtr())
		{
			m_loudnessMetadata->SetContentName(XMLString::transcode("loudnessMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioProgrammeType::InvalidateloudnessMetadata()
{
	m_loudnessMetadata_Exist = false;
}

Dc1NodeEnum EbuCoreaudioProgrammeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  15, 15 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioProgrammeID")) == 0)
	{
		// audioProgrammeID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioProgrammeName")) == 0)
	{
		// audioProgrammeName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioProgrammeLanguage")) == 0)
	{
		// audioProgrammeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("end")) == 0)
	{
		// end is simple attribute timecodeType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "EbuCoretimecodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLabel")) == 0)
	{
		// formatLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatDefinition")) == 0)
	{
		// formatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLink")) == 0)
	{
		// formatLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatSource")) == 0)
	{
		// formatSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLanguage")) == 0)
	{
		// formatLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioContentIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("loudnessMetadata")) == 0)
	{
		// loudnessMetadata is simple element loudnessMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetloudnessMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType")))) != empty)
			{
				// Is type allowed
				EbuCoreloudnessMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetloudnessMetadata(p, client);
					if((p = GetloudnessMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioProgrammeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioProgrammeType");
	}
	return result;
}

XMLCh * EbuCoreaudioProgrammeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioProgrammeType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioProgrammeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioProgrammeType"));
	// Attribute Serialization:
	// String
	if(m_audioProgrammeID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioProgrammeID"), m_audioProgrammeID);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioProgrammeName_Exist)
	{
	// String
	if(m_audioProgrammeName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioProgrammeName"), m_audioProgrammeName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioProgrammeLanguage_Exist)
	{
	// String
	if(m_audioProgrammeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioProgrammeLanguage"), m_audioProgrammeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_start_Exist)
	{
	// Pattern
	if (m_start != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_start->ToText();
		element->setAttributeNS(X(""), X("start"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_end_Exist)
	{
	// Pattern
	if (m_end != EbuCoretimecodePtr())
	{
		XMLCh * tmp = m_end->ToText();
		element->setAttributeNS(X(""), X("end"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioContentIDRef != EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioContentIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_loudnessMetadata != EbuCoreloudnessMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_loudnessMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("loudnessMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_loudnessMetadata->Serialize(doc, element, &elem);
	}

}

bool EbuCoreaudioProgrammeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioProgrammeID")))
	{
		// Deserialize string type
		this->SetaudioProgrammeID(Dc1Convert::TextToString(parent->getAttribute(X("audioProgrammeID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioProgrammeName")))
	{
		// Deserialize string type
		this->SetaudioProgrammeName(Dc1Convert::TextToString(parent->getAttribute(X("audioProgrammeName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioProgrammeLanguage")))
	{
		// Deserialize string type
		this->SetaudioProgrammeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("audioProgrammeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("start")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("start")));
		this->Setstart(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("end")))
	{
		EbuCoretimecodePtr tmp = CreatetimecodeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("end")));
		this->Setend(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioContentIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioContentIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr tmp = CreateaudioProgrammeType_audioContentIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioContentIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("loudnessMetadata"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("loudnessMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateloudnessMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetloudnessMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioProgrammeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioContentIDRef")) == 0))
  {
	EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr tmp = CreateaudioProgrammeType_audioContentIDRef_CollectionType; // FTT, check this
	this->SetaudioContentIDRef(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("loudnessMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateloudnessMetadataType; // FTT, check this
	}
	this->SetloudnessMetadata(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioProgrammeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioContentIDRef() != Dc1NodePtr())
		result.Insert(GetaudioContentIDRef());
	if (GetloudnessMetadata() != Dc1NodePtr())
		result.Insert(GetloudnessMetadata());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioProgrammeType_ExtMethodImpl.h


