
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "EbuCoreFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file EbuCoretimecodeType_ExtImplInclude.h


#include "EbuCoretimecodeType.h"

EbuCoretimecodeType::EbuCoretimecodeType()
	: m_nsURI(X("urn:ebu:metadata-schema:ebuCore_2014"))
{
		Init();
}

EbuCoretimecodeType::~EbuCoretimecodeType()
{
		Cleanup();
}

void EbuCoretimecodeType::Init()
{	   
		m_Hour = -1;
		m_Minute = -1;
		m_Second = -1;
		m_Frame = -1;
		m_DropFrame = false;

// no includefile for extension defined 
// file EbuCoretimecodeType_ExtPropInit.h

}

void EbuCoretimecodeType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretimecodeType_ExtPropCleanup.h


}

void EbuCoretimecodeType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< EbuCoretimecodeType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr EbuCoretimecodeType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretimecodeType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int EbuCoretimecodeType::GetHour() const
{
		return m_Hour;
}

void EbuCoretimecodeType::SetHour(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimecodeType::SetHour().");
		}
		m_Hour = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int EbuCoretimecodeType::GetMinute() const
{
		return m_Minute;
}

void EbuCoretimecodeType::SetMinute(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimecodeType::SetMinute().");
		}
		m_Minute = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int EbuCoretimecodeType::GetSecond() const
{
		return m_Second;
}

void EbuCoretimecodeType::SetSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimecodeType::SetSecond().");
		}
		m_Second = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int EbuCoretimecodeType::GetFrame() const
{
		return m_Frame;
}

void EbuCoretimecodeType::SetFrame(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimecodeType::SetFrame().");
		}
		m_Frame = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool EbuCoretimecodeType::GetDropFrame() const
{
		return m_DropFrame;
}

void EbuCoretimecodeType::SetDropFrame(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretimecodeType::SetDropFrame().");
		}
		m_DropFrame = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * EbuCoretimecodeType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		XMLCh * sep = m_DropFrame ? L"." : L":";
		if(m_Hour >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Hour);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, sep);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Minute >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Minute);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, sep);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Second >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Second);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, sep);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Frame >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Frame);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, sep);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool EbuCoretimecodeType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring[128] = { 0 };
		RegularExpression r ("^(\\d{2}:\\d{2}:\\d{2}:\\d{2,5})|(\\d{2};\\d{2};\\d{2};\\d{2,5})|(\\d{2}\\.\\d{2}\\.\\d{2}\\.\\d{2,5})|(\\d{2}:\\d{2}:\\d{2}\\.\\d{2,5})|(\\d{2}:\\d{2}:\\d{2};\\d{2,5})$");

		// This is for Xerces 3.1.1+
		if (r.matches(txt, &m))
		{
			for (int i = 0; i < m.getNoGroups(); i++)
			{
				int i1 = m.getStartPos(i);
				int i2 = Dc1Util::GetNextEndPos(m, i);
				if (i1 == i2) continue; // ignore missing optional or wrong group

				switch (i)
				{
				case 0: // General group
					// d2
					XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 2);
					m_Hour = Dc1Convert::TextToUnsignedInt(substring);
					// d2
					XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 3, i1 + 5);
					m_Minute = Dc1Convert::TextToUnsignedInt(substring);
					// d2
					XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 6, i1 + 8);
					m_Second = Dc1Convert::TextToUnsignedInt(substring);
					// d2 - d5

					XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 9, i2);
					m_Frame = Dc1Convert::TextToUnsignedInt(substring);
					break;

				case 1: // Non dropframe
					if (i1 >= 0)
						m_DropFrame = false;
					break;
				case 2: // Dropframe
				case 3:
				case 4:
				case 5:
					if (i1 >= 0)
						m_DropFrame = true;
					break;
				default: return true;
					break;
				}
			}
		}
		else return false;
		return true;
}

bool EbuCoretimecodeType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// EbuCoretimecodeType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* EbuCoretimecodeType::ContentToString() const
{
	// EbuCoretimecodeType has no attributes so forward it to ToText()
	return ToText();
}

void EbuCoretimecodeType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timecodeType")))));
		}

// no includefile for extension defined 
// file EbuCoretimecodeType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool EbuCoretimecodeType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file EbuCoretimecodeType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file EbuCoretimecodeType_ExtMethodImpl.h








