
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorerightsType_ExtImplInclude.h


#include "EbuCorerightsType_rights_CollectionType.h"
#include "EbuCorerightsType_rightsHolder_CollectionType.h"
#include "EbuCorerightsType_exploitationIssues_CollectionType.h"
#include "EbuCorerightsType_copyrightStatement_CollectionType.h"
#include "EbuCorecoverageType.h"
#include "EbuCorerightsType_disclaimer_CollectionType.h"
#include "EbuCorerightsType_rightsAttributedId_CollectionType.h"
#include "EbuCorerightsType_contactDetails_CollectionType.h"
#include "EbuCorerightsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:rights
#include "EbuCoreentityType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rightsHolder
#include "EbuCoreelementType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:exploitationIssues
#include "EbuCoreidentifierType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:rightsAttributedId
#include "EbuCorecontactDetailsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:contactDetails

#include <assert.h>
IEbuCorerightsType::IEbuCorerightsType()
{

// no includefile for extension defined 
// file EbuCorerightsType_ExtPropInit.h

}

IEbuCorerightsType::~IEbuCorerightsType()
{
// no includefile for extension defined 
// file EbuCorerightsType_ExtPropCleanup.h

}

EbuCorerightsType::EbuCorerightsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorerightsType::~EbuCorerightsType()
{
	Cleanup();
}

void EbuCorerightsType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;
	m_formatIDRefs = NULL; // String
	m_formatIDRefs_Exist = false;
	m_rightsID = NULL; // String
	m_rightsID_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_rights = EbuCorerightsType_rights_CollectionPtr(); // Collection
	m_rightsLink = NULL; // Optional String
	m_rightsLink_Exist = false;
	m_rightsHolder = EbuCorerightsType_rightsHolder_CollectionPtr(); // Collection
	m_exploitationIssues = EbuCorerightsType_exploitationIssues_CollectionPtr(); // Collection
	m_copyrightStatement = EbuCorerightsType_copyrightStatement_CollectionPtr(); // Collection
	m_coverage = EbuCorecoveragePtr(); // Class
	m_coverage_Exist = false;
	m_rightsClearanceFlag = (false); // Value

	m_rightsClearanceFlag_Exist = false;
	m_disclaimer = EbuCorerightsType_disclaimer_CollectionPtr(); // Collection
	m_rightsAttributedId = EbuCorerightsType_rightsAttributedId_CollectionPtr(); // Collection
	m_contactDetails = EbuCorerightsType_contactDetails_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCorerightsType_ExtMyPropInit.h

}

void EbuCorerightsType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorerightsType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_note); // String
	XMLString::release(&m_formatIDRefs); // String
	XMLString::release(&m_rightsID); // String
	// Dc1Factory::DeleteObject(m_rights);
	XMLString::release(&this->m_rightsLink);
	this->m_rightsLink = NULL;
	// Dc1Factory::DeleteObject(m_rightsHolder);
	// Dc1Factory::DeleteObject(m_exploitationIssues);
	// Dc1Factory::DeleteObject(m_copyrightStatement);
	// Dc1Factory::DeleteObject(m_coverage);
	// Dc1Factory::DeleteObject(m_disclaimer);
	// Dc1Factory::DeleteObject(m_rightsAttributedId);
	// Dc1Factory::DeleteObject(m_contactDetails);
}

void EbuCorerightsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use rightsTypePtr, since we
	// might need GetBase(), which isn't defined in IrightsType
	const Dc1Ptr< EbuCorerightsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
	{
	XMLString::release(&m_formatIDRefs); // String
	if (tmp->ExistformatIDRefs())
	{
		this->SetformatIDRefs(XMLString::replicate(tmp->GetformatIDRefs()));
	}
	else
	{
		InvalidateformatIDRefs();
	}
	}
	{
	XMLString::release(&m_rightsID); // String
	if (tmp->ExistrightsID())
	{
		this->SetrightsID(XMLString::replicate(tmp->GetrightsID()));
	}
	else
	{
		InvalidaterightsID();
	}
	}
		// Dc1Factory::DeleteObject(m_rights);
		this->Setrights(Dc1Factory::CloneObject(tmp->Getrights()));
	if (tmp->IsValidrightsLink())
	{
		this->SetrightsLink(XMLString::replicate(tmp->GetrightsLink()));
	}
	else
	{
		InvalidaterightsLink();
	}
		// Dc1Factory::DeleteObject(m_rightsHolder);
		this->SetrightsHolder(Dc1Factory::CloneObject(tmp->GetrightsHolder()));
		// Dc1Factory::DeleteObject(m_exploitationIssues);
		this->SetexploitationIssues(Dc1Factory::CloneObject(tmp->GetexploitationIssues()));
		// Dc1Factory::DeleteObject(m_copyrightStatement);
		this->SetcopyrightStatement(Dc1Factory::CloneObject(tmp->GetcopyrightStatement()));
	if (tmp->IsValidcoverage())
	{
		// Dc1Factory::DeleteObject(m_coverage);
		this->Setcoverage(Dc1Factory::CloneObject(tmp->Getcoverage()));
	}
	else
	{
		Invalidatecoverage();
	}
	if (tmp->IsValidrightsClearanceFlag())
	{
		this->SetrightsClearanceFlag(tmp->GetrightsClearanceFlag());
	}
	else
	{
		InvalidaterightsClearanceFlag();
	}
		// Dc1Factory::DeleteObject(m_disclaimer);
		this->Setdisclaimer(Dc1Factory::CloneObject(tmp->Getdisclaimer()));
		// Dc1Factory::DeleteObject(m_rightsAttributedId);
		this->SetrightsAttributedId(Dc1Factory::CloneObject(tmp->GetrightsAttributedId()));
		// Dc1Factory::DeleteObject(m_contactDetails);
		this->SetcontactDetails(Dc1Factory::CloneObject(tmp->GetcontactDetails()));
}

Dc1NodePtr EbuCorerightsType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorerightsType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCorerightsType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCorerightsType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCorerightsType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCorerightsType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCorerightsType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCorerightsType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCorerightsType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCorerightsType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCorerightsType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCorerightsType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCorerightsType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCorerightsType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCorerightsType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCorerightsType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCorerightsType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCorerightsType::Getnote() const
{
	return m_note;
}

bool EbuCorerightsType::Existnote() const
{
	return m_note_Exist;
}
void EbuCorerightsType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::Invalidatenote()
{
	m_note_Exist = false;
}
XMLCh * EbuCorerightsType::GetformatIDRefs() const
{
	return m_formatIDRefs;
}

bool EbuCorerightsType::ExistformatIDRefs() const
{
	return m_formatIDRefs_Exist;
}
void EbuCorerightsType::SetformatIDRefs(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetformatIDRefs().");
	}
	m_formatIDRefs = item;
	m_formatIDRefs_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidateformatIDRefs()
{
	m_formatIDRefs_Exist = false;
}
XMLCh * EbuCorerightsType::GetrightsID() const
{
	return m_rightsID;
}

bool EbuCorerightsType::ExistrightsID() const
{
	return m_rightsID_Exist;
}
void EbuCorerightsType::SetrightsID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetrightsID().");
	}
	m_rightsID = item;
	m_rightsID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidaterightsID()
{
	m_rightsID_Exist = false;
}
EbuCorerightsType_rights_CollectionPtr EbuCorerightsType::Getrights() const
{
		return m_rights;
}

XMLCh * EbuCorerightsType::GetrightsLink() const
{
		return m_rightsLink;
}

// Element is optional
bool EbuCorerightsType::IsValidrightsLink() const
{
	return m_rightsLink_Exist;
}

EbuCorerightsType_rightsHolder_CollectionPtr EbuCorerightsType::GetrightsHolder() const
{
		return m_rightsHolder;
}

EbuCorerightsType_exploitationIssues_CollectionPtr EbuCorerightsType::GetexploitationIssues() const
{
		return m_exploitationIssues;
}

EbuCorerightsType_copyrightStatement_CollectionPtr EbuCorerightsType::GetcopyrightStatement() const
{
		return m_copyrightStatement;
}

EbuCorecoveragePtr EbuCorerightsType::Getcoverage() const
{
		return m_coverage;
}

// Element is optional
bool EbuCorerightsType::IsValidcoverage() const
{
	return m_coverage_Exist;
}

bool EbuCorerightsType::GetrightsClearanceFlag() const
{
		return m_rightsClearanceFlag;
}

// Element is optional
bool EbuCorerightsType::IsValidrightsClearanceFlag() const
{
	return m_rightsClearanceFlag_Exist;
}

EbuCorerightsType_disclaimer_CollectionPtr EbuCorerightsType::Getdisclaimer() const
{
		return m_disclaimer;
}

EbuCorerightsType_rightsAttributedId_CollectionPtr EbuCorerightsType::GetrightsAttributedId() const
{
		return m_rightsAttributedId;
}

EbuCorerightsType_contactDetails_CollectionPtr EbuCorerightsType::GetcontactDetails() const
{
		return m_contactDetails;
}

void EbuCorerightsType::Setrights(const EbuCorerightsType_rights_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::Setrights().");
	}
	if (m_rights != item)
	{
		// Dc1Factory::DeleteObject(m_rights);
		m_rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rights) m_rights->SetParent(m_myself.getPointer());
		if(m_rights != EbuCorerightsType_rights_CollectionPtr())
		{
			m_rights->SetContentName(XMLString::transcode("rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::SetrightsLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetrightsLink().");
	}
	if (m_rightsLink != item || m_rightsLink_Exist == false)
	{
		XMLString::release(&m_rightsLink);
		m_rightsLink = item;
		m_rightsLink_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidaterightsLink()
{
	m_rightsLink_Exist = false;
}
void EbuCorerightsType::SetrightsHolder(const EbuCorerightsType_rightsHolder_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetrightsHolder().");
	}
	if (m_rightsHolder != item)
	{
		// Dc1Factory::DeleteObject(m_rightsHolder);
		m_rightsHolder = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rightsHolder) m_rightsHolder->SetParent(m_myself.getPointer());
		if(m_rightsHolder != EbuCorerightsType_rightsHolder_CollectionPtr())
		{
			m_rightsHolder->SetContentName(XMLString::transcode("rightsHolder"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::SetexploitationIssues(const EbuCorerightsType_exploitationIssues_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetexploitationIssues().");
	}
	if (m_exploitationIssues != item)
	{
		// Dc1Factory::DeleteObject(m_exploitationIssues);
		m_exploitationIssues = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_exploitationIssues) m_exploitationIssues->SetParent(m_myself.getPointer());
		if(m_exploitationIssues != EbuCorerightsType_exploitationIssues_CollectionPtr())
		{
			m_exploitationIssues->SetContentName(XMLString::transcode("exploitationIssues"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::SetcopyrightStatement(const EbuCorerightsType_copyrightStatement_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetcopyrightStatement().");
	}
	if (m_copyrightStatement != item)
	{
		// Dc1Factory::DeleteObject(m_copyrightStatement);
		m_copyrightStatement = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_copyrightStatement) m_copyrightStatement->SetParent(m_myself.getPointer());
		if(m_copyrightStatement != EbuCorerightsType_copyrightStatement_CollectionPtr())
		{
			m_copyrightStatement->SetContentName(XMLString::transcode("copyrightStatement"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::Setcoverage(const EbuCorecoveragePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::Setcoverage().");
	}
	if (m_coverage != item || m_coverage_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_coverage);
		m_coverage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_coverage) m_coverage->SetParent(m_myself.getPointer());
		if (m_coverage && m_coverage->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coverageType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_coverage->UseTypeAttribute = true;
		}
		m_coverage_Exist = true;
		if(m_coverage != EbuCorecoveragePtr())
		{
			m_coverage->SetContentName(XMLString::transcode("coverage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::Invalidatecoverage()
{
	m_coverage_Exist = false;
}
void EbuCorerightsType::SetrightsClearanceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetrightsClearanceFlag().");
	}
	if (m_rightsClearanceFlag != item || m_rightsClearanceFlag_Exist == false)
	{
		m_rightsClearanceFlag = item;
		m_rightsClearanceFlag_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::InvalidaterightsClearanceFlag()
{
	m_rightsClearanceFlag_Exist = false;
}
void EbuCorerightsType::Setdisclaimer(const EbuCorerightsType_disclaimer_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::Setdisclaimer().");
	}
	if (m_disclaimer != item)
	{
		// Dc1Factory::DeleteObject(m_disclaimer);
		m_disclaimer = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_disclaimer) m_disclaimer->SetParent(m_myself.getPointer());
		if(m_disclaimer != EbuCorerightsType_disclaimer_CollectionPtr())
		{
			m_disclaimer->SetContentName(XMLString::transcode("disclaimer"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::SetrightsAttributedId(const EbuCorerightsType_rightsAttributedId_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetrightsAttributedId().");
	}
	if (m_rightsAttributedId != item)
	{
		// Dc1Factory::DeleteObject(m_rightsAttributedId);
		m_rightsAttributedId = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rightsAttributedId) m_rightsAttributedId->SetParent(m_myself.getPointer());
		if(m_rightsAttributedId != EbuCorerightsType_rightsAttributedId_CollectionPtr())
		{
			m_rightsAttributedId->SetContentName(XMLString::transcode("rightsAttributedId"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorerightsType::SetcontactDetails(const EbuCorerightsType_contactDetails_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorerightsType::SetcontactDetails().");
	}
	if (m_contactDetails != item)
	{
		// Dc1Factory::DeleteObject(m_contactDetails);
		m_contactDetails = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_contactDetails) m_contactDetails->SetParent(m_myself.getPointer());
		if(m_contactDetails != EbuCorerightsType_contactDetails_CollectionPtr())
		{
			m_contactDetails->SetContentName(XMLString::transcode("contactDetails"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCorerightsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  8, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatIDRefs")) == 0)
	{
		// formatIDRefs is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("rightsID")) == 0)
	{
		// rightsID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rights")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:rights is item of type elementType
		// in element collection rightsType_rights_CollectionType
		
		context->Found = true;
		EbuCorerightsType_rights_CollectionPtr coll = Getrights();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_rights_CollectionType; // FTT, check this
				Setrights(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rightsHolder")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:rightsHolder is item of type entityType
		// in element collection rightsType_rightsHolder_CollectionType
		
		context->Found = true;
		EbuCorerightsType_rightsHolder_CollectionPtr coll = GetrightsHolder();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_rightsHolder_CollectionType; // FTT, check this
				SetrightsHolder(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("exploitationIssues")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:exploitationIssues is item of type elementType
		// in element collection rightsType_exploitationIssues_CollectionType
		
		context->Found = true;
		EbuCorerightsType_exploitationIssues_CollectionPtr coll = GetexploitationIssues();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_exploitationIssues_CollectionType; // FTT, check this
				SetexploitationIssues(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("copyrightStatement")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:copyrightStatement is item of type elementType
		// in element collection rightsType_copyrightStatement_CollectionType
		
		context->Found = true;
		EbuCorerightsType_copyrightStatement_CollectionPtr coll = GetcopyrightStatement();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_copyrightStatement_CollectionType; // FTT, check this
				SetcopyrightStatement(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("coverage")) == 0)
	{
		// coverage is simple element coverageType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getcoverage()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coverageType")))) != empty)
			{
				// Is type allowed
				EbuCorecoveragePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setcoverage(p, client);
					if((p = Getcoverage()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("disclaimer")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:disclaimer is item of type elementType
		// in element collection rightsType_disclaimer_CollectionType
		
		context->Found = true;
		EbuCorerightsType_disclaimer_CollectionPtr coll = Getdisclaimer();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_disclaimer_CollectionType; // FTT, check this
				Setdisclaimer(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rightsAttributedId")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:rightsAttributedId is item of type identifierType
		// in element collection rightsType_rightsAttributedId_CollectionType
		
		context->Found = true;
		EbuCorerightsType_rightsAttributedId_CollectionPtr coll = GetrightsAttributedId();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_rightsAttributedId_CollectionType; // FTT, check this
				SetrightsAttributedId(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))) != empty)
			{
				// Is type allowed
				EbuCoreidentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("contactDetails")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:contactDetails is item of type contactDetailsType
		// in element collection rightsType_contactDetails_CollectionType
		
		context->Found = true;
		EbuCorerightsType_contactDetails_CollectionPtr coll = GetcontactDetails();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreaterightsType_contactDetails_CollectionType; // FTT, check this
				SetcontactDetails(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType")))) != empty)
			{
				// Is type allowed
				EbuCorecontactDetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for rightsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "rightsType");
	}
	return result;
}

XMLCh * EbuCorerightsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorerightsType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorerightsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("rightsType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatIDRefs_Exist)
	{
	// String
	if(m_formatIDRefs != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatIDRefs"), m_formatIDRefs);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_rightsID_Exist)
	{
	// String
	if(m_rightsID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("rightsID"), m_rightsID);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_rights != EbuCorerightsType_rights_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_rights->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	 // String
	if(m_rightsLink != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_rightsLink, X("urn:ebu:metadata-schema:ebuCore_2014"), X("rightsLink"), true);
	if (m_rightsHolder != EbuCorerightsType_rightsHolder_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_rightsHolder->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_exploitationIssues != EbuCorerightsType_exploitationIssues_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_exploitationIssues->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_copyrightStatement != EbuCorerightsType_copyrightStatement_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_copyrightStatement->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_coverage != EbuCorecoveragePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_coverage->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("coverage"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_coverage->Serialize(doc, element, &elem);
	}
	if(m_rightsClearanceFlag_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_rightsClearanceFlag);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("rightsClearanceFlag"), true);
		XMLString::release(&tmp);
	}
	if (m_disclaimer != EbuCorerightsType_disclaimer_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_disclaimer->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_rightsAttributedId != EbuCorerightsType_rightsAttributedId_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_rightsAttributedId->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_contactDetails != EbuCorerightsType_contactDetails_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_contactDetails->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCorerightsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatIDRefs")))
	{
		// Deserialize string type
		this->SetformatIDRefs(Dc1Convert::TextToString(parent->getAttribute(X("formatIDRefs"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("rightsID")))
	{
		// Deserialize string type
		this->SetrightsID(Dc1Convert::TextToString(parent->getAttribute(X("rightsID"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("rights"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("rights")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_rights_CollectionPtr tmp = CreaterightsType_rights_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setrights(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("rightsLink"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetrightsLink(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetrightsLink(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("rightsHolder"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("rightsHolder")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_rightsHolder_CollectionPtr tmp = CreaterightsType_rightsHolder_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetrightsHolder(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("exploitationIssues"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("exploitationIssues")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_exploitationIssues_CollectionPtr tmp = CreaterightsType_exploitationIssues_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetexploitationIssues(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("copyrightStatement"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("copyrightStatement")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_copyrightStatement_CollectionPtr tmp = CreaterightsType_copyrightStatement_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetcopyrightStatement(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("coverage"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("coverage")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecoverageType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcoverage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("rightsClearanceFlag"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetrightsClearanceFlag(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("disclaimer"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("disclaimer")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_disclaimer_CollectionPtr tmp = CreaterightsType_disclaimer_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdisclaimer(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("rightsAttributedId"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("rightsAttributedId")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_rightsAttributedId_CollectionPtr tmp = CreaterightsType_rightsAttributedId_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetrightsAttributedId(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("contactDetails"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("contactDetails")) == 0))
		{
			// Deserialize factory type
			EbuCorerightsType_contactDetails_CollectionPtr tmp = CreaterightsType_contactDetails_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetcontactDetails(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorerightsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorerightsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("rights")) == 0))
  {
	EbuCorerightsType_rights_CollectionPtr tmp = CreaterightsType_rights_CollectionType; // FTT, check this
	this->Setrights(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("rightsHolder")) == 0))
  {
	EbuCorerightsType_rightsHolder_CollectionPtr tmp = CreaterightsType_rightsHolder_CollectionType; // FTT, check this
	this->SetrightsHolder(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("exploitationIssues")) == 0))
  {
	EbuCorerightsType_exploitationIssues_CollectionPtr tmp = CreaterightsType_exploitationIssues_CollectionType; // FTT, check this
	this->SetexploitationIssues(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("copyrightStatement")) == 0))
  {
	EbuCorerightsType_copyrightStatement_CollectionPtr tmp = CreaterightsType_copyrightStatement_CollectionType; // FTT, check this
	this->SetcopyrightStatement(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("coverage")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecoverageType; // FTT, check this
	}
	this->Setcoverage(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("disclaimer")) == 0))
  {
	EbuCorerightsType_disclaimer_CollectionPtr tmp = CreaterightsType_disclaimer_CollectionType; // FTT, check this
	this->Setdisclaimer(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("rightsAttributedId")) == 0))
  {
	EbuCorerightsType_rightsAttributedId_CollectionPtr tmp = CreaterightsType_rightsAttributedId_CollectionType; // FTT, check this
	this->SetrightsAttributedId(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("contactDetails")) == 0))
  {
	EbuCorerightsType_contactDetails_CollectionPtr tmp = CreaterightsType_contactDetails_CollectionType; // FTT, check this
	this->SetcontactDetails(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCorerightsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getrights() != Dc1NodePtr())
		result.Insert(Getrights());
	if (GetrightsHolder() != Dc1NodePtr())
		result.Insert(GetrightsHolder());
	if (GetexploitationIssues() != Dc1NodePtr())
		result.Insert(GetexploitationIssues());
	if (GetcopyrightStatement() != Dc1NodePtr())
		result.Insert(GetcopyrightStatement());
	if (Getcoverage() != Dc1NodePtr())
		result.Insert(Getcoverage());
	if (Getdisclaimer() != Dc1NodePtr())
		result.Insert(Getdisclaimer());
	if (GetrightsAttributedId() != Dc1NodePtr())
		result.Insert(GetrightsAttributedId());
	if (GetcontactDetails() != Dc1NodePtr())
		result.Insert(GetcontactDetails());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorerightsType_ExtMethodImpl.h


