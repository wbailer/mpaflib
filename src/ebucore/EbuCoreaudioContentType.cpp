
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtImplInclude.h


#include "EbuCoreaudioContentType_audioObjectIDRef_CollectionType.h"
#include "EbuCoreloudnessMetadataType.h"
#include "EbuCoreaudioContentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioContentType::IEbuCoreaudioContentType()
{

// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtPropInit.h

}

IEbuCoreaudioContentType::~IEbuCoreaudioContentType()
{
// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtPropCleanup.h

}

EbuCoreaudioContentType::EbuCoreaudioContentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioContentType::~EbuCoreaudioContentType()
{
	Cleanup();
}

void EbuCoreaudioContentType::Init()
{

	// Init attributes
	m_audioContentID = NULL; // String
	m_audioContentID_Exist = false;
	m_audioContentName = NULL; // String
	m_audioContentName_Exist = false;
	m_audioContentLanguage = NULL; // String
	m_audioContentLanguage_Exist = false;
	m_dialogue = 0; // Value
	m_dialogue_Default = 0; // Default value
	m_dialogue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioObjectIDRef = EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr(); // Collection
	m_loudnessMetadata = EbuCoreloudnessMetadataPtr(); // Class
	m_loudnessMetadata_Exist = false;


// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtMyPropInit.h

}

void EbuCoreaudioContentType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtMyPropCleanup.h


	XMLString::release(&m_audioContentID); // String
	XMLString::release(&m_audioContentName); // String
	XMLString::release(&m_audioContentLanguage); // String
	// Dc1Factory::DeleteObject(m_audioObjectIDRef);
	// Dc1Factory::DeleteObject(m_loudnessMetadata);
}

void EbuCoreaudioContentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioContentTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioContentType
	const Dc1Ptr< EbuCoreaudioContentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioContentID); // String
	if (tmp->ExistaudioContentID())
	{
		this->SetaudioContentID(XMLString::replicate(tmp->GetaudioContentID()));
	}
	else
	{
		InvalidateaudioContentID();
	}
	}
	{
	XMLString::release(&m_audioContentName); // String
	if (tmp->ExistaudioContentName())
	{
		this->SetaudioContentName(XMLString::replicate(tmp->GetaudioContentName()));
	}
	else
	{
		InvalidateaudioContentName();
	}
	}
	{
	XMLString::release(&m_audioContentLanguage); // String
	if (tmp->ExistaudioContentLanguage())
	{
		this->SetaudioContentLanguage(XMLString::replicate(tmp->GetaudioContentLanguage()));
	}
	else
	{
		InvalidateaudioContentLanguage();
	}
	}
	{
	if (tmp->Existdialogue())
	{
		this->Setdialogue(tmp->Getdialogue());
	}
	else
	{
		Invalidatedialogue();
	}
	}
		// Dc1Factory::DeleteObject(m_audioObjectIDRef);
		this->SetaudioObjectIDRef(Dc1Factory::CloneObject(tmp->GetaudioObjectIDRef()));
	if (tmp->IsValidloudnessMetadata())
	{
		// Dc1Factory::DeleteObject(m_loudnessMetadata);
		this->SetloudnessMetadata(Dc1Factory::CloneObject(tmp->GetloudnessMetadata()));
	}
	else
	{
		InvalidateloudnessMetadata();
	}
}

Dc1NodePtr EbuCoreaudioContentType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioContentType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioContentType::GetaudioContentID() const
{
	return m_audioContentID;
}

bool EbuCoreaudioContentType::ExistaudioContentID() const
{
	return m_audioContentID_Exist;
}
void EbuCoreaudioContentType::SetaudioContentID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::SetaudioContentID().");
	}
	m_audioContentID = item;
	m_audioContentID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::InvalidateaudioContentID()
{
	m_audioContentID_Exist = false;
}
XMLCh * EbuCoreaudioContentType::GetaudioContentName() const
{
	return m_audioContentName;
}

bool EbuCoreaudioContentType::ExistaudioContentName() const
{
	return m_audioContentName_Exist;
}
void EbuCoreaudioContentType::SetaudioContentName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::SetaudioContentName().");
	}
	m_audioContentName = item;
	m_audioContentName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::InvalidateaudioContentName()
{
	m_audioContentName_Exist = false;
}
XMLCh * EbuCoreaudioContentType::GetaudioContentLanguage() const
{
	return m_audioContentLanguage;
}

bool EbuCoreaudioContentType::ExistaudioContentLanguage() const
{
	return m_audioContentLanguage_Exist;
}
void EbuCoreaudioContentType::SetaudioContentLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::SetaudioContentLanguage().");
	}
	m_audioContentLanguage = item;
	m_audioContentLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::InvalidateaudioContentLanguage()
{
	m_audioContentLanguage_Exist = false;
}
int EbuCoreaudioContentType::Getdialogue() const
{
	if (this->Existdialogue()) {
		return m_dialogue;
	} else {
		return m_dialogue_Default;
	}
}

bool EbuCoreaudioContentType::Existdialogue() const
{
	return m_dialogue_Exist;
}
void EbuCoreaudioContentType::Setdialogue(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::Setdialogue().");
	}
	m_dialogue = item;
	m_dialogue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::Invalidatedialogue()
{
	m_dialogue_Exist = false;
}
EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr EbuCoreaudioContentType::GetaudioObjectIDRef() const
{
		return m_audioObjectIDRef;
}

EbuCoreloudnessMetadataPtr EbuCoreaudioContentType::GetloudnessMetadata() const
{
		return m_loudnessMetadata;
}

// Element is optional
bool EbuCoreaudioContentType::IsValidloudnessMetadata() const
{
	return m_loudnessMetadata_Exist;
}

void EbuCoreaudioContentType::SetaudioObjectIDRef(const EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::SetaudioObjectIDRef().");
	}
	if (m_audioObjectIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioObjectIDRef);
		m_audioObjectIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioObjectIDRef) m_audioObjectIDRef->SetParent(m_myself.getPointer());
		if(m_audioObjectIDRef != EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr())
		{
			m_audioObjectIDRef->SetContentName(XMLString::transcode("audioObjectIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::SetloudnessMetadata(const EbuCoreloudnessMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioContentType::SetloudnessMetadata().");
	}
	if (m_loudnessMetadata != item || m_loudnessMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_loudnessMetadata);
		m_loudnessMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_loudnessMetadata) m_loudnessMetadata->SetParent(m_myself.getPointer());
		if (m_loudnessMetadata && m_loudnessMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_loudnessMetadata->UseTypeAttribute = true;
		}
		m_loudnessMetadata_Exist = true;
		if(m_loudnessMetadata != EbuCoreloudnessMetadataPtr())
		{
			m_loudnessMetadata->SetContentName(XMLString::transcode("loudnessMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioContentType::InvalidateloudnessMetadata()
{
	m_loudnessMetadata_Exist = false;
}

Dc1NodeEnum EbuCoreaudioContentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioContentID")) == 0)
	{
		// audioContentID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioContentName")) == 0)
	{
		// audioContentName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioContentLanguage")) == 0)
	{
		// audioContentLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dialogue")) == 0)
	{
		// dialogue is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioObjectIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("loudnessMetadata")) == 0)
	{
		// loudnessMetadata is simple element loudnessMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetloudnessMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType")))) != empty)
			{
				// Is type allowed
				EbuCoreloudnessMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetloudnessMetadata(p, client);
					if((p = GetloudnessMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioContentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioContentType");
	}
	return result;
}

XMLCh * EbuCoreaudioContentType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioContentType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioContentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioContentType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioContentID_Exist)
	{
	// String
	if(m_audioContentID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioContentID"), m_audioContentID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioContentName_Exist)
	{
	// String
	if(m_audioContentName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioContentName"), m_audioContentName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioContentLanguage_Exist)
	{
	// String
	if(m_audioContentLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioContentLanguage"), m_audioContentLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dialogue_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_dialogue);
		element->setAttributeNS(X(""), X("dialogue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioObjectIDRef != EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioObjectIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_loudnessMetadata != EbuCoreloudnessMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_loudnessMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("loudnessMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_loudnessMetadata->Serialize(doc, element, &elem);
	}

}

bool EbuCoreaudioContentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioContentID")))
	{
		// Deserialize string type
		this->SetaudioContentID(Dc1Convert::TextToString(parent->getAttribute(X("audioContentID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioContentName")))
	{
		// Deserialize string type
		this->SetaudioContentName(Dc1Convert::TextToString(parent->getAttribute(X("audioContentName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioContentLanguage")))
	{
		// Deserialize string type
		this->SetaudioContentLanguage(Dc1Convert::TextToString(parent->getAttribute(X("audioContentLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dialogue")))
	{
		// deserialize value type
		this->Setdialogue(Dc1Convert::TextToInt(parent->getAttribute(X("dialogue"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioObjectIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioObjectIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr tmp = CreateaudioContentType_audioObjectIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioObjectIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("loudnessMetadata"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("loudnessMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateloudnessMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetloudnessMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioContentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioObjectIDRef")) == 0))
  {
	EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr tmp = CreateaudioContentType_audioObjectIDRef_CollectionType; // FTT, check this
	this->SetaudioObjectIDRef(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("loudnessMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateloudnessMetadataType; // FTT, check this
	}
	this->SetloudnessMetadata(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioContentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioObjectIDRef() != Dc1NodePtr())
		result.Insert(GetaudioObjectIDRef());
	if (GetloudnessMetadata() != Dc1NodePtr())
		result.Insert(GetloudnessMetadata());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioContentType_ExtMethodImpl.h


