
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtImplInclude.h


#include "EbuCoreaudioFormatExtendedType_audioProgramme_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioContent_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioObject_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreaudioProgrammeType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioProgramme
#include "EbuCoreaudioContentType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioContent
#include "EbuCoreaudioObjectType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioObject
#include "EbuCoreaudioPackFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioPackFormat
#include "EbuCoreaudioChannelFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormat
#include "EbuCoreaudioBlockFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat
#include "EbuCoreaudioStreamFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormat
#include "EbuCoreaudioTrackFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormat
#include "EbuCoreaudioTrackUIDType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioTrackUID

#include <assert.h>
IEbuCoreaudioFormatExtendedType::IEbuCoreaudioFormatExtendedType()
{

// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtPropInit.h

}

IEbuCoreaudioFormatExtendedType::~IEbuCoreaudioFormatExtendedType()
{
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtPropCleanup.h

}

EbuCoreaudioFormatExtendedType::EbuCoreaudioFormatExtendedType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioFormatExtendedType::~EbuCoreaudioFormatExtendedType()
{
	Cleanup();
}

void EbuCoreaudioFormatExtendedType::Init()
{

	// Init attributes
	m_audioFormatExtendedID = NULL; // String
	m_audioFormatExtendedID_Exist = false;
	m_audioFormatExtendedName = NULL; // String
	m_audioFormatExtendedName_Exist = false;
	m_audioFormatExtendedDefinition = NULL; // String
	m_audioFormatExtendedDefinition_Exist = false;
	m_audioFormatExtendedVersion = NULL; // String
	m_audioFormatExtendedVersion_Exist = false;
	m_audioFormatExtendedPresenceFlag = NULL; // String
	m_audioFormatExtendedPresenceFlag_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioProgramme = EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr(); // Collection
	m_audioContent = EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr(); // Collection
	m_audioObject = EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr(); // Collection
	m_audioPackFormat = EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr(); // Collection
	m_audioChannelFormat = EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr(); // Collection
	m_audioBlockFormat = EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr(); // Collection
	m_audioStreamFormat = EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr(); // Collection
	m_audioTrackFormat = EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr(); // Collection
	m_audioTrackUID = EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMyPropInit.h

}

void EbuCoreaudioFormatExtendedType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMyPropCleanup.h


	XMLString::release(&m_audioFormatExtendedID); // String
	XMLString::release(&m_audioFormatExtendedName); // String
	XMLString::release(&m_audioFormatExtendedDefinition); // String
	XMLString::release(&m_audioFormatExtendedVersion); // String
	XMLString::release(&m_audioFormatExtendedPresenceFlag); // String
	// Dc1Factory::DeleteObject(m_audioProgramme);
	// Dc1Factory::DeleteObject(m_audioContent);
	// Dc1Factory::DeleteObject(m_audioObject);
	// Dc1Factory::DeleteObject(m_audioPackFormat);
	// Dc1Factory::DeleteObject(m_audioChannelFormat);
	// Dc1Factory::DeleteObject(m_audioBlockFormat);
	// Dc1Factory::DeleteObject(m_audioStreamFormat);
	// Dc1Factory::DeleteObject(m_audioTrackFormat);
	// Dc1Factory::DeleteObject(m_audioTrackUID);
}

void EbuCoreaudioFormatExtendedType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioFormatExtendedTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioFormatExtendedType
	const Dc1Ptr< EbuCoreaudioFormatExtendedType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioFormatExtendedID); // String
	if (tmp->ExistaudioFormatExtendedID())
	{
		this->SetaudioFormatExtendedID(XMLString::replicate(tmp->GetaudioFormatExtendedID()));
	}
	else
	{
		InvalidateaudioFormatExtendedID();
	}
	}
	{
	XMLString::release(&m_audioFormatExtendedName); // String
	if (tmp->ExistaudioFormatExtendedName())
	{
		this->SetaudioFormatExtendedName(XMLString::replicate(tmp->GetaudioFormatExtendedName()));
	}
	else
	{
		InvalidateaudioFormatExtendedName();
	}
	}
	{
	XMLString::release(&m_audioFormatExtendedDefinition); // String
	if (tmp->ExistaudioFormatExtendedDefinition())
	{
		this->SetaudioFormatExtendedDefinition(XMLString::replicate(tmp->GetaudioFormatExtendedDefinition()));
	}
	else
	{
		InvalidateaudioFormatExtendedDefinition();
	}
	}
	{
	XMLString::release(&m_audioFormatExtendedVersion); // String
	if (tmp->ExistaudioFormatExtendedVersion())
	{
		this->SetaudioFormatExtendedVersion(XMLString::replicate(tmp->GetaudioFormatExtendedVersion()));
	}
	else
	{
		InvalidateaudioFormatExtendedVersion();
	}
	}
	{
	XMLString::release(&m_audioFormatExtendedPresenceFlag); // String
	if (tmp->ExistaudioFormatExtendedPresenceFlag())
	{
		this->SetaudioFormatExtendedPresenceFlag(XMLString::replicate(tmp->GetaudioFormatExtendedPresenceFlag()));
	}
	else
	{
		InvalidateaudioFormatExtendedPresenceFlag();
	}
	}
		// Dc1Factory::DeleteObject(m_audioProgramme);
		this->SetaudioProgramme(Dc1Factory::CloneObject(tmp->GetaudioProgramme()));
		// Dc1Factory::DeleteObject(m_audioContent);
		this->SetaudioContent(Dc1Factory::CloneObject(tmp->GetaudioContent()));
		// Dc1Factory::DeleteObject(m_audioObject);
		this->SetaudioObject(Dc1Factory::CloneObject(tmp->GetaudioObject()));
		// Dc1Factory::DeleteObject(m_audioPackFormat);
		this->SetaudioPackFormat(Dc1Factory::CloneObject(tmp->GetaudioPackFormat()));
		// Dc1Factory::DeleteObject(m_audioChannelFormat);
		this->SetaudioChannelFormat(Dc1Factory::CloneObject(tmp->GetaudioChannelFormat()));
		// Dc1Factory::DeleteObject(m_audioBlockFormat);
		this->SetaudioBlockFormat(Dc1Factory::CloneObject(tmp->GetaudioBlockFormat()));
		// Dc1Factory::DeleteObject(m_audioStreamFormat);
		this->SetaudioStreamFormat(Dc1Factory::CloneObject(tmp->GetaudioStreamFormat()));
		// Dc1Factory::DeleteObject(m_audioTrackFormat);
		this->SetaudioTrackFormat(Dc1Factory::CloneObject(tmp->GetaudioTrackFormat()));
		// Dc1Factory::DeleteObject(m_audioTrackUID);
		this->SetaudioTrackUID(Dc1Factory::CloneObject(tmp->GetaudioTrackUID()));
}

Dc1NodePtr EbuCoreaudioFormatExtendedType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioFormatExtendedType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioFormatExtendedType::GetaudioFormatExtendedID() const
{
	return m_audioFormatExtendedID;
}

bool EbuCoreaudioFormatExtendedType::ExistaudioFormatExtendedID() const
{
	return m_audioFormatExtendedID_Exist;
}
void EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedID().");
	}
	m_audioFormatExtendedID = item;
	m_audioFormatExtendedID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::InvalidateaudioFormatExtendedID()
{
	m_audioFormatExtendedID_Exist = false;
}
XMLCh * EbuCoreaudioFormatExtendedType::GetaudioFormatExtendedName() const
{
	return m_audioFormatExtendedName;
}

bool EbuCoreaudioFormatExtendedType::ExistaudioFormatExtendedName() const
{
	return m_audioFormatExtendedName_Exist;
}
void EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedName().");
	}
	m_audioFormatExtendedName = item;
	m_audioFormatExtendedName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::InvalidateaudioFormatExtendedName()
{
	m_audioFormatExtendedName_Exist = false;
}
XMLCh * EbuCoreaudioFormatExtendedType::GetaudioFormatExtendedDefinition() const
{
	return m_audioFormatExtendedDefinition;
}

bool EbuCoreaudioFormatExtendedType::ExistaudioFormatExtendedDefinition() const
{
	return m_audioFormatExtendedDefinition_Exist;
}
void EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedDefinition().");
	}
	m_audioFormatExtendedDefinition = item;
	m_audioFormatExtendedDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::InvalidateaudioFormatExtendedDefinition()
{
	m_audioFormatExtendedDefinition_Exist = false;
}
XMLCh * EbuCoreaudioFormatExtendedType::GetaudioFormatExtendedVersion() const
{
	return m_audioFormatExtendedVersion;
}

bool EbuCoreaudioFormatExtendedType::ExistaudioFormatExtendedVersion() const
{
	return m_audioFormatExtendedVersion_Exist;
}
void EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedVersion(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedVersion().");
	}
	m_audioFormatExtendedVersion = item;
	m_audioFormatExtendedVersion_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::InvalidateaudioFormatExtendedVersion()
{
	m_audioFormatExtendedVersion_Exist = false;
}
XMLCh * EbuCoreaudioFormatExtendedType::GetaudioFormatExtendedPresenceFlag() const
{
	return m_audioFormatExtendedPresenceFlag;
}

bool EbuCoreaudioFormatExtendedType::ExistaudioFormatExtendedPresenceFlag() const
{
	return m_audioFormatExtendedPresenceFlag_Exist;
}
void EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedPresenceFlag(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioFormatExtendedPresenceFlag().");
	}
	m_audioFormatExtendedPresenceFlag = item;
	m_audioFormatExtendedPresenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::InvalidateaudioFormatExtendedPresenceFlag()
{
	m_audioFormatExtendedPresenceFlag_Exist = false;
}
EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioProgramme() const
{
		return m_audioProgramme;
}

EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioContent() const
{
		return m_audioContent;
}

EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioObject() const
{
		return m_audioObject;
}

EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioPackFormat() const
{
		return m_audioPackFormat;
}

EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioChannelFormat() const
{
		return m_audioChannelFormat;
}

EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioBlockFormat() const
{
		return m_audioBlockFormat;
}

EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioStreamFormat() const
{
		return m_audioStreamFormat;
}

EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioTrackFormat() const
{
		return m_audioTrackFormat;
}

EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr EbuCoreaudioFormatExtendedType::GetaudioTrackUID() const
{
		return m_audioTrackUID;
}

void EbuCoreaudioFormatExtendedType::SetaudioProgramme(const EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioProgramme().");
	}
	if (m_audioProgramme != item)
	{
		// Dc1Factory::DeleteObject(m_audioProgramme);
		m_audioProgramme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioProgramme) m_audioProgramme->SetParent(m_myself.getPointer());
		if(m_audioProgramme != EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr())
		{
			m_audioProgramme->SetContentName(XMLString::transcode("audioProgramme"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioContent(const EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioContent().");
	}
	if (m_audioContent != item)
	{
		// Dc1Factory::DeleteObject(m_audioContent);
		m_audioContent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioContent) m_audioContent->SetParent(m_myself.getPointer());
		if(m_audioContent != EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr())
		{
			m_audioContent->SetContentName(XMLString::transcode("audioContent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioObject(const EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioObject().");
	}
	if (m_audioObject != item)
	{
		// Dc1Factory::DeleteObject(m_audioObject);
		m_audioObject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioObject) m_audioObject->SetParent(m_myself.getPointer());
		if(m_audioObject != EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr())
		{
			m_audioObject->SetContentName(XMLString::transcode("audioObject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioPackFormat(const EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioPackFormat().");
	}
	if (m_audioPackFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioPackFormat);
		m_audioPackFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioPackFormat) m_audioPackFormat->SetParent(m_myself.getPointer());
		if(m_audioPackFormat != EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr())
		{
			m_audioPackFormat->SetContentName(XMLString::transcode("audioPackFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioChannelFormat(const EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioChannelFormat().");
	}
	if (m_audioChannelFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioChannelFormat);
		m_audioChannelFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioChannelFormat) m_audioChannelFormat->SetParent(m_myself.getPointer());
		if(m_audioChannelFormat != EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr())
		{
			m_audioChannelFormat->SetContentName(XMLString::transcode("audioChannelFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioBlockFormat(const EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioBlockFormat().");
	}
	if (m_audioBlockFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioBlockFormat);
		m_audioBlockFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioBlockFormat) m_audioBlockFormat->SetParent(m_myself.getPointer());
		if(m_audioBlockFormat != EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr())
		{
			m_audioBlockFormat->SetContentName(XMLString::transcode("audioBlockFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioStreamFormat(const EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioStreamFormat().");
	}
	if (m_audioStreamFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioStreamFormat);
		m_audioStreamFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioStreamFormat) m_audioStreamFormat->SetParent(m_myself.getPointer());
		if(m_audioStreamFormat != EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr())
		{
			m_audioStreamFormat->SetContentName(XMLString::transcode("audioStreamFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioTrackFormat(const EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioTrackFormat().");
	}
	if (m_audioTrackFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioTrackFormat);
		m_audioTrackFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrackFormat) m_audioTrackFormat->SetParent(m_myself.getPointer());
		if(m_audioTrackFormat != EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr())
		{
			m_audioTrackFormat->SetContentName(XMLString::transcode("audioTrackFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioFormatExtendedType::SetaudioTrackUID(const EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioFormatExtendedType::SetaudioTrackUID().");
	}
	if (m_audioTrackUID != item)
	{
		// Dc1Factory::DeleteObject(m_audioTrackUID);
		m_audioTrackUID = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrackUID) m_audioTrackUID->SetParent(m_myself.getPointer());
		if(m_audioTrackUID != EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr())
		{
			m_audioTrackUID->SetContentName(XMLString::transcode("audioTrackUID"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioFormatExtendedType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatExtendedID")) == 0)
	{
		// audioFormatExtendedID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatExtendedName")) == 0)
	{
		// audioFormatExtendedName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatExtendedDefinition")) == 0)
	{
		// audioFormatExtendedDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatExtendedVersion")) == 0)
	{
		// audioFormatExtendedVersion is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioFormatExtendedPresenceFlag")) == 0)
	{
		// audioFormatExtendedPresenceFlag is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioProgramme")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioProgramme is item of type audioProgrammeType
		// in element collection audioFormatExtendedType_audioProgramme_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr coll = GetaudioProgramme();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioProgramme_CollectionType; // FTT, check this
				SetaudioProgramme(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioProgrammeType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioProgrammePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioContent")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioContent is item of type audioContentType
		// in element collection audioFormatExtendedType_audioContent_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr coll = GetaudioContent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioContent_CollectionType; // FTT, check this
				SetaudioContent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioContentType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioObject")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioObject is item of type audioObjectType
		// in element collection audioFormatExtendedType_audioObject_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr coll = GetaudioObject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioObject_CollectionType; // FTT, check this
				SetaudioObject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioObjectPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioPackFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioPackFormat is item of type audioPackFormatType
		// in element collection audioFormatExtendedType_audioPackFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr coll = GetaudioPackFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioPackFormat_CollectionType; // FTT, check this
				SetaudioPackFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioPackFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioChannelFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormat is item of type audioChannelFormatType
		// in element collection audioFormatExtendedType_audioChannelFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr coll = GetaudioChannelFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioChannelFormat_CollectionType; // FTT, check this
				SetaudioChannelFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioChannelFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioBlockFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat is item of type audioBlockFormatType
		// in element collection audioFormatExtendedType_audioBlockFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr coll = GetaudioBlockFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioBlockFormat_CollectionType; // FTT, check this
				SetaudioBlockFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioBlockFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioStreamFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormat is item of type audioStreamFormatType
		// in element collection audioFormatExtendedType_audioStreamFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr coll = GetaudioStreamFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioStreamFormat_CollectionType; // FTT, check this
				SetaudioStreamFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioStreamFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrackFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormat is item of type audioTrackFormatType
		// in element collection audioFormatExtendedType_audioTrackFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr coll = GetaudioTrackFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioTrackFormat_CollectionType; // FTT, check this
				SetaudioTrackFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioTrackFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrackUID")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioTrackUID is item of type audioTrackUIDType
		// in element collection audioFormatExtendedType_audioTrackUID_CollectionType
		
		context->Found = true;
		EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr coll = GetaudioTrackUID();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioFormatExtendedType_audioTrackUID_CollectionType; // FTT, check this
				SetaudioTrackUID(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioTrackUIDType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioTrackUIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioFormatExtendedType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioFormatExtendedType");
	}
	return result;
}

XMLCh * EbuCoreaudioFormatExtendedType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioFormatExtendedType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioFormatExtendedType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioFormatExtendedType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatExtendedID_Exist)
	{
	// String
	if(m_audioFormatExtendedID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatExtendedID"), m_audioFormatExtendedID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatExtendedName_Exist)
	{
	// String
	if(m_audioFormatExtendedName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatExtendedName"), m_audioFormatExtendedName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatExtendedDefinition_Exist)
	{
	// String
	if(m_audioFormatExtendedDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatExtendedDefinition"), m_audioFormatExtendedDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatExtendedVersion_Exist)
	{
	// String
	if(m_audioFormatExtendedVersion != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatExtendedVersion"), m_audioFormatExtendedVersion);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioFormatExtendedPresenceFlag_Exist)
	{
	// String
	if(m_audioFormatExtendedPresenceFlag != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioFormatExtendedPresenceFlag"), m_audioFormatExtendedPresenceFlag);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioProgramme != EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioProgramme->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioContent != EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioContent->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioObject != EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioObject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioPackFormat != EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioPackFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioChannelFormat != EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioChannelFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioBlockFormat != EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioBlockFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioStreamFormat != EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioStreamFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioTrackFormat != EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioTrackFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioTrackUID != EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioTrackUID->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioFormatExtendedType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatExtendedID")))
	{
		// Deserialize string type
		this->SetaudioFormatExtendedID(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatExtendedID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatExtendedName")))
	{
		// Deserialize string type
		this->SetaudioFormatExtendedName(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatExtendedName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatExtendedDefinition")))
	{
		// Deserialize string type
		this->SetaudioFormatExtendedDefinition(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatExtendedDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatExtendedVersion")))
	{
		// Deserialize string type
		this->SetaudioFormatExtendedVersion(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatExtendedVersion"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioFormatExtendedPresenceFlag")))
	{
		// Deserialize string type
		this->SetaudioFormatExtendedPresenceFlag(Dc1Convert::TextToString(parent->getAttribute(X("audioFormatExtendedPresenceFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioProgramme"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioProgramme")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr tmp = CreateaudioFormatExtendedType_audioProgramme_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioProgramme(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioContent"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioContent")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr tmp = CreateaudioFormatExtendedType_audioContent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioContent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioObject"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioObject")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr tmp = CreateaudioFormatExtendedType_audioObject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioObject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioPackFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioPackFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioPackFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioPackFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioChannelFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioChannelFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioChannelFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioChannelFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioBlockFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioBlockFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioBlockFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioBlockFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioStreamFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioStreamFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioStreamFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioStreamFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioTrackFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioTrackFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioTrackFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioTrackFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioTrackUID"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioTrackUID")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr tmp = CreateaudioFormatExtendedType_audioTrackUID_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioTrackUID(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioFormatExtendedType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioProgramme")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr tmp = CreateaudioFormatExtendedType_audioProgramme_CollectionType; // FTT, check this
	this->SetaudioProgramme(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioContent")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr tmp = CreateaudioFormatExtendedType_audioContent_CollectionType; // FTT, check this
	this->SetaudioContent(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioObject")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr tmp = CreateaudioFormatExtendedType_audioObject_CollectionType; // FTT, check this
	this->SetaudioObject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioPackFormat")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioPackFormat_CollectionType; // FTT, check this
	this->SetaudioPackFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioChannelFormat")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioChannelFormat_CollectionType; // FTT, check this
	this->SetaudioChannelFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioBlockFormat")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioBlockFormat_CollectionType; // FTT, check this
	this->SetaudioBlockFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioStreamFormat")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioStreamFormat_CollectionType; // FTT, check this
	this->SetaudioStreamFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioTrackFormat")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr tmp = CreateaudioFormatExtendedType_audioTrackFormat_CollectionType; // FTT, check this
	this->SetaudioTrackFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioTrackUID")) == 0))
  {
	EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr tmp = CreateaudioFormatExtendedType_audioTrackUID_CollectionType; // FTT, check this
	this->SetaudioTrackUID(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioFormatExtendedType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioProgramme() != Dc1NodePtr())
		result.Insert(GetaudioProgramme());
	if (GetaudioContent() != Dc1NodePtr())
		result.Insert(GetaudioContent());
	if (GetaudioObject() != Dc1NodePtr())
		result.Insert(GetaudioObject());
	if (GetaudioPackFormat() != Dc1NodePtr())
		result.Insert(GetaudioPackFormat());
	if (GetaudioChannelFormat() != Dc1NodePtr())
		result.Insert(GetaudioChannelFormat());
	if (GetaudioBlockFormat() != Dc1NodePtr())
		result.Insert(GetaudioBlockFormat());
	if (GetaudioStreamFormat() != Dc1NodePtr())
		result.Insert(GetaudioStreamFormat());
	if (GetaudioTrackFormat() != Dc1NodePtr())
		result.Insert(GetaudioTrackFormat());
	if (GetaudioTrackUID() != Dc1NodePtr())
		result.Insert(GetaudioTrackUID());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMethodImpl.h


