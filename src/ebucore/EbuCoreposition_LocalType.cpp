
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtImplInclude.h


#include "EbuCoreposition_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreposition_LocalType::IEbuCoreposition_LocalType()
{

// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtPropInit.h

}

IEbuCoreposition_LocalType::~IEbuCoreposition_LocalType()
{
// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtPropCleanup.h

}

EbuCoreposition_LocalType::EbuCoreposition_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreposition_LocalType::~EbuCoreposition_LocalType()
{
	Cleanup();
}

void EbuCoreposition_LocalType::Init()
{
	// Init base
	m_Base = 0.0f;

	// Init attributes
	m_coordinate = NULL; // String
	m_coordinate_Exist = false;
	m_bound = NULL; // String
	m_bound_Exist = false;



// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtMyPropInit.h

}

void EbuCoreposition_LocalType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_coordinate); // String
	XMLString::release(&m_bound); // String
}

void EbuCoreposition_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use position_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in Iposition_LocalType
	const Dc1Ptr< EbuCoreposition_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	XMLString::release(&m_coordinate); // String
	if (tmp->Existcoordinate())
	{
		this->Setcoordinate(XMLString::replicate(tmp->Getcoordinate()));
	}
	else
	{
		Invalidatecoordinate();
	}
	}
	{
	XMLString::release(&m_bound); // String
	if (tmp->Existbound())
	{
		this->Setbound(XMLString::replicate(tmp->Getbound()));
	}
	else
	{
		Invalidatebound();
	}
	}
}

Dc1NodePtr EbuCoreposition_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreposition_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void EbuCoreposition_LocalType::SetContent(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreposition_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float EbuCoreposition_LocalType::GetContent() const
{
	return m_Base;
}
XMLCh * EbuCoreposition_LocalType::Getcoordinate() const
{
	return m_coordinate;
}

bool EbuCoreposition_LocalType::Existcoordinate() const
{
	return m_coordinate_Exist;
}
void EbuCoreposition_LocalType::Setcoordinate(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreposition_LocalType::Setcoordinate().");
	}
	m_coordinate = item;
	m_coordinate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreposition_LocalType::Invalidatecoordinate()
{
	m_coordinate_Exist = false;
}
XMLCh * EbuCoreposition_LocalType::Getbound() const
{
	return m_bound;
}

bool EbuCoreposition_LocalType::Existbound() const
{
	return m_bound_Exist;
}
void EbuCoreposition_LocalType::Setbound(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreposition_LocalType::Setbound().");
	}
	m_bound = item;
	m_bound_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreposition_LocalType::Invalidatebound()
{
	m_bound_Exist = false;
}

Dc1NodeEnum EbuCoreposition_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("coordinate")) == 0)
	{
		// coordinate is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("bound")) == 0)
	{
		// bound is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for position_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "position_LocalType");
	}
	return result;
}

XMLCh * EbuCoreposition_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreposition_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCoreposition_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// EbuCoreposition_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* EbuCoreposition_LocalType::ContentToString() const
{
	// EbuCoreposition_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void EbuCoreposition_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("position_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_coordinate_Exist)
	{
	// String
	if(m_coordinate != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("coordinate"), m_coordinate);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_bound_Exist)
	{
	// String
	if(m_bound != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("bound"), m_bound);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCoreposition_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("coordinate")))
	{
		// Deserialize string type
		this->Setcoordinate(Dc1Convert::TextToString(parent->getAttribute(X("coordinate"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("bound")))
	{
		// Deserialize string type
		this->Setbound(Dc1Convert::TextToString(parent->getAttribute(X("bound"))));
		* current = parent;
	}

  m_Base = Dc1Convert::TextToFloat(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreposition_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCoreposition_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreposition_LocalType_ExtMethodImpl.h


