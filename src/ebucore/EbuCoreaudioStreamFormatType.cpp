
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtImplInclude.h


#include "EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioStreamFormatType::IEbuCoreaudioStreamFormatType()
{

// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtPropInit.h

}

IEbuCoreaudioStreamFormatType::~IEbuCoreaudioStreamFormatType()
{
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtPropCleanup.h

}

EbuCoreaudioStreamFormatType::EbuCoreaudioStreamFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioStreamFormatType::~EbuCoreaudioStreamFormatType()
{
	Cleanup();
}

void EbuCoreaudioStreamFormatType::Init()
{

	// Init attributes
	m_audioStreamFormatID = NULL; // String
	m_audioStreamFormatName = NULL; // String
	m_audioStreamFormatName_Exist = false;
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioChannelFormatIDRef = EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr(); // Collection
	m_audioPackFormatIDRef = EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr(); // Collection
	m_audioTrackFormatIDRef = EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMyPropInit.h

}

void EbuCoreaudioStreamFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_audioStreamFormatID); // String
	XMLString::release(&m_audioStreamFormatName); // String
	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
	// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
	// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
	// Dc1Factory::DeleteObject(m_audioTrackFormatIDRef);
}

void EbuCoreaudioStreamFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioStreamFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioStreamFormatType
	const Dc1Ptr< EbuCoreaudioStreamFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioStreamFormatID); // String
		this->SetaudioStreamFormatID(XMLString::replicate(tmp->GetaudioStreamFormatID()));
	}
	{
	XMLString::release(&m_audioStreamFormatName); // String
	if (tmp->ExistaudioStreamFormatName())
	{
		this->SetaudioStreamFormatName(XMLString::replicate(tmp->GetaudioStreamFormatName()));
	}
	else
	{
		InvalidateaudioStreamFormatName();
	}
	}
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
		// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
		this->SetaudioChannelFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioChannelFormatIDRef()));
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		this->SetaudioPackFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioPackFormatIDRef()));
		// Dc1Factory::DeleteObject(m_audioTrackFormatIDRef);
		this->SetaudioTrackFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioTrackFormatIDRef()));
}

Dc1NodePtr EbuCoreaudioStreamFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioStreamFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioStreamFormatType::GetaudioStreamFormatID() const
{
	return m_audioStreamFormatID;
}

void EbuCoreaudioStreamFormatType::SetaudioStreamFormatID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetaudioStreamFormatID().");
	}
	m_audioStreamFormatID = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * EbuCoreaudioStreamFormatType::GetaudioStreamFormatName() const
{
	return m_audioStreamFormatName;
}

bool EbuCoreaudioStreamFormatType::ExistaudioStreamFormatName() const
{
	return m_audioStreamFormatName_Exist;
}
void EbuCoreaudioStreamFormatType::SetaudioStreamFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetaudioStreamFormatName().");
	}
	m_audioStreamFormatName = item;
	m_audioStreamFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateaudioStreamFormatName()
{
	m_audioStreamFormatName_Exist = false;
}
XMLCh * EbuCoreaudioStreamFormatType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCoreaudioStreamFormatType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCoreaudioStreamFormatType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCoreaudioStreamFormatType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCoreaudioStreamFormatType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCoreaudioStreamFormatType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCoreaudioStreamFormatType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCoreaudioStreamFormatType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCoreaudioStreamFormatType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCoreaudioStreamFormatType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCoreaudioStreamFormatType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCoreaudioStreamFormatType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCoreaudioStreamFormatType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCoreaudioStreamFormatType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCoreaudioStreamFormatType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr EbuCoreaudioStreamFormatType::GetaudioChannelFormatIDRef() const
{
		return m_audioChannelFormatIDRef;
}

EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr EbuCoreaudioStreamFormatType::GetaudioPackFormatIDRef() const
{
		return m_audioPackFormatIDRef;
}

EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr EbuCoreaudioStreamFormatType::GetaudioTrackFormatIDRef() const
{
		return m_audioTrackFormatIDRef;
}

void EbuCoreaudioStreamFormatType::SetaudioChannelFormatIDRef(const EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetaudioChannelFormatIDRef().");
	}
	if (m_audioChannelFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
		m_audioChannelFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioChannelFormatIDRef) m_audioChannelFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioChannelFormatIDRef != EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr())
		{
			m_audioChannelFormatIDRef->SetContentName(XMLString::transcode("audioChannelFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::SetaudioPackFormatIDRef(const EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetaudioPackFormatIDRef().");
	}
	if (m_audioPackFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		m_audioPackFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioPackFormatIDRef) m_audioPackFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioPackFormatIDRef != EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr())
		{
			m_audioPackFormatIDRef->SetContentName(XMLString::transcode("audioPackFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioStreamFormatType::SetaudioTrackFormatIDRef(const EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioStreamFormatType::SetaudioTrackFormatIDRef().");
	}
	if (m_audioTrackFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioTrackFormatIDRef);
		m_audioTrackFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioTrackFormatIDRef) m_audioTrackFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioTrackFormatIDRef != EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr())
		{
			m_audioTrackFormatIDRef->SetContentName(XMLString::transcode("audioTrackFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioStreamFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioStreamFormatID")) == 0)
	{
		// audioStreamFormatID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioStreamFormatName")) == 0)
	{
		// audioStreamFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLabel")) == 0)
	{
		// formatLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatDefinition")) == 0)
	{
		// formatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLink")) == 0)
	{
		// formatLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatSource")) == 0)
	{
		// formatSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLanguage")) == 0)
	{
		// formatLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioChannelFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioPackFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioTrackFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioStreamFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioStreamFormatType");
	}
	return result;
}

XMLCh * EbuCoreaudioStreamFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioStreamFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioStreamFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioStreamFormatType"));
	// Attribute Serialization:
	// String
	if(m_audioStreamFormatID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioStreamFormatID"), m_audioStreamFormatID);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioStreamFormatName_Exist)
	{
	// String
	if(m_audioStreamFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioStreamFormatName"), m_audioStreamFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioChannelFormatIDRef != EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioChannelFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioPackFormatIDRef != EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioPackFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioTrackFormatIDRef != EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioTrackFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioStreamFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioStreamFormatID")))
	{
		// Deserialize string type
		this->SetaudioStreamFormatID(Dc1Convert::TextToString(parent->getAttribute(X("audioStreamFormatID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioStreamFormatName")))
	{
		// Deserialize string type
		this->SetaudioStreamFormatName(Dc1Convert::TextToString(parent->getAttribute(X("audioStreamFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioChannelFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioChannelFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioChannelFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioChannelFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioPackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioPackFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioPackFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioPackFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioTrackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioTrackFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioTrackFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioTrackFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioStreamFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioChannelFormatIDRef")) == 0))
  {
	EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioChannelFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioChannelFormatIDRef(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioPackFormatIDRef")) == 0))
  {
	EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioPackFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioPackFormatIDRef(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioTrackFormatIDRef")) == 0))
  {
	EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr tmp = CreateaudioStreamFormatType_audioTrackFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioTrackFormatIDRef(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioStreamFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioChannelFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioChannelFormatIDRef());
	if (GetaudioPackFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioPackFormatIDRef());
	if (GetaudioTrackFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioTrackFormatIDRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMethodImpl.h


