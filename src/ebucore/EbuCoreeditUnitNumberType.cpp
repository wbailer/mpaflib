
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtImplInclude.h


#include "EbuCoreeditUnitNumberType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreeditUnitNumberType::IEbuCoreeditUnitNumberType()
{

// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtPropInit.h

}

IEbuCoreeditUnitNumberType::~IEbuCoreeditUnitNumberType()
{
// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtPropCleanup.h

}

EbuCoreeditUnitNumberType::EbuCoreeditUnitNumberType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreeditUnitNumberType::~EbuCoreeditUnitNumberType()
{
	Cleanup();
}

void EbuCoreeditUnitNumberType::Init()
{
	// Init base
	m_Base = 0;

	// Init attributes
	m_editRate = 1; // Value
	m_editRate_Exist = false;
	m_factorNumerator = 1; // Value
	m_factorNumerator_Default = 1; // Default value
	m_factorNumerator_Exist = false;
	m_factorDenominator = 1; // Value
	m_factorDenominator_Default = 1; // Default value
	m_factorDenominator_Exist = false;



// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtMyPropInit.h

}

void EbuCoreeditUnitNumberType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtMyPropCleanup.h


}

void EbuCoreeditUnitNumberType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use editUnitNumberTypePtr, since we
	// might need GetBase(), which isn't defined in IeditUnitNumberType
	const Dc1Ptr< EbuCoreeditUnitNumberType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	if (tmp->ExisteditRate())
	{
		this->SeteditRate(tmp->GeteditRate());
	}
	else
	{
		InvalidateeditRate();
	}
	}
	{
	if (tmp->ExistfactorNumerator())
	{
		this->SetfactorNumerator(tmp->GetfactorNumerator());
	}
	else
	{
		InvalidatefactorNumerator();
	}
	}
	{
	if (tmp->ExistfactorDenominator())
	{
		this->SetfactorDenominator(tmp->GetfactorDenominator());
	}
	else
	{
		InvalidatefactorDenominator();
	}
	}
}

Dc1NodePtr EbuCoreeditUnitNumberType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreeditUnitNumberType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void EbuCoreeditUnitNumberType::SetContent(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreeditUnitNumberType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int EbuCoreeditUnitNumberType::GetContent() const
{
	return m_Base;
}
unsigned EbuCoreeditUnitNumberType::GeteditRate() const
{
	return m_editRate;
}

bool EbuCoreeditUnitNumberType::ExisteditRate() const
{
	return m_editRate_Exist;
}
void EbuCoreeditUnitNumberType::SeteditRate(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreeditUnitNumberType::SeteditRate().");
	}
	m_editRate = item;
	m_editRate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreeditUnitNumberType::InvalidateeditRate()
{
	m_editRate_Exist = false;
}
unsigned EbuCoreeditUnitNumberType::GetfactorNumerator() const
{
	if (this->ExistfactorNumerator()) {
		return m_factorNumerator;
	} else {
		return m_factorNumerator_Default;
	}
}

bool EbuCoreeditUnitNumberType::ExistfactorNumerator() const
{
	return m_factorNumerator_Exist;
}
void EbuCoreeditUnitNumberType::SetfactorNumerator(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreeditUnitNumberType::SetfactorNumerator().");
	}
	m_factorNumerator = item;
	m_factorNumerator_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreeditUnitNumberType::InvalidatefactorNumerator()
{
	m_factorNumerator_Exist = false;
}
unsigned EbuCoreeditUnitNumberType::GetfactorDenominator() const
{
	if (this->ExistfactorDenominator()) {
		return m_factorDenominator;
	} else {
		return m_factorDenominator_Default;
	}
}

bool EbuCoreeditUnitNumberType::ExistfactorDenominator() const
{
	return m_factorDenominator_Exist;
}
void EbuCoreeditUnitNumberType::SetfactorDenominator(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreeditUnitNumberType::SetfactorDenominator().");
	}
	m_factorDenominator = item;
	m_factorDenominator_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreeditUnitNumberType::InvalidatefactorDenominator()
{
	m_factorDenominator_Exist = false;
}

Dc1NodeEnum EbuCoreeditUnitNumberType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("editRate")) == 0)
	{
		// editRate is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("factorNumerator")) == 0)
	{
		// factorNumerator is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("factorDenominator")) == 0)
	{
		// factorDenominator is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for editUnitNumberType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "editUnitNumberType");
	}
	return result;
}

XMLCh * EbuCoreeditUnitNumberType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreeditUnitNumberType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool EbuCoreeditUnitNumberType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// EbuCoreeditUnitNumberType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* EbuCoreeditUnitNumberType::ContentToString() const
{
	// EbuCoreeditUnitNumberType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void EbuCoreeditUnitNumberType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("editUnitNumberType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_editRate_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_editRate);
		element->setAttributeNS(X(""), X("editRate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_factorNumerator_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_factorNumerator);
		element->setAttributeNS(X(""), X("factorNumerator"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_factorDenominator_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_factorDenominator);
		element->setAttributeNS(X(""), X("factorDenominator"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool EbuCoreeditUnitNumberType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("editRate")))
	{
		// deserialize value type
		this->SeteditRate(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("editRate"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("factorNumerator")))
	{
		// deserialize value type
		this->SetfactorNumerator(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("factorNumerator"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("factorDenominator")))
	{
		// deserialize value type
		this->SetfactorDenominator(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("factorDenominator"))));
		* current = parent;
	}

  m_Base = Dc1Convert::TextToInt(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreeditUnitNumberType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum EbuCoreeditUnitNumberType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreeditUnitNumberType_ExtMethodImpl.h


