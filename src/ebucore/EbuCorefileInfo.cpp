
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorefileInfo_ExtImplInclude.h


#include "EbuCorefileInfo_mimeType_CollectionType.h"
#include "EbuCorefileInfo_locator_CollectionType.h"
#include "EbuCorehashType.h"
#include "EbuCorefileInfo.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorefileInfo_mimeType_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:mimeType
#include "EbuCorefileInfo_locator_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:locator

#include <assert.h>
IEbuCorefileInfo::IEbuCorefileInfo()
{

// no includefile for extension defined 
// file EbuCorefileInfo_ExtPropInit.h

}

IEbuCorefileInfo::~IEbuCorefileInfo()
{
// no includefile for extension defined 
// file EbuCorefileInfo_ExtPropCleanup.h

}

EbuCorefileInfo::EbuCorefileInfo()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorefileInfo::~EbuCorefileInfo()
{
	Cleanup();
}

void EbuCorefileInfo::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_fileSize = (0); // Value

	m_fileSize_Exist = false;
	m_fileName = NULL; // Optional String
	m_fileName_Exist = false;
	m_mimeType = EbuCorefileInfo_mimeType_CollectionPtr(); // Collection
	m_locator = EbuCorefileInfo_locator_CollectionPtr(); // Collection
	m_hash = EbuCorehashPtr(); // Class
	m_hash_Exist = false;


// no includefile for extension defined 
// file EbuCorefileInfo_ExtMyPropInit.h

}

void EbuCorefileInfo::Cleanup()
{
// no includefile for extension defined 
// file EbuCorefileInfo_ExtMyPropCleanup.h


	XMLString::release(&this->m_fileName);
	this->m_fileName = NULL;
	// Dc1Factory::DeleteObject(m_mimeType);
	// Dc1Factory::DeleteObject(m_locator);
	// Dc1Factory::DeleteObject(m_hash);
}

void EbuCorefileInfo::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use fileInfoPtr, since we
	// might need GetBase(), which isn't defined in IfileInfo
	const Dc1Ptr< EbuCorefileInfo > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidfileSize())
	{
		this->SetfileSize(tmp->GetfileSize());
	}
	else
	{
		InvalidatefileSize();
	}
	if (tmp->IsValidfileName())
	{
		this->SetfileName(XMLString::replicate(tmp->GetfileName()));
	}
	else
	{
		InvalidatefileName();
	}
		// Dc1Factory::DeleteObject(m_mimeType);
		this->SetmimeType(Dc1Factory::CloneObject(tmp->GetmimeType()));
		// Dc1Factory::DeleteObject(m_locator);
		this->Setlocator(Dc1Factory::CloneObject(tmp->Getlocator()));
	if (tmp->IsValidhash())
	{
		// Dc1Factory::DeleteObject(m_hash);
		this->Sethash(Dc1Factory::CloneObject(tmp->Gethash()));
	}
	else
	{
		Invalidatehash();
	}
}

Dc1NodePtr EbuCorefileInfo::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorefileInfo::GetBaseRoot() const
{
	return m_myself.getPointer();
}


int EbuCorefileInfo::GetfileSize() const
{
		return m_fileSize;
}

// Element is optional
bool EbuCorefileInfo::IsValidfileSize() const
{
	return m_fileSize_Exist;
}

XMLCh * EbuCorefileInfo::GetfileName() const
{
		return m_fileName;
}

// Element is optional
bool EbuCorefileInfo::IsValidfileName() const
{
	return m_fileName_Exist;
}

EbuCorefileInfo_mimeType_CollectionPtr EbuCorefileInfo::GetmimeType() const
{
		return m_mimeType;
}

EbuCorefileInfo_locator_CollectionPtr EbuCorefileInfo::Getlocator() const
{
		return m_locator;
}

EbuCorehashPtr EbuCorefileInfo::Gethash() const
{
		return m_hash;
}

// Element is optional
bool EbuCorefileInfo::IsValidhash() const
{
	return m_hash_Exist;
}

void EbuCorefileInfo::SetfileSize(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorefileInfo::SetfileSize().");
	}
	if (m_fileSize != item || m_fileSize_Exist == false)
	{
		m_fileSize = item;
		m_fileSize_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorefileInfo::InvalidatefileSize()
{
	m_fileSize_Exist = false;
}
void EbuCorefileInfo::SetfileName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorefileInfo::SetfileName().");
	}
	if (m_fileName != item || m_fileName_Exist == false)
	{
		XMLString::release(&m_fileName);
		m_fileName = item;
		m_fileName_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorefileInfo::InvalidatefileName()
{
	m_fileName_Exist = false;
}
void EbuCorefileInfo::SetmimeType(const EbuCorefileInfo_mimeType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorefileInfo::SetmimeType().");
	}
	if (m_mimeType != item)
	{
		// Dc1Factory::DeleteObject(m_mimeType);
		m_mimeType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_mimeType) m_mimeType->SetParent(m_myself.getPointer());
		if(m_mimeType != EbuCorefileInfo_mimeType_CollectionPtr())
		{
			m_mimeType->SetContentName(XMLString::transcode("mimeType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorefileInfo::Setlocator(const EbuCorefileInfo_locator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorefileInfo::Setlocator().");
	}
	if (m_locator != item)
	{
		// Dc1Factory::DeleteObject(m_locator);
		m_locator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_locator) m_locator->SetParent(m_myself.getPointer());
		if(m_locator != EbuCorefileInfo_locator_CollectionPtr())
		{
			m_locator->SetContentName(XMLString::transcode("locator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorefileInfo::Sethash(const EbuCorehashPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorefileInfo::Sethash().");
	}
	if (m_hash != item || m_hash_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_hash);
		m_hash = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_hash) m_hash->SetParent(m_myself.getPointer());
		if (m_hash && m_hash->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:hashType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_hash->UseTypeAttribute = true;
		}
		m_hash_Exist = true;
		if(m_hash != EbuCorehashPtr())
		{
			m_hash->SetContentName(XMLString::transcode("hash"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorefileInfo::Invalidatehash()
{
	m_hash_Exist = false;
}

Dc1NodeEnum EbuCorefileInfo::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("mimeType")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:mimeType is item of type fileInfo_mimeType_LocalType
		// in element collection fileInfo_mimeType_CollectionType
		
		context->Found = true;
		EbuCorefileInfo_mimeType_CollectionPtr coll = GetmimeType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatefileInfo_mimeType_CollectionType; // FTT, check this
				SetmimeType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_mimeType_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorefileInfo_mimeType_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("locator")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:locator is item of type fileInfo_locator_LocalType
		// in element collection fileInfo_locator_CollectionType
		
		context->Found = true;
		EbuCorefileInfo_locator_CollectionPtr coll = Getlocator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatefileInfo_locator_CollectionType; // FTT, check this
				Setlocator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_locator_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorefileInfo_locator_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("hash")) == 0)
	{
		// hash is simple element hashType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Gethash()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:hashType")))) != empty)
			{
				// Is type allowed
				EbuCorehashPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Sethash(p, client);
					if((p = Gethash()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for fileInfo
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "fileInfo");
	}
	return result;
}

XMLCh * EbuCorefileInfo::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorefileInfo::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorefileInfo::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("fileInfo"));
	// Element serialization:
	if(m_fileSize_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_fileSize);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:ebu:metadata-schema:ebuCore_2014"), X("fileSize"), true);
		XMLString::release(&tmp);
	}
	 // String
	if(m_fileName != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_fileName, X("urn:ebu:metadata-schema:ebuCore_2014"), X("fileName"), true);
	if (m_mimeType != EbuCorefileInfo_mimeType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_mimeType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_locator != EbuCorefileInfo_locator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_locator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_hash != EbuCorehashPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_hash->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("hash"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_hash->Serialize(doc, element, &elem);
	}

}

bool EbuCorefileInfo::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("fileSize"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetfileSize(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("fileName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetfileName(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetfileName(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("mimeType"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("mimeType")) == 0))
		{
			// Deserialize factory type
			EbuCorefileInfo_mimeType_CollectionPtr tmp = CreatefileInfo_mimeType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetmimeType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("locator"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("locator")) == 0))
		{
			// Deserialize factory type
			EbuCorefileInfo_locator_CollectionPtr tmp = CreatefileInfo_locator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setlocator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("hash"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("hash")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatehashType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Sethash(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCorefileInfo_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorefileInfo::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("mimeType")) == 0))
  {
	EbuCorefileInfo_mimeType_CollectionPtr tmp = CreatefileInfo_mimeType_CollectionType; // FTT, check this
	this->SetmimeType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("locator")) == 0))
  {
	EbuCorefileInfo_locator_CollectionPtr tmp = CreatefileInfo_locator_CollectionType; // FTT, check this
	this->Setlocator(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("hash")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatehashType; // FTT, check this
	}
	this->Sethash(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorefileInfo::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetmimeType() != Dc1NodePtr())
		result.Insert(GetmimeType());
	if (Getlocator() != Dc1NodePtr())
		result.Insert(Getlocator());
	if (Gethash() != Dc1NodePtr())
		result.Insert(Gethash());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorefileInfo_ExtMethodImpl.h


