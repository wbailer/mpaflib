
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCoreelementType.h"
#include "EbuCoreperiodOfTimeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreperiodOfTimeType::IEbuCoreperiodOfTimeType()
{

// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtPropInit.h

}

IEbuCoreperiodOfTimeType::~IEbuCoreperiodOfTimeType()
{
// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtPropCleanup.h

}

EbuCoreperiodOfTimeType::EbuCoreperiodOfTimeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreperiodOfTimeType::~EbuCoreperiodOfTimeType()
{
	Cleanup();
}

void EbuCoreperiodOfTimeType::Init()
{

	// Init attributes
	m_startYear = 2015; // Value
	m_startYear_Exist = false;
	m_startDate = W3CdatePtr(); // Pattern
	m_startDate_Exist = false;
	m_startTime = W3CtimePtr(); // Pattern
	m_startTime_Exist = false;
	m_endYear = 2015; // Value
	m_endYear_Exist = false;
	m_endDate = W3CdatePtr(); // Pattern
	m_endDate_Exist = false;
	m_endTime = W3CtimePtr(); // Pattern
	m_endTime_Exist = false;
	m_period = NULL; // String
	m_period_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_periodName = EbuCoreelementPtr(); // Class
	m_periodName_Exist = false;


// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtMyPropInit.h

}

void EbuCoreperiodOfTimeType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	XMLString::release(&m_period); // String
	// Dc1Factory::DeleteObject(m_periodName);
}

void EbuCoreperiodOfTimeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use periodOfTimeTypePtr, since we
	// might need GetBase(), which isn't defined in IperiodOfTimeType
	const Dc1Ptr< EbuCoreperiodOfTimeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExiststartYear())
	{
		this->SetstartYear(tmp->GetstartYear());
	}
	else
	{
		InvalidatestartYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	if (tmp->ExiststartDate())
	{
		this->SetstartDate(Dc1Factory::CloneObject(tmp->GetstartDate()));
	}
	else
	{
		InvalidatestartDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	if (tmp->ExiststartTime())
	{
		this->SetstartTime(Dc1Factory::CloneObject(tmp->GetstartTime()));
	}
	else
	{
		InvalidatestartTime();
	}
	}
	{
	if (tmp->ExistendYear())
	{
		this->SetendYear(tmp->GetendYear());
	}
	else
	{
		InvalidateendYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	if (tmp->ExistendDate())
	{
		this->SetendDate(Dc1Factory::CloneObject(tmp->GetendDate()));
	}
	else
	{
		InvalidateendDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	if (tmp->ExistendTime())
	{
		this->SetendTime(Dc1Factory::CloneObject(tmp->GetendTime()));
	}
	else
	{
		InvalidateendTime();
	}
	}
	{
	XMLString::release(&m_period); // String
	if (tmp->Existperiod())
	{
		this->Setperiod(XMLString::replicate(tmp->Getperiod()));
	}
	else
	{
		Invalidateperiod();
	}
	}
	if (tmp->IsValidperiodName())
	{
		// Dc1Factory::DeleteObject(m_periodName);
		this->SetperiodName(Dc1Factory::CloneObject(tmp->GetperiodName()));
	}
	else
	{
		InvalidateperiodName();
	}
}

Dc1NodePtr EbuCoreperiodOfTimeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreperiodOfTimeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


int EbuCoreperiodOfTimeType::GetstartYear() const
{
	return m_startYear;
}

bool EbuCoreperiodOfTimeType::ExiststartYear() const
{
	return m_startYear_Exist;
}
void EbuCoreperiodOfTimeType::SetstartYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetstartYear().");
	}
	m_startYear = item;
	m_startYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidatestartYear()
{
	m_startYear_Exist = false;
}
W3CdatePtr EbuCoreperiodOfTimeType::GetstartDate() const
{
	return m_startDate;
}

bool EbuCoreperiodOfTimeType::ExiststartDate() const
{
	return m_startDate_Exist;
}
void EbuCoreperiodOfTimeType::SetstartDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetstartDate().");
	}
	m_startDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startDate) m_startDate->SetParent(m_myself.getPointer());
	m_startDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidatestartDate()
{
	m_startDate_Exist = false;
}
W3CtimePtr EbuCoreperiodOfTimeType::GetstartTime() const
{
	return m_startTime;
}

bool EbuCoreperiodOfTimeType::ExiststartTime() const
{
	return m_startTime_Exist;
}
void EbuCoreperiodOfTimeType::SetstartTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetstartTime().");
	}
	m_startTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startTime) m_startTime->SetParent(m_myself.getPointer());
	m_startTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidatestartTime()
{
	m_startTime_Exist = false;
}
int EbuCoreperiodOfTimeType::GetendYear() const
{
	return m_endYear;
}

bool EbuCoreperiodOfTimeType::ExistendYear() const
{
	return m_endYear_Exist;
}
void EbuCoreperiodOfTimeType::SetendYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetendYear().");
	}
	m_endYear = item;
	m_endYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidateendYear()
{
	m_endYear_Exist = false;
}
W3CdatePtr EbuCoreperiodOfTimeType::GetendDate() const
{
	return m_endDate;
}

bool EbuCoreperiodOfTimeType::ExistendDate() const
{
	return m_endDate_Exist;
}
void EbuCoreperiodOfTimeType::SetendDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetendDate().");
	}
	m_endDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endDate) m_endDate->SetParent(m_myself.getPointer());
	m_endDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidateendDate()
{
	m_endDate_Exist = false;
}
W3CtimePtr EbuCoreperiodOfTimeType::GetendTime() const
{
	return m_endTime;
}

bool EbuCoreperiodOfTimeType::ExistendTime() const
{
	return m_endTime_Exist;
}
void EbuCoreperiodOfTimeType::SetendTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetendTime().");
	}
	m_endTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endTime) m_endTime->SetParent(m_myself.getPointer());
	m_endTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidateendTime()
{
	m_endTime_Exist = false;
}
XMLCh * EbuCoreperiodOfTimeType::Getperiod() const
{
	return m_period;
}

bool EbuCoreperiodOfTimeType::Existperiod() const
{
	return m_period_Exist;
}
void EbuCoreperiodOfTimeType::Setperiod(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::Setperiod().");
	}
	m_period = item;
	m_period_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::Invalidateperiod()
{
	m_period_Exist = false;
}
EbuCoreelementPtr EbuCoreperiodOfTimeType::GetperiodName() const
{
		return m_periodName;
}

// Element is optional
bool EbuCoreperiodOfTimeType::IsValidperiodName() const
{
	return m_periodName_Exist;
}

void EbuCoreperiodOfTimeType::SetperiodName(const EbuCoreelementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreperiodOfTimeType::SetperiodName().");
	}
	if (m_periodName != item || m_periodName_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_periodName);
		m_periodName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_periodName) m_periodName->SetParent(m_myself.getPointer());
		if (m_periodName && m_periodName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_periodName->UseTypeAttribute = true;
		}
		m_periodName_Exist = true;
		if(m_periodName != EbuCoreelementPtr())
		{
			m_periodName->SetContentName(XMLString::transcode("periodName"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreperiodOfTimeType::InvalidateperiodName()
{
	m_periodName_Exist = false;
}

Dc1NodeEnum EbuCoreperiodOfTimeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startYear")) == 0)
	{
		// startYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startDate")) == 0)
	{
		// startDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startTime")) == 0)
	{
		// startTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endYear")) == 0)
	{
		// endYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endDate")) == 0)
	{
		// endDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endTime")) == 0)
	{
		// endTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("period")) == 0)
	{
		// period is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("periodName")) == 0)
	{
		// periodName is simple element elementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetperiodName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))) != empty)
			{
				// Is type allowed
				EbuCoreelementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetperiodName(p, client);
					if((p = GetperiodName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for periodOfTimeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "periodOfTimeType");
	}
	return result;
}

XMLCh * EbuCoreperiodOfTimeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreperiodOfTimeType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreperiodOfTimeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("periodOfTimeType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_startYear);
		element->setAttributeNS(X(""), X("startYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startDate_Exist)
	{
	// Pattern
	if (m_startDate != W3CdatePtr())
	{
		XMLCh * tmp = m_startDate->ToText();
		element->setAttributeNS(X(""), X("startDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startTime_Exist)
	{
	// Pattern
	if (m_startTime != W3CtimePtr())
	{
		XMLCh * tmp = m_startTime->ToText();
		element->setAttributeNS(X(""), X("startTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_endYear);
		element->setAttributeNS(X(""), X("endYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endDate_Exist)
	{
	// Pattern
	if (m_endDate != W3CdatePtr())
	{
		XMLCh * tmp = m_endDate->ToText();
		element->setAttributeNS(X(""), X("endDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endTime_Exist)
	{
	// Pattern
	if (m_endTime != W3CtimePtr())
	{
		XMLCh * tmp = m_endTime->ToText();
		element->setAttributeNS(X(""), X("endTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_period_Exist)
	{
	// String
	if(m_period != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("period"), m_period);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_periodName != EbuCoreelementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_periodName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("periodName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_periodName->Serialize(doc, element, &elem);
	}

}

bool EbuCoreperiodOfTimeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startYear")))
	{
		// deserialize value type
		this->SetstartYear(Dc1Convert::TextToInt(parent->getAttribute(X("startYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startDate")));
		this->SetstartDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startTime")));
		this->SetstartTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endYear")))
	{
		// deserialize value type
		this->SetendYear(Dc1Convert::TextToInt(parent->getAttribute(X("endYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endDate")));
		this->SetendDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endTime")));
		this->SetendTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("period")))
	{
		// Deserialize string type
		this->Setperiod(Dc1Convert::TextToString(parent->getAttribute(X("period"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("periodName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("periodName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEbuCoreelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetperiodName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreperiodOfTimeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("periodName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEbuCoreelementType; // FTT, check this
	}
	this->SetperiodName(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoreperiodOfTimeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetperiodName() != Dc1NodePtr())
		result.Insert(GetperiodName());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreperiodOfTimeType_ExtMethodImpl.h


