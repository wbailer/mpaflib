
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtImplInclude.h


#include "EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType.h"
#include "EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioPackFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoreaudioPackFormatType::IEbuCoreaudioPackFormatType()
{

// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtPropInit.h

}

IEbuCoreaudioPackFormatType::~IEbuCoreaudioPackFormatType()
{
// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtPropCleanup.h

}

EbuCoreaudioPackFormatType::EbuCoreaudioPackFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioPackFormatType::~EbuCoreaudioPackFormatType()
{
	Cleanup();
}

void EbuCoreaudioPackFormatType::Init()
{

	// Init attributes
	m_absoluteDistance = 0.0f; // Value
	m_absoluteDistance_Exist = false;
	m_audioPackFormatID = NULL; // String
	m_audioPackFormatID_Exist = false;
	m_audioPackFormatName = NULL; // String
	m_audioPackFormatName_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_importance = 0; // Value
	m_importance_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_audioChannelFormatIDRef = EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr(); // Collection
	m_audioPackFormatIDRef = EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtMyPropInit.h

}

void EbuCoreaudioPackFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_audioPackFormatID); // String
	XMLString::release(&m_audioPackFormatName); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
	// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
}

void EbuCoreaudioPackFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioPackFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioPackFormatType
	const Dc1Ptr< EbuCoreaudioPackFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExistabsoluteDistance())
	{
		this->SetabsoluteDistance(tmp->GetabsoluteDistance());
	}
	else
	{
		InvalidateabsoluteDistance();
	}
	}
	{
	XMLString::release(&m_audioPackFormatID); // String
	if (tmp->ExistaudioPackFormatID())
	{
		this->SetaudioPackFormatID(XMLString::replicate(tmp->GetaudioPackFormatID()));
	}
	else
	{
		InvalidateaudioPackFormatID();
	}
	}
	{
	XMLString::release(&m_audioPackFormatName); // String
	if (tmp->ExistaudioPackFormatName())
	{
		this->SetaudioPackFormatName(XMLString::replicate(tmp->GetaudioPackFormatName()));
	}
	else
	{
		InvalidateaudioPackFormatName();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	if (tmp->Existimportance())
	{
		this->Setimportance(tmp->Getimportance());
	}
	else
	{
		Invalidateimportance();
	}
	}
		// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
		this->SetaudioChannelFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioChannelFormatIDRef()));
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		this->SetaudioPackFormatIDRef(Dc1Factory::CloneObject(tmp->GetaudioPackFormatIDRef()));
}

Dc1NodePtr EbuCoreaudioPackFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioPackFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


float EbuCoreaudioPackFormatType::GetabsoluteDistance() const
{
	return m_absoluteDistance;
}

bool EbuCoreaudioPackFormatType::ExistabsoluteDistance() const
{
	return m_absoluteDistance_Exist;
}
void EbuCoreaudioPackFormatType::SetabsoluteDistance(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SetabsoluteDistance().");
	}
	m_absoluteDistance = item;
	m_absoluteDistance_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidateabsoluteDistance()
{
	m_absoluteDistance_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GetaudioPackFormatID() const
{
	return m_audioPackFormatID;
}

bool EbuCoreaudioPackFormatType::ExistaudioPackFormatID() const
{
	return m_audioPackFormatID_Exist;
}
void EbuCoreaudioPackFormatType::SetaudioPackFormatID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SetaudioPackFormatID().");
	}
	m_audioPackFormatID = item;
	m_audioPackFormatID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidateaudioPackFormatID()
{
	m_audioPackFormatID_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GetaudioPackFormatName() const
{
	return m_audioPackFormatName;
}

bool EbuCoreaudioPackFormatType::ExistaudioPackFormatName() const
{
	return m_audioPackFormatName_Exist;
}
void EbuCoreaudioPackFormatType::SetaudioPackFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SetaudioPackFormatName().");
	}
	m_audioPackFormatName = item;
	m_audioPackFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidateaudioPackFormatName()
{
	m_audioPackFormatName_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreaudioPackFormatType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreaudioPackFormatType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreaudioPackFormatType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreaudioPackFormatType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreaudioPackFormatType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreaudioPackFormatType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreaudioPackFormatType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreaudioPackFormatType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreaudioPackFormatType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreaudioPackFormatType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreaudioPackFormatType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
int EbuCoreaudioPackFormatType::Getimportance() const
{
	return m_importance;
}

bool EbuCoreaudioPackFormatType::Existimportance() const
{
	return m_importance_Exist;
}
void EbuCoreaudioPackFormatType::Setimportance(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::Setimportance().");
	}
	m_importance = item;
	m_importance_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::Invalidateimportance()
{
	m_importance_Exist = false;
}
EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr EbuCoreaudioPackFormatType::GetaudioChannelFormatIDRef() const
{
		return m_audioChannelFormatIDRef;
}

EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr EbuCoreaudioPackFormatType::GetaudioPackFormatIDRef() const
{
		return m_audioPackFormatIDRef;
}

void EbuCoreaudioPackFormatType::SetaudioChannelFormatIDRef(const EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SetaudioChannelFormatIDRef().");
	}
	if (m_audioChannelFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioChannelFormatIDRef);
		m_audioChannelFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioChannelFormatIDRef) m_audioChannelFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioChannelFormatIDRef != EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr())
		{
			m_audioChannelFormatIDRef->SetContentName(XMLString::transcode("audioChannelFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioPackFormatType::SetaudioPackFormatIDRef(const EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioPackFormatType::SetaudioPackFormatIDRef().");
	}
	if (m_audioPackFormatIDRef != item)
	{
		// Dc1Factory::DeleteObject(m_audioPackFormatIDRef);
		m_audioPackFormatIDRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioPackFormatIDRef) m_audioPackFormatIDRef->SetParent(m_myself.getPointer());
		if(m_audioPackFormatIDRef != EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr())
		{
			m_audioPackFormatIDRef->SetContentName(XMLString::transcode("audioPackFormatIDRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioPackFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  9, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("absoluteDistance")) == 0)
	{
		// absoluteDistance is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioPackFormatID")) == 0)
	{
		// audioPackFormatID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioPackFormatName")) == 0)
	{
		// audioPackFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("importance")) == 0)
	{
		// importance is simple attribute int
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioChannelFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioPackFormatIDRef")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioPackFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioPackFormatType");
	}
	return result;
}

XMLCh * EbuCoreaudioPackFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioPackFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioPackFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioPackFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_absoluteDistance_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_absoluteDistance);
		element->setAttributeNS(X(""), X("absoluteDistance"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioPackFormatID_Exist)
	{
	// String
	if(m_audioPackFormatID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioPackFormatID"), m_audioPackFormatID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioPackFormatName_Exist)
	{
	// String
	if(m_audioPackFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioPackFormatName"), m_audioPackFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_importance_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_importance);
		element->setAttributeNS(X(""), X("importance"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_audioChannelFormatIDRef != EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioChannelFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioPackFormatIDRef != EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioPackFormatIDRef->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioPackFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("absoluteDistance")))
	{
		// deserialize value type
		this->SetabsoluteDistance(Dc1Convert::TextToFloat(parent->getAttribute(X("absoluteDistance"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioPackFormatID")))
	{
		// Deserialize string type
		this->SetaudioPackFormatID(Dc1Convert::TextToString(parent->getAttribute(X("audioPackFormatID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioPackFormatName")))
	{
		// Deserialize string type
		this->SetaudioPackFormatName(Dc1Convert::TextToString(parent->getAttribute(X("audioPackFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("importance")))
	{
		// deserialize value type
		this->Setimportance(Dc1Convert::TextToInt(parent->getAttribute(X("importance"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioChannelFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioChannelFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr tmp = CreateaudioPackFormatType_audioChannelFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioChannelFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioPackFormatIDRef"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioPackFormatIDRef")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioPackFormatType_audioPackFormatIDRef_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioPackFormatIDRef(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioPackFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioChannelFormatIDRef")) == 0))
  {
	EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr tmp = CreateaudioPackFormatType_audioChannelFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioChannelFormatIDRef(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioPackFormatIDRef")) == 0))
  {
	EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr tmp = CreateaudioPackFormatType_audioPackFormatIDRef_CollectionType; // FTT, check this
	this->SetaudioPackFormatIDRef(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioPackFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetaudioChannelFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioChannelFormatIDRef());
	if (GetaudioPackFormatIDRef() != Dc1NodePtr())
		result.Insert(GetaudioPackFormatIDRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioPackFormatType_ExtMethodImpl.h


