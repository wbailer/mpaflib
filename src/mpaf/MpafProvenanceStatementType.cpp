
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtImplInclude.h


#include "MpafStatementType.h"
#include "MpafProvenanceStatementType_Operator_CollectionType.h"
#include "MpafProvenanceStatementType_Activity_CollectionType.h"
#include "W3CNMTOKENS.h"
#include "MpafProvenanceStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "MpafOperatorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Operator
#include "MpafActivityType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Activity

#include <assert.h>
IMpafProvenanceStatementType::IMpafProvenanceStatementType()
{

// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtPropInit.h

}

IMpafProvenanceStatementType::~IMpafProvenanceStatementType()
{
// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtPropCleanup.h

}

MpafProvenanceStatementType::MpafProvenanceStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafProvenanceStatementType::~MpafProvenanceStatementType()
{
	Cleanup();
}

void MpafProvenanceStatementType::Init()
{
	// Init base
	m_Base = CreateMpafStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Operator = MpafProvenanceStatementType_Operator_CollectionPtr(); // Collection
	m_Activity = MpafProvenanceStatementType_Activity_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtMyPropInit.h

}

void MpafProvenanceStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Operator);
	// Dc1Factory::DeleteObject(m_Activity);
}

void MpafProvenanceStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ProvenanceStatementTypePtr, since we
	// might need GetBase(), which isn't defined in IProvenanceStatementType
	const Dc1Ptr< MpafProvenanceStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafStatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafProvenanceStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Operator);
		this->SetOperator(Dc1Factory::CloneObject(tmp->GetOperator()));
		// Dc1Factory::DeleteObject(m_Activity);
		this->SetActivity(Dc1Factory::CloneObject(tmp->GetActivity()));
}

Dc1NodePtr MpafProvenanceStatementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafProvenanceStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafStatementType > MpafProvenanceStatementType::GetBase() const
{
	return m_Base;
}

MpafProvenanceStatementType_Operator_CollectionPtr MpafProvenanceStatementType::GetOperator() const
{
		return m_Operator;
}

MpafProvenanceStatementType_Activity_CollectionPtr MpafProvenanceStatementType::GetActivity() const
{
		return m_Activity;
}

void MpafProvenanceStatementType::SetOperator(const MpafProvenanceStatementType_Operator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::SetOperator().");
	}
	if (m_Operator != item)
	{
		// Dc1Factory::DeleteObject(m_Operator);
		m_Operator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Operator) m_Operator->SetParent(m_myself.getPointer());
		if(m_Operator != MpafProvenanceStatementType_Operator_CollectionPtr())
		{
			m_Operator->SetContentName(XMLString::transcode("Operator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafProvenanceStatementType::SetActivity(const MpafProvenanceStatementType_Activity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::SetActivity().");
	}
	if (m_Activity != item)
	{
		// Dc1Factory::DeleteObject(m_Activity);
		m_Activity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Activity) m_Activity->SetParent(m_myself.getPointer());
		if(m_Activity != MpafProvenanceStatementType_Activity_CollectionPtr())
		{
			m_Activity->SetContentName(XMLString::transcode("Activity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafProvenanceStatementType::GetmimeType() const
{
	return GetBase()->GetmimeType();
}

void MpafProvenanceStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::SetmimeType().");
	}
	GetBase()->SetmimeType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafProvenanceStatementType::Getref() const
{
	return GetBase()->Getref();
}

bool MpafProvenanceStatementType::Existref() const
{
	return GetBase()->Existref();
}
void MpafProvenanceStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::Setref().");
	}
	GetBase()->Setref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafProvenanceStatementType::Invalidateref()
{
	GetBase()->Invalidateref();
}
XMLCh * MpafProvenanceStatementType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool MpafProvenanceStatementType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void MpafProvenanceStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafProvenanceStatementType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
W3CNMTOKENSPtr MpafProvenanceStatementType::GetcontentEncoding() const
{
	return GetBase()->GetcontentEncoding();
}

bool MpafProvenanceStatementType::ExistcontentEncoding() const
{
	return GetBase()->ExistcontentEncoding();
}
void MpafProvenanceStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafProvenanceStatementType::SetcontentEncoding().");
	}
	GetBase()->SetcontentEncoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafProvenanceStatementType::InvalidatecontentEncoding()
{
	GetBase()->InvalidatecontentEncoding();
}

Dc1NodeEnum MpafProvenanceStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Operator")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Operator is item of abstract type OperatorType
		// in element collection ProvenanceStatementType_Operator_CollectionType
		
		context->Found = true;
		MpafProvenanceStatementType_Operator_CollectionPtr coll = GetOperator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateProvenanceStatementType_Operator_CollectionType; // FTT, check this
				SetOperator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				MpafOperatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Activity")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Activity is item of type ActivityType
		// in element collection ProvenanceStatementType_Activity_CollectionType
		
		context->Found = true;
		MpafProvenanceStatementType_Activity_CollectionPtr coll = GetActivity();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateProvenanceStatementType_Activity_CollectionType; // FTT, check this
				SetActivity(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType")))) != empty)
			{
				// Is type allowed
				MpafActivityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ProvenanceStatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ProvenanceStatementType");
	}
	return result;
}

XMLCh * MpafProvenanceStatementType::ToText() const
{
	return m_Base->ToText();
}


bool MpafProvenanceStatementType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafProvenanceStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("ProvenanceStatementType"));
	// Element serialization:
	if (m_Operator != MpafProvenanceStatementType_Operator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Operator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Activity != MpafProvenanceStatementType_Activity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Activity->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafProvenanceStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafStatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Operator"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Operator")) == 0))
		{
			// Deserialize factory type
			MpafProvenanceStatementType_Operator_CollectionPtr tmp = CreateProvenanceStatementType_Operator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOperator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Activity"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Activity")) == 0))
		{
			// Deserialize factory type
			MpafProvenanceStatementType_Activity_CollectionPtr tmp = CreateProvenanceStatementType_Activity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetActivity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafProvenanceStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Operator")) == 0))
  {
	MpafProvenanceStatementType_Operator_CollectionPtr tmp = CreateProvenanceStatementType_Operator_CollectionType; // FTT, check this
	this->SetOperator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Activity")) == 0))
  {
	MpafProvenanceStatementType_Activity_CollectionPtr tmp = CreateProvenanceStatementType_Activity_CollectionType; // FTT, check this
	this->SetActivity(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafProvenanceStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOperator() != Dc1NodePtr())
		result.Insert(GetOperator());
	if (GetActivity() != Dc1NodePtr())
		result.Insert(GetActivity());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafProvenanceStatementType_ExtMethodImpl.h


