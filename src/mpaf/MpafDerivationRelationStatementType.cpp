
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtImplInclude.h


#include "MpafRelationStatementType.h"
#include "Mpeg21RelatedIdentifier_LocalType.h"
#include "W3CNMTOKENS.h"
#include "MpafDerivationRelationStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafDerivationRelationStatementType::IMpafDerivationRelationStatementType()
{

// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtPropInit.h

}

IMpafDerivationRelationStatementType::~IMpafDerivationRelationStatementType()
{
// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtPropCleanup.h

}

MpafDerivationRelationStatementType::MpafDerivationRelationStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafDerivationRelationStatementType::~MpafDerivationRelationStatementType()
{
	Cleanup();
}

void MpafDerivationRelationStatementType::Init()
{
	// Init base
	m_Base = CreateRelationStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtMyPropInit.h

}

void MpafDerivationRelationStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void MpafDerivationRelationStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DerivationRelationStatementTypePtr, since we
	// might need GetBase(), which isn't defined in IDerivationRelationStatementType
	const Dc1Ptr< MpafDerivationRelationStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafRelationStatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafDerivationRelationStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr MpafDerivationRelationStatementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafDerivationRelationStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafRelationStatementType > MpafDerivationRelationStatementType::GetBase() const
{
	return m_Base;
}

Mpeg21RelatedIdentifier_LocalPtr MpafDerivationRelationStatementType::GetRelatedIdentifier() const
{
	return GetBase()->GetRelatedIdentifier();
}

// Element is optional
bool MpafDerivationRelationStatementType::IsValidRelatedIdentifier() const
{
	return GetBase()->IsValidRelatedIdentifier();
}

void MpafDerivationRelationStatementType::SetRelatedIdentifier(const Mpeg21RelatedIdentifier_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafDerivationRelationStatementType::SetRelatedIdentifier().");
	}
	GetBase()->SetRelatedIdentifier(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafDerivationRelationStatementType::InvalidateRelatedIdentifier()
{
	GetBase()->InvalidateRelatedIdentifier();
}
XMLCh * MpafDerivationRelationStatementType::GetmimeType() const
{
	return GetBase()->GetBase()->GetmimeType();
}

void MpafDerivationRelationStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafDerivationRelationStatementType::SetmimeType().");
	}
	GetBase()->GetBase()->SetmimeType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafDerivationRelationStatementType::Getref() const
{
	return GetBase()->GetBase()->Getref();
}

bool MpafDerivationRelationStatementType::Existref() const
{
	return GetBase()->GetBase()->Existref();
}
void MpafDerivationRelationStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafDerivationRelationStatementType::Setref().");
	}
	GetBase()->GetBase()->Setref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafDerivationRelationStatementType::Invalidateref()
{
	GetBase()->GetBase()->Invalidateref();
}
XMLCh * MpafDerivationRelationStatementType::Getencoding() const
{
	return GetBase()->GetBase()->Getencoding();
}

bool MpafDerivationRelationStatementType::Existencoding() const
{
	return GetBase()->GetBase()->Existencoding();
}
void MpafDerivationRelationStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafDerivationRelationStatementType::Setencoding().");
	}
	GetBase()->GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafDerivationRelationStatementType::Invalidateencoding()
{
	GetBase()->GetBase()->Invalidateencoding();
}
W3CNMTOKENSPtr MpafDerivationRelationStatementType::GetcontentEncoding() const
{
	return GetBase()->GetBase()->GetcontentEncoding();
}

bool MpafDerivationRelationStatementType::ExistcontentEncoding() const
{
	return GetBase()->GetBase()->ExistcontentEncoding();
}
void MpafDerivationRelationStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafDerivationRelationStatementType::SetcontentEncoding().");
	}
	GetBase()->GetBase()->SetcontentEncoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafDerivationRelationStatementType::InvalidatecontentEncoding()
{
	GetBase()->GetBase()->InvalidatecontentEncoding();
}

Dc1NodeEnum MpafDerivationRelationStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DerivationRelationStatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DerivationRelationStatementType");
	}
	return result;
}

XMLCh * MpafDerivationRelationStatementType::ToText() const
{
	return m_Base->ToText();
}


bool MpafDerivationRelationStatementType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafDerivationRelationStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("DerivationRelationStatementType"));
	// Element serialization:

}

bool MpafDerivationRelationStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafRelationStatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafDerivationRelationStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum MpafDerivationRelationStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRelatedIdentifier() != Dc1NodePtr())
		result.Insert(GetRelatedIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafDerivationRelationStatementType_ExtMethodImpl.h


