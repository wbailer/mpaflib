
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtImplInclude.h


#include "MpafGroupType.h"
#include "MpafPreservationObjectType.h"
#include "MpafGroupType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMpafGroupType_LocalType::IMpafGroupType_LocalType()
{

// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtPropInit.h

}

IMpafGroupType_LocalType::~IMpafGroupType_LocalType()
{
// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtPropCleanup.h

}

MpafGroupType_LocalType::MpafGroupType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafGroupType_LocalType::~MpafGroupType_LocalType()
{
	Cleanup();
}

void MpafGroupType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Container = MpafGroupPtr(); // Class
	m_Item = MpafPreservationObjectPtr(); // Class


// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtMyPropInit.h

}

void MpafGroupType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Container);
	// Dc1Factory::DeleteObject(m_Item);
}

void MpafGroupType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GroupType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IGroupType_LocalType
	const Dc1Ptr< MpafGroupType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Container);
		this->SetContainer(Dc1Factory::CloneObject(tmp->GetContainer()));
		// Dc1Factory::DeleteObject(m_Item);
		this->SetItem(Dc1Factory::CloneObject(tmp->GetItem()));
}

Dc1NodePtr MpafGroupType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafGroupType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


MpafGroupPtr MpafGroupType_LocalType::GetContainer() const
{
		return m_Container;
}

MpafPreservationObjectPtr MpafGroupType_LocalType::GetItem() const
{
		return m_Item;
}

// implementing setter for choice 
/* element */
void MpafGroupType_LocalType::SetContainer(const MpafGroupPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafGroupType_LocalType::SetContainer().");
	}
	if (m_Container != item)
	{
		// Dc1Factory::DeleteObject(m_Container);
		m_Container = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Container) m_Container->SetParent(m_myself.getPointer());
		if (m_Container && m_Container->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Container->UseTypeAttribute = true;
		}
		if(m_Container != MpafGroupPtr())
		{
			m_Container->SetContentName(XMLString::transcode("Container"));
		}
	// Dc1Factory::DeleteObject(m_Item);
	m_Item = MpafPreservationObjectPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void MpafGroupType_LocalType::SetItem(const MpafPreservationObjectPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafGroupType_LocalType::SetItem().");
	}
	if (m_Item != item)
	{
		// Dc1Factory::DeleteObject(m_Item);
		m_Item = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Item) m_Item->SetParent(m_myself.getPointer());
		if (m_Item && m_Item->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Item->UseTypeAttribute = true;
		}
		if(m_Item != MpafPreservationObjectPtr())
		{
			m_Item->SetContentName(XMLString::transcode("Item"));
		}
	// Dc1Factory::DeleteObject(m_Container);
	m_Container = MpafGroupPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: GroupType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum MpafGroupType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * MpafGroupType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafGroupType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafGroupType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Container != MpafGroupPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Container->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Container"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Container->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Item != MpafPreservationObjectPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Item->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Item"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Item->Serialize(doc, element, &elem);
	}

}

bool MpafGroupType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Container"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Container")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGroupType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContainer(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Item"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Item")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePreservationObjectType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetItem(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafGroupType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Container")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGroupType; // FTT, check this
	}
	this->SetContainer(child);
  }
  if (XMLString::compareString(elementname, X("Item")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePreservationObjectType; // FTT, check this
	}
	this->SetItem(child);
  }
  return child;
 
}

Dc1NodeEnum MpafGroupType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContainer() != Dc1NodePtr())
		result.Insert(GetContainer());
	if (GetItem() != Dc1NodePtr())
		result.Insert(GetItem());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafGroupType_LocalType_ExtMethodImpl.h


