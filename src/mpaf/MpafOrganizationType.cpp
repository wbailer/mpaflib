
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafOrganizationType_ExtImplInclude.h


#include "MpafAgentType.h"
#include "MpafOrganizationType_Name_CollectionType0.h"
#include "MpafOrganizationType_NameTerm_CollectionType0.h"
#include "Mp7JrsTermUseType.h"
#include "MpafOrganizationType_Contact_CollectionType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsElectronicAddressType.h"
#include "MpafOperatorType_Descriptor_CollectionType.h"
#include "MpafOrganizationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor
#include "MpafOrganizationType_Name_LocalType0.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Name
#include "MpafOrganizationType_NameTerm_LocalType0.h" // Element collection urn:mpeg:maf:schema:preservation:2015:NameTerm
#include "Mp7JrsAgentType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Contact

#include <assert.h>
IMpafOrganizationType::IMpafOrganizationType()
{

// no includefile for extension defined 
// file MpafOrganizationType_ExtPropInit.h

}

IMpafOrganizationType::~IMpafOrganizationType()
{
// no includefile for extension defined 
// file MpafOrganizationType_ExtPropCleanup.h

}

MpafOrganizationType::MpafOrganizationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafOrganizationType::~MpafOrganizationType()
{
	Cleanup();
}

void MpafOrganizationType::Init()
{
	// Init base
	m_Base = CreateAgentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Name = MpafOrganizationType_Name_Collection0Ptr(); // Collection
	m_NameTerm = MpafOrganizationType_NameTerm_Collection0Ptr(); // Collection
	m_Kind = Mp7JrsTermUsePtr(); // Class
	m_Kind_Exist = false;
	m_Contact = MpafOrganizationType_Contact_CollectionPtr(); // Collection
	m_Address = Mp7JrsPlacePtr(); // Class
	m_Address_Exist = false;
	m_ElectronicAddress = Mp7JrsElectronicAddressPtr(); // Class
	m_ElectronicAddress_Exist = false;


// no includefile for extension defined 
// file MpafOrganizationType_ExtMyPropInit.h

}

void MpafOrganizationType::Cleanup()
{
// no includefile for extension defined 
// file MpafOrganizationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_NameTerm);
	// Dc1Factory::DeleteObject(m_Kind);
	// Dc1Factory::DeleteObject(m_Contact);
	// Dc1Factory::DeleteObject(m_Address);
	// Dc1Factory::DeleteObject(m_ElectronicAddress);
}

void MpafOrganizationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use OrganizationTypePtr, since we
	// might need GetBase(), which isn't defined in IOrganizationType
	const Dc1Ptr< MpafOrganizationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafAgentPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafOrganizationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
		// Dc1Factory::DeleteObject(m_NameTerm);
		this->SetNameTerm(Dc1Factory::CloneObject(tmp->GetNameTerm()));
	if (tmp->IsValidKind())
	{
		// Dc1Factory::DeleteObject(m_Kind);
		this->SetKind(Dc1Factory::CloneObject(tmp->GetKind()));
	}
	else
	{
		InvalidateKind();
	}
		// Dc1Factory::DeleteObject(m_Contact);
		this->SetContact(Dc1Factory::CloneObject(tmp->GetContact()));
	if (tmp->IsValidAddress())
	{
		// Dc1Factory::DeleteObject(m_Address);
		this->SetAddress(Dc1Factory::CloneObject(tmp->GetAddress()));
	}
	else
	{
		InvalidateAddress();
	}
	if (tmp->IsValidElectronicAddress())
	{
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		this->SetElectronicAddress(Dc1Factory::CloneObject(tmp->GetElectronicAddress()));
	}
	else
	{
		InvalidateElectronicAddress();
	}
}

Dc1NodePtr MpafOrganizationType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafOrganizationType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafAgentType > MpafOrganizationType::GetBase() const
{
	return m_Base;
}

MpafOrganizationType_Name_Collection0Ptr MpafOrganizationType::GetName() const
{
		return m_Name;
}

MpafOrganizationType_NameTerm_Collection0Ptr MpafOrganizationType::GetNameTerm() const
{
		return m_NameTerm;
}

Mp7JrsTermUsePtr MpafOrganizationType::GetKind() const
{
		return m_Kind;
}

// Element is optional
bool MpafOrganizationType::IsValidKind() const
{
	return m_Kind_Exist;
}

MpafOrganizationType_Contact_CollectionPtr MpafOrganizationType::GetContact() const
{
		return m_Contact;
}

Mp7JrsPlacePtr MpafOrganizationType::GetAddress() const
{
		return m_Address;
}

// Element is optional
bool MpafOrganizationType::IsValidAddress() const
{
	return m_Address_Exist;
}

Mp7JrsElectronicAddressPtr MpafOrganizationType::GetElectronicAddress() const
{
		return m_ElectronicAddress;
}

// Element is optional
bool MpafOrganizationType::IsValidElectronicAddress() const
{
	return m_ElectronicAddress_Exist;
}

void MpafOrganizationType::SetName(const MpafOrganizationType_Name_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != MpafOrganizationType_Name_Collection0Ptr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::SetNameTerm(const MpafOrganizationType_NameTerm_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetNameTerm().");
	}
	if (m_NameTerm != item)
	{
		// Dc1Factory::DeleteObject(m_NameTerm);
		m_NameTerm = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NameTerm) m_NameTerm->SetParent(m_myself.getPointer());
		if(m_NameTerm != MpafOrganizationType_NameTerm_Collection0Ptr())
		{
			m_NameTerm->SetContentName(XMLString::transcode("NameTerm"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::SetKind(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetKind().");
	}
	if (m_Kind != item || m_Kind_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Kind);
		m_Kind = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Kind) m_Kind->SetParent(m_myself.getPointer());
		if (m_Kind && m_Kind->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Kind->UseTypeAttribute = true;
		}
		m_Kind_Exist = true;
		if(m_Kind != Mp7JrsTermUsePtr())
		{
			m_Kind->SetContentName(XMLString::transcode("Kind"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::InvalidateKind()
{
	m_Kind_Exist = false;
}
void MpafOrganizationType::SetContact(const MpafOrganizationType_Contact_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetContact().");
	}
	if (m_Contact != item)
	{
		// Dc1Factory::DeleteObject(m_Contact);
		m_Contact = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Contact) m_Contact->SetParent(m_myself.getPointer());
		if(m_Contact != MpafOrganizationType_Contact_CollectionPtr())
		{
			m_Contact->SetContentName(XMLString::transcode("Contact"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::SetAddress(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetAddress().");
	}
	if (m_Address != item || m_Address_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Address);
		m_Address = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Address) m_Address->SetParent(m_myself.getPointer());
		if (m_Address && m_Address->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Address->UseTypeAttribute = true;
		}
		m_Address_Exist = true;
		if(m_Address != Mp7JrsPlacePtr())
		{
			m_Address->SetContentName(XMLString::transcode("Address"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::InvalidateAddress()
{
	m_Address_Exist = false;
}
void MpafOrganizationType::SetElectronicAddress(const Mp7JrsElectronicAddressPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetElectronicAddress().");
	}
	if (m_ElectronicAddress != item || m_ElectronicAddress_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		m_ElectronicAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ElectronicAddress) m_ElectronicAddress->SetParent(m_myself.getPointer());
		if (m_ElectronicAddress && m_ElectronicAddress->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ElectronicAddress->UseTypeAttribute = true;
		}
		m_ElectronicAddress_Exist = true;
		if(m_ElectronicAddress != Mp7JrsElectronicAddressPtr())
		{
			m_ElectronicAddress->SetContentName(XMLString::transcode("ElectronicAddress"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::InvalidateElectronicAddress()
{
	m_ElectronicAddress_Exist = false;
}
XMLCh * MpafOrganizationType::Gettype() const
{
	return GetBase()->GetBase()->Gettype();
}

void MpafOrganizationType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::Settype().");
	}
	GetBase()->GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

MpafOperatorType_Descriptor_CollectionPtr MpafOrganizationType::GetDescriptor() const
{
	return GetBase()->GetBase()->GetDescriptor();
}

void MpafOrganizationType::SetDescriptor(const MpafOperatorType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::SetDescriptor().");
	}
	GetBase()->GetBase()->SetDescriptor(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafOrganizationType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool MpafOrganizationType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void MpafOrganizationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOrganizationType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
XMLCh * MpafOrganizationType::Geturi() const
{
	return GetBase()->GetBase()->GetBase()->Geturi();
}

void MpafOrganizationType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOrganizationType::Seturi().");
	}
	GetBase()->GetBase()->GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafOrganizationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Name is item of type OrganizationType_Name_LocalType0
		// in element collection OrganizationType_Name_CollectionType0
		
		context->Found = true;
		MpafOrganizationType_Name_Collection0Ptr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateOrganizationType_Name_CollectionType0; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_LocalType0")))) != empty)
			{
				// Is type allowed
				MpafOrganizationType_Name_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NameTerm")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:NameTerm is item of type OrganizationType_NameTerm_LocalType0
		// in element collection OrganizationType_NameTerm_CollectionType0
		
		context->Found = true;
		MpafOrganizationType_NameTerm_Collection0Ptr coll = GetNameTerm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateOrganizationType_NameTerm_CollectionType0; // FTT, check this
				SetNameTerm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_LocalType0")))) != empty)
			{
				// Is type allowed
				MpafOrganizationType_NameTerm_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Kind")) == 0)
	{
		// Kind is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetKind()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetKind(p, client);
					if((p = GetKind()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Contact")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Contact is item of abstract type AgentType
		// in element collection OrganizationType_Contact_CollectionType
		
		context->Found = true;
		MpafOrganizationType_Contact_CollectionPtr coll = GetContact();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateOrganizationType_Contact_CollectionType; // FTT, check this
				SetContact(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Address")) == 0)
	{
		// Address is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAddress(p, client);
					if((p = GetAddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ElectronicAddress")) == 0)
	{
		// ElectronicAddress is simple element ElectronicAddressType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetElectronicAddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType")))) != empty)
			{
				// Is type allowed
				Mp7JrsElectronicAddressPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetElectronicAddress(p, client);
					if((p = GetElectronicAddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for OrganizationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "OrganizationType");
	}
	return result;
}

XMLCh * MpafOrganizationType::ToText() const
{
	return m_Base->ToText();
}


bool MpafOrganizationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafOrganizationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafOrganizationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("OrganizationType"));
	// Element serialization:
	if (m_Name != MpafOrganizationType_Name_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_NameTerm != MpafOrganizationType_NameTerm_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_NameTerm->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Kind != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Kind->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Kind"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Kind->Serialize(doc, element, &elem);
	}
	if (m_Contact != MpafOrganizationType_Contact_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Contact->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Address != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Address->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Address"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Address->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ElectronicAddress != Mp7JrsElectronicAddressPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ElectronicAddress->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("ElectronicAddress"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ElectronicAddress->Serialize(doc, element, &elem);
	}

}

bool MpafOrganizationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafAgentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			MpafOrganizationType_Name_Collection0Ptr tmp = CreateOrganizationType_Name_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("NameTerm"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("NameTerm")) == 0))
		{
			// Deserialize factory type
			MpafOrganizationType_NameTerm_Collection0Ptr tmp = CreateOrganizationType_NameTerm_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNameTerm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Kind"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Kind")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKind(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Contact"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Contact")) == 0))
		{
			// Deserialize factory type
			MpafOrganizationType_Contact_CollectionPtr tmp = CreateOrganizationType_Contact_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetContact(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Address"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Address")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ElectronicAddress"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ElectronicAddress")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateElectronicAddressType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetElectronicAddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafOrganizationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafOrganizationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	MpafOrganizationType_Name_Collection0Ptr tmp = CreateOrganizationType_Name_CollectionType0; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("NameTerm")) == 0))
  {
	MpafOrganizationType_NameTerm_Collection0Ptr tmp = CreateOrganizationType_NameTerm_CollectionType0; // FTT, check this
	this->SetNameTerm(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Kind")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetKind(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Contact")) == 0))
  {
	MpafOrganizationType_Contact_CollectionPtr tmp = CreateOrganizationType_Contact_CollectionType; // FTT, check this
	this->SetContact(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Address")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetAddress(child);
  }
  if (XMLString::compareString(elementname, X("ElectronicAddress")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateElectronicAddressType; // FTT, check this
	}
	this->SetElectronicAddress(child);
  }
  return child;
 
}

Dc1NodeEnum MpafOrganizationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetNameTerm() != Dc1NodePtr())
		result.Insert(GetNameTerm());
	if (GetKind() != Dc1NodePtr())
		result.Insert(GetKind());
	if (GetContact() != Dc1NodePtr())
		result.Insert(GetContact());
	if (GetAddress() != Dc1NodePtr())
		result.Insert(GetAddress());
	if (GetElectronicAddress() != Dc1NodePtr())
		result.Insert(GetElectronicAddress());
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafOrganizationType_ExtMethodImpl.h


