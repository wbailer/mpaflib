
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafActivityType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "MpafMPAFBaseType.h"
#include "W3CdateTime.h"
#include "MpafActivityType_Descriptor_CollectionType.h"
#include "MpafActivityType_Parameters_LocalType.h"
#include "MpafActivityType_Activity_CollectionType.h"
#include "MpafActivityType_Operator_CollectionType.h"
#include "MpafActivityType_Content_CollectionType0.h"
#include "Mp7JrsPlaceType.h"
#include "MpafActivityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor
#include "MpafActivityType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Activity
#include "MpafOperatorRefType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Operator
#include "MpafActivityType_Content_LocalType0.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Content

#include <assert.h>
IMpafActivityType::IMpafActivityType()
{

// no includefile for extension defined 
// file MpafActivityType_ExtPropInit.h

}

IMpafActivityType::~IMpafActivityType()
{
// no includefile for extension defined 
// file MpafActivityType_ExtPropCleanup.h

}

MpafActivityType::MpafActivityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafActivityType::~MpafActivityType()
{
	Cleanup();
}

void MpafActivityType::Init()
{
	// Init base
	m_Base = CreateMPAFBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_start = W3CdateTimePtr(); // Pattern
	m_start_Exist = false;
	m_end = W3CdateTimePtr(); // Pattern
	m_end_Exist = false;
	m_type = NULL; // String
	m_executed = false; // Value
	m_executed_Default = true; // Default value
	m_executed_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Descriptor = MpafActivityType_Descriptor_CollectionPtr(); // Collection
	m_Parameters = MpafActivityType_Parameters_LocalPtr(); // Class
	m_Parameters_Exist = false;
	m_Activity = MpafActivityType_Activity_CollectionPtr(); // Collection
	m_Operator = MpafActivityType_Operator_CollectionPtr(); // Collection
	m_Content = MpafActivityType_Content_Collection0Ptr(); // Collection
	m_Location = Mp7JrsPlacePtr(); // Class
	m_Location_Exist = false;


// no includefile for extension defined 
// file MpafActivityType_ExtMyPropInit.h

}

void MpafActivityType::Cleanup()
{
// no includefile for extension defined 
// file MpafActivityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_start); // Pattern
	// Dc1Factory::DeleteObject(m_end); // Pattern
	XMLString::release(&m_type); // String
	// Dc1Factory::DeleteObject(m_Descriptor);
	// Dc1Factory::DeleteObject(m_Parameters);
	// Dc1Factory::DeleteObject(m_Activity);
	// Dc1Factory::DeleteObject(m_Operator);
	// Dc1Factory::DeleteObject(m_Content);
	// Dc1Factory::DeleteObject(m_Location);
}

void MpafActivityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ActivityTypePtr, since we
	// might need GetBase(), which isn't defined in IActivityType
	const Dc1Ptr< MpafActivityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafMPAFBasePtr(Dc1Factory::CloneObject((dynamic_cast< const MpafActivityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_start); // Pattern
	if (tmp->Existstart())
	{
		this->Setstart(Dc1Factory::CloneObject(tmp->Getstart()));
	}
	else
	{
		Invalidatestart();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_end); // Pattern
	if (tmp->Existend())
	{
		this->Setend(Dc1Factory::CloneObject(tmp->Getend()));
	}
	else
	{
		Invalidateend();
	}
	}
	{
	XMLString::release(&m_type); // String
		this->Settype(XMLString::replicate(tmp->Gettype()));
	}
	{
	if (tmp->Existexecuted())
	{
		this->Setexecuted(tmp->Getexecuted());
	}
	else
	{
		Invalidateexecuted();
	}
	}
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
	if (tmp->IsValidParameters())
	{
		// Dc1Factory::DeleteObject(m_Parameters);
		this->SetParameters(Dc1Factory::CloneObject(tmp->GetParameters()));
	}
	else
	{
		InvalidateParameters();
	}
		// Dc1Factory::DeleteObject(m_Activity);
		this->SetActivity(Dc1Factory::CloneObject(tmp->GetActivity()));
		// Dc1Factory::DeleteObject(m_Operator);
		this->SetOperator(Dc1Factory::CloneObject(tmp->GetOperator()));
		// Dc1Factory::DeleteObject(m_Content);
		this->SetContent(Dc1Factory::CloneObject(tmp->GetContent()));
	if (tmp->IsValidLocation())
	{
		// Dc1Factory::DeleteObject(m_Location);
		this->SetLocation(Dc1Factory::CloneObject(tmp->GetLocation()));
	}
	else
	{
		InvalidateLocation();
	}
}

Dc1NodePtr MpafActivityType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr MpafActivityType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< MpafMPAFBaseType > MpafActivityType::GetBase() const
{
	return m_Base;
}

W3CdateTimePtr MpafActivityType::Getstart() const
{
	return m_start;
}

bool MpafActivityType::Existstart() const
{
	return m_start_Exist;
}
void MpafActivityType::Setstart(const W3CdateTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Setstart().");
	}
	m_start = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_start) m_start->SetParent(m_myself.getPointer());
	m_start_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::Invalidatestart()
{
	m_start_Exist = false;
}
W3CdateTimePtr MpafActivityType::Getend() const
{
	return m_end;
}

bool MpafActivityType::Existend() const
{
	return m_end_Exist;
}
void MpafActivityType::Setend(const W3CdateTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Setend().");
	}
	m_end = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_end) m_end->SetParent(m_myself.getPointer());
	m_end_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::Invalidateend()
{
	m_end_Exist = false;
}
XMLCh * MpafActivityType::Gettype() const
{
	return m_type;
}

void MpafActivityType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Settype().");
	}
	m_type = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool MpafActivityType::Getexecuted() const
{
	if (this->Existexecuted()) {
		return m_executed;
	} else {
		return m_executed_Default;
	}
}

bool MpafActivityType::Existexecuted() const
{
	return m_executed_Exist;
}
void MpafActivityType::Setexecuted(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Setexecuted().");
	}
	m_executed = item;
	m_executed_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::Invalidateexecuted()
{
	m_executed_Exist = false;
}
MpafActivityType_Descriptor_CollectionPtr MpafActivityType::GetDescriptor() const
{
		return m_Descriptor;
}

MpafActivityType_Parameters_LocalPtr MpafActivityType::GetParameters() const
{
		return m_Parameters;
}

// Element is optional
bool MpafActivityType::IsValidParameters() const
{
	return m_Parameters_Exist;
}

MpafActivityType_Activity_CollectionPtr MpafActivityType::GetActivity() const
{
		return m_Activity;
}

MpafActivityType_Operator_CollectionPtr MpafActivityType::GetOperator() const
{
		return m_Operator;
}

MpafActivityType_Content_Collection0Ptr MpafActivityType::GetContent() const
{
		return m_Content;
}

Mp7JrsPlacePtr MpafActivityType::GetLocation() const
{
		return m_Location;
}

// Element is optional
bool MpafActivityType::IsValidLocation() const
{
	return m_Location_Exist;
}

void MpafActivityType::SetDescriptor(const MpafActivityType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != MpafActivityType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::SetParameters(const MpafActivityType_Parameters_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetParameters().");
	}
	if (m_Parameters != item || m_Parameters_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Parameters);
		m_Parameters = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Parameters) m_Parameters->SetParent(m_myself.getPointer());
		if (m_Parameters && m_Parameters->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Parameters->UseTypeAttribute = true;
		}
		m_Parameters_Exist = true;
		if(m_Parameters != MpafActivityType_Parameters_LocalPtr())
		{
			m_Parameters->SetContentName(XMLString::transcode("Parameters"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::InvalidateParameters()
{
	m_Parameters_Exist = false;
}
void MpafActivityType::SetActivity(const MpafActivityType_Activity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetActivity().");
	}
	if (m_Activity != item)
	{
		// Dc1Factory::DeleteObject(m_Activity);
		m_Activity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Activity) m_Activity->SetParent(m_myself.getPointer());
		if(m_Activity != MpafActivityType_Activity_CollectionPtr())
		{
			m_Activity->SetContentName(XMLString::transcode("Activity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::SetOperator(const MpafActivityType_Operator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetOperator().");
	}
	if (m_Operator != item)
	{
		// Dc1Factory::DeleteObject(m_Operator);
		m_Operator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Operator) m_Operator->SetParent(m_myself.getPointer());
		if(m_Operator != MpafActivityType_Operator_CollectionPtr())
		{
			m_Operator->SetContentName(XMLString::transcode("Operator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::SetContent(const MpafActivityType_Content_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetContent().");
	}
	if (m_Content != item)
	{
		// Dc1Factory::DeleteObject(m_Content);
		m_Content = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Content) m_Content->SetParent(m_myself.getPointer());
		if(m_Content != MpafActivityType_Content_Collection0Ptr())
		{
			m_Content->SetContentName(XMLString::transcode("Content"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::SetLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::SetLocation().");
	}
	if (m_Location != item || m_Location_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Location);
		m_Location = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Location) m_Location->SetParent(m_myself.getPointer());
		if (m_Location && m_Location->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Location->UseTypeAttribute = true;
		}
		m_Location_Exist = true;
		if(m_Location != Mp7JrsPlacePtr())
		{
			m_Location->SetContentName(XMLString::transcode("Location"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::InvalidateLocation()
{
	m_Location_Exist = false;
}
XMLCh * MpafActivityType::Getid() const
{
	return GetBase()->Getid();
}

bool MpafActivityType::Existid() const
{
	return GetBase()->Existid();
}
void MpafActivityType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafActivityType::Invalidateid()
{
	GetBase()->Invalidateid();
}
XMLCh * MpafActivityType::Geturi() const
{
	return GetBase()->Geturi();
}

void MpafActivityType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType::Seturi().");
	}
	GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafActivityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple attribute dateTime
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdateTimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("end")) == 0)
	{
		// end is simple attribute dateTime
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdateTimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("executed")) == 0)
	{
		// executed is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Descriptor is item of abstract type DescriptorType
		// in element collection ActivityType_Descriptor_CollectionType
		
		context->Found = true;
		MpafActivityType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateActivityType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21DescriptorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Parameters")) == 0)
	{
		// Parameters is simple element ActivityType_Parameters_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetParameters()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_LocalType")))) != empty)
			{
				// Is type allowed
				MpafActivityType_Parameters_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetParameters(p, client);
					if((p = GetParameters()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Activity")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Activity is item of type ActivityType
		// in element collection ActivityType_Activity_CollectionType
		
		context->Found = true;
		MpafActivityType_Activity_CollectionPtr coll = GetActivity();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateActivityType_Activity_CollectionType; // FTT, check this
				SetActivity(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType")))) != empty)
			{
				// Is type allowed
				MpafActivityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Operator")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Operator is item of type OperatorRefType
		// in element collection ActivityType_Operator_CollectionType
		
		context->Found = true;
		MpafActivityType_Operator_CollectionPtr coll = GetOperator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateActivityType_Operator_CollectionType; // FTT, check this
				SetOperator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OperatorRefType")))) != empty)
			{
				// Is type allowed
				MpafOperatorRefPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Content")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Content is item of type ActivityType_Content_LocalType0
		// in element collection ActivityType_Content_CollectionType0
		
		context->Found = true;
		MpafActivityType_Content_Collection0Ptr coll = GetContent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateActivityType_Content_CollectionType0; // FTT, check this
				SetContent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_LocalType0")))) != empty)
			{
				// Is type allowed
				MpafActivityType_Content_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Location")) == 0)
	{
		// Location is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLocation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLocation(p, client);
					if((p = GetLocation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ActivityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ActivityType");
	}
	return result;
}

XMLCh * MpafActivityType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafActivityType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafActivityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafActivityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("ActivityType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_start_Exist)
	{
	// Pattern
	if (m_start != W3CdateTimePtr())
	{
		XMLCh * tmp = m_start->ToText();
		element->setAttributeNS(X(""), X("start"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_end_Exist)
	{
	// Pattern
	if (m_end != W3CdateTimePtr())
	{
		XMLCh * tmp = m_end->ToText();
		element->setAttributeNS(X(""), X("end"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
	// String
	if(m_type != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("type"), m_type);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_executed_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_executed);
		element->setAttributeNS(X(""), X("executed"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Descriptor != MpafActivityType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Parameters != MpafActivityType_Parameters_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Parameters->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Parameters"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Parameters->Serialize(doc, element, &elem);
	}
	if (m_Activity != MpafActivityType_Activity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Activity->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Operator != MpafActivityType_Operator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Operator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Content != MpafActivityType_Content_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Content->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Location != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Location->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Location"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Location->Serialize(doc, element, &elem);
	}

}

bool MpafActivityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("start")))
	{
		W3CdateTimePtr tmp = CreatedateTime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("start")));
		this->Setstart(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("end")))
	{
		W3CdateTimePtr tmp = CreatedateTime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("end")));
		this->Setend(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize string type
		this->Settype(Dc1Convert::TextToString(parent->getAttribute(X("type"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("executed")))
	{
		// deserialize value type
		this->Setexecuted(Dc1Convert::TextToBool(parent->getAttribute(X("executed"))));
		* current = parent;
	}

	// Extensionbase is MpafMPAFBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			MpafActivityType_Descriptor_CollectionPtr tmp = CreateActivityType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Parameters"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Parameters")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateActivityType_Parameters_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParameters(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Activity"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Activity")) == 0))
		{
			// Deserialize factory type
			MpafActivityType_Activity_CollectionPtr tmp = CreateActivityType_Activity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetActivity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Operator"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Operator")) == 0))
		{
			// Deserialize factory type
			MpafActivityType_Operator_CollectionPtr tmp = CreateActivityType_Operator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOperator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Content")) == 0))
		{
			// Deserialize factory type
			MpafActivityType_Content_Collection0Ptr tmp = CreateActivityType_Content_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetContent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Location"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Location")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLocation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafActivityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafActivityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	MpafActivityType_Descriptor_CollectionPtr tmp = CreateActivityType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Parameters")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateActivityType_Parameters_LocalType; // FTT, check this
	}
	this->SetParameters(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Activity")) == 0))
  {
	MpafActivityType_Activity_CollectionPtr tmp = CreateActivityType_Activity_CollectionType; // FTT, check this
	this->SetActivity(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Operator")) == 0))
  {
	MpafActivityType_Operator_CollectionPtr tmp = CreateActivityType_Operator_CollectionType; // FTT, check this
	this->SetOperator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Content")) == 0))
  {
	MpafActivityType_Content_Collection0Ptr tmp = CreateActivityType_Content_CollectionType0; // FTT, check this
	this->SetContent(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Location")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetLocation(child);
  }
  return child;
 
}

Dc1NodeEnum MpafActivityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	if (GetParameters() != Dc1NodePtr())
		result.Insert(GetParameters());
	if (GetActivity() != Dc1NodePtr())
		result.Insert(GetActivity());
	if (GetOperator() != Dc1NodePtr())
		result.Insert(GetOperator());
	if (GetContent() != Dc1NodePtr())
		result.Insert(GetContent());
	if (GetLocation() != Dc1NodePtr())
		result.Insert(GetLocation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafActivityType_ExtMethodImpl.h


