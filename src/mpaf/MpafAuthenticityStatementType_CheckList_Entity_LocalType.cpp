
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtImplInclude.h


#include "MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafAuthenticityStatementType_CheckList_Entity_LocalType::IMpafAuthenticityStatementType_CheckList_Entity_LocalType()
{

// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtPropInit.h

}

IMpafAuthenticityStatementType_CheckList_Entity_LocalType::~IMpafAuthenticityStatementType_CheckList_Entity_LocalType()
{
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtPropCleanup.h

}

MpafAuthenticityStatementType_CheckList_Entity_LocalType::MpafAuthenticityStatementType_CheckList_Entity_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafAuthenticityStatementType_CheckList_Entity_LocalType::~MpafAuthenticityStatementType_CheckList_Entity_LocalType()
{
	Cleanup();
}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::Init()
{

	// Init attributes
	m_ref = NULL; // String
	m_ref_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Confidence = MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr(); // Class with content 
	m_Annotation = Mp7JrsTextAnnotationPtr(); // Class


// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtMyPropInit.h

}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_ref); // String
	// Dc1Factory::DeleteObject(m_Confidence);
	// Dc1Factory::DeleteObject(m_Annotation);
}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AuthenticityStatementType_CheckList_Entity_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAuthenticityStatementType_CheckList_Entity_LocalType
	const Dc1Ptr< MpafAuthenticityStatementType_CheckList_Entity_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_ref); // String
	if (tmp->Existref())
	{
		this->Setref(XMLString::replicate(tmp->Getref()));
	}
	else
	{
		Invalidateref();
	}
	}
		// Dc1Factory::DeleteObject(m_Confidence);
		this->SetConfidence(Dc1Factory::CloneObject(tmp->GetConfidence()));
		// Dc1Factory::DeleteObject(m_Annotation);
		this->SetAnnotation(Dc1Factory::CloneObject(tmp->GetAnnotation()));
}

Dc1NodePtr MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * MpafAuthenticityStatementType_CheckList_Entity_LocalType::Getref() const
{
	return m_ref;
}

bool MpafAuthenticityStatementType_CheckList_Entity_LocalType::Existref() const
{
	return m_ref_Exist;
}
void MpafAuthenticityStatementType_CheckList_Entity_LocalType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_Entity_LocalType::Setref().");
	}
	m_ref = item;
	m_ref_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::Invalidateref()
{
	m_ref_Exist = false;
}
MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetConfidence() const
{
		return m_Confidence;
}

Mp7JrsTextAnnotationPtr MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetAnnotation() const
{
		return m_Annotation;
}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::SetConfidence(const MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_Entity_LocalType::SetConfidence().");
	}
	if (m_Confidence != item)
	{
		// Dc1Factory::DeleteObject(m_Confidence);
		m_Confidence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Confidence) m_Confidence->SetParent(m_myself.getPointer());
		if (m_Confidence && m_Confidence->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_Confidence_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Confidence->UseTypeAttribute = true;
		}
		if(m_Confidence != MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr())
		{
			m_Confidence->SetContentName(XMLString::transcode("Confidence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafAuthenticityStatementType_CheckList_Entity_LocalType::SetAnnotation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_Entity_LocalType::SetAnnotation().");
	}
	if (m_Annotation != item)
	{
		// Dc1Factory::DeleteObject(m_Annotation);
		m_Annotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Annotation) m_Annotation->SetParent(m_myself.getPointer());
		if (m_Annotation && m_Annotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Annotation->UseTypeAttribute = true;
		}
		if(m_Annotation != Mp7JrsTextAnnotationPtr())
		{
			m_Annotation->SetContentName(XMLString::transcode("Annotation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("ref")) == 0)
	{
		// ref is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Confidence")) == 0)
	{
		// Confidence is simple element AuthenticityStatementType_CheckList_Entity_Confidence_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetConfidence()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_Confidence_LocalType")))) != empty)
			{
				// Is type allowed
				MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetConfidence(p, client);
					if((p = GetConfidence()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Annotation")) == 0)
	{
		// Annotation is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAnnotation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAnnotation(p, client);
					if((p = GetAnnotation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AuthenticityStatementType_CheckList_Entity_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AuthenticityStatementType_CheckList_Entity_LocalType");
	}
	return result;
}

XMLCh * MpafAuthenticityStatementType_CheckList_Entity_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafAuthenticityStatementType_CheckList_Entity_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafAuthenticityStatementType_CheckList_Entity_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("AuthenticityStatementType_CheckList_Entity_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_ref_Exist)
	{
	// String
	if(m_ref != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("ref"), m_ref);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class with content		
	
	if (m_Confidence != MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Confidence->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Confidence"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Confidence->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Annotation != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Annotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Annotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Annotation->Serialize(doc, element, &elem);
	}

}

bool MpafAuthenticityStatementType_CheckList_Entity_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("ref")))
	{
		// Deserialize string type
		this->Setref(Dc1Convert::TextToString(parent->getAttribute(X("ref"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Confidence"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Confidence")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAuthenticityStatementType_CheckList_Entity_Confidence_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetConfidence(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Annotation"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Annotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafAuthenticityStatementType_CheckList_Entity_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Confidence")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAuthenticityStatementType_CheckList_Entity_Confidence_LocalType; // FTT, check this
	}
	this->SetConfidence(child);
  }
  if (XMLString::compareString(elementname, X("Annotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetAnnotation(child);
  }
  return child;
 
}

Dc1NodeEnum MpafAuthenticityStatementType_CheckList_Entity_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetConfidence() != Dc1NodePtr())
		result.Insert(GetConfidence());
	if (GetAnnotation() != Dc1NodePtr())
		result.Insert(GetAnnotation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_Entity_LocalType_ExtMethodImpl.h


