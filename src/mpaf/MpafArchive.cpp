
// Based on Archive_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/




#include "MpafArchive.h"

#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include "Dc1PreservationMetadataType.h" 
#include "MpafFactoryDefines.h"
#include "Dc1Node.h"
#include "Dc1Ptr.h"
#include <assert.h>
#if defined (WIN32)
# include <typeinfo.h>
#else
# include <typeinfo>
#endif

class MpafErrorHandler : public ErrorHandler
{
public:

	MpafErrorHandler(){};
	~MpafErrorHandler(){};

	virtual void warning(const SAXParseException& exception)
	{
		XMLCh * msg = extractErrorMessage(exception); // FTT fixed mem leak
		Dc1Exception e(DC1_WARNING, msg);
		Dc1Util::deallocate(&msg);
		throw e;
	}

	virtual void error(const SAXParseException& exception)
	{
		XMLCh * msg = extractErrorMessage(exception); // FTT fixed mem leak
		Dc1Exception e( DC1_ERROR, msg);
		Dc1Util::deallocate(&msg);
		throw e;
	}

	virtual void fatalError(const SAXParseException& exception)
	{
		XMLCh * msg = extractErrorMessage(exception); // FTT fixed mem leak
		Dc1Exception e( DC1_FATAL_ERROR, msg);
		Dc1Util::deallocate(&msg);
		throw e;
	}

	void resetErrors(){};

private :
	XMLCh* extractErrorMessage(const SAXParseException& exception)
	{
		XMLCh * msg = XMLString::transcode("");
		Dc1Util::CatString(&msg, exception.getMessage());
		Dc1Util::CatString(&msg, X(". column: "));
		Dc1Util::CatString(&msg, Dc1Util::ConvertToXMLCh(exception.getColumnNumber()));
		Dc1Util::CatString(&msg, X("; line: "));
		Dc1Util::CatString(&msg, Dc1Util::ConvertToXMLCh(exception.getLineNumber()));
		Dc1Util::CatString(&msg, X("."));
		return msg;
	}

	/* Unimplemented constructors and operators */ 
	MpafErrorHandler(const ErrorHandler&);
	void operator=(const DOMErrorHandler&);
};

class MpafArchive::XercesWrapper 
{
public:
	XercesWrapper(InputSource &source, bool fullSchema, bool validate,
		const Dc1SchemaLocationMap &locationMap);
	virtual ~XercesWrapper();
	DOMDocument * getDoc() const
	{ return doc_; }
	DOMElement * getRoot() const
	{ return root_; }
private:
	XercesWrapper();				// w/o implementation
	XercesWrapper &operator=(const XercesWrapper &rhs);	 // w/o impl
	// Is this attribute necessary?
	DOMImplementation * impl_;
	XercesDOMParser * parser_;
	MpafErrorHandler * handler_;
	DOMDocument * doc_;
	DOMElement * root_;
};

MpafArchive::XercesWrapper::XercesWrapper(InputSource &source,
	bool fullSchema, bool validate, const Dc1SchemaLocationMap &locationMap)
{
	bool useNS = locationMap.size() > 0; // This is new, we only use namespacehandling, if necessary

	// Is this necessary?
	impl_ =  DOMImplementationRegistry::getDOMImplementation(X("Core"));
	parser_ = new XercesDOMParser;
	
	// parser options
	parser_->setValidationSchemaFullChecking(fullSchema);
	// FTT This is for 2.7, 2.8 AND 3.0.1 (no deprecated setDoValidation())
	parser_->setValidationScheme(validate ? AbstractDOMParser::Val_Always : AbstractDOMParser::Val_Never);
	
	// So, if you don't define a default namespace (+prefix) and additional namespaces, then we skip namespacehandling
	parser_->setDoNamespaces(useNS);
	
	parser_->setDoSchema(validate);
	parser_->setExitOnFirstFatalError(true);
	XMLCh * txt = locationMap.ToText();	  // FTT: setExternalSchemaLocation(const XMLCh const *)
	parser_->setExternalSchemaLocation(txt); // FTT: There is definitely something wrong with a language
	XMLString::release(&txt);				// FTT: where you can define "const char const *"
	handler_ = new Dc1ErrorHandler; 
	parser_->setErrorHandler(handler_);
	parser_->parse(source);
	doc_ = parser_->getDocument();
	root_ = doc_->getDocumentElement();
}

MpafArchive::XercesWrapper::~XercesWrapper()
{
	// This was factored out from _Deserialize; apparently, releasing
	// the document releases the parser as well. 
	// FTT: This is not the whole truth! Some parts of the parser seemed to be released, others not!
	delete handler_;
	// FTT: excluded this: doc_->release();
	delete parser_;
}

MpafArchive::MpafArchive(void)
{
	encoding = XMLString::transcode("UTF-16");
	
	destination = NULL;
	validate = false;
	fullSchema = false;
	writeXMLDecl = true;
	xmlDeclDefault = true;
	usePrefixForDefaultNS = false; // No prefix for default namespace
	prettyformat = false; // small xmlcode
	forceNamespaceDeclaration = true; // Declare ns in root not in each and every subnode
	standalone = true;
	version = XMLString::transcode("1.0");
	
	// Set default schema locations and namespaces
	// If no default namespace is given, skip it.
	
	// TODO: unify param order: uri,prefix vs prefix,uri
	_locationMap.SetDefaultNamespace( X("urn:mpeg:maf:schema:preservation:2015"), X("mpaf"));
	// There is a default schema location specified
	_locationMap.SetSchemaLocation(X("urn:mpeg:maf:schema:preservation:2015"),X("mpaf"),X("mpaf.xsd"));
	
	_locationMap.SetSchemaLocation(X("http://purl.org/dc/elements/1.1/"), X("dc1"), X("./dc/simpledc20021212.xsd"));
	_locationMap.SetSchemaLocation(X("urn:ebu:metadata-schema:ebuCore_2014"), X("ebucore"), X("./ebucore/EBU_CORE_20140318.xsd"));
	_locationMap.SetSchemaLocation(X("urn:mpeg:mpeg7:schema:2004"), X("mpeg7"), X("./mpeg7/mpeg7-v4.xsd"));
	_locationMap.SetSchemaLocation(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS"), X("didmodel"), X("./mpeg21/did/didmodel.xsd"));
	_locationMap.SetSchemaLocation(X("urn:mpeg:mpeg21:2002:01-DII-NS"), X("dii"), X("./mpeg21/dii/dii.xsd"));
	_locationMap.SetSchemaLocation(X("urn:mpeg:maf:schema:preservation:2015"), X("mpaf"), X("mpaf.xsd"));
	_locationMap.SetSchemaLocation(X("http://www.w3.org/2001/XMLSchema"), X("xsd"), X(""));
	_locationMap.SetSchemaLocation(X("http://www.w3.org/2001/XMLSchema"), X("xsd"), X(""));
	_locationMap.SetSchemaLocation(X("http://www.w3.org/XML/1998/namespace"), X("xml"), X(""));
	
}

MpafArchive::~MpafArchive(void)
{
	XMLString::release(&encoding);
	XMLString::release(&version);
}

void MpafArchive::ToFile(const XMLCh * path, Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr)
{
	destination = new LocalFileFormatTarget(path);
	// FTT This doesn't work for abstract or derived rootelements	
	//	bool writeHeader = node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType"));
	bool writeHeader = ((Dc1PreservationMetadataPtr)node != 0);
	_Serialize(node, nodeName, useTypeAttr, writeHeader);
	
	delete destination;
	}

void MpafArchive::ToFile(const char* path, Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr) 
{
	if(!path || !node) {
		return;
	}
	ToFile(X(path),node,nodeName,useTypeAttr);
}

Dc1NodePtr MpafArchive::FromFile(const XMLCh * path, Dc1NodePtr &node)
{
	InputSource *source = new LocalFileInputSource(path);
	Dc1NodePtr result = _Deserialize(node, *source);
	delete source;
	return result;
}

Dc1NodePtr MpafArchive::FromFile(const XMLCh * path)
{
	Dc1NodePtr empty = Dc1NodePtr();
	return FromFile(path, empty);
}

Dc1NodePtr MpafArchive::FromFile(const char * path, Dc1NodePtr &node)
{
	return FromFile(X(path), node);
}

Dc1NodePtr MpafArchive::FromFile(const char * path)
{
	Dc1NodePtr empty = Dc1NodePtr();
	return FromFile(X(path), empty);
}

//---
int MpafArchive::AddToCollectionFromFile(const XMLCh * path,
	Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where)
{
	InputSource *source = new LocalFileInputSource(path);
	int result =_DeserializeIntoCollection(*source, collection, howtoadd, where);
	delete source;
	return result;
}

int MpafArchive::AddToCollectionFromFile(const char * path,
  Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where)
{
	return AddToCollectionFromFile(X(path), collection, howtoadd, where);
}

int MpafArchive::AddToCollectionFromBuffer(const XMLCh * buffer,
  Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where)
{
	MemBufInputSource source((XMLByte*)buffer,XMLString::stringLen(buffer)*2,"id",false);
	source.setEncoding(X("UTF-16"));
	
	return _DeserializeIntoCollection(source, collection, howtoadd, where);
}

int MpafArchive::AddToCollectionFromBuffer(const char * buffer,
  Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where)
{
	return AddToCollectionFromBuffer(X(buffer), collection, howtoadd, where);
}

//---


XMLCh* MpafArchive::ToWBuffer(Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr) 
{
	destination = new MemBufFormatTarget();
	
	XMLCh * saveencoding = encoding;
	encoding = XMLString::transcode("UTF-16");
	
// FTT This doesn't work for abstract or derived rootelements	
//	bool writeHeader = node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType"));
	bool writeHeader = ((Dc1PreservationMetadataPtr)node != 0);
	_Serialize(node, nodeName, useTypeAttr, writeHeader);
	
//	FTT don't do this, since people might use XMLString::release() -> on the wrong heap!
//	   XMLCh* buf = new XMLCh[(((MemBufFormatTarget*)destination)->getLen()/2)+1];

// FTT NEW!!!! We consequently use the Xerces heap for this kind of stuff!!		
// So, call DeleteBuffer() after using it...
	XMLCh* buf = Dc1Util::allocate((long)((((MemBufFormatTarget*)destination)->getLen()/2)+1));
	XMLString::copyString (buf,(XMLCh*)(((MemBufFormatTarget*)destination)->getRawBuffer())); 
	
	delete destination;
	XMLString::release(&encoding);
	encoding = saveencoding;
	
	return buf;
}

char* MpafArchive::ToBuffer(Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr) 
{
	destination = new MemBufFormatTarget();
	
// FTT This doesn't work for abstract or derived rootelements	
//	bool writeHeader = node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType"));
	bool writeHeader = ((Dc1PreservationMetadataPtr)node != 0);
	_Serialize(node, nodeName, useTypeAttr, writeHeader);

// FTT NEW!!!! We consequently use the Xerces heap for this kind of stuff!!		
// So, call DeleteBuffer() after using it...
	char * buf = NULL;
	if((((MemBufFormatTarget*)destination)->getLen()) > 0)
	{
		buf = XMLString::replicate((char*)(((MemBufFormatTarget*)destination)->getRawBuffer()));
	}
	delete destination;
	return buf;
}

Dc1NodePtr MpafArchive::FromBuffer(const XMLCh * buf, Dc1NodePtr &node)
{
	InputSource *source = new MemBufInputSource((XMLByte*)buf,XMLString::stringLen(buf)*2,"id",false);
	source->setEncoding(X("UTF-16"));
	
	Dc1NodePtr result = _Deserialize(node, *source);
	
	delete source;
	return result;
}

Dc1NodePtr MpafArchive::FromBuffer(const XMLCh * buf)
{
	Dc1NodePtr empty = Dc1NodePtr();
	return FromBuffer(buf, empty);
}

Dc1NodePtr MpafArchive::FromBuffer(const char * buf, Dc1NodePtr &node)
{
	InputSource *source = new MemBufInputSource((XMLByte*)buf,XMLString::stringLen(buf),"id",false);
	source->setEncoding(X("UTF-8"));
	
	Dc1NodePtr result = _Deserialize(node, *source);
	
	delete source;
	return result;
}

Dc1NodePtr MpafArchive::FromBuffer(const char * buf)
{
	Dc1NodePtr empty = Dc1NodePtr();
	return FromBuffer(buf, empty);
}

void MpafArchive::ToStdOut(Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr) 
{
	destination = new StdOutFormatTarget();

// FTT This doesn't work for abstract or derived rootelements	
//	bool writeHeader = node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType"));
	bool writeHeader = ((Dc1PreservationMetadataPtr)node != 0);
	_Serialize(node, nodeName, useTypeAttr, writeHeader);
	
	delete destination;
}


void MpafArchive::_Serialize(Dc1NodePtr &node, XMLCh* nodeName, bool useTypeAttr, bool writeHeader) 
{
#if XERCES_VERSION_MAJOR < 3
	DOMImplementation * impl =  DOMImplementationRegistry::getDOMImplementation(X("Core"));
#else
  DOMImplementation * impl = DOMImplementationRegistry::getDOMImplementation(X("LS"));
#endif // XERCES_VERSION_MAJOR < 3

	// FTT This doesn't work for abstract or derived rootelements	
	//	if (node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType")))
	if((Dc1PreservationMetadataPtr)node != 0)
	{
		XMLString::release(&nodeName);
		nodeName = XMLString::transcode("DIDL");
	}
	else if (nodeName == NULL) 
	{
	  nodeName = XMLString::transcode("DIDLFragment");
	}

	const XMLCh* defaultNS = 0;
	const XMLCh* prefix = 0;
	const XMLCh* ns = NULL;
	_locationMap.GetDefaultNamespace(&defaultNS, &prefix);
	
	DOMDocument * doc = impl->createDocument(defaultNS, nodeName, 0);
	
#if XERCES_VERSION_MAJOR < 3
	doc->setStandalone(standalone);
	doc->setEncoding(encoding);
	doc->setVersion(version);

	DOMWriter * theSerializer = ((DOMImplementationLS*)impl)->createDOMWriter();
#else	
	doc->setXmlStandalone(standalone);
//	doc->setXmlEncoding(encoding);
	doc->setXmlVersion(version);

  DOMLSSerializer * theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();
  DOMLSOutput * theOutput = ((DOMImplementationLS*)impl)->createLSOutput();
#endif // XERCES_VERSION_MAJOR < 3
  
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	theSerializer->setNewLine(X("\r\n"));
#elif defined(XML_MACOS) || defined(APPLE) || defined(__APPLE__)// Xerces 3.0 doesn't know XML_MACOS
	theSerializer->setNewLine(X("\n"));
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	theSerializer->setNewLine(X("\n"));
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for archive."
#endif
	
#if XERCES_VERSION_MAJOR < 3
	theSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, prettyformat);
#else
  if(theSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
	theSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, prettyformat);
#endif // XERCES_VERSION_MAJOR < 3
	
	// baw 2004-02-04: save previous node name
	XMLCh* prevNodeName = XMLString::replicate(node->GetContentName());
	
	node->SetContentName(nodeName);
	node->UseTypeAttribute = useTypeAttr;
	
	DOMElement* nullNode = NULL;
	
	DOMElement * root = doc->getDocumentElement();
	
	// FTT For compatibility, we always write the namespace decls, 
	// but schema locations only if writeHeader is set.
	
	if ((writeHeader || forceNamespaceDeclaration) && _locationMap.size() > 0) {
		// Default namespace is already done (if exist)
		
		if(writeHeader || forceNamespaceDeclaration){
			// Add xmlns:prefix="URI" namespaces
			// Note, that if defaultnamespace has a prefix, but user sets
			// usePrefixForDefaultNS = false, then we skip adding the prefix for default ns.
			const XMLCh* prefix = NULL;
			const XMLCh* ns = NULL;
			bool ok = _locationMap.GetFirstNamespace(&prefix,&ns);
			while (ok) {
				if(prefix != 0 && XMLString::stringLen(prefix) > 0) {
			
					XMLCh * xmlns_prefix = 0;
					if(!XMLString::startsWith(prefix, X("xmlns:"))) // For compatibility
						xmlns_prefix = Dc1Util::CatString(X("xmlns:"), prefix);
					else 
						xmlns_prefix = XMLString::replicate(prefix);
					
					if(defaultNS != 0 && XMLString::compareString(ns, defaultNS) == 0) {
						if(usePrefixForDefaultNS) {
							root->setAttributeNS(X("http://www.w3.org/2000/xmlns/"), xmlns_prefix, ns); // Mind the trailing slash!
							const XMLCh* p = _locationMap.GetFirstSchemaPrefix(defaultNS);
							root->setPrefix(p);
						}
					}
					else {
						// Add xmlns:prefix="URI" attribute 
						root->setAttributeNS(X("http://www.w3.org/2000/xmlns/"), xmlns_prefix, ns); // Mind the trailing slash!
					}
					Dc1Util::deallocate(&xmlns_prefix);
				}
				if((prefix = _locationMap.GetNextSchemaPrefix(ns)) != 0)
					continue;
			
				ok = _locationMap.GetNextNamespace(&prefix,&ns);
			}
			// Add xsi namespace for xsi:type needed for schemalocation
			root->setAttributeNS(X("http://www.w3.org/2000/xmlns/"), X("xmlns:xsi"), X("http://www.w3.org/2001/XMLSchema-instance"));
		}

		// Add schema location only to root node!
		if (writeHeader) {
			XMLCh* txt = _locationMap.ToText(); // TODO check this if more than one schema location
			root->setAttributeNS(X("http://www.w3.org/2001/XMLSchema-instance"), X("xsi:schemaLocation")
				, txt);
			Dc1Util::deallocate(&txt);
		}
	}
	
	node->Serialize(doc, nullNode, &root);
	
	
	// baw 09 04 2003 
	// Write XMLDecl if desired
	// Has to be done by hand, as this is not supported in DOM 2 impl
	bool doWriteXMLDecl = writeXMLDecl;
	// If default value is set, determine if whole document or fragment is to be written
	if (xmlDeclDefault) {
		// FTT This doesn't work for abstract or derived rootelements	
		// if (node->GetClassId() == Dc1Factory::GetTypeIndex(X("PreservationMetadataType"))) 
		if((Dc1PreservationMetadataPtr)node != 0)
			doWriteXMLDecl = true;
		else 
			doWriteXMLDecl = false;
	}
	
	if (doWriteXMLDecl) {
		// create the string, fixed len = 45
		XMLSize_t /*unsigned int*/ len = 45;
		// add length of other items to be written
		len += XMLString::stringLen(version);
		len += XMLString::stringLen(encoding);
		if (standalone) len+=3; 
		else len+=3;
		
		// build string
		XMLCh* xmlDeclStr = new XMLCh[len+1];
		xmlDeclStr[0] = '\0';
		
		XMLString::catString(xmlDeclStr,X("<?xml version=\""));
		XMLString::catString(xmlDeclStr,version);
		XMLString::catString(xmlDeclStr,X("\" encoding=\""));
		XMLString::catString(xmlDeclStr,encoding);
		XMLString::catString(xmlDeclStr,X("\" standalone=\""));
		if (standalone) XMLString::catString(xmlDeclStr,X("yes"));
		else XMLString::catString(xmlDeclStr,X("no"));
		XMLString::catString(xmlDeclStr,X("\" ?>"));
		
		// write to formatter
		XMLFormatter formatter(encoding,destination);
		// 8 bit				
		if ( (XMLString::compareString(encoding,X("UTF-8"))==0)
			||(XMLString::compareString(encoding,X("ISO-8859-1"))==0)) 
		{
			char* xmlDeclStrA = XMLString::transcode(xmlDeclStr);
			destination->writeChars((const XMLByte*) xmlDeclStrA, len, &formatter); 
			XMLString::release(&xmlDeclStrA);
		}
		// 16 bit
		else {
			destination->writeChars((const XMLByte*) xmlDeclStr, len * 2, &formatter); 
		}

		delete [] xmlDeclStr;
	}
	
#if XERCES_VERSION_MAJOR < 3
	theSerializer->writeNode(destination, *root);
#else
  theOutput->setEncoding(encoding);
	theOutput->setByteStream(destination);
	theSerializer->write(root, theOutput);
	theOutput->release();
#endif // XERCES_VERSION_MAJOR < 3	

	theSerializer->release();
	doc->release();
	
	// set content name back to previous value
	node->SetContentName(prevNodeName);
}

int MpafArchive::_DeserializeIntoCollection(InputSource &source,
	Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where)
{
	// don't set encoding here. source should know encoding already.
	
	XercesWrapper wrapper(source, fullSchema, validate, _locationMap);
	if (!wrapper.getRoot()) return -1;
	if (!collection) return -1;
	
	unsigned int old_size = collection->size();
	
	DOMElement * dummy = NULL;
	collection->Deserialize(wrapper.getDoc(), wrapper.getRoot(), & dummy);
	unsigned int new_size = collection->size();
	
	if (old_size == new_size) return -1;
	
	Dc1NodePtr new_node;
	
	switch (howtoadd) {
	case APPEND:
		// Nothing to do.
		return collection->size() - 1;
	case INSERT:
		assert(where >= 0 && where < (int)collection->size());
		if (!(where >= 0 && where < (int)collection->size())) return -1;
		new_node = collection->elementAt(new_size - 1);
		collection->removeElementAt(new_size - 1);
		collection->insertElementAt(new_node, where);
		return where;
	case REPLACE:
		assert(where >= 0 && where < (int)collection->size());
		if (!(where >= 0 && where < (int)collection->size())) return -1;
		new_node = collection->elementAt(new_size - 1);
		collection->removeElementAt(new_size - 1);
		collection->setElementAt(new_node, where);
		return where;
	default:
		return -1;
	}
}

Dc1NodePtr MpafArchive::_Deserialize(Dc1NodePtr &parentnode, InputSource &source)
{
	XercesWrapper wrapper(source, fullSchema, validate, _locationMap);
	bool isFragment = false;
	Dc1NodePtr rootNode;

  if(!wrapper.getRoot()) { // Document is empty
	return Dc1NodePtr(); // Bail out
  }
  
  if(parentnode == NULL) // Create decent root node
  {  
	if (Dc1Util::HasNodeName(wrapper.getRoot(), X("DIDL"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
 		  rootNode = CreatePreservationMetadataType;
	} 
	else
	{
	  XMLCh * msg = Dc1Util::CatString(X("MpafArchive::_Deserialize(): XML root nodes other than "), X("DIDL"));
	  Dc1Util::CatString(&msg, X(" need an xsi:type attribute to be deserialised correctly."));
	  Dc1Exception e(DC1_ERROR, msg);
	  Dc1Util::deallocate(&msg);
	  throw e;
	}
  }
  else // Check if node matches doc root type
  {		
	const XMLCh* name = wrapper.getRoot()->getNodeName();
//	const XMLCh* xsitype = wrapper.getRoot()->getAttribute(X("xsi:type"); // FTT for now xsi prefix is assumed
	NamespaceHelper result;
	Dc1Util::GetXsiType(wrapper.getRoot(), wrapper.getDoc(), &result);
	// Create child node based on the document's root node's type information
	XMLCh * xsitype = Dc1Util::QualifiedName(&result);
	
	if(!xsitype || XMLString::stringLen(xsitype) == 0) // No hint given, so is it root?
	{
	  if(xsitype) Dc1Util::deallocate(&xsitype);
	  if (Dc1Util::HasNodeName(wrapper.getRoot(), X("DIDL"), X("urn:mpeg:maf:schema:preservation:2015")))
	  {
 	  	  rootNode = parentnode;
 		}
 		else
 		{
		rootNode = parentnode->CreateChild(name, xsitype);
		if(!rootNode) {
		  XMLCh * msg = Dc1Util::CatString(X("MpafArchive::_Deserialize(): "), X("XML root node name "));
		  Dc1Util::CatString(&msg, name);
		  Dc1Util::CatString(&msg, X(" is not matching or xsi:type attribute is missing."));
		  Dc1Exception e(DC1_ERROR, msg);
		  Dc1Util::deallocate(&msg);
		  throw e;
		}		 
		isFragment = true; // Ok, this is a fragment
 		}
	} else { // We have an xsi:type hint
	  // Create child node based on the document's root node's type information
	  isFragment = true; // Ok, this is a fragment, so use the node as parent
	  rootNode = parentnode->CreateChild(name, xsitype);
	  Dc1Util::deallocate(&xsitype);
	}
  }	
#if 0	  
	
	else {
	  // Create child node based on the document's root node's type information
	  rootNode = node->CreateChild(type, xsitype);
	}
  }
 else
	  	throw Dc1Exception (DC1_ERROR, "Sorry, root element is abstract and needs a decent root node to start deserialization.");

				if (!Dc1Util::HasNodeName(wrapper.getRoot(), X("DIDL"), X("urn:mpeg:maf:schema:preservation:2015")))
				{
					// No valid root node in document.
					XMLCh * msg = Dc1Util::CatString(X("MpafArchive::_Deserialize(): Input source is not a valid MPEG-7 document. Root node "), X("DIDL"));
					Dc1Util::CatString(&msg, X(" node or proper fragment pointer is expected."));
					Dc1Exception e(DC1_ERROR, msg);
					Dc1Util::deallocate(&msg);
					throw e;
				}
  		  rootNode = CreatePreservationMetadataType;

	  } else { // Given node and root
			if (wrapper.getRoot()) { // Check if document root exists
				if (!Dc1Util::HasNodeName(wrapper.getRoot(), X("DIDL"), X("urn:mpeg:maf:schema:preservation:2015"))
				{
					// No valid root node in document.
					XMLCh * msg = Dc1Util::CatString(X("MpafArchive::_Deserialize: Input source is no valid MPEG-7 document. Root node "), X("DIDL"));
					Dc1Util::CatString(&msg, X(" is expected."));
					Dc1Exception e(DC1_ERROR, msg);
					Dc1Util::deallocate(&msg);
					throw e;
				}
			}
			rootNode = node; // Ok this is a root node
		} else {
		
			isFragment = true; // Ok, this is a fragment, so use the node as parent
			const XMLCh * name = wrapper.getRoot()->getNodeName();
			// const XMLCh* xsitype = wrapper.getRoot()->getAttribute(XMLString::transcode("xsi:type"));
			NamespaceHelper result;
			Dc1Util::GetXsiType(wrapper.getRoot(), wrapper.getDoc(), &result);
			// Create child node based on the document's root node's type information
			XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			rootNode = node->CreateChild(name, xsitype);
			Dc1Util::deallocate(&xsitype);
		}
	}
#endif

	if (rootNode != NULL) {
		DOMElement * dummy = NULL;
		rootNode->Deserialize(wrapper.getDoc(), wrapper.getRoot(), & dummy);
		// baw 2004-11-15: set content name of root node only if we are not parsing into a parent node
		if (!isFragment) 
			rootNode->SetContentName(XMLString::replicate(wrapper.getRoot()->getNodeName()));   
	}
	return rootNode;

#if 0	
/////////////////////////////
  if (wrapper.getRoot()) { // ok, there is a document
	if (node == NULL) { // deserialize whole mpeg-7 document
	  if (XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(
	  wrapper.getRoot()->getNodeName(), X("DIDL")) == 0) {
	rootNode = CreatePreservationMetadataType;
	  } else {
	// no valid root node in document.
	throw Dc1Exception(DC1_WARNING, "MpafArchive::_Deserialize: Input source is no mpeg-7 document. Root node not found.");
	  }
	} else { // parse fragment into node
	  const XMLCh* type = wrapper.getRoot()->getNodeName();
	  const XMLCh* xsitype = wrapper.getRoot()->getAttribute(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("xsi:type"));
	  // create child node based on the document's root node's type information
	  rootNode = node->CreateChild(type, xsitype);
	}
	
	if (rootNode != NULL) {
	  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * dummy = NULL;
	  rootNode->Deserialize(wrapper.getDoc(), wrapper.getRoot(), & dummy);
	  // baw 2004-11-15: set content name of root node only if we are not parsing into a parent node
	  if (node == NULL) rootNode->SetContentName(XMLString::replicate(wrapper.getRoot()->getNodeName()));   
	}
  }
////////////////////////////	
#endif 

  return rootNode;
}

void MpafArchive::SetEncoding(const XMLCh * encoding) 
{
	XMLString::release(&(this->encoding));
	this->encoding = XMLString::replicate(encoding);
}

char * MpafArchive::GetEncoding() 
{
	return XMLString::transcode(encoding);
}


void MpafArchive::SetSchemaLocation(const XMLCh* namespaceURI, const XMLCh* prefix, const XMLCh * schemaLocation) 
{
	_locationMap.SetSchemaLocation(namespaceURI,prefix,schemaLocation);
}

char * MpafArchive::GetSchemaLocation(const char* namespaceURI) 
{
	return XMLString::transcode(_locationMap.GetSchemaLocation(X(namespaceURI)));
}

void MpafArchive::SetDefaultNamespace(const XMLCh* namespaceURI, const XMLCh* prefix) 
{
	_locationMap.SetDefaultNamespace(namespaceURI, prefix);
}

bool MpafArchive::GetDefaultNamespace(const XMLCh* * namespaceURI, const XMLCh* * prefix) 
{
	return _locationMap.GetDefaultNamespace(namespaceURI, prefix);
}


void MpafArchive::DeleteBuffer(char** buffer) 
{
	// This is now on Xerces heap, TODO add Util::allocate() /deallocate() for 8 bit strings as well
	XMLString::release(buffer);
}

void MpafArchive::DeleteBuffer(XMLCh** buffer) 
{
	// This is now on Xerces heap
	Dc1Util::deallocate(buffer);
}

