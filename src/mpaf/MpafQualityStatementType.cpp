
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafQualityStatementType_ExtImplInclude.h


#include "MpafStatementType.h"
#include "MpafQualityStatementType_CollectionType.h"
#include "W3CNMTOKENS.h"
#include "MpafQualityStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "MpafQualityStatementType_LocalType.h" // Choice collection DescriptionUnit
#include "Mp7JrsMpeg7BaseType.h" // Choice collection element DescriptionUnit
#include "Mp7JrsCompleteDescriptionType.h" // Choice collection element Description

#include <assert.h>
IMpafQualityStatementType::IMpafQualityStatementType()
{

// no includefile for extension defined 
// file MpafQualityStatementType_ExtPropInit.h

}

IMpafQualityStatementType::~IMpafQualityStatementType()
{
// no includefile for extension defined 
// file MpafQualityStatementType_ExtPropCleanup.h

}

MpafQualityStatementType::MpafQualityStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafQualityStatementType::~MpafQualityStatementType()
{
	Cleanup();
}

void MpafQualityStatementType::Init()
{
	// Init base
	m_Base = CreateMpafStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_QualityStatementType_LocalType = MpafQualityStatementType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafQualityStatementType_ExtMyPropInit.h

}

void MpafQualityStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafQualityStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_QualityStatementType_LocalType);
}

void MpafQualityStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QualityStatementTypePtr, since we
	// might need GetBase(), which isn't defined in IQualityStatementType
	const Dc1Ptr< MpafQualityStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafStatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafQualityStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_QualityStatementType_LocalType);
		this->SetQualityStatementType_LocalType(Dc1Factory::CloneObject(tmp->GetQualityStatementType_LocalType()));
}

Dc1NodePtr MpafQualityStatementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafQualityStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafStatementType > MpafQualityStatementType::GetBase() const
{
	return m_Base;
}

MpafQualityStatementType_CollectionPtr MpafQualityStatementType::GetQualityStatementType_LocalType() const
{
		return m_QualityStatementType_LocalType;
}

void MpafQualityStatementType::SetQualityStatementType_LocalType(const MpafQualityStatementType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType::SetQualityStatementType_LocalType().");
	}
	if (m_QualityStatementType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_QualityStatementType_LocalType);
		m_QualityStatementType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QualityStatementType_LocalType) m_QualityStatementType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafQualityStatementType::GetmimeType() const
{
	return GetBase()->GetmimeType();
}

void MpafQualityStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType::SetmimeType().");
	}
	GetBase()->SetmimeType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafQualityStatementType::Getref() const
{
	return GetBase()->Getref();
}

bool MpafQualityStatementType::Existref() const
{
	return GetBase()->Existref();
}
void MpafQualityStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType::Setref().");
	}
	GetBase()->Setref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityStatementType::Invalidateref()
{
	GetBase()->Invalidateref();
}
XMLCh * MpafQualityStatementType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool MpafQualityStatementType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void MpafQualityStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityStatementType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
W3CNMTOKENSPtr MpafQualityStatementType::GetcontentEncoding() const
{
	return GetBase()->GetcontentEncoding();
}

bool MpafQualityStatementType::ExistcontentEncoding() const
{
	return GetBase()->ExistcontentEncoding();
}
void MpafQualityStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType::SetcontentEncoding().");
	}
	GetBase()->SetcontentEncoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityStatementType::InvalidatecontentEncoding()
{
	GetBase()->InvalidatecontentEncoding();
}

Dc1NodeEnum MpafQualityStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DescriptionUnit")) == 0)
	{
		// DescriptionUnit is contained in itemtype QualityStatementType_LocalType
		// in choice collection QualityStatementType_CollectionType

		context->Found = true;
		MpafQualityStatementType_CollectionPtr coll = GetQualityStatementType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQualityStatementType_CollectionType; // FTT, check this
				SetQualityStatementType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->GetDescriptionUnit()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				MpafQualityStatementType_LocalPtr item = CreateQualityStatementType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMpeg7BasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->SetDescriptionUnit(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->GetDescriptionUnit()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Description")) == 0)
	{
		// Description is contained in itemtype QualityStatementType_LocalType
		// in choice collection QualityStatementType_CollectionType

		context->Found = true;
		MpafQualityStatementType_CollectionPtr coll = GetQualityStatementType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQualityStatementType_CollectionType; // FTT, check this
				SetQualityStatementType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->GetDescription()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				MpafQualityStatementType_LocalPtr item = CreateQualityStatementType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCompleteDescriptionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->SetDescription(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((MpafQualityStatementType_LocalPtr)coll->elementAt(i))->GetDescription()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for QualityStatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "QualityStatementType");
	}
	return result;
}

XMLCh * MpafQualityStatementType::ToText() const
{
	return m_Base->ToText();
}


bool MpafQualityStatementType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafQualityStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafQualityStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("QualityStatementType"));
	// Element serialization:
	if (m_QualityStatementType_LocalType != MpafQualityStatementType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QualityStatementType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafQualityStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafStatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:maf:schema:preservation:2015:DescriptionUnit
			Dc1Util::HasNodeName(parent, X("DescriptionUnit"), X("urn:mpeg:maf:schema:preservation:2015"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:DescriptionUnit")) == 0)
				||
			// urn:mpeg:maf:schema:preservation:2015:Description
			Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:maf:schema:preservation:2015"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:Description")) == 0)
				))
		{
			// Deserialize factory type
			MpafQualityStatementType_CollectionPtr tmp = CreateQualityStatementType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQualityStatementType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafQualityStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafQualityStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("DescriptionUnit"), X("urn:mpeg:maf:schema:preservation:2015")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("DescriptionUnit")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:maf:schema:preservation:2015")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Description")) == 0)
	)
  {
	MpafQualityStatementType_CollectionPtr tmp = CreateQualityStatementType_CollectionType; // FTT, check this
	this->SetQualityStatementType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafQualityStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetQualityStatementType_LocalType() != Dc1NodePtr())
		result.Insert(GetQualityStatementType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafQualityStatementType_ExtMethodImpl.h


