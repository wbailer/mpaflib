
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafRepresentationType_ExtImplInclude.h


#include "MpafItemType.h"
#include "MpafRepresentationType_Condition_CollectionType.h"
#include "MpafRepresentationType_Descriptor_CollectionType.h"
#include "MpafRepresentationType_Choice_CollectionType.h"
#include "MpafRepresentationType_Item_CollectionType.h"
#include "MpafRepresentationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21ConditionType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Condition
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor
#include "Mpeg21ChoiceType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Choice
#include "MpafEssenceType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Item

#include <assert.h>
IMpafRepresentationType::IMpafRepresentationType()
{

// no includefile for extension defined 
// file MpafRepresentationType_ExtPropInit.h

}

IMpafRepresentationType::~IMpafRepresentationType()
{
// no includefile for extension defined 
// file MpafRepresentationType_ExtPropCleanup.h

}

MpafRepresentationType::MpafRepresentationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafRepresentationType::~MpafRepresentationType()
{
	Cleanup();
}

void MpafRepresentationType::Init()
{
	// Init base
	m_Base = CreateItemType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Condition = MpafRepresentationType_Condition_CollectionPtr(); // Collection
	m_Descriptor = MpafRepresentationType_Descriptor_CollectionPtr(); // Collection
	m_Choice = MpafRepresentationType_Choice_CollectionPtr(); // Collection
	m_Item = MpafRepresentationType_Item_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafRepresentationType_ExtMyPropInit.h

}

void MpafRepresentationType::Cleanup()
{
// no includefile for extension defined 
// file MpafRepresentationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Condition);
	// Dc1Factory::DeleteObject(m_Descriptor);
	// Dc1Factory::DeleteObject(m_Choice);
	// Dc1Factory::DeleteObject(m_Item);
}

void MpafRepresentationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RepresentationTypePtr, since we
	// might need GetBase(), which isn't defined in IRepresentationType
	const Dc1Ptr< MpafRepresentationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafItemPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafRepresentationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Condition);
		this->SetCondition(Dc1Factory::CloneObject(tmp->GetCondition()));
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
		// Dc1Factory::DeleteObject(m_Choice);
		this->SetChoice(Dc1Factory::CloneObject(tmp->GetChoice()));
		// Dc1Factory::DeleteObject(m_Item);
		this->SetItem(Dc1Factory::CloneObject(tmp->GetItem()));
}

Dc1NodePtr MpafRepresentationType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafRepresentationType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafItemType > MpafRepresentationType::GetBase() const
{
	return m_Base;
}

MpafRepresentationType_Condition_CollectionPtr MpafRepresentationType::GetCondition() const
{
		return m_Condition;
}

MpafRepresentationType_Descriptor_CollectionPtr MpafRepresentationType::GetDescriptor() const
{
		return m_Descriptor;
}

MpafRepresentationType_Choice_CollectionPtr MpafRepresentationType::GetChoice() const
{
		return m_Choice;
}

MpafRepresentationType_Item_CollectionPtr MpafRepresentationType::GetItem() const
{
		return m_Item;
}

void MpafRepresentationType::SetCondition(const MpafRepresentationType_Condition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafRepresentationType::SetCondition().");
	}
	if (m_Condition != item)
	{
		// Dc1Factory::DeleteObject(m_Condition);
		m_Condition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Condition) m_Condition->SetParent(m_myself.getPointer());
		if(m_Condition != MpafRepresentationType_Condition_CollectionPtr())
		{
			m_Condition->SetContentName(XMLString::transcode("Condition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafRepresentationType::SetDescriptor(const MpafRepresentationType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafRepresentationType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != MpafRepresentationType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafRepresentationType::SetChoice(const MpafRepresentationType_Choice_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafRepresentationType::SetChoice().");
	}
	if (m_Choice != item)
	{
		// Dc1Factory::DeleteObject(m_Choice);
		m_Choice = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Choice) m_Choice->SetParent(m_myself.getPointer());
		if(m_Choice != MpafRepresentationType_Choice_CollectionPtr())
		{
			m_Choice->SetContentName(XMLString::transcode("Choice"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafRepresentationType::SetItem(const MpafRepresentationType_Item_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafRepresentationType::SetItem().");
	}
	if (m_Item != item)
	{
		// Dc1Factory::DeleteObject(m_Item);
		m_Item = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Item) m_Item->SetParent(m_myself.getPointer());
		if(m_Item != MpafRepresentationType_Item_CollectionPtr())
		{
			m_Item->SetContentName(XMLString::transcode("Item"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafRepresentationType::Geturi() const
{
	return GetBase()->Geturi();
}

bool MpafRepresentationType::Existuri() const
{
	return GetBase()->Existuri();
}
void MpafRepresentationType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafRepresentationType::Seturi().");
	}
	GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafRepresentationType::Invalidateuri()
{
	GetBase()->Invalidateuri();
}

Dc1NodeEnum MpafRepresentationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Condition")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Condition is item of abstract type ConditionType
		// in element collection RepresentationType_Condition_CollectionType
		
		context->Found = true;
		MpafRepresentationType_Condition_CollectionPtr coll = GetCondition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRepresentationType_Condition_CollectionType; // FTT, check this
				SetCondition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21ConditionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Descriptor is item of abstract type DescriptorType
		// in element collection RepresentationType_Descriptor_CollectionType
		
		context->Found = true;
		MpafRepresentationType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRepresentationType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21DescriptorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Choice")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Choice is item of abstract type ChoiceType
		// in element collection RepresentationType_Choice_CollectionType
		
		context->Found = true;
		MpafRepresentationType_Choice_CollectionPtr coll = GetChoice();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRepresentationType_Choice_CollectionType; // FTT, check this
				SetChoice(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21ChoicePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Item")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Item is item of type EssenceType
		// in element collection RepresentationType_Item_CollectionType
		
		context->Found = true;
		MpafRepresentationType_Item_CollectionPtr coll = GetItem();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRepresentationType_Item_CollectionType; // FTT, check this
				SetItem(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType")))) != empty)
			{
				// Is type allowed
				MpafEssencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RepresentationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RepresentationType");
	}
	return result;
}

XMLCh * MpafRepresentationType::ToText() const
{
	return m_Base->ToText();
}


bool MpafRepresentationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafRepresentationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafRepresentationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("RepresentationType"));
	// Element serialization:
	if (m_Condition != MpafRepresentationType_Condition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Condition->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Descriptor != MpafRepresentationType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Choice != MpafRepresentationType_Choice_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Choice->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Item != MpafRepresentationType_Item_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Item->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafRepresentationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafItemType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Condition"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Condition")) == 0))
		{
			// Deserialize factory type
			MpafRepresentationType_Condition_CollectionPtr tmp = CreateRepresentationType_Condition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCondition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			MpafRepresentationType_Descriptor_CollectionPtr tmp = CreateRepresentationType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Choice"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Choice")) == 0))
		{
			// Deserialize factory type
			MpafRepresentationType_Choice_CollectionPtr tmp = CreateRepresentationType_Choice_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetChoice(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Item"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Item")) == 0))
		{
			// Deserialize factory type
			MpafRepresentationType_Item_CollectionPtr tmp = CreateRepresentationType_Item_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetItem(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafRepresentationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafRepresentationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Condition")) == 0))
  {
	MpafRepresentationType_Condition_CollectionPtr tmp = CreateRepresentationType_Condition_CollectionType; // FTT, check this
	this->SetCondition(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	MpafRepresentationType_Descriptor_CollectionPtr tmp = CreateRepresentationType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Choice")) == 0))
  {
	MpafRepresentationType_Choice_CollectionPtr tmp = CreateRepresentationType_Choice_CollectionType; // FTT, check this
	this->SetChoice(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Item")) == 0))
  {
	MpafRepresentationType_Item_CollectionPtr tmp = CreateRepresentationType_Item_CollectionType; // FTT, check this
	this->SetItem(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafRepresentationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCondition() != Dc1NodePtr())
		result.Insert(GetCondition());
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	if (GetChoice() != Dc1NodePtr())
		result.Insert(GetChoice());
	if (GetItem() != Dc1NodePtr())
		result.Insert(GetItem());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafRepresentationType_ExtMethodImpl.h


