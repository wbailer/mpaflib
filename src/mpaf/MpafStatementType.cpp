
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafStatementType_ExtImplInclude.h


#include "Mpeg21StatementType.h"
#include "W3CNMTOKENS.h"
#include "MpafStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafStatementType::IMpafStatementType()
{

// no includefile for extension defined 
// file MpafStatementType_ExtPropInit.h

}

IMpafStatementType::~IMpafStatementType()
{
// no includefile for extension defined 
// file MpafStatementType_ExtPropCleanup.h

}

MpafStatementType::MpafStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafStatementType::~MpafStatementType()
{
	Cleanup();
}

void MpafStatementType::Init()
{
	// Init base
	m_Base = CreateStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_mimeType = NULL; // String
	m_ref = NULL; // String
	m_ref_Exist = false;
	m_encoding = NULL; // String
	m_encoding_Exist = false;
	m_contentEncoding = W3CNMTOKENSPtr(); // Collection
	m_contentEncoding_Exist = false;



// no includefile for extension defined 
// file MpafStatementType_ExtMyPropInit.h

}

void MpafStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_mimeType); // String
	XMLString::release(&m_ref); // String
	XMLString::release(&m_encoding); // String
	// Dc1Factory::DeleteObject(m_contentEncoding);
}

void MpafStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StatementTypePtr, since we
	// might need GetBase(), which isn't defined in IStatementType
	const Dc1Ptr< MpafStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mpeg21StatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_mimeType); // String
		this->SetmimeType(XMLString::replicate(tmp->GetmimeType()));
	}
	{
	XMLString::release(&m_ref); // String
	if (tmp->Existref())
	{
		this->Setref(XMLString::replicate(tmp->Getref()));
	}
	else
	{
		Invalidateref();
	}
	}
	{
	XMLString::release(&m_encoding); // String
	if (tmp->Existencoding())
	{
		this->Setencoding(XMLString::replicate(tmp->Getencoding()));
	}
	else
	{
		Invalidateencoding();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_contentEncoding);
	if (tmp->ExistcontentEncoding())
	{
		this->SetcontentEncoding(Dc1Factory::CloneObject(tmp->GetcontentEncoding()));
	}
	else
	{
		InvalidatecontentEncoding();
	}
	}
}

Dc1NodePtr MpafStatementType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr MpafStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mpeg21StatementType > MpafStatementType::GetBase() const
{
	return m_Base;
}

XMLCh * MpafStatementType::GetmimeType() const
{
	return m_mimeType;
}

void MpafStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafStatementType::SetmimeType().");
	}
	m_mimeType = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafStatementType::Getref() const
{
	return m_ref;
}

bool MpafStatementType::Existref() const
{
	return m_ref_Exist;
}
void MpafStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafStatementType::Setref().");
	}
	m_ref = item;
	m_ref_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafStatementType::Invalidateref()
{
	m_ref_Exist = false;
}
XMLCh * MpafStatementType::Getencoding() const
{
	return m_encoding;
}

bool MpafStatementType::Existencoding() const
{
	return m_encoding_Exist;
}
void MpafStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafStatementType::Setencoding().");
	}
	m_encoding = item;
	m_encoding_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafStatementType::Invalidateencoding()
{
	m_encoding_Exist = false;
}
W3CNMTOKENSPtr MpafStatementType::GetcontentEncoding() const
{
	return m_contentEncoding;
}

bool MpafStatementType::ExistcontentEncoding() const
{
	return m_contentEncoding_Exist;
}
void MpafStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafStatementType::SetcontentEncoding().");
	}
	m_contentEncoding = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_contentEncoding) m_contentEncoding->SetParent(m_myself.getPointer());
	m_contentEncoding_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafStatementType::InvalidatecontentEncoding()
{
	m_contentEncoding_Exist = false;
}

Dc1NodeEnum MpafStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mimeType")) == 0)
	{
		// mimeType is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("ref")) == 0)
	{
		// ref is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("encoding")) == 0)
	{
		// encoding is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("contentEncoding")) == 0)
	{
		// contentEncoding is simple attribute NMTOKENS
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CNMTOKENSPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StatementType");
	}
	return result;
}

XMLCh * MpafStatementType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafStatementType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void MpafStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("StatementType"));
	// Attribute Serialization:
	// String
	if(m_mimeType != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("mimeType"), m_mimeType);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_ref_Exist)
	{
	// String
	if(m_ref != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("ref"), m_ref);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_encoding_Exist)
	{
	// String
	if(m_encoding != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("encoding"), m_encoding);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_contentEncoding_Exist)
	{
	// Collection
	if(m_contentEncoding != W3CNMTOKENSPtr())
	{
		XMLCh * tmp = m_contentEncoding->ToText();
		element->setAttributeNS(X(""), X("contentEncoding"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool MpafStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mimeType")))
	{
		// Deserialize string type
		this->SetmimeType(Dc1Convert::TextToString(parent->getAttribute(X("mimeType"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("ref")))
	{
		// Deserialize string type
		this->Setref(Dc1Convert::TextToString(parent->getAttribute(X("ref"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("encoding")))
	{
		// Deserialize string type
		this->Setencoding(Dc1Convert::TextToString(parent->getAttribute(X("encoding"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("contentEncoding")))
	{
		// Deserialize collection type
		W3CNMTOKENSPtr tmp = CreateNMTOKENS; // FTT, check this
		tmp->Parse(parent->getAttribute(X("contentEncoding")));
		this->SetcontentEncoding(tmp);
		* current = parent;
	}

	// Extensionbase is Mpeg21StatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file MpafStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum MpafStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafStatementType_ExtMethodImpl.h


