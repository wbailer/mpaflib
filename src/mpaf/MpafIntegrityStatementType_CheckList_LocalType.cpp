
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtImplInclude.h


#include "MpafIntegrityStatementType_CheckList_Entity_CollectionType.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType.h"
#include "MpafIntegrityStatementType_CheckList_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:DeprecatedEntity

#include <assert.h>
IMpafIntegrityStatementType_CheckList_LocalType::IMpafIntegrityStatementType_CheckList_LocalType()
{

// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtPropInit.h

}

IMpafIntegrityStatementType_CheckList_LocalType::~IMpafIntegrityStatementType_CheckList_LocalType()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtPropCleanup.h

}

MpafIntegrityStatementType_CheckList_LocalType::MpafIntegrityStatementType_CheckList_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafIntegrityStatementType_CheckList_LocalType::~MpafIntegrityStatementType_CheckList_LocalType()
{
	Cleanup();
}

void MpafIntegrityStatementType_CheckList_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Entity = MpafIntegrityStatementType_CheckList_Entity_CollectionPtr(); // Collection
	m_DeprecatedEntity = MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtMyPropInit.h

}

void MpafIntegrityStatementType_CheckList_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Entity);
	// Dc1Factory::DeleteObject(m_DeprecatedEntity);
}

void MpafIntegrityStatementType_CheckList_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IntegrityStatementType_CheckList_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IIntegrityStatementType_CheckList_LocalType
	const Dc1Ptr< MpafIntegrityStatementType_CheckList_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Entity);
		this->SetEntity(Dc1Factory::CloneObject(tmp->GetEntity()));
		// Dc1Factory::DeleteObject(m_DeprecatedEntity);
		this->SetDeprecatedEntity(Dc1Factory::CloneObject(tmp->GetDeprecatedEntity()));
}

Dc1NodePtr MpafIntegrityStatementType_CheckList_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafIntegrityStatementType_CheckList_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


MpafIntegrityStatementType_CheckList_Entity_CollectionPtr MpafIntegrityStatementType_CheckList_LocalType::GetEntity() const
{
		return m_Entity;
}

MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr MpafIntegrityStatementType_CheckList_LocalType::GetDeprecatedEntity() const
{
		return m_DeprecatedEntity;
}

void MpafIntegrityStatementType_CheckList_LocalType::SetEntity(const MpafIntegrityStatementType_CheckList_Entity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType_CheckList_LocalType::SetEntity().");
	}
	if (m_Entity != item)
	{
		// Dc1Factory::DeleteObject(m_Entity);
		m_Entity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Entity) m_Entity->SetParent(m_myself.getPointer());
		if(m_Entity != MpafIntegrityStatementType_CheckList_Entity_CollectionPtr())
		{
			m_Entity->SetContentName(XMLString::transcode("Entity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType_CheckList_LocalType::SetDeprecatedEntity(const MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType_CheckList_LocalType::SetDeprecatedEntity().");
	}
	if (m_DeprecatedEntity != item)
	{
		// Dc1Factory::DeleteObject(m_DeprecatedEntity);
		m_DeprecatedEntity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DeprecatedEntity) m_DeprecatedEntity->SetParent(m_myself.getPointer());
		if(m_DeprecatedEntity != MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr())
		{
			m_DeprecatedEntity->SetContentName(XMLString::transcode("DeprecatedEntity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafIntegrityStatementType_CheckList_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Entity")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DeprecatedEntity")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:DeprecatedEntity is item of type IntegrityStatementType_CheckList_DeprecatedEntity_LocalType
		// in element collection IntegrityStatementType_CheckList_DeprecatedEntity_CollectionType
		
		context->Found = true;
		MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr coll = GetDeprecatedEntity();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType; // FTT, check this
				SetDeprecatedEntity(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_DeprecatedEntity_LocalType")))) != empty)
			{
				// Is type allowed
				MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IntegrityStatementType_CheckList_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IntegrityStatementType_CheckList_LocalType");
	}
	return result;
}

XMLCh * MpafIntegrityStatementType_CheckList_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafIntegrityStatementType_CheckList_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafIntegrityStatementType_CheckList_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("IntegrityStatementType_CheckList_LocalType"));
	// Element serialization:
	if (m_Entity != MpafIntegrityStatementType_CheckList_Entity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Entity->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DeprecatedEntity != MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DeprecatedEntity->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafIntegrityStatementType_CheckList_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Entity"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Entity")) == 0))
		{
			// Deserialize factory type
			MpafIntegrityStatementType_CheckList_Entity_CollectionPtr tmp = CreateIntegrityStatementType_CheckList_Entity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEntity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DeprecatedEntity"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DeprecatedEntity")) == 0))
		{
			// Deserialize factory type
			MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr tmp = CreateIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDeprecatedEntity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafIntegrityStatementType_CheckList_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Entity")) == 0))
  {
	MpafIntegrityStatementType_CheckList_Entity_CollectionPtr tmp = CreateIntegrityStatementType_CheckList_Entity_CollectionType; // FTT, check this
	this->SetEntity(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DeprecatedEntity")) == 0))
  {
	MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr tmp = CreateIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType; // FTT, check this
	this->SetDeprecatedEntity(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafIntegrityStatementType_CheckList_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEntity() != Dc1NodePtr())
		result.Insert(GetEntity());
	if (GetDeprecatedEntity() != Dc1NodePtr())
		result.Insert(GetDeprecatedEntity());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_LocalType_ExtMethodImpl.h


