
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtImplInclude.h


#include "MpafIdentifierBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafIdentifierBaseType::IMpafIdentifierBaseType()
{

// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtPropInit.h

}

IMpafIdentifierBaseType::~IMpafIdentifierBaseType()
{
// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtPropCleanup.h

}

MpafIdentifierBaseType::MpafIdentifierBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafIdentifierBaseType::~MpafIdentifierBaseType()
{
	Cleanup();
}

void MpafIdentifierBaseType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_anyURI = XMLString::transcode(""); //  Mandatory String
	m_string = XMLString::transcode(""); //  Mandatory String


// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtMyPropInit.h

}

void MpafIdentifierBaseType::Cleanup()
{
// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtMyPropCleanup.h


	XMLString::release(&this->m_anyURI);
	this->m_anyURI = NULL;
	XMLString::release(&this->m_string);
	this->m_string = NULL;
}

void MpafIdentifierBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IdentifierBaseTypePtr, since we
	// might need GetBase(), which isn't defined in IIdentifierBaseType
	const Dc1Ptr< MpafIdentifierBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if(tmp->GetanyURI() != NULL)
	{
		this->SetanyURI(XMLString::replicate(tmp->GetanyURI()));
		return; 
	}
	if(tmp->Getstring() != NULL)
	{
		this->Setstring(XMLString::replicate(tmp->Getstring()));
		return; 
	}
	}
}

Dc1NodePtr MpafIdentifierBaseType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafIdentifierBaseType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * MpafIdentifierBaseType::GetanyURI() const
{
		return m_anyURI;
}

XMLCh * MpafIdentifierBaseType::Getstring() const
{
		return m_string;
}

void MpafIdentifierBaseType::SetanyURI(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIdentifierBaseType::SetanyURI().");
	}
	if (m_anyURI != item)
	{
		XMLString::release(&m_anyURI);
		m_anyURI = item;
	XMLString::release(&this->m_string);
	m_string = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIdentifierBaseType::Setstring(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIdentifierBaseType::Setstring().");
	}
	if (m_string != item)
	{
		XMLString::release(&m_string);
		m_string = item;
	XMLString::release(&this->m_anyURI);
	m_anyURI = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafIdentifierBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * MpafIdentifierBaseType::ToText() const
{
	if(m_anyURI != NULL)
	{
		return m_anyURI;
	}
	if(m_string != NULL)
	{
		return m_string;
	}
	return XMLString::transcode("");
}


bool MpafIdentifierBaseType::Parse(const XMLCh * const txt)
{

	// Parse stu type "anyURI"
	{
		XMLCh * tmp = XMLString::replicate(txt);
		this->SetanyURI(tmp);
		return true;
	}

	// Parse stu type "string"
	{
		XMLCh * tmp = XMLString::replicate(txt);
		this->Setstring(tmp);
		return true;
	}
	return false;
}

bool MpafIdentifierBaseType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// MpafIdentifierBaseType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* MpafIdentifierBaseType::ContentToString() const
{
	// MpafIdentifierBaseType has no attributes so forward it to ToText()
	return ToText();
}

void MpafIdentifierBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("IdentifierBaseType"));
	// Element serialization:
	 // String
	if(m_anyURI != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_anyURI, X("urn:mpeg:maf:schema:preservation:2015"), X("anyURI"), false);
	 // String
	if(m_string != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_string, X("urn:mpeg:maf:schema:preservation:2015"), X("string"), false);

}

bool MpafIdentifierBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("anyURI"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->SetanyURI(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetanyURI(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("string"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->Setstring(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setstring(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafIdentifierBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum MpafIdentifierBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafIdentifierBaseType_ExtMethodImpl.h


