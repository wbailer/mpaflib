
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtImplInclude.h


#include "MpafStatementType.h"
#include "MpafIntegrityStatementType_CheckList_LocalType.h"
#include "MpafIntegrityStatementType_ExternalDependency_CollectionType.h"
#include "W3CNMTOKENS.h"
#include "MpafIntegrityStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "MpafRelatedEntityType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:ExternalDependency

#include <assert.h>
IMpafIntegrityStatementType::IMpafIntegrityStatementType()
{

// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtPropInit.h

}

IMpafIntegrityStatementType::~IMpafIntegrityStatementType()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtPropCleanup.h

}

MpafIntegrityStatementType::MpafIntegrityStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafIntegrityStatementType::~MpafIntegrityStatementType()
{
	Cleanup();
}

void MpafIntegrityStatementType::Init()
{
	// Init base
	m_Base = CreateMpafStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_CheckList = MpafIntegrityStatementType_CheckList_LocalPtr(); // Class
	m_CheckList_Exist = false;
	m_ExternalDependency = MpafIntegrityStatementType_ExternalDependency_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtMyPropInit.h

}

void MpafIntegrityStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_CheckList);
	// Dc1Factory::DeleteObject(m_ExternalDependency);
}

void MpafIntegrityStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IntegrityStatementTypePtr, since we
	// might need GetBase(), which isn't defined in IIntegrityStatementType
	const Dc1Ptr< MpafIntegrityStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafStatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafIntegrityStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidCheckList())
	{
		// Dc1Factory::DeleteObject(m_CheckList);
		this->SetCheckList(Dc1Factory::CloneObject(tmp->GetCheckList()));
	}
	else
	{
		InvalidateCheckList();
	}
		// Dc1Factory::DeleteObject(m_ExternalDependency);
		this->SetExternalDependency(Dc1Factory::CloneObject(tmp->GetExternalDependency()));
}

Dc1NodePtr MpafIntegrityStatementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafIntegrityStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafStatementType > MpafIntegrityStatementType::GetBase() const
{
	return m_Base;
}

MpafIntegrityStatementType_CheckList_LocalPtr MpafIntegrityStatementType::GetCheckList() const
{
		return m_CheckList;
}

// Element is optional
bool MpafIntegrityStatementType::IsValidCheckList() const
{
	return m_CheckList_Exist;
}

MpafIntegrityStatementType_ExternalDependency_CollectionPtr MpafIntegrityStatementType::GetExternalDependency() const
{
		return m_ExternalDependency;
}

void MpafIntegrityStatementType::SetCheckList(const MpafIntegrityStatementType_CheckList_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::SetCheckList().");
	}
	if (m_CheckList != item || m_CheckList_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CheckList);
		m_CheckList = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CheckList) m_CheckList->SetParent(m_myself.getPointer());
		if (m_CheckList && m_CheckList->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CheckList->UseTypeAttribute = true;
		}
		m_CheckList_Exist = true;
		if(m_CheckList != MpafIntegrityStatementType_CheckList_LocalPtr())
		{
			m_CheckList->SetContentName(XMLString::transcode("CheckList"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType::InvalidateCheckList()
{
	m_CheckList_Exist = false;
}
void MpafIntegrityStatementType::SetExternalDependency(const MpafIntegrityStatementType_ExternalDependency_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::SetExternalDependency().");
	}
	if (m_ExternalDependency != item)
	{
		// Dc1Factory::DeleteObject(m_ExternalDependency);
		m_ExternalDependency = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ExternalDependency) m_ExternalDependency->SetParent(m_myself.getPointer());
		if(m_ExternalDependency != MpafIntegrityStatementType_ExternalDependency_CollectionPtr())
		{
			m_ExternalDependency->SetContentName(XMLString::transcode("ExternalDependency"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafIntegrityStatementType::GetmimeType() const
{
	return GetBase()->GetmimeType();
}

void MpafIntegrityStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::SetmimeType().");
	}
	GetBase()->SetmimeType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafIntegrityStatementType::Getref() const
{
	return GetBase()->Getref();
}

bool MpafIntegrityStatementType::Existref() const
{
	return GetBase()->Existref();
}
void MpafIntegrityStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::Setref().");
	}
	GetBase()->Setref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType::Invalidateref()
{
	GetBase()->Invalidateref();
}
XMLCh * MpafIntegrityStatementType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool MpafIntegrityStatementType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void MpafIntegrityStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
W3CNMTOKENSPtr MpafIntegrityStatementType::GetcontentEncoding() const
{
	return GetBase()->GetcontentEncoding();
}

bool MpafIntegrityStatementType::ExistcontentEncoding() const
{
	return GetBase()->ExistcontentEncoding();
}
void MpafIntegrityStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType::SetcontentEncoding().");
	}
	GetBase()->SetcontentEncoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType::InvalidatecontentEncoding()
{
	GetBase()->InvalidatecontentEncoding();
}

Dc1NodeEnum MpafIntegrityStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("CheckList")) == 0)
	{
		// CheckList is simple element IntegrityStatementType_CheckList_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCheckList()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_LocalType")))) != empty)
			{
				// Is type allowed
				MpafIntegrityStatementType_CheckList_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCheckList(p, client);
					if((p = GetCheckList()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ExternalDependency")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:ExternalDependency is item of type RelatedEntityType
		// in element collection IntegrityStatementType_ExternalDependency_CollectionType
		
		context->Found = true;
		MpafIntegrityStatementType_ExternalDependency_CollectionPtr coll = GetExternalDependency();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateIntegrityStatementType_ExternalDependency_CollectionType; // FTT, check this
				SetExternalDependency(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RelatedEntityType")))) != empty)
			{
				// Is type allowed
				MpafRelatedEntityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IntegrityStatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IntegrityStatementType");
	}
	return result;
}

XMLCh * MpafIntegrityStatementType::ToText() const
{
	return m_Base->ToText();
}


bool MpafIntegrityStatementType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafIntegrityStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("IntegrityStatementType"));
	// Element serialization:
	// Class
	
	if (m_CheckList != MpafIntegrityStatementType_CheckList_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CheckList->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("CheckList"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CheckList->Serialize(doc, element, &elem);
	}
	if (m_ExternalDependency != MpafIntegrityStatementType_ExternalDependency_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ExternalDependency->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafIntegrityStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafStatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CheckList"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CheckList")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIntegrityStatementType_CheckList_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCheckList(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ExternalDependency"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ExternalDependency")) == 0))
		{
			// Deserialize factory type
			MpafIntegrityStatementType_ExternalDependency_CollectionPtr tmp = CreateIntegrityStatementType_ExternalDependency_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetExternalDependency(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafIntegrityStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("CheckList")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIntegrityStatementType_CheckList_LocalType; // FTT, check this
	}
	this->SetCheckList(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ExternalDependency")) == 0))
  {
	MpafIntegrityStatementType_ExternalDependency_CollectionPtr tmp = CreateIntegrityStatementType_ExternalDependency_CollectionType; // FTT, check this
	this->SetExternalDependency(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafIntegrityStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCheckList() != Dc1NodePtr())
		result.Insert(GetCheckList());
	if (GetExternalDependency() != Dc1NodePtr())
		result.Insert(GetExternalDependency());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafIntegrityStatementType_ExtMethodImpl.h


