/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Based on Factory_CPP.template

#include <stdio.h>
#include "MpafPreservationObjectType_Condition_CollectionTypeFactory.h"
#include "MpafPreservationObjectType_Condition_CollectionType.h"

MpafPreservationObjectType_Condition_CollectionTypeFactory::MpafPreservationObjectType_Condition_CollectionTypeFactory()
	: Dc1Factory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Condition_CollectionType", "urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Condition_CollectionType")
{
}

MpafPreservationObjectType_Condition_CollectionTypeFactory::~MpafPreservationObjectType_Condition_CollectionTypeFactory()
{
}

const char * MpafPreservationObjectType_Condition_CollectionTypeFactory::GetTypeName() const
{
	// Note: This is slow
	// return MpafPreservationObjectType_Condition_CollectionType().GetTypeName();
	
	// FTT Easier but needs more memory in debug mode (no string pooling)
	return "urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Condition_CollectionType";
}

Dc1NodePtr MpafPreservationObjectType_Condition_CollectionTypeFactory::CreateObject()
{
	return Dc1NodePtr(new MpafPreservationObjectType_Condition_CollectionType);
}
