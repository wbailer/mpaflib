
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafOperatorType_ExtImplInclude.h


#include "MpafMPAFBaseType.h"
#include "MpafOperatorType_Descriptor_CollectionType.h"
#include "MpafOperatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor

#include <assert.h>
IMpafOperatorType::IMpafOperatorType()
{

// no includefile for extension defined 
// file MpafOperatorType_ExtPropInit.h

}

IMpafOperatorType::~IMpafOperatorType()
{
// no includefile for extension defined 
// file MpafOperatorType_ExtPropCleanup.h

}

MpafOperatorType::MpafOperatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafOperatorType::~MpafOperatorType()
{
	Cleanup();
}

void MpafOperatorType::Init()
{
	// Init base
	m_Base = CreateMPAFBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_type = NULL; // String

	// Init elements (element, union sequence choice all any)
	
	m_Descriptor = MpafOperatorType_Descriptor_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafOperatorType_ExtMyPropInit.h

}

void MpafOperatorType::Cleanup()
{
// no includefile for extension defined 
// file MpafOperatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_type); // String
	// Dc1Factory::DeleteObject(m_Descriptor);
}

void MpafOperatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use OperatorTypePtr, since we
	// might need GetBase(), which isn't defined in IOperatorType
	const Dc1Ptr< MpafOperatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafMPAFBasePtr(Dc1Factory::CloneObject((dynamic_cast< const MpafOperatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_type); // String
		this->Settype(XMLString::replicate(tmp->Gettype()));
	}
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
}

Dc1NodePtr MpafOperatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr MpafOperatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< MpafMPAFBaseType > MpafOperatorType::GetBase() const
{
	return m_Base;
}

XMLCh * MpafOperatorType::Gettype() const
{
	return m_type;
}

void MpafOperatorType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOperatorType::Settype().");
	}
	m_type = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

MpafOperatorType_Descriptor_CollectionPtr MpafOperatorType::GetDescriptor() const
{
		return m_Descriptor;
}

void MpafOperatorType::SetDescriptor(const MpafOperatorType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOperatorType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != MpafOperatorType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafOperatorType::Getid() const
{
	return GetBase()->Getid();
}

bool MpafOperatorType::Existid() const
{
	return GetBase()->Existid();
}
void MpafOperatorType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOperatorType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafOperatorType::Invalidateid()
{
	GetBase()->Invalidateid();
}
XMLCh * MpafOperatorType::Geturi() const
{
	return GetBase()->Geturi();
}

void MpafOperatorType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafOperatorType::Seturi().");
	}
	GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafOperatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Descriptor is item of abstract type DescriptorType
		// in element collection OperatorType_Descriptor_CollectionType
		
		context->Found = true;
		MpafOperatorType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateOperatorType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21DescriptorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for OperatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "OperatorType");
	}
	return result;
}

XMLCh * MpafOperatorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafOperatorType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafOperatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafOperatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("OperatorType"));
	// Attribute Serialization:
	// String
	if(m_type != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("type"), m_type);
	}
	// Element serialization:
	if (m_Descriptor != MpafOperatorType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafOperatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize string type
		this->Settype(Dc1Convert::TextToString(parent->getAttribute(X("type"))));
		* current = parent;
	}

	// Extensionbase is MpafMPAFBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			MpafOperatorType_Descriptor_CollectionPtr tmp = CreateOperatorType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafOperatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafOperatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	MpafOperatorType_Descriptor_CollectionPtr tmp = CreateOperatorType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafOperatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafOperatorType_ExtMethodImpl.h


