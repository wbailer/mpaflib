
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtImplInclude.h


#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0.h"
#include "MpafPreservationMetadataType_DIDLInfo_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafPreservationMetadataType_DIDLInfo_LocalType::IMpafPreservationMetadataType_DIDLInfo_LocalType()
{

// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtPropInit.h

}

IMpafPreservationMetadataType_DIDLInfo_LocalType::~IMpafPreservationMetadataType_DIDLInfo_LocalType()
{
// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtPropCleanup.h

}

MpafPreservationMetadataType_DIDLInfo_LocalType::MpafPreservationMetadataType_DIDLInfo_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafPreservationMetadataType_DIDLInfo_LocalType::~MpafPreservationMetadataType_DIDLInfo_LocalType()
{
	Cleanup();
}

void MpafPreservationMetadataType_DIDLInfo_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_ProcessEntities = MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr(); // Class


// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtMyPropInit.h

}

void MpafPreservationMetadataType_DIDLInfo_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_ProcessEntities);
}

void MpafPreservationMetadataType_DIDLInfo_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PreservationMetadataType_DIDLInfo_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPreservationMetadataType_DIDLInfo_LocalType
	const Dc1Ptr< MpafPreservationMetadataType_DIDLInfo_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_ProcessEntities);
		this->SetProcessEntities(Dc1Factory::CloneObject(tmp->GetProcessEntities()));
}

Dc1NodePtr MpafPreservationMetadataType_DIDLInfo_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafPreservationMetadataType_DIDLInfo_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr MpafPreservationMetadataType_DIDLInfo_LocalType::GetProcessEntities() const
{
		return m_ProcessEntities;
}

void MpafPreservationMetadataType_DIDLInfo_LocalType::SetProcessEntities(const MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafPreservationMetadataType_DIDLInfo_LocalType::SetProcessEntities().");
	}
	if (m_ProcessEntities != item)
	{
		// Dc1Factory::DeleteObject(m_ProcessEntities);
		m_ProcessEntities = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ProcessEntities) m_ProcessEntities->SetParent(m_myself.getPointer());
		if (m_ProcessEntities && m_ProcessEntities->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ProcessEntities->UseTypeAttribute = true;
		}
		if(m_ProcessEntities != MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr())
		{
			m_ProcessEntities->SetContentName(XMLString::transcode("ProcessEntities"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafPreservationMetadataType_DIDLInfo_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ProcessEntities")) == 0)
	{
		// ProcessEntities is simple element PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetProcessEntities()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0")))) != empty)
			{
				// Is type allowed
				MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetProcessEntities(p, client);
					if((p = GetProcessEntities()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PreservationMetadataType_DIDLInfo_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PreservationMetadataType_DIDLInfo_LocalType");
	}
	return result;
}

XMLCh * MpafPreservationMetadataType_DIDLInfo_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafPreservationMetadataType_DIDLInfo_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafPreservationMetadataType_DIDLInfo_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("PreservationMetadataType_DIDLInfo_LocalType"));
	// Element serialization:
	// Class
	
	if (m_ProcessEntities != MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ProcessEntities->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("ProcessEntities"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ProcessEntities->Serialize(doc, element, &elem);
	}

}

bool MpafPreservationMetadataType_DIDLInfo_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ProcessEntities"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ProcessEntities")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetProcessEntities(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafPreservationMetadataType_DIDLInfo_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("ProcessEntities")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0; // FTT, check this
	}
	this->SetProcessEntities(child);
  }
  return child;
 
}

Dc1NodeEnum MpafPreservationMetadataType_DIDLInfo_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetProcessEntities() != Dc1NodePtr())
		result.Insert(GetProcessEntities());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafPreservationMetadataType_DIDLInfo_LocalType_ExtMethodImpl.h


