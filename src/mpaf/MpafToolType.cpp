
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafToolType_ExtImplInclude.h


#include "MpafOperatorType.h"
#include "MpafToolType_Name_CollectionType.h"
#include "MpafToolType_Manufacturer_CollectionType.h"
#include "MpafToolType_Parameters_LocalType.h"
#include "MpafOperatorType_Descriptor_CollectionType.h"
#include "MpafToolType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Name
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Manufacturer

#include <assert.h>
IMpafToolType::IMpafToolType()
{

// no includefile for extension defined 
// file MpafToolType_ExtPropInit.h

}

IMpafToolType::~IMpafToolType()
{
// no includefile for extension defined 
// file MpafToolType_ExtPropCleanup.h

}

MpafToolType::MpafToolType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafToolType::~MpafToolType()
{
	Cleanup();
}

void MpafToolType::Init()
{
	// Init base
	m_Base = CreateOperatorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_information = NULL; // String
	m_information_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Name = MpafToolType_Name_CollectionPtr(); // Collection
	m_Manufacturer = MpafToolType_Manufacturer_CollectionPtr(); // Collection
	m_Version = NULL; // Optional String
	m_Version_Exist = false;
	m_Parameters = MpafToolType_Parameters_LocalPtr(); // Class
	m_Parameters_Exist = false;


// no includefile for extension defined 
// file MpafToolType_ExtMyPropInit.h

}

void MpafToolType::Cleanup()
{
// no includefile for extension defined 
// file MpafToolType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_information); // String
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Manufacturer);
	XMLString::release(&this->m_Version);
	this->m_Version = NULL;
	// Dc1Factory::DeleteObject(m_Parameters);
}

void MpafToolType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ToolTypePtr, since we
	// might need GetBase(), which isn't defined in IToolType
	const Dc1Ptr< MpafToolType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafOperatorPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafToolType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_information); // String
	if (tmp->Existinformation())
	{
		this->Setinformation(XMLString::replicate(tmp->Getinformation()));
	}
	else
	{
		Invalidateinformation();
	}
	}
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
		// Dc1Factory::DeleteObject(m_Manufacturer);
		this->SetManufacturer(Dc1Factory::CloneObject(tmp->GetManufacturer()));
	if (tmp->IsValidVersion())
	{
		this->SetVersion(XMLString::replicate(tmp->GetVersion()));
	}
	else
	{
		InvalidateVersion();
	}
	if (tmp->IsValidParameters())
	{
		// Dc1Factory::DeleteObject(m_Parameters);
		this->SetParameters(Dc1Factory::CloneObject(tmp->GetParameters()));
	}
	else
	{
		InvalidateParameters();
	}
}

Dc1NodePtr MpafToolType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr MpafToolType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< MpafOperatorType > MpafToolType::GetBase() const
{
	return m_Base;
}

XMLCh * MpafToolType::Getinformation() const
{
	return m_information;
}

bool MpafToolType::Existinformation() const
{
	return m_information_Exist;
}
void MpafToolType::Setinformation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::Setinformation().");
	}
	m_information = item;
	m_information_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::Invalidateinformation()
{
	m_information_Exist = false;
}
MpafToolType_Name_CollectionPtr MpafToolType::GetName() const
{
		return m_Name;
}

MpafToolType_Manufacturer_CollectionPtr MpafToolType::GetManufacturer() const
{
		return m_Manufacturer;
}

XMLCh * MpafToolType::GetVersion() const
{
		return m_Version;
}

// Element is optional
bool MpafToolType::IsValidVersion() const
{
	return m_Version_Exist;
}

MpafToolType_Parameters_LocalPtr MpafToolType::GetParameters() const
{
		return m_Parameters;
}

// Element is optional
bool MpafToolType::IsValidParameters() const
{
	return m_Parameters_Exist;
}

void MpafToolType::SetName(const MpafToolType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != MpafToolType_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::SetManufacturer(const MpafToolType_Manufacturer_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::SetManufacturer().");
	}
	if (m_Manufacturer != item)
	{
		// Dc1Factory::DeleteObject(m_Manufacturer);
		m_Manufacturer = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Manufacturer) m_Manufacturer->SetParent(m_myself.getPointer());
		if(m_Manufacturer != MpafToolType_Manufacturer_CollectionPtr())
		{
			m_Manufacturer->SetContentName(XMLString::transcode("Manufacturer"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::SetVersion(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::SetVersion().");
	}
	if (m_Version != item || m_Version_Exist == false)
	{
		XMLString::release(&m_Version);
		m_Version = item;
		m_Version_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::InvalidateVersion()
{
	m_Version_Exist = false;
}
void MpafToolType::SetParameters(const MpafToolType_Parameters_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::SetParameters().");
	}
	if (m_Parameters != item || m_Parameters_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Parameters);
		m_Parameters = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Parameters) m_Parameters->SetParent(m_myself.getPointer());
		if (m_Parameters && m_Parameters->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Parameters->UseTypeAttribute = true;
		}
		m_Parameters_Exist = true;
		if(m_Parameters != MpafToolType_Parameters_LocalPtr())
		{
			m_Parameters->SetContentName(XMLString::transcode("Parameters"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::InvalidateParameters()
{
	m_Parameters_Exist = false;
}
XMLCh * MpafToolType::Gettype() const
{
	return GetBase()->Gettype();
}

void MpafToolType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

MpafOperatorType_Descriptor_CollectionPtr MpafToolType::GetDescriptor() const
{
	return GetBase()->GetDescriptor();
}

void MpafToolType::SetDescriptor(const MpafOperatorType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::SetDescriptor().");
	}
	GetBase()->SetDescriptor(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafToolType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool MpafToolType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void MpafToolType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafToolType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
XMLCh * MpafToolType::Geturi() const
{
	return GetBase()->GetBase()->Geturi();
}

void MpafToolType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafToolType::Seturi().");
	}
	GetBase()->GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafToolType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("information")) == 0)
	{
		// information is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Name is item of type TextualType
		// in element collection ToolType_Name_CollectionType
		
		context->Found = true;
		MpafToolType_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateToolType_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Manufacturer")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Manufacturer is item of type ControlledTermUseType
		// in element collection ToolType_Manufacturer_CollectionType
		
		context->Found = true;
		MpafToolType_Manufacturer_CollectionPtr coll = GetManufacturer();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateToolType_Manufacturer_CollectionType; // FTT, check this
				SetManufacturer(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Parameters")) == 0)
	{
		// Parameters is simple element ToolType_Parameters_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetParameters()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_LocalType")))) != empty)
			{
				// Is type allowed
				MpafToolType_Parameters_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetParameters(p, client);
					if((p = GetParameters()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ToolType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ToolType");
	}
	return result;
}

XMLCh * MpafToolType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafToolType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafToolType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafToolType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("ToolType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_information_Exist)
	{
	// String
	if(m_information != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("information"), m_information);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Name != MpafToolType_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Manufacturer != MpafToolType_Manufacturer_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Manufacturer->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	 // String
	if(m_Version != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Version, X("urn:mpeg:maf:schema:preservation:2015"), X("Version"), true);
	// Class
	
	if (m_Parameters != MpafToolType_Parameters_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Parameters->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Parameters"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Parameters->Serialize(doc, element, &elem);
	}

}

bool MpafToolType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("information")))
	{
		// Deserialize string type
		this->Setinformation(Dc1Convert::TextToString(parent->getAttribute(X("information"))));
		* current = parent;
	}

	// Extensionbase is MpafOperatorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			MpafToolType_Name_CollectionPtr tmp = CreateToolType_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Manufacturer"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Manufacturer")) == 0))
		{
			// Deserialize factory type
			MpafToolType_Manufacturer_CollectionPtr tmp = CreateToolType_Manufacturer_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetManufacturer(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Version"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->SetVersion(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetVersion(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Parameters"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Parameters")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateToolType_Parameters_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParameters(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafToolType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafToolType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	MpafToolType_Name_CollectionPtr tmp = CreateToolType_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Manufacturer")) == 0))
  {
	MpafToolType_Manufacturer_CollectionPtr tmp = CreateToolType_Manufacturer_CollectionType; // FTT, check this
	this->SetManufacturer(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Parameters")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateToolType_Parameters_LocalType; // FTT, check this
	}
	this->SetParameters(child);
  }
  return child;
 
}

Dc1NodeEnum MpafToolType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetManufacturer() != Dc1NodePtr())
		result.Insert(GetManufacturer());
	if (GetParameters() != Dc1NodePtr())
		result.Insert(GetParameters());
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafToolType_ExtMethodImpl.h


