
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3CdateTime.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType()
{

// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtPropInit.h

}

IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::~IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtPropCleanup.h

}

MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::~MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType()
{
	Cleanup();
}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Init()
{
	// Init base
	m_Base = XMLString::transcode("");

	// Init attributes
	m_accessibleUntil = W3CdateTimePtr(); // Pattern
	m_accessibleUntil_Exist = false;
	m_description = NULL; // String
	m_description_Exist = false;



// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMyPropInit.h

}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_Base);
	// Dc1Factory::DeleteObject(m_accessibleUntil); // Pattern
	XMLString::release(&m_description); // String
}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IntegrityStatementType_CheckList_DeprecatedEntity_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IIntegrityStatementType_CheckList_DeprecatedEntity_LocalType
	const Dc1Ptr< MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	XMLString::release(&m_Base);
	m_Base = XMLString::replicate(tmp->GetContent());
	{
	// Dc1Factory::DeleteObject(m_accessibleUntil); // Pattern
	if (tmp->ExistaccessibleUntil())
	{
		this->SetaccessibleUntil(Dc1Factory::CloneObject(tmp->GetaccessibleUntil()));
	}
	else
	{
		InvalidateaccessibleUntil();
	}
	}
	{
	XMLString::release(&m_description); // String
	if (tmp->Existdescription())
	{
		this->Setdescription(XMLString::replicate(tmp->Getdescription()));
	}
	else
	{
		Invalidatedescription();
	}
	}
}

Dc1NodePtr MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		XMLString::release(&m_Base);
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetContent() const
{
	return m_Base;
}
W3CdateTimePtr MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetaccessibleUntil() const
{
	return m_accessibleUntil;
}

bool MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::ExistaccessibleUntil() const
{
	return m_accessibleUntil_Exist;
}
void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::SetaccessibleUntil(const W3CdateTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::SetaccessibleUntil().");
	}
	m_accessibleUntil = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_accessibleUntil) m_accessibleUntil->SetParent(m_myself.getPointer());
	m_accessibleUntil_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::InvalidateaccessibleUntil()
{
	m_accessibleUntil_Exist = false;
}
XMLCh * MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Getdescription() const
{
	return m_description;
}

bool MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Existdescription() const
{
	return m_description_Exist;
}
void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Setdescription(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Setdescription().");
	}
	m_description = item;
	m_description_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Invalidatedescription()
{
	m_description_Exist = false;
}

Dc1NodeEnum MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("accessibleUntil")) == 0)
	{
		// accessibleUntil is simple attribute dateTime
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdateTimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("description")) == 0)
	{
		// description is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IntegrityStatementType_CheckList_DeprecatedEntity_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IntegrityStatementType_CheckList_DeprecatedEntity_LocalType");
	}
	return result;
}

XMLCh * MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::ContentToString() const
{
	// MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	element->appendChild(doc->createTextNode(m_Base));

	assert(element);
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("IntegrityStatementType_CheckList_DeprecatedEntity_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_accessibleUntil_Exist)
	{
	// Pattern
	if (m_accessibleUntil != W3CdateTimePtr())
	{
		XMLCh * tmp = m_accessibleUntil->ToText();
		element->setAttributeNS(X(""), X("accessibleUntil"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_description_Exist)
	{
	// String
	if(m_description != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("description"), m_description);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("accessibleUntil")))
	{
		W3CdateTimePtr tmp = CreatedateTime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("accessibleUntil")));
		this->SetaccessibleUntil(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("description")))
	{
		// Deserialize string type
		this->Setdescription(Dc1Convert::TextToString(parent->getAttribute(X("description"))));
		* current = parent;
	}

	XMLCh * tmp = Dc1Convert::TextToString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		XMLString::release(&m_Base);
		m_Base = tmp;
		found = true;
	}
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMethodImpl.h


