
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtImplInclude.h


#include "MpafDescriptorBaseType.h"
#include "MpafQualityStatementType.h"
#include "MpafDescriptorBaseType_Condition_CollectionType.h"
#include "MpafDescriptorBaseType_Descriptor_CollectionType.h"
#include "MpafQualityDescriptorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21ConditionType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Condition
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor

#include <assert.h>
IMpafQualityDescriptorType::IMpafQualityDescriptorType()
{

// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtPropInit.h

}

IMpafQualityDescriptorType::~IMpafQualityDescriptorType()
{
// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtPropCleanup.h

}

MpafQualityDescriptorType::MpafQualityDescriptorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafQualityDescriptorType::~MpafQualityDescriptorType()
{
	Cleanup();
}

void MpafQualityDescriptorType::Init()
{
	// Init base
	m_Base = CreateDescriptorBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Statement = MpafQualityStatementPtr(); // Class


// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtMyPropInit.h

}

void MpafQualityDescriptorType::Cleanup()
{
// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Statement);
}

void MpafQualityDescriptorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QualityDescriptorTypePtr, since we
	// might need GetBase(), which isn't defined in IQualityDescriptorType
	const Dc1Ptr< MpafQualityDescriptorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafDescriptorBasePtr(Dc1Factory::CloneObject((dynamic_cast< const MpafQualityDescriptorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Statement);
		this->SetStatement(Dc1Factory::CloneObject(tmp->GetStatement()));
}

Dc1NodePtr MpafQualityDescriptorType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafQualityDescriptorType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafDescriptorBaseType > MpafQualityDescriptorType::GetBase() const
{
	return m_Base;
}

MpafQualityStatementPtr MpafQualityDescriptorType::GetStatement() const
{
		return m_Statement;
}

void MpafQualityDescriptorType::SetStatement(const MpafQualityStatementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityDescriptorType::SetStatement().");
	}
	if (m_Statement != item)
	{
		// Dc1Factory::DeleteObject(m_Statement);
		m_Statement = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Statement) m_Statement->SetParent(m_myself.getPointer());
		if (m_Statement && m_Statement->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityStatementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Statement->UseTypeAttribute = true;
		}
		if(m_Statement != MpafQualityStatementPtr())
		{
			m_Statement->SetContentName(XMLString::transcode("Statement"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafQualityDescriptorType::Getid() const
{
	return GetBase()->Getid();
}

bool MpafQualityDescriptorType::Existid() const
{
	return GetBase()->Existid();
}
void MpafQualityDescriptorType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityDescriptorType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityDescriptorType::Invalidateid()
{
	GetBase()->Invalidateid();
}
XMLCh * MpafQualityDescriptorType::Geturi() const
{
	return GetBase()->Geturi();
}

bool MpafQualityDescriptorType::Existuri() const
{
	return GetBase()->Existuri();
}
void MpafQualityDescriptorType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityDescriptorType::Seturi().");
	}
	GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityDescriptorType::Invalidateuri()
{
	GetBase()->Invalidateuri();
}
MpafDescriptorBaseType_Condition_CollectionPtr MpafQualityDescriptorType::GetCondition() const
{
	return GetBase()->GetCondition();
}

MpafDescriptorBaseType_Descriptor_CollectionPtr MpafQualityDescriptorType::GetDescriptor() const
{
	return GetBase()->GetDescriptor();
}

void MpafQualityDescriptorType::SetCondition(const MpafDescriptorBaseType_Condition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityDescriptorType::SetCondition().");
	}
	GetBase()->SetCondition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafQualityDescriptorType::SetDescriptor(const MpafDescriptorBaseType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityDescriptorType::SetDescriptor().");
	}
	GetBase()->SetDescriptor(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafQualityDescriptorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Statement")) == 0)
	{
		// Statement is simple element QualityStatementType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStatement()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityStatementType")))) != empty)
			{
				// Is type allowed
				MpafQualityStatementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStatement(p, client);
					if((p = GetStatement()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for QualityDescriptorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "QualityDescriptorType");
	}
	return result;
}

XMLCh * MpafQualityDescriptorType::ToText() const
{
	return m_Base->ToText();
}


bool MpafQualityDescriptorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafQualityDescriptorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("QualityDescriptorType"));
	// Element serialization:
	// Class
	
	if (m_Statement != MpafQualityStatementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Statement->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Statement"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Statement->Serialize(doc, element, &elem);
	}

}

bool MpafQualityDescriptorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafDescriptorBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Statement"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Statement")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateQualityStatementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStatement(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafQualityDescriptorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Statement")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateQualityStatementType; // FTT, check this
	}
	this->SetStatement(child);
  }
  return child;
 
}

Dc1NodeEnum MpafQualityDescriptorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStatement() != Dc1NodePtr())
		result.Insert(GetStatement());
	if (GetCondition() != Dc1NodePtr())
		result.Insert(GetCondition());
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafQualityDescriptorType_ExtMethodImpl.h


