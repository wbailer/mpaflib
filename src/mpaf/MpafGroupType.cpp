
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafGroupType_ExtImplInclude.h


#include "Mpeg21ContainerType.h"
#include "MpafGroupType_Descriptor_CollectionType.h"
#include "MpafGroupType_CollectionType.h"
#include "MpafGroupType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mpeg21DescriptorType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Descriptor
#include "MpafGroupType_LocalType.h" // Choice collection Container
#include "MpafGroupType.h" // Choice collection element Container
#include "MpafPreservationObjectType.h" // Choice collection element Item

#include <assert.h>
IMpafGroupType::IMpafGroupType()
{

// no includefile for extension defined 
// file MpafGroupType_ExtPropInit.h

}

IMpafGroupType::~IMpafGroupType()
{
// no includefile for extension defined 
// file MpafGroupType_ExtPropCleanup.h

}

MpafGroupType::MpafGroupType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafGroupType::~MpafGroupType()
{
	Cleanup();
}

void MpafGroupType::Init()
{
	// Init base
	m_Base = CreateContainerType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_uri = NULL; // String
	m_uri_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Descriptor = MpafGroupType_Descriptor_CollectionPtr(); // Collection
	m_GroupType_LocalType = MpafGroupType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafGroupType_ExtMyPropInit.h

}

void MpafGroupType::Cleanup()
{
// no includefile for extension defined 
// file MpafGroupType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_uri); // String
	// Dc1Factory::DeleteObject(m_Descriptor);
	// Dc1Factory::DeleteObject(m_GroupType_LocalType);
}

void MpafGroupType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GroupTypePtr, since we
	// might need GetBase(), which isn't defined in IGroupType
	const Dc1Ptr< MpafGroupType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mpeg21ContainerPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafGroupType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_uri); // String
	if (tmp->Existuri())
	{
		this->Seturi(XMLString::replicate(tmp->Geturi()));
	}
	else
	{
		Invalidateuri();
	}
	}
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
		// Dc1Factory::DeleteObject(m_GroupType_LocalType);
		this->SetGroupType_LocalType(Dc1Factory::CloneObject(tmp->GetGroupType_LocalType()));
}

Dc1NodePtr MpafGroupType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr MpafGroupType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mpeg21ContainerType > MpafGroupType::GetBase() const
{
	return m_Base;
}

XMLCh * MpafGroupType::Geturi() const
{
	return m_uri;
}

bool MpafGroupType::Existuri() const
{
	return m_uri_Exist;
}
void MpafGroupType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafGroupType::Seturi().");
	}
	m_uri = item;
	m_uri_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafGroupType::Invalidateuri()
{
	m_uri_Exist = false;
}
MpafGroupType_Descriptor_CollectionPtr MpafGroupType::GetDescriptor() const
{
		return m_Descriptor;
}

MpafGroupType_CollectionPtr MpafGroupType::GetGroupType_LocalType() const
{
		return m_GroupType_LocalType;
}

void MpafGroupType::SetDescriptor(const MpafGroupType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafGroupType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != MpafGroupType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafGroupType::SetGroupType_LocalType(const MpafGroupType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafGroupType::SetGroupType_LocalType().");
	}
	if (m_GroupType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_GroupType_LocalType);
		m_GroupType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GroupType_LocalType) m_GroupType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafGroupType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("uri")) == 0)
	{
		// uri is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Descriptor is item of abstract type DescriptorType
		// in element collection GroupType_Descriptor_CollectionType
		
		context->Found = true;
		MpafGroupType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateGroupType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mpeg21DescriptorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Container")) == 0)
	{
		// Container is contained in itemtype GroupType_LocalType
		// in choice collection GroupType_CollectionType

		context->Found = true;
		MpafGroupType_CollectionPtr coll = GetGroupType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateGroupType_CollectionType; // FTT, check this
				SetGroupType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((MpafGroupType_LocalPtr)coll->elementAt(i))->GetContainer()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				MpafGroupType_LocalPtr item = CreateGroupType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType")))) != empty)
			{
				// Is type allowed
				MpafGroupPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((MpafGroupType_LocalPtr)coll->elementAt(i))->SetContainer(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((MpafGroupType_LocalPtr)coll->elementAt(i))->GetContainer()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Item")) == 0)
	{
		// Item is contained in itemtype GroupType_LocalType
		// in choice collection GroupType_CollectionType

		context->Found = true;
		MpafGroupType_CollectionPtr coll = GetGroupType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateGroupType_CollectionType; // FTT, check this
				SetGroupType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((MpafGroupType_LocalPtr)coll->elementAt(i))->GetItem()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				MpafGroupType_LocalPtr item = CreateGroupType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType")))) != empty)
			{
				// Is type allowed
				MpafPreservationObjectPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((MpafGroupType_LocalPtr)coll->elementAt(i))->SetItem(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((MpafGroupType_LocalPtr)coll->elementAt(i))->GetItem()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GroupType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GroupType");
	}
	return result;
}

XMLCh * MpafGroupType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafGroupType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafGroupType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafGroupType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("GroupType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_uri_Exist)
	{
	// String
	if(m_uri != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("uri"), m_uri);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Descriptor != MpafGroupType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_GroupType_LocalType != MpafGroupType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_GroupType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafGroupType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("uri")))
	{
		// Deserialize string type
		this->Seturi(Dc1Convert::TextToString(parent->getAttribute(X("uri"))));
		* current = parent;
	}

	// Extensionbase is Mpeg21ContainerType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			MpafGroupType_Descriptor_CollectionPtr tmp = CreateGroupType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:maf:schema:preservation:2015:Container
			Dc1Util::HasNodeName(parent, X("Container"), X("urn:mpeg:maf:schema:preservation:2015"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:Container")) == 0)
				||
			// urn:mpeg:maf:schema:preservation:2015:Item
			Dc1Util::HasNodeName(parent, X("Item"), X("urn:mpeg:maf:schema:preservation:2015"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:Item")) == 0)
				))
		{
			// Deserialize factory type
			MpafGroupType_CollectionPtr tmp = CreateGroupType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetGroupType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafGroupType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafGroupType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	MpafGroupType_Descriptor_CollectionPtr tmp = CreateGroupType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Container"), X("urn:mpeg:maf:schema:preservation:2015")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Container")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Item"), X("urn:mpeg:maf:schema:preservation:2015")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Item")) == 0)
	)
  {
	MpafGroupType_CollectionPtr tmp = CreateGroupType_CollectionType; // FTT, check this
	this->SetGroupType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafGroupType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	if (GetGroupType_LocalType() != Dc1NodePtr())
		result.Insert(GetGroupType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafGroupType_ExtMethodImpl.h


