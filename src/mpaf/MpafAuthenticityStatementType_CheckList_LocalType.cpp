
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_CollectionType.h"
#include "MpafAuthenticityStatementType_CheckList_Operator_CollectionType.h"
#include "W3CdateTime.h"
#include "MpafAuthenticityStatementType_CheckList_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "MpafAuthenticityStatementType_CheckList_Entity_LocalType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Entity
#include "MpafOperatorRefType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Operator

#include <assert.h>
IMpafAuthenticityStatementType_CheckList_LocalType::IMpafAuthenticityStatementType_CheckList_LocalType()
{

// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtPropInit.h

}

IMpafAuthenticityStatementType_CheckList_LocalType::~IMpafAuthenticityStatementType_CheckList_LocalType()
{
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtPropCleanup.h

}

MpafAuthenticityStatementType_CheckList_LocalType::MpafAuthenticityStatementType_CheckList_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafAuthenticityStatementType_CheckList_LocalType::~MpafAuthenticityStatementType_CheckList_LocalType()
{
	Cleanup();
}

void MpafAuthenticityStatementType_CheckList_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Entity = MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr(); // Collection
	m_Operator = MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr(); // Collection
	m_Timestamp = W3CdateTimePtr(); // Pattern


// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtMyPropInit.h

}

void MpafAuthenticityStatementType_CheckList_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Entity);
	// Dc1Factory::DeleteObject(m_Operator);
	// Dc1Factory::DeleteObject(m_Timestamp);
}

void MpafAuthenticityStatementType_CheckList_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AuthenticityStatementType_CheckList_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAuthenticityStatementType_CheckList_LocalType
	const Dc1Ptr< MpafAuthenticityStatementType_CheckList_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Entity);
		this->SetEntity(Dc1Factory::CloneObject(tmp->GetEntity()));
		// Dc1Factory::DeleteObject(m_Operator);
		this->SetOperator(Dc1Factory::CloneObject(tmp->GetOperator()));
		// Dc1Factory::DeleteObject(m_Timestamp);
		this->SetTimestamp(Dc1Factory::CloneObject(tmp->GetTimestamp()));
}

Dc1NodePtr MpafAuthenticityStatementType_CheckList_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafAuthenticityStatementType_CheckList_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr MpafAuthenticityStatementType_CheckList_LocalType::GetEntity() const
{
		return m_Entity;
}

MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr MpafAuthenticityStatementType_CheckList_LocalType::GetOperator() const
{
		return m_Operator;
}

W3CdateTimePtr MpafAuthenticityStatementType_CheckList_LocalType::GetTimestamp() const
{
		return m_Timestamp;
}

void MpafAuthenticityStatementType_CheckList_LocalType::SetEntity(const MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_LocalType::SetEntity().");
	}
	if (m_Entity != item)
	{
		// Dc1Factory::DeleteObject(m_Entity);
		m_Entity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Entity) m_Entity->SetParent(m_myself.getPointer());
		if(m_Entity != MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr())
		{
			m_Entity->SetContentName(XMLString::transcode("Entity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafAuthenticityStatementType_CheckList_LocalType::SetOperator(const MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_LocalType::SetOperator().");
	}
	if (m_Operator != item)
	{
		// Dc1Factory::DeleteObject(m_Operator);
		m_Operator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Operator) m_Operator->SetParent(m_myself.getPointer());
		if(m_Operator != MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr())
		{
			m_Operator->SetContentName(XMLString::transcode("Operator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafAuthenticityStatementType_CheckList_LocalType::SetTimestamp(const W3CdateTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafAuthenticityStatementType_CheckList_LocalType::SetTimestamp().");
	}
	if (m_Timestamp != item)
	{
		// Dc1Factory::DeleteObject(m_Timestamp);
		m_Timestamp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Timestamp) m_Timestamp->SetParent(m_myself.getPointer());
		if (m_Timestamp && m_Timestamp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:dateTime"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Timestamp->UseTypeAttribute = true;
		}
		if(m_Timestamp != W3CdateTimePtr())
		{
			m_Timestamp->SetContentName(XMLString::transcode("Timestamp"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafAuthenticityStatementType_CheckList_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Entity")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Entity is item of type AuthenticityStatementType_CheckList_Entity_LocalType
		// in element collection AuthenticityStatementType_CheckList_Entity_CollectionType
		
		context->Found = true;
		MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr coll = GetEntity();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAuthenticityStatementType_CheckList_Entity_CollectionType; // FTT, check this
				SetEntity(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_LocalType")))) != empty)
			{
				// Is type allowed
				MpafAuthenticityStatementType_CheckList_Entity_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Operator")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Operator is item of type OperatorRefType
		// in element collection AuthenticityStatementType_CheckList_Operator_CollectionType
		
		context->Found = true;
		MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr coll = GetOperator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAuthenticityStatementType_CheckList_Operator_CollectionType; // FTT, check this
				SetOperator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OperatorRefType")))) != empty)
			{
				// Is type allowed
				MpafOperatorRefPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Timestamp")) == 0)
	{
		// Timestamp is simple element dateTime
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTimestamp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:dateTime")))) != empty)
			{
				// Is type allowed
				W3CdateTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTimestamp(p, client);
					if((p = GetTimestamp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AuthenticityStatementType_CheckList_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AuthenticityStatementType_CheckList_LocalType");
	}
	return result;
}

XMLCh * MpafAuthenticityStatementType_CheckList_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafAuthenticityStatementType_CheckList_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafAuthenticityStatementType_CheckList_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("AuthenticityStatementType_CheckList_LocalType"));
	// Element serialization:
	if (m_Entity != MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Entity->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Operator != MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Operator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_Timestamp != W3CdateTimePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("Timestamp");
//		m_Timestamp->SetContentName(contentname);
		m_Timestamp->UseTypeAttribute = this->UseTypeAttribute;
		m_Timestamp->Serialize(doc, element);
	}

}

bool MpafAuthenticityStatementType_CheckList_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Entity"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Entity")) == 0))
		{
			// Deserialize factory type
			MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr tmp = CreateAuthenticityStatementType_CheckList_Entity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEntity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Operator"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Operator")) == 0))
		{
			// Deserialize factory type
			MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr tmp = CreateAuthenticityStatementType_CheckList_Operator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOperator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("Timestamp"), X("urn:mpeg:maf:schema:preservation:2015")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Timestamp")) == 0))
		{
			W3CdateTimePtr tmp = CreatedateTime; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTimestamp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafAuthenticityStatementType_CheckList_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Entity")) == 0))
  {
	MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr tmp = CreateAuthenticityStatementType_CheckList_Entity_CollectionType; // FTT, check this
	this->SetEntity(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Operator")) == 0))
  {
	MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr tmp = CreateAuthenticityStatementType_CheckList_Operator_CollectionType; // FTT, check this
	this->SetOperator(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Timestamp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedateTime; // FTT, check this
	}
	this->SetTimestamp(child);
  }
  return child;
 
}

Dc1NodeEnum MpafAuthenticityStatementType_CheckList_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEntity() != Dc1NodePtr())
		result.Insert(GetEntity());
	if (GetOperator() != Dc1NodePtr())
		result.Insert(GetOperator());
	if (GetTimestamp() != Dc1NodePtr())
		result.Insert(GetTimestamp());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafAuthenticityStatementType_CheckList_LocalType_ExtMethodImpl.h


