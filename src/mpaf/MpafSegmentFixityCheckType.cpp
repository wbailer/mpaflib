
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtImplInclude.h


#include "MpafFixityCheckType.h"
#include "MpafSegmentFixityCheckType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafSegmentFixityCheckType::IMpafSegmentFixityCheckType()
{

// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtPropInit.h

}

IMpafSegmentFixityCheckType::~IMpafSegmentFixityCheckType()
{
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtPropCleanup.h

}

MpafSegmentFixityCheckType::MpafSegmentFixityCheckType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafSegmentFixityCheckType::~MpafSegmentFixityCheckType()
{
	Cleanup();
}

void MpafSegmentFixityCheckType::Init()
{
	// Init base
	m_Base = CreateFixityCheckType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_byteOffset = 0; // Value
	m_byteOffset_Exist = false;
	m_byteNumber = 0; // Value
	m_byteNumber_Exist = false;
	m_start = 0.0; // Value
	m_start_Exist = false;
	m_span = 0.0; // Value
	m_span_Exist = false;
	m_unit = NULL; // String
	m_unit_Exist = false;
	m_chunkUnit = NULL; // String
	m_chunkUnit_Exist = false;
	m_stream = NULL; // String
	m_stream_Exist = false;



// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMyPropInit.h

}

void MpafSegmentFixityCheckType::Cleanup()
{
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_unit); // String
	XMLString::release(&m_chunkUnit); // String
	XMLString::release(&m_stream); // String
}

void MpafSegmentFixityCheckType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SegmentFixityCheckTypePtr, since we
	// might need GetBase(), which isn't defined in ISegmentFixityCheckType
	const Dc1Ptr< MpafSegmentFixityCheckType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafFixityCheckPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafSegmentFixityCheckType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistbyteOffset())
	{
		this->SetbyteOffset(tmp->GetbyteOffset());
	}
	else
	{
		InvalidatebyteOffset();
	}
	}
	{
	if (tmp->ExistbyteNumber())
	{
		this->SetbyteNumber(tmp->GetbyteNumber());
	}
	else
	{
		InvalidatebyteNumber();
	}
	}
	{
	if (tmp->Existstart())
	{
		this->Setstart(tmp->Getstart());
	}
	else
	{
		Invalidatestart();
	}
	}
	{
	if (tmp->Existspan())
	{
		this->Setspan(tmp->Getspan());
	}
	else
	{
		Invalidatespan();
	}
	}
	{
	XMLString::release(&m_unit); // String
	if (tmp->Existunit())
	{
		this->Setunit(XMLString::replicate(tmp->Getunit()));
	}
	else
	{
		Invalidateunit();
	}
	}
	{
	XMLString::release(&m_chunkUnit); // String
	if (tmp->ExistchunkUnit())
	{
		this->SetchunkUnit(XMLString::replicate(tmp->GetchunkUnit()));
	}
	else
	{
		InvalidatechunkUnit();
	}
	}
	{
	XMLString::release(&m_stream); // String
	if (tmp->Existstream())
	{
		this->Setstream(XMLString::replicate(tmp->Getstream()));
	}
	else
	{
		Invalidatestream();
	}
	}
}

Dc1NodePtr MpafSegmentFixityCheckType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr MpafSegmentFixityCheckType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< MpafFixityCheckType > MpafSegmentFixityCheckType::GetBase() const
{
	return m_Base;
}

void MpafSegmentFixityCheckType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafSegmentFixityCheckType::GetContent() const
{
	return GetBase()->GetContent();
}
int MpafSegmentFixityCheckType::GetbyteOffset() const
{
	return m_byteOffset;
}

bool MpafSegmentFixityCheckType::ExistbyteOffset() const
{
	return m_byteOffset_Exist;
}
void MpafSegmentFixityCheckType::SetbyteOffset(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::SetbyteOffset().");
	}
	m_byteOffset = item;
	m_byteOffset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::InvalidatebyteOffset()
{
	m_byteOffset_Exist = false;
}
int MpafSegmentFixityCheckType::GetbyteNumber() const
{
	return m_byteNumber;
}

bool MpafSegmentFixityCheckType::ExistbyteNumber() const
{
	return m_byteNumber_Exist;
}
void MpafSegmentFixityCheckType::SetbyteNumber(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::SetbyteNumber().");
	}
	m_byteNumber = item;
	m_byteNumber_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::InvalidatebyteNumber()
{
	m_byteNumber_Exist = false;
}
double MpafSegmentFixityCheckType::Getstart() const
{
	return m_start;
}

bool MpafSegmentFixityCheckType::Existstart() const
{
	return m_start_Exist;
}
void MpafSegmentFixityCheckType::Setstart(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::Setstart().");
	}
	m_start = item;
	m_start_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::Invalidatestart()
{
	m_start_Exist = false;
}
double MpafSegmentFixityCheckType::Getspan() const
{
	return m_span;
}

bool MpafSegmentFixityCheckType::Existspan() const
{
	return m_span_Exist;
}
void MpafSegmentFixityCheckType::Setspan(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::Setspan().");
	}
	m_span = item;
	m_span_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::Invalidatespan()
{
	m_span_Exist = false;
}
XMLCh * MpafSegmentFixityCheckType::Getunit() const
{
	return m_unit;
}

bool MpafSegmentFixityCheckType::Existunit() const
{
	return m_unit_Exist;
}
void MpafSegmentFixityCheckType::Setunit(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::Setunit().");
	}
	m_unit = item;
	m_unit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::Invalidateunit()
{
	m_unit_Exist = false;
}
XMLCh * MpafSegmentFixityCheckType::GetchunkUnit() const
{
	return m_chunkUnit;
}

bool MpafSegmentFixityCheckType::ExistchunkUnit() const
{
	return m_chunkUnit_Exist;
}
void MpafSegmentFixityCheckType::SetchunkUnit(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::SetchunkUnit().");
	}
	m_chunkUnit = item;
	m_chunkUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::InvalidatechunkUnit()
{
	m_chunkUnit_Exist = false;
}
XMLCh * MpafSegmentFixityCheckType::Getstream() const
{
	return m_stream;
}

bool MpafSegmentFixityCheckType::Existstream() const
{
	return m_stream_Exist;
}
void MpafSegmentFixityCheckType::Setstream(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::Setstream().");
	}
	m_stream = item;
	m_stream_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafSegmentFixityCheckType::Invalidatestream()
{
	m_stream_Exist = false;
}
XMLCh * MpafSegmentFixityCheckType::Gettype() const
{
	return GetBase()->Gettype();
}

void MpafSegmentFixityCheckType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafSegmentFixityCheckType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafSegmentFixityCheckType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("byteOffset")) == 0)
	{
		// byteOffset is simple attribute unsignedLong
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("byteNumber")) == 0)
	{
		// byteNumber is simple attribute unsignedLong
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("span")) == 0)
	{
		// span is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("unit")) == 0)
	{
		// unit is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("chunkUnit")) == 0)
	{
		// chunkUnit is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("stream")) == 0)
	{
		// stream is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SegmentFixityCheckType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SegmentFixityCheckType");
	}
	return result;
}

XMLCh * MpafSegmentFixityCheckType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafSegmentFixityCheckType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool MpafSegmentFixityCheckType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* MpafSegmentFixityCheckType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void MpafSegmentFixityCheckType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("SegmentFixityCheckType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_byteOffset_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_byteOffset);
		element->setAttributeNS(X(""), X("byteOffset"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_byteNumber_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_byteNumber);
		element->setAttributeNS(X(""), X("byteNumber"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_start_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_start);
		element->setAttributeNS(X(""), X("start"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_span_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_span);
		element->setAttributeNS(X(""), X("span"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_unit_Exist)
	{
	// String
	if(m_unit != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("unit"), m_unit);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_chunkUnit_Exist)
	{
	// String
	if(m_chunkUnit != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("chunkUnit"), m_chunkUnit);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_stream_Exist)
	{
	// String
	if(m_stream != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("stream"), m_stream);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool MpafSegmentFixityCheckType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("byteOffset")))
	{
		// deserialize value type
		this->SetbyteOffset(Dc1Convert::TextToInt(parent->getAttribute(X("byteOffset"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("byteNumber")))
	{
		// deserialize value type
		this->SetbyteNumber(Dc1Convert::TextToInt(parent->getAttribute(X("byteNumber"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("start")))
	{
		// deserialize value type
		this->Setstart(Dc1Convert::TextToDouble(parent->getAttribute(X("start"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("span")))
	{
		// deserialize value type
		this->Setspan(Dc1Convert::TextToDouble(parent->getAttribute(X("span"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("unit")))
	{
		// Deserialize string type
		this->Setunit(Dc1Convert::TextToString(parent->getAttribute(X("unit"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("chunkUnit")))
	{
		// Deserialize string type
		this->SetchunkUnit(Dc1Convert::TextToString(parent->getAttribute(X("chunkUnit"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("stream")))
	{
		// Deserialize string type
		this->Setstream(Dc1Convert::TextToString(parent->getAttribute(X("stream"))));
		* current = parent;
	}

	// Extensionbase is MpafFixityCheckType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafSegmentFixityCheckType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum MpafSegmentFixityCheckType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMethodImpl.h


