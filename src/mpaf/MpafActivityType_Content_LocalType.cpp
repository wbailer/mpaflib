
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtImplInclude.h


#include "MpafActivityType_Content_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMpafActivityType_Content_LocalType::IMpafActivityType_Content_LocalType()
{

// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtPropInit.h

}

IMpafActivityType_Content_LocalType::~IMpafActivityType_Content_LocalType()
{
// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtPropCleanup.h

}

MpafActivityType_Content_LocalType::MpafActivityType_Content_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafActivityType_Content_LocalType::~MpafActivityType_Content_LocalType()
{
	Cleanup();
}

void MpafActivityType_Content_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Added = XMLString::transcode(""); //  Mandatory String
	m_Modified = XMLString::transcode(""); //  Mandatory String
	m_Removed = XMLString::transcode(""); //  Mandatory String


// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtMyPropInit.h

}

void MpafActivityType_Content_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtMyPropCleanup.h


	XMLString::release(&this->m_Added);
	this->m_Added = NULL;
	XMLString::release(&this->m_Modified);
	this->m_Modified = NULL;
	XMLString::release(&this->m_Removed);
	this->m_Removed = NULL;
}

void MpafActivityType_Content_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ActivityType_Content_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IActivityType_Content_LocalType
	const Dc1Ptr< MpafActivityType_Content_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetAdded(XMLString::replicate(tmp->GetAdded()));
		this->SetModified(XMLString::replicate(tmp->GetModified()));
		this->SetRemoved(XMLString::replicate(tmp->GetRemoved()));
}

Dc1NodePtr MpafActivityType_Content_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafActivityType_Content_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * MpafActivityType_Content_LocalType::GetAdded() const
{
		return m_Added;
}

XMLCh * MpafActivityType_Content_LocalType::GetModified() const
{
		return m_Modified;
}

XMLCh * MpafActivityType_Content_LocalType::GetRemoved() const
{
		return m_Removed;
}

// implementing setter for choice 
/* element */
void MpafActivityType_Content_LocalType::SetAdded(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType_Content_LocalType::SetAdded().");
	}
	if (m_Added != item)
	{
		XMLString::release(&m_Added);
		m_Added = item;
	XMLString::release(&this->m_Modified);
	m_Modified = NULL; // FTT Shouldn't it be XMLString::release()?
	XMLString::release(&this->m_Removed);
	m_Removed = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void MpafActivityType_Content_LocalType::SetModified(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType_Content_LocalType::SetModified().");
	}
	if (m_Modified != item)
	{
		XMLString::release(&m_Modified);
		m_Modified = item;
	XMLString::release(&this->m_Added);
	m_Added = NULL; // FTT Shouldn't it be XMLString::release()?
	XMLString::release(&this->m_Removed);
	m_Removed = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void MpafActivityType_Content_LocalType::SetRemoved(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafActivityType_Content_LocalType::SetRemoved().");
	}
	if (m_Removed != item)
	{
		XMLString::release(&m_Removed);
		m_Removed = item;
	XMLString::release(&this->m_Added);
	m_Added = NULL; // FTT Shouldn't it be XMLString::release()?
	XMLString::release(&this->m_Modified);
	m_Modified = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: ActivityType_Content_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum MpafActivityType_Content_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * MpafActivityType_Content_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafActivityType_Content_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafActivityType_Content_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	 // String
	if(m_Added != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Added, X("urn:mpeg:maf:schema:preservation:2015"), X("Added"), false);
	 // String
	if(m_Modified != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Modified, X("urn:mpeg:maf:schema:preservation:2015"), X("Modified"), false);
	 // String
	if(m_Removed != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Removed, X("urn:mpeg:maf:schema:preservation:2015"), X("Removed"), false);

}

bool MpafActivityType_Content_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Added"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->SetAdded(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetAdded(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Modified"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->SetModified(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetModified(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Removed"), X("urn:mpeg:maf:schema:preservation:2015")))
	{
		// FTT memleaking		this->SetRemoved(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetRemoved(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	} while(false);
// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafActivityType_Content_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum MpafActivityType_Content_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafActivityType_Content_LocalType_ExtMethodImpl.h


