
// Extension library class
// NodeRefCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <stdio.h>
#include "MpafMPEG7DMStatementType_CollectionType.h"
#include "Dc1FactoryDefines.h"
 
#include "MpafFactoryDefines.h"

#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtImplInclude.h


#include "MpafMPEG7DMStatementType_LocalType.h" // Collection itemtype
// Choice item type CreationInformationType is concrete
// Choice item type Mpeg7BaseType is abstract
#include "Mp7JrsAcquaintanceType.h"
#include "Mp7JrsAdvancedFaceRecognitionType.h"
#include "Mp7JrsAffectiveType.h"
#include "Mp7JrsAffiliationType.h"
#include "Mp7JrsAgentObjectType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType.h"
#include "Mp7JrsAnalyticEditedVideoType.h"
#include "Mp7JrsAnalyticModelType_Semantics_LocalType.h"
#include "Mp7JrsAudioBPMType.h"
#include "Mp7JrsAudioChordPatternDS.h"
#include "Mp7JrsAudioFundamentalFrequencyType.h"
#include "Mp7JrsAudioHarmonicityType.h"
#include "Mp7JrsAudioPowerType.h"
#include "Mp7JrsAudioRhythmicPatternDS.h"
#include "Mp7JrsAudioSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsAudioSegmentTemporalDecompositionType.h"
#include "Mp7JrsAudioSegmentType.h"
#include "Mp7JrsAudioSignalQualityType.h"
#include "Mp7JrsAudioSignatureType.h"
#include "Mp7JrsAudioSpectrumBasisType.h"
#include "Mp7JrsAudioSpectrumCentroidType.h"
#include "Mp7JrsAudioSpectrumEnvelopeType.h"
#include "Mp7JrsAudioSpectrumFlatnessType.h"
#include "Mp7JrsAudioSpectrumProjectionType.h"
#include "Mp7JrsAudioSpectrumSpreadType.h"
#include "Mp7JrsAudioSummaryComponentType.h"
#include "Mp7JrsAudioTempoType.h"
#include "Mp7JrsAudioType.h"
#include "Mp7JrsAudioVisualRegionMediaSourceDecompositionType.h"
#include "Mp7JrsAudioVisualRegionSpatialDecompositionType.h"
#include "Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualRegionTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualRegionType.h"
#include "Mp7JrsAudioVisualSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentSpatialDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentType.h"
#include "Mp7JrsAudioVisualType.h"
#include "Mp7JrsAudioWaveformType.h"
#include "Mp7JrsAvailabilityType.h"
#include "Mp7JrsBackgroundNoiseLevelType.h"
#include "Mp7JrsBalanceType.h"
#include "Mp7JrsBandwidthType.h"
#include "Mp7JrsBinomialDistributionType.h"
#include "Mp7JrsBrowsingPreferencesType.h"
#include "Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType.h"
#include "Mp7JrsCameraMotionType.h"
#include "Mp7JrsChordBaseType.h"
#include "Mp7JrsClassificationPerceptualAttributeType.h"
#include "Mp7JrsClassificationPreferencesType.h"
#include "Mp7JrsClassificationSchemeAliasType.h"
#include "Mp7JrsClassificationSchemeType.h"
#include "Mp7JrsClassificationType.h"
#include "Mp7JrsClusterClassificationModelType.h"
#include "Mp7JrsClusterModelType.h"
#include "Mp7JrsCollectionModelType.h"
#include "Mp7JrsColorLayoutType.h"
#include "Mp7JrsColorSamplingType.h"
#include "Mp7JrsColorStructureType.h"
#include "Mp7JrsColorTemperatureType.h"
#include "Mp7JrsCompositionShotEditingTemporalDecompositionType.h"
#include "Mp7JrsCompositionShotType.h"
#include "Mp7JrsCompositionTransitionType.h"
#include "Mp7JrsConceptCollectionType.h"
#include "Mp7JrsConceptType.h"
#include "Mp7JrsConfusionCountType.h"
#include "Mp7JrsContentCollectionType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType.h"
#include "Mp7JrsContinuousUniformDistributionType.h"
#include "Mp7JrsContourShapeType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsCreationPreferencesType.h"
#include "Mp7JrsCreationPreferencesType_Location_LocalType.h"
#include "Mp7JrsCreationType.h"
#include "Mp7JrsCrossChannelCorrelationType.h"
#include "Mp7JrsDcOffsetType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsDescriptorCollectionType.h"
#include "Mp7JrsDescriptorModelType.h"
#include "Mp7JrsDiscreteHiddenMarkovModelType.h"
#include "Mp7JrsDiscreteUniformDistributionType.h"
#include "Mp7JrsDisseminationType.h"
#include "Mp7JrsDistributionPerceptualAttributeType.h"
#include "Mp7JrsDominantColorType.h"
#include "Mp7JrsDoublePullbackDefinitionType.h"
#include "Mp7JrsDoublePushoutDefinitionType.h"
#include "Mp7JrsEdgeHistogramType.h"
#include "Mp7JrsEditedMovingRegionType.h"
#include "Mp7JrsEditedVideoEditingTemporalDecompositionType.h"
#include "Mp7JrsEditedVideoType.h"
#include "Mp7JrsEnhancedAudioSignatureType.h"
#include "Mp7JrsErrorEventType.h"
#include "Mp7JrsEventType.h"
#include "Mp7JrsExponentialDistributionType.h"
#include "Mp7JrsExtendedMediaQualityType.h"
#include "Mp7JrsFaceRecognitionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType.h"
#include "Mp7JrsFrequencyTreeType.h"
#include "Mp7JrsFrequencyViewType.h"
#include "Mp7JrsGammaDistributionType.h"
#include "Mp7JrsGaussianDistributionType.h"
#include "Mp7JrsGaussianMixtureModelType.h"
#include "Mp7JrsGeneralizedGaussianDistributionType.h"
#include "Mp7JrsGeometricDistributionType.h"
#include "Mp7JrsGlobalTransitionType.h"
#include "Mp7JrsGoFGoPColorType.h"
#include "Mp7JrsGraphicalClassificationSchemeType.h"
#include "Mp7JrsGraphicalTermDefinitionType.h"
#include "Mp7JrsGraphType.h"
#include "Mp7JrsHandWritingRecogInformationType.h"
#include "Mp7JrsHandWritingRecogResultType.h"
#include "Mp7JrsHarmonicInstrumentTimbreType.h"
#include "Mp7JrsHarmonicSpectralCentroidType.h"
#include "Mp7JrsHarmonicSpectralDeviationType.h"
#include "Mp7JrsHarmonicSpectralSpreadType.h"
#include "Mp7JrsHarmonicSpectralVariationType.h"
#include "Mp7JrsHierarchicalSummaryType.h"
#include "Mp7JrsHistogramProbabilityType.h"
#include "Mp7JrsHomogeneousTextureType.h"
#include "Mp7JrsHyperGeometricDistributionType.h"
#include "Mp7JrsImageSignatureType.h"
#include "Mp7JrsImageTextType.h"
#include "Mp7JrsImageType.h"
#include "Mp7JrsInkContentType.h"
#include "Mp7JrsInkMediaInformationType.h"
#include "Mp7JrsInkSegmentSpatialDecompositionType.h"
#include "Mp7JrsInkSegmentTemporalDecompositionType.h"
#include "Mp7JrsInkSegmentType.h"
#include "Mp7JrsInstrumentTimbreType.h"
#include "Mp7JrsInternalTransitionType.h"
#include "Mp7JrsIntraCompositionShotEditingTemporalDecompositionType.h"
#include "Mp7JrsIntraCompositionShotType.h"
#include "Mp7JrsKeyType.h"
#include "Mp7JrsLinearPerceptualAttributeType.h"
#include "Mp7JrsLinguisticDocumentType.h"
#include "Mp7JrsLinguisticType.h"
#include "Mp7JrsLogAttackTimeType.h"
#include "Mp7JrsLognormalDistributionType.h"
#include "Mp7JrsMatchingHintType.h"
#include "Mp7JrsMediaFormatType.h"
#include "Mp7JrsMediaIdentificationType.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsMediaInstanceType.h"
#include "Mp7JrsMediaProfileType.h"
#include "Mp7JrsMediaQualityType.h"
#include "Mp7JrsMediaSpaceMaskType.h"
#include "Mp7JrsMediaTranscodingHintsType.h"
#include "Mp7JrsMelodyContourType.h"
#include "Mp7JrsMelodySequenceType.h"
#include "Mp7JrsMelodySequenceType_NoteArray_LocalType.h"
#include "Mp7JrsMelodyType.h"
#include "Mp7JrsMeterType.h"
#include "Mp7JrsMixedCollectionType.h"
#include "Mp7JrsModelStateType.h"
#include "Mp7JrsMorphismGraphType.h"
#include "Mp7JrsMosaicType.h"
#include "Mp7JrsMotionActivityType.h"
#include "Mp7JrsMotionTrajectoryType.h"
#include "Mp7JrsMovingRegionFeatureType.h"
#include "Mp7JrsMovingRegionMediaSourceDecompositionType.h"
#include "Mp7JrsMovingRegionSpatialDecompositionType.h"
#include "Mp7JrsMovingRegionSpatioTemporalDecompositionType.h"
#include "Mp7JrsMovingRegionTemporalDecompositionType.h"
#include "Mp7JrsMovingRegionType.h"
#include "Mp7JrsMultimediaCollectionType.h"
#include "Mp7JrsMultimediaSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsMultimediaSegmentType.h"
#include "Mp7JrsMultimediaType.h"
#include "Mp7JrsMultiResolutionPyramidType.h"
#include "Mp7JrsObjectType.h"
#include "Mp7JrsOrderedGroupDataSetMaskType.h"
#include "Mp7JrsOrderingKeyType.h"
#include "Mp7JrsOrganizationType.h"
#include "Mp7JrsPackageType.h"
#include "Mp7JrsParametricMotionType.h"
#include "Mp7JrsPerceptual3DShapeType.h"
#include "Mp7JrsPerceptualAttributeDSType.h"
#include "Mp7JrsPerceptualBeatType.h"
#include "Mp7JrsPerceptualEnergyType.h"
#include "Mp7JrsPerceptualGenreDistributionElementType.h"
#include "Mp7JrsPerceptualGenreDistributionType.h"
#include "Mp7JrsPerceptualInstrumentationDistributionElementType.h"
#include "Mp7JrsPerceptualInstrumentationDistributionType.h"
#include "Mp7JrsPerceptualLanguageType.h"
#include "Mp7JrsPerceptualLyricsDistributionElementType.h"
#include "Mp7JrsPerceptualLyricsDistributionType.h"
#include "Mp7JrsPerceptualMoodDistributionElementType.h"
#include "Mp7JrsPerceptualMoodDistributionType.h"
#include "Mp7JrsPerceptualRecordingType.h"
#include "Mp7JrsPerceptualSoundType.h"
#include "Mp7JrsPerceptualTempoType.h"
#include "Mp7JrsPerceptualValenceType.h"
#include "Mp7JrsPerceptualVocalsType.h"
#include "Mp7JrsPercussiveInstrumentTimbreType.h"
#include "Mp7JrsPersonGroupType.h"
#include "Mp7JrsPersonType.h"
#include "Mp7JrsPhoneLexiconType.h"
#include "Mp7JrsPhoneticTranscriptionLexiconType.h"
#include "Mp7JrsPitchProfileDSType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsPointOfViewType.h"
#include "Mp7JrsPoissonDistributionType.h"
#include "Mp7JrsPreferenceConditionType.h"
#include "Mp7JrsProbabilityClassificationModelType.h"
#include "Mp7JrsProbabilityDistributionType.h"
#include "Mp7JrsProbabilityModelClassType.h"
#include "Mp7JrsPullbackDefinitionType.h"
#include "Mp7JrsPushoutDefinitionType.h"
#include "Mp7JrsQCItemResultType.h"
#include "Mp7JrsQCProfileType.h"
#include "Mp7JrsReferencePitchType.h"
#include "Mp7JrsRegionShapeType.h"
#include "Mp7JrsRelatedMaterialType.h"
#include "Mp7JrsRelationType.h"
#include "Mp7JrsRelativeDelayType.h"
#include "Mp7JrsResolutionViewType.h"
#include "Mp7JrsRhythmicBaseType.h"
#include "Mp7JrsScalableColorType.h"
#include "Mp7JrsSceneGraphMaskType.h"
#include "Mp7JrsSegmentCollectionType.h"
#include "Mp7JrsSemanticPlaceType.h"
#include "Mp7JrsSemanticStateType.h"
#include "Mp7JrsSemanticTimeType.h"
#include "Mp7JrsSemanticType.h"
#include "Mp7JrsSentencesType.h"
#include "Mp7JrsSequentialSummaryType.h"
#include "Mp7JrsShape3DType.h"
#include "Mp7JrsShapeVariationType.h"
#include "Mp7JrsShotEditingTemporalDecompositionType.h"
#include "Mp7JrsShotType.h"
#include "Mp7JrsSignalType.h"
#include "Mp7JrsSilenceHeaderType.h"
#include "Mp7JrsSilenceType.h"
#include "Mp7JrsSoundClassificationModelType.h"
#include "Mp7JrsSoundModelStateHistogramType.h"
#include "Mp7JrsSoundModelStatePathType.h"
#include "Mp7JrsSoundModelType.h"
#include "Mp7JrsSourcePreferencesType.h"
#include "Mp7JrsSourcePreferencesType_DisseminationLocation_LocalType.h"
#include "Mp7JrsSourcePreferencesType_MediaFormat_LocalType.h"
#include "Mp7JrsSpaceFrequencyGraphType.h"
#include "Mp7JrsSpaceFrequencyViewType.h"
#include "Mp7JrsSpaceResolutionViewType.h"
#include "Mp7JrsSpaceTreeType.h"
#include "Mp7JrsSpaceViewType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType.h"
#include "Mp7JrsSpatialMaskType.h"
#include "Mp7JrsSpatioTemporalMaskType.h"
#include "Mp7JrsSpeakerInfoType.h"
#include "Mp7JrsSpectralCentroidType.h"
#include "Mp7JrsSpokenContentHeaderType.h"
#include "Mp7JrsSpokenContentLatticeType.h"
#include "Mp7JrsStateTransitionModelType.h"
#include "Mp7JrsStillRegion3DSpatialDecompositionType.h"
#include "Mp7JrsStillRegion3DType.h"
#include "Mp7JrsStillRegionFeatureType.h"
#include "Mp7JrsStillRegionSpatialDecompositionType.h"
#include "Mp7JrsStillRegionType.h"
#include "Mp7JrsStreamMaskType.h"
#include "Mp7JrsStructuredCollectionType.h"
#include "Mp7JrsSubjectClassificationSchemeType.h"
#include "Mp7JrsSubjectTermDefinitionType.h"
#include "Mp7JrsSubjectTermDefinitionType_Term_LocalType.h"
#include "Mp7JrsSummarizationType.h"
#include "Mp7JrsSummaryPreferencesType.h"
#include "Mp7JrsSummarySegmentGroupType.h"
#include "Mp7JrsSummarySegmentType.h"
#include "Mp7JrsSummaryThemeListType.h"
#include "Mp7JrsSyntacticConstituentType.h"
#include "Mp7JrsTemporalCentroidType.h"
#include "Mp7JrsTemporalMaskType.h"
#include "Mp7JrsTermDefinitionType.h"
#include "Mp7JrsTermDefinitionType_Term_LocalType.h"
#include "Mp7JrsTextDecompositionType.h"
#include "Mp7JrsTextSegmentType.h"
#include "Mp7JrsTextType.h"
#include "Mp7JrsTextualSummaryComponentType.h"
#include "Mp7JrsTextureBrowsingType.h"
#include "Mp7JrsUsageHistoryType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsUsageRecordType.h"
#include "Mp7JrsUserActionHistoryType.h"
#include "Mp7JrsUserActionListType.h"
#include "Mp7JrsUserActionType.h"
#include "Mp7JrsUserPreferencesType.h"
#include "Mp7JrsUserProfileType.h"
#include "Mp7JrsVariationSetType.h"
#include "Mp7JrsVariationType.h"
#include "Mp7JrsVideoSegmentFeatureType.h"
#include "Mp7JrsVideoSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsVideoSegmentSpatialDecompositionType.h"
#include "Mp7JrsVideoSegmentSpatioTemporalDecompositionType.h"
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h"
#include "Mp7JrsVideoSegmentType.h"
#include "Mp7JrsVideoSignatureType.h"
#include "Mp7JrsVideoTextType.h"
#include "Mp7JrsVideoType.h"
#include "Mp7JrsVideoViewGraphType.h"
#include "Mp7JrsViewSetType.h"
#include "Mp7JrsVisualSummaryComponentType.h"
#include "Mp7JrsWordLexiconType.h"



MpafMPEG7DMStatementType_CollectionType::MpafMPEG7DMStatementType_CollectionType()
{
	m_Item = new Dc1ValueVectorOf<Dc1NodePtr>(0); // XXX
	dontSerializeWithContentName = false; // default -- (see Customize.template::DontSetContentNameFor)

// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtPropInit.h


}

MpafMPEG7DMStatementType_CollectionType::~MpafMPEG7DMStatementType_CollectionType()
{
// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtPropCleanup.h


	delete m_Item;
}

void MpafMPEG7DMStatementType_CollectionType::DeepCopy(const Dc1NodePtr &original)
{
	const MpafMPEG7DMStatementType_CollectionPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: passed argument of wrong type

	removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		Dc1NodePtr dummy = Dc1Factory::CloneObject(tmp->elementAt(i));
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (dummy) dummy->SetParent(m_myself.getPointer());
		m_Item->addElement(dummy);
	}
}

// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtMethodImpl.h


void MpafMPEG7DMStatementType_CollectionType::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void MpafMPEG7DMStatementType_CollectionType::addElement(const Dc1NodePtr &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafMPEG7DMStatementType_CollectionType::addElement().");
	}
	m_Item->addElement(toAdd);
	if (toAdd) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toAdd->SetParent(m_myself.getPointer());
		if (!toAdd->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toAdd->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_ADDED,
		client, toAdd);
}

void MpafMPEG7DMStatementType_CollectionType::setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafMPEG7DMStatementType_CollectionType::setElementAt().");
	}
	Dc1NodePtr previous = m_Item->elementAt(setAt);
	if (previous) previous->SetParent(Dc1NodePtr());
	m_Item->setElementAt(toSet, setAt);

	if (toSet) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toSet->SetParent(m_myself.getPointer());
		if (!toSet->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toSet->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, toSet, setAt);
}

void MpafMPEG7DMStatementType_CollectionType::insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafMPEG7DMStatementType_CollectionType::insertElementAt().");
	}
	m_Item->insertElementAt(toInsert, insertAt);
	if (toInsert) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toInsert->SetParent(m_myself.getPointer());
		if (!toInsert->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toInsert->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_INSERTED,
		client, toInsert, insertAt);
}

/*
Dc1NodePtr MpafMPEG7DMStatementType_CollectionType::orphanElementAt(unsigned int orphanAt)
{
	return m_Item->orphanElementAt(orphanAt);
}
*/

void MpafMPEG7DMStatementType_CollectionType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafMPEG7DMStatementType_CollectionType::removeAllElements().");
	}

	for (unsigned int i = 0; i < m_Item->size(); i++) {
		// FIXME: handle elementAt(removeAt) == NULL
		m_Item->elementAt(i)->SetParent(Dc1NodePtr());
	}

	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafMPEG7DMStatementType_CollectionType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafMPEG7DMStatementType_CollectionType::removeElementAt().");
	}
	// FIXME: handle elementAt(removeAt) == NULL
	m_Item->elementAt(removeAt)->SetParent(Dc1NodePtr());
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool MpafMPEG7DMStatementType_CollectionType::containsElement(const Dc1NodePtr &toCheck)
{
	return m_Item->containsElement(toCheck);
}

unsigned int MpafMPEG7DMStatementType_CollectionType::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const Dc1NodePtr MpafMPEG7DMStatementType_CollectionType::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

Dc1NodePtr MpafMPEG7DMStatementType_CollectionType::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

Dc1NodeEnum MpafMPEG7DMStatementType_CollectionType::GetElementsContainingType(unsigned int id) const
{
	Dc1Enumerator< Dc1NodePtr > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		MpafMPEG7DMStatementType_LocalPtr tmp = elementAt(i);
		if (tmp != MpafMPEG7DMStatementType_LocalPtr() && tmp->GetClassId() == Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType_LocalType"))) {
			// FTT this is better!
			if (tmp->GetCreationInformationType() != Mp7JrsCreationInformationPtr()
				&& tmp->GetCreationInformationType()->GetClassId() == id)
				{
					result.Insert(tmp->GetCreationInformationType());
					continue;
				}
			if (tmp->GetDescriptionUnit() != Dc1Ptr< Dc1Node >()
				&& tmp->GetDescriptionUnit()->GetClassId() == id)
				{
					result.Insert(tmp->GetDescriptionUnit());
					continue;
				}
		}
	}
	return result;
}
Dc1NodeEnum MpafMPEG7DMStatementType_CollectionType::GetElementsOfType(unsigned int id) const
{
	Dc1Enumerator< Dc1NodePtr > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		if (elementAt(i)->GetClassId() == id)
		{
			result.Insert(elementAt(i));
		}
	}
	return result;
}

Dc1NodeEnum MpafMPEG7DMStatementType_CollectionType::GetElements() const
{
	Dc1Enumerator< Dc1Ptr<Dc1Node> > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		result.Insert(elementAt(i));
	}
	return result;
}

unsigned int MpafMPEG7DMStatementType_CollectionType::size() const
{
	return (unsigned int)m_Item->size();
}

void MpafMPEG7DMStatementType_CollectionType::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

// JRS: addition to Xerces collection
int MpafMPEG7DMStatementType_CollectionType::elementIndexOf(const Dc1NodePtr &toCheck) const
{
	for(unsigned int i = 0; i < size(); i++)
	{
		if(elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1; // not found
}

XMLCh * MpafMPEG7DMStatementType_CollectionType::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < size(); i++)
	{
		XMLCh * tmp = (elementAt(i))->ToText();
		Dc1Util::CatString(&buf, tmp);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		if(i < size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer
	return buf;
}

bool MpafMPEG7DMStatementType_CollectionType::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
		
	// This is not a simpletype list ItemType and intentionally unimplemented for collections
	return false;
}

/** 
 * Item type is ElementOnly. 
 */
void MpafMPEG7DMStatementType_CollectionType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
	} else {
		element = doc->getDocumentElement();
	}


// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtPreSerialize.h


	for(unsigned int i = 0; i < size(); i++)
	{
		// Collection is not SimpleTypeList -> serialize elements
		if(this->GetContentName() != NULL && !dontSerializeWithContentName)
		{
// OLD			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			*newElem = elem;
			(elementAt(i))->Serialize(doc, elem, newElem);
			element->appendChild(elem);
		}
		else
		{
			// Serialize into current node
			(elementAt(i))->Serialize(doc, element, &element);
		}
		if(i < size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
	}
}



bool MpafMPEG7DMStatementType_CollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(parent == NULL)
	{
		return false;
	}
	* current = parent; // Init with first element - check this!

	// Deserialize collection
	// Itemtype = Empty | ElementOnly
	DOMElement * ignore = NULL;
	while((parent != NULL) && (
		Dc1Util::HasNodeName(parent, X("CreationInformationType"), X("urn:mpeg:maf:schema:preservation:2015"))
// OLD3		(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:CreationInformationType")) == 0)
		||
		Dc1Util::HasNodeName(parent, X("DescriptionUnit"), X("urn:mpeg:maf:schema:preservation:2015"))
// OLD3		(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:maf:schema:preservation:2015:DescriptionUnit")) == 0)
	))
	{
		Dc1NodePtr item = MpafMPEG7DMStatementType_LocalPtr();

		item = CreateMPEG7DMStatementType_LocalType; // FTT, check this
		// Sequence or Choice -- don't use "own" node
		item->Deserialize(doc, parent, current);
		// baw 2004-11-15 begin: do not set content name on local type
		// XMLCh * contentname = XMLString::replicate(parent->getNodeName());
		// item->SetContentName(contentname);
		item->SetContentName(NULL);
		// baw 2004-11-15 end
		addElement(item);
		parent = Dc1Util::GetNextAkin(* current); // Previously GetNextSibling
	}


// no includefile for extension defined 
// file MpafMPEG7DMStatementType_CollectionType_ExtPostDeserialize.h


	return true;
}

inline
bool am_in_collection(int next_slash, int next_bracket, int next_close_bracket)
{
	return next_bracket >= 0
		&& next_close_bracket > next_bracket + 1 // at least one digit
		&& (next_slash < 0		// either at end of string ...
			|| next_slash > next_close_bracket); // ... or collection
												 // is in current part
}

Dc1NodePtr MpafMPEG7DMStatementType_CollectionType::NodeFromXPath(const XMLCh *xpath) const
{
	const XMLCh *work_string = xpath;
	const XMLCh *content_name = GetContentName();
	Dc1NodePtr me = m_myself.getPointer();
	Dc1NodeCollectionPtr me_as_collection = me;
	Dc1NodePtr parent = GetParent();

	if (XMLString::startsWith(work_string, X("/"))) {
		if (!parent)
			work_string++;		// consume leading slash
		else {
			Dc1NodePtr root = parent;
			while (root->GetParent())
				root = root->GetParent();
			return root->NodeFromXPath(work_string); // EXIT: parse from root
		}
	}

	if (XMLString::stringLen(work_string) == 0)
		return me;

	if (XMLString::startsWith(work_string, X("..")))
		return UpwardWithXPath(work_string);

	XMLCh slash = X("/")[0];
	XMLCh bracket = X("[")[0];
	XMLCh close_bracket = X("]")[0];
	int next_slash = XMLString::indexOf(work_string, slash);
	int next_bracket = XMLString::indexOf(work_string, bracket);
	int next_close_bracket = XMLString::indexOf(work_string, close_bracket);

	// We're a collection -- find n, nth element, parse from there
	int n;
	if (!am_in_collection(next_slash, next_bracket, next_close_bracket))
	{
		if (!content_name) {
			// A collection can be a a choice too; there's a valid
			// test case where we needed to descend into a
			// collection for the xpath string "Name".  The
			// "!content_name" clause is just a futile attempt to
			// cut back the lossage a bit; I'm assuming that none
			// of these freak collections has it set.
			Dc1NodeEnum children = me_as_collection->GetElements();
			while (children.HasNext()) {
				Dc1NodePtr result = children.GetNext()->NodeFromXPath(xpath);
				if (result != Dc1NodePtr())
					return result;	// EXIT: success!
			}
		}
		// fall-through case: no collection index found, malformed
		// xpath string or xpath string didn't match the children.
		return Dc1NodePtr(); // EXIT
	}
	XMLCh *n_str = Dc1Util::allocate(next_close_bracket - next_bracket + 1);
	XMLString::moveChars(n_str, work_string + next_bracket + 1,
		next_close_bracket - next_bracket - 1);
	n = XMLString::parseInt(n_str) - 1;	// XPath indices start
	// with 1, ours start with
	// 0 -- adjust
	XMLString::release(&n_str);
	if (n >= 0 && n < (int)me_as_collection->size()) {
		// if the parent is a LocalType, our ContentName has to
		// match; otherwise, it's allowed to be unset.  if it's
		// set, it has to match.
		if ((content_name
				&& XMLString::startsWith(work_string, content_name)
				&& !XMLString::isAlphaNum(
					work_string[XMLString::stringLen(content_name)])))
		{
			if (next_slash != -1)
				if (dontSerializeWithContentName)
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string);
				else
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string + next_slash + 1);
			else
				// EXIT: success!  (xpath ended with ".../elemName[n]")
				return me_as_collection->elementAt(n);
		}
		else if (!content_name)
		{
			// It's probably a local collection (collection ->
			// LocalType -> Type) -- the name will be picked up by
			// the Type node, so pass in full work string here.
			return me_as_collection->elementAt(n)->NodeFromXPath(
				work_string); // EXIT
		}
		else
			return Dc1NodePtr(); // EXIT: wrong content name
	} else
	return Dc1NodePtr();	// EXIT: index out of range

}

