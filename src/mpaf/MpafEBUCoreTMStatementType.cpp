
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtImplInclude.h


#include "MpafTechnicalMetadataStatementType.h"
#include "EbuCoreformatType.h"
#include "W3CNMTOKENS.h"
#include "MpafEBUCoreTMStatementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMpafEBUCoreTMStatementType::IMpafEBUCoreTMStatementType()
{

// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtPropInit.h

}

IMpafEBUCoreTMStatementType::~IMpafEBUCoreTMStatementType()
{
// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtPropCleanup.h

}

MpafEBUCoreTMStatementType::MpafEBUCoreTMStatementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafEBUCoreTMStatementType::~MpafEBUCoreTMStatementType()
{
	Cleanup();
}

void MpafEBUCoreTMStatementType::Init()
{
	// Init base
	m_Base = CreateTechnicalMetadataStatementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_TechMetadata = EbuCoreformatPtr(); // Class
	m_TechMetadata_Exist = false;


// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtMyPropInit.h

}

void MpafEBUCoreTMStatementType::Cleanup()
{
// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_TechMetadata);
}

void MpafEBUCoreTMStatementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use EBUCoreTMStatementTypePtr, since we
	// might need GetBase(), which isn't defined in IEBUCoreTMStatementType
	const Dc1Ptr< MpafEBUCoreTMStatementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = MpafTechnicalMetadataStatementPtr(Dc1Factory::CloneObject((dynamic_cast< const MpafEBUCoreTMStatementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidTechMetadata())
	{
		// Dc1Factory::DeleteObject(m_TechMetadata);
		this->SetTechMetadata(Dc1Factory::CloneObject(tmp->GetTechMetadata()));
	}
	else
	{
		InvalidateTechMetadata();
	}
}

Dc1NodePtr MpafEBUCoreTMStatementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr MpafEBUCoreTMStatementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< MpafTechnicalMetadataStatementType > MpafEBUCoreTMStatementType::GetBase() const
{
	return m_Base;
}

EbuCoreformatPtr MpafEBUCoreTMStatementType::GetTechMetadata() const
{
		return m_TechMetadata;
}

// Element is optional
bool MpafEBUCoreTMStatementType::IsValidTechMetadata() const
{
	return m_TechMetadata_Exist;
}

void MpafEBUCoreTMStatementType::SetTechMetadata(const EbuCoreformatPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafEBUCoreTMStatementType::SetTechMetadata().");
	}
	if (m_TechMetadata != item || m_TechMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TechMetadata);
		m_TechMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TechMetadata) m_TechMetadata->SetParent(m_myself.getPointer());
		if (m_TechMetadata && m_TechMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TechMetadata->UseTypeAttribute = true;
		}
		m_TechMetadata_Exist = true;
		if(m_TechMetadata != EbuCoreformatPtr())
		{
			m_TechMetadata->SetContentName(XMLString::transcode("TechMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafEBUCoreTMStatementType::InvalidateTechMetadata()
{
	m_TechMetadata_Exist = false;
}
XMLCh * MpafEBUCoreTMStatementType::GetmimeType() const
{
	return GetBase()->GetBase()->GetmimeType();
}

void MpafEBUCoreTMStatementType::SetmimeType(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafEBUCoreTMStatementType::SetmimeType().");
	}
	GetBase()->GetBase()->SetmimeType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * MpafEBUCoreTMStatementType::Getref() const
{
	return GetBase()->GetBase()->Getref();
}

bool MpafEBUCoreTMStatementType::Existref() const
{
	return GetBase()->GetBase()->Existref();
}
void MpafEBUCoreTMStatementType::Setref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafEBUCoreTMStatementType::Setref().");
	}
	GetBase()->GetBase()->Setref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafEBUCoreTMStatementType::Invalidateref()
{
	GetBase()->GetBase()->Invalidateref();
}
XMLCh * MpafEBUCoreTMStatementType::Getencoding() const
{
	return GetBase()->GetBase()->Getencoding();
}

bool MpafEBUCoreTMStatementType::Existencoding() const
{
	return GetBase()->GetBase()->Existencoding();
}
void MpafEBUCoreTMStatementType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafEBUCoreTMStatementType::Setencoding().");
	}
	GetBase()->GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafEBUCoreTMStatementType::Invalidateencoding()
{
	GetBase()->GetBase()->Invalidateencoding();
}
W3CNMTOKENSPtr MpafEBUCoreTMStatementType::GetcontentEncoding() const
{
	return GetBase()->GetBase()->GetcontentEncoding();
}

bool MpafEBUCoreTMStatementType::ExistcontentEncoding() const
{
	return GetBase()->GetBase()->ExistcontentEncoding();
}
void MpafEBUCoreTMStatementType::SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafEBUCoreTMStatementType::SetcontentEncoding().");
	}
	GetBase()->GetBase()->SetcontentEncoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafEBUCoreTMStatementType::InvalidatecontentEncoding()
{
	GetBase()->GetBase()->InvalidatecontentEncoding();
}

Dc1NodeEnum MpafEBUCoreTMStatementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TechMetadata")) == 0)
	{
		// TechMetadata is simple element formatType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTechMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType")))) != empty)
			{
				// Is type allowed
				EbuCoreformatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTechMetadata(p, client);
					if((p = GetTechMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for EBUCoreTMStatementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "EBUCoreTMStatementType");
	}
	return result;
}

XMLCh * MpafEBUCoreTMStatementType::ToText() const
{
	return m_Base->ToText();
}


bool MpafEBUCoreTMStatementType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void MpafEBUCoreTMStatementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("EBUCoreTMStatementType"));
	// Element serialization:
	// Class
	
	if (m_TechMetadata != EbuCoreformatPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TechMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("TechMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TechMetadata->Serialize(doc, element, &elem);
	}

}

bool MpafEBUCoreTMStatementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is MpafTechnicalMetadataStatementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TechMetadata"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TechMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateformatType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTechMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafEBUCoreTMStatementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("TechMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateformatType; // FTT, check this
	}
	this->SetTechMetadata(child);
  }
  return child;
 
}

Dc1NodeEnum MpafEBUCoreTMStatementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTechMetadata() != Dc1NodePtr())
		result.Insert(GetTechMetadata());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafEBUCoreTMStatementType_ExtMethodImpl.h


