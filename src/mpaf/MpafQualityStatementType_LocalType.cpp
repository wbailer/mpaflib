
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtImplInclude.h


#include "Mp7JrsMpeg7BaseType.h"
#include "Mp7JrsCompleteDescriptionType.h"
#include "MpafQualityStatementType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMpafQualityStatementType_LocalType::IMpafQualityStatementType_LocalType()
{

// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtPropInit.h

}

IMpafQualityStatementType_LocalType::~IMpafQualityStatementType_LocalType()
{
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtPropCleanup.h

}

MpafQualityStatementType_LocalType::MpafQualityStatementType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafQualityStatementType_LocalType::~MpafQualityStatementType_LocalType()
{
	Cleanup();
}

void MpafQualityStatementType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_DescriptionUnit = Dc1Ptr< Dc1Node >(); // Class
	m_Description = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMyPropInit.h

}

void MpafQualityStatementType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_DescriptionUnit);
	// Dc1Factory::DeleteObject(m_Description);
}

void MpafQualityStatementType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QualityStatementType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IQualityStatementType_LocalType
	const Dc1Ptr< MpafQualityStatementType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_DescriptionUnit);
		this->SetDescriptionUnit(Dc1Factory::CloneObject(tmp->GetDescriptionUnit()));
		// Dc1Factory::DeleteObject(m_Description);
		this->SetDescription(Dc1Factory::CloneObject(tmp->GetDescription()));
}

Dc1NodePtr MpafQualityStatementType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafQualityStatementType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1Ptr< Dc1Node > MpafQualityStatementType_LocalType::GetDescriptionUnit() const
{
		return m_DescriptionUnit;
}

Dc1Ptr< Dc1Node > MpafQualityStatementType_LocalType::GetDescription() const
{
		return m_Description;
}

// implementing setter for choice 
/* element */
void MpafQualityStatementType_LocalType::SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType_LocalType::SetDescriptionUnit().");
	}
	if (m_DescriptionUnit != item)
	{
		// Dc1Factory::DeleteObject(m_DescriptionUnit);
		m_DescriptionUnit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionUnit) m_DescriptionUnit->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_DescriptionUnit) m_DescriptionUnit->UseTypeAttribute = true;
		if(m_DescriptionUnit != Dc1Ptr< Dc1Node >())
		{
			m_DescriptionUnit->SetContentName(XMLString::transcode("DescriptionUnit"));
		}
	// Dc1Factory::DeleteObject(m_Description);
	m_Description = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void MpafQualityStatementType_LocalType::SetDescription(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafQualityStatementType_LocalType::SetDescription().");
	}
	if (m_Description != item)
	{
		// Dc1Factory::DeleteObject(m_Description);
		m_Description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Description) m_Description->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Description) m_Description->UseTypeAttribute = true;
		if(m_Description != Dc1Ptr< Dc1Node >())
		{
			m_Description->SetContentName(XMLString::transcode("Description"));
		}
	// Dc1Factory::DeleteObject(m_DescriptionUnit);
	m_DescriptionUnit = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: QualityStatementType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum MpafQualityStatementType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * MpafQualityStatementType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafQualityStatementType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafQualityStatementType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_DescriptionUnit != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionUnit->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("DescriptionUnit"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_DescriptionUnit->UseTypeAttribute = true;
		}
		m_DescriptionUnit->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Description != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Description->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), X("Description"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Description->UseTypeAttribute = true;
		}
		m_Description->Serialize(doc, element, &elem);
	}

}

bool MpafQualityStatementType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionUnit"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionUnit")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMpeg7BaseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionUnit(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:maf:schema:preservation:2015")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Description")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCompleteDescriptionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafQualityStatementType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("DescriptionUnit")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMpeg7BaseType; // FTT, check this
	}
	this->SetDescriptionUnit(child);
  }
  if (XMLString::compareString(elementname, X("Description")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCompleteDescriptionType; // FTT, check this
	}
	this->SetDescription(child);
  }
  return child;
 
}

Dc1NodeEnum MpafQualityStatementType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptionUnit() != Dc1NodePtr())
		result.Insert(GetDescriptionUnit());
	if (GetDescription() != Dc1NodePtr())
		result.Insert(GetDescription());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMethodImpl.h


