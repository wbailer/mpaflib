
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "MpafFactoryDefines.h"
// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtImplInclude.h


#include "MpafContextStatementType_Reference_Label_CollectionType.h"
#include "MpafContextStatementType_Reference_Relation_CollectionType.h"
#include "MpafContextStatementType_Reference_Description_CollectionType.h"
#include "MpafContextStatementType_Reference_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Label
#include "MpafRelatedEntityType.h" // Element collection urn:mpeg:maf:schema:preservation:2015:Relation

#include <assert.h>
IMpafContextStatementType_Reference_LocalType::IMpafContextStatementType_Reference_LocalType()
{

// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtPropInit.h

}

IMpafContextStatementType_Reference_LocalType::~IMpafContextStatementType_Reference_LocalType()
{
// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtPropCleanup.h

}

MpafContextStatementType_Reference_LocalType::MpafContextStatementType_Reference_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

MpafContextStatementType_Reference_LocalType::~MpafContextStatementType_Reference_LocalType()
{
	Cleanup();
}

void MpafContextStatementType_Reference_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Label = MpafContextStatementType_Reference_Label_CollectionPtr(); // Collection
	m_Relation = MpafContextStatementType_Reference_Relation_CollectionPtr(); // Collection
	m_Description = MpafContextStatementType_Reference_Description_CollectionPtr(); // Collection


// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtMyPropInit.h

}

void MpafContextStatementType_Reference_LocalType::Cleanup()
{
// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Label);
	// Dc1Factory::DeleteObject(m_Relation);
	// Dc1Factory::DeleteObject(m_Description);
}

void MpafContextStatementType_Reference_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContextStatementType_Reference_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IContextStatementType_Reference_LocalType
	const Dc1Ptr< MpafContextStatementType_Reference_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Label);
		this->SetLabel(Dc1Factory::CloneObject(tmp->GetLabel()));
		// Dc1Factory::DeleteObject(m_Relation);
		this->SetRelation(Dc1Factory::CloneObject(tmp->GetRelation()));
		// Dc1Factory::DeleteObject(m_Description);
		this->SetDescription(Dc1Factory::CloneObject(tmp->GetDescription()));
}

Dc1NodePtr MpafContextStatementType_Reference_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr MpafContextStatementType_Reference_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


MpafContextStatementType_Reference_Label_CollectionPtr MpafContextStatementType_Reference_LocalType::GetLabel() const
{
		return m_Label;
}

MpafContextStatementType_Reference_Relation_CollectionPtr MpafContextStatementType_Reference_LocalType::GetRelation() const
{
		return m_Relation;
}

MpafContextStatementType_Reference_Description_CollectionPtr MpafContextStatementType_Reference_LocalType::GetDescription() const
{
		return m_Description;
}

void MpafContextStatementType_Reference_LocalType::SetLabel(const MpafContextStatementType_Reference_Label_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafContextStatementType_Reference_LocalType::SetLabel().");
	}
	if (m_Label != item)
	{
		// Dc1Factory::DeleteObject(m_Label);
		m_Label = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Label) m_Label->SetParent(m_myself.getPointer());
		if(m_Label != MpafContextStatementType_Reference_Label_CollectionPtr())
		{
			m_Label->SetContentName(XMLString::transcode("Label"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafContextStatementType_Reference_LocalType::SetRelation(const MpafContextStatementType_Reference_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafContextStatementType_Reference_LocalType::SetRelation().");
	}
	if (m_Relation != item)
	{
		// Dc1Factory::DeleteObject(m_Relation);
		m_Relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relation) m_Relation->SetParent(m_myself.getPointer());
		if(m_Relation != MpafContextStatementType_Reference_Relation_CollectionPtr())
		{
			m_Relation->SetContentName(XMLString::transcode("Relation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void MpafContextStatementType_Reference_LocalType::SetDescription(const MpafContextStatementType_Reference_Description_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in MpafContextStatementType_Reference_LocalType::SetDescription().");
	}
	if (m_Description != item)
	{
		// Dc1Factory::DeleteObject(m_Description);
		m_Description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Description) m_Description->SetParent(m_myself.getPointer());
		if(m_Description != MpafContextStatementType_Reference_Description_CollectionPtr())
		{
			m_Description->SetContentName(XMLString::transcode("Description"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum MpafContextStatementType_Reference_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Label")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Label is item of type TextualType
		// in element collection ContextStatementType_Reference_Label_CollectionType
		
		context->Found = true;
		MpafContextStatementType_Reference_Label_CollectionPtr coll = GetLabel();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContextStatementType_Reference_Label_CollectionType; // FTT, check this
				SetLabel(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relation")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Relation is item of type RelatedEntityType
		// in element collection ContextStatementType_Reference_Relation_CollectionType
		
		context->Found = true;
		MpafContextStatementType_Reference_Relation_CollectionPtr coll = GetRelation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContextStatementType_Reference_Relation_CollectionType; // FTT, check this
				SetRelation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RelatedEntityType")))) != empty)
			{
				// Is type allowed
				MpafRelatedEntityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Description")) == 0)
	{
		// urn:mpeg:maf:schema:preservation:2015:Description is item of type TextualType
		// in element collection ContextStatementType_Reference_Description_CollectionType
		
		context->Found = true;
		MpafContextStatementType_Reference_Description_CollectionPtr coll = GetDescription();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContextStatementType_Reference_Description_CollectionType; // FTT, check this
				SetDescription(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ContextStatementType_Reference_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ContextStatementType_Reference_LocalType");
	}
	return result;
}

XMLCh * MpafContextStatementType_Reference_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool MpafContextStatementType_Reference_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void MpafContextStatementType_Reference_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:maf:schema:preservation:2015"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:maf:schema:preservation:2015"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:maf:schema:preservation:2015"), X("ContextStatementType_Reference_LocalType"));
	// Element serialization:
	if (m_Label != MpafContextStatementType_Reference_Label_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Label->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Relation != MpafContextStatementType_Reference_Relation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Description != MpafContextStatementType_Reference_Description_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Description->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool MpafContextStatementType_Reference_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Label"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Label")) == 0))
		{
			// Deserialize factory type
			MpafContextStatementType_Reference_Label_CollectionPtr tmp = CreateContextStatementType_Reference_Label_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLabel(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relation"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relation")) == 0))
		{
			// Deserialize factory type
			MpafContextStatementType_Reference_Relation_CollectionPtr tmp = CreateContextStatementType_Reference_Relation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:maf:schema:preservation:2015")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Description")) == 0))
		{
			// Deserialize factory type
			MpafContextStatementType_Reference_Description_CollectionPtr tmp = CreateContextStatementType_Reference_Description_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescription(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr MpafContextStatementType_Reference_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Label")) == 0))
  {
	MpafContextStatementType_Reference_Label_CollectionPtr tmp = CreateContextStatementType_Reference_Label_CollectionType; // FTT, check this
	this->SetLabel(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relation")) == 0))
  {
	MpafContextStatementType_Reference_Relation_CollectionPtr tmp = CreateContextStatementType_Reference_Relation_CollectionType; // FTT, check this
	this->SetRelation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Description")) == 0))
  {
	MpafContextStatementType_Reference_Description_CollectionPtr tmp = CreateContextStatementType_Reference_Description_CollectionType; // FTT, check this
	this->SetDescription(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum MpafContextStatementType_Reference_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLabel() != Dc1NodePtr())
		result.Insert(GetLabel());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetDescription() != Dc1NodePtr())
		result.Insert(GetDescription());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file MpafContextStatementType_Reference_LocalType_ExtMethodImpl.h


