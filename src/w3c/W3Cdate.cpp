
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "W3CFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file W3Cdate_ExtImplInclude.h


#include "W3Cdate.h"

W3Cdate::W3Cdate()
	: m_nsURI(X("http://www.w3.org/2001/XMLSchema"))
{
		Init();
}

W3Cdate::~W3Cdate()
{
		Cleanup();
}

void W3Cdate::Init()
{	   
		m_YearSign = +1;
		m_Year = 0;
		m_Month = 0;
		m_Day = 0;
		m_UTCTimezone_Exist = false;
		m_TimezoneSign = +1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_TimezoneOffset_Exist = false;

// no includefile for extension defined 
// file W3Cdate_ExtPropInit.h

}

void W3Cdate::Cleanup()
{
// no includefile for extension defined 
// file W3Cdate_ExtPropCleanup.h


}

void W3Cdate::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< W3Cdate > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr W3Cdate::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr W3Cdate::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int W3Cdate::GetYearSign() const
{
		return m_YearSign;
}

void W3Cdate::SetYearSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetYearSign().");
		}
		m_YearSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetYear() const
{
		return m_Year;
}

void W3Cdate::SetYear(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetYear().");
		}
		m_Year = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetMonth() const
{
		return m_Month;
}

void W3Cdate::SetMonth(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetMonth().");
		}
		m_Month = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetDay() const
{
		return m_Day;
}

void W3Cdate::SetDay(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetDay().");
		}
		m_Day = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool W3Cdate::GetUTCTimezone_Exist() const
{
		return m_UTCTimezone_Exist;
}

void W3Cdate::SetUTCTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetUTCTimezone_Exist().");
		}
		m_UTCTimezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void W3Cdate::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void W3Cdate::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cdate::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void W3Cdate::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool W3Cdate::GetTimezoneOffset_Exist() const
{
		return m_TimezoneOffset_Exist;
}

void W3Cdate::SetTimezoneOffset_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cdate::SetTimezoneOffset_Exist().");
		}
		m_TimezoneOffset_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * W3Cdate::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_YearSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}

		if(m_Year >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Year);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 4);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Month >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Month);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, X("-"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Day >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Day);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, X("-"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_UTCTimezone_Exist)
		{
			Dc1Util::CatString(&buf, X("Z"));
		}
		else if (m_TimezoneOffset_Exist)
		{
			if(m_TimezoneSign >= 0)
				Dc1Util::CatString(&buf, X("+"));
			else
				Dc1Util::CatString(&buf, X("-"));

			if(m_TimezoneHours >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_TimezoneMinutes >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool W3Cdate::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring[128] = { 0 };
		RegularExpression r("^\\-?(\\d{4}\\-\\d{2}\\-\\d{2})(Z|((\\-|\\+)\\d{2}:\\d{2}))?$");
		// This is for Xerces 3.1.1+

		if (r.matches(txt, &m))
		{
			for (int i = 0; i < m.getNoGroups(); i++)
			{
				int i1 = m.getStartPos(i);
				int i2 = Dc1Util::GetNextEndPos(m, i);
				if (i1 == i2) continue; // ignore missing optional or wrong group
				switch (i)
				{
				case 0: // \\-?
					if (i1 >= 0 && i2 - i1 == 1)
					{
						m_YearSign = -1;
					}
					break;
				case 1: // yyyy-mm-dd
					if (i1 >= 0)
					{
						// d4
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 4);
						m_Year = Dc1Convert::TextToUnsignedInt(substring);
						// d2
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1+5, i1 + 7);
						m_Month = Dc1Convert::TextToUnsignedInt(substring);
						// d2
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 8, i1 + 10);
						m_Day = Dc1Convert::TextToUnsignedInt(substring);
					}
					break;
				case 2: // Z
					if (i1 >= 0 && i2 - i1 == 1)
					{
						m_UTCTimezone_Exist = true;
					}
					break;
				case 3: // (-|+)hh:mm
					if (i1 >= 0)
					{
						m_TimezoneOffset_Exist = true;
						// -|+
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1+1);
						m_TimezoneSign = Dc1Convert::TextToSign(substring);
						// hh
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1+1, i1+3);
						m_TimezoneHours = Dc1Convert::TextToUnsignedInt(substring);
						// mm
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1+4, i1+6);
						m_TimezoneMinutes = Dc1Convert::TextToUnsignedInt(substring);
					}
					break;
				default: return true;
					break;
				}
			}
		}
		else return false;
		return true;
}


void W3Cdate::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("http://www.w3.org/2001/XMLSchema"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))));
		}

// no includefile for extension defined 
// file W3Cdate_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool W3Cdate::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file W3Cdate_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file W3Cdate_ExtMethodImpl.h








