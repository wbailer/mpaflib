
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "W3CFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file W3Ctime_ExtImplInclude.h


#include "W3Ctime.h"

W3Ctime::W3Ctime()
	: m_nsURI(X("http://www.w3.org/2001/XMLSchema"))
{
		Init();
}

W3Ctime::~W3Ctime()
{
		Cleanup();
}

void W3Ctime::Init()
{	   
		m_Hour = -1;
		m_Minute = -1;
		m_Second = -1;
		m_UTCTimezone_Exist = false;
		m_TimezoneSign = +1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_TimezoneOffset_Exist = false;

// no includefile for extension defined 
// file W3Ctime_ExtPropInit.h

}

void W3Ctime::Cleanup()
{
// no includefile for extension defined 
// file W3Ctime_ExtPropCleanup.h


}

void W3Ctime::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< W3Ctime > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr W3Ctime::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr W3Ctime::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int W3Ctime::GetHour() const
{
		return m_Hour;
}

void W3Ctime::SetHour(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetHour().");
		}
		m_Hour = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Ctime::GetMinute() const
{
		return m_Minute;
}

void W3Ctime::SetMinute(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetMinute().");
		}
		m_Minute = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Ctime::GetSecond() const
{
		return m_Second;
}

void W3Ctime::SetSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetSecond().");
		}
		m_Second = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool W3Ctime::GetUTCTimezone_Exist() const
{
		return m_UTCTimezone_Exist;
}

void W3Ctime::SetUTCTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetUTCTimezone_Exist().");
		}
		m_UTCTimezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Ctime::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void W3Ctime::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Ctime::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void W3Ctime::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Ctime::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void W3Ctime::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool W3Ctime::GetTimezoneOffset_Exist() const
{
		return m_TimezoneOffset_Exist;
}

void W3Ctime::SetTimezoneOffset_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Ctime::SetTimezoneOffset_Exist().");
		}
		m_TimezoneOffset_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * W3Ctime::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_Hour >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Hour);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Minute >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Minute);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, X("-"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Second >= 0)
		{
			XMLCh* tmp1 = Dc1Convert::BinToText(m_Second);
			Dc1Util::CatString(&buf, tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
		}

		if(m_UTCTimezone_Exist)
		{
			Dc1Util::CatString(&buf, X("Z"));
		}
		else if (m_TimezoneOffset_Exist)
		{
			if(m_TimezoneSign >= 0)
				Dc1Util::CatString(&buf, X("+"));
			else
				Dc1Util::CatString(&buf, X("-"));

			if(m_TimezoneHours >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_TimezoneMinutes >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool W3Ctime::Parse(const XMLCh * const txt)
{
		Cleanup();
		// mio 07 10 2004
		// FIXME: Regular expression validation is not implemented for this general pattern type.
		// (developers: see PatternType_CPP.template comments for more details.) 
		
		m_Content =  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(txt);
		return true;
}


void W3Ctime::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("http://www.w3.org/2001/XMLSchema"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time")))));
		}

// no includefile for extension defined 
// file W3Ctime_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool W3Ctime::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file W3Ctime_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file W3Ctime_ExtMethodImpl.h








