
// Based on RefCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include "W3CNMTOKENS.h"
#include "Dc1FactoryDefines.h"
 
#include "W3CFactoryDefines.h"
#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file W3CNMTOKENS_ExtImplInclude.h


W3CNMTOKENS::W3CNMTOKENS()
{
	m_Item = new XERCES_CPP_NAMESPACE_QUALIFIER RefVectorOf <XMLCh>(0);

// no includefile for extension defined 
// file W3CNMTOKENS_ExtPropInit.h

}

W3CNMTOKENS::~W3CNMTOKENS()
{

// no includefile for extension defined 
// file W3CNMTOKENS_ExtPropCleanup.h


	delete m_Item;
}

void W3CNMTOKENS::DeepCopy(const Dc1NodePtr &original)
{
	const W3CNMTOKENSPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: wrong argument type

	m_Item->removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		// This is necessary when Xerces and Mp7Jrs are in
        // different libraries
		XMLCh *text = tmp->elementAt(i);
		XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
		XMLString::copyString(ours, text);
		m_Item->addElement(ours);
	}
}

// no includefile for extension defined 
// file W3CNMTOKENS_ExtMethodImpl.h


void W3CNMTOKENS::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void W3CNMTOKENS::addElement(XMLCh * const toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::addElement().");
	}
	
	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toAdd) + 1];
	XMLString::copyString(ours, toAdd);
	m_Item->addElement(ours);	
		
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void W3CNMTOKENS::setElementAt(XMLCh * const toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::setElementAt().");
	}
	
	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toSet) + 1];
	XMLString::copyString(ours, toSet);
	m_Item->setElementAt(ours,setAt);

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void W3CNMTOKENS::insertElementAt(XMLCh * const toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::insertElementAt().");
	}

	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toInsert) + 1];
	XMLString::copyString(ours, toInsert);
	m_Item->insertElementAt(ours,insertAt);

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * W3CNMTOKENS::orphanElementAt(unsigned int orphanAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::orphanElementAt().");
	}
	return m_Item->orphanElementAt(orphanAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void W3CNMTOKENS::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::removeAllElements().");
	}
	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void W3CNMTOKENS::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3CNMTOKENS::removeElementAt().");
	}
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool W3CNMTOKENS::containsElement(const XMLCh * toCheck)
{
	return m_Item->containsElement(toCheck);
}

void W3CNMTOKENS::cleanup()
{
	m_Item->cleanup();
}

void W3CNMTOKENS::reinitialize()
{
	m_Item->reinitialize();
}

unsigned int W3CNMTOKENS::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const XMLCh * W3CNMTOKENS::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

XMLCh * W3CNMTOKENS::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

unsigned int W3CNMTOKENS::size() const
{
	return (unsigned int)m_Item->size();
}

void W3CNMTOKENS::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

// JRS: addition to Xerces collection
int W3CNMTOKENS::elementIndexOf(XMLCh * const toCheck) const
{
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		if(m_Item->elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1;
}

XMLCh * W3CNMTOKENS::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * tmp = (m_Item->elementAt(i));
		Dc1Util::CatString(&buf, tmp);
		if(i < m_Item->size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer;
	return buf;
}

bool W3CNMTOKENS::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(txt);
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			// This is necessary when Xerces and Mp7Jrs are in
            // different libraries
			XMLCh *text = tmp->elementAt(i);
			XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
			XMLString::copyString(ours, text);
			m_Item->addElement(ours);
		}
	}
	delete tmp; 
	return m_Item->size() > 0;
}


void W3CNMTOKENS::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent,XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
	if(this->GetContentName() != (XMLCh*)NULL)
		{
			// only for SimpleTypeList collections - check this!
// OLD			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("http://www.w3.org/2001/XMLSchema"), this->GetContentName());
			element->appendChild(elem);
			*newElem = elem;
			element = elem;
		}
	} else {
		element = doc->getDocumentElement();
	}

// no includefile for extension defined 
// file W3CNMTOKENS_ExtPreSerialize.h


	// Collection is simpleType list
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		element->appendChild(doc->createTextNode(m_Item->elementAt(i)));
		if(i < m_Item->size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
	}
}

bool W3CNMTOKENS::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
	// Deserialize collection
	if(parent == NULL)
	{
		return false;
	}

	* current = parent; // Init with first element - check this!
	// Deserialize XMLCh * collection
	// Collection is simpleType list
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			// This is necessary when Xerces and Mp7Jrs are in
            // different libraries
			XMLCh *text = tmp->elementAt(i);
			XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
			XMLString::copyString(ours, text);
			m_Item->addElement(ours);
		}
		// TODO Check do we need elementnames?
	}
	delete tmp;
// NOTE: Use the following includefile for extension
// #include "Dc1RelationType_target_CollectionType_ExtPostDeserialize.h"

	return true;
}

