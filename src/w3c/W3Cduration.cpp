
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "W3CFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file W3Cduration_ExtImplInclude.h


#include "W3Cduration.h"

W3Cduration::W3Cduration()
	: m_nsURI(X("http://www.w3.org/2001/XMLSchema"))
{
		Init();
}

W3Cduration::~W3Cduration()
{
		Cleanup();
}

void W3Cduration::Init()
{	   
		m_DurationSign = +1;
		m_Year = -1;
		m_Month = -1;
		m_Day = -1;
		m_Hour = -1;
		m_Minute = -1;
		m_Second = -1;

// no includefile for extension defined 
// file W3Cduration_ExtPropInit.h

}

void W3Cduration::Cleanup()
{
// no includefile for extension defined 
// file W3Cduration_ExtPropCleanup.h


}

void W3Cduration::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< W3Cduration > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr W3Cduration::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr W3Cduration::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int W3Cduration::GetDurationSign() const
{
		return m_DurationSign;
}

void W3Cduration::SetDurationSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetDurationSign().");
		}
		m_DurationSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cduration::GetYear() const
{
		return m_Year;
}

void W3Cduration::SetYear(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetYear().");
		}
		m_Year = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cduration::GetMonth() const
{
		return m_Month;
}

void W3Cduration::SetMonth(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetMonth().");
		}
		m_Month = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cduration::GetDay() const
{
		return m_Day;
}

void W3Cduration::SetDay(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetDay().");
		}
		m_Day = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cduration::GetHour() const
{
		return m_Hour;
}

void W3Cduration::SetHour(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetHour().");
		}
		m_Hour = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int W3Cduration::GetMinute() const
{
		return m_Minute;
}

void W3Cduration::SetMinute(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetMinute().");
		}
		m_Minute = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
double W3Cduration::GetSecond() const
{
		return m_Second;
}

void W3Cduration::SetSecond(double val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in W3Cduration::SetSecond().");
		}
		m_Second = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * W3Cduration::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_DurationSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}
		Dc1Util::CatString(&buf, L"P");

		if(m_Year >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Year);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 4);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, L"Y");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Month >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Month);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, L"M");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Day >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Day);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, L"D");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Hour >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Hour);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, L"T");
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, L"H");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Minute >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Minute);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			if(m_Hour < 0)
				Dc1Util::CatString(&buf, L"T");
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, L"M");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Second >= 0)
		{
			tmp1 = Dc1Convert::BinToText(m_Second);
			if(m_Hour < 0 && m_Minute < 0)
				Dc1Util::CatString(&buf, L"T");
			Dc1Util::CatString(&buf, tmp1);
			Dc1Util::CatString(&buf, L"S");
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1)
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool W3Cduration::Parse(const XMLCh * const txt)
{
		Cleanup();
		// mio 07 10 2004
		// FIXME: Regular expression validation is not implemented for this general pattern type.
		// (developers: see PatternType_CPP.template comments for more details.) 
		
		m_Content =  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(txt);
		return true;
}


void W3Cduration::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("http://www.w3.org/2001/XMLSchema"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:duration")))));
		}

// no includefile for extension defined 
// file W3Cduration_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool W3Cduration::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file W3Cduration_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file W3Cduration_ExtMethodImpl.h








