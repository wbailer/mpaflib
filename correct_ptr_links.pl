
#!/usr/bin/perl


# open and parse list of typedefs

@ptrTypes;

open(TYPEDEFS, "typedefs.out");
$ln;
while ($ln = readline(TYPEDEFS)) {
	($typename,$ptrname) = split(/\>/,$ln);
	$typename =~ s/[\t\n\r ]//g;
	$ptrname =~ s/[\t\n\r ]//g;

#	print $ptrname . "..." . $typename . "\n";

# as doxgen doubles all underscores of class names in filename, do the same here
	$typename =~ s/_/\__/g;
	
# use interface instead of implementation type

	$ptrTypes{$ptrname} = "I" . $typename;
}

close(TYPEDEFS);

# add entry to replace Dc1Ptr with itself
$ptrTypes{"Dc1Ptr"} = "Dc1Ptr";
$ptrTypes{"Dc1NodePtr"} = "Dc1Node";

#process all files in current directory

$basedir = shift || die 'Sorry, you must specify the doc/html directory as second parameter';

opendir(DIR, $basedir);

my $File = readdir DIR;

while( $File )
{
	if ($File ne '.' && $File ne '..') {
		$extension = $File;
		$extension =~ s/\w*\.(\w*)/$1/g;

		if ($extension eq "html") {

			# read a HTML file
			$replaced = "";

			print "processing file " . $File . " ... ";

			$fname = $basedir . "\\" . $File;

			open(HTMLFILE, $fname);
			$ln;
			while ($ln = readline(HTMLFILE)) {
				
				# find links to pointer template
				$ln =~ s/\<a class="el" href=\"classDc1Ptr.html\"\>(\w*)<\/a\>/\<a\ class="el" href=\"class$ptrTypes{$1}.html\"\>$1\<\/a\>/g;


				$replaced = $replaced . $ln;
			}

			close(HTMLFILE);

			#print $replaced ."\n-----\n";

			# open same file for writing
			$fname = ">" . $fname;

			open(HTMLFILE, $fname);
			print HTMLFILE $replaced;
			close (HTMLFILE);
			
			print "done\n";
		}
	}

	$File = readdir DIR;
}
closedir(DIR);

