
echo Generating doxygen documentation for Dc1  ...
doxygen Dc1.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./doc/html
                         

echo Generating doxygen documentation for EbuCore  ...
doxygen EbuCore.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./ebucore_doc/html
                         

echo Generating doxygen documentation for Mp7Jrs  ...
doxygen Mp7Jrs.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./mp7jrs_doc/html
                         

echo Generating doxygen documentation for Mpeg21  ...
doxygen Mpeg21.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./mpeg21_doc/html
                         

echo Generating doxygen documentation for Mpaf  ...
doxygen Mpaf.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./mpaf_doc/html
                         

echo Generating doxygen documentation for W3C  ...
doxygen W3C.doxygen

echo Correcting links to smart pointer classes ...
perl correct_ptr_links.pl ./w3c_doc/html
                         
                         
