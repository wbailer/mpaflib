/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef MPAFARCHIVE_H
#define MPAFARCHIVE_H

//#pragma once

#include "Dc1Node.h"
#include "Dc1Ptr.h"
#include "Dc1Defines.h"
#include "Dc1NodeCollection.h"
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

#include "Dc1SchemaLocationMap.h"
#include "Dc1Exception.h"

/** Mpaf archive.
  * A class which handles (de)serializing of Mpaf XML documents.
  */
class MPAF_EXPORT MpafArchive
{

public:
	/** @name Constructors and Destructors
	 */
	//@{

	MpafArchive(void);
	~MpafArchive(void);

	//@}

	/** @name Serializing and Deserializing
	 */
	//@{

	/** Serializes to a file.
	  * @param path Path and file name of the destination file.
	  * @param node Root node of the subtree to be serialized.
	  * @param nodeName Name of the root node (only necessary if a node of this type may have different names when used in different places of the schema).
	  * @param useTypeAttr Write the xsi:type attribute for all nodes in the subtree.
	  */
	void ToFile(const XMLCh * path, Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false);

	/** Serializes to a file.
	  * @param path Path and file name of the destination file.
	  * @param node Root node of the subtree to be serialized.
	  * @param nodeName Name of the root node (only necessary if a node of this type may have different names when used in different places of the schema).
	  * @param useTypeAttr Write the xsi:type attribute for all nodes in the subtree.
	  */
	void ToFile(const char* path, Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false);
	
	// TODO FTT Maybe we shouldn't use optional args for similar code in C# and JAVA

	/** Deserializes from file.
	  * @param path Path and file name of the source file.
	  * @param node Node to be used as parent of the fragement to be deserialized.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromFile(const XMLCh * path, Dc1NodePtr &node);

	/** Deserializes from file.
	  * The root node of the fragment in the file will become the root node of the document.
	  * @param path Path and file name of the source file.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromFile(const XMLCh * path);

	/** Deserializes from file.
	  * @param path Path and file name of the source file.
	  * @param node Node to be used as parent of the fragement to be deserialized.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromFile(const char * path, Dc1NodePtr &node);

	/** Deserializes from file.
	  * The root node of the fragment in the file will become the root node of the document.
	  * @param path Path and file name of the source file.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromFile(const char * path);

//---

	/** Constants that describe how to add something to a collection.
	 * - APPEND: append to the end of the collection.
	 * - INSERT: insert at given index.
	 * - REPLACE: replace element at given index.
	 */
	enum HowToAdd { APPEND, INSERT, REPLACE };

	/** Inserts a fragment from file into a collection.
	  * @param path Path and file name of the source file.
	  * @param collection Collection where the fragment is to be added.
	  * @param howtoadd how to add the fragment.
	  * @param where the index where the fragment is to be added.
	  * @return The index of the added node, or -1 on failure.
	  */
	int AddToCollectionFromFile(const XMLCh * path, Dc1NodeCollectionPtr &collection,
		HowToAdd howtoadd, int where = -1);

	/** Inserts a fragment from a buffer into a collection.
	  * @param buffer Buffer containing the MPEG-7 XML.
	  * @param collection Collection where the fragment is to be added.
	  * @param howtoadd how to add the fragment.
	  * @param where the index where the fragment is to be added.
	  * @return The index of the added node, or -1 on failure.
	  */
	int AddToCollectionFromBuffer(const XMLCh * buffer, Dc1NodeCollectionPtr &collection,
		HowToAdd howtoadd, int where = -1);

	/** Inserts a fragment from file into a collection.
	  * @param path Path and file name of the source file.
	  * @param collection Collection where the fragment is to be added.
	  * @param howtoadd how to add the fragment.
	  * @param where the index where the fragment is to be added.
	  * @return The index of the added node, or -1 on failure.
	  */
	int AddToCollectionFromFile(const char * path, Dc1NodeCollectionPtr &collection,
		HowToAdd howtoadd, int where = -1);
		
	/** Inserts a fragment from a buffer into a collection.
	  * @param buffer Buffer containing the MPEG-7 XML.
	  * @param collection Collection where the fragment is to be added.
	  * @param howtoadd how to add the fragment.
	  * @param where the index where the fragment is to be added.
	  * @return The index of the added node, or -1 on failure.
	  */
	int AddToCollectionFromBuffer(const char * buffer, Dc1NodeCollectionPtr &collection,
		HowToAdd howtoadd, int where = -1);
//---

	/** Serializes to a 16 bit Unicode (Xerces) buffer.
	  * @param node Root node of the subtree to be serialized.
	  * @param nodeName Name of the root node (only necessary if a node of this type may have different names when used in different places of the schema).
	  * @param useTypeAttr Write the xsi:type attribute for all nodes in the subtree.
	  * @return a 16 bit Unicode buffer (on the Xerces heap), which *MUST* be released via <code>DeleteBuffer()</code>
	  */
	XMLCh* ToWBuffer(Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false);

	/** Serializes to a 8 bit (Xerces) buffer.
	  * @param node Root node of the subtree to be serialized.
	  * @param nodeName Name of the root node (only necessary if a node of this type may have different names when used in different places of the schema).
	  * @param useTypeAttr Write the xsi:type attribute for all nodes in the subtree.
	  * @return a 8 bit buffer (on the Xerces heap), which *MUST* be released via <code>DeleteBuffer()</code>
	  */
	char* ToBuffer(Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false);

	/** Deserializes from a 16 bit Unicode buffer.
	  * @param buf Source buffer.
	  * @param node Node to be used as parent of the fragement to be deserialized.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromBuffer(const XMLCh * buf, Dc1NodePtr &node);

	/** Deserializes from a 16 bit Unicode buffer.
	  * The root node of the fragment in the buffer will become the root node of the document.
	  * @param buf Source buffer.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromBuffer(const XMLCh * buf);

	/** Deserializes from a 8 bit buffer.
	  * @param buf Source buffer.
	  * @param node Node to be used as parent of the fragement to be deserialized.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromBuffer(const char * buf, Dc1NodePtr &node);

	/** Deserializes from a 8 bit buffer.
	  * The root node of the fragment in the buffer will become the root node of the document.
	  * @param buf Source buffer.
	  * @return The root node of the deserialized fragment.
	  */
	Dc1NodePtr FromBuffer(const char * buf);

	/** Serializes to a console (stdout).
	  * @param node Root node of the subtree to be serialized.
	  * @param nodeName Name of the root node (only necessary if a node of this type may have different names when used in different places of the schema).
	  * @param useTypeAttr Write the xsi:type attribute for all nodes in the subtree.
	  */
	void ToStdOut(Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false);

	/** Deletes a buffer created when serializing (on the Xerces heap). Note: if you call this method with a pointer to your heap, the system becomes inconsitent.
	  * @param buffer Buffer to be deleted. 
	  */
	void DeleteBuffer(char** buffer);

	/** Deletes a buffer created when serializing (on the Xerces heap). Note: if you call this method with a pointer to your heap, the system becomes inconsitent.
	  * @param buffer Buffer to be deleted. 
	  */
	void DeleteBuffer(XMLCh** buffer);

	//@}

	/** @name Get and Set Options
	 */
	//@{

	/** Set the encoding (UTF-8, UTF-16 ...) of XML document to be (de)serialized. */
	void SetEncoding(const char * encoding) { SetEncoding(X(encoding)); } 
	/** Set the encoding (UTF-8, UTF-16 ...) of XML document to be (de)serialized. */
	void SetEncoding(const XMLCh* encoding);

	/** Get current encoding setting (UTF-8, UTF-16 ...) */
	char * GetEncoding();

	/** Set a schema location.
	  * @param namespaceURI URI of the schema namespace.
	  * @param prefix Namespace prefix of the schema.
	  * @param schemaLocation Local path and file name of the schema.
	  */
	void SetSchemaLocation(const char* namespaceURI, const char* prefix, const char * schemaLocation) { SetSchemaLocation(X(namespaceURI),X(prefix),X(schemaLocation)); } 

	/** Set a schema location.
	  * @param namespaceURI URI of the schema namespace.
	  * @param prefix Namespace prefix of the schema.
	  * @param schemaLocation Local path and file name of the schema.
	  */
	void SetSchemaLocation(const XMLCh* namespaceURI, const XMLCh* prefix, const XMLCh * schemaLocation);

	/** Get location of given schema.
	  */
	char * GetSchemaLocation(const char* namespaceURI);

	/** Sets the default namespace. If there is an existing entry it will be overwritten. 
	  * @param namespaceURI Namespace name
	  * @param prefix default namespace prefix, if used, otherwise maybe 0
	  */
	void SetDefaultNamespace(const XMLCh* namespaceURI, const XMLCh* prefix);

	/** Gets the namespace's name and prefix of the default namespace.
	  * @param namespaceURI Default namespace name
	  * @param prefix Default namespace prefix, if used, otherwise maybe null
	  * @return true if values set in the arguments, false if no namespace has been found. The prefix can be null.
	  */
	bool GetDefaultNamespace(const XMLCh* * namespaceURI, const XMLCh* * prefix);


	/** Set if validation shall be done
	  * @param validate If true, validation will be done.
	  */
	void SetDoValidation(bool validate) { this->validate = validate; }
	
	/** Get validation setting. */
	bool GetDoValidation() { return validate; }

	/** Set if full schema checking shall be done.
	  * @param fullSchema If true, full schema checking will be done.
	  * <b>IMPORTANT NOTE: This will cause Xerces 2.2 to crash at large maxOccurs values. Do not use!</b>
	  */
	void SetFullSchemaChecking(bool fullSchema) { this->fullSchema = fullSchema; }
	
	/** Get full schema checking setting */
	bool GetFullSchemaChecking() { return fullSchema; }
	
	/** Set if pretty formatted (with line breaks) output is desired.
	  */
	void SetPrettyFormatting(bool flag) { this->prettyformat = flag; }
	
	/** Get if pretty formatted output flag (using line breaks) is set.
	  */
	bool GetPrettyFormatting() { return prettyformat; }
	
	
	/** Set whether the XMLDecl header (version, encoding, standalone) shall be written.
	  * @param decl If true, the XMLDecl header will be written when serializing. */
	void SetWriteXMLDecl(bool decl) { writeXMLDecl = decl; xmlDeclDefault = false; }

	/** Get XMLDecl setting */
	bool GetWriteXMLDecl() { return writeXMLDecl; }

	/** Set whether the namespace declarations should be written to the XML root node or not (default: true).
	  * This is necessary to avoid xsi namespace declarations in case of an xsi:type all over the place.
	  * @param flag If true, all namespaces are declared in the XML root node. */
	void SetForceNamespaceDeclaration(bool flag) { forceNamespaceDeclaration = flag; }

	/** Get ForceNamespaceDeclaration setting */
	bool GetForceNamespaceDeclaration() { return forceNamespaceDeclaration; }

	/** Set whether or not you want your elements of the default namespace be prefixed (Initially: false).
	  * @param flag If true, xmlns:mpaf=urn:mpeg:maf:schema:preservation:2015 is used instead of xmlns=urn:mpeg:maf:schema:preservation:2015
	  */
	void SetUsePrefixForDefaultNS(bool flag) { this->usePrefixForDefaultNS = flag; }

	/** Get whether or not you want your elements of the default namespace be prefixed with mpaf (Initially: false). 
	*/
	bool GetUsePrefixForDefaultNS() { return this->usePrefixForDefaultNS; }
	
	//@}

private:

	class XercesWrapper;
	friend class XercesWrapper;
	
	void _Serialize(Dc1NodePtr &node, XMLCh* nodeName = (XMLCh*)NULL, bool useTypeAttr = false, bool writeHeader = true);

	int _DeserializeIntoCollection(InputSource &source,
	Dc1NodeCollectionPtr &collection, HowToAdd howtoadd, int where);

	Dc1NodePtr _Deserialize(Dc1NodePtr &node,
		InputSource &source);

	
	XMLCh* encoding;
	XMLFormatTarget* destination;
	bool standalone;
	XMLCh* version;

	Dc1SchemaLocationMap	_locationMap;

	bool validate;			// do validation
	bool fullSchema;		// do full schema checking
	bool writeXMLDecl;		// write XMLDecl
	bool xmlDeclDefault;	// use default setting for writing XMLDecl
							// default=true for complete files and false for fragments
							// default is used, if SetXMLDecl has not been called
							
	bool usePrefixForDefaultNS; // If true, all elements of the default namespace are prefixed. If no prefix is available, the prefix is ignored
	bool prettyformat; // Readable vs small xmlfiles
	bool forceNamespaceDeclaration;		// Write namespace decls in XML root node

	static Dc1ClientID maxId; //maximum client ID
};

#endif
