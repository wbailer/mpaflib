/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _803MPAFINTEGRITYSTATEMENTTYPE_CHECKLIST_DEPRECATEDENTITY_LOCALTYPE_H
#define _803MPAFINTEGRITYSTATEMENTTYPE_CHECKLIST_DEPRECATEDENTITY_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtInclude.h


class IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType;
typedef Dc1Ptr< IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType> MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalPtr;
class IW3CdateTime;
typedef Dc1Ptr< IW3CdateTime > W3CdateTimePtr;

/** 
 * Generated interface IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType for class MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType<br>
 * Located at: mpaf.xsd, line 1059<br>
 * Classified: Class<br>
 * Derived from: anyURI (String)<br>
 */
class MPAF_EXPORT IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType();
	virtual ~IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType();
	/**
	 * Sets the content of MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get accessibleUntil attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr GetaccessibleUntil() const = 0;

	/**
	 * Get accessibleUntil attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaccessibleUntil() const = 0;

	/**
	 * Set accessibleUntil attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void SetaccessibleUntil(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate accessibleUntil attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaccessibleUntil() = 0;

	/**
	 * Get description attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getdescription() const = 0;

	/**
	 * Get description attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdescription() const = 0;

	/**
	 * Set description attribute.
	 * @param item XMLCh *
	 */
	virtual void Setdescription(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate description attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedescription() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of IntegrityStatementType_CheckList_DeprecatedEntity_LocalType.
	 * Currently just supports rudimentary XPath expressions as IntegrityStatementType_CheckList_DeprecatedEntity_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtPropDef.h

};




/*
 * Generated class MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType<br>
 * Located at: mpaf.xsd, line 1059<br>
 * Classified: Class<br>
 * Derived from: anyURI (String)<br>
 * Defined in mpaf.xsd, line 1059.<br>
 */
class DC1_EXPORT MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType :
		public IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType
{
	friend class MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get accessibleUntil attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr GetaccessibleUntil() const;

	/*
	 * Get accessibleUntil attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaccessibleUntil() const;

	/*
	 * Set accessibleUntil attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void SetaccessibleUntil(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate accessibleUntil attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaccessibleUntil();

	/*
	 * Get description attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getdescription() const;

	/*
	 * Get description attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdescription() const;

	/*
	 * Set description attribute.
	 * @param item XMLCh *
	 */
	virtual void Setdescription(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate description attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedescription();

	/*
	 * Gets or creates Dc1NodePtr child elements of IntegrityStatementType_CheckList_DeprecatedEntity_LocalType.
	 * Currently just supports rudimentary XPath expressions as IntegrityStatementType_CheckList_DeprecatedEntity_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMyMethodDef.h


public:

	virtual ~MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType();

protected:
	MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	W3CdateTimePtr m_accessibleUntil;
	bool m_accessibleUntil_Exist;
	XMLCh * m_description;
	bool m_description_Exist;

	// Base String
	XMLCh * m_Base;


// no includefile for extension defined 
// file MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _803MPAFINTEGRITYSTATEMENTTYPE_CHECKLIST_DEPRECATEDENTITY_LOCALTYPE_H

