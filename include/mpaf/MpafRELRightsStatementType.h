/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _927MPAFRELRIGHTSSTATEMENTTYPE_H
#define _927MPAFRELRIGHTSSTATEMENTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "MpafFactoryDefines.h"
#include "Mpeg21FactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "MpafRELRightsStatementTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafRELRightsStatementType_ExtInclude.h


class IMpafRightsStatementType;
typedef Dc1Ptr< IMpafRightsStatementType > MpafRightsStatementPtr;
class IMpafRELRightsStatementType;
typedef Dc1Ptr< IMpafRELRightsStatementType> MpafRELRightsStatementPtr;
#include "Dc1Unknown.h" // for Any
#include "MpafRightsStatementType.h"
#include "MpafStatementType.h"
class IW3CNMTOKENS;
typedef Dc1Ptr< IW3CNMTOKENS > W3CNMTOKENSPtr;
#include "Mpeg21StatementType.h"
#include "Mpeg21DIDBaseType.h"

/** 
 * Generated interface IMpafRELRightsStatementType for class MpafRELRightsStatementType<br>
 * Located at: mpaf.xsd, line 1254<br>
 * Classified: Class<br>
 * Derived from: RightsStatementType (Class)<br>
 */
class MPAF_EXPORT IMpafRELRightsStatementType 
 :
		public IMpafRightsStatementType
{
public:
	// TODO: make these protected?
	IMpafRELRightsStatementType();
	virtual ~IMpafRELRightsStatementType();
		/** @name Sequence

		 */
		//@{
	// Optional xs:any block
	/**
	 * Get Any element.
	 * @return Dc1UnknownPtr containing an arbitrary XML fragment.
	 */
	virtual Dc1UnknownPtr GetAny(Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;
	/**
	 * Set Any element.
	 * @param item, the Dc1UnknownPtr containing an arbitrary XML fragment.
	 */
	virtual void SetAny(Dc1UnknownPtr item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;
		//@}
	/**
	 * Get mimeType attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetmimeType() const = 0;

	/**
	 * Set mimeType attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetmimeType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ref attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getref() const = 0;

	/**
	 * Get ref attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existref() const = 0;

	/**
	 * Set ref attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	virtual void Setref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ref attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateref() = 0;

	/**
	 * Get encoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const = 0;

	/**
	 * Get encoding attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const = 0;

	/**
	 * Set encoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate encoding attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding() = 0;

	/**
	 * Get contentEncoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return W3CNMTOKENSPtr
	 */
	virtual W3CNMTOKENSPtr GetcontentEncoding() const = 0;

	/**
	 * Get contentEncoding attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontentEncoding() const = 0;

	/**
	 * Set contentEncoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item const W3CNMTOKENSPtr &
	 */
	virtual void SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate contentEncoding attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontentEncoding() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of RELRightsStatementType.
	 * Currently just supports rudimentary XPath expressions as RELRightsStatementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafRELRightsStatementType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafRELRightsStatementType_ExtPropDef.h

};




/*
 * Generated class MpafRELRightsStatementType<br>
 * Located at: mpaf.xsd, line 1254<br>
 * Classified: Class<br>
 * Derived from: RightsStatementType (Class)<br>
 * Defined in mpaf.xsd, line 1254.<br>
 */
class DC1_EXPORT MpafRELRightsStatementType :
		public IMpafRELRightsStatementType
{
	friend class MpafRELRightsStatementTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of MpafRELRightsStatementType
	 
	 * @return Dc1Ptr< MpafRightsStatementType >
	 */
	virtual Dc1Ptr< MpafRightsStatementType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	// Optional xs:any block
	/**
	 * Get Any element.
	 * @return Dc1UnknownPtr containing an arbitrary XML fragment.
	 */
	virtual Dc1UnknownPtr GetAny(Dc1ClientID client = DC1_UNDEFINED_CLIENT);
	/**
	 * Set Any element.
	 * @param item, the Dc1UnknownPtr containing an arbitrary XML fragment.
	 */
	virtual void SetAny(Dc1UnknownPtr item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);
		//@}
	/*
	 * Get mimeType attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetmimeType() const;

	/*
	 * Set mimeType attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetmimeType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ref attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getref() const;

	/*
	 * Get ref attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existref() const;

	/*
	 * Set ref attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	virtual void Setref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ref attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateref();

	/*
	 * Get encoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const;

	/*
	 * Get encoding attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const;

	/*
	 * Set encoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate encoding attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding();

	/*
	 * Get contentEncoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @return W3CNMTOKENSPtr
	 */
	virtual W3CNMTOKENSPtr GetcontentEncoding() const;

	/*
	 * Get contentEncoding attribute validity information.
	 * (Inherited from MpafStatementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontentEncoding() const;

	/*
	 * Set contentEncoding attribute.
<br>
	 * (Inherited from MpafStatementType)
	 * @param item const W3CNMTOKENSPtr &
	 */
	virtual void SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate contentEncoding attribute.
	 * (Inherited from MpafStatementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontentEncoding();

	/*
	 * Gets or creates Dc1NodePtr child elements of RELRightsStatementType.
	 * Currently just supports rudimentary XPath expressions as RELRightsStatementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafRELRightsStatementType_ExtMyMethodDef.h


public:

	virtual ~MpafRELRightsStatementType();

protected:
	MpafRELRightsStatementType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< MpafRightsStatementType > m_Base;

	// Any 
	Dc1UnknownPtr m_Any;
	bool m_Any_Exist; // For optional elements 

// no includefile for extension defined 
// file MpafRELRightsStatementType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _927MPAFRELRIGHTSSTATEMENTTYPE_H

