/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _833MPAFOPERATORREFTYPE_H
#define _833MPAFOPERATORREFTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"

#include "MpafOperatorRefTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafOperatorRefType_ExtInclude.h


class IMpafOperatorRefType;
typedef Dc1Ptr< IMpafOperatorRefType> MpafOperatorRefPtr;

/** 
 * Generated interface IMpafOperatorRefType for class MpafOperatorRefType<br>
 * Located at: mpaf.xsd, line 502<br>
 * Classified: Class<br>
 * Derived from: anyURI (String)<br>
 */
class MPAF_EXPORT IMpafOperatorRefType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMpafOperatorRefType();
	virtual ~IMpafOperatorRefType();
	/**
	 * Sets the content of MpafOperatorRefType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of MpafOperatorRefType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get role attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getrole() const = 0;

	/**
	 * Get role attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrole() const = 0;

	/**
	 * Set role attribute.
	 * @param item XMLCh *
	 */
	virtual void Setrole(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate role attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterole() = 0;

	/**
	 * Get operatorOnBehalfOf attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetoperatorOnBehalfOf() const = 0;

	/**
	 * Get operatorOnBehalfOf attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistoperatorOnBehalfOf() const = 0;

	/**
	 * Set operatorOnBehalfOf attribute.
	 * @param item XMLCh *
	 */
	virtual void SetoperatorOnBehalfOf(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate operatorOnBehalfOf attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateoperatorOnBehalfOf() = 0;

	/**
	 * Get hasPermission attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GethasPermission() const = 0;

	/**
	 * Get hasPermission attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisthasPermission() const = 0;

	/**
	 * Set hasPermission attribute.
	 * @param item XMLCh *
	 */
	virtual void SethasPermission(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate hasPermission attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatehasPermission() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of OperatorRefType.
	 * Currently just supports rudimentary XPath expressions as OperatorRefType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafOperatorRefType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafOperatorRefType_ExtPropDef.h

};




/*
 * Generated class MpafOperatorRefType<br>
 * Located at: mpaf.xsd, line 502<br>
 * Classified: Class<br>
 * Derived from: anyURI (String)<br>
 * Defined in mpaf.xsd, line 502.<br>
 */
class DC1_EXPORT MpafOperatorRefType :
		public IMpafOperatorRefType
{
	friend class MpafOperatorRefTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of MpafOperatorRefType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of MpafOperatorRefType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get role attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getrole() const;

	/*
	 * Get role attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrole() const;

	/*
	 * Set role attribute.
	 * @param item XMLCh *
	 */
	virtual void Setrole(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate role attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterole();

	/*
	 * Get operatorOnBehalfOf attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetoperatorOnBehalfOf() const;

	/*
	 * Get operatorOnBehalfOf attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistoperatorOnBehalfOf() const;

	/*
	 * Set operatorOnBehalfOf attribute.
	 * @param item XMLCh *
	 */
	virtual void SetoperatorOnBehalfOf(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate operatorOnBehalfOf attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateoperatorOnBehalfOf();

	/*
	 * Get hasPermission attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GethasPermission() const;

	/*
	 * Get hasPermission attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisthasPermission() const;

	/*
	 * Set hasPermission attribute.
	 * @param item XMLCh *
	 */
	virtual void SethasPermission(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate hasPermission attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatehasPermission();

	/*
	 * Gets or creates Dc1NodePtr child elements of OperatorRefType.
	 * Currently just supports rudimentary XPath expressions as OperatorRefType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafOperatorRefType_ExtMyMethodDef.h


public:

	virtual ~MpafOperatorRefType();

protected:
	MpafOperatorRefType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_role;
	bool m_role_Exist;
	XMLCh * m_operatorOnBehalfOf;
	bool m_operatorOnBehalfOf_Exist;
	XMLCh * m_hasPermission;
	bool m_hasPermission_Exist;

	// Base String
	XMLCh * m_Base;


// no includefile for extension defined 
// file MpafOperatorRefType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _833MPAFOPERATORREFTYPE_H

