/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _945MPAFSEGMENTFIXITYCHECKTYPE_H
#define _945MPAFSEGMENTFIXITYCHECKTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "MpafFactoryDefines.h"

#include "MpafSegmentFixityCheckTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtInclude.h


class IMpafFixityCheckType;
typedef Dc1Ptr< IMpafFixityCheckType > MpafFixityCheckPtr;
class IMpafSegmentFixityCheckType;
typedef Dc1Ptr< IMpafSegmentFixityCheckType> MpafSegmentFixityCheckPtr;
#include "MpafFixityCheckType.h"

/** 
 * Generated interface IMpafSegmentFixityCheckType for class MpafSegmentFixityCheckType<br>
 * Located at: mpaf.xsd, line 982<br>
 * Classified: Class<br>
 * Derived from: FixityCheckType (Class)<br>
 */
class MPAF_EXPORT IMpafSegmentFixityCheckType 
 :
		public IMpafFixityCheckType
{
public:
	// TODO: make these protected?
	IMpafSegmentFixityCheckType();
	virtual ~IMpafSegmentFixityCheckType();
	/**
	 * Sets the content of MpafFixityCheckType<br>
	 * (Inherited from MpafFixityCheckType)
	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of MpafFixityCheckType 
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get byteOffset attribute.
	 * @return int
	 */
	virtual int GetbyteOffset() const = 0;

	/**
	 * Get byteOffset attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbyteOffset() const = 0;

	/**
	 * Set byteOffset attribute.
	 * @param item int
	 */
	virtual void SetbyteOffset(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate byteOffset attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebyteOffset() = 0;

	/**
	 * Get byteNumber attribute.
	 * @return int
	 */
	virtual int GetbyteNumber() const = 0;

	/**
	 * Get byteNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbyteNumber() const = 0;

	/**
	 * Set byteNumber attribute.
	 * @param item int
	 */
	virtual void SetbyteNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate byteNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebyteNumber() = 0;

	/**
	 * Get start attribute.
	 * @return double
	 */
	virtual double Getstart() const = 0;

	/**
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const = 0;

	/**
	 * Set start attribute.
	 * @param item double
	 */
	virtual void Setstart(double item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get span attribute.
	 * @return double
	 */
	virtual double Getspan() const = 0;

	/**
	 * Get span attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existspan() const = 0;

	/**
	 * Set span attribute.
	 * @param item double
	 */
	virtual void Setspan(double item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate span attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatespan() = 0;

	/**
	 * Get unit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getunit() const = 0;

	/**
	 * Get unit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existunit() const = 0;

	/**
	 * Set unit attribute.
	 * @param item XMLCh *
	 */
	virtual void Setunit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate unit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateunit() = 0;

	/**
	 * Get chunkUnit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetchunkUnit() const = 0;

	/**
	 * Get chunkUnit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistchunkUnit() const = 0;

	/**
	 * Set chunkUnit attribute.
	 * @param item XMLCh *
	 */
	virtual void SetchunkUnit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate chunkUnit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatechunkUnit() = 0;

	/**
	 * Get stream attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getstream() const = 0;

	/**
	 * Get stream attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstream() const = 0;

	/**
	 * Set stream attribute.
	 * @param item XMLCh *
	 */
	virtual void Setstream(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate stream attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestream() = 0;

	/**
	 * Get type attribute.
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const = 0;

	/**
	 * Set type attribute.
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of SegmentFixityCheckType.
	 * Currently just supports rudimentary XPath expressions as SegmentFixityCheckType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtPropDef.h

};




/*
 * Generated class MpafSegmentFixityCheckType<br>
 * Located at: mpaf.xsd, line 982<br>
 * Classified: Class<br>
 * Derived from: FixityCheckType (Class)<br>
 * Defined in mpaf.xsd, line 982.<br>
 */
class DC1_EXPORT MpafSegmentFixityCheckType :
		public IMpafSegmentFixityCheckType
{
	friend class MpafSegmentFixityCheckTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of MpafSegmentFixityCheckType
	 
	 * @return Dc1Ptr< MpafFixityCheckType >
	 */
	virtual Dc1Ptr< MpafFixityCheckType > GetBase() const;
	/*
	 * Sets the content of MpafFixityCheckType<br>
	 * (Inherited from MpafFixityCheckType)
	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of MpafFixityCheckType 
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get byteOffset attribute.
	 * @return int
	 */
	virtual int GetbyteOffset() const;

	/*
	 * Get byteOffset attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbyteOffset() const;

	/*
	 * Set byteOffset attribute.
	 * @param item int
	 */
	virtual void SetbyteOffset(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate byteOffset attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebyteOffset();

	/*
	 * Get byteNumber attribute.
	 * @return int
	 */
	virtual int GetbyteNumber() const;

	/*
	 * Get byteNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbyteNumber() const;

	/*
	 * Set byteNumber attribute.
	 * @param item int
	 */
	virtual void SetbyteNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate byteNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebyteNumber();

	/*
	 * Get start attribute.
	 * @return double
	 */
	virtual double Getstart() const;

	/*
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const;

	/*
	 * Set start attribute.
	 * @param item double
	 */
	virtual void Setstart(double item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get span attribute.
	 * @return double
	 */
	virtual double Getspan() const;

	/*
	 * Get span attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existspan() const;

	/*
	 * Set span attribute.
	 * @param item double
	 */
	virtual void Setspan(double item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate span attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatespan();

	/*
	 * Get unit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getunit() const;

	/*
	 * Get unit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existunit() const;

	/*
	 * Set unit attribute.
	 * @param item XMLCh *
	 */
	virtual void Setunit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate unit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateunit();

	/*
	 * Get chunkUnit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetchunkUnit() const;

	/*
	 * Get chunkUnit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistchunkUnit() const;

	/*
	 * Set chunkUnit attribute.
	 * @param item XMLCh *
	 */
	virtual void SetchunkUnit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate chunkUnit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatechunkUnit();

	/*
	 * Get stream attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getstream() const;

	/*
	 * Get stream attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstream() const;

	/*
	 * Set stream attribute.
	 * @param item XMLCh *
	 */
	virtual void Setstream(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate stream attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestream();

	/*
	 * Get type attribute.
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const;

	/*
	 * Set type attribute.
<br>
	 * (Inherited from MpafFixityCheckType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Gets or creates Dc1NodePtr child elements of SegmentFixityCheckType.
	 * Currently just supports rudimentary XPath expressions as SegmentFixityCheckType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMyMethodDef.h


public:

	virtual ~MpafSegmentFixityCheckType();

protected:
	MpafSegmentFixityCheckType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_byteOffset;
	bool m_byteOffset_Exist;
	int m_byteNumber;
	bool m_byteNumber_Exist;
	double m_start;
	bool m_start_Exist;
	double m_span;
	bool m_span_Exist;
	XMLCh * m_unit;
	bool m_unit_Exist;
	XMLCh * m_chunkUnit;
	bool m_chunkUnit_Exist;
	XMLCh * m_stream;
	bool m_stream_Exist;

	// Base Class
	Dc1Ptr< MpafFixityCheckType > m_Base;


// no includefile for extension defined 
// file MpafSegmentFixityCheckType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _945MPAFSEGMENTFIXITYCHECKTYPE_H

