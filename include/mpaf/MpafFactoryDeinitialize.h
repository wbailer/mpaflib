/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// DeInitialize section of Dc1Factory.h
// To remove a bunch of registered factories from the list.
{
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Activity_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Operator_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_Param_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_Param_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AgentType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AnnotationDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AnnotationStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AssetType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Choice_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Item_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_Confidence_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Operator_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:BitstreamType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:CELRightsStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ComponentType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ComponentType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ComponentType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ComponentType_Resource_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Description_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Label_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Relation_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DerivationRelationStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptiveMetadataDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptiveMetadataStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:DublinCoreDMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EBUCoreRDFDMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EBUCoreTMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EBUCoreXMLDMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EssenceType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EssenceType_Choice_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EssenceType_Component_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EssenceType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:EssenceType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ExploitationRightsDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:FileType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:FixityCheckType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:FixityDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:FixityStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:FixityStatementType_Check_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:GroupType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:GroupType_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:GroupType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:GroupType_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IdentificationDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IdentificationStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IdentificationStatementType_Identifier_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IdentifierBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IdentifierType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_DeprecatedEntity_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_DeprecatedEntity_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_Entity_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_ExternalDependency_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ItemBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ItemRefBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ItemRefType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ItemType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MCORightsStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MediaLocatorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MPAFBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:MPEG7TMStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OperatorRefType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OperatorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OperatorType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Contact_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Member_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Name_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Name_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_NameTerm_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_NameTerm_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_Affiliation_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_Affiliation_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_Citizenship_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_ElectronicAddress_CollectionType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PersonType_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Choice_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Item_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ProvenanceDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType_Activity_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType_Operator_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:QualityDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:QualityStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:QualityStatementType_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:QualityStatementType_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ReferenceDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RelatedEntityType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RelationStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RELRightsStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RepresentationType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Choice_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Condition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Descriptor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Item_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ResourceBaseType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RightsDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:RightsStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:SegmentFixityCheckType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:StatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:TechnicalMetadataDescriptorType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:TechnicalMetadataStatementType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType_Manufacturer_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType_Name_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_Param_CollectionType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_Param_LocalType");
  Dc1Factory::UnregisterFactory("urn:mpeg:maf:schema:preservation:2015:UsageRightsDescriptorType");
}
