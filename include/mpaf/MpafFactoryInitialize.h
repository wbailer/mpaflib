/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Initialize section of Dc1Factory.h
// included in Dc1Factory::Initialize()

{
	// Register extension namespace uri and its prefix(es), so that we can resolve it without Xerces XML DOM
	Dc1Factory::SetNamespaceUri(X("mpaf"), X("urn:mpeg:maf:schema:preservation:2015"));

	Dc1Factory * factory;
	
	factory = new MpafActivityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Activity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Content_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Content_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Content_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Content_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Operator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Parameters_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Parameters_Param_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafActivityType_Parameters_Param_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAgentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAnnotationDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAnnotationStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAssetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAssetType_PreservationObjectType_Choice_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAssetType_PreservationObjectType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAssetType_PreservationObjectType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAssetType_PreservationObjectType_Item_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementType_CheckList_Entity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementType_CheckList_Entity_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementType_CheckList_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafAuthenticityStatementType_CheckList_Operator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafBitstreamTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafCELRightsStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafComponentType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafComponentType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafComponentType_Resource_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementType_Reference_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementType_Reference_Description_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementType_Reference_Label_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementType_Reference_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafContextStatementType_Reference_Relation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDerivationRelationStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptiveMetadataDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptiveMetadataStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptorBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptorBaseType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptorBaseType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafDublinCoreDMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEBUCoreRDFDMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEBUCoreTMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEBUCoreXMLDMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEssenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEssenceType_Choice_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEssenceType_Component_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEssenceType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafEssenceType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafExploitationRightsDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafFileTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafFixityCheckTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafFixityDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafFixityStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafFixityStatementType_Check_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafGroupTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafGroupType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafGroupType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafGroupType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIdentificationDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIdentificationStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIdentificationStatementType_Identifier_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIdentifierBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIdentifierTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementType_CheckList_Entity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementType_CheckList_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafIntegrityStatementType_ExternalDependency_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafItemBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafItemRefBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafItemRefTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafItemTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMCORightsStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMediaLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMPAFBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMPEG7DMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMPEG7DMStatementType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMPEG7DMStatementType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafMPEG7TMStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOperatorRefTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOperatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOperatorType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationType_Contact_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationType_Name_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationType_Name_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationType_NameTerm_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafOrganizationType_NameTerm_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupType_Member_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupType_Name_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupType_Name_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupType_NameTerm_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonGroupType_NameTerm_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_Affiliation_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_Affiliation_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_Citizenship_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_ElectronicAddress_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPersonType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationMetadataTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationMetadataType_DIDLInfo_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationObjectTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationObjectType_Choice_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationObjectType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationObjectType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafPreservationObjectType_Item_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafProvenanceDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafProvenanceStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafProvenanceStatementType_Activity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafProvenanceStatementType_Operator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafQualityDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafQualityStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafQualityStatementType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafQualityStatementType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafReferenceDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRelatedEntityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRelationStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRELRightsStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRepresentationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRepresentationType_Choice_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRepresentationType_Condition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRepresentationType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRepresentationType_Item_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafResourceBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRightsDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafRightsStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafSegmentFixityCheckTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafTechnicalMetadataDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafTechnicalMetadataStatementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolType_Manufacturer_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolType_Parameters_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolType_Parameters_Param_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafToolType_Parameters_Param_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new MpafUsageRightsDescriptorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

}
