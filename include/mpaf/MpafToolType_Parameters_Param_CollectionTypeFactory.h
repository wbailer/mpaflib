/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef _962MPAFTOOLTYPE_PARAMETERS_PARAM_COLLECTIONTYPEFACTORY_H
#define _962MPAFTOOLTYPE_PARAMETERS_PARAM_COLLECTIONTYPEFACTORY_H

#include "Dc1Defines.h"
#include "Dc1Node.h"
#include "Dc1Factory.h"
#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"

/*
 * Generated factory MpafToolType_Parameters_Param_CollectionTypeFactory<br>
 * for class MpafToolType_Parameters_Param_CollectionType:<br>
 * Located at: mpaf.xsd, line 707<br>
 * Item type: ToolType_Parameters_Param_LocalType (Class)<br>
 */

class DC1_EXPORT MpafToolType_Parameters_Param_CollectionTypeFactory :
	public Dc1Factory
{
public:
	MpafToolType_Parameters_Param_CollectionTypeFactory();
	virtual ~MpafToolType_Parameters_Param_CollectionTypeFactory();

	virtual const char * GetTypeName() const;
	virtual Dc1NodePtr CreateObject();
};

#endif // _962MPAFTOOLTYPE_PARAMETERS_PARAM_COLLECTIONTYPEFACTORY_H
