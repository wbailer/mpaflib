/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _947MPAFSTATEMENTTYPE_H
#define _947MPAFSTATEMENTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mpeg21FactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "MpafStatementTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafStatementType_ExtInclude.h


class IMpeg21StatementType;
typedef Dc1Ptr< IMpeg21StatementType > Mpeg21StatementPtr;
class IMpafStatementType;
typedef Dc1Ptr< IMpafStatementType> MpafStatementPtr;
class IW3CNMTOKENS;
typedef Dc1Ptr< IW3CNMTOKENS > W3CNMTOKENSPtr;
#include "Mpeg21StatementType.h"
#include "Mpeg21DIDBaseType.h"

/** 
 * Generated interface IMpafStatementType for class MpafStatementType<br>
 * Located at: mpaf.xsd, line 833<br>
 * Classified: Class<br>
 * Derived from: StatementType (Class)<br>
 */
class MPAF_EXPORT IMpafStatementType 
 :
		public IMpeg21StatementType
{
public:
	// TODO: make these protected?
	IMpafStatementType();
	virtual ~IMpafStatementType();
	/**
	 * Get mimeType attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetmimeType() const = 0;

	/**
	 * Set mimeType attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetmimeType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ref attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getref() const = 0;

	/**
	 * Get ref attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existref() const = 0;

	/**
	 * Set ref attribute.
	 * @param item XMLCh *
	 */
	virtual void Setref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ref attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateref() = 0;

	/**
	 * Get encoding attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const = 0;

	/**
	 * Get encoding attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const = 0;

	/**
	 * Set encoding attribute.
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate encoding attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding() = 0;

	/**
	 * Get contentEncoding attribute.
	 * @return W3CNMTOKENSPtr
	 */
	virtual W3CNMTOKENSPtr GetcontentEncoding() const = 0;

	/**
	 * Get contentEncoding attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontentEncoding() const = 0;

	/**
	 * Set contentEncoding attribute.
	 * @param item const W3CNMTOKENSPtr &
	 */
	virtual void SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate contentEncoding attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontentEncoding() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of StatementType.
	 * Currently just supports rudimentary XPath expressions as StatementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafStatementType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafStatementType_ExtPropDef.h

};




/*
 * Generated class MpafStatementType<br>
 * Located at: mpaf.xsd, line 833<br>
 * Classified: Class<br>
 * Derived from: StatementType (Class)<br>
 * Defined in mpaf.xsd, line 833.<br>
 */
class DC1_EXPORT MpafStatementType :
		public IMpafStatementType
{
	friend class MpafStatementTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of MpafStatementType
	 
	 * @return Dc1Ptr< Mpeg21StatementType >
	 */
	virtual Dc1Ptr< Mpeg21StatementType > GetBase() const;
	/*
	 * Get mimeType attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetmimeType() const;

	/*
	 * Set mimeType attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetmimeType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ref attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getref() const;

	/*
	 * Get ref attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existref() const;

	/*
	 * Set ref attribute.
	 * @param item XMLCh *
	 */
	virtual void Setref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ref attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateref();

	/*
	 * Get encoding attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const;

	/*
	 * Get encoding attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const;

	/*
	 * Set encoding attribute.
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate encoding attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding();

	/*
	 * Get contentEncoding attribute.
	 * @return W3CNMTOKENSPtr
	 */
	virtual W3CNMTOKENSPtr GetcontentEncoding() const;

	/*
	 * Get contentEncoding attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontentEncoding() const;

	/*
	 * Set contentEncoding attribute.
	 * @param item const W3CNMTOKENSPtr &
	 */
	virtual void SetcontentEncoding(const W3CNMTOKENSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate contentEncoding attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontentEncoding();

	/*
	 * Gets or creates Dc1NodePtr child elements of StatementType.
	 * Currently just supports rudimentary XPath expressions as StatementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafStatementType_ExtMyMethodDef.h


public:

	virtual ~MpafStatementType();

protected:
	MpafStatementType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_mimeType;
	XMLCh * m_ref;
	bool m_ref_Exist;
	XMLCh * m_encoding;
	bool m_encoding_Exist;
	W3CNMTOKENSPtr m_contentEncoding;
	bool m_contentEncoding_Exist;

	// Base Class
	Dc1Ptr< Mpeg21StatementType > m_Base;


// no includefile for extension defined 
// file MpafStatementType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _947MPAFSTATEMENTTYPE_H

