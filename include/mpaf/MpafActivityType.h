/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _655MPAFACTIVITYTYPE_H
#define _655MPAFACTIVITYTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"
#include "MpafFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "MpafActivityTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafActivityType_ExtInclude.h


class IMpafMPAFBaseType;
typedef Dc1Ptr< IMpafMPAFBaseType > MpafMPAFBasePtr;
class IMpafActivityType;
typedef Dc1Ptr< IMpafActivityType> MpafActivityPtr;
class IW3CdateTime;
typedef Dc1Ptr< IW3CdateTime > W3CdateTimePtr;
class IMpafActivityType_Descriptor_CollectionType;
typedef Dc1Ptr< IMpafActivityType_Descriptor_CollectionType > MpafActivityType_Descriptor_CollectionPtr;
class IMpafActivityType_Parameters_LocalType;
typedef Dc1Ptr< IMpafActivityType_Parameters_LocalType > MpafActivityType_Parameters_LocalPtr;
class IMpafActivityType_Activity_CollectionType;
typedef Dc1Ptr< IMpafActivityType_Activity_CollectionType > MpafActivityType_Activity_CollectionPtr;
class IMpafActivityType_Operator_CollectionType;
typedef Dc1Ptr< IMpafActivityType_Operator_CollectionType > MpafActivityType_Operator_CollectionPtr;
class IMpafActivityType_Content_CollectionType0;
typedef Dc1Ptr< IMpafActivityType_Content_CollectionType0 > MpafActivityType_Content_Collection0Ptr;
class IMp7JrsPlaceType;
typedef Dc1Ptr< IMp7JrsPlaceType > Mp7JrsPlacePtr;
#include "MpafMPAFBaseType.h"

/** 
 * Generated interface IMpafActivityType for class MpafActivityType<br>
 * Located at: mpaf.xsd, line 728<br>
 * Classified: Class<br>
 * Derived from: MPAFBaseType (Class)<br>
 */
class MPAF_EXPORT IMpafActivityType 
 :
		public IMpafMPAFBaseType
{
public:
	// TODO: make these protected?
	IMpafActivityType();
	virtual ~IMpafActivityType();
	/**
	 * Get start attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr Getstart() const = 0;

	/**
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const = 0;

	/**
	 * Set start attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void Setstart(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get end attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr Getend() const = 0;

	/**
	 * Get end attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existend() const = 0;

	/**
	 * Set end attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void Setend(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate end attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateend() = 0;

	/**
	 * Get type attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const = 0;

	/**
	 * Set type attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get executed attribute.
	 * @return bool
	 */
	virtual bool Getexecuted() const = 0;

	/**
	 * Get executed attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existexecuted() const = 0;

	/**
	 * Set executed attribute.
	 * @param item bool
	 */
	virtual void Setexecuted(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate executed attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateexecuted() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Descriptor element.
	 * @return MpafActivityType_Descriptor_CollectionPtr
	 */
	virtual MpafActivityType_Descriptor_CollectionPtr GetDescriptor() const = 0;

	/**
	 * Set Descriptor element.
	 * @param item const MpafActivityType_Descriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescriptor(const MpafActivityType_Descriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Parameters element.
	 * @return MpafActivityType_Parameters_LocalPtr
	 */
	virtual MpafActivityType_Parameters_LocalPtr GetParameters() const = 0;

	virtual bool IsValidParameters() const = 0;

	/**
	 * Set Parameters element.
	 * @param item const MpafActivityType_Parameters_LocalPtr &
	 */
	virtual void SetParameters(const MpafActivityType_Parameters_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Parameters element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateParameters() = 0;

	/**
	 * Get Activity element.
	 * @return MpafActivityType_Activity_CollectionPtr
	 */
	virtual MpafActivityType_Activity_CollectionPtr GetActivity() const = 0;

	/**
	 * Set Activity element.
	 * @param item const MpafActivityType_Activity_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetActivity(const MpafActivityType_Activity_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Operator element.
	 * @return MpafActivityType_Operator_CollectionPtr
	 */
	virtual MpafActivityType_Operator_CollectionPtr GetOperator() const = 0;

	/**
	 * Set Operator element.
	 * @param item const MpafActivityType_Operator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetOperator(const MpafActivityType_Operator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Content element.
	 * @return MpafActivityType_Content_Collection0Ptr
	 */
	virtual MpafActivityType_Content_Collection0Ptr GetContent() const = 0;

	/**
	 * Set Content element.
	 * @param item const MpafActivityType_Content_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetContent(const MpafActivityType_Content_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Location element.
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetLocation() const = 0;

	virtual bool IsValidLocation() const = 0;

	/**
	 * Set Location element.
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Location element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLocation() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from MpafMPAFBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from MpafMPAFBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get uri attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Geturi() const = 0;

	/**
	 * Set uri attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Seturi(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of ActivityType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Descriptor</li>
	 * <li>Parameters</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Activity</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Operator</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Content</li>
	 * <li>Location</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafActivityType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafActivityType_ExtPropDef.h

};




/*
 * Generated class MpafActivityType<br>
 * Located at: mpaf.xsd, line 728<br>
 * Classified: Class<br>
 * Derived from: MPAFBaseType (Class)<br>
 * Defined in mpaf.xsd, line 728.<br>
 */
class DC1_EXPORT MpafActivityType :
		public IMpafActivityType
{
	friend class MpafActivityTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of MpafActivityType
	 
	 * @return Dc1Ptr< MpafMPAFBaseType >
	 */
	virtual Dc1Ptr< MpafMPAFBaseType > GetBase() const;
	/*
	 * Get start attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr Getstart() const;

	/*
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const;

	/*
	 * Set start attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void Setstart(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get end attribute.
	 * @return W3CdateTimePtr
	 */
	virtual W3CdateTimePtr Getend() const;

	/*
	 * Get end attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existend() const;

	/*
	 * Set end attribute.
	 * @param item const W3CdateTimePtr &
	 */
	virtual void Setend(const W3CdateTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate end attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateend();

	/*
	 * Get type attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const;

	/*
	 * Set type attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get executed attribute.
	 * @return bool
	 */
	virtual bool Getexecuted() const;

	/*
	 * Get executed attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existexecuted() const;

	/*
	 * Set executed attribute.
	 * @param item bool
	 */
	virtual void Setexecuted(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate executed attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateexecuted();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Descriptor element.
	 * @return MpafActivityType_Descriptor_CollectionPtr
	 */
	virtual MpafActivityType_Descriptor_CollectionPtr GetDescriptor() const;

	/*
	 * Set Descriptor element.
	 * @param item const MpafActivityType_Descriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescriptor(const MpafActivityType_Descriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Parameters element.
	 * @return MpafActivityType_Parameters_LocalPtr
	 */
	virtual MpafActivityType_Parameters_LocalPtr GetParameters() const;

	virtual bool IsValidParameters() const;

	/*
	 * Set Parameters element.
	 * @param item const MpafActivityType_Parameters_LocalPtr &
	 */
	virtual void SetParameters(const MpafActivityType_Parameters_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Parameters element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateParameters();

	/*
	 * Get Activity element.
	 * @return MpafActivityType_Activity_CollectionPtr
	 */
	virtual MpafActivityType_Activity_CollectionPtr GetActivity() const;

	/*
	 * Set Activity element.
	 * @param item const MpafActivityType_Activity_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetActivity(const MpafActivityType_Activity_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Operator element.
	 * @return MpafActivityType_Operator_CollectionPtr
	 */
	virtual MpafActivityType_Operator_CollectionPtr GetOperator() const;

	/*
	 * Set Operator element.
	 * @param item const MpafActivityType_Operator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetOperator(const MpafActivityType_Operator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Content element.
	 * @return MpafActivityType_Content_Collection0Ptr
	 */
	virtual MpafActivityType_Content_Collection0Ptr GetContent() const;

	/*
	 * Set Content element.
	 * @param item const MpafActivityType_Content_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetContent(const MpafActivityType_Content_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Location element.
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetLocation() const;

	virtual bool IsValidLocation() const;

	/*
	 * Set Location element.
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Location element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLocation();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from MpafMPAFBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from MpafMPAFBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get uri attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Geturi() const;

	/*
	 * Set uri attribute.
<br>
	 * (Inherited from MpafMPAFBaseType)
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Seturi(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Gets or creates Dc1NodePtr child elements of ActivityType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Descriptor</li>
	 * <li>Parameters</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Activity</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Operator</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Content</li>
	 * <li>Location</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafActivityType_ExtMyMethodDef.h


public:

	virtual ~MpafActivityType();

protected:
	MpafActivityType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	W3CdateTimePtr m_start;
	bool m_start_Exist;
	W3CdateTimePtr m_end;
	bool m_end_Exist;
	XMLCh * m_type;
	bool m_executed;
	bool m_executed_Exist;
	bool m_executed_Default;

	// Base Class
	Dc1Ptr< MpafMPAFBaseType > m_Base;

	MpafActivityType_Descriptor_CollectionPtr m_Descriptor;
	MpafActivityType_Parameters_LocalPtr m_Parameters;
	bool m_Parameters_Exist; // For optional elements 
	MpafActivityType_Activity_CollectionPtr m_Activity;
	MpafActivityType_Operator_CollectionPtr m_Operator;
	MpafActivityType_Content_Collection0Ptr m_Content;
	Mp7JrsPlacePtr m_Location;
	bool m_Location_Exist; // For optional elements 

// no includefile for extension defined 
// file MpafActivityType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _655MPAFACTIVITYTYPE_H

