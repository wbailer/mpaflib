/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Include section of Dc1Factory.h
#include "Dc1UnknownFactory.h" // Handles unknown xsi:type elements

#include "MpafActivityTypeFactory.h"
#include "MpafActivityType_Activity_CollectionTypeFactory.h"
#include "MpafActivityType_Content_CollectionTypeFactory.h"
#include "MpafActivityType_Content_CollectionType0Factory.h"
#include "MpafActivityType_Content_LocalTypeFactory.h"
#include "MpafActivityType_Content_LocalType0Factory.h"
#include "MpafActivityType_Descriptor_CollectionTypeFactory.h"
#include "MpafActivityType_Operator_CollectionTypeFactory.h"
#include "MpafActivityType_Parameters_LocalTypeFactory.h"
#include "MpafActivityType_Parameters_Param_CollectionTypeFactory.h"
#include "MpafActivityType_Parameters_Param_LocalTypeFactory.h"
#include "MpafAgentTypeFactory.h"
#include "MpafAnnotationDescriptorTypeFactory.h"
#include "MpafAnnotationStatementTypeFactory.h"
#include "MpafAssetTypeFactory.h"
#include "MpafAssetType_PreservationObjectType_Choice_CollectionTypeFactory.h"
#include "MpafAssetType_PreservationObjectType_Condition_CollectionTypeFactory.h"
#include "MpafAssetType_PreservationObjectType_Descriptor_CollectionTypeFactory.h"
#include "MpafAssetType_PreservationObjectType_Item_CollectionTypeFactory.h"
#include "MpafAuthenticityDescriptorTypeFactory.h"
#include "MpafAuthenticityStatementTypeFactory.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_CollectionTypeFactory.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalTypeFactory.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_LocalTypeFactory.h"
#include "MpafAuthenticityStatementType_CheckList_LocalTypeFactory.h"
#include "MpafAuthenticityStatementType_CheckList_Operator_CollectionTypeFactory.h"
#include "MpafBitstreamTypeFactory.h"
#include "MpafCELRightsStatementTypeFactory.h"
#include "MpafComponentTypeFactory.h"
#include "MpafComponentType_Condition_CollectionTypeFactory.h"
#include "MpafComponentType_Descriptor_CollectionTypeFactory.h"
#include "MpafComponentType_Resource_CollectionTypeFactory.h"
#include "MpafContextDescriptorTypeFactory.h"
#include "MpafContextStatementTypeFactory.h"
#include "MpafContextStatementType_Reference_CollectionTypeFactory.h"
#include "MpafContextStatementType_Reference_Description_CollectionTypeFactory.h"
#include "MpafContextStatementType_Reference_Label_CollectionTypeFactory.h"
#include "MpafContextStatementType_Reference_LocalTypeFactory.h"
#include "MpafContextStatementType_Reference_Relation_CollectionTypeFactory.h"
#include "MpafDerivationRelationStatementTypeFactory.h"
#include "MpafDescriptiveMetadataDescriptorTypeFactory.h"
#include "MpafDescriptiveMetadataStatementTypeFactory.h"
#include "MpafDescriptorBaseTypeFactory.h"
#include "MpafDescriptorBaseType_Condition_CollectionTypeFactory.h"
#include "MpafDescriptorBaseType_Descriptor_CollectionTypeFactory.h"
#include "MpafDescriptorTypeFactory.h"
#include "MpafDublinCoreDMStatementTypeFactory.h"
#include "MpafEBUCoreRDFDMStatementTypeFactory.h"
#include "MpafEBUCoreTMStatementTypeFactory.h"
#include "MpafEBUCoreXMLDMStatementTypeFactory.h"
#include "MpafEssenceTypeFactory.h"
#include "MpafEssenceType_Choice_CollectionTypeFactory.h"
#include "MpafEssenceType_Component_CollectionTypeFactory.h"
#include "MpafEssenceType_Condition_CollectionTypeFactory.h"
#include "MpafEssenceType_Descriptor_CollectionTypeFactory.h"
#include "MpafExploitationRightsDescriptorTypeFactory.h"
#include "MpafFileTypeFactory.h"
#include "MpafFixityCheckTypeFactory.h"
#include "MpafFixityDescriptorTypeFactory.h"
#include "MpafFixityStatementTypeFactory.h"
#include "MpafFixityStatementType_Check_CollectionTypeFactory.h"
#include "MpafGroupTypeFactory.h"
#include "MpafGroupType_CollectionTypeFactory.h"
#include "MpafGroupType_Descriptor_CollectionTypeFactory.h"
#include "MpafGroupType_LocalTypeFactory.h"
#include "MpafIdentificationDescriptorTypeFactory.h"
#include "MpafIdentificationStatementTypeFactory.h"
#include "MpafIdentificationStatementType_Identifier_CollectionTypeFactory.h"
#include "MpafIdentifierBaseTypeFactory.h"
#include "MpafIdentifierTypeFactory.h"
#include "MpafIntegrityDescriptorTypeFactory.h"
#include "MpafIntegrityStatementTypeFactory.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionTypeFactory.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalTypeFactory.h"
#include "MpafIntegrityStatementType_CheckList_Entity_CollectionTypeFactory.h"
#include "MpafIntegrityStatementType_CheckList_LocalTypeFactory.h"
#include "MpafIntegrityStatementType_ExternalDependency_CollectionTypeFactory.h"
#include "MpafItemBaseTypeFactory.h"
#include "MpafItemRefBaseTypeFactory.h"
#include "MpafItemRefTypeFactory.h"
#include "MpafItemTypeFactory.h"
#include "MpafMCORightsStatementTypeFactory.h"
#include "MpafMediaLocatorTypeFactory.h"
#include "MpafMPAFBaseTypeFactory.h"
#include "MpafMPEG7DMStatementTypeFactory.h"
#include "MpafMPEG7DMStatementType_CollectionTypeFactory.h"
#include "MpafMPEG7DMStatementType_LocalTypeFactory.h"
#include "MpafMPEG7TMStatementTypeFactory.h"
#include "MpafOperatorRefTypeFactory.h"
#include "MpafOperatorTypeFactory.h"
#include "MpafOperatorType_Descriptor_CollectionTypeFactory.h"
#include "MpafOrganizationTypeFactory.h"
#include "MpafOrganizationType_Contact_CollectionTypeFactory.h"
#include "MpafOrganizationType_Name_CollectionType0Factory.h"
#include "MpafOrganizationType_Name_LocalType0Factory.h"
#include "MpafOrganizationType_NameTerm_CollectionType0Factory.h"
#include "MpafOrganizationType_NameTerm_LocalType0Factory.h"
#include "MpafPersonGroupTypeFactory.h"
#include "MpafPersonGroupType_Member_CollectionTypeFactory.h"
#include "MpafPersonGroupType_Name_CollectionType0Factory.h"
#include "MpafPersonGroupType_Name_LocalType0Factory.h"
#include "MpafPersonGroupType_NameTerm_CollectionType0Factory.h"
#include "MpafPersonGroupType_NameTerm_LocalType0Factory.h"
#include "MpafPersonTypeFactory.h"
#include "MpafPersonType_Affiliation_CollectionType0Factory.h"
#include "MpafPersonType_Affiliation_LocalType0Factory.h"
#include "MpafPersonType_Citizenship_CollectionType0Factory.h"
#include "MpafPersonType_CollectionType0Factory.h"
#include "MpafPersonType_ElectronicAddress_CollectionType0Factory.h"
#include "MpafPersonType_LocalType0Factory.h"
#include "MpafPreservationMetadataTypeFactory.h"
#include "MpafPreservationMetadataType_DIDLInfo_LocalTypeFactory.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionTypeFactory.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalTypeFactory.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0Factory.h"
#include "MpafPreservationObjectTypeFactory.h"
#include "MpafPreservationObjectType_Choice_CollectionTypeFactory.h"
#include "MpafPreservationObjectType_Condition_CollectionTypeFactory.h"
#include "MpafPreservationObjectType_Descriptor_CollectionTypeFactory.h"
#include "MpafPreservationObjectType_Item_CollectionTypeFactory.h"
#include "MpafProvenanceDescriptorTypeFactory.h"
#include "MpafProvenanceStatementTypeFactory.h"
#include "MpafProvenanceStatementType_Activity_CollectionTypeFactory.h"
#include "MpafProvenanceStatementType_Operator_CollectionTypeFactory.h"
#include "MpafQualityDescriptorTypeFactory.h"
#include "MpafQualityStatementTypeFactory.h"
#include "MpafQualityStatementType_CollectionTypeFactory.h"
#include "MpafQualityStatementType_LocalTypeFactory.h"
#include "MpafReferenceDescriptorTypeFactory.h"
#include "MpafRelatedEntityTypeFactory.h"
#include "MpafRelationStatementTypeFactory.h"
#include "MpafRELRightsStatementTypeFactory.h"
#include "MpafRepresentationTypeFactory.h"
#include "MpafRepresentationType_Choice_CollectionTypeFactory.h"
#include "MpafRepresentationType_Condition_CollectionTypeFactory.h"
#include "MpafRepresentationType_Descriptor_CollectionTypeFactory.h"
#include "MpafRepresentationType_Item_CollectionTypeFactory.h"
#include "MpafResourceBaseTypeFactory.h"
#include "MpafRightsDescriptorTypeFactory.h"
#include "MpafRightsStatementTypeFactory.h"
#include "MpafSegmentFixityCheckTypeFactory.h"
#include "MpafStatementTypeFactory.h"
#include "MpafTechnicalMetadataDescriptorTypeFactory.h"
#include "MpafTechnicalMetadataStatementTypeFactory.h"
#include "MpafToolTypeFactory.h"
#include "MpafToolType_Manufacturer_CollectionTypeFactory.h"
#include "MpafToolType_Name_CollectionTypeFactory.h"
#include "MpafToolType_Parameters_LocalTypeFactory.h"
#include "MpafToolType_Parameters_Param_CollectionTypeFactory.h"
#include "MpafToolType_Parameters_Param_LocalTypeFactory.h"
#include "MpafUsageRightsDescriptorTypeFactory.h"

