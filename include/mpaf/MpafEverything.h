/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef MPAFEVERYTHING_INCLUDED_H
#define MPAFEVERYTHING_INCLUDED_H

#include "Dc1Defines.h"

#include "MpafActivityType.h"
#include "MpafActivityType_Activity_CollectionType.h"
#include "MpafActivityType_Content_CollectionType.h"
#include "MpafActivityType_Content_CollectionType0.h"
#include "MpafActivityType_Content_LocalType.h"
#include "MpafActivityType_Content_LocalType0.h"
#include "MpafActivityType_Content_relationType_LocalType.h"
#include "MpafActivityType_Descriptor_CollectionType.h"
#include "MpafActivityType_Operator_CollectionType.h"
#include "MpafActivityType_Parameters_LocalType.h"
#include "MpafActivityType_Parameters_Param_CollectionType.h"
#include "MpafActivityType_Parameters_Param_LocalType.h"
#include "MpafAgentType.h"
#include "MpafAnnotationDescriptorType.h"
#include "MpafAnnotationStatementType.h"
#include "MpafAssetType.h"
#include "MpafAssetType_PreservationObjectType_Choice_CollectionType.h"
#include "MpafAssetType_PreservationObjectType_Condition_CollectionType.h"
#include "MpafAssetType_PreservationObjectType_Descriptor_CollectionType.h"
#include "MpafAssetType_PreservationObjectType_Item_CollectionType.h"
#include "MpafAuthenticityDescriptorType.h"
#include "MpafAuthenticityStatementType.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_CollectionType.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType.h"
#include "MpafAuthenticityStatementType_CheckList_Entity_LocalType.h"
#include "MpafAuthenticityStatementType_CheckList_LocalType.h"
#include "MpafAuthenticityStatementType_CheckList_Operator_CollectionType.h"
#include "MpafBitstreamType.h"
#include "MpafCELRightsStatementType.h"
#include "MpafComponentType.h"
#include "MpafComponentType_Condition_CollectionType.h"
#include "MpafComponentType_Descriptor_CollectionType.h"
#include "MpafComponentType_Resource_CollectionType.h"
#include "MpafContextDescriptorType.h"
#include "MpafContextStatementType.h"
#include "MpafContextStatementType_Reference_CollectionType.h"
#include "MpafContextStatementType_Reference_Description_CollectionType.h"
#include "MpafContextStatementType_Reference_Label_CollectionType.h"
#include "MpafContextStatementType_Reference_LocalType.h"
#include "MpafContextStatementType_Reference_Relation_CollectionType.h"
#include "MpafDerivationRelationStatementType.h"
#include "MpafDescriptiveMetadataDescriptorType.h"
#include "MpafDescriptiveMetadataStatementType.h"
#include "MpafDescriptorBaseType.h"
#include "MpafDescriptorBaseType_Condition_CollectionType.h"
#include "MpafDescriptorBaseType_Descriptor_CollectionType.h"
#include "MpafDescriptorType.h"
#include "MpafDublinCoreDMStatementType.h"
#include "MpafEBUCoreRDFDMStatementType.h"
#include "MpafEBUCoreTMStatementType.h"
#include "MpafEBUCoreXMLDMStatementType.h"
#include "MpafEssenceType.h"
#include "MpafEssenceType_Choice_CollectionType.h"
#include "MpafEssenceType_Component_CollectionType.h"
#include "MpafEssenceType_Condition_CollectionType.h"
#include "MpafEssenceType_Descriptor_CollectionType.h"
#include "MpafExploitationRightsDescriptorType.h"
#include "MpafFileType.h"
#include "MpafFixityCheckType.h"
#include "MpafFixityDescriptorType.h"
#include "MpafFixityStatementType.h"
#include "MpafFixityStatementType_Check_CollectionType.h"
#include "MpafGroupType.h"
#include "MpafGroupType_CollectionType.h"
#include "MpafGroupType_Descriptor_CollectionType.h"
#include "MpafGroupType_LocalType.h"
#include "MpafIdentificationDescriptorType.h"
#include "MpafIdentificationStatementType.h"
#include "MpafIdentificationStatementType_Identifier_CollectionType.h"
#include "MpafIdentifierBaseType.h"
#include "MpafIdentifierType.h"
#include "MpafIntegrityDescriptorType.h"
#include "MpafIntegrityStatementType.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType.h"
#include "MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType.h"
#include "MpafIntegrityStatementType_CheckList_Entity_CollectionType.h"
#include "MpafIntegrityStatementType_CheckList_LocalType.h"
#include "MpafIntegrityStatementType_ExternalDependency_CollectionType.h"
#include "MpafItemBaseType.h"
#include "MpafItemRefBaseType.h"
#include "MpafItemRefType.h"
#include "MpafItemType.h"
#include "MpafMCORightsStatementType.h"
#include "MpafMediaLocatorType.h"
#include "MpafMPAFBaseType.h"
#include "MpafMPEG7DMStatementType.h"
#include "MpafMPEG7DMStatementType_CollectionType.h"
#include "MpafMPEG7DMStatementType_LocalType.h"
#include "MpafMPEG7TMStatementType.h"
#include "MpafOperatorRefType.h"
#include "MpafOperatorType.h"
#include "MpafOperatorType_Descriptor_CollectionType.h"
#include "MpafOrganizationType.h"
#include "MpafOrganizationType_Contact_CollectionType.h"
#include "MpafOrganizationType_Name_CollectionType0.h"
#include "MpafOrganizationType_Name_LocalType0.h"
#include "MpafOrganizationType_Name_type_LocalType0.h"
#include "MpafOrganizationType_NameTerm_CollectionType0.h"
#include "MpafOrganizationType_NameTerm_LocalType0.h"
#include "MpafOrganizationType_NameTerm_type_LocalType0.h"
#include "MpafPersonGroupType.h"
#include "MpafPersonGroupType_Member_CollectionType.h"
#include "MpafPersonGroupType_Name_CollectionType0.h"
#include "MpafPersonGroupType_Name_LocalType0.h"
#include "MpafPersonGroupType_Name_type_LocalType0.h"
#include "MpafPersonGroupType_NameTerm_CollectionType0.h"
#include "MpafPersonGroupType_NameTerm_LocalType0.h"
#include "MpafPersonGroupType_NameTerm_type_LocalType0.h"
#include "MpafPersonType.h"
#include "MpafPersonType_Affiliation_CollectionType0.h"
#include "MpafPersonType_Affiliation_LocalType0.h"
#include "MpafPersonType_Citizenship_CollectionType0.h"
#include "MpafPersonType_CollectionType0.h"
#include "MpafPersonType_ElectronicAddress_CollectionType0.h"
#include "MpafPersonType_LocalType0.h"
#include "MpafPreservationMetadataType.h"
#include "MpafPreservationMetadataType_DIDLInfo_LocalType.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType.h"
#include "MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0.h"
#include "MpafPreservationObjectType.h"
#include "MpafPreservationObjectType_Choice_CollectionType.h"
#include "MpafPreservationObjectType_Condition_CollectionType.h"
#include "MpafPreservationObjectType_Descriptor_CollectionType.h"
#include "MpafPreservationObjectType_Item_CollectionType.h"
#include "MpafProvenanceDescriptorType.h"
#include "MpafProvenanceStatementType.h"
#include "MpafProvenanceStatementType_Activity_CollectionType.h"
#include "MpafProvenanceStatementType_Operator_CollectionType.h"
#include "MpafQualityDescriptorType.h"
#include "MpafQualityStatementType.h"
#include "MpafQualityStatementType_CollectionType.h"
#include "MpafQualityStatementType_LocalType.h"
#include "MpafReferenceDescriptorType.h"
#include "MpafRelatedEntityType.h"
#include "MpafRelationStatementType.h"
#include "MpafRELRightsStatementType.h"
#include "MpafRepresentationType.h"
#include "MpafRepresentationType_Choice_CollectionType.h"
#include "MpafRepresentationType_Condition_CollectionType.h"
#include "MpafRepresentationType_Descriptor_CollectionType.h"
#include "MpafRepresentationType_Item_CollectionType.h"
#include "MpafResourceBaseType.h"
#include "MpafRightsDescriptorType.h"
#include "MpafRightsStatementType.h"
#include "MpafSegmentFixityCheckType.h"
#include "MpafStatementType.h"
#include "MpafTechnicalMetadataDescriptorType.h"
#include "MpafTechnicalMetadataStatementType.h"
#include "MpafToolType.h"
#include "MpafToolType_Manufacturer_CollectionType.h"
#include "MpafToolType_Name_CollectionType.h"
#include "MpafToolType_Parameters_LocalType.h"
#include "MpafToolType_Parameters_Param_CollectionType.h"
#include "MpafToolType_Parameters_Param_LocalType.h"
#include "MpafUsageRightsDescriptorType.h"

#endif // MPAFEVERYTHING_INCLUDED_H
