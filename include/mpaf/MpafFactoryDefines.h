/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef MPAFFACTORYDEFINES_H
#define MPAFFACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for extension classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfActivityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType")))
#define CreateActivityType (Dc1Factory::CreateObject(TypeOfActivityType))
class MpafActivityType;
class IMpafActivityType;
/** Smart pointer for instance of IMpafActivityType */
typedef Dc1Ptr< IMpafActivityType > MpafActivityPtr;

#define TypeOfActivityType_Activity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Activity_CollectionType")))
#define CreateActivityType_Activity_CollectionType (Dc1Factory::CreateObject(TypeOfActivityType_Activity_CollectionType))
class MpafActivityType_Activity_CollectionType;
class IMpafActivityType_Activity_CollectionType;
/** Smart pointer for instance of IMpafActivityType_Activity_CollectionType */
typedef Dc1Ptr< IMpafActivityType_Activity_CollectionType > MpafActivityType_Activity_CollectionPtr;

#define TypeOfActivityType_Content_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_CollectionType")))
#define CreateActivityType_Content_CollectionType (Dc1Factory::CreateObject(TypeOfActivityType_Content_CollectionType))
class MpafActivityType_Content_CollectionType;
class IMpafActivityType_Content_CollectionType;
/** Smart pointer for instance of IMpafActivityType_Content_CollectionType */
typedef Dc1Ptr< IMpafActivityType_Content_CollectionType > MpafActivityType_Content_CollectionPtr;

#define TypeOfActivityType_Content_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_CollectionType0")))
#define CreateActivityType_Content_CollectionType0 (Dc1Factory::CreateObject(TypeOfActivityType_Content_CollectionType0))
class MpafActivityType_Content_CollectionType0;
class IMpafActivityType_Content_CollectionType0;
/** Smart pointer for instance of IMpafActivityType_Content_CollectionType0 */
typedef Dc1Ptr< IMpafActivityType_Content_CollectionType0 > MpafActivityType_Content_Collection0Ptr;

#define TypeOfActivityType_Content_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_LocalType")))
#define CreateActivityType_Content_LocalType (Dc1Factory::CreateObject(TypeOfActivityType_Content_LocalType))
class MpafActivityType_Content_LocalType;
class IMpafActivityType_Content_LocalType;
/** Smart pointer for instance of IMpafActivityType_Content_LocalType */
typedef Dc1Ptr< IMpafActivityType_Content_LocalType > MpafActivityType_Content_LocalPtr;

#define TypeOfActivityType_Content_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_LocalType0")))
#define CreateActivityType_Content_LocalType0 (Dc1Factory::CreateObject(TypeOfActivityType_Content_LocalType0))
class MpafActivityType_Content_LocalType0;
class IMpafActivityType_Content_LocalType0;
/** Smart pointer for instance of IMpafActivityType_Content_LocalType0 */
typedef Dc1Ptr< IMpafActivityType_Content_LocalType0 > MpafActivityType_Content_Local0Ptr;

#define TypeOfActivityType_Content_relationType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Content_relationType_LocalType")))
#define CreateActivityType_Content_relationType_LocalType (Dc1Factory::CreateObject(TypeOfActivityType_Content_relationType_LocalType))
class MpafActivityType_Content_relationType_LocalType;
class IMpafActivityType_Content_relationType_LocalType;
// No smart pointer instance for IMpafActivityType_Content_relationType_LocalType

#define TypeOfActivityType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Descriptor_CollectionType")))
#define CreateActivityType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfActivityType_Descriptor_CollectionType))
class MpafActivityType_Descriptor_CollectionType;
class IMpafActivityType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafActivityType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafActivityType_Descriptor_CollectionType > MpafActivityType_Descriptor_CollectionPtr;

#define TypeOfActivityType_Operator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Operator_CollectionType")))
#define CreateActivityType_Operator_CollectionType (Dc1Factory::CreateObject(TypeOfActivityType_Operator_CollectionType))
class MpafActivityType_Operator_CollectionType;
class IMpafActivityType_Operator_CollectionType;
/** Smart pointer for instance of IMpafActivityType_Operator_CollectionType */
typedef Dc1Ptr< IMpafActivityType_Operator_CollectionType > MpafActivityType_Operator_CollectionPtr;

#define TypeOfActivityType_Parameters_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_LocalType")))
#define CreateActivityType_Parameters_LocalType (Dc1Factory::CreateObject(TypeOfActivityType_Parameters_LocalType))
class MpafActivityType_Parameters_LocalType;
class IMpafActivityType_Parameters_LocalType;
/** Smart pointer for instance of IMpafActivityType_Parameters_LocalType */
typedef Dc1Ptr< IMpafActivityType_Parameters_LocalType > MpafActivityType_Parameters_LocalPtr;

#define TypeOfActivityType_Parameters_Param_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_Param_CollectionType")))
#define CreateActivityType_Parameters_Param_CollectionType (Dc1Factory::CreateObject(TypeOfActivityType_Parameters_Param_CollectionType))
class MpafActivityType_Parameters_Param_CollectionType;
class IMpafActivityType_Parameters_Param_CollectionType;
/** Smart pointer for instance of IMpafActivityType_Parameters_Param_CollectionType */
typedef Dc1Ptr< IMpafActivityType_Parameters_Param_CollectionType > MpafActivityType_Parameters_Param_CollectionPtr;

#define TypeOfActivityType_Parameters_Param_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ActivityType_Parameters_Param_LocalType")))
#define CreateActivityType_Parameters_Param_LocalType (Dc1Factory::CreateObject(TypeOfActivityType_Parameters_Param_LocalType))
class MpafActivityType_Parameters_Param_LocalType;
class IMpafActivityType_Parameters_Param_LocalType;
/** Smart pointer for instance of IMpafActivityType_Parameters_Param_LocalType */
typedef Dc1Ptr< IMpafActivityType_Parameters_Param_LocalType > MpafActivityType_Parameters_Param_LocalPtr;

// This is special, since there is an mpeg-7 type with the same name
#define TypeOfMpafAgentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AgentType")))
#define CreateMpafAgentType (Dc1Factory::CreateObject(TypeOfMpafAgentType))
class MpafAgentType;
class IMpafAgentType;
/** Smart pointer to instance of IMpafAgentType */
typedef Dc1Ptr< IMpafAgentType > MpafAgentPtr;

#define TypeOfAnnotationDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AnnotationDescriptorType")))
#define CreateAnnotationDescriptorType (Dc1Factory::CreateObject(TypeOfAnnotationDescriptorType))
class MpafAnnotationDescriptorType;
class IMpafAnnotationDescriptorType;
/** Smart pointer for instance of IMpafAnnotationDescriptorType */
typedef Dc1Ptr< IMpafAnnotationDescriptorType > MpafAnnotationDescriptorPtr;

#define TypeOfAnnotationStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AnnotationStatementType")))
#define CreateAnnotationStatementType (Dc1Factory::CreateObject(TypeOfAnnotationStatementType))
class MpafAnnotationStatementType;
class IMpafAnnotationStatementType;
/** Smart pointer for instance of IMpafAnnotationStatementType */
typedef Dc1Ptr< IMpafAnnotationStatementType > MpafAnnotationStatementPtr;

#define TypeOfAssetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AssetType")))
#define CreateAssetType (Dc1Factory::CreateObject(TypeOfAssetType))
class MpafAssetType;
class IMpafAssetType;
/** Smart pointer for instance of IMpafAssetType */
typedef Dc1Ptr< IMpafAssetType > MpafAssetPtr;

#define TypeOfAssetType_PreservationObjectType_Choice_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Choice_CollectionType")))
#define CreateAssetType_PreservationObjectType_Choice_CollectionType (Dc1Factory::CreateObject(TypeOfAssetType_PreservationObjectType_Choice_CollectionType))
class MpafAssetType_PreservationObjectType_Choice_CollectionType;
class IMpafAssetType_PreservationObjectType_Choice_CollectionType;
/** Smart pointer for instance of IMpafAssetType_PreservationObjectType_Choice_CollectionType */
typedef Dc1Ptr< IMpafAssetType_PreservationObjectType_Choice_CollectionType > MpafAssetType_PreservationObjectType_Choice_CollectionPtr;

#define TypeOfAssetType_PreservationObjectType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Condition_CollectionType")))
#define CreateAssetType_PreservationObjectType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfAssetType_PreservationObjectType_Condition_CollectionType))
class MpafAssetType_PreservationObjectType_Condition_CollectionType;
class IMpafAssetType_PreservationObjectType_Condition_CollectionType;
/** Smart pointer for instance of IMpafAssetType_PreservationObjectType_Condition_CollectionType */
typedef Dc1Ptr< IMpafAssetType_PreservationObjectType_Condition_CollectionType > MpafAssetType_PreservationObjectType_Condition_CollectionPtr;

#define TypeOfAssetType_PreservationObjectType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Descriptor_CollectionType")))
#define CreateAssetType_PreservationObjectType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfAssetType_PreservationObjectType_Descriptor_CollectionType))
class MpafAssetType_PreservationObjectType_Descriptor_CollectionType;
class IMpafAssetType_PreservationObjectType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafAssetType_PreservationObjectType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafAssetType_PreservationObjectType_Descriptor_CollectionType > MpafAssetType_PreservationObjectType_Descriptor_CollectionPtr;

#define TypeOfAssetType_PreservationObjectType_Item_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AssetType_PreservationObjectType_Item_CollectionType")))
#define CreateAssetType_PreservationObjectType_Item_CollectionType (Dc1Factory::CreateObject(TypeOfAssetType_PreservationObjectType_Item_CollectionType))
class MpafAssetType_PreservationObjectType_Item_CollectionType;
class IMpafAssetType_PreservationObjectType_Item_CollectionType;
/** Smart pointer for instance of IMpafAssetType_PreservationObjectType_Item_CollectionType */
typedef Dc1Ptr< IMpafAssetType_PreservationObjectType_Item_CollectionType > MpafAssetType_PreservationObjectType_Item_CollectionPtr;

#define TypeOfAuthenticityDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityDescriptorType")))
#define CreateAuthenticityDescriptorType (Dc1Factory::CreateObject(TypeOfAuthenticityDescriptorType))
class MpafAuthenticityDescriptorType;
class IMpafAuthenticityDescriptorType;
/** Smart pointer for instance of IMpafAuthenticityDescriptorType */
typedef Dc1Ptr< IMpafAuthenticityDescriptorType > MpafAuthenticityDescriptorPtr;

#define TypeOfAuthenticityStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType")))
#define CreateAuthenticityStatementType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType))
class MpafAuthenticityStatementType;
class IMpafAuthenticityStatementType;
/** Smart pointer for instance of IMpafAuthenticityStatementType */
typedef Dc1Ptr< IMpafAuthenticityStatementType > MpafAuthenticityStatementPtr;

#define TypeOfAuthenticityStatementType_CheckList_Entity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_CollectionType")))
#define CreateAuthenticityStatementType_CheckList_Entity_CollectionType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType_CheckList_Entity_CollectionType))
class MpafAuthenticityStatementType_CheckList_Entity_CollectionType;
class IMpafAuthenticityStatementType_CheckList_Entity_CollectionType;
/** Smart pointer for instance of IMpafAuthenticityStatementType_CheckList_Entity_CollectionType */
typedef Dc1Ptr< IMpafAuthenticityStatementType_CheckList_Entity_CollectionType > MpafAuthenticityStatementType_CheckList_Entity_CollectionPtr;

#define TypeOfAuthenticityStatementType_CheckList_Entity_Confidence_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_Confidence_LocalType")))
#define CreateAuthenticityStatementType_CheckList_Entity_Confidence_LocalType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType_CheckList_Entity_Confidence_LocalType))
class MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType;
class IMpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType;
/** Smart pointer for instance of IMpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType */
typedef Dc1Ptr< IMpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalType > MpafAuthenticityStatementType_CheckList_Entity_Confidence_LocalPtr;

#define TypeOfAuthenticityStatementType_CheckList_Entity_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Entity_LocalType")))
#define CreateAuthenticityStatementType_CheckList_Entity_LocalType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType_CheckList_Entity_LocalType))
class MpafAuthenticityStatementType_CheckList_Entity_LocalType;
class IMpafAuthenticityStatementType_CheckList_Entity_LocalType;
/** Smart pointer for instance of IMpafAuthenticityStatementType_CheckList_Entity_LocalType */
typedef Dc1Ptr< IMpafAuthenticityStatementType_CheckList_Entity_LocalType > MpafAuthenticityStatementType_CheckList_Entity_LocalPtr;

#define TypeOfAuthenticityStatementType_CheckList_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_LocalType")))
#define CreateAuthenticityStatementType_CheckList_LocalType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType_CheckList_LocalType))
class MpafAuthenticityStatementType_CheckList_LocalType;
class IMpafAuthenticityStatementType_CheckList_LocalType;
/** Smart pointer for instance of IMpafAuthenticityStatementType_CheckList_LocalType */
typedef Dc1Ptr< IMpafAuthenticityStatementType_CheckList_LocalType > MpafAuthenticityStatementType_CheckList_LocalPtr;

#define TypeOfAuthenticityStatementType_CheckList_Operator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:AuthenticityStatementType_CheckList_Operator_CollectionType")))
#define CreateAuthenticityStatementType_CheckList_Operator_CollectionType (Dc1Factory::CreateObject(TypeOfAuthenticityStatementType_CheckList_Operator_CollectionType))
class MpafAuthenticityStatementType_CheckList_Operator_CollectionType;
class IMpafAuthenticityStatementType_CheckList_Operator_CollectionType;
/** Smart pointer for instance of IMpafAuthenticityStatementType_CheckList_Operator_CollectionType */
typedef Dc1Ptr< IMpafAuthenticityStatementType_CheckList_Operator_CollectionType > MpafAuthenticityStatementType_CheckList_Operator_CollectionPtr;

#define TypeOfBitstreamType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:BitstreamType")))
#define CreateBitstreamType (Dc1Factory::CreateObject(TypeOfBitstreamType))
class MpafBitstreamType;
class IMpafBitstreamType;
/** Smart pointer for instance of IMpafBitstreamType */
typedef Dc1Ptr< IMpafBitstreamType > MpafBitstreamPtr;

#define TypeOfCELRightsStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:CELRightsStatementType")))
#define CreateCELRightsStatementType (Dc1Factory::CreateObject(TypeOfCELRightsStatementType))
class MpafCELRightsStatementType;
class IMpafCELRightsStatementType;
/** Smart pointer for instance of IMpafCELRightsStatementType */
typedef Dc1Ptr< IMpafCELRightsStatementType > MpafCELRightsStatementPtr;

// This is special, since it derives from didmodel complextype with the same name
#define TypeOfMpafComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ComponentType")))
#define CreateMpafComponentType (Dc1Factory::CreateObject(TypeOfMpafComponentType))
class MpafComponentType;
class IMpafComponentType;
/** Smart pointer to instance of IMpafComponentType */
typedef Dc1Ptr< IMpafComponentType > MpafComponentPtr;

#define TypeOfComponentType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ComponentType_Condition_CollectionType")))
#define CreateComponentType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfComponentType_Condition_CollectionType))
class MpafComponentType_Condition_CollectionType;
class IMpafComponentType_Condition_CollectionType;
/** Smart pointer for instance of IMpafComponentType_Condition_CollectionType */
typedef Dc1Ptr< IMpafComponentType_Condition_CollectionType > MpafComponentType_Condition_CollectionPtr;

#define TypeOfComponentType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ComponentType_Descriptor_CollectionType")))
#define CreateComponentType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfComponentType_Descriptor_CollectionType))
class MpafComponentType_Descriptor_CollectionType;
class IMpafComponentType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafComponentType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafComponentType_Descriptor_CollectionType > MpafComponentType_Descriptor_CollectionPtr;

#define TypeOfComponentType_Resource_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ComponentType_Resource_CollectionType")))
#define CreateComponentType_Resource_CollectionType (Dc1Factory::CreateObject(TypeOfComponentType_Resource_CollectionType))
class MpafComponentType_Resource_CollectionType;
class IMpafComponentType_Resource_CollectionType;
/** Smart pointer for instance of IMpafComponentType_Resource_CollectionType */
typedef Dc1Ptr< IMpafComponentType_Resource_CollectionType > MpafComponentType_Resource_CollectionPtr;

#define TypeOfContextDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextDescriptorType")))
#define CreateContextDescriptorType (Dc1Factory::CreateObject(TypeOfContextDescriptorType))
class MpafContextDescriptorType;
class IMpafContextDescriptorType;
/** Smart pointer for instance of IMpafContextDescriptorType */
typedef Dc1Ptr< IMpafContextDescriptorType > MpafContextDescriptorPtr;

#define TypeOfContextStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType")))
#define CreateContextStatementType (Dc1Factory::CreateObject(TypeOfContextStatementType))
class MpafContextStatementType;
class IMpafContextStatementType;
/** Smart pointer for instance of IMpafContextStatementType */
typedef Dc1Ptr< IMpafContextStatementType > MpafContextStatementPtr;

#define TypeOfContextStatementType_Reference_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_CollectionType")))
#define CreateContextStatementType_Reference_CollectionType (Dc1Factory::CreateObject(TypeOfContextStatementType_Reference_CollectionType))
class MpafContextStatementType_Reference_CollectionType;
class IMpafContextStatementType_Reference_CollectionType;
/** Smart pointer for instance of IMpafContextStatementType_Reference_CollectionType */
typedef Dc1Ptr< IMpafContextStatementType_Reference_CollectionType > MpafContextStatementType_Reference_CollectionPtr;

#define TypeOfContextStatementType_Reference_Description_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Description_CollectionType")))
#define CreateContextStatementType_Reference_Description_CollectionType (Dc1Factory::CreateObject(TypeOfContextStatementType_Reference_Description_CollectionType))
class MpafContextStatementType_Reference_Description_CollectionType;
class IMpafContextStatementType_Reference_Description_CollectionType;
/** Smart pointer for instance of IMpafContextStatementType_Reference_Description_CollectionType */
typedef Dc1Ptr< IMpafContextStatementType_Reference_Description_CollectionType > MpafContextStatementType_Reference_Description_CollectionPtr;

#define TypeOfContextStatementType_Reference_Label_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Label_CollectionType")))
#define CreateContextStatementType_Reference_Label_CollectionType (Dc1Factory::CreateObject(TypeOfContextStatementType_Reference_Label_CollectionType))
class MpafContextStatementType_Reference_Label_CollectionType;
class IMpafContextStatementType_Reference_Label_CollectionType;
/** Smart pointer for instance of IMpafContextStatementType_Reference_Label_CollectionType */
typedef Dc1Ptr< IMpafContextStatementType_Reference_Label_CollectionType > MpafContextStatementType_Reference_Label_CollectionPtr;

#define TypeOfContextStatementType_Reference_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_LocalType")))
#define CreateContextStatementType_Reference_LocalType (Dc1Factory::CreateObject(TypeOfContextStatementType_Reference_LocalType))
class MpafContextStatementType_Reference_LocalType;
class IMpafContextStatementType_Reference_LocalType;
/** Smart pointer for instance of IMpafContextStatementType_Reference_LocalType */
typedef Dc1Ptr< IMpafContextStatementType_Reference_LocalType > MpafContextStatementType_Reference_LocalPtr;

#define TypeOfContextStatementType_Reference_Relation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ContextStatementType_Reference_Relation_CollectionType")))
#define CreateContextStatementType_Reference_Relation_CollectionType (Dc1Factory::CreateObject(TypeOfContextStatementType_Reference_Relation_CollectionType))
class MpafContextStatementType_Reference_Relation_CollectionType;
class IMpafContextStatementType_Reference_Relation_CollectionType;
/** Smart pointer for instance of IMpafContextStatementType_Reference_Relation_CollectionType */
typedef Dc1Ptr< IMpafContextStatementType_Reference_Relation_CollectionType > MpafContextStatementType_Reference_Relation_CollectionPtr;

#define TypeOfDerivationRelationStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DerivationRelationStatementType")))
#define CreateDerivationRelationStatementType (Dc1Factory::CreateObject(TypeOfDerivationRelationStatementType))
class MpafDerivationRelationStatementType;
class IMpafDerivationRelationStatementType;
/** Smart pointer for instance of IMpafDerivationRelationStatementType */
typedef Dc1Ptr< IMpafDerivationRelationStatementType > MpafDerivationRelationStatementPtr;

#define TypeOfDescriptiveMetadataDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptiveMetadataDescriptorType")))
#define CreateDescriptiveMetadataDescriptorType (Dc1Factory::CreateObject(TypeOfDescriptiveMetadataDescriptorType))
class MpafDescriptiveMetadataDescriptorType;
class IMpafDescriptiveMetadataDescriptorType;
/** Smart pointer for instance of IMpafDescriptiveMetadataDescriptorType */
typedef Dc1Ptr< IMpafDescriptiveMetadataDescriptorType > MpafDescriptiveMetadataDescriptorPtr;

#define TypeOfDescriptiveMetadataStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptiveMetadataStatementType")))
#define CreateDescriptiveMetadataStatementType (Dc1Factory::CreateObject(TypeOfDescriptiveMetadataStatementType))
class MpafDescriptiveMetadataStatementType;
class IMpafDescriptiveMetadataStatementType;
/** Smart pointer for instance of IMpafDescriptiveMetadataStatementType */
typedef Dc1Ptr< IMpafDescriptiveMetadataStatementType > MpafDescriptiveMetadataStatementPtr;

#define TypeOfDescriptorBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType")))
#define CreateDescriptorBaseType (Dc1Factory::CreateObject(TypeOfDescriptorBaseType))
class MpafDescriptorBaseType;
class IMpafDescriptorBaseType;
/** Smart pointer for instance of IMpafDescriptorBaseType */
typedef Dc1Ptr< IMpafDescriptorBaseType > MpafDescriptorBasePtr;

#define TypeOfDescriptorBaseType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType_Condition_CollectionType")))
#define CreateDescriptorBaseType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptorBaseType_Condition_CollectionType))
class MpafDescriptorBaseType_Condition_CollectionType;
class IMpafDescriptorBaseType_Condition_CollectionType;
/** Smart pointer for instance of IMpafDescriptorBaseType_Condition_CollectionType */
typedef Dc1Ptr< IMpafDescriptorBaseType_Condition_CollectionType > MpafDescriptorBaseType_Condition_CollectionPtr;

#define TypeOfDescriptorBaseType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptorBaseType_Descriptor_CollectionType")))
#define CreateDescriptorBaseType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptorBaseType_Descriptor_CollectionType))
class MpafDescriptorBaseType_Descriptor_CollectionType;
class IMpafDescriptorBaseType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafDescriptorBaseType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafDescriptorBaseType_Descriptor_CollectionType > MpafDescriptorBaseType_Descriptor_CollectionPtr;

// This is special, since there is an mpeg21 type with the same name
#define TypeOfMpafDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DescriptorType")))
#define CreateMpafDescriptorType (Dc1Factory::CreateObject(TypeOfMpafDescriptorType))
class MpafDescriptorType;
class IMpafDescriptorType;
/** Smart pointer to instance of IMpafDescriptorType */
typedef Dc1Ptr< IMpafDescriptorType > MpafDescriptorPtr;

#define TypeOfDublinCoreDMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:DublinCoreDMStatementType")))
#define CreateDublinCoreDMStatementType (Dc1Factory::CreateObject(TypeOfDublinCoreDMStatementType))
class MpafDublinCoreDMStatementType;
class IMpafDublinCoreDMStatementType;
/** Smart pointer for instance of IMpafDublinCoreDMStatementType */
typedef Dc1Ptr< IMpafDublinCoreDMStatementType > MpafDublinCoreDMStatementPtr;

#define TypeOfEBUCoreRDFDMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EBUCoreRDFDMStatementType")))
#define CreateEBUCoreRDFDMStatementType (Dc1Factory::CreateObject(TypeOfEBUCoreRDFDMStatementType))
class MpafEBUCoreRDFDMStatementType;
class IMpafEBUCoreRDFDMStatementType;
/** Smart pointer for instance of IMpafEBUCoreRDFDMStatementType */
typedef Dc1Ptr< IMpafEBUCoreRDFDMStatementType > MpafEBUCoreRDFDMStatementPtr;

#define TypeOfEBUCoreTMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EBUCoreTMStatementType")))
#define CreateEBUCoreTMStatementType (Dc1Factory::CreateObject(TypeOfEBUCoreTMStatementType))
class MpafEBUCoreTMStatementType;
class IMpafEBUCoreTMStatementType;
/** Smart pointer for instance of IMpafEBUCoreTMStatementType */
typedef Dc1Ptr< IMpafEBUCoreTMStatementType > MpafEBUCoreTMStatementPtr;

#define TypeOfEBUCoreXMLDMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EBUCoreXMLDMStatementType")))
#define CreateEBUCoreXMLDMStatementType (Dc1Factory::CreateObject(TypeOfEBUCoreXMLDMStatementType))
class MpafEBUCoreXMLDMStatementType;
class IMpafEBUCoreXMLDMStatementType;
/** Smart pointer for instance of IMpafEBUCoreXMLDMStatementType */
typedef Dc1Ptr< IMpafEBUCoreXMLDMStatementType > MpafEBUCoreXMLDMStatementPtr;

#define TypeOfEssenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType")))
#define CreateEssenceType (Dc1Factory::CreateObject(TypeOfEssenceType))
class MpafEssenceType;
class IMpafEssenceType;
/** Smart pointer for instance of IMpafEssenceType */
typedef Dc1Ptr< IMpafEssenceType > MpafEssencePtr;

#define TypeOfEssenceType_Choice_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType_Choice_CollectionType")))
#define CreateEssenceType_Choice_CollectionType (Dc1Factory::CreateObject(TypeOfEssenceType_Choice_CollectionType))
class MpafEssenceType_Choice_CollectionType;
class IMpafEssenceType_Choice_CollectionType;
/** Smart pointer for instance of IMpafEssenceType_Choice_CollectionType */
typedef Dc1Ptr< IMpafEssenceType_Choice_CollectionType > MpafEssenceType_Choice_CollectionPtr;

#define TypeOfEssenceType_Component_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType_Component_CollectionType")))
#define CreateEssenceType_Component_CollectionType (Dc1Factory::CreateObject(TypeOfEssenceType_Component_CollectionType))
class MpafEssenceType_Component_CollectionType;
class IMpafEssenceType_Component_CollectionType;
/** Smart pointer for instance of IMpafEssenceType_Component_CollectionType */
typedef Dc1Ptr< IMpafEssenceType_Component_CollectionType > MpafEssenceType_Component_CollectionPtr;

#define TypeOfEssenceType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType_Condition_CollectionType")))
#define CreateEssenceType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfEssenceType_Condition_CollectionType))
class MpafEssenceType_Condition_CollectionType;
class IMpafEssenceType_Condition_CollectionType;
/** Smart pointer for instance of IMpafEssenceType_Condition_CollectionType */
typedef Dc1Ptr< IMpafEssenceType_Condition_CollectionType > MpafEssenceType_Condition_CollectionPtr;

#define TypeOfEssenceType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:EssenceType_Descriptor_CollectionType")))
#define CreateEssenceType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfEssenceType_Descriptor_CollectionType))
class MpafEssenceType_Descriptor_CollectionType;
class IMpafEssenceType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafEssenceType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafEssenceType_Descriptor_CollectionType > MpafEssenceType_Descriptor_CollectionPtr;

#define TypeOfExploitationRightsDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ExploitationRightsDescriptorType")))
#define CreateExploitationRightsDescriptorType (Dc1Factory::CreateObject(TypeOfExploitationRightsDescriptorType))
class MpafExploitationRightsDescriptorType;
class IMpafExploitationRightsDescriptorType;
/** Smart pointer for instance of IMpafExploitationRightsDescriptorType */
typedef Dc1Ptr< IMpafExploitationRightsDescriptorType > MpafExploitationRightsDescriptorPtr;

#define TypeOfFileType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:FileType")))
#define CreateFileType (Dc1Factory::CreateObject(TypeOfFileType))
class MpafFileType;
class IMpafFileType;
/** Smart pointer for instance of IMpafFileType */
typedef Dc1Ptr< IMpafFileType > MpafFilePtr;

#define TypeOfFixityCheckType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:FixityCheckType")))
#define CreateFixityCheckType (Dc1Factory::CreateObject(TypeOfFixityCheckType))
class MpafFixityCheckType;
class IMpafFixityCheckType;
/** Smart pointer for instance of IMpafFixityCheckType */
typedef Dc1Ptr< IMpafFixityCheckType > MpafFixityCheckPtr;

#define TypeOfFixityDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:FixityDescriptorType")))
#define CreateFixityDescriptorType (Dc1Factory::CreateObject(TypeOfFixityDescriptorType))
class MpafFixityDescriptorType;
class IMpafFixityDescriptorType;
/** Smart pointer for instance of IMpafFixityDescriptorType */
typedef Dc1Ptr< IMpafFixityDescriptorType > MpafFixityDescriptorPtr;

#define TypeOfFixityStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:FixityStatementType")))
#define CreateFixityStatementType (Dc1Factory::CreateObject(TypeOfFixityStatementType))
class MpafFixityStatementType;
class IMpafFixityStatementType;
/** Smart pointer for instance of IMpafFixityStatementType */
typedef Dc1Ptr< IMpafFixityStatementType > MpafFixityStatementPtr;

#define TypeOfFixityStatementType_Check_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:FixityStatementType_Check_CollectionType")))
#define CreateFixityStatementType_Check_CollectionType (Dc1Factory::CreateObject(TypeOfFixityStatementType_Check_CollectionType))
class MpafFixityStatementType_Check_CollectionType;
class IMpafFixityStatementType_Check_CollectionType;
/** Smart pointer for instance of IMpafFixityStatementType_Check_CollectionType */
typedef Dc1Ptr< IMpafFixityStatementType_Check_CollectionType > MpafFixityStatementType_Check_CollectionPtr;

#define TypeOfGroupType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType")))
#define CreateGroupType (Dc1Factory::CreateObject(TypeOfGroupType))
class MpafGroupType;
class IMpafGroupType;
/** Smart pointer for instance of IMpafGroupType */
typedef Dc1Ptr< IMpafGroupType > MpafGroupPtr;

#define TypeOfGroupType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType_CollectionType")))
#define CreateGroupType_CollectionType (Dc1Factory::CreateObject(TypeOfGroupType_CollectionType))
class MpafGroupType_CollectionType;
class IMpafGroupType_CollectionType;
/** Smart pointer for instance of IMpafGroupType_CollectionType */
typedef Dc1Ptr< IMpafGroupType_CollectionType > MpafGroupType_CollectionPtr;

#define TypeOfGroupType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType_Descriptor_CollectionType")))
#define CreateGroupType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfGroupType_Descriptor_CollectionType))
class MpafGroupType_Descriptor_CollectionType;
class IMpafGroupType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafGroupType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafGroupType_Descriptor_CollectionType > MpafGroupType_Descriptor_CollectionPtr;

#define TypeOfGroupType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:GroupType_LocalType")))
#define CreateGroupType_LocalType (Dc1Factory::CreateObject(TypeOfGroupType_LocalType))
class MpafGroupType_LocalType;
class IMpafGroupType_LocalType;
/** Smart pointer for instance of IMpafGroupType_LocalType */
typedef Dc1Ptr< IMpafGroupType_LocalType > MpafGroupType_LocalPtr;

#define TypeOfIdentificationDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IdentificationDescriptorType")))
#define CreateIdentificationDescriptorType (Dc1Factory::CreateObject(TypeOfIdentificationDescriptorType))
class MpafIdentificationDescriptorType;
class IMpafIdentificationDescriptorType;
/** Smart pointer for instance of IMpafIdentificationDescriptorType */
typedef Dc1Ptr< IMpafIdentificationDescriptorType > MpafIdentificationDescriptorPtr;

#define TypeOfIdentificationStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IdentificationStatementType")))
#define CreateIdentificationStatementType (Dc1Factory::CreateObject(TypeOfIdentificationStatementType))
class MpafIdentificationStatementType;
class IMpafIdentificationStatementType;
/** Smart pointer for instance of IMpafIdentificationStatementType */
typedef Dc1Ptr< IMpafIdentificationStatementType > MpafIdentificationStatementPtr;

#define TypeOfIdentificationStatementType_Identifier_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IdentificationStatementType_Identifier_CollectionType")))
#define CreateIdentificationStatementType_Identifier_CollectionType (Dc1Factory::CreateObject(TypeOfIdentificationStatementType_Identifier_CollectionType))
class MpafIdentificationStatementType_Identifier_CollectionType;
class IMpafIdentificationStatementType_Identifier_CollectionType;
/** Smart pointer for instance of IMpafIdentificationStatementType_Identifier_CollectionType */
typedef Dc1Ptr< IMpafIdentificationStatementType_Identifier_CollectionType > MpafIdentificationStatementType_Identifier_CollectionPtr;

#define TypeOfIdentifierBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IdentifierBaseType")))
#define CreateIdentifierBaseType (Dc1Factory::CreateObject(TypeOfIdentifierBaseType))
class MpafIdentifierBaseType;
class IMpafIdentifierBaseType;
/** Smart pointer for instance of IMpafIdentifierBaseType */
typedef Dc1Ptr< IMpafIdentifierBaseType > MpafIdentifierBasePtr;

#define TypeOfIdentifierType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IdentifierType")))
#define CreateIdentifierType (Dc1Factory::CreateObject(TypeOfIdentifierType))
class MpafIdentifierType;
class IMpafIdentifierType;
/** Smart pointer for instance of IMpafIdentifierType */
typedef Dc1Ptr< IMpafIdentifierType > MpafIdentifierPtr;

#define TypeOfIntegrityDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityDescriptorType")))
#define CreateIntegrityDescriptorType (Dc1Factory::CreateObject(TypeOfIntegrityDescriptorType))
class MpafIntegrityDescriptorType;
class IMpafIntegrityDescriptorType;
/** Smart pointer for instance of IMpafIntegrityDescriptorType */
typedef Dc1Ptr< IMpafIntegrityDescriptorType > MpafIntegrityDescriptorPtr;

#define TypeOfIntegrityStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType")))
#define CreateIntegrityStatementType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType))
class MpafIntegrityStatementType;
class IMpafIntegrityStatementType;
/** Smart pointer for instance of IMpafIntegrityStatementType */
typedef Dc1Ptr< IMpafIntegrityStatementType > MpafIntegrityStatementPtr;

#define TypeOfIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_DeprecatedEntity_CollectionType")))
#define CreateIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType))
class MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType;
class IMpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType;
/** Smart pointer for instance of IMpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType */
typedef Dc1Ptr< IMpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionType > MpafIntegrityStatementType_CheckList_DeprecatedEntity_CollectionPtr;

#define TypeOfIntegrityStatementType_CheckList_DeprecatedEntity_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_DeprecatedEntity_LocalType")))
#define CreateIntegrityStatementType_CheckList_DeprecatedEntity_LocalType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType_CheckList_DeprecatedEntity_LocalType))
class MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType;
class IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType;
/** Smart pointer for instance of IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType */
typedef Dc1Ptr< IMpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalType > MpafIntegrityStatementType_CheckList_DeprecatedEntity_LocalPtr;

#define TypeOfIntegrityStatementType_CheckList_Entity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_Entity_CollectionType")))
#define CreateIntegrityStatementType_CheckList_Entity_CollectionType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType_CheckList_Entity_CollectionType))
class MpafIntegrityStatementType_CheckList_Entity_CollectionType;
class IMpafIntegrityStatementType_CheckList_Entity_CollectionType;
/** Smart pointer for instance of IMpafIntegrityStatementType_CheckList_Entity_CollectionType */
typedef Dc1Ptr< IMpafIntegrityStatementType_CheckList_Entity_CollectionType > MpafIntegrityStatementType_CheckList_Entity_CollectionPtr;

#define TypeOfIntegrityStatementType_CheckList_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_CheckList_LocalType")))
#define CreateIntegrityStatementType_CheckList_LocalType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType_CheckList_LocalType))
class MpafIntegrityStatementType_CheckList_LocalType;
class IMpafIntegrityStatementType_CheckList_LocalType;
/** Smart pointer for instance of IMpafIntegrityStatementType_CheckList_LocalType */
typedef Dc1Ptr< IMpafIntegrityStatementType_CheckList_LocalType > MpafIntegrityStatementType_CheckList_LocalPtr;

#define TypeOfIntegrityStatementType_ExternalDependency_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:IntegrityStatementType_ExternalDependency_CollectionType")))
#define CreateIntegrityStatementType_ExternalDependency_CollectionType (Dc1Factory::CreateObject(TypeOfIntegrityStatementType_ExternalDependency_CollectionType))
class MpafIntegrityStatementType_ExternalDependency_CollectionType;
class IMpafIntegrityStatementType_ExternalDependency_CollectionType;
/** Smart pointer for instance of IMpafIntegrityStatementType_ExternalDependency_CollectionType */
typedef Dc1Ptr< IMpafIntegrityStatementType_ExternalDependency_CollectionType > MpafIntegrityStatementType_ExternalDependency_CollectionPtr;

#define TypeOfItemBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ItemBaseType")))
#define CreateItemBaseType (Dc1Factory::CreateObject(TypeOfItemBaseType))
class MpafItemBaseType;
class IMpafItemBaseType;
/** Smart pointer for instance of IMpafItemBaseType */
typedef Dc1Ptr< IMpafItemBaseType > MpafItemBasePtr;

#define TypeOfItemRefBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ItemRefBaseType")))
#define CreateItemRefBaseType (Dc1Factory::CreateObject(TypeOfItemRefBaseType))
class MpafItemRefBaseType;
class IMpafItemRefBaseType;
/** Smart pointer for instance of IMpafItemRefBaseType */
typedef Dc1Ptr< IMpafItemRefBaseType > MpafItemRefBasePtr;

#define TypeOfItemRefType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ItemRefType")))
#define CreateItemRefType (Dc1Factory::CreateObject(TypeOfItemRefType))
class MpafItemRefType;
class IMpafItemRefType;
/** Smart pointer for instance of IMpafItemRefType */
typedef Dc1Ptr< IMpafItemRefType > MpafItemRefPtr;

// This is special, since there is an mpeg21 type with the same name
#define TypeOfMpafItemType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ItemType")))
#define CreateMpafItemType (Dc1Factory::CreateObject(TypeOfMpafItemType))
class MpafItemType;
class IMpafItemType;
/** Smart pointer to instance of IMpafItemType */
typedef Dc1Ptr< IMpafItemType > MpafItemPtr;

#define TypeOfMCORightsStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MCORightsStatementType")))
#define CreateMCORightsStatementType (Dc1Factory::CreateObject(TypeOfMCORightsStatementType))
class MpafMCORightsStatementType;
class IMpafMCORightsStatementType;
/** Smart pointer for instance of IMpafMCORightsStatementType */
typedef Dc1Ptr< IMpafMCORightsStatementType > MpafMCORightsStatementPtr;

// This is special, since there is an mpeg-7 type with the same name
#define TypeOfMpafMediaLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MediaLocatorType")))
#define CreateMpafMediaLocatorType (Dc1Factory::CreateObject(TypeOfMpafMediaLocatorType))
class MpafMediaLocatorType;
class IMpafMediaLocatorType;
/** Smart pointer to instance of IMpafMediaLocatorType */
typedef Dc1Ptr< IMpafMediaLocatorType > MpafMediaLocatorPtr;

#define TypeOfMPAFBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPAFBaseType")))
#define CreateMPAFBaseType (Dc1Factory::CreateObject(TypeOfMPAFBaseType))
class MpafMPAFBaseType;
class IMpafMPAFBaseType;
/** Smart pointer for instance of IMpafMPAFBaseType */
typedef Dc1Ptr< IMpafMPAFBaseType > MpafMPAFBasePtr;

#define TypeOfMPEG7DMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType")))
#define CreateMPEG7DMStatementType (Dc1Factory::CreateObject(TypeOfMPEG7DMStatementType))
class MpafMPEG7DMStatementType;
class IMpafMPEG7DMStatementType;
/** Smart pointer for instance of IMpafMPEG7DMStatementType */
typedef Dc1Ptr< IMpafMPEG7DMStatementType > MpafMPEG7DMStatementPtr;

#define TypeOfMPEG7DMStatementType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType_CollectionType")))
#define CreateMPEG7DMStatementType_CollectionType (Dc1Factory::CreateObject(TypeOfMPEG7DMStatementType_CollectionType))
class MpafMPEG7DMStatementType_CollectionType;
class IMpafMPEG7DMStatementType_CollectionType;
/** Smart pointer for instance of IMpafMPEG7DMStatementType_CollectionType */
typedef Dc1Ptr< IMpafMPEG7DMStatementType_CollectionType > MpafMPEG7DMStatementType_CollectionPtr;

#define TypeOfMPEG7DMStatementType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPEG7DMStatementType_LocalType")))
#define CreateMPEG7DMStatementType_LocalType (Dc1Factory::CreateObject(TypeOfMPEG7DMStatementType_LocalType))
class MpafMPEG7DMStatementType_LocalType;
class IMpafMPEG7DMStatementType_LocalType;
/** Smart pointer for instance of IMpafMPEG7DMStatementType_LocalType */
typedef Dc1Ptr< IMpafMPEG7DMStatementType_LocalType > MpafMPEG7DMStatementType_LocalPtr;

#define TypeOfMPEG7TMStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:MPEG7TMStatementType")))
#define CreateMPEG7TMStatementType (Dc1Factory::CreateObject(TypeOfMPEG7TMStatementType))
class MpafMPEG7TMStatementType;
class IMpafMPEG7TMStatementType;
/** Smart pointer for instance of IMpafMPEG7TMStatementType */
typedef Dc1Ptr< IMpafMPEG7TMStatementType > MpafMPEG7TMStatementPtr;

#define TypeOfOperatorRefType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OperatorRefType")))
#define CreateOperatorRefType (Dc1Factory::CreateObject(TypeOfOperatorRefType))
class MpafOperatorRefType;
class IMpafOperatorRefType;
/** Smart pointer for instance of IMpafOperatorRefType */
typedef Dc1Ptr< IMpafOperatorRefType > MpafOperatorRefPtr;

#define TypeOfOperatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OperatorType")))
#define CreateOperatorType (Dc1Factory::CreateObject(TypeOfOperatorType))
class MpafOperatorType;
class IMpafOperatorType;
/** Smart pointer for instance of IMpafOperatorType */
typedef Dc1Ptr< IMpafOperatorType > MpafOperatorPtr;

#define TypeOfOperatorType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OperatorType_Descriptor_CollectionType")))
#define CreateOperatorType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfOperatorType_Descriptor_CollectionType))
class MpafOperatorType_Descriptor_CollectionType;
class IMpafOperatorType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafOperatorType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafOperatorType_Descriptor_CollectionType > MpafOperatorType_Descriptor_CollectionPtr;

// This is special, since there is an mpeg-7 type with the same name
#define TypeOfMpafOrganizationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType")))
#define CreateMpafOrganizationType (Dc1Factory::CreateObject(TypeOfMpafOrganizationType))
class MpafOrganizationType;
class IMpafOrganizationType;
/** Smart pointer to instance of IMpafOrganizationType */
typedef Dc1Ptr< IMpafOrganizationType > MpafOrganizationPtr;

#define TypeOfOrganizationType_Contact_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Contact_CollectionType")))
#define CreateOrganizationType_Contact_CollectionType (Dc1Factory::CreateObject(TypeOfOrganizationType_Contact_CollectionType))
class MpafOrganizationType_Contact_CollectionType;
class IMpafOrganizationType_Contact_CollectionType;
/** Smart pointer for instance of IMpafOrganizationType_Contact_CollectionType */
typedef Dc1Ptr< IMpafOrganizationType_Contact_CollectionType > MpafOrganizationType_Contact_CollectionPtr;

#define TypeOfOrganizationType_Name_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_CollectionType0")))
#define CreateOrganizationType_Name_CollectionType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_CollectionType0))
class MpafOrganizationType_Name_CollectionType0;
class IMpafOrganizationType_Name_CollectionType0;
/** Smart pointer for instance of IMpafOrganizationType_Name_CollectionType0 */
typedef Dc1Ptr< IMpafOrganizationType_Name_CollectionType0 > MpafOrganizationType_Name_Collection0Ptr;

#define TypeOfOrganizationType_Name_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_LocalType0")))
#define CreateOrganizationType_Name_LocalType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_LocalType0))
class MpafOrganizationType_Name_LocalType0;
class IMpafOrganizationType_Name_LocalType0;
/** Smart pointer for instance of IMpafOrganizationType_Name_LocalType0 */
typedef Dc1Ptr< IMpafOrganizationType_Name_LocalType0 > MpafOrganizationType_Name_Local0Ptr;

#define TypeOfOrganizationType_Name_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_Name_type_LocalType0")))
#define CreateOrganizationType_Name_type_LocalType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_type_LocalType0))
class MpafOrganizationType_Name_type_LocalType0;
class IMpafOrganizationType_Name_type_LocalType0;
// No smart pointer instance for IMpafOrganizationType_Name_type_LocalType0

#define TypeOfOrganizationType_NameTerm_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_CollectionType0")))
#define CreateOrganizationType_NameTerm_CollectionType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_CollectionType0))
class MpafOrganizationType_NameTerm_CollectionType0;
class IMpafOrganizationType_NameTerm_CollectionType0;
/** Smart pointer for instance of IMpafOrganizationType_NameTerm_CollectionType0 */
typedef Dc1Ptr< IMpafOrganizationType_NameTerm_CollectionType0 > MpafOrganizationType_NameTerm_Collection0Ptr;

#define TypeOfOrganizationType_NameTerm_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_LocalType0")))
#define CreateOrganizationType_NameTerm_LocalType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_LocalType0))
class MpafOrganizationType_NameTerm_LocalType0;
class IMpafOrganizationType_NameTerm_LocalType0;
/** Smart pointer for instance of IMpafOrganizationType_NameTerm_LocalType0 */
typedef Dc1Ptr< IMpafOrganizationType_NameTerm_LocalType0 > MpafOrganizationType_NameTerm_Local0Ptr;

#define TypeOfOrganizationType_NameTerm_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:OrganizationType_NameTerm_type_LocalType0")))
#define CreateOrganizationType_NameTerm_type_LocalType0 (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_type_LocalType0))
class MpafOrganizationType_NameTerm_type_LocalType0;
class IMpafOrganizationType_NameTerm_type_LocalType0;
// No smart pointer instance for IMpafOrganizationType_NameTerm_type_LocalType0

// This is special, since there is an mpeg-7 type with the same name
#define TypeOfMpafPersonGroupType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType")))
#define CreateMpafPersonGroupType (Dc1Factory::CreateObject(TypeOfMpafPersonGroupType))
class MpafPersonGroupType;
class IMpafPersonGroupType;
/** Smart pointer to instance of IMpafPersonGroupType */
typedef Dc1Ptr< IMpafPersonGroupType > MpafPersonGroupPtr;

#define TypeOfPersonGroupType_Member_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Member_CollectionType")))
#define CreatePersonGroupType_Member_CollectionType (Dc1Factory::CreateObject(TypeOfPersonGroupType_Member_CollectionType))
class MpafPersonGroupType_Member_CollectionType;
class IMpafPersonGroupType_Member_CollectionType;
/** Smart pointer for instance of IMpafPersonGroupType_Member_CollectionType */
typedef Dc1Ptr< IMpafPersonGroupType_Member_CollectionType > MpafPersonGroupType_Member_CollectionPtr;

#define TypeOfPersonGroupType_Name_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Name_CollectionType0")))
#define CreatePersonGroupType_Name_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_CollectionType0))
class MpafPersonGroupType_Name_CollectionType0;
class IMpafPersonGroupType_Name_CollectionType0;
/** Smart pointer for instance of IMpafPersonGroupType_Name_CollectionType0 */
typedef Dc1Ptr< IMpafPersonGroupType_Name_CollectionType0 > MpafPersonGroupType_Name_Collection0Ptr;

#define TypeOfPersonGroupType_Name_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Name_LocalType0")))
#define CreatePersonGroupType_Name_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_LocalType0))
class MpafPersonGroupType_Name_LocalType0;
class IMpafPersonGroupType_Name_LocalType0;
/** Smart pointer for instance of IMpafPersonGroupType_Name_LocalType0 */
typedef Dc1Ptr< IMpafPersonGroupType_Name_LocalType0 > MpafPersonGroupType_Name_Local0Ptr;

#define TypeOfPersonGroupType_Name_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_Name_type_LocalType0")))
#define CreatePersonGroupType_Name_type_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_type_LocalType0))
class MpafPersonGroupType_Name_type_LocalType0;
class IMpafPersonGroupType_Name_type_LocalType0;
// No smart pointer instance for IMpafPersonGroupType_Name_type_LocalType0

#define TypeOfPersonGroupType_NameTerm_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_NameTerm_CollectionType0")))
#define CreatePersonGroupType_NameTerm_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_CollectionType0))
class MpafPersonGroupType_NameTerm_CollectionType0;
class IMpafPersonGroupType_NameTerm_CollectionType0;
/** Smart pointer for instance of IMpafPersonGroupType_NameTerm_CollectionType0 */
typedef Dc1Ptr< IMpafPersonGroupType_NameTerm_CollectionType0 > MpafPersonGroupType_NameTerm_Collection0Ptr;

#define TypeOfPersonGroupType_NameTerm_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_NameTerm_LocalType0")))
#define CreatePersonGroupType_NameTerm_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_LocalType0))
class MpafPersonGroupType_NameTerm_LocalType0;
class IMpafPersonGroupType_NameTerm_LocalType0;
/** Smart pointer for instance of IMpafPersonGroupType_NameTerm_LocalType0 */
typedef Dc1Ptr< IMpafPersonGroupType_NameTerm_LocalType0 > MpafPersonGroupType_NameTerm_Local0Ptr;

#define TypeOfPersonGroupType_NameTerm_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonGroupType_NameTerm_type_LocalType0")))
#define CreatePersonGroupType_NameTerm_type_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_type_LocalType0))
class MpafPersonGroupType_NameTerm_type_LocalType0;
class IMpafPersonGroupType_NameTerm_type_LocalType0;
// No smart pointer instance for IMpafPersonGroupType_NameTerm_type_LocalType0

// This is special, since there is an mpeg-7 type with the same name
#define TypeOfMpafPersonType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType")))
#define CreateMpafPersonType (Dc1Factory::CreateObject(TypeOfMpafPersonType))
class MpafPersonType;
class IMpafPersonType;
/** Smart pointer to instance of IMpafPersonType */
typedef Dc1Ptr< IMpafPersonType > MpafPersonPtr;

#define TypeOfPersonType_Affiliation_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_Affiliation_CollectionType0")))
#define CreatePersonType_Affiliation_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonType_Affiliation_CollectionType0))
class MpafPersonType_Affiliation_CollectionType0;
class IMpafPersonType_Affiliation_CollectionType0;
/** Smart pointer for instance of IMpafPersonType_Affiliation_CollectionType0 */
typedef Dc1Ptr< IMpafPersonType_Affiliation_CollectionType0 > MpafPersonType_Affiliation_Collection0Ptr;

#define TypeOfPersonType_Affiliation_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_Affiliation_LocalType0")))
#define CreatePersonType_Affiliation_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonType_Affiliation_LocalType0))
class MpafPersonType_Affiliation_LocalType0;
class IMpafPersonType_Affiliation_LocalType0;
/** Smart pointer for instance of IMpafPersonType_Affiliation_LocalType0 */
typedef Dc1Ptr< IMpafPersonType_Affiliation_LocalType0 > MpafPersonType_Affiliation_Local0Ptr;

#define TypeOfPersonType_Citizenship_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_Citizenship_CollectionType0")))
#define CreatePersonType_Citizenship_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonType_Citizenship_CollectionType0))
class MpafPersonType_Citizenship_CollectionType0;
class IMpafPersonType_Citizenship_CollectionType0;
/** Smart pointer for instance of IMpafPersonType_Citizenship_CollectionType0 */
typedef Dc1Ptr< IMpafPersonType_Citizenship_CollectionType0 > MpafPersonType_Citizenship_Collection0Ptr;

#define TypeOfPersonType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_CollectionType0")))
#define CreatePersonType_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonType_CollectionType0))
class MpafPersonType_CollectionType0;
class IMpafPersonType_CollectionType0;
/** Smart pointer for instance of IMpafPersonType_CollectionType0 */
typedef Dc1Ptr< IMpafPersonType_CollectionType0 > MpafPersonType_Collection0Ptr;

#define TypeOfPersonType_ElectronicAddress_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_ElectronicAddress_CollectionType0")))
#define CreatePersonType_ElectronicAddress_CollectionType0 (Dc1Factory::CreateObject(TypeOfPersonType_ElectronicAddress_CollectionType0))
class MpafPersonType_ElectronicAddress_CollectionType0;
class IMpafPersonType_ElectronicAddress_CollectionType0;
/** Smart pointer for instance of IMpafPersonType_ElectronicAddress_CollectionType0 */
typedef Dc1Ptr< IMpafPersonType_ElectronicAddress_CollectionType0 > MpafPersonType_ElectronicAddress_Collection0Ptr;

#define TypeOfPersonType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PersonType_LocalType0")))
#define CreatePersonType_LocalType0 (Dc1Factory::CreateObject(TypeOfPersonType_LocalType0))
class MpafPersonType_LocalType0;
class IMpafPersonType_LocalType0;
/** Smart pointer for instance of IMpafPersonType_LocalType0 */
typedef Dc1Ptr< IMpafPersonType_LocalType0 > MpafPersonType_Local0Ptr;

#define TypeOfPreservationMetadataType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType")))
#define CreatePreservationMetadataType (Dc1Factory::CreateObject(TypeOfPreservationMetadataType))
class MpafPreservationMetadataType;
class IMpafPreservationMetadataType;
/** Smart pointer for instance of IMpafPreservationMetadataType */
typedef Dc1Ptr< IMpafPreservationMetadataType > MpafPreservationMetadataPtr;

#define TypeOfPreservationMetadataType_DIDLInfo_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_LocalType")))
#define CreatePreservationMetadataType_DIDLInfo_LocalType (Dc1Factory::CreateObject(TypeOfPreservationMetadataType_DIDLInfo_LocalType))
class MpafPreservationMetadataType_DIDLInfo_LocalType;
class IMpafPreservationMetadataType_DIDLInfo_LocalType;
/** Smart pointer for instance of IMpafPreservationMetadataType_DIDLInfo_LocalType */
typedef Dc1Ptr< IMpafPreservationMetadataType_DIDLInfo_LocalType > MpafPreservationMetadataType_DIDLInfo_LocalPtr;

#define TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType")))
#define CreatePreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType (Dc1Factory::CreateObject(TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType))
class MpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType;
class IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType;
/** Smart pointer for instance of IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType */
typedef Dc1Ptr< IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionType > MpafPreservationMetadataType_DIDLInfo_ProcessEntities_CollectionPtr;

#define TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType")))
#define CreatePreservationMetadataType_DIDLInfo_ProcessEntities_LocalType (Dc1Factory::CreateObject(TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType))
class MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType;
class IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType;
/** Smart pointer for instance of IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType */
typedef Dc1Ptr< IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType > MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalPtr;

#define TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0")))
#define CreatePreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0 (Dc1Factory::CreateObject(TypeOfPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0))
class MpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0;
class IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0;
/** Smart pointer for instance of IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0 */
typedef Dc1Ptr< IMpafPreservationMetadataType_DIDLInfo_ProcessEntities_LocalType0 > MpafPreservationMetadataType_DIDLInfo_ProcessEntities_Local0Ptr;

#define TypeOfPreservationObjectType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType")))
#define CreatePreservationObjectType (Dc1Factory::CreateObject(TypeOfPreservationObjectType))
class MpafPreservationObjectType;
class IMpafPreservationObjectType;
/** Smart pointer for instance of IMpafPreservationObjectType */
typedef Dc1Ptr< IMpafPreservationObjectType > MpafPreservationObjectPtr;

#define TypeOfPreservationObjectType_Choice_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Choice_CollectionType")))
#define CreatePreservationObjectType_Choice_CollectionType (Dc1Factory::CreateObject(TypeOfPreservationObjectType_Choice_CollectionType))
class MpafPreservationObjectType_Choice_CollectionType;
class IMpafPreservationObjectType_Choice_CollectionType;
/** Smart pointer for instance of IMpafPreservationObjectType_Choice_CollectionType */
typedef Dc1Ptr< IMpafPreservationObjectType_Choice_CollectionType > MpafPreservationObjectType_Choice_CollectionPtr;

#define TypeOfPreservationObjectType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Condition_CollectionType")))
#define CreatePreservationObjectType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfPreservationObjectType_Condition_CollectionType))
class MpafPreservationObjectType_Condition_CollectionType;
class IMpafPreservationObjectType_Condition_CollectionType;
/** Smart pointer for instance of IMpafPreservationObjectType_Condition_CollectionType */
typedef Dc1Ptr< IMpafPreservationObjectType_Condition_CollectionType > MpafPreservationObjectType_Condition_CollectionPtr;

#define TypeOfPreservationObjectType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Descriptor_CollectionType")))
#define CreatePreservationObjectType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfPreservationObjectType_Descriptor_CollectionType))
class MpafPreservationObjectType_Descriptor_CollectionType;
class IMpafPreservationObjectType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafPreservationObjectType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafPreservationObjectType_Descriptor_CollectionType > MpafPreservationObjectType_Descriptor_CollectionPtr;

#define TypeOfPreservationObjectType_Item_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:PreservationObjectType_Item_CollectionType")))
#define CreatePreservationObjectType_Item_CollectionType (Dc1Factory::CreateObject(TypeOfPreservationObjectType_Item_CollectionType))
class MpafPreservationObjectType_Item_CollectionType;
class IMpafPreservationObjectType_Item_CollectionType;
/** Smart pointer for instance of IMpafPreservationObjectType_Item_CollectionType */
typedef Dc1Ptr< IMpafPreservationObjectType_Item_CollectionType > MpafPreservationObjectType_Item_CollectionPtr;

#define TypeOfProvenanceDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ProvenanceDescriptorType")))
#define CreateProvenanceDescriptorType (Dc1Factory::CreateObject(TypeOfProvenanceDescriptorType))
class MpafProvenanceDescriptorType;
class IMpafProvenanceDescriptorType;
/** Smart pointer for instance of IMpafProvenanceDescriptorType */
typedef Dc1Ptr< IMpafProvenanceDescriptorType > MpafProvenanceDescriptorPtr;

#define TypeOfProvenanceStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType")))
#define CreateProvenanceStatementType (Dc1Factory::CreateObject(TypeOfProvenanceStatementType))
class MpafProvenanceStatementType;
class IMpafProvenanceStatementType;
/** Smart pointer for instance of IMpafProvenanceStatementType */
typedef Dc1Ptr< IMpafProvenanceStatementType > MpafProvenanceStatementPtr;

#define TypeOfProvenanceStatementType_Activity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType_Activity_CollectionType")))
#define CreateProvenanceStatementType_Activity_CollectionType (Dc1Factory::CreateObject(TypeOfProvenanceStatementType_Activity_CollectionType))
class MpafProvenanceStatementType_Activity_CollectionType;
class IMpafProvenanceStatementType_Activity_CollectionType;
/** Smart pointer for instance of IMpafProvenanceStatementType_Activity_CollectionType */
typedef Dc1Ptr< IMpafProvenanceStatementType_Activity_CollectionType > MpafProvenanceStatementType_Activity_CollectionPtr;

#define TypeOfProvenanceStatementType_Operator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ProvenanceStatementType_Operator_CollectionType")))
#define CreateProvenanceStatementType_Operator_CollectionType (Dc1Factory::CreateObject(TypeOfProvenanceStatementType_Operator_CollectionType))
class MpafProvenanceStatementType_Operator_CollectionType;
class IMpafProvenanceStatementType_Operator_CollectionType;
/** Smart pointer for instance of IMpafProvenanceStatementType_Operator_CollectionType */
typedef Dc1Ptr< IMpafProvenanceStatementType_Operator_CollectionType > MpafProvenanceStatementType_Operator_CollectionPtr;

#define TypeOfQualityDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityDescriptorType")))
#define CreateQualityDescriptorType (Dc1Factory::CreateObject(TypeOfQualityDescriptorType))
class MpafQualityDescriptorType;
class IMpafQualityDescriptorType;
/** Smart pointer for instance of IMpafQualityDescriptorType */
typedef Dc1Ptr< IMpafQualityDescriptorType > MpafQualityDescriptorPtr;

#define TypeOfQualityStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityStatementType")))
#define CreateQualityStatementType (Dc1Factory::CreateObject(TypeOfQualityStatementType))
class MpafQualityStatementType;
class IMpafQualityStatementType;
/** Smart pointer for instance of IMpafQualityStatementType */
typedef Dc1Ptr< IMpafQualityStatementType > MpafQualityStatementPtr;

#define TypeOfQualityStatementType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityStatementType_CollectionType")))
#define CreateQualityStatementType_CollectionType (Dc1Factory::CreateObject(TypeOfQualityStatementType_CollectionType))
class MpafQualityStatementType_CollectionType;
class IMpafQualityStatementType_CollectionType;
/** Smart pointer for instance of IMpafQualityStatementType_CollectionType */
typedef Dc1Ptr< IMpafQualityStatementType_CollectionType > MpafQualityStatementType_CollectionPtr;

#define TypeOfQualityStatementType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:QualityStatementType_LocalType")))
#define CreateQualityStatementType_LocalType (Dc1Factory::CreateObject(TypeOfQualityStatementType_LocalType))
class MpafQualityStatementType_LocalType;
class IMpafQualityStatementType_LocalType;
/** Smart pointer for instance of IMpafQualityStatementType_LocalType */
typedef Dc1Ptr< IMpafQualityStatementType_LocalType > MpafQualityStatementType_LocalPtr;

#define TypeOfReferenceDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ReferenceDescriptorType")))
#define CreateReferenceDescriptorType (Dc1Factory::CreateObject(TypeOfReferenceDescriptorType))
class MpafReferenceDescriptorType;
class IMpafReferenceDescriptorType;
/** Smart pointer for instance of IMpafReferenceDescriptorType */
typedef Dc1Ptr< IMpafReferenceDescriptorType > MpafReferenceDescriptorPtr;

#define TypeOfRelatedEntityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RelatedEntityType")))
#define CreateRelatedEntityType (Dc1Factory::CreateObject(TypeOfRelatedEntityType))
class MpafRelatedEntityType;
class IMpafRelatedEntityType;
/** Smart pointer for instance of IMpafRelatedEntityType */
typedef Dc1Ptr< IMpafRelatedEntityType > MpafRelatedEntityPtr;

#define TypeOfRelationStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RelationStatementType")))
#define CreateRelationStatementType (Dc1Factory::CreateObject(TypeOfRelationStatementType))
class MpafRelationStatementType;
class IMpafRelationStatementType;
/** Smart pointer for instance of IMpafRelationStatementType */
typedef Dc1Ptr< IMpafRelationStatementType > MpafRelationStatementPtr;

#define TypeOfRELRightsStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RELRightsStatementType")))
#define CreateRELRightsStatementType (Dc1Factory::CreateObject(TypeOfRELRightsStatementType))
class MpafRELRightsStatementType;
class IMpafRELRightsStatementType;
/** Smart pointer for instance of IMpafRELRightsStatementType */
typedef Dc1Ptr< IMpafRELRightsStatementType > MpafRELRightsStatementPtr;

#define TypeOfRepresentationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RepresentationType")))
#define CreateRepresentationType (Dc1Factory::CreateObject(TypeOfRepresentationType))
class MpafRepresentationType;
class IMpafRepresentationType;
/** Smart pointer for instance of IMpafRepresentationType */
typedef Dc1Ptr< IMpafRepresentationType > MpafRepresentationPtr;

#define TypeOfRepresentationType_Choice_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Choice_CollectionType")))
#define CreateRepresentationType_Choice_CollectionType (Dc1Factory::CreateObject(TypeOfRepresentationType_Choice_CollectionType))
class MpafRepresentationType_Choice_CollectionType;
class IMpafRepresentationType_Choice_CollectionType;
/** Smart pointer for instance of IMpafRepresentationType_Choice_CollectionType */
typedef Dc1Ptr< IMpafRepresentationType_Choice_CollectionType > MpafRepresentationType_Choice_CollectionPtr;

#define TypeOfRepresentationType_Condition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Condition_CollectionType")))
#define CreateRepresentationType_Condition_CollectionType (Dc1Factory::CreateObject(TypeOfRepresentationType_Condition_CollectionType))
class MpafRepresentationType_Condition_CollectionType;
class IMpafRepresentationType_Condition_CollectionType;
/** Smart pointer for instance of IMpafRepresentationType_Condition_CollectionType */
typedef Dc1Ptr< IMpafRepresentationType_Condition_CollectionType > MpafRepresentationType_Condition_CollectionPtr;

#define TypeOfRepresentationType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Descriptor_CollectionType")))
#define CreateRepresentationType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfRepresentationType_Descriptor_CollectionType))
class MpafRepresentationType_Descriptor_CollectionType;
class IMpafRepresentationType_Descriptor_CollectionType;
/** Smart pointer for instance of IMpafRepresentationType_Descriptor_CollectionType */
typedef Dc1Ptr< IMpafRepresentationType_Descriptor_CollectionType > MpafRepresentationType_Descriptor_CollectionPtr;

#define TypeOfRepresentationType_Item_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RepresentationType_Item_CollectionType")))
#define CreateRepresentationType_Item_CollectionType (Dc1Factory::CreateObject(TypeOfRepresentationType_Item_CollectionType))
class MpafRepresentationType_Item_CollectionType;
class IMpafRepresentationType_Item_CollectionType;
/** Smart pointer for instance of IMpafRepresentationType_Item_CollectionType */
typedef Dc1Ptr< IMpafRepresentationType_Item_CollectionType > MpafRepresentationType_Item_CollectionPtr;

#define TypeOfResourceBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ResourceBaseType")))
#define CreateResourceBaseType (Dc1Factory::CreateObject(TypeOfResourceBaseType))
class MpafResourceBaseType;
class IMpafResourceBaseType;
/** Smart pointer for instance of IMpafResourceBaseType */
typedef Dc1Ptr< IMpafResourceBaseType > MpafResourceBasePtr;

#define TypeOfRightsDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RightsDescriptorType")))
#define CreateRightsDescriptorType (Dc1Factory::CreateObject(TypeOfRightsDescriptorType))
class MpafRightsDescriptorType;
class IMpafRightsDescriptorType;
/** Smart pointer for instance of IMpafRightsDescriptorType */
typedef Dc1Ptr< IMpafRightsDescriptorType > MpafRightsDescriptorPtr;

#define TypeOfRightsStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:RightsStatementType")))
#define CreateRightsStatementType (Dc1Factory::CreateObject(TypeOfRightsStatementType))
class MpafRightsStatementType;
class IMpafRightsStatementType;
/** Smart pointer for instance of IMpafRightsStatementType */
typedef Dc1Ptr< IMpafRightsStatementType > MpafRightsStatementPtr;

#define TypeOfSegmentFixityCheckType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:SegmentFixityCheckType")))
#define CreateSegmentFixityCheckType (Dc1Factory::CreateObject(TypeOfSegmentFixityCheckType))
class MpafSegmentFixityCheckType;
class IMpafSegmentFixityCheckType;
/** Smart pointer for instance of IMpafSegmentFixityCheckType */
typedef Dc1Ptr< IMpafSegmentFixityCheckType > MpafSegmentFixityCheckPtr;

// This is special, since it derives from didmodel complextype with the same name
#define TypeOfMpafStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:StatementType")))
#define CreateMpafStatementType (Dc1Factory::CreateObject(TypeOfMpafStatementType))
class MpafStatementType;
class IMpafStatementType;
/** Smart pointer to instance of IMpafStatementType */
typedef Dc1Ptr< IMpafStatementType > MpafStatementPtr;

#define TypeOfTechnicalMetadataDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:TechnicalMetadataDescriptorType")))
#define CreateTechnicalMetadataDescriptorType (Dc1Factory::CreateObject(TypeOfTechnicalMetadataDescriptorType))
class MpafTechnicalMetadataDescriptorType;
class IMpafTechnicalMetadataDescriptorType;
/** Smart pointer for instance of IMpafTechnicalMetadataDescriptorType */
typedef Dc1Ptr< IMpafTechnicalMetadataDescriptorType > MpafTechnicalMetadataDescriptorPtr;

#define TypeOfTechnicalMetadataStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:TechnicalMetadataStatementType")))
#define CreateTechnicalMetadataStatementType (Dc1Factory::CreateObject(TypeOfTechnicalMetadataStatementType))
class MpafTechnicalMetadataStatementType;
class IMpafTechnicalMetadataStatementType;
/** Smart pointer for instance of IMpafTechnicalMetadataStatementType */
typedef Dc1Ptr< IMpafTechnicalMetadataStatementType > MpafTechnicalMetadataStatementPtr;

#define TypeOfToolType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType")))
#define CreateToolType (Dc1Factory::CreateObject(TypeOfToolType))
class MpafToolType;
class IMpafToolType;
/** Smart pointer for instance of IMpafToolType */
typedef Dc1Ptr< IMpafToolType > MpafToolPtr;

#define TypeOfToolType_Manufacturer_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Manufacturer_CollectionType")))
#define CreateToolType_Manufacturer_CollectionType (Dc1Factory::CreateObject(TypeOfToolType_Manufacturer_CollectionType))
class MpafToolType_Manufacturer_CollectionType;
class IMpafToolType_Manufacturer_CollectionType;
/** Smart pointer for instance of IMpafToolType_Manufacturer_CollectionType */
typedef Dc1Ptr< IMpafToolType_Manufacturer_CollectionType > MpafToolType_Manufacturer_CollectionPtr;

#define TypeOfToolType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Name_CollectionType")))
#define CreateToolType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfToolType_Name_CollectionType))
class MpafToolType_Name_CollectionType;
class IMpafToolType_Name_CollectionType;
/** Smart pointer for instance of IMpafToolType_Name_CollectionType */
typedef Dc1Ptr< IMpafToolType_Name_CollectionType > MpafToolType_Name_CollectionPtr;

#define TypeOfToolType_Parameters_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_LocalType")))
#define CreateToolType_Parameters_LocalType (Dc1Factory::CreateObject(TypeOfToolType_Parameters_LocalType))
class MpafToolType_Parameters_LocalType;
class IMpafToolType_Parameters_LocalType;
/** Smart pointer for instance of IMpafToolType_Parameters_LocalType */
typedef Dc1Ptr< IMpafToolType_Parameters_LocalType > MpafToolType_Parameters_LocalPtr;

#define TypeOfToolType_Parameters_Param_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_Param_CollectionType")))
#define CreateToolType_Parameters_Param_CollectionType (Dc1Factory::CreateObject(TypeOfToolType_Parameters_Param_CollectionType))
class MpafToolType_Parameters_Param_CollectionType;
class IMpafToolType_Parameters_Param_CollectionType;
/** Smart pointer for instance of IMpafToolType_Parameters_Param_CollectionType */
typedef Dc1Ptr< IMpafToolType_Parameters_Param_CollectionType > MpafToolType_Parameters_Param_CollectionPtr;

#define TypeOfToolType_Parameters_Param_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:ToolType_Parameters_Param_LocalType")))
#define CreateToolType_Parameters_Param_LocalType (Dc1Factory::CreateObject(TypeOfToolType_Parameters_Param_LocalType))
class MpafToolType_Parameters_Param_LocalType;
class IMpafToolType_Parameters_Param_LocalType;
/** Smart pointer for instance of IMpafToolType_Parameters_Param_LocalType */
typedef Dc1Ptr< IMpafToolType_Parameters_Param_LocalType > MpafToolType_Parameters_Param_LocalPtr;

#define TypeOfUsageRightsDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:maf:schema:preservation:2015:UsageRightsDescriptorType")))
#define CreateUsageRightsDescriptorType (Dc1Factory::CreateObject(TypeOfUsageRightsDescriptorType))
class MpafUsageRightsDescriptorType;
class IMpafUsageRightsDescriptorType;
/** Smart pointer for instance of IMpafUsageRightsDescriptorType */
typedef Dc1Ptr< IMpafUsageRightsDescriptorType > MpafUsageRightsDescriptorPtr;

#include "Dc1PtrTypes.h"

#endif // MPAFFACTORYDEFINES_H
