/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _949MPAFTECHNICALMETADATADESCRIPTORTYPE_H
#define _949MPAFTECHNICALMETADATADESCRIPTORTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "MpafFactoryDefines.h"
#include "Mpeg21FactoryDefines.h"

#include "MpafTechnicalMetadataDescriptorTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafTechnicalMetadataDescriptorType_ExtInclude.h


class IMpafDescriptorBaseType;
typedef Dc1Ptr< IMpafDescriptorBaseType > MpafDescriptorBasePtr;
class IMpafTechnicalMetadataDescriptorType;
typedef Dc1Ptr< IMpafTechnicalMetadataDescriptorType> MpafTechnicalMetadataDescriptorPtr;
class IMpafTechnicalMetadataStatementType;
typedef Dc1Ptr< IMpafTechnicalMetadataStatementType > MpafTechnicalMetadataStatementPtr;
#include "MpafDescriptorBaseType.h"
class IMpafDescriptorBaseType_Condition_CollectionType;
typedef Dc1Ptr< IMpafDescriptorBaseType_Condition_CollectionType > MpafDescriptorBaseType_Condition_CollectionPtr;
class IMpafDescriptorBaseType_Descriptor_CollectionType;
typedef Dc1Ptr< IMpafDescriptorBaseType_Descriptor_CollectionType > MpafDescriptorBaseType_Descriptor_CollectionPtr;
#include "Mpeg21DescriptorType.h"
#include "Mpeg21DIDBaseType.h"

/** 
 * Generated interface IMpafTechnicalMetadataDescriptorType for class MpafTechnicalMetadataDescriptorType<br>
 * Located at: mpaf.xsd, line 1417<br>
 * Classified: Class<br>
 * Derived from: DescriptorBaseType (Class)<br>
 */
class MPAF_EXPORT IMpafTechnicalMetadataDescriptorType 
 :
		public IMpafDescriptorBaseType
{
public:
	// TODO: make these protected?
	IMpafTechnicalMetadataDescriptorType();
	virtual ~IMpafTechnicalMetadataDescriptorType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Statement element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMpafEBUCoreTMStatementPtr</li>
	 * <li>IMpafMPEG7TMStatementPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetStatement() const = 0;

	virtual bool IsValidStatement() const = 0;

	/**
	 * Set Statement element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMpafEBUCoreTMStatementPtr</li>
	 * <li>IMpafMPEG7TMStatementPtr</li>
	 * </ul>
	 */
	virtual void SetStatement(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Statement element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStatement() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from MpafDescriptorBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from MpafDescriptorBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get uri attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Geturi() const = 0;

	/**
	 * Get uri attribute validity information.
	 * (Inherited from MpafDescriptorBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existuri() const = 0;

	/**
	 * Set uri attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item XMLCh *
	 */
	virtual void Seturi(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate uri attribute.
	 * (Inherited from MpafDescriptorBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateuri() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Condition element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return MpafDescriptorBaseType_Condition_CollectionPtr
	 */
	virtual MpafDescriptorBaseType_Condition_CollectionPtr GetCondition() const = 0;

	/**
	 * Set Condition element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item const MpafDescriptorBaseType_Condition_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCondition(const MpafDescriptorBaseType_Condition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Descriptor element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return MpafDescriptorBaseType_Descriptor_CollectionPtr
	 */
	virtual MpafDescriptorBaseType_Descriptor_CollectionPtr GetDescriptor() const = 0;

	/**
	 * Set Descriptor element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item const MpafDescriptorBaseType_Descriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescriptor(const MpafDescriptorBaseType_Descriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of TechnicalMetadataDescriptorType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Condition</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Descriptor</li>
	 * <li>Statement</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file MpafTechnicalMetadataDescriptorType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafTechnicalMetadataDescriptorType_ExtPropDef.h

};




/*
 * Generated class MpafTechnicalMetadataDescriptorType<br>
 * Located at: mpaf.xsd, line 1417<br>
 * Classified: Class<br>
 * Derived from: DescriptorBaseType (Class)<br>
 * Defined in mpaf.xsd, line 1417.<br>
 */
class DC1_EXPORT MpafTechnicalMetadataDescriptorType :
		public IMpafTechnicalMetadataDescriptorType
{
	friend class MpafTechnicalMetadataDescriptorTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of MpafTechnicalMetadataDescriptorType
	 
	 * @return Dc1Ptr< MpafDescriptorBaseType >
	 */
	virtual Dc1Ptr< MpafDescriptorBaseType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Statement element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMpafEBUCoreTMStatementPtr</li>
	 * <li>IMpafMPEG7TMStatementPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetStatement() const;

	virtual bool IsValidStatement() const;

	/*
	 * Set Statement element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMpafEBUCoreTMStatementPtr</li>
	 * <li>IMpafMPEG7TMStatementPtr</li>
	 * </ul>
	 */
	virtual void SetStatement(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Statement element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStatement();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from MpafDescriptorBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from MpafDescriptorBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get uri attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Geturi() const;

	/*
	 * Get uri attribute validity information.
	 * (Inherited from MpafDescriptorBaseType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existuri() const;

	/*
	 * Set uri attribute.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item XMLCh *
	 */
	virtual void Seturi(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate uri attribute.
	 * (Inherited from MpafDescriptorBaseType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateuri();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Condition element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return MpafDescriptorBaseType_Condition_CollectionPtr
	 */
	virtual MpafDescriptorBaseType_Condition_CollectionPtr GetCondition() const;

	/*
	 * Set Condition element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item const MpafDescriptorBaseType_Condition_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCondition(const MpafDescriptorBaseType_Condition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Descriptor element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @return MpafDescriptorBaseType_Descriptor_CollectionPtr
	 */
	virtual MpafDescriptorBaseType_Descriptor_CollectionPtr GetDescriptor() const;

	/*
	 * Set Descriptor element.
<br>
	 * (Inherited from MpafDescriptorBaseType)
	 * @param item const MpafDescriptorBaseType_Descriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescriptor(const MpafDescriptorBaseType_Descriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of TechnicalMetadataDescriptorType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Condition</li>
	 * <li>urn:mpeg:maf:schema:preservation:2015:Descriptor</li>
	 * <li>Statement</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafTechnicalMetadataDescriptorType_ExtMyMethodDef.h


public:

	virtual ~MpafTechnicalMetadataDescriptorType();

protected:
	MpafTechnicalMetadataDescriptorType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< MpafDescriptorBaseType > m_Base;

	Dc1Ptr< Dc1Node > m_Statement;
	bool m_Statement_Exist; // For optional elements 

// no includefile for extension defined 
// file MpafTechnicalMetadataDescriptorType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _949MPAFTECHNICALMETADATADESCRIPTORTYPE_H

