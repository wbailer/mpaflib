/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _919MPAFQUALITYSTATEMENTTYPE_LOCALTYPE_H
#define _919MPAFQUALITYSTATEMENTTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "MpafDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "MpafQualityStatementType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtInclude.h


class IMpafQualityStatementType_LocalType;
typedef Dc1Ptr< IMpafQualityStatementType_LocalType> MpafQualityStatementType_LocalPtr;
class IMp7JrsMpeg7BaseType;
typedef Dc1Ptr< IMp7JrsMpeg7BaseType > Mp7JrsMpeg7BasePtr;
class IMp7JrsCompleteDescriptionType;
typedef Dc1Ptr< IMp7JrsCompleteDescriptionType > Mp7JrsCompleteDescriptionPtr;

/** 
 * Generated interface IMpafQualityStatementType_LocalType for class MpafQualityStatementType_LocalType<br>
 */
class MPAF_EXPORT IMpafQualityStatementType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMpafQualityStatementType_LocalType();
	virtual ~IMpafQualityStatementType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get DescriptionUnit element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptionUnit() const = 0;

	/**
	 * Set DescriptionUnit element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual void SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Description element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAggregatedMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsClassificationSchemeDescriptionPtr</li>
	 * <li>IMp7JrsContentEntityPtr</li>
	 * <li>IMp7JrsCreationDescriptionPtr</li>
	 * <li>IMp7JrsIndividualMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsMediaDescriptionPtr</li>
	 * <li>IMp7JrsModelDescriptionPtr</li>
	 * <li>IMp7JrsSemanticDescriptionPtr</li>
	 * <li>IMp7JrsSummaryDescriptionPtr</li>
	 * <li>IMp7JrsUsageDescriptionPtr</li>
	 * <li>IMp7JrsUserDescriptionPtr</li>
	 * <li>IMp7JrsVariationDescriptionPtr</li>
	 * <li>IMp7JrsViewDescriptionPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescription() const = 0;

	/**
	 * Set Description element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAggregatedMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsClassificationSchemeDescriptionPtr</li>
	 * <li>IMp7JrsContentEntityPtr</li>
	 * <li>IMp7JrsCreationDescriptionPtr</li>
	 * <li>IMp7JrsIndividualMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsMediaDescriptionPtr</li>
	 * <li>IMp7JrsModelDescriptionPtr</li>
	 * <li>IMp7JrsSemanticDescriptionPtr</li>
	 * <li>IMp7JrsSummaryDescriptionPtr</li>
	 * <li>IMp7JrsUsageDescriptionPtr</li>
	 * <li>IMp7JrsUserDescriptionPtr</li>
	 * <li>IMp7JrsVariationDescriptionPtr</li>
	 * <li>IMp7JrsViewDescriptionPtr</li>
	 * </ul>
	 */
	virtual void SetDescription(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtPropDef.h

};




/*
 * Generated class MpafQualityStatementType_LocalType<br>
 * Defined in mpaf.xsd, line 922.<br>
 */
class DC1_EXPORT MpafQualityStatementType_LocalType :
		public IMpafQualityStatementType_LocalType
{
	friend class MpafQualityStatementType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get DescriptionUnit element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptionUnit() const;

	/*
	 * Set DescriptionUnit element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual void SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Description element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAggregatedMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsClassificationSchemeDescriptionPtr</li>
	 * <li>IMp7JrsContentEntityPtr</li>
	 * <li>IMp7JrsCreationDescriptionPtr</li>
	 * <li>IMp7JrsIndividualMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsMediaDescriptionPtr</li>
	 * <li>IMp7JrsModelDescriptionPtr</li>
	 * <li>IMp7JrsSemanticDescriptionPtr</li>
	 * <li>IMp7JrsSummaryDescriptionPtr</li>
	 * <li>IMp7JrsUsageDescriptionPtr</li>
	 * <li>IMp7JrsUserDescriptionPtr</li>
	 * <li>IMp7JrsVariationDescriptionPtr</li>
	 * <li>IMp7JrsViewDescriptionPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescription() const;

	/*
	 * Set Description element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAggregatedMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsClassificationSchemeDescriptionPtr</li>
	 * <li>IMp7JrsContentEntityPtr</li>
	 * <li>IMp7JrsCreationDescriptionPtr</li>
	 * <li>IMp7JrsIndividualMediaReviewDescriptionPtr</li>
	 * <li>IMp7JrsMediaDescriptionPtr</li>
	 * <li>IMp7JrsModelDescriptionPtr</li>
	 * <li>IMp7JrsSemanticDescriptionPtr</li>
	 * <li>IMp7JrsSummaryDescriptionPtr</li>
	 * <li>IMp7JrsUsageDescriptionPtr</li>
	 * <li>IMp7JrsUserDescriptionPtr</li>
	 * <li>IMp7JrsVariationDescriptionPtr</li>
	 * <li>IMp7JrsViewDescriptionPtr</li>
	 * </ul>
	 */
	virtual void SetDescription(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMyMethodDef.h


public:

	virtual ~MpafQualityStatementType_LocalType();

protected:
	MpafQualityStatementType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Dc1Ptr< Dc1Node > m_DescriptionUnit;
	Dc1Ptr< Dc1Node > m_Description;
// End DefinionChoice

// no includefile for extension defined 
// file MpafQualityStatementType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _919MPAFQUALITYSTATEMENTTYPE_LOCALTYPE_H

