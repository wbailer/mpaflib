/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef MPEG21FACTORYDEFINES_H
#define MPEG21FACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for extension classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfRelatedIdentifier_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:01-DII-NS:RelatedIdentifier_LocalType")))
#define CreateRelatedIdentifier_LocalType (Dc1Factory::CreateObject(TypeOfRelatedIdentifier_LocalType))
class Mpeg21RelatedIdentifier_LocalType;
class IMpeg21RelatedIdentifier_LocalType;
/** Smart pointer for instance of IMpeg21RelatedIdentifier_LocalType */
typedef Dc1Ptr< IMpeg21RelatedIdentifier_LocalType > Mpeg21RelatedIdentifier_LocalPtr;

#define TypeOfAnchorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:AnchorType")))
#define CreateAnchorType (Dc1Factory::CreateObject(TypeOfAnchorType))
class Mpeg21AnchorType;
class IMpeg21AnchorType;
/** Smart pointer for instance of IMpeg21AnchorType */
typedef Dc1Ptr< IMpeg21AnchorType > Mpeg21AnchorPtr;

#define TypeOfAnnotationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:AnnotationType")))
#define CreateAnnotationType (Dc1Factory::CreateObject(TypeOfAnnotationType))
class Mpeg21AnnotationType;
class IMpeg21AnnotationType;
/** Smart pointer for instance of IMpeg21AnnotationType */
typedef Dc1Ptr< IMpeg21AnnotationType > Mpeg21AnnotationPtr;

#define TypeOfAssertionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:AssertionType")))
#define CreateAssertionType (Dc1Factory::CreateObject(TypeOfAssertionType))
class Mpeg21AssertionType;
class IMpeg21AssertionType;
/** Smart pointer for instance of IMpeg21AssertionType */
typedef Dc1Ptr< IMpeg21AssertionType > Mpeg21AssertionPtr;

#define TypeOfChoiceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ChoiceType")))
#define CreateChoiceType (Dc1Factory::CreateObject(TypeOfChoiceType))
class Mpeg21ChoiceType;
class IMpeg21ChoiceType;
/** Smart pointer for instance of IMpeg21ChoiceType */
typedef Dc1Ptr< IMpeg21ChoiceType > Mpeg21ChoicePtr;

#define TypeOfComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ComponentType")))
#define CreateComponentType (Dc1Factory::CreateObject(TypeOfComponentType))
class Mpeg21ComponentType;
class IMpeg21ComponentType;
/** Smart pointer for instance of IMpeg21ComponentType */
typedef Dc1Ptr< IMpeg21ComponentType > Mpeg21ComponentPtr;

#define TypeOfConditionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ConditionType")))
#define CreateConditionType (Dc1Factory::CreateObject(TypeOfConditionType))
class Mpeg21ConditionType;
class IMpeg21ConditionType;
/** Smart pointer for instance of IMpeg21ConditionType */
typedef Dc1Ptr< IMpeg21ConditionType > Mpeg21ConditionPtr;

#define TypeOfContainerType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ContainerType")))
#define CreateContainerType (Dc1Factory::CreateObject(TypeOfContainerType))
class Mpeg21ContainerType;
class IMpeg21ContainerType;
/** Smart pointer for instance of IMpeg21ContainerType */
typedef Dc1Ptr< IMpeg21ContainerType > Mpeg21ContainerPtr;

#define TypeOfDescriptorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:DescriptorType")))
#define CreateDescriptorType (Dc1Factory::CreateObject(TypeOfDescriptorType))
class Mpeg21DescriptorType;
class IMpeg21DescriptorType;
/** Smart pointer for instance of IMpeg21DescriptorType */
typedef Dc1Ptr< IMpeg21DescriptorType > Mpeg21DescriptorPtr;

#define TypeOfDIDBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:DIDBaseType")))
#define CreateDIDBaseType (Dc1Factory::CreateObject(TypeOfDIDBaseType))
class Mpeg21DIDBaseType;
class IMpeg21DIDBaseType;
/** Smart pointer for instance of IMpeg21DIDBaseType */
typedef Dc1Ptr< IMpeg21DIDBaseType > Mpeg21DIDBasePtr;

#define TypeOfFragmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:FragmentType")))
#define CreateFragmentType (Dc1Factory::CreateObject(TypeOfFragmentType))
class Mpeg21FragmentType;
class IMpeg21FragmentType;
/** Smart pointer for instance of IMpeg21FragmentType */
typedef Dc1Ptr< IMpeg21FragmentType > Mpeg21FragmentPtr;

#define TypeOfItemType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ItemType")))
#define CreateItemType (Dc1Factory::CreateObject(TypeOfItemType))
class Mpeg21ItemType;
class IMpeg21ItemType;
/** Smart pointer for instance of IMpeg21ItemType */
typedef Dc1Ptr< IMpeg21ItemType > Mpeg21ItemPtr;

#define TypeOfResourceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:ResourceType")))
#define CreateResourceType (Dc1Factory::CreateObject(TypeOfResourceType))
class Mpeg21ResourceType;
class IMpeg21ResourceType;
/** Smart pointer for instance of IMpeg21ResourceType */
typedef Dc1Ptr< IMpeg21ResourceType > Mpeg21ResourcePtr;

#define TypeOfSelectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:SelectionType")))
#define CreateSelectionType (Dc1Factory::CreateObject(TypeOfSelectionType))
class Mpeg21SelectionType;
class IMpeg21SelectionType;
/** Smart pointer for instance of IMpeg21SelectionType */
typedef Dc1Ptr< IMpeg21SelectionType > Mpeg21SelectionPtr;

#define TypeOfStatementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg21:2002:02-DIDMODEL-NS:StatementType")))
#define CreateStatementType (Dc1Factory::CreateObject(TypeOfStatementType))
class Mpeg21StatementType;
class IMpeg21StatementType;
/** Smart pointer for instance of IMpeg21StatementType */
typedef Dc1Ptr< IMpeg21StatementType > Mpeg21StatementPtr;

#include "Dc1PtrTypes.h"

#endif // MPEG21FACTORYDEFINES_H
