/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


// Heavily inspired by the boost smart_ptr class 
// and Andrei Alexandrescu, "Modern C++ Design"

#ifndef DC1_POINTER_INCLUDED
#define DC1_POINTER_INCLUDED

#if defined(WIN32)
// FTT: To get rid of nasty 
// "c:\Programme\Microsoft Visual Studio .NET 2003\Vc7\PlatformSDK\Include\MsXml.h(9594): fatal error C1001: INTERNAL COMPILER ERROR"
#	ifndef WIN32_LEAN_AND_MEAN
#		define WIN32_LEAN_AND_MEAN
#	endif
#	include <windows.h>
#	pragma warning( disable : 4290 )
#elif defined(__APPLE__)
#	include <pthread.h>
#elif defined(__linux__)
#	include <pthread.h>
#else
#	error TODO: Add header for mutex type.
#endif


#include "Dc1Exception.h"
#include "Dc1Util.h"

class Dc1Node;

class DC1_EXPORT Dc1PtrException : public Dc1Exception {

public:

	Dc1PtrException(Dc1ExSeverity sev, const XMLCh* msg) 
		: Dc1Exception(sev, msg) 
	{}

	Dc1PtrException(Dc1ExSeverity sev, const char* msg) 
		: Dc1Exception(sev, msg) 
	{}
};




class Dc1Node;

class DC1_EXPORT Dc1counter {

private:
#if defined(WIN32)
	CRITICAL_SECTION crit_sect_;
#elif defined(__APPLE__)
	pthread_mutex_t mutex_;
#elif defined(__linux__)
	pthread_mutex_t mutex_;
#else
#	error("TODO: Define Platform-specific mutex type.")
#endif

	// use count is always <= weak count.  When use count drops to
	// zero, the pointer can be deleted.  When weak count drops to
	// zero, counter self-destructs.
	long use_count_;
	long weak_count_;

private:
	// private: each counter is unique
	Dc1counter(Dc1counter const &);
	Dc1counter & operator=(Dc1counter const &);

public:
	Dc1counter(Dc1Node *p) : use_count_(1), weak_count_(1), ptr(p)
	{
#if defined(WIN32)
		InitializeCriticalSection(&crit_sect_);
#elif defined(__APPLE__)
		pthread_mutex_init(&mutex_, NULL);	
#elif defined(__linux__)
		pthread_mutex_init(&mutex_, NULL);
#else
#	error("TODO: Define platform-specific mutex init func.")
#endif
	}

	~Dc1counter()
	{
#if defined(WIN32)
		DeleteCriticalSection(&crit_sect_);
#elif defined(__APPLE__)
		pthread_mutex_destroy(&mutex_);
#elif defined(__linux__)
		pthread_mutex_destroy(&mutex_);
#else
#	error("TODO: Define platform-specific mutex destroy func.")
#endif
	}

public:
	// Create a counter.  Don't do this via new in a method defined 
	// in this file, because it would be allocated in the heap of
	// whatever program that included the header.
	static Dc1counter *makeCounter(Dc1Node *p);
	
	// baw 22 04 2004
	// Delete a counter, don't do this via delete in this file,
	// because it would be attempted to delete from the wrong heap.
	static void deleteCounter(Dc1counter* cntr);

protected:

	class _lock;
	friend class _lock;

	class _lock
	{
	private:

		Dc1counter & b_;

		_lock(_lock const &);
		_lock & operator=(_lock const &);

	public:

		explicit _lock(Dc1counter & b): b_(b)
		{
#if defined(WIN32)
			EnterCriticalSection(&b_.crit_sect_);
#elif defined(__APPLE__)
			pthread_mutex_lock(&(b_.mutex_));
#elif defined(__linux__)
			pthread_mutex_lock(&(b_.mutex_));
#else
#			error("TODO: Define platform-specific mutex lock func.")
#endif
		}

		~_lock()
		{
#if defined(WIN32)
			LeaveCriticalSection(&b_.crit_sect_);
#elif defined(__APPLE__)
			pthread_mutex_unlock(&(b_.mutex_));
#elif defined(__linux__)
			pthread_mutex_unlock(&(b_.mutex_));
#else
#			error("TODO: Define platform-specific mutex unlock func.")
#endif
		}
	};

	// Called when weak_count_ drops to zero
	void destruct()
	{
		// baw 22 04 2004: use static method of counter class for deleting it
		//Dc1Util::DeleteInDll(this);
		Dc1counter::deleteCounter(this);
	}

	// Called when use_count_ drops to zero; responsible for releasing
	// the managed pointer
	void dispose()
	{
		disposeUsingFactory(ptr);
	}

	// Helper function: non-template so it can be defined in a
	// cpp-file, defined in a cpp file so Dc1Factory.h doesn't have
	// to be included here.
	void disposeUsingFactory(Dc1Node *ptr);

public:

	void add_ref()
	{
		_lock lock(*this);
		if(use_count_ <= 0) throw Dc1PtrException(DC1_FATAL_ERROR, "Attempted to add reference to invalid pointer.");
		++use_count_;
		++weak_count_;
	}

	void release()
	{
		{
			_lock lock(*this);
			long new_use_count = --use_count_;

			if(new_use_count > 0)
			{
				--weak_count_;
				return;
			}
		}
		dispose();
		weak_release();
	}

	void weak_add_ref() // nothrow
	{
		_lock lock(*this);
		++weak_count_;
	}

	void weak_release() // nothrow
	{
		long new_weak_count;

		{
			_lock lock(*this);
			new_weak_count = --weak_count_;
		}

		if(new_weak_count == 0)
		{
			destruct();
		}
	}

	long use_count() const
	{
		_lock lock(const_cast<Dc1counter &>(*this));
		return use_count_;
	}

private:
	Dc1Node *ptr;	// the managed pointer

};



template<class P>
class Dc1WeakPtr;

/** Smart pointer template */
template<class P>
class Dc1Ptr {

public:

	Dc1Ptr()
		: ptr(0), use_count_(0)
	{}

	template<class W>
	explicit Dc1Ptr(const W *p)
	{
		if (!p) {
			ptr = 0;
			use_count_ = 0;
		} else {
			W *dummy = const_cast<W *>(p);
			ptr = dynamic_cast<P *>(dummy);
			if (ptr)
				use_count_ = Dc1counter::makeCounter(dynamic_cast<Dc1Node *>(ptr));
			else {
				use_count_ = NULL;
#ifdef ASSIGNMENT_EXCEPTION
				// cast failed because of invalid assignment type
				throw Dc1PtrException(DC1_ERROR, "Invalid cast. Attempted to assign invalid pointer type.");
#endif
			}
		}
	}

	template<class W>
	Dc1Ptr(const Dc1Ptr<W>& p)
	{
		if (*(p.__count())) {
			(*p.__count())->add_ref();
			// It is now safe to access p.Ptr()
			ptr = dynamic_cast<P *>(p.Ptr());
			if (ptr) {
				use_count_ = const_cast<Dc1counter *>(*(p.__count()));
			}
			else {
				// Couldn't convert p.Ptr() to our type -> drop reference
				(*p.__count())->release();
				use_count_ = NULL;
#ifdef ASSIGNMENT_EXCEPTION
				// cast failed because of invalid assignment type
				throw Dc1PtrException(DC1_ERROR, "Invalid cast. Attempted to assign invalid pointer type.");
#endif
			}
		} else {
			// p is a null pointer, so are we
			ptr = NULL;
			use_count_ = NULL;
		}
	}

	// overwrite default copy constructor -- a template constructor is
	// never a default constructor, even if its argument would match.
	// VC++6 needs this after the template constructor
	Dc1Ptr(const Dc1Ptr<P>& p)
	{
		if (p.use_count_) {
			p.use_count_->add_ref();
			// It is now safe to access p.ptr
			ptr = p.ptr;
			use_count_ = p.use_count_;
		} else {
			// p is a null pointer, so are we
			ptr = NULL;
			use_count_ = NULL;
		}
	}

	explicit Dc1Ptr(const Dc1WeakPtr<P>& p)
	{
		if (p.__count()) {
			(*p.__count())->add_ref();
			// It is now safe to access p.Ptr()
			ptr = dynamic_cast<P *>(p.Ptr());
			if (ptr) {
				use_count_ = const_cast<Dc1counter *>(*(p.__count()));
			}
			else {
				// Couldn't convert p.Ptr() to our type -> drop reference
				(*p.__count())->release();
				use_count_ = NULL;
#ifdef ASSIGNMENT_EXCEPTION
				// cast failed because of invalid assignment type
				throw Dc1PtrException(DC1_ERROR, "Invalid cast. Attempted to assign invalid pointer type.");
#endif
			}
		} else {
			// p is a null pointer, so are we
			ptr = NULL;
			use_count_ = NULL;
		}
	}

	~Dc1Ptr()
	{
		if (use_count_) {
			use_count_->release();
		}
	}

	template<class W>
	Dc1Ptr<P>& operator= (const Dc1Ptr<W>& p)
	{
		if (p != *this) {
			// Implement assignment in terms of construction
			Dc1Ptr<P> temp(p);
			swap(temp);
		}
		return *this;
	}

	// overwrite default assignment operator -- a template assignment
	// operator is never the default assignment operator, even if its
	// arguments would match
	// VC++6 needs this after the template operator=
	Dc1Ptr<P>& operator= (const Dc1Ptr<P>& p)
	{
		if (p != *this) {
			// Implement assignment in terms of construction
			Dc1Ptr<P> temp(p);
			swap(temp);
		}
		return *this;
	}

	template<class W>
	bool operator== (const Dc1Ptr<W>& p) const
	{
		return ptr == p.Ptr();
	}

	template<class W>
	bool operator< (const Dc1Ptr<W>& p) const
	{
		return ptr < p.Ptr();
	}

	template<class W>
	bool operator> (const Dc1Ptr<W>& p) const
	{
		return ptr > p.Ptr();
	}

	template<class W>
	bool operator!= (const Dc1Ptr<W>& p) const
	{
		return !operator==(p);
	}

	// enables  if (!smartptr) ...
	bool operator! () const
	{
		return ptr == NULL;
	}

	// enables  if (smartptr == 0)
	inline friend bool operator==(const Dc1Ptr<P> &lhs, const P *rhs)
	{
		return lhs.ptr == rhs;
	}

	// enables  if (0 == smartptr)
	inline friend bool operator==(const P *lhs, const Dc1Ptr<P> &rhs)
	{
		return lhs == rhs.ptr;
	}

	// enables  if (smartptr != 0)
	inline friend bool operator!=(const Dc1Ptr<P> &lhs, const P *rhs)
	{
		return lhs.ptr != rhs;
	}

	// enables  if (0 != smartptr)
	inline friend bool operator!=(const P *lhs, const Dc1Ptr<P> &rhs)
	{
		return lhs != rhs.ptr;
	}

	// TODO: perhaps write templated versions of the above four operators,
	// so that Smart Ptrs can be compared against normal pointers of
	// another type.

	// See: Alexei Alexandrescu, "Modern C++ Design", p. 178
private:
	class Tester
	{
		void operator delete(void *);
	};

public:
	// enables  if (smartptr)
	operator Tester*() const
	{
		if (!ptr) return 0;
		static Tester test;
		return &test;
	}

	P& operator*() const
	{
		if (ptr)
			return *ptr;
		else
			throw Dc1PtrException(DC1_FATAL_ERROR, "Attempted to dereference null pointer.");
	}

	P* operator->() const
	{
		if (ptr)
			return ptr;
		else
			throw Dc1PtrException(DC1_FATAL_ERROR, "Attempted to dereference null pointer.");
	}

	inline P* Ptr() const
	{
		return ptr;
	}

	inline Dc1counter * const *__count() const
	{
		// HACK - not for external use
		return &use_count_;
	}

	inline Dc1counter **__count()
	{
		// HACK - not for external use
		return &use_count_;
	}

/*
	// Auto-conversion is evil, but necessary so Smart Ptrs are
	// transparent to old extensions expecting raw pointers
	// TODO: can this go now?
	inline operator P *() const
	{
		return Ptr();
	}
*/

private:

	void swap(Dc1Ptr<P>& p)
	{
		P *temp = ptr;
		ptr = p.ptr;
		p.ptr = temp;
		Dc1counter *tmp = use_count_;
		use_count_ = p.use_count_;
		p.use_count_ = tmp;
	}

private:
	P *ptr;
	Dc1counter *use_count_;
};


template<class P>
class Dc1WeakPtr {
public:

	Dc1WeakPtr()
		: ptr(0), use_count_(0)
	{}

	template<class W>
	Dc1WeakPtr(const Dc1Ptr<W>& p)
	{
		if (*(p.__count())) {
			(*p.__count())->add_ref();
			// It is now safe to access p.Ptr()
			ptr = dynamic_cast<P *>(p.Ptr());
			if (ptr) {
				use_count_ = const_cast<Dc1counter *>(*(p.__count()));
				use_count_->weak_add_ref();
				use_count_->release(); // release add_ref() from above
			}
			else {
				// Couldn't convert p.Ptr() to our type -> drop reference
				(*p.__count())->release();
				use_count_ = NULL;
#ifdef ASSIGNMENT_EXCEPTION
				// cast failed because of invalid assignment type
				throw Dc1PtrException(DC1_ERROR, "Invalid cast. Attempted to assign invalid pointer type.");
#endif
			}
		} else {
			// p is a null pointer, so are we
			ptr = NULL;
			use_count_ = NULL;
		}
	}

	Dc1WeakPtr(const Dc1WeakPtr<P>& p)
	{
		Dc1Ptr<P> temp(p.getPointer());
		// It is now safe to access the pointer.
		use_count_ = *(temp.__count());
		ptr = p.Ptr();
		if (use_count_) use_count_->weak_add_ref();
	}

	~Dc1WeakPtr()
	{
		if (use_count_) {
			use_count_->weak_release();
		}
	}

	bool expired() const
	{
		if (use_count_)
			return use_count_->use_count() <= 0;
		else
			return true;
	}

	Dc1Ptr<P> getPointer() const
	{
		if (expired())
			return Dc1Ptr<P>();

		try {
			return Dc1Ptr<P>(*this);
		} catch (Dc1PtrException const &) {
			return Dc1Ptr<P>();
		}
	}

	template<class W>
	Dc1WeakPtr<P>& operator= (const Dc1Ptr<W>& p)
	{
		// Implement assignment in terms of construction
		Dc1WeakPtr<P> temp(p);
		swap(temp);
		return *this;
	}

	template<class W>
	bool operator== (const Dc1Ptr<W>& p) const
	{
		return ptr == p.Ptr();
	}

	template<class W>
	bool operator< (const Dc1Ptr<W>& p) const
	{
		return ptr < p.Ptr();
	}

	template<class W>
	bool operator> (const Dc1Ptr<W>& p) const
	{
		return ptr > p.Ptr();
	}

	template<class W>
	bool operator!= (const Dc1Ptr<W>& p) const
	{
		return !operator==(p);
	}

	template<class W>
	bool operator== (const Dc1WeakPtr<W>& p) const
	{
		return ptr == p.Ptr();
	}

	template<class W>
	bool operator< (const Dc1WeakPtr<W>& p) const
	{
		return ptr < p.Ptr();
	}

	template<class W>
	bool operator> (const Dc1WeakPtr<W>& p) const
	{
		return ptr > p.Ptr();
	}

	template<class W>
	bool operator!= (const Dc1WeakPtr<W>& p) const
	{
		return !operator==(p);
	}


	inline P* Ptr() const
	{
		// Unsafe at any speed
		return ptr;
	}

	inline Dc1counter * const *__count() const
	{
		// HACK - not for external use
		return &use_count_;
	}

	inline Dc1counter **__count()
	{
		// HACK - not for external use
		return &use_count_;
	}

/*
	// Auto-conversion is evil, but necessary so Smart Ptrs are
	// transparent to old extensions expecting raw pointers
	// TODO: can this go now?
	inline operator P *() const
	{
		return Ptr();
	}
*/

private:

	void swap(Dc1WeakPtr<P>& p)
	{
		P *temp = ptr;
		ptr = p.ptr;
		p.ptr = temp;
		Dc1counter *tmp = use_count_;
		use_count_ = *(p.__count());
		p.use_count_ = tmp;
	}

private:
	P *ptr;
	Dc1counter *use_count_;
};

#endif // DC1_POINTER_INCLUDED
