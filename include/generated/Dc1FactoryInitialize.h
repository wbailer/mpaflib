/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Initialize section of Dc1Factory.h
// included in Dc1Factory::Initialize()

{
	// Register default namespace uri and its prefix(es), so that we can resolve it without Xerces XML DOM
	// Note, First entry must be default namespaceuri
	Dc1Factory::SetNamespaceUri(X("dc1"), X("http://purl.org/dc/elements/1.1/"));

	Dc1Factory * factory;
	
	// This factory wraps all unknown xsi:types
	factory = new Dc1UnknownFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	// This has been moved to w3c lib
	// This factory wraps all IDREFS
	// factory = new < % =type.NameAlias % >Factory; // Create and install factory
	// factory->SetToDelete(true); // We are owner - responsible for destruction
	// This has been moved to w3c lib
	// This factory wraps all NMTOKENS
	// factory = new < % =type.NameAlias % >Factory; // Create and install factory
	// factory->SetToDelete(true); // We are owner - responsible for destruction
	// This has been moved to w3c lib
	// This factory wraps all ENTITIES
	// factory = new < % =type.NameAlias % >Factory; // Create and install factory
	// factory->SetToDelete(true); // We are owner - responsible for destruction
	factory = new Dc1elementsGroupFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementsGroup_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementsGroup_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementsGroup1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementsGroup1_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementsGroup1_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Dc1elementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

}
