/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1_TYPES_H_INCLUDED
#define DC1_TYPES_H_INCLUDED

// smart pointer type definitions for hand coded classes
// baw 11 08 2003

class Dc1Archive;
typedef Dc1Ptr<Dc1Archive> Dc1ArchivePtr;

class Dc1ClientManager;
typedef Dc1Ptr<Dc1ClientManager> Dc1ClientManagerPtr;

class Dc1Document;
typedef Dc1Ptr<Dc1Document> Dc1ClientDocumentPtr;

class Dc1Node;
typedef Dc1Ptr<Dc1Node> Dc1NodePtr;

class Dc1NodeLockNotification;
typedef Dc1Ptr<Dc1NodeLockNotification> Dc1NodeLockNotificationPtr;

class Dc1NodeUpdateNotification;
typedef Dc1Ptr<Dc1NodeUpdateNotification> Dc1NodeUpdateNotificationPtr;
class Dc1Notification;
typedef Dc1Ptr<Dc1Notification> Dc1NotificationPtr;

class Dc1NotificationListener;
typedef Dc1Ptr<Dc1NotificationListener> Dc1NotificationListenerPtr;

class Dc1Unknown;
typedef Dc1Ptr<Dc1Unknown> Dc1UnknownPtr;

#endif /* DC1_TYPES_H_INCLUDED */
