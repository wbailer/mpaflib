/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1_UTIL_H
#define DC1_UTIL_H

#include "Dc1Defines.h"

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/util/regx/Match.hpp>

// check for MS GetNextSibling macro
// show custom error to avoid cryptic error message when macro has been executed
//#ifdef GetNextSibling
//#error Macro GetNextSibling from windowsx.h has been found, which causes error in Dc1Util code. Make sure that windowsx.h is included after Dc1Util.h .
//#endif

// FTT TODO outsource this into its own header file.
/** Helperclass for namespace handling */
#ifndef NAMESPACEHELPER_DEFINED
typedef struct nshelper
{
	nshelper(){NamespaceUri = Prefix = LocalName = 0;}
	~nshelper(){Reset();}
	void Reset(){XMLString::release(&NamespaceUri); XMLString::release(&LocalName); XMLString::release(&Prefix);}
	bool IsValid() const {return LocalName != 0;}
	
	bool operator ==(const nshelper & other) const{return XMLString::compareString(LocalName, other.LocalName) == 0 && XMLString::compareString(NamespaceUri, other.NamespaceUri) == 0;}
	bool operator !=(const nshelper & other) const{return ! (*this == other);}
	
	XMLCh * NamespaceUri;
	XMLCh * Prefix;
	XMLCh * LocalName;
} NamespaceHelper;
#define NAMESPACEHELPER_DEFINED
#endif // 

// This is a little helper for typenames
// It is a macro, since the signature of typeid() is typeid(expression), 
// which can not be used as param for a function.
// Basically, it returns a stripped typename - without "class "
// Note: Not the holy grail of software engineering, but it will do its job until V3 comes along...
#ifndef GETTYPENAME
#define GETTYPENAME(t) \
(strrchr(typeid(t).name(), ' ')?strrchr(typeid(t).name()+1, ' ') : typeid(t).name())
#endif // GETTYPENAME(t)

/** Utility class */
class DC1_EXPORT Dc1Util
{
public:

	/** 
	 * Use this function to avoid troubles with Xerces heap management.
	 * Since Xerces uses its own heap, you should not create a XML string
	 * like XMLCh * tmp = new XMLCh[123]; // our heap
	 * and release it with XMLString::release(tmp) // the other heap
	 * More worse, Xerces 2.2.0 does not provide XMLString::allocate(), so our
	 * wrapper uses XMLString::replicate to create it on the correct heap.
	 */
	static XMLCh * allocate(long length);
	
	/** 
	 * Releases strings which are allocated with <code>allocate()</code> (counterpart).
	 * @param src the address of the XMLCh pointer. This method sets the pointer to NULL as well.
	 */
	static void deallocate(XMLCh** src);

	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh * CatString(const XMLCh * const target, const XMLCh * const src);
	static void CatString(XMLCh ** target, const XMLCh * const src);

	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh* ConvertToXMLCh(XMLSSize_t val);

#if XERCES_VERSION_MAJOR < 3
#else
	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh* ConvertToXMLCh(XMLFileLoc val);
#endif // XERCES_VERSION_MAJOR < 3	
	

	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh * PadLeft(const XMLCh * const target, const XMLCh * const fill, const unsigned int maxlen);
	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh * PadRight(const XMLCh * const target, const XMLCh * const fill, const unsigned int maxlen);

	/** Previously GetNextSibling, but unfortunately there is a MS macro!!! with the same name. */
	static DOMElement * GetNextAkin(DOMElement * currentnode);
	static DOMElement * GetNextChild(DOMElement * currentnode);
	
	/** Note, you have to free the allocated memory with deallocate() */
	static XMLCh * GetElementText(XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * currentnode);

	static int GetNextEndPos(Match & m, int i);

	/** Find prefix for given namespace 
	 * @return the prefix string or NULL if not found or no prefix set.
	 */
	static const XMLCh * LookupNamespacePrefix(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, const XMLCh * ns);
	
	/** Find namespace URI for given prefix
	 * @return the URI or NULL if not found.
	 */
	static const XMLCh * LookupNamespaceUri(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, const XMLCh * prefix);

	/** Adds an xsi:type if needed 
	 * @param element  the DOM element candidate for the xsi attribute.
	 * @param doc  the XML document the element is added to.
	 * @param usexsitype  if true, xsi type will be set if not already done.
	 * @param ns  the namespace of the type. Used to figure out the prefix.
	 * @param elementtype  the name of the element type.
	 * @return true, if xsi type has been added, false otherwise.
	 */
	static bool HandleXsiType(DOMElement * element, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, const bool usexsitype, const XMLCh * ns, const XMLCh * elementtype);

	/** Adds a textnode if not empty 
	 * @param element  the parent DOM element for the text node.
	 * @param doc  the XML document, needed for namespace handling.
	 * @param data  the textnode content.
	 * @param ns  the namespace of the content. Used to figure out the prefix.
	 * @param name  the name of the element type.
	 * @param isoptional  if true, the node is not written in case of empty (null) data.
	 * @return true, if a textnode has been added, false otherwise.
	 */
	static bool SerializeTextNode(DOMElement * element, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc
	, const XMLCh * data, const XMLCh * ns, const XMLCh * name, bool isoptional = false);

	/** Adds a textnode if not empty 
	 * @param node  the DOM node containing a text node.
	 * @param name  the name of the element type.
	 * @param ns  the namespace of the content.
	 * @return true, if the node name is correct.
	 */
	static bool HasNodeName(DOMNode * node, const XMLCh * name, const XMLCh * ns);

	/** Extracts the xsi:type if set.
	 * @param node  the DOM node possibly containing an xsi:type.
	 * @param doc  the DOM document used for namespace resolving.
	 * @param xsitype  if xsi:type is valid, the container is filled with URI, prefix and name of the xsi:type, if existing, otherwise not. You mustnot delete the contents of this result.
	 * @return true, if the xsi:type has been found, false otherwise.
	 */
	static bool GetXsiType(DOMElement * node, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, NamespaceHelper * xsitype);


	/** Returns a static string of the name without the prefix (if any).
	 * @param name  the DOM nodename with or without prefix.
	 * @return a string, which is just a pointer inside the name param pointing to the sucessor of a ":".
	 * @warning Don't use the X() macro for name since its lifetime ends after the call and hence result is pointing to nowhere.
	 */
	static const XMLCh * LocalName(const XMLCh * name);

	/** Returns an allocated string of the prefix (if any) without the name. You have to deallocate the string with XMLString::release().
	 * @param name  the DOM nodename with or without prefix.
	 * @return an allocated string with a copy of the local name without the ":" separator.
	 */
	static XMLCh * Prefix(const XMLCh * name);

	/** Generates a qualified name in the format "namespaceURI : name". 
	 * @param the NamespaceHelper containing namespace and name.
	 * @return the combined output, which must be deallocated afterwards.
	 */
	static XMLCh * QualifiedName(const NamespaceHelper * val);

// FTT To remain binary compatible
	/** Generates a qualified name in the format "namespaceURI : name". 
	 * @param nsuri the namespaceURI
	 * @param name the local name
	 * @return the combined output, which must be deallocated afterwards.
	 */
	static XMLCh * QualifiedName(const XMLCh * nsuri, const XMLCh * name);

private:
	//Unimplemented
	Dc1Util();
	~Dc1Util();
};

#endif // DC1_UTIL_H
