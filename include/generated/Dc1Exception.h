/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1EXCEPTION_H
#define DC1EXCEPTION_H

#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

typedef enum {
	DC1_WARNING,
	DC1_ERROR,
	DC1_FATAL_ERROR
} Dc1ExSeverity;

typedef enum {
	DC1_AUTODELETE, // Message created on stack, nothing to do
	DC1_NOAUTODELETE, // Message created on our heap (NOT Xerces heap), we have to destroy it.
} Dc1MsgMemory;

/** Primary exception class. */
class DC1_EXPORT Dc1Exception 
{
public:
	/**
	 * This is version 2.0
	 */
	Dc1Exception(Dc1ExSeverity sev, const XMLCh * msg, Dc1MsgMemory mode = DC1_AUTODELETE) 
		: _severity(sev) { 
		_msg = XMLString::transcode(msg); // Xerces heap
		if(mode == DC1_NOAUTODELETE && msg){
			XMLCh * tmp = const_cast<XMLCh*>(msg);
			delete [] tmp; // Our heap
		}		
	}

	/**
	 * This is version 2.0
	 */
	Dc1Exception(Dc1ExSeverity sev, const char * msg, Dc1MsgMemory mode = DC1_AUTODELETE) 
		: _severity(sev) { 
		_msg = XMLString::replicate(msg);  // Xerces heap
		if(mode == DC1_NOAUTODELETE && msg){
			char * tmp = const_cast<char*>(msg);
			delete [] tmp; // Our heap
		}
	}

	// Copy ctor, otherwise we leave a memory leak!
	Dc1Exception(const Dc1Exception & other)
	{
		_severity = other._severity;
		_msg = XMLString::replicate(other._msg);  // Xerces heap
	}
	
	virtual ~Dc1Exception()
	{
		XMLString::release(&_msg); // Xerces heap, _msg is reset to NULL
	}
	
	/** Get severity identifier */
	Dc1ExSeverity GetSeverity() { return _severity; };

	/** Get the severity as string. */
	const char* GetSeverityText() {
		switch (_severity) {
		case DC1_WARNING: return "Warning"; 
		case DC1_ERROR: return "Error";
		case DC1_FATAL_ERROR: return "Fatal error";
		default: return "";
		};
	}

	/** Get a descriptive error message. 
	  * Since the original GetMessage() method is overloaded 
	  * in Windows with a Microsoft macro and expands to either
	  * GetMessageA or GetMessageW, we change our method to avoid this.
	  */
	const char* GetMessageText() { return _msg; }

private:
	Dc1ExSeverity _severity;
	char *_msg;
};

#endif
