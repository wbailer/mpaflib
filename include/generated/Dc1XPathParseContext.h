/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef DC1_XPATHPARSECONTEXT_H
#define DC1_XPATHPARSECONTEXT_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Util.h"


#include "Dc1Defines.h"
#include "Dc1Node.h"
#include "Dc1Enumerator.h"
#include "Dc1ValueVectorOf.h" 

// Intro of API documentation
#include "docintro.h"


typedef enum {
	DC1_XPATH_UNKNOWN = 0,

	DC1_XPATH_ROOT, // /
	DC1_XPATH_UP, // ../

	
	// The following operations are allowed for Get() and GetOrCreate()
	DC1_XPATH_SELECT_ALL, // 
	DC1_XPATH_SELECT_TYPED_ALL, // [@xsi:type='bla']

	DC1_XPATH_SELECT_INDEX, // [i]
	DC1_XPATH_SELECT_INDEXED_TYPE, // [i][@xsi:type]
	DC1_XPATH_SELECT_TYPED_INDEX, // [@xsi:type][i]

	DC1_XPATH_SELECT_LAST, // [last()]
	DC1_XPATH_SELECT_LAST_TYPE, // [last()][xsi:type]
	DC1_XPATH_SELECT_TYPED_LAST, // [xsi:type][last()]

	// Force node creation if in create mode
	DC1_XPATH_FORCE_CREATE, // [position()>last()] [position()>last()][xsi:type] [xsi:type][position()>last()]

	// The following operations are allowed for Get() only
	DC1_XPATH_SELECT_POSITION_RANGE, // [position()>A | position()<B]
	DC1_XPATH_SELECT_POSITION_RANGE_TYPE, // [position()>A | position()<B][xsi:type]
	DC1_XPATH_SELECT_TYPED_POSITION_RANGE, // [xsi:type][position()>A | position()<B]

	DC1_XPATH_SELECT_POSITION_LT, // [position()<A]
	DC1_XPATH_SELECT_POSITION_LT_TYPE, // [position()<A][xsi:type]
	DC1_XPATH_SELECT_TYPED_POSITION_LT, // [xsi:type][position()<A]

	DC1_XPATH_SELECT_POSITION_GT, // [position()>A]
	DC1_XPATH_SELECT_POSITION_GT_TYPE, // [position()>A][xsi:type]
	DC1_XPATH_SELECT_TYPED_POSITION_GT, // [xsi:type][position()>A]

} Dc1XPathOperation;

typedef Dc1ValueVectorOf<int> Dc1IndexVector;


/** Dc1 XPath parsing context. 
 * Deals with all XPath related activities.
 */
struct DC1_EXPORT Dc1XPathParseContext
{
	Dc1XPathParseContext();
	virtual ~Dc1XPathParseContext();
	static bool Initialise(Dc1XPathParseContext & c, Dc1XPathParseContext ** context, const XMLCh *xpath, Dc1XPathCreation flag);

	// Main routine
	static void Parse(Dc1XPathParseContext * context);

	// Parsing helpers
	static void Reset(Dc1XPathParseContext * context);
	static bool ScanSlash(Dc1XPathParseContext * context);
	static bool ScanUp(Dc1XPathParseContext * context);
	static bool ScanTail(Dc1XPathParseContext * context);
	static bool ScanAttribute(Dc1XPathParseContext * context);
	static bool ScanElement(Dc1XPathParseContext * context);
	static bool ScanIndex(Dc1XPathParseContext * context);
	static bool ScanXsiType(Dc1XPathParseContext * context);
	static bool ConvertIndex(int startpos, int endpos, int minval, Dc1XPathParseContext * context, int & n);

	static Dc1NodePtr CreateNode(Dc1XPathParseContext * context, unsigned int defaulttype);

	// Simple attribute
	static void PreProcessSimpleAttribute(Dc1XPathParseContext * context);
	static bool HandleSimpleAttribute(Dc1XPathParseContext * context, Dc1NodePtr element);

	// Simple element
	static void PreProcessSimpleElement(Dc1XPathParseContext * context);
	static bool HandleSimpleElement(Dc1XPathParseContext * context, Dc1NodePtr element);

	// Collections
	static bool PreProcessCollection(Dc1XPathParseContext * context, unsigned int collsize, int upperboundary);
	static int HandleCollection(Dc1XPathParseContext * context, int i, Dc1NodePtr element, Dc1NodeEnum & result);
	static bool PostProcessElementCollection(Dc1XPathParseContext * context, int & i, Dc1NodeEnum & result);
	static bool PostProcessSequenceCollection(Dc1XPathParseContext * context, int & i, int & start, int & stop, Dc1NodeEnum & result);
	static bool PostProcessChoiceCollection(Dc1XPathParseContext * context, int & i, Dc1NodeEnum & result);


	// TODO Moving
	static Dc1NodeEnum Root(Dc1XPathParseContext * context, const Dc1NodePtr node, Dc1ClientID client);
	static Dc1NodeEnum Up(Dc1XPathParseContext * context, const Dc1NodePtr node, Dc1ClientID client);


	// Some exceptions
	static void ErrorBoundary(Dc1XPathParseContext * context);
	static void ErrorUnallowedExpression(Dc1XPathParseContext * context);
	static void ErrorAbstractType(Dc1XPathParseContext * context);
	static void ErrorInvalidIndex(Dc1XPathParseContext * context);
	static void ErrorInvalidEmptyItem(Dc1XPathParseContext * context, int i);
	static void ErrorNoFillingGaps(Dc1XPathParseContext * context, unsigned int lastvalidindex);
	static void ErrorCannotSetAttribute(Dc1XPathParseContext * context);
	static void ErrorCannotSetElement(Dc1XPathParseContext * context);
	static void ErrorUnknownSubElement(Dc1XPathParseContext * context, const char * type);
	static void ErrorNoSubElement(Dc1XPathParseContext * context, const char * type);
	static void ErrorUnsupportedType(Dc1XPathParseContext * context, const char * type);
	static void ErrorUnsupportedAttributeType(Dc1XPathParseContext * context, const char * type);
	static void ErrorWrongCaller(const XMLCh * xpath);
	static void ErrorNotImplemented(const XMLCh * xpath, const char * type);

	// FTT Workaround for setting content data in XPath
	static bool ContentFromString(Dc1Node * node, const XMLCh * const txt);
	// FTT Workaround for getting content data in XPath
	static XMLCh * ContentToString(const Dc1Node * node);

	// FTT Using our locationmap for now, TODO: change the name to something like xsitype_to_typeid()
	// FTT Changed name from ResolveNamespace to
	static XMLCh * ResolveTypeName(const XMLCh * xsitype);


	Dc1XPathOperation Operation;
	bool Found;
	XMLCh * ElementName;
	XMLCh * XsiType;
	const XMLCh * XPath; // Global, used for error messages
	XMLCh * UnparsedXPath;
	int StartIndex; // -3 = take all, -2 = take last
	int StopIndex;

	bool Create; // Global flag
	bool PreferType;
	bool IsAttribute;
	unsigned int Id;
	int CollectionSize;
	int UpperBoundary;

	int ItemCount; // Used to count [i] ...
	int ValidItemCount; // For [xsi:type i] or [i xsi:type]
	int LastItemIndex;
	int LastValidItemIndex;
	Dc1NodePtr LastNode;
	Dc1IndexVector FreeIndexList;
	XMLCh * QualifiedXsiType; // Format "URI:TYPENAME"

};

#endif // DC1XPATHPARSECONTEXT_H
