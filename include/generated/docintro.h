/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


/*! \mainpage The Dc1 library
 *
 * \section intro Introduction
 *
 * This lib is part of the MP-AF library 
 *
 * \section about About this API documentation
 *
 * This set of HTML pages is an automatically generated API documentation of the Dc1 classes. It is intended as a reference for a developer using Dc1. If you are looking for an overview description of the library, instructions on how to setup a project using Dc1 and some commented examples, please look at the document located in the /doc subdirectory of the Dc1 distribution.
 * <br>
 * This API documentation contains the following:
 * <ul>
 * <li>class descriptions, including the extension base type and the schema file and line number where they are defined</li>
 * <li>descriptions of methods, including a reference to the class from which they are inherited</li>
 * <li>if the type of parameters or return values is abstract, a list of allowed types is provided</li>
 * <li>for collections, a reference to the local type which is the default type of collection elements</li>
 * </ul>
 *
 
 * \section extensions Extensions
 
 * Additional documentation for extensions can be found under
 * <ul> 
_doc/html/index.html" > 
%>
 * <li>EbuCore</li>
 
_doc/html/index.html" > 
%>
 * <li>Mp7Jrs</li>
 
_doc/html/index.html" > 
%>
 * <li>Mpeg21</li>
 
_doc/html/index.html" > 
%>
 * <li>Mpaf</li>
 
_doc/html/index.html" > 
%>
 * <li>W3C</li>
 
 * </ul>
 * All you need to extend types of the core library, is an XML schema,
 * which must be - of course - an extension of the core schema and the extented types
 * must have their own namespace(s). Then an additional library can easily be created
 * along with the core lib.
 *	
 * For more information about this, please contact <a href="http://www.joanneum.at/en/digital/avm/products-solutions-services/mpeg-7-library.html">DIGITAL-AVM (formerly IIS)</a> at JOANNEUM RESEARCH.
 * 
 * \section news Whats new?
 *
 * Version 2.5:<br>
 * Links against bugfixed XercesC 3.3.1 now.
 * <br>Builds on 64 bit platforms (Win64 VC9, LINUX 64 bit, MacOS 10.5 64 bit).
 * <br>With adjusted CMAKE file to build and install libs and doc on LINUX the more natural way.
 * <br>Example: <code>cmake -G "Unix Makefiles" . , make, make doc, sudo make install</code>.
 *
 * Version 2.4:<br>
 * Switched to MPEG-7 standard 2004 (using namespace xmlns:mpeg7="urn:mpeg:mpeg7:schema:2004")
 * Fixed a lot of bugs - mostly concerning extension libraries. 
 *
 * Version 2.3:<br>
 * Full XML namespace support. Dc1 serialises XML with full namespace declaration in the root element.
 * If you define a default namespace, no prefix is used. Xerces V.2.7 does not allow both - a default namespace beeing
 * declared the second time with a prefix as well. So you can either choose between the default namespace or the prefixed version
 * of your (default) namespace. Having all namespaces prefixed is very handy for XSLTs.
 * To do so, use <code>myArchive.SetUsePrefixForDefaultNS(true);</code>
 *
 * On the contrary, if you generate MPEG-7 fragments with your own or other third-party tools, and read them with 
 * Dc1 library, you must provide a proper namespace declaration in the fragment root
 * <br>e.g.: <code><mpeg7:MyFragment xmlns:mpeg7="urn:mpeg:mpeg7:schema:2004" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">My_Data</mpeg7:MyFragment></code>
 * to read it properly. If you don't use XSI types, you can suppress the declaration of XSI namepace via 
 * <code>myArchive.SetForceNamespaceDeclaration(false);</code>
 
 *
 * Version 2.2:<br>
 * ATTENTION: A stunning new method for accessing nodes has been introduced.
 * Instead of dealing with difficult intermediate types (and a huge \#include section), it is strongly recommended to use 
 * <code>GetFromXPath()</code> or <code>GetOrCreateFromXPath()</code> methods now.
 * So one single call retrieves a list of desired nodes or creates a complete path with one or more nodes.
 *
 * Example:<br>
 * <code>root.GetFromXPath("A/B/C[last()]/D[3]/E[@xsi:type="Desired"]</code> to get all desired nodes with this type.
 * or:<br>
 * <code>rootnode.GetOrCreateFromXPath("A/B/C[last()]/D[3]/E[@xsi:type="Desired"]</code> creates a node with this type (if there was none before).
 *
 * For a detailed list of supported XPath expressions please read the class documentation.<br>
 * These two methods should be sufficient to handle your requests properly.
 *
 *
 * ATTENTION: The methods <code>Archive::ToWBuffer()</code> and <code>Archive::ToBuffer()</code> are using 
 * Xerces heap now. You *MUST* call the appropriate <code>Archive::DeleteBuffer()</code> methods now
 * , otherwise memory errors will occur! 
 *
 * ATTENTION: The macros for <code>GetObjectType</code> and <code>GetTypeId</code> have been removed,
 * in order to get rid of nasty library problems with existing MS libs! If you need these definitions,
 * redefine them in your source code.
 *
 * ATTENTION: The macro <code>GetClassType</code> has been removed. Use the correspondend <code>GetTypeName()</code>
 * method instead. If you need this definition, please do it in your source code.
 *
 * ATTENTION: The method <code>GetNextSibling()</code> was renamed to <code>GetNextAkin()</code> because
 * of interferences with a macro from MS XML library...
 * If you need this definition, please do it in your source code.
 *
 * Other changes:
 * <ul>
 * <li>Subelements of type string are now created automatically (as empty string) if they are mandatory.
 * <li>Abstract classes automatically handle the <code>UseTypeAttribute</code> flag, needed for correct serialisation.</li>
 * <li>Interface setter methods additionally create VS7 IntelliSense comments for mandatory elements.</li>
 * <li>Library version string can be queried with <code>GetVersion()</code> method.</li>
 * </ul>
 
 */
