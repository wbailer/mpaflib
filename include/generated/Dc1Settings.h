/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1SETTINGS_H
#define DC1SETTINGS_H

#pragma once

#include "Dc1Defines.h"

/** Dc1 settings.
  * A singleton class which contains all global settings.
  * This class is NOT threadsafe.
  */
class DC1_EXPORT Dc1Settings
{

public:
	/** @name Constructors and Destructors
	 */
	//@{

	static Dc1Settings & GetInstance()
	{
		static Dc1Settings instance; // Guaranteed to be destroyed. Instantiated on first use.
		return instance;
	}
	//@}


	/** Triggers special treatment of MediaTime in AVDP.
		In MPEG-7 V2 MediaTime offers a choice of MediaTimePoint, MediaRelTimePoint and MediaRelIncrTimePoint.
		In AVDP we only have MediaRelIncrTimePoint (+ and optional MediaIncrDuration)

		Since we don't want an extra lib for AVDP
		the MediaTimePoint is used to retrieve the absolute timepoint
		calculated from the MediaRelIncrTimePoint and the mediaTimeUnit and mediaTimeBase
		taken from an outer DOM element (like AudioVisual).
		mediaTimeBase points to a MediaLocator, that now contains an absolute TimePoint
		so that MediaTimePoint (absolute) = MediaRelIncrTimePoint * mediaTimeUnit + mediaTimeBase (from MediaLocator)
		This behaviour is only available if 
		Dc1Settings.instance().TreatRelIncrTimeAsAbsolute is set to true.

		Default setting: false.
	*/
	bool TreatRelIncrTimeAsAbsolute;


private:
	Dc1Settings(void)
	{
		TreatRelIncrTimeAsAbsolute = false;
	}

	Dc1Settings(Dc1Settings const&);
	void operator=(Dc1Settings const&);
};

#endif
