/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


// -*- c++ -*-

#ifndef DC1_ENUMERATOR_INCLUDED
#define DC1_ENUMERATOR_INCLUDED

//#include "Dc1ValueVectorOf.h"
#include "Dc1Ptr.h"

class Dc1Node;

// Allocation and deallocation have to happen in the library so we can
// pass objects over DLL boundaries.
DC1_EXPORT Dc1Ptr<Dc1Node> * allocateVector(unsigned int size);

void DC1_EXPORT deallocateVector(Dc1Ptr<Dc1Node> *vect);


/** Enumerator template */
template<class C>
class Dc1Enumerator {

public:

	Dc1Enumerator()
		: content_length(0)
		, current_outpos(0)
		, max_outpos(0)
		, content(0)
	{
		content = allocateVector(20);
		content_length = 20;
	}

	Dc1Enumerator(const Dc1Enumerator<C>& e)
		: content_length(0)
		, current_outpos(0)
		, max_outpos(0)
		, content(0)
	{
		content = allocateVector(e.content_length);
		content_length = e.content_length;
		unsigned int i;
		for (i = 0; i < e.Size(); i++) {
			content[i] = e.content[i];
		}
		current_outpos = e.current_outpos;
		max_outpos = e.max_outpos;
	}

	~Dc1Enumerator()
	{
		Clear();
		deallocateVector(content);
	}

	Dc1Enumerator<C>& operator= (const Dc1Enumerator<C>& e)
	{
		if (&e != this) {
			// Implement assignment in terms of construction
			Dc1Enumerator<C> temp(e);
			Swap(temp);
		}
		return *this;
	}

	bool operator== (const Dc1Enumerator<C>& e) const
	{
		return this == &e;
	}

	bool operator!= (const Dc1Enumerator<C>& e) const
	{
		return !operator==(e);
	}

	/** Checks if there are more elements in the enumerator when iterating in forward direction.
	  * @return True if there are more elements, false otherwise. */
	bool HasNext() const
	{
		return (current_outpos < Size());
	}

	/** Checks if there are more elements in the enumerator when iterating in reverse direction.
	  * @return True if there are more elements, false otherwise. */
	bool HasPrevious() const
	{
		return current_outpos > 0;
	}

	/** Gets the next element.
	  * @return A smart pointer to the next element. */
	C GetNext()
	{
		if (!HasNext())
			return C();
		else
			return content[current_outpos++];
	}

	/** Gets the previous element.
	  * @return A smart pointer to the previous element. */
	C GetPrevious()
	{
		if (!HasPrevious())
			return C();
		else
			return content[current_outpos--];
	}

	/** Gets the element at the specified position.
	  * @param i The position of the element to be retrieved. 
	  * @return The element at the specified position.
	  */
	C ElementAt(unsigned int i) const
	{
		if (i >= 0 && i < Size())
			return content[i];
		else
			return C();
	}

	/** Gets the first element.
	  */
	C GetFirst() const
	{ 
		return ElementAt(0); 
	}

	/** Gets the last element.
	  */
	C GetLast() const
	{ 
		return ElementAt(Size() - 1); 
	}

	/** Removes all elements from the enumerator.
	  */
	void Clear()
	{
		unsigned int i;
		for (i=0; i < Size(); i++) {
			// Overwrite vector so that destructors of stored elements
			// are called
			content[i] = C();
		}

		max_outpos = 0;
		current_outpos = 0;
	}

	/** Adds an item to the enumeration.
	  */
	void Insert(const C &item) 
	{
		if (max_outpos >= content_length) {
			unsigned int new_content_length;
			C *new_content;
			unsigned int i;

			new_content_length = (unsigned int)((content_length + 1) * 1.3);
			new_content = allocateVector(new_content_length);
			for (i = 0; i < Size(); i++) {
				new_content[i] = content[i];
			}
			deallocateVector(content);
			content = new_content;
			content_length = new_content_length;
		}
		content[max_outpos++] = item;
	}

	/** Adds the contents of an enumeration to this enumeration.
	  */
	void Insert(const Dc1Enumerator<C> & e) 
	{
		while(e.HasNext())
			Insert(const_cast< Dc1Enumerator<C> & >(e).GetNext());
	}

	/** Gets the number of elements of the enumerator.
	  */
	unsigned int Size() const
	{
		return max_outpos;
	}

private:

	void Swap(Dc1Enumerator<C>& e)
	{
		C *tmp;
		unsigned int tmp_2;

		tmp = content;
		content = e.content;
		e.content = tmp;

		tmp_2 = current_outpos;
		current_outpos = e.current_outpos;
		e.current_outpos = tmp_2;
		tmp_2 = max_outpos;
		max_outpos = e.max_outpos;
		e.max_outpos = tmp_2;
		tmp_2 = content_length;
		content_length = e.content_length;
		e.content_length = tmp_2;
	}

private:

	unsigned int content_length, current_outpos, max_outpos;
	Dc1Ptr<Dc1Node> *content;
};

typedef Dc1Enumerator< Dc1Ptr<Dc1Node> > Dc1NodeEnum;

#endif // DC1_ENUMERATOR_INCLUDED
