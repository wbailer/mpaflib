/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef DC1FACTORY_H
#define DC1FACTORY_H

#include "Dc1Defines.h"

#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"

#include "Dc1Exception.h"

typedef const char * SimpleGuid; // Extensible library version with real guids. Note, the const is to make GCC happy

class DC1_EXPORT Dc1LicenseException : public Dc1Exception 
{
	public:
	Dc1LicenseException(const char* msg) : Dc1Exception(DC1_ERROR,msg) {}  
};

/** Core factory base class. */
class DC1_EXPORT Dc1Factory
{

public:
	/** @name Initialization
	 */
	//@{
	/** Initialize factory registry. */
	static void Initialize();
	
	/** Clean up factory registry. */
	static void Terminate();

	/** Returns the version string of the library. This normally changes, if the XML schema has changed. */
	static const char * GetVersion();
	
	/** Returns the version string of the source code design model for this library. This normally changes, if a template has been adjusted. */
	static const char * GetModelVersion();
	
	//@}

	/** Map the possibly unordered type id to an ordered index. Note: this is used for registering only! */
	static unsigned int MapGuidToIndex(SimpleGuid id);

	/** Create a node object with type given as index */
	static Dc1NodePtr CreateObject(unsigned int index);
	
	/** Clone the given node */
	static Dc1NodePtr CloneObject(const Dc1NodePtr &original);

	/** Get the classname associated with the given factory index. */
	static const char * GetTypeName(unsigned int index);
	/** Get the factory index (i.e. type id) for a certain classname. */
	static unsigned int GetTypeIndex(const XMLCh * qname);

	/** Delete the given object. */
	static void DeleteObject(Dc1Node * item);

	/** "Delete" the smart pointer (nothing to do) */
	template<class P>
	static void DeleteObject(Dc1Ptr<P> & /*item*/) {}

	/** Set if the factory must be deleted when cleaning up the factory registry */
	void SetToDelete(bool todelete) {m_ToDelete = todelete;}

	/** Get information, if factory must be deleted when factory registry is cleaned up. */
	bool GetToDelete() const {return m_ToDelete;}

	// FTT For binary compatibility
	static void SetNamespaceUri(const XMLCh * prefix, const XMLCh * nsuri);
	// FTT For binary compatibility
	static const XMLCh * GetNamespaceUri(const XMLCh * prefix);
	// FTT For binary compatibility
	static void UnregisterFactory(SimpleGuid id);

protected:
	Dc1Factory(SimpleGuid id, const char * classname);
	virtual ~Dc1Factory(void); // Don't use delete, use DeleteFactory() instead;

	virtual void DeleteFactory();

	virtual Dc1NodePtr CreateObject() = 0;
	virtual const char * GetTypeName() const = 0;
	virtual void DeleteRawObject(Dc1Node * item);

private: 
	static int initialized;
	
	// Unimplemented methods
	Dc1Factory(const Dc1Factory &);

protected:
	// False, if the factory is created on stack (default)
	// Must be set to true when created on heap.
	bool m_ToDelete;

	// We must know our ordered class id for adding it to our generated nodes
	unsigned int m_ClassId;
	
};

#endif // DC1FACTORY_H
