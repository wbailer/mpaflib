/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1_DEFINES_H
#define DC1_DEFINES_H

#ifndef DC1_VERSION
#	define DC1_VERSION 0100
#endif // DC1_VERSION

// use DLL version, if static version is not explicitely forced
#ifndef DC1_STATIC
#	ifndef DC1_DLL
#		define DC1_DLL
#	endif
#endif

#if defined(WIN32)
// For DLL you must define DC1_DLL and DC1_DECLSPEC_EXPORT
#	ifdef DC1_DLL
#		ifdef DC1_DECLSPEC_EXPORT
#			define DC1_EXPORT __declspec(dllexport)
#		else
#			define DC1_EXPORT __declspec(dllimport)
#		endif
#	else
#		define DC1_EXPORT
#	endif // DC1_DLL
#elif defined(__APPLE__)
#	define DC1_EXPORT
#elif defined(__linux__)
#	define DC1_EXPORT
#else
#	error("TODO: Adjust platform specific export methods.")
#endif



// From WinDef.h
#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#if defined(WIN32) // FTT XML_VISUALCPP is not defined in Xerces  >= 3.0.
// sat 2003-08-06
// Shut up compiler about long identifiers
#pragma warning( disable : 4786 )

// suppress warning about missing DLL interface of ptr template
#pragma warning( disable : 4251 )
#endif // WIN32 // XML_VISUALCPP


// baw 12 02 2003
// modification to support Xerces 2.2.0 and later
// uses the Xerces namespace if it has been defined
XERCES_CPP_NAMESPACE_USE


// baw 07 07 2003
// definitions for lock status and client identification
typedef enum {
	DC1_NO_LOCKS_USED,
	DC1_UNLOCKED,
	DC1_LOCKED,
	DC1_LOCKED4ADD
} Dc1LockType;

typedef long Dc1ClientID;

#define DC1_UNDEFINED_CLIENT ((Dc1ClientID) -1)

// string conversion class

class DC1_EXPORT Dc1XStr
{
public :
    Dc1XStr(const char* const toTranscode)
    {
        // Call the private transcoding method
        fUnicodeForm = XMLString::transcode(toTranscode);
		fNonUnicodeForm = (char*)NULL;
    }

	Dc1XStr(const XMLCh* const toTranscode) 
	{
		fNonUnicodeForm = XMLString::transcode(toTranscode);
		fUnicodeForm = (XMLCh*)NULL;
	}

    ~Dc1XStr()
    {
        XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&fUnicodeForm);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&fNonUnicodeForm);
    }

    XMLCh* unicodeForm() const
    {
        return fUnicodeForm;
    }

    char* nonUnicodeForm() const
    {
        return fNonUnicodeForm;
    }

private :
    XMLCh*   fUnicodeForm;
	char*	 fNonUnicodeForm;
};

#define X(str) Dc1XStr(str).unicodeForm()
#define Xinv(str) Dc1XStr(str).nonUnicodeForm()

typedef enum {
	DC1_NOTIF_LOCK,
	DC1_NOTIF_UPDATE
} Dc1NotificationType;

typedef enum {
	DC1_NA_LOCK_REQUESTED,
	DC1_NA_LOCK_GRANTED,
	DC1_NA_UNLOCK,
	DC1_NA_NODE_ADDED,
	DC1_NA_NODE_INSERTED,
	DC1_NA_NODE_UPDATED
} Dc1NotificationAction;


#endif // DC1_DEFINES_H
