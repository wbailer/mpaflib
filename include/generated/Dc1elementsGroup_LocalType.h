/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _5DC1ELEMENTSGROUP_LOCALTYPE_H
#define _5DC1ELEMENTSGROUP_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"

#include "Dc1elementsGroup_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtInclude.h


class IDc1elementsGroup_LocalType;
typedef Dc1Ptr< IDc1elementsGroup_LocalType> Dc1elementsGroup_LocalPtr;
class IDc1elementType;
typedef Dc1Ptr< IDc1elementType > Dc1elementPtr;

/** 
 * Generated interface IDc1elementsGroup_LocalType for class Dc1elementsGroup_LocalType<br>
 */
class DC1_EXPORT IDc1elementsGroup_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IDc1elementsGroup_LocalType();
	virtual ~IDc1elementsGroup_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get title element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Gettitle() const = 0;

	/**
	 * Set title element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Settitle(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get creator element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcreator() const = 0;

	/**
	 * Set creator element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcreator(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get subject element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getsubject() const = 0;

	/**
	 * Set subject element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setsubject(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get description element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getdescription() const = 0;

	/**
	 * Set description element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setdescription(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get publisher element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getpublisher() const = 0;

	/**
	 * Set publisher element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setpublisher(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get contributor element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcontributor() const = 0;

	/**
	 * Set contributor element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcontributor(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get date element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getdate() const = 0;

	/**
	 * Set date element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setdate(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get type element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Gettype() const = 0;

	/**
	 * Set type element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Settype(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get format element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getformat() const = 0;

	/**
	 * Set format element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setformat(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get identifier element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getidentifier() const = 0;

	/**
	 * Set identifier element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setidentifier(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get source element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getsource() const = 0;

	/**
	 * Set source element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setsource(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get language element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getlanguage() const = 0;

	/**
	 * Set language element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setlanguage(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relation element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrelation() const = 0;

	/**
	 * Set relation element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setrelation(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get coverage element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcoverage() const = 0;

	/**
	 * Set coverage element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcoverage(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get rights element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrights() const = 0;

	/**
	 * Set rights element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setrights(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtPropDef.h

};




/*
 * Generated class Dc1elementsGroup_LocalType<br>
 * Defined in simpledc20021212.xsd, line 50.<br>
 */
class DC1_EXPORT Dc1elementsGroup_LocalType :
		public IDc1elementsGroup_LocalType
{
	friend class Dc1elementsGroup_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get title element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Gettitle() const;

	/*
	 * Set title element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Settitle(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get creator element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcreator() const;

	/*
	 * Set creator element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcreator(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get subject element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getsubject() const;

	/*
	 * Set subject element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setsubject(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get description element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getdescription() const;

	/*
	 * Set description element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setdescription(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get publisher element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getpublisher() const;

	/*
	 * Set publisher element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setpublisher(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get contributor element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcontributor() const;

	/*
	 * Set contributor element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcontributor(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get date element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getdate() const;

	/*
	 * Set date element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setdate(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get type element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Gettype() const;

	/*
	 * Set type element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Settype(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get format element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getformat() const;

	/*
	 * Set format element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setformat(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get identifier element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getidentifier() const;

	/*
	 * Set identifier element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setidentifier(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get source element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getsource() const;

	/*
	 * Set source element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setsource(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get language element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getlanguage() const;

	/*
	 * Set language element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setlanguage(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relation element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrelation() const;

	/*
	 * Set relation element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setrelation(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get coverage element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getcoverage() const;

	/*
	 * Set coverage element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setcoverage(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get rights element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrights() const;

	/*
	 * Set rights element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setrights(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMyMethodDef.h


public:

	virtual ~Dc1elementsGroup_LocalType();

protected:
	Dc1elementsGroup_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Dc1elementPtr m_title;
	Dc1elementPtr m_creator;
	Dc1elementPtr m_subject;
	Dc1elementPtr m_description;
	Dc1elementPtr m_publisher;
	Dc1elementPtr m_contributor;
	Dc1elementPtr m_date;
	Dc1elementPtr m_type;
	Dc1elementPtr m_format;
	Dc1elementPtr m_identifier;
	Dc1elementPtr m_source;
	Dc1elementPtr m_language;
	Dc1elementPtr m_relation;
	Dc1elementPtr m_coverage;
	Dc1elementPtr m_rights;
// End DefinionChoice

// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _5DC1ELEMENTSGROUP_LOCALTYPE_H

