/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1_CLIENT_MANAGER_H
#define DC1_CLIENT_MANAGER_H

#include "Dc1Defines.h"
#include "Dc1NotificationListener.h"
#include "Dc1Ptr.h"
#include "Dc1Node.h"

#include <xercesc/util/XMLString.hpp>
#include "Dc1ValueVectorOf.h"

// NOTE:
// In order to deal with xerces-c > 2.2.0
// *DON'T* use <KeyValuePair.hpp>!!!
// Use "Dc1KeyValuePair.h" instead.
// Each stable version above 2.4. declares operator =() in KeyValuePair as private 
// which is consistent in terms of memory management
// but inconsistent in terms of usability.
// i.e.: it makes a KeyValuePair unusable for all xerces collection classes ...
// As it turns out, the utility template KeyValuePair is not used in Xerces itself.
#include "Dc1KeyValuePair.h" 


typedef Dc1KeyValuePair<Dc1ClientID,Dc1NotificationListener*> ClientListenerPair;
typedef Dc1ValueVectorOf<ClientListenerPair> ClientMap;

typedef Dc1KeyValuePair<Dc1ClientID,Dc1NodePtr> ClientNodePair;
typedef Dc1ValueVectorOf<ClientNodePair> LockQueue;

typedef Dc1ValueVectorOf<Dc1ClientID> Dc1ClientList;

// BAW 26 02 2004: added explicit == operator for compatibility with VC++ .NET 2003
typedef struct _Subtree {
	Dc1NodePtr node;
	bool recursive;

	bool operator==(const _Subtree& second) const { return node==second.node; }
} Subtree;

// BAW 26 02 2004: derived to add explicit == operator for compatibility with VC++ .NET 2003
class DC1_EXPORT Dc1ClientRNodePair : public Dc1KeyValuePair<Dc1ClientID,Subtree> {
public:
	Dc1ClientRNodePair() : Dc1KeyValuePair<Dc1ClientID,Subtree>() {}
	Dc1ClientRNodePair(Dc1ClientID id, Subtree subtree) : Dc1KeyValuePair<Dc1ClientID,Subtree>(id, subtree) {}

	bool operator==(const Dc1ClientRNodePair& second) const { return (getKey()==second.getKey()) && (getValue()==second.getValue()) ; }
};

#if defined(XML_VISUALCPP)
// SAT: REMOVEME -- fix this warning when there's more time
#pragma warning( disable : 4275 )
#endif

class DC1_EXPORT Dc1ListenerList : public Dc1ValueVectorOf<Dc1ClientRNodePair> {
public:
	Dc1ListenerList();

	Dc1ClientList GetClientsToBeNotified(Dc1NodePtr node);

};


/** Manages clients accessing Dc1 and need to register for locks and write access. */
class DC1_EXPORT Dc1ClientManager {
friend class Dc1Node;
public:
	/** Get a new client ID.
	  * @param listener Object implementing the listener interface which will be registered with the new ID
	  * to receive notifications.
	  */
	static Dc1ClientID GetNewID(Dc1NotificationListener* listener);

	/** Get the listener associated with the given client ID. */
	static Dc1NotificationListener* GetListener(Dc1ClientID id);

	static void Unregister(Dc1ClientID id);

	// SAT: this needs to be accessible from all node subtypes as well
	static void SendUpdateNotification(Dc1NodePtr sender, Dc1NotificationAction action, Dc1ClientID client, Dc1NodePtr second, int index = 0);

 	static void SetNotificationsEnabled(bool enabled);

	static void SetLockingEnabled(bool enabled);

	static bool IsNotificationsEnabled();

	static bool IsLockingEnabled();

protected:

	static void QueueForLock(Dc1NodePtr sender, Dc1ClientID client, bool forAdd);

	// send notifications

	static bool SendLockRequestNotification(Dc1NodePtr sender, Dc1ClientID client, bool forAdd, bool notify);

	static void SendLockNotification(Dc1NodePtr sender, Dc1ClientID client, bool forAdd);

	static void SendUnlockNotification(Dc1NodePtr sender);
	
	// register / unregister on node level
	static void Register(Dc1ClientID id, Dc1NotificationAction action, Dc1NodePtr node, bool recursive = false);
	
	static void Unregister(Dc1ClientID id, Dc1NotificationAction action, Dc1NodePtr node);
	

private:
	static Dc1ClientID	_maxID;
	static ClientMap		_clientMap;	
	
	static LockQueue		_lockQueue;

	static Dc1ListenerList		_lockRequested;
	static Dc1ListenerList		_lockGranted;
	static Dc1ListenerList		_unlocked;
	static Dc1ListenerList		_added;
	static Dc1ListenerList		_inserted;
	static Dc1ListenerList		_replaced;

	static bool				_lockingEnabled;
	static bool				_notificationsEnabled;
};

#endif /* DC1_CLIENT_MANAGER_H */
