/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef DC1FACTORYDEFINES_H
#define DC1FACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for core classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfelementsGroup (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup")))
#define CreateelementsGroup (Dc1Factory::CreateObject(TypeOfelementsGroup))
class Dc1elementsGroup;
class IDc1elementsGroup;
/** Smart pointer for instance of IDc1elementsGroup */
typedef Dc1Ptr< IDc1elementsGroup > Dc1elementsGroupPtr;

#define TypeOfelementsGroup_CollectionType (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup_CollectionType")))
#define CreateelementsGroup_CollectionType (Dc1Factory::CreateObject(TypeOfelementsGroup_CollectionType))
class Dc1elementsGroup_CollectionType;
class IDc1elementsGroup_CollectionType;
/** Smart pointer for instance of IDc1elementsGroup_CollectionType */
typedef Dc1Ptr< IDc1elementsGroup_CollectionType > Dc1elementsGroup_CollectionPtr;

#define TypeOfelementsGroup_LocalType (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup_LocalType")))
#define CreateelementsGroup_LocalType (Dc1Factory::CreateObject(TypeOfelementsGroup_LocalType))
class Dc1elementsGroup_LocalType;
class IDc1elementsGroup_LocalType;
/** Smart pointer for instance of IDc1elementsGroup_LocalType */
typedef Dc1Ptr< IDc1elementsGroup_LocalType > Dc1elementsGroup_LocalPtr;

#define TypeOfelementsGroup1 (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup1")))
#define CreateelementsGroup1 (Dc1Factory::CreateObject(TypeOfelementsGroup1))
class Dc1elementsGroup1;
class IDc1elementsGroup1;
/** Smart pointer for instance of IDc1elementsGroup1 */
typedef Dc1Ptr< IDc1elementsGroup1 > Dc1elementsGroup1Ptr;

#define TypeOfelementsGroup1_CollectionType (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup1_CollectionType")))
#define CreateelementsGroup1_CollectionType (Dc1Factory::CreateObject(TypeOfelementsGroup1_CollectionType))
class Dc1elementsGroup1_CollectionType;
class IDc1elementsGroup1_CollectionType;
/** Smart pointer for instance of IDc1elementsGroup1_CollectionType */
typedef Dc1Ptr< IDc1elementsGroup1_CollectionType > Dc1elementsGroup1_CollectionPtr;

#define TypeOfelementsGroup1_LocalType (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementsGroup1_LocalType")))
#define CreateelementsGroup1_LocalType (Dc1Factory::CreateObject(TypeOfelementsGroup1_LocalType))
class Dc1elementsGroup1_LocalType;
class IDc1elementsGroup1_LocalType;
/** Smart pointer for instance of IDc1elementsGroup1_LocalType */
typedef Dc1Ptr< IDc1elementsGroup1_LocalType > Dc1elementsGroup1_LocalPtr;

#define TypeOfelementType (Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))
#define CreateelementType (Dc1Factory::CreateObject(TypeOfelementType))
class Dc1elementType;
class IDc1elementType;
/** Smart pointer for instance of IDc1elementType */
typedef Dc1Ptr< IDc1elementType > Dc1elementPtr;

#include "Dc1PtrTypes.h"

#endif // DC1FACTORYDEFINES_H
