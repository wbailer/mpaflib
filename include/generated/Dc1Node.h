/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef DC1_NODE_H
#define DC1_NODE_H

// Intro of API documentation
#include "docintro.h"

#include "Dc1Defines.h"
#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"
#include "Dc1Enumerator.h"

#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/sax/InputSource.hpp>
#include <xercesc/framework/XMLFormatter.hpp>

// This macro has been removed in version 2.0.
// If you need this macro, please do it in your own source code!
// Method GetClassType has been renamed to GetTypeName()
// #define GetClassType GetTypeName


// The baseclass for all factories
class Dc1Factory;

// Helperclass for XPath functions
struct Dc1XPathParseContext;

typedef enum {
	DC1_NOCREATE, // Get existing nodes or nothing.
	DC1_FORCECREATE, // Create missing nodes
} Dc1XPathCreation; // Must come before 


/** Basic Dc1 node type. */
class DC1_EXPORT Dc1Node
{

friend class Dc1Factory;

public:
	/** Get class type name as string. */
	const char * GetTypeName() const;

	/** Get the class ID of this node (i.e. the MPEG-7 type it represents. */
	const unsigned int GetClassId() const;

	/** Set the content name of this node. */
	void SetContentName(XMLCh * contentname);

	/** Get the content name of this node */
	const XMLCh * GetContentName() const;

	/** Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types return the class name.
	  * @return The converted content of this class or NULL if something goes terribly wrong. */
	virtual XMLCh * ToText() const = 0;

	/** Parse the content of this type. Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types return false.
	  * @param txt buffer to parse from.
	  * @return True if parsing was successful, false otherwise..
	  */
	virtual bool Parse(const XMLCh * const txt) = 0;

	/** Parse the content from a string. Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	  * @param txt buffer to parse from.
	  * @return This default implementation always return false.
	  */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. 
	 	 * @return This default implementation always return NULL.
	 	 */
	virtual XMLCh * ContentToString() const;

	/** Serialialize this object to a DOM tree.
	  * @param doc DOM document.
	  * @param parent Parent node in DOM tree.
	  */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL) = 0;

	/** Deserialize this object from a DOM tree.
	  * @param doc DOM document.
	  * @param parent Parent element in DOM tree.
	  * @param current Current element in DOM tree.
	  */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current) = 0;

	/* Creates and inserts a childnode of the specified type
	 * @deprecated In order to reduce API size (KISS), this method will be removed in the next version.
	 * @param elementname The tag type of the child to be created (XML element name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);

	/** Get the absolute XPath expression describing this node.
	  * This method is experimental...
	  * @return An absolute XPath expression describing this node.
	  */
	virtual XMLCh *GetXPath() const;

	/** Get the node represented by this xpath.
	  * @deprecated In order to reduce API size (KISS), this method will be removed in the next version.
	  * @return A node pointer or NULL if an invalid xpath argument was given.
	  * NOTE: This method implements only a very restricted subset of
	  * XPath: only the child path is supported, only a single node is
	  * returned.
	  */
	Dc1NodePtr NodeFromXPath(const char *xpath) const;
	virtual Dc1NodePtr NodeFromXPath(const XMLCh *xpath) const;

	/** Get the node(s) represented by this xpath.
	  * Missing nodes are created and inserted/appended automatically.
	  * Note, we don't fill in missing elements in between.
	  * If a collection contains 2 elements and you are requesting the 4th, then an exception is thrown.
	  * @return An enumeration of nodes, which may be empty.
	  * NOTE: This method implements only a very restricted subset of
	  * XPath: xsi:type attributes are allowed, and the order of 
	  * collection indexes and xsi:types is taken into account.
	  * 
	  * For more information, please see the class documentation
	  */
	Dc1NodeEnum GetOrCreateFromXPath(const char *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE);
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);
	
	/**
	  * The same like GetOrCreateFromXPath() just read only.
	  * For more information, please see the class documentations of GetOrCreateFromXPath.
	  */
	Dc1NodeEnum GetFromXPath(const char *xpath ) /* const */;
	/**
	  * The same like GetOrCreateFromXPath() just read only.
	  * For more information, please see the class documentations of GetOrCreateFromXPath.
	  */
	Dc1NodeEnum GetFromXPath(const XMLCh *xpath) /* const */;


protected:
	// Consume leading "..", go upward in tree, then continue with
	// xpath from there.
	Dc1NodePtr UpwardWithXPath(const XMLCh *xpath) const;
	
public:

	/** Get the parent of the node.
	  * @return A pointer to the parent of the node or NULL if this is the root node of a tree fragment.
	  */
	virtual Dc1NodePtr GetParent() const;

	/** @name Locking
	 */
	//@{

	/** Request to lock this node for any operation.
	  * @param id ID of the client that wants to lock this node.
	  * @param receiveNotification Determines whether the client will be notifified later, in the case that the
	  *	lock cannot be granted immediately.
	  * @return True if the node has been locked, false otherwise.
	  * <b>NOTE: Not implemented, will currently always return true.</b>
	  */
	virtual bool Lock(Dc1ClientID id, bool receiveNotification = true);

	/** Request to lock this node for add or insert.
	  * @param id ID of the client that wants to lock this node.
	  * @param receiveNotification Determines whether the client will be notifified later, in the case that the
	  *	lock cannot be granted immediately.
	  * @return True if the node has been locked, false otherwise.
	  * <b>NOTE: Not implemented, will currently always return true.</b>
	  */
	virtual bool Lock4Add(Dc1ClientID id, bool receiveNotification = true);

	/** Unlock this node.
	  * @param id ID of the client that wants to unlock.
	  */
	virtual void Unlock(Dc1ClientID id);

	//@}

	/** @name Notifications
	 */
	//@{

	/** Register to receive notifications from this node.
	  * @param id ID of the client that wants to receive notifications.
	  * @param action Type of action, for which notifications shall be sent.
	  * @param subtree If true, the client is also registered to receive notifications from all nodes
	  * in the subtree below the specified node. This may be slow, if many nodes are involved.
	  * <b>NOTE: Not implemented. No notifications will be sent</b>
	  */
	virtual void Register(Dc1ClientID id, Dc1NotificationAction action, bool subtree = false);

	/** Unregister from receiving notifications from this node.
	  * @param id ID of the client that wants to unregister.
	  * @param action Type of action, for which to unregister.
	  * @param subtree If true, the client is also unregistered from receving notifications from all nodes
	  * in the subtree below the specified node.
	  */
	virtual void Unregister(Dc1ClientID id, Dc1NotificationAction action);

	//@}

	virtual ~Dc1Node(void);

protected:
	Dc1Node(void);
	virtual void DeepCopy(const Dc1NodePtr &original) = 0;

	// checks if the specified client possesses a lock on this node
	virtual bool IsLocked(Dc1ClientID id);

	// FTT: deprecated In order to reduce API size (KISS), this method will be removed in the next version.
	// Returns all node element children (no attributes).  Only
	// applicable for nodes of type Class.
	virtual Dc1NodeEnum GetAllChildElements() const = 0;

	Dc1ClientID lockOwner;

	// Overwritten in nodes of type Class to return the base of the
	// "inheritance" hierarchy
	virtual Dc1NodePtr GetBaseRoot() { return m_myself.getPointer(); }

	virtual const Dc1NodePtr GetBaseRoot() const { return m_myself.getPointer(); }

public:
	// public because it's called from unrelated-by-inheritance nodes
	// that happen to be the new parent of the node.
	void SetParent(const Dc1NodePtr &new_parent);

private:
	// Unimplemented
	Dc1Node(const Dc1Node &);

	// Implemented in ClassTypes
	virtual void Init() {}

public:
	/** Set if xsi:type attribute shall be written when serialising this node (useful for elements having abstract types). */
	bool UseTypeAttribute;

protected:
	unsigned int m_ClassId;
	XMLCh * m_ContentName;

	Dc1WeakPtr< Dc1Node > m_Parent;

public:
	// What's this?  When storing any element, we need to do
	// m_element.setParent(this), only we can't use the `this' pointer
	// directly because we also need the reference count associated
	// with ourselves.  This is set by the factory during creation; if
	// the node is the base of another node, it's set to point to the
	// inheriter by inheriter's Init() method (can't be done by the
	// Factory, since m_Base isn't provided by the generic Node
	// class), so that all elements and attributes always point to the
	// "most specific parent".
	Dc1WeakPtr< Dc1Node > m_myself;

#if 0 // Not in use yet - there is an issue with NodePtr
	// For attributes retrieved from XPATH we use a union
	union Attribute {
		int IntValue;
		unsigned UnsignedValue;
		float FloatValue;
		double DoubleValue;
		Dc1Ptr< Dc1Node > Dc1NodePtr;
	};
#endif
};

#endif // DC1NODE_H
