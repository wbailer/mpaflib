/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/



#ifndef DC1_NODECOLLECTION_H
#define DC1_NODECOLLECTION_H


#include "Dc1Defines.h"
#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"
#include "Dc1Enumerator.h"

/** Supertype of all Dc1 collections that hold nodes (as opposed to
 * strings and immediate values). */
class DC1_EXPORT Dc1NodeCollection :
	public Dc1Node
{
public:

	/** Initializes the collection.
	  * @param maxElems Number of elements for which memory is initially allocated.
	  */
	virtual void Initialize(unsigned int maxElems) = 0;

	/** Adds a new element to the collection.
	  * @param toAdd Element to be added.
	  */
	virtual void addElement(const Dc1NodePtr &toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Sets the element at the given position in the collection.
	  * @param toSet New value of the element.
	  * @param setAt Position of the element which shall be set.
	  */
	virtual void setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Inserts the element at the given position in the collection.
	  * @param toInsert Element to be inserted.
	  * @param insertAt Position where the element will be inserted.
	  */
	virtual void insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes all elements from the collection.
	  */
	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes the specified element from the collection.
	  * @param removeAt Position of the element to be removed. 
	  */
	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Check whether the collection contains the given element.
	  * @param toCheck Element to be checked for.
	  * @return True if the element is in the collection, false otherwise.
	  */
	virtual bool containsElement(const Dc1NodePtr &toCheck) = 0;

	/** Get the current capacity of the collection.
	  * @return The number of elements for which memory has been allocated.
	  */
	virtual unsigned int curCapacity() const = 0;

	/** Gets a constant pointer to the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return A pointer to the given element or a null pointer if there is no element at the specified position.
	  */
	virtual const Dc1NodePtr elementAt(unsigned int getAt) const = 0;

	/** Gets a pointer to the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return A pointer to the given element or a null pointer if there is no element at the specified position.
	  */
	virtual Dc1NodePtr elementAt(unsigned int getAt) = 0;

	/** Gets an enumeration of all contained elements of the specified type.
	  * @param The class id of the wanted elements.
	  * @return A (possibly empty) enumeration of all contained elements of type id.
	  */
	virtual Dc1NodeEnum GetElementsOfType(unsigned int id) const = 0;

	/** Gets an enumeration of all contained elements.
	  * @return A (possibly empty) enumeration of all contained elements.
	  */
	virtual Dc1NodeEnum GetElements() const = 0;

	/** Get the size of the collection.
	  * @return The current number of elements in the collection.
	  */
	virtual unsigned int size() const = 0;

	/** Allocate additional memory for the speciifed number of elements.
	  * @param length Number of elements to allocate memory for.
	  */
	virtual void ensureExtraCapacity(unsigned int length) = 0;

	// JRS: addition to Xerces collection
	/** Get the position of the specified element. 
	  * @param toCheck Element to be found in the collection.
	  * @return The position of the specified element or -1 if the element has not been found.
	  */
	virtual int elementIndexOf(const Dc1NodePtr &toCheck) const = 0;

};

typedef Dc1Ptr< Dc1NodeCollection> Dc1NodeCollectionPtr;

#endif // DC1_NODECOLLECTION_H
