/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1751MP7JRSDESCRIPTIONMETADATATYPE_H
#define _1751MP7JRSDESCRIPTIONMETADATATYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsDescriptionMetadataTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtInclude.h


class IMp7JrsHeaderType;
typedef Dc1Ptr< IMp7JrsHeaderType > Mp7JrsHeaderPtr;
class IMp7JrsDescriptionMetadataType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType> Mp7JrsDescriptionMetadataPtr;
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
class IMp7JrstimePointType;
typedef Dc1Ptr< IMp7JrstimePointType > Mp7JrstimePointPtr;
class IMp7JrsTextAnnotationType;
typedef Dc1Ptr< IMp7JrsTextAnnotationType > Mp7JrsTextAnnotationPtr;
class IMp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType > Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr;
class IMp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType > Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr;
class IMp7JrsDescriptionMetadataType_Creator_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_Creator_CollectionType > Mp7JrsDescriptionMetadataType_Creator_CollectionPtr;
class IMp7JrsPlaceType;
typedef Dc1Ptr< IMp7JrsPlaceType > Mp7JrsPlacePtr;
class IMp7JrsDescriptionMetadataType_Instrument_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_Instrument_CollectionType > Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr;
class IMp7JrsRightsType;
typedef Dc1Ptr< IMp7JrsRightsType > Mp7JrsRightsPtr;
class IMp7JrsPackageType;
typedef Dc1Ptr< IMp7JrsPackageType > Mp7JrsPackagePtr;
#include "Mp7JrsHeaderType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsDescriptionMetadataType for class Mp7JrsDescriptionMetadataType<br>
 * Located at: mpeg7-v4.xsd, line 3795<br>
 * Classified: Class<br>
 * Derived from: HeaderType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsDescriptionMetadataType 
 :
		public IMp7JrsHeaderType
{
public:
	// TODO: make these protected?
	IMp7JrsDescriptionMetadataType();
	virtual ~IMp7JrsDescriptionMetadataType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Confidence element.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GetConfidence() const = 0;

	virtual bool IsValidConfidence() const = 0;

	/**
	 * Set Confidence element.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SetConfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Confidence element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateConfidence() = 0;

	/**
	 * Get Version element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetVersion() const = 0;

	virtual bool IsValidVersion() const = 0;

	/**
	 * Set Version element.
	 * @param item XMLCh *
	 */
	virtual void SetVersion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Version element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVersion() = 0;

	/**
	 * Get LastUpdate element.
	 * @return Mp7JrstimePointPtr
	 */
	virtual Mp7JrstimePointPtr GetLastUpdate() const = 0;

	virtual bool IsValidLastUpdate() const = 0;

	/**
	 * Set LastUpdate element.
	 * @param item const Mp7JrstimePointPtr &
	 */
	virtual void SetLastUpdate(const Mp7JrstimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate LastUpdate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLastUpdate() = 0;

	/**
	 * Get Comment element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetComment() const = 0;

	virtual bool IsValidComment() const = 0;

	/**
	 * Set Comment element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetComment(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Comment element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateComment() = 0;

	/**
	 * Get PublicIdentifier element.
	 * @return Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr GetPublicIdentifier() const = 0;

	/**
	 * Set PublicIdentifier element.
	 * @param item const Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPublicIdentifier(const Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PrivateIdentifier element.
	 * @return Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr GetPrivateIdentifier() const = 0;

	/**
	 * Set PrivateIdentifier element.
	 * @param item const Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPrivateIdentifier(const Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Creator element.
	 * @return Mp7JrsDescriptionMetadataType_Creator_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_Creator_CollectionPtr GetCreator() const = 0;

	/**
	 * Set Creator element.
	 * @param item const Mp7JrsDescriptionMetadataType_Creator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCreator(const Mp7JrsDescriptionMetadataType_Creator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CreationLocation element.
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetCreationLocation() const = 0;

	virtual bool IsValidCreationLocation() const = 0;

	/**
	 * Set CreationLocation element.
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetCreationLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate CreationLocation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCreationLocation() = 0;

	/**
	 * Get CreationTime element.
	 * @return Mp7JrstimePointPtr
	 */
	virtual Mp7JrstimePointPtr GetCreationTime() const = 0;

	virtual bool IsValidCreationTime() const = 0;

	/**
	 * Set CreationTime element.
	 * @param item const Mp7JrstimePointPtr &
	 */
	virtual void SetCreationTime(const Mp7JrstimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate CreationTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCreationTime() = 0;

	/**
	 * Get Instrument element.
	 * @return Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr GetInstrument() const = 0;

	/**
	 * Set Instrument element.
	 * @param item const Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetInstrument(const Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Rights element.
	 * @return Mp7JrsRightsPtr
	 */
	virtual Mp7JrsRightsPtr GetRights() const = 0;

	virtual bool IsValidRights() const = 0;

	/**
	 * Set Rights element.
	 * @param item const Mp7JrsRightsPtr &
	 */
	virtual void SetRights(const Mp7JrsRightsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Rights element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRights() = 0;

	/**
	 * Get Package element.
	 * @return Mp7JrsPackagePtr
	 */
	virtual Mp7JrsPackagePtr GetPackage() const = 0;

	virtual bool IsValidPackage() const = 0;

	/**
	 * Set Package element.
	 * @param item const Mp7JrsPackagePtr &
	 */
	virtual void SetPackage(const Mp7JrsPackagePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Package element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePackage() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsHeaderType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsHeaderType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of DescriptionMetadataType.
	 * Currently this type contains 11 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Confidence</li>
	 * <li>LastUpdate</li>
	 * <li>Comment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PublicIdentifier</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PrivateIdentifier</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Creator</li>
	 * <li>CreationLocation</li>
	 * <li>CreationTime</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Instrument</li>
	 * <li>Rights</li>
	 * <li>Package</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsDescriptionMetadataType<br>
 * Located at: mpeg7-v4.xsd, line 3795<br>
 * Classified: Class<br>
 * Derived from: HeaderType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 3795.<br>
 */
class DC1_EXPORT Mp7JrsDescriptionMetadataType :
		public IMp7JrsDescriptionMetadataType
{
	friend class Mp7JrsDescriptionMetadataTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsDescriptionMetadataType
	 
	 * @return Dc1Ptr< Mp7JrsHeaderType >
	 */
	virtual Dc1Ptr< Mp7JrsHeaderType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Confidence element.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GetConfidence() const;

	virtual bool IsValidConfidence() const;

	/*
	 * Set Confidence element.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SetConfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Confidence element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateConfidence();

	/*
	 * Get Version element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetVersion() const;

	virtual bool IsValidVersion() const;

	/*
	 * Set Version element.
	 * @param item XMLCh *
	 */
	virtual void SetVersion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Version element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVersion();

	/*
	 * Get LastUpdate element.
	 * @return Mp7JrstimePointPtr
	 */
	virtual Mp7JrstimePointPtr GetLastUpdate() const;

	virtual bool IsValidLastUpdate() const;

	/*
	 * Set LastUpdate element.
	 * @param item const Mp7JrstimePointPtr &
	 */
	virtual void SetLastUpdate(const Mp7JrstimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate LastUpdate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLastUpdate();

	/*
	 * Get Comment element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetComment() const;

	virtual bool IsValidComment() const;

	/*
	 * Set Comment element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetComment(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Comment element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateComment();

	/*
	 * Get PublicIdentifier element.
	 * @return Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr GetPublicIdentifier() const;

	/*
	 * Set PublicIdentifier element.
	 * @param item const Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPublicIdentifier(const Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PrivateIdentifier element.
	 * @return Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr GetPrivateIdentifier() const;

	/*
	 * Set PrivateIdentifier element.
	 * @param item const Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPrivateIdentifier(const Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Creator element.
	 * @return Mp7JrsDescriptionMetadataType_Creator_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_Creator_CollectionPtr GetCreator() const;

	/*
	 * Set Creator element.
	 * @param item const Mp7JrsDescriptionMetadataType_Creator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCreator(const Mp7JrsDescriptionMetadataType_Creator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CreationLocation element.
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetCreationLocation() const;

	virtual bool IsValidCreationLocation() const;

	/*
	 * Set CreationLocation element.
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetCreationLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate CreationLocation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCreationLocation();

	/*
	 * Get CreationTime element.
	 * @return Mp7JrstimePointPtr
	 */
	virtual Mp7JrstimePointPtr GetCreationTime() const;

	virtual bool IsValidCreationTime() const;

	/*
	 * Set CreationTime element.
	 * @param item const Mp7JrstimePointPtr &
	 */
	virtual void SetCreationTime(const Mp7JrstimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate CreationTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCreationTime();

	/*
	 * Get Instrument element.
	 * @return Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr
	 */
	virtual Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr GetInstrument() const;

	/*
	 * Set Instrument element.
	 * @param item const Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetInstrument(const Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Rights element.
	 * @return Mp7JrsRightsPtr
	 */
	virtual Mp7JrsRightsPtr GetRights() const;

	virtual bool IsValidRights() const;

	/*
	 * Set Rights element.
	 * @param item const Mp7JrsRightsPtr &
	 */
	virtual void SetRights(const Mp7JrsRightsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Rights element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRights();

	/*
	 * Get Package element.
	 * @return Mp7JrsPackagePtr
	 */
	virtual Mp7JrsPackagePtr GetPackage() const;

	virtual bool IsValidPackage() const;

	/*
	 * Set Package element.
	 * @param item const Mp7JrsPackagePtr &
	 */
	virtual void SetPackage(const Mp7JrsPackagePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Package element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePackage();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsHeaderType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsHeaderType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Gets or creates Dc1NodePtr child elements of DescriptionMetadataType.
	 * Currently this type contains 11 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Confidence</li>
	 * <li>LastUpdate</li>
	 * <li>Comment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PublicIdentifier</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PrivateIdentifier</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Creator</li>
	 * <li>CreationLocation</li>
	 * <li>CreationTime</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Instrument</li>
	 * <li>Rights</li>
	 * <li>Package</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsDescriptionMetadataType();

protected:
	Mp7JrsDescriptionMetadataType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsHeaderType > m_Base;

	Mp7JrszeroToOnePtr m_Confidence;
	bool m_Confidence_Exist; // For optional elements 
	XMLCh * m_Version;
	bool m_Version_Exist; // For optional elements 
	Mp7JrstimePointPtr m_LastUpdate;
	bool m_LastUpdate_Exist; // For optional elements 
	Mp7JrsTextAnnotationPtr m_Comment;
	bool m_Comment_Exist; // For optional elements 
	Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr m_PublicIdentifier;
	Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr m_PrivateIdentifier;
	Mp7JrsDescriptionMetadataType_Creator_CollectionPtr m_Creator;
	Mp7JrsPlacePtr m_CreationLocation;
	bool m_CreationLocation_Exist; // For optional elements 
	Mp7JrstimePointPtr m_CreationTime;
	bool m_CreationTime_Exist; // For optional elements 
	Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr m_Instrument;
	Mp7JrsRightsPtr m_Rights;
	bool m_Rights_Exist; // For optional elements 
	Mp7JrsPackagePtr m_Package;
	bool m_Package_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1751MP7JRSDESCRIPTIONMETADATATYPE_H

