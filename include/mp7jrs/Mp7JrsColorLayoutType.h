/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1427MP7JRSCOLORLAYOUTTYPE_H
#define _1427MP7JRSCOLORLAYOUTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsColorLayoutTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtInclude.h


class IMp7JrsVisualDType;
typedef Dc1Ptr< IMp7JrsVisualDType > Mp7JrsVisualDPtr;
class IMp7JrsColorLayoutType;
typedef Dc1Ptr< IMp7JrsColorLayoutType> Mp7JrsColorLayoutPtr;
class IMp7Jrsunsigned6;
typedef Dc1Ptr< IMp7Jrsunsigned6 > Mp7Jrsunsigned6Ptr;
class IMp7JrsColorLayoutType_YACCoeff2_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff2_LocalType > Mp7JrsColorLayoutType_YACCoeff2_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff5_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff5_LocalType > Mp7JrsColorLayoutType_YACCoeff5_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff9_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff9_LocalType > Mp7JrsColorLayoutType_YACCoeff9_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff14_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff14_LocalType > Mp7JrsColorLayoutType_YACCoeff14_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff20_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff20_LocalType > Mp7JrsColorLayoutType_YACCoeff20_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff27_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff27_LocalType > Mp7JrsColorLayoutType_YACCoeff27_LocalPtr;
class IMp7JrsColorLayoutType_YACCoeff63_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff63_LocalType > Mp7JrsColorLayoutType_YACCoeff63_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff2_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff2_LocalType > Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff2_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff2_LocalType > Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff5_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff5_LocalType > Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff5_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff5_LocalType > Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff9_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff9_LocalType > Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff9_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff9_LocalType > Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff14_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff14_LocalType > Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff14_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff14_LocalType > Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff20_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff20_LocalType > Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff20_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff20_LocalType > Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff27_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff27_LocalType > Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff27_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff27_LocalType > Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr;
class IMp7JrsColorLayoutType_CbACCoeff63_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff63_LocalType > Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr;
class IMp7JrsColorLayoutType_CrACCoeff63_LocalType;
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff63_LocalType > Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr;
#include "Mp7JrsVisualDType.h"
#include "Mp7JrsDType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsColorLayoutType for class Mp7JrsColorLayoutType<br>
 * Located at: mpeg7-v4.xsd, line 524<br>
 * Classified: Class<br>
 * Derived from: VisualDType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsColorLayoutType 
 :
		public IMp7JrsVisualDType
{
public:
	// TODO: make these protected?
	IMp7JrsColorLayoutType();
	virtual ~IMp7JrsColorLayoutType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get YDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetYDCCoeff() const = 0;

	/**
	 * Set YDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetYDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CbDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetCbDCCoeff() const = 0;

	/**
	 * Set CbDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetCbDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetCrDCCoeff() const = 0;

	/**
	 * Set CrDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetCrDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get YACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff2_LocalPtr GetYACCoeff2() const = 0;

	/**
	 * Set YACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff2(const Mp7JrsColorLayoutType_YACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff5_LocalPtr GetYACCoeff5() const = 0;

	/**
	 * Set YACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff5(const Mp7JrsColorLayoutType_YACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff9_LocalPtr GetYACCoeff9() const = 0;

	/**
	 * Set YACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff9(const Mp7JrsColorLayoutType_YACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff14_LocalPtr GetYACCoeff14() const = 0;

	/**
	 * Set YACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff14(const Mp7JrsColorLayoutType_YACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff20_LocalPtr GetYACCoeff20() const = 0;

	/**
	 * Set YACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff20(const Mp7JrsColorLayoutType_YACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff27_LocalPtr GetYACCoeff27() const = 0;

	/**
	 * Set YACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff27(const Mp7JrsColorLayoutType_YACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get YACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff63_LocalPtr GetYACCoeff63() const = 0;

	/**
	 * Set YACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff63(const Mp7JrsColorLayoutType_YACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr GetCbACCoeff2() const = 0;

	/**
	 * Set CbACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff2(const Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr GetCrACCoeff2() const = 0;

	/**
	 * Set CrACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff2(const Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr GetCbACCoeff5() const = 0;

	/**
	 * Set CbACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff5(const Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr GetCrACCoeff5() const = 0;

	/**
	 * Set CrACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff5(const Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr GetCbACCoeff9() const = 0;

	/**
	 * Set CbACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff9(const Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr GetCrACCoeff9() const = 0;

	/**
	 * Set CrACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff9(const Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr GetCbACCoeff14() const = 0;

	/**
	 * Set CbACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff14(const Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr GetCrACCoeff14() const = 0;

	/**
	 * Set CrACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff14(const Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr GetCbACCoeff20() const = 0;

	/**
	 * Set CbACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff20(const Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr GetCrACCoeff20() const = 0;

	/**
	 * Set CrACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff20(const Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr GetCbACCoeff27() const = 0;

	/**
	 * Set CbACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff27(const Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr GetCrACCoeff27() const = 0;

	/**
	 * Set CrACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff27(const Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get CbACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr GetCbACCoeff63() const = 0;

	/**
	 * Set CbACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff63(const Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CrACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr GetCrACCoeff63() const = 0;

	/**
	 * Set CrACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff63(const Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of ColorLayoutType.
	 * Currently this type contains 24 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>YDCCoeff</li>
	 * <li>CbDCCoeff</li>
	 * <li>CrDCCoeff</li>
	 * <li>YACCoeff2</li>
	 * <li>YACCoeff5</li>
	 * <li>YACCoeff9</li>
	 * <li>YACCoeff14</li>
	 * <li>YACCoeff20</li>
	 * <li>YACCoeff27</li>
	 * <li>YACCoeff63</li>
	 * <li>CbACCoeff2</li>
	 * <li>CrACCoeff2</li>
	 * <li>CbACCoeff5</li>
	 * <li>CrACCoeff5</li>
	 * <li>CbACCoeff9</li>
	 * <li>CrACCoeff9</li>
	 * <li>CbACCoeff14</li>
	 * <li>CrACCoeff14</li>
	 * <li>CbACCoeff20</li>
	 * <li>CrACCoeff20</li>
	 * <li>CbACCoeff27</li>
	 * <li>CrACCoeff27</li>
	 * <li>CbACCoeff63</li>
	 * <li>CrACCoeff63</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// begin extension included
// file Mp7JrsColorLayoutType_ExtMethodDef.h

public:
	
	/** Set the number of AC coefficients.
	  * @param y Number of Y AC coefficients.
	  * @param c Number of Cb/Cr AC coefficients.
	  */
	virtual void SetNrCoefficients(unsigned* y, unsigned* c, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get number of Y coefficents (DC+AC). */
	virtual unsigned GetNrYCoeffs();

	/** Get number of Cb/Cr AC coefficents (DC+AC). */
	virtual unsigned GetNrCCoeffs();

	/** Set Y coefficents.
	  * @param coeffs The number of coefficients stored in this array must equal the number of DC
	  *    + AC coefficients. The coefficients must be ordered by ascending frequency. */
	virtual void SetY(unsigned* coeffs, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get Y coefficents.
	  * param The size of this array must equal the number of DC
	  *    + AC coefficients. The coefficients are ordered by ascending frequency. */
	virtual void GetY(unsigned* coeffs);

	/** Set Cb coefficents.
	  * @param coeffs The number of coefficients stored in this array must equal the number of DC
	  *    + AC coefficients. The coefficients must be ordered by ascending frequency. */
	virtual void SetCb(unsigned* coeffs, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get Cb coefficents.
	  * param The size of this array must equal the number of DC
	  *    + AC coefficients. The coefficients are ordered by ascending frequency. */
	virtual void GetCb(unsigned* coeffs);

	/** Set Cr coefficents.
	  * @param coeffs The number of coefficients stored in this array must equal the number of DC
	  *    + AC coefficients. The coefficients must be ordered by ascending frequency. */
	virtual void SetCr(unsigned* coeffs, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get Cr coefficents.
	  * param The size of this array must equal the number of DC
	  *    + AC coefficients. The coefficients are ordered by ascending frequency. */
	virtual void GetCr(unsigned* coeffs);


private:
	/** number of coefficients to be used for MPEG-7 representation
	  * @param nrCoeffs nr of defined coefficients
	  * @return one of the predefined numbers of coefficients
	  */
	unsigned _NrCoeffsToUse(unsigned nrCoeffs);

	/** init the y coefficents list */
	void _CreateCoeffListsY(unsigned y, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** init the Cb/Cr coefficients lists */
	void _CreateCoeffListsC(unsigned c, Dc1ClientID client = DC1_UNDEFINED_CLIENT);






// end extension included


// begin extension included
// file Mp7JrsColorLayoutType_ExtPropDef.h



	// nr coefficients
	int _nrYCoeff;
	int _nrCCoeff;
// end extension included

};




/*
 * Generated class Mp7JrsColorLayoutType<br>
 * Located at: mpeg7-v4.xsd, line 524<br>
 * Classified: Class<br>
 * Derived from: VisualDType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 524.<br>
 */
class DC1_EXPORT Mp7JrsColorLayoutType :
		public IMp7JrsColorLayoutType
{
	friend class Mp7JrsColorLayoutTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsColorLayoutType
	 
	 * @return Dc1Ptr< Mp7JrsVisualDType >
	 */
	virtual Dc1Ptr< Mp7JrsVisualDType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get YDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetYDCCoeff() const;

	/*
	 * Set YDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetYDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CbDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetCbDCCoeff() const;

	/*
	 * Set CbDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetCbDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrDCCoeff element.
	 * @return Mp7Jrsunsigned6Ptr
	 */
	virtual Mp7Jrsunsigned6Ptr GetCrDCCoeff() const;

	/*
	 * Set CrDCCoeff element.
	 * @param item const Mp7Jrsunsigned6Ptr &
	 */
	// Mandatory			
	virtual void SetCrDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Choice

		 */
		//@{
	/*
	 * Get YACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff2_LocalPtr GetYACCoeff2() const;

	/*
	 * Set YACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff2(const Mp7JrsColorLayoutType_YACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff5_LocalPtr GetYACCoeff5() const;

	/*
	 * Set YACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff5(const Mp7JrsColorLayoutType_YACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff9_LocalPtr GetYACCoeff9() const;

	/*
	 * Set YACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff9(const Mp7JrsColorLayoutType_YACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff14_LocalPtr GetYACCoeff14() const;

	/*
	 * Set YACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff14(const Mp7JrsColorLayoutType_YACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff20_LocalPtr GetYACCoeff20() const;

	/*
	 * Set YACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff20(const Mp7JrsColorLayoutType_YACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff27_LocalPtr GetYACCoeff27() const;

	/*
	 * Set YACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff27(const Mp7JrsColorLayoutType_YACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get YACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_YACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_YACCoeff63_LocalPtr GetYACCoeff63() const;

	/*
	 * Set YACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_YACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetYACCoeff63(const Mp7JrsColorLayoutType_YACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr GetCbACCoeff2() const;

	/*
	 * Set CbACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff2(const Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff2 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr GetCrACCoeff2() const;

	/*
	 * Set CrACCoeff2 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff2(const Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr GetCbACCoeff5() const;

	/*
	 * Set CbACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff5(const Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff5 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr GetCrACCoeff5() const;

	/*
	 * Set CrACCoeff5 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff5(const Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr GetCbACCoeff9() const;

	/*
	 * Set CbACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff9(const Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff9 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr GetCrACCoeff9() const;

	/*
	 * Set CrACCoeff9 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff9(const Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr GetCbACCoeff14() const;

	/*
	 * Set CbACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff14(const Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff14 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr GetCrACCoeff14() const;

	/*
	 * Set CrACCoeff14 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff14(const Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr GetCbACCoeff20() const;

	/*
	 * Set CbACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff20(const Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff20 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr GetCrACCoeff20() const;

	/*
	 * Set CrACCoeff20 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff20(const Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr GetCbACCoeff27() const;

	/*
	 * Set CbACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff27(const Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff27 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr GetCrACCoeff27() const;

	/*
	 * Set CrACCoeff27 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff27(const Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get CbACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr GetCbACCoeff63() const;

	/*
	 * Set CbACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCbACCoeff63(const Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CrACCoeff63 element.
	 * @return Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr
	 */
	virtual Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr GetCrACCoeff63() const;

	/*
	 * Set CrACCoeff63 element.
	 * @param item const Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr &
	 */
	// Mandatory			
	virtual void SetCrACCoeff63(const Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of ColorLayoutType.
	 * Currently this type contains 24 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>YDCCoeff</li>
	 * <li>CbDCCoeff</li>
	 * <li>CrDCCoeff</li>
	 * <li>YACCoeff2</li>
	 * <li>YACCoeff5</li>
	 * <li>YACCoeff9</li>
	 * <li>YACCoeff14</li>
	 * <li>YACCoeff20</li>
	 * <li>YACCoeff27</li>
	 * <li>YACCoeff63</li>
	 * <li>CbACCoeff2</li>
	 * <li>CrACCoeff2</li>
	 * <li>CbACCoeff5</li>
	 * <li>CrACCoeff5</li>
	 * <li>CbACCoeff9</li>
	 * <li>CrACCoeff9</li>
	 * <li>CbACCoeff14</li>
	 * <li>CrACCoeff14</li>
	 * <li>CbACCoeff20</li>
	 * <li>CrACCoeff20</li>
	 * <li>CbACCoeff27</li>
	 * <li>CrACCoeff27</li>
	 * <li>CbACCoeff63</li>
	 * <li>CrACCoeff63</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsColorLayoutType();

protected:
	Mp7JrsColorLayoutType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsVisualDType > m_Base;

	Mp7Jrsunsigned6Ptr m_YDCCoeff;
	Mp7Jrsunsigned6Ptr m_CbDCCoeff;
	Mp7Jrsunsigned6Ptr m_CrDCCoeff;
// DefinionChoice
	Mp7JrsColorLayoutType_YACCoeff2_LocalPtr m_YACCoeff2;
	Mp7JrsColorLayoutType_YACCoeff5_LocalPtr m_YACCoeff5;
	Mp7JrsColorLayoutType_YACCoeff9_LocalPtr m_YACCoeff9;
	Mp7JrsColorLayoutType_YACCoeff14_LocalPtr m_YACCoeff14;
	Mp7JrsColorLayoutType_YACCoeff20_LocalPtr m_YACCoeff20;
	Mp7JrsColorLayoutType_YACCoeff27_LocalPtr m_YACCoeff27;
	Mp7JrsColorLayoutType_YACCoeff63_LocalPtr m_YACCoeff63;
// End DefinionChoice
// DefinionChoice
	Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr m_CbACCoeff2;
	Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr m_CrACCoeff2;
	Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr m_CbACCoeff5;
	Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr m_CrACCoeff5;
	Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr m_CbACCoeff9;
	Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr m_CrACCoeff9;
	Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr m_CbACCoeff14;
	Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr m_CrACCoeff14;
	Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr m_CbACCoeff20;
	Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr m_CrACCoeff20;
	Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr m_CbACCoeff27;
	Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr m_CrACCoeff27;
	Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr m_CbACCoeff63;
	Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr m_CrACCoeff63;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1427MP7JRSCOLORLAYOUTTYPE_H

