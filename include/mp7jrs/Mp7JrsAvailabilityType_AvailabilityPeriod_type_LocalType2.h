/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1245MP7JRSAVAILABILITYTYPE_AVAILABILITYPERIOD_TYPE_LOCALTYPE2_H
#define _1245MP7JRSAVAILABILITYTYPE_AVAILABILITYPERIOD_TYPE_LOCALTYPE2_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2Factory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2_ExtInclude.h


class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2;
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2> Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local2Ptr;
#include "Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType.h"
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0;
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 > Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1;
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 > Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr;

/** 
 * Generated interface IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2 for class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2<br>
 * Located at: mpeg7-v4.xsd, line 6699<br>
 */
class MP7JRS_EXPORT IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2();
	virtual ~IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2();
	/** @name Union

	 */
	//@{
	/**
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration GetAvailabilityType_AvailabilityPeriod_type_LocalType() const = 0;

	/**
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType element.
	 * @param item Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType(Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr GetAvailabilityType_AvailabilityPeriod_type_LocalType0() const = 0;

	/**
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 element.
	 * @param item const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr &
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType0(const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr GetAvailabilityType_AvailabilityPeriod_type_LocalType1() const = 0;

	/**
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 element.
	 * @param item const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr &
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType1(const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	//@}
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2_ExtPropDef.h

};




/*
 * Generated class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2<br>
 * Located at: mpeg7-v4.xsd, line 6699<br>
 * Defined in mpeg7-v4.xsd, line 6699.<br>
 */
class DC1_EXPORT Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2 :
		public IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2
{
	friend class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2Factory; // constructs objects

public:
	/* @name Union

	 */
	//@{
	/*
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration GetAvailabilityType_AvailabilityPeriod_type_LocalType() const;

	/*
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType element.
	 * @param item Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType(Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr GetAvailabilityType_AvailabilityPeriod_type_LocalType0() const;

	/*
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 element.
	 * @param item const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr &
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType0(const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 element.
	 * @return Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr
	 */
	virtual Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr GetAvailabilityType_AvailabilityPeriod_type_LocalType1() const;

	/*
	 * Set Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 element.
	 * @param item const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr &
	 */
	// Mandatory			
	virtual void SetAvailabilityType_AvailabilityPeriod_type_LocalType1(const Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2();

protected:
	Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionUnion
	Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration m_AvailabilityType_AvailabilityPeriod_type_LocalType;
	Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr m_AvailabilityType_AvailabilityPeriod_type_LocalType0;
	Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr m_AvailabilityType_AvailabilityPeriod_type_LocalType1;
// End DefinionUnion

// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1245MP7JRSAVAILABILITYTYPE_AVAILABILITYPERIOD_TYPE_LOCALTYPE2_H

