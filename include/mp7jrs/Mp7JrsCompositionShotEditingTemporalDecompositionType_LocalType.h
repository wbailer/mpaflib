/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1565MP7JRSCOMPOSITIONSHOTEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H
#define _1565MP7JRSCOMPOSITIONSHOTEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType_ExtInclude.h


class IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType;
typedef Dc1Ptr< IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType> Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr;
class IMp7JrsIntraCompositionShotType;
typedef Dc1Ptr< IMp7JrsIntraCompositionShotType > Mp7JrsIntraCompositionShotPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsInternalTransitionType;
typedef Dc1Ptr< IMp7JrsInternalTransitionType > Mp7JrsInternalTransitionPtr;

/** 
 * Generated interface IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType for class Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType();
	virtual ~IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType();
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get IntraCompositionShot element.
	 * @return Mp7JrsIntraCompositionShotPtr
	 */
	virtual Mp7JrsIntraCompositionShotPtr GetIntraCompositionShot() const = 0;

	/**
	 * Set IntraCompositionShot element.
	 * @param item const Mp7JrsIntraCompositionShotPtr &
	 */
	// Mandatory			
	virtual void SetIntraCompositionShot(const Mp7JrsIntraCompositionShotPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get IntraCompositionShotRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetIntraCompositionShotRef() const = 0;

	/**
	 * Set IntraCompositionShotRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetIntraCompositionShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get InternalTransition element.
	 * @return Mp7JrsInternalTransitionPtr
	 */
	virtual Mp7JrsInternalTransitionPtr GetInternalTransition() const = 0;

	/**
	 * Set InternalTransition element.
	 * @param item const Mp7JrsInternalTransitionPtr &
	 */
	// Mandatory			
	virtual void SetInternalTransition(const Mp7JrsInternalTransitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get InternalTransitionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetInternalTransitionRef() const = 0;

	/**
	 * Set InternalTransitionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetInternalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
		//@}
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 7971.<br>
 */
class DC1_EXPORT Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType :
		public IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType
{
	friend class Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get IntraCompositionShot element.
	 * @return Mp7JrsIntraCompositionShotPtr
	 */
	virtual Mp7JrsIntraCompositionShotPtr GetIntraCompositionShot() const;

	/*
	 * Set IntraCompositionShot element.
	 * @param item const Mp7JrsIntraCompositionShotPtr &
	 */
	// Mandatory			
	virtual void SetIntraCompositionShot(const Mp7JrsIntraCompositionShotPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get IntraCompositionShotRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetIntraCompositionShotRef() const;

	/*
	 * Set IntraCompositionShotRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetIntraCompositionShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get InternalTransition element.
	 * @return Mp7JrsInternalTransitionPtr
	 */
	virtual Mp7JrsInternalTransitionPtr GetInternalTransition() const;

	/*
	 * Set InternalTransition element.
	 * @param item const Mp7JrsInternalTransitionPtr &
	 */
	// Mandatory			
	virtual void SetInternalTransition(const Mp7JrsInternalTransitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get InternalTransitionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetInternalTransitionRef() const;

	/*
	 * Set InternalTransitionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetInternalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType();

protected:
	Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
// DefinionChoice
	Mp7JrsIntraCompositionShotPtr m_IntraCompositionShot;
	Mp7JrsReferencePtr m_IntraCompositionShotRef;
// End DefinionChoice
// DefinionChoice
	Mp7JrsInternalTransitionPtr m_InternalTransition;
	Mp7JrsReferencePtr m_InternalTransitionRef;
// End DefinionChoice
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1565MP7JRSCOMPOSITIONSHOTEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H

