/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3207MP7JRSSENTENCESTYPE_LOCALTYPE_H
#define _3207MP7JRSSENTENCESTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSentencesType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtInclude.h


class IMp7JrsSentencesType_LocalType;
typedef Dc1Ptr< IMp7JrsSentencesType_LocalType> Mp7JrsSentencesType_LocalPtr;
class IMp7JrsSentencesType;
typedef Dc1Ptr< IMp7JrsSentencesType > Mp7JrsSentencesPtr;
class IMp7JrsSyntacticConstituentType;
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType > Mp7JrsSyntacticConstituentPtr;
class IMp7JrsLinguisticDocumentType;
typedef Dc1Ptr< IMp7JrsLinguisticDocumentType > Mp7JrsLinguisticDocumentPtr;

/** 
 * Generated interface IMp7JrsSentencesType_LocalType for class Mp7JrsSentencesType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsSentencesType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsSentencesType_LocalType();
	virtual ~IMp7JrsSentencesType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get Sentences element.
	 * @return Mp7JrsSentencesPtr
	 */
	virtual Mp7JrsSentencesPtr GetSentences() const = 0;

	/**
	 * Set Sentences element.
	 * @param item const Mp7JrsSentencesPtr &
	 */
	virtual void SetSentences(const Mp7JrsSentencesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Sentence element.
	 * @return Mp7JrsSyntacticConstituentPtr
	 */
	virtual Mp7JrsSyntacticConstituentPtr GetSentence() const = 0;

	/**
	 * Set Sentence element.
	 * @param item const Mp7JrsSyntacticConstituentPtr &
	 */
	virtual void SetSentence(const Mp7JrsSyntacticConstituentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Quotation element.
	 * @return Mp7JrsLinguisticDocumentPtr
	 */
	virtual Mp7JrsLinguisticDocumentPtr GetQuotation() const = 0;

	/**
	 * Set Quotation element.
	 * @param item const Mp7JrsLinguisticDocumentPtr &
	 */
	virtual void SetQuotation(const Mp7JrsLinguisticDocumentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSentencesType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 5770.<br>
 */
class DC1_EXPORT Mp7JrsSentencesType_LocalType :
		public IMp7JrsSentencesType_LocalType
{
	friend class Mp7JrsSentencesType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get Sentences element.
	 * @return Mp7JrsSentencesPtr
	 */
	virtual Mp7JrsSentencesPtr GetSentences() const;

	/*
	 * Set Sentences element.
	 * @param item const Mp7JrsSentencesPtr &
	 */
	virtual void SetSentences(const Mp7JrsSentencesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Sentence element.
	 * @return Mp7JrsSyntacticConstituentPtr
	 */
	virtual Mp7JrsSyntacticConstituentPtr GetSentence() const;

	/*
	 * Set Sentence element.
	 * @param item const Mp7JrsSyntacticConstituentPtr &
	 */
	virtual void SetSentence(const Mp7JrsSyntacticConstituentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Quotation element.
	 * @return Mp7JrsLinguisticDocumentPtr
	 */
	virtual Mp7JrsLinguisticDocumentPtr GetQuotation() const;

	/*
	 * Set Quotation element.
	 * @param item const Mp7JrsLinguisticDocumentPtr &
	 */
	virtual void SetQuotation(const Mp7JrsLinguisticDocumentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSentencesType_LocalType();

protected:
	Mp7JrsSentencesType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsSentencesPtr m_Sentences;
	Mp7JrsSyntacticConstituentPtr m_Sentence;
	Mp7JrsLinguisticDocumentPtr m_Quotation;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3207MP7JRSSENTENCESTYPE_LOCALTYPE_H

