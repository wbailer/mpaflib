/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2421MP7JRSMEDIATRANSCODINGHINTSTYPE_SHAPEHINT_LOCALTYPE_H
#define _2421MP7JRSMEDIATRANSCODINGHINTSTYPE_SHAPEHINT_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType_ExtInclude.h


class IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType;
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType> Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr;
class IMp7JrsnonNegativeReal;
typedef Dc1Ptr< IMp7JrsnonNegativeReal > Mp7JrsnonNegativeRealPtr;

/** 
 * Generated interface IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType for class Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 6087<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType();
	virtual ~IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType();
	/**
	 * Get shapeChange attribute.
	 * @return float
	 */
	virtual float GetshapeChange() const = 0;

	/**
	 * Get shapeChange attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistshapeChange() const = 0;

	/**
	 * Set shapeChange attribute.
	 * @param item float
	 */
	virtual void SetshapeChange(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate shapeChange attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateshapeChange() = 0;

	/**
	 * Get numOfNonTranspBlocks attribute.
	 * @return Mp7JrsnonNegativeRealPtr
	 */
	virtual Mp7JrsnonNegativeRealPtr GetnumOfNonTranspBlocks() const = 0;

	/**
	 * Get numOfNonTranspBlocks attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnumOfNonTranspBlocks() const = 0;

	/**
	 * Set numOfNonTranspBlocks attribute.
	 * @param item const Mp7JrsnonNegativeRealPtr &
	 */
	virtual void SetnumOfNonTranspBlocks(const Mp7JrsnonNegativeRealPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate numOfNonTranspBlocks attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenumOfNonTranspBlocks() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of MediaTranscodingHintsType_ShapeHint_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaTranscodingHintsType_ShapeHint_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 6087<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 6087.<br>
 */
class DC1_EXPORT Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType :
		public IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType
{
	friend class Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get shapeChange attribute.
	 * @return float
	 */
	virtual float GetshapeChange() const;

	/*
	 * Get shapeChange attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistshapeChange() const;

	/*
	 * Set shapeChange attribute.
	 * @param item float
	 */
	virtual void SetshapeChange(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate shapeChange attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateshapeChange();

	/*
	 * Get numOfNonTranspBlocks attribute.
	 * @return Mp7JrsnonNegativeRealPtr
	 */
	virtual Mp7JrsnonNegativeRealPtr GetnumOfNonTranspBlocks() const;

	/*
	 * Get numOfNonTranspBlocks attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnumOfNonTranspBlocks() const;

	/*
	 * Set numOfNonTranspBlocks attribute.
	 * @param item const Mp7JrsnonNegativeRealPtr &
	 */
	virtual void SetnumOfNonTranspBlocks(const Mp7JrsnonNegativeRealPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate numOfNonTranspBlocks attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenumOfNonTranspBlocks();

	/*
	 * Gets or creates Dc1NodePtr child elements of MediaTranscodingHintsType_ShapeHint_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaTranscodingHintsType_ShapeHint_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType();

protected:
	Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	float m_shapeChange;
	bool m_shapeChange_Exist;
	Mp7JrsnonNegativeRealPtr m_numOfNonTranspBlocks;
	bool m_numOfNonTranspBlocks_Exist;



// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2421MP7JRSMEDIATRANSCODINGHINTSTYPE_SHAPEHINT_LOCALTYPE_H

