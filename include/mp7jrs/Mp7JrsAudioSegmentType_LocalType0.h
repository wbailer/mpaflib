/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1127MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE0_H
#define _1127MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE0_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsAudioSegmentType_LocalType0Factory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType0_ExtInclude.h


class IMp7JrsAudioSegmentType_LocalType0;
typedef Dc1Ptr< IMp7JrsAudioSegmentType_LocalType0> Mp7JrsAudioSegmentType_Local0Ptr;
class IMp7JrsAudioSegmentTemporalDecompositionType;
typedef Dc1Ptr< IMp7JrsAudioSegmentTemporalDecompositionType > Mp7JrsAudioSegmentTemporalDecompositionPtr;
class IMp7JrsAudioSegmentMediaSourceDecompositionType;
typedef Dc1Ptr< IMp7JrsAudioSegmentMediaSourceDecompositionType > Mp7JrsAudioSegmentMediaSourceDecompositionPtr;

/** 
 * Generated interface IMp7JrsAudioSegmentType_LocalType0 for class Mp7JrsAudioSegmentType_LocalType0<br>
 */
class MP7JRS_EXPORT IMp7JrsAudioSegmentType_LocalType0 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsAudioSegmentType_LocalType0();
	virtual ~IMp7JrsAudioSegmentType_LocalType0();
		/** @name Choice

		 */
		//@{
	/**
	 * Get TemporalDecomposition element.
	 * @return Mp7JrsAudioSegmentTemporalDecompositionPtr
	 */
	virtual Mp7JrsAudioSegmentTemporalDecompositionPtr GetTemporalDecomposition() const = 0;

	/**
	 * Set TemporalDecomposition element.
	 * @param item const Mp7JrsAudioSegmentTemporalDecompositionPtr &
	 */
	virtual void SetTemporalDecomposition(const Mp7JrsAudioSegmentTemporalDecompositionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaSourceDecomposition element.
	 * @return Mp7JrsAudioSegmentMediaSourceDecompositionPtr
	 */
	virtual Mp7JrsAudioSegmentMediaSourceDecompositionPtr GetMediaSourceDecomposition() const = 0;

	/**
	 * Set MediaSourceDecomposition element.
	 * @param item const Mp7JrsAudioSegmentMediaSourceDecompositionPtr &
	 */
	virtual void SetMediaSourceDecomposition(const Mp7JrsAudioSegmentMediaSourceDecompositionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType0_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType0_ExtPropDef.h

};




/*
 * Generated class Mp7JrsAudioSegmentType_LocalType0<br>
 * Defined in mpeg7-v4.xsd, line 7386.<br>
 */
class DC1_EXPORT Mp7JrsAudioSegmentType_LocalType0 :
		public IMp7JrsAudioSegmentType_LocalType0
{
	friend class Mp7JrsAudioSegmentType_LocalType0Factory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get TemporalDecomposition element.
	 * @return Mp7JrsAudioSegmentTemporalDecompositionPtr
	 */
	virtual Mp7JrsAudioSegmentTemporalDecompositionPtr GetTemporalDecomposition() const;

	/*
	 * Set TemporalDecomposition element.
	 * @param item const Mp7JrsAudioSegmentTemporalDecompositionPtr &
	 */
	virtual void SetTemporalDecomposition(const Mp7JrsAudioSegmentTemporalDecompositionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaSourceDecomposition element.
	 * @return Mp7JrsAudioSegmentMediaSourceDecompositionPtr
	 */
	virtual Mp7JrsAudioSegmentMediaSourceDecompositionPtr GetMediaSourceDecomposition() const;

	/*
	 * Set MediaSourceDecomposition element.
	 * @param item const Mp7JrsAudioSegmentMediaSourceDecompositionPtr &
	 */
	virtual void SetMediaSourceDecomposition(const Mp7JrsAudioSegmentMediaSourceDecompositionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType0_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsAudioSegmentType_LocalType0();

protected:
	Mp7JrsAudioSegmentType_LocalType0();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsAudioSegmentTemporalDecompositionPtr m_TemporalDecomposition;
	Mp7JrsAudioSegmentMediaSourceDecompositionPtr m_MediaSourceDecomposition;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType0_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1127MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE0_H

