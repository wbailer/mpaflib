/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3337MP7JRSSPACERESOLUTIONVIEWTYPE_H
#define _3337MP7JRSSPACERESOLUTIONVIEWTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSpaceResolutionViewTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtInclude.h


class IMp7JrsViewType;
typedef Dc1Ptr< IMp7JrsViewType > Mp7JrsViewPtr;
class IMp7JrsSpaceResolutionViewType;
typedef Dc1Ptr< IMp7JrsSpaceResolutionViewType> Mp7JrsSpaceResolutionViewPtr;
#include "Mp7JrsSpaceResolutionViewType_order_LocalType.h"
class IMp7JrsPartitionType;
typedef Dc1Ptr< IMp7JrsPartitionType > Mp7JrsPartitionPtr;
class IMp7JrsRegionLocatorType;
typedef Dc1Ptr< IMp7JrsRegionLocatorType > Mp7JrsRegionLocatorPtr;
class IMp7JrsFilteringType;
typedef Dc1Ptr< IMp7JrsFilteringType > Mp7JrsFilteringPtr;
class IMp7JrsSignalPlaneSampleType;
typedef Dc1Ptr< IMp7JrsSignalPlaneSampleType > Mp7JrsSignalPlaneSamplePtr;
#include "Mp7JrsViewType.h"
class IMp7JrsSignalType;
typedef Dc1Ptr< IMp7JrsSignalType > Mp7JrsSignalPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSpaceResolutionViewType for class Mp7JrsSpaceResolutionViewType<br>
 * Located at: mpeg7-v4.xsd, line 8854<br>
 * Classified: Class<br>
 * Derived from: ViewType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSpaceResolutionViewType 
 :
		public IMp7JrsViewType
{
public:
	// TODO: make these protected?
	IMp7JrsSpaceResolutionViewType();
	virtual ~IMp7JrsSpaceResolutionViewType();
	/**
	 * Get order attribute.
	 * @return Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration
	 */
	virtual Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration Getorder() const = 0;

	/**
	 * Get order attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existorder() const = 0;

	/**
	 * Set order attribute.
	 * @param item Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration
	 */
	virtual void Setorder(Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate order attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateorder() = 0;

		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get SpacePartition element.
	 * @return Mp7JrsPartitionPtr
	 */
	virtual Mp7JrsPartitionPtr GetSpacePartition() const = 0;

	/**
	 * Set SpacePartition element.
	 * @param item const Mp7JrsPartitionPtr &
	 */
	// Mandatory			
	virtual void SetSpacePartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SpaceRegion element.
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetSpaceRegion() const = 0;

	/**
	 * Set SpaceRegion element.
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	// Mandatory			
	virtual void SetSpaceRegion(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get FrequencyPartition element.
	 * @return Mp7JrsPartitionPtr
	 */
	virtual Mp7JrsPartitionPtr GetFrequencyPartition() const = 0;

	virtual bool IsValidFrequencyPartition() const = 0;

	/**
	 * Set FrequencyPartition element.
	 * @param item const Mp7JrsPartitionPtr &
	 */
	virtual void SetFrequencyPartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate FrequencyPartition element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFrequencyPartition() = 0;

	/**
	 * Get Filtering element.
	 * @return Mp7JrsFilteringPtr
	 */
	virtual Mp7JrsFilteringPtr GetFiltering() const = 0;

	virtual bool IsValidFiltering() const = 0;

	/**
	 * Set Filtering element.
	 * @param item const Mp7JrsFilteringPtr &
	 */
	virtual void SetFiltering(const Mp7JrsFilteringPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Filtering element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFiltering() = 0;

	/**
	 * Get DownSamplingFactor element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetDownSamplingFactor() const = 0;

	/**
	 * Set DownSamplingFactor element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	// Mandatory			
	virtual void SetDownSamplingFactor(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Target element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @return Mp7JrsSignalPtr
	 */
	virtual Mp7JrsSignalPtr GetTarget() const = 0;

	/**
	 * Set Target element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @param item const Mp7JrsSignalPtr &
	 */
	// Mandatory			
	virtual void SetTarget(const Mp7JrsSignalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Source element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @return Mp7JrsSignalPtr
	 */
	virtual Mp7JrsSignalPtr GetSource() const = 0;

	virtual bool IsValidSource() const = 0;

	/**
	 * Set Source element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @param item const Mp7JrsSignalPtr &
	 */
	virtual void SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Source element.
	 * (Inherited from Mp7JrsViewType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSource() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SpaceResolutionViewType.
	 * Currently this type contains 8 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Target</li>
	 * <li>Source</li>
	 * <li>SpacePartition</li>
	 * <li>SpaceRegion</li>
	 * <li>FrequencyPartition</li>
	 * <li>Filtering</li>
	 * <li>DownSamplingFactor</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSpaceResolutionViewType<br>
 * Located at: mpeg7-v4.xsd, line 8854<br>
 * Classified: Class<br>
 * Derived from: ViewType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 8854.<br>
 */
class DC1_EXPORT Mp7JrsSpaceResolutionViewType :
		public IMp7JrsSpaceResolutionViewType
{
	friend class Mp7JrsSpaceResolutionViewTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSpaceResolutionViewType
	 
	 * @return Dc1Ptr< Mp7JrsViewType >
	 */
	virtual Dc1Ptr< Mp7JrsViewType > GetBase() const;
	/*
	 * Get order attribute.
	 * @return Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration
	 */
	virtual Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration Getorder() const;

	/*
	 * Get order attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existorder() const;

	/*
	 * Set order attribute.
	 * @param item Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration
	 */
	virtual void Setorder(Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate order attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateorder();

		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get SpacePartition element.
	 * @return Mp7JrsPartitionPtr
	 */
	virtual Mp7JrsPartitionPtr GetSpacePartition() const;

	/*
	 * Set SpacePartition element.
	 * @param item const Mp7JrsPartitionPtr &
	 */
	// Mandatory			
	virtual void SetSpacePartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SpaceRegion element.
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetSpaceRegion() const;

	/*
	 * Set SpaceRegion element.
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	// Mandatory			
	virtual void SetSpaceRegion(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get FrequencyPartition element.
	 * @return Mp7JrsPartitionPtr
	 */
	virtual Mp7JrsPartitionPtr GetFrequencyPartition() const;

	virtual bool IsValidFrequencyPartition() const;

	/*
	 * Set FrequencyPartition element.
	 * @param item const Mp7JrsPartitionPtr &
	 */
	virtual void SetFrequencyPartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate FrequencyPartition element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFrequencyPartition();

	/*
	 * Get Filtering element.
	 * @return Mp7JrsFilteringPtr
	 */
	virtual Mp7JrsFilteringPtr GetFiltering() const;

	virtual bool IsValidFiltering() const;

	/*
	 * Set Filtering element.
	 * @param item const Mp7JrsFilteringPtr &
	 */
	virtual void SetFiltering(const Mp7JrsFilteringPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Filtering element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFiltering();

	/*
	 * Get DownSamplingFactor element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetDownSamplingFactor() const;

	/*
	 * Set DownSamplingFactor element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	// Mandatory			
	virtual void SetDownSamplingFactor(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Target element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @return Mp7JrsSignalPtr
	 */
	virtual Mp7JrsSignalPtr GetTarget() const;

	/*
	 * Set Target element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @param item const Mp7JrsSignalPtr &
	 */
	// Mandatory			
	virtual void SetTarget(const Mp7JrsSignalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Source element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @return Mp7JrsSignalPtr
	 */
	virtual Mp7JrsSignalPtr GetSource() const;

	virtual bool IsValidSource() const;

	/*
	 * Set Source element.
<br>
	 * (Inherited from Mp7JrsViewType)
	 * @param item const Mp7JrsSignalPtr &
	 */
	virtual void SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Source element.
	 * (Inherited from Mp7JrsViewType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSource();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SpaceResolutionViewType.
	 * Currently this type contains 8 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Target</li>
	 * <li>Source</li>
	 * <li>SpacePartition</li>
	 * <li>SpaceRegion</li>
	 * <li>FrequencyPartition</li>
	 * <li>Filtering</li>
	 * <li>DownSamplingFactor</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSpaceResolutionViewType();

protected:
	Mp7JrsSpaceResolutionViewType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration m_order;
	bool m_order_Exist;
	Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration m_order_Default;

	// Base Class
	Dc1Ptr< Mp7JrsViewType > m_Base;

// DefinionChoice
	Mp7JrsPartitionPtr m_SpacePartition;
	Mp7JrsRegionLocatorPtr m_SpaceRegion;
// End DefinionChoice
	Mp7JrsPartitionPtr m_FrequencyPartition;
	bool m_FrequencyPartition_Exist; // For optional elements 
	Mp7JrsFilteringPtr m_Filtering;
	bool m_Filtering_Exist; // For optional elements 
	Mp7JrsSignalPlaneSamplePtr m_DownSamplingFactor;

// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3337MP7JRSSPACERESOLUTIONVIEWTYPE_H

