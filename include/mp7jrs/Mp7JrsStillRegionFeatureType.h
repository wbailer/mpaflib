/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3481MP7JRSSTILLREGIONFEATURETYPE_H
#define _3481MP7JRSSTILLREGIONFEATURETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsStillRegionFeatureTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtInclude.h


class IMp7JrsVisualDSType;
typedef Dc1Ptr< IMp7JrsVisualDSType > Mp7JrsVisualDSPtr;
class IMp7JrsStillRegionFeatureType;
typedef Dc1Ptr< IMp7JrsStillRegionFeatureType> Mp7JrsStillRegionFeaturePtr;
class IMp7JrsDominantColorType;
typedef Dc1Ptr< IMp7JrsDominantColorType > Mp7JrsDominantColorPtr;
class IMp7JrsScalableColorType;
typedef Dc1Ptr< IMp7JrsScalableColorType > Mp7JrsScalableColorPtr;
class IMp7JrsColorStructureType;
typedef Dc1Ptr< IMp7JrsColorStructureType > Mp7JrsColorStructurePtr;
class IMp7JrsColorLayoutType;
typedef Dc1Ptr< IMp7JrsColorLayoutType > Mp7JrsColorLayoutPtr;
class IMp7JrsColorTemperatureType;
typedef Dc1Ptr< IMp7JrsColorTemperatureType > Mp7JrsColorTemperaturePtr;
class IMp7JrsIlluminationInvariantColorType;
typedef Dc1Ptr< IMp7JrsIlluminationInvariantColorType > Mp7JrsIlluminationInvariantColorPtr;
class IMp7JrsEdgeHistogramType;
typedef Dc1Ptr< IMp7JrsEdgeHistogramType > Mp7JrsEdgeHistogramPtr;
class IMp7JrsHomogeneousTextureType;
typedef Dc1Ptr< IMp7JrsHomogeneousTextureType > Mp7JrsHomogeneousTexturePtr;
class IMp7JrsTextureBrowsingType;
typedef Dc1Ptr< IMp7JrsTextureBrowsingType > Mp7JrsTextureBrowsingPtr;
class IMp7JrsRegionShapeType;
typedef Dc1Ptr< IMp7JrsRegionShapeType > Mp7JrsRegionShapePtr;
class IMp7JrsContourShapeType;
typedef Dc1Ptr< IMp7JrsContourShapeType > Mp7JrsContourShapePtr;
#include "Mp7JrsVisualDSType.h"
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsStillRegionFeatureType for class Mp7JrsStillRegionFeatureType<br>
 * Located at: mpeg7-v4.xsd, line 1527<br>
 * Classified: Class<br>
 * Derived from: VisualDSType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsStillRegionFeatureType 
 :
		public IMp7JrsVisualDSType
{
public:
	// TODO: make these protected?
	IMp7JrsStillRegionFeatureType();
	virtual ~IMp7JrsStillRegionFeatureType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get DominantColor element.
	 * @return Mp7JrsDominantColorPtr
	 */
	virtual Mp7JrsDominantColorPtr GetDominantColor() const = 0;

	virtual bool IsValidDominantColor() const = 0;

	/**
	 * Set DominantColor element.
	 * @param item const Mp7JrsDominantColorPtr &
	 */
	virtual void SetDominantColor(const Mp7JrsDominantColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DominantColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDominantColor() = 0;

	/**
	 * Get ScalableColor element.
	 * @return Mp7JrsScalableColorPtr
	 */
	virtual Mp7JrsScalableColorPtr GetScalableColor() const = 0;

	virtual bool IsValidScalableColor() const = 0;

	/**
	 * Set ScalableColor element.
	 * @param item const Mp7JrsScalableColorPtr &
	 */
	virtual void SetScalableColor(const Mp7JrsScalableColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ScalableColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScalableColor() = 0;

	/**
	 * Get ColorStructure element.
	 * @return Mp7JrsColorStructurePtr
	 */
	virtual Mp7JrsColorStructurePtr GetColorStructure() const = 0;

	virtual bool IsValidColorStructure() const = 0;

	/**
	 * Set ColorStructure element.
	 * @param item const Mp7JrsColorStructurePtr &
	 */
	virtual void SetColorStructure(const Mp7JrsColorStructurePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ColorStructure element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorStructure() = 0;

	/**
	 * Get ColorLayout element.
	 * @return Mp7JrsColorLayoutPtr
	 */
	virtual Mp7JrsColorLayoutPtr GetColorLayout() const = 0;

	virtual bool IsValidColorLayout() const = 0;

	/**
	 * Set ColorLayout element.
	 * @param item const Mp7JrsColorLayoutPtr &
	 */
	virtual void SetColorLayout(const Mp7JrsColorLayoutPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ColorLayout element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorLayout() = 0;

	/**
	 * Get ColorTemperature element.
	 * @return Mp7JrsColorTemperaturePtr
	 */
	virtual Mp7JrsColorTemperaturePtr GetColorTemperature() const = 0;

	virtual bool IsValidColorTemperature() const = 0;

	/**
	 * Set ColorTemperature element.
	 * @param item const Mp7JrsColorTemperaturePtr &
	 */
	virtual void SetColorTemperature(const Mp7JrsColorTemperaturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ColorTemperature element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorTemperature() = 0;

	/**
	 * Get IlluminationCompensatedColor element.
	 * @return Mp7JrsIlluminationInvariantColorPtr
	 */
	virtual Mp7JrsIlluminationInvariantColorPtr GetIlluminationCompensatedColor() const = 0;

	virtual bool IsValidIlluminationCompensatedColor() const = 0;

	/**
	 * Set IlluminationCompensatedColor element.
	 * @param item const Mp7JrsIlluminationInvariantColorPtr &
	 */
	virtual void SetIlluminationCompensatedColor(const Mp7JrsIlluminationInvariantColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate IlluminationCompensatedColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateIlluminationCompensatedColor() = 0;

	/**
	 * Get Edge element.
	 * @return Mp7JrsEdgeHistogramPtr
	 */
	virtual Mp7JrsEdgeHistogramPtr GetEdge() const = 0;

	virtual bool IsValidEdge() const = 0;

	/**
	 * Set Edge element.
	 * @param item const Mp7JrsEdgeHistogramPtr &
	 */
	virtual void SetEdge(const Mp7JrsEdgeHistogramPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Edge element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEdge() = 0;

	/**
	 * Get HomogeneousPattern element.
	 * @return Mp7JrsHomogeneousTexturePtr
	 */
	virtual Mp7JrsHomogeneousTexturePtr GetHomogeneousPattern() const = 0;

	virtual bool IsValidHomogeneousPattern() const = 0;

	/**
	 * Set HomogeneousPattern element.
	 * @param item const Mp7JrsHomogeneousTexturePtr &
	 */
	virtual void SetHomogeneousPattern(const Mp7JrsHomogeneousTexturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate HomogeneousPattern element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateHomogeneousPattern() = 0;

	/**
	 * Get TextureBrowsing element.
	 * @return Mp7JrsTextureBrowsingPtr
	 */
	virtual Mp7JrsTextureBrowsingPtr GetTextureBrowsing() const = 0;

	virtual bool IsValidTextureBrowsing() const = 0;

	/**
	 * Set TextureBrowsing element.
	 * @param item const Mp7JrsTextureBrowsingPtr &
	 */
	virtual void SetTextureBrowsing(const Mp7JrsTextureBrowsingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TextureBrowsing element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTextureBrowsing() = 0;

	/**
	 * Get ShapeMask element.
	 * @return Mp7JrsRegionShapePtr
	 */
	virtual Mp7JrsRegionShapePtr GetShapeMask() const = 0;

	virtual bool IsValidShapeMask() const = 0;

	/**
	 * Set ShapeMask element.
	 * @param item const Mp7JrsRegionShapePtr &
	 */
	virtual void SetShapeMask(const Mp7JrsRegionShapePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ShapeMask element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateShapeMask() = 0;

	/**
	 * Get Contour element.
	 * @return Mp7JrsContourShapePtr
	 */
	virtual Mp7JrsContourShapePtr GetContour() const = 0;

	virtual bool IsValidContour() const = 0;

	/**
	 * Set Contour element.
	 * @param item const Mp7JrsContourShapePtr &
	 */
	virtual void SetContour(const Mp7JrsContourShapePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Contour element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateContour() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of StillRegionFeatureType.
	 * Currently this type contains 12 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>DominantColor</li>
	 * <li>ScalableColor</li>
	 * <li>ColorStructure</li>
	 * <li>ColorLayout</li>
	 * <li>ColorTemperature</li>
	 * <li>IlluminationCompensatedColor</li>
	 * <li>Edge</li>
	 * <li>HomogeneousPattern</li>
	 * <li>TextureBrowsing</li>
	 * <li>ShapeMask</li>
	 * <li>Contour</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsStillRegionFeatureType<br>
 * Located at: mpeg7-v4.xsd, line 1527<br>
 * Classified: Class<br>
 * Derived from: VisualDSType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 1527.<br>
 */
class DC1_EXPORT Mp7JrsStillRegionFeatureType :
		public IMp7JrsStillRegionFeatureType
{
	friend class Mp7JrsStillRegionFeatureTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsStillRegionFeatureType
	 
	 * @return Dc1Ptr< Mp7JrsVisualDSType >
	 */
	virtual Dc1Ptr< Mp7JrsVisualDSType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get DominantColor element.
	 * @return Mp7JrsDominantColorPtr
	 */
	virtual Mp7JrsDominantColorPtr GetDominantColor() const;

	virtual bool IsValidDominantColor() const;

	/*
	 * Set DominantColor element.
	 * @param item const Mp7JrsDominantColorPtr &
	 */
	virtual void SetDominantColor(const Mp7JrsDominantColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DominantColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDominantColor();

	/*
	 * Get ScalableColor element.
	 * @return Mp7JrsScalableColorPtr
	 */
	virtual Mp7JrsScalableColorPtr GetScalableColor() const;

	virtual bool IsValidScalableColor() const;

	/*
	 * Set ScalableColor element.
	 * @param item const Mp7JrsScalableColorPtr &
	 */
	virtual void SetScalableColor(const Mp7JrsScalableColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ScalableColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScalableColor();

	/*
	 * Get ColorStructure element.
	 * @return Mp7JrsColorStructurePtr
	 */
	virtual Mp7JrsColorStructurePtr GetColorStructure() const;

	virtual bool IsValidColorStructure() const;

	/*
	 * Set ColorStructure element.
	 * @param item const Mp7JrsColorStructurePtr &
	 */
	virtual void SetColorStructure(const Mp7JrsColorStructurePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ColorStructure element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorStructure();

	/*
	 * Get ColorLayout element.
	 * @return Mp7JrsColorLayoutPtr
	 */
	virtual Mp7JrsColorLayoutPtr GetColorLayout() const;

	virtual bool IsValidColorLayout() const;

	/*
	 * Set ColorLayout element.
	 * @param item const Mp7JrsColorLayoutPtr &
	 */
	virtual void SetColorLayout(const Mp7JrsColorLayoutPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ColorLayout element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorLayout();

	/*
	 * Get ColorTemperature element.
	 * @return Mp7JrsColorTemperaturePtr
	 */
	virtual Mp7JrsColorTemperaturePtr GetColorTemperature() const;

	virtual bool IsValidColorTemperature() const;

	/*
	 * Set ColorTemperature element.
	 * @param item const Mp7JrsColorTemperaturePtr &
	 */
	virtual void SetColorTemperature(const Mp7JrsColorTemperaturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ColorTemperature element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateColorTemperature();

	/*
	 * Get IlluminationCompensatedColor element.
	 * @return Mp7JrsIlluminationInvariantColorPtr
	 */
	virtual Mp7JrsIlluminationInvariantColorPtr GetIlluminationCompensatedColor() const;

	virtual bool IsValidIlluminationCompensatedColor() const;

	/*
	 * Set IlluminationCompensatedColor element.
	 * @param item const Mp7JrsIlluminationInvariantColorPtr &
	 */
	virtual void SetIlluminationCompensatedColor(const Mp7JrsIlluminationInvariantColorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate IlluminationCompensatedColor element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateIlluminationCompensatedColor();

	/*
	 * Get Edge element.
	 * @return Mp7JrsEdgeHistogramPtr
	 */
	virtual Mp7JrsEdgeHistogramPtr GetEdge() const;

	virtual bool IsValidEdge() const;

	/*
	 * Set Edge element.
	 * @param item const Mp7JrsEdgeHistogramPtr &
	 */
	virtual void SetEdge(const Mp7JrsEdgeHistogramPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Edge element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEdge();

	/*
	 * Get HomogeneousPattern element.
	 * @return Mp7JrsHomogeneousTexturePtr
	 */
	virtual Mp7JrsHomogeneousTexturePtr GetHomogeneousPattern() const;

	virtual bool IsValidHomogeneousPattern() const;

	/*
	 * Set HomogeneousPattern element.
	 * @param item const Mp7JrsHomogeneousTexturePtr &
	 */
	virtual void SetHomogeneousPattern(const Mp7JrsHomogeneousTexturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate HomogeneousPattern element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateHomogeneousPattern();

	/*
	 * Get TextureBrowsing element.
	 * @return Mp7JrsTextureBrowsingPtr
	 */
	virtual Mp7JrsTextureBrowsingPtr GetTextureBrowsing() const;

	virtual bool IsValidTextureBrowsing() const;

	/*
	 * Set TextureBrowsing element.
	 * @param item const Mp7JrsTextureBrowsingPtr &
	 */
	virtual void SetTextureBrowsing(const Mp7JrsTextureBrowsingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TextureBrowsing element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTextureBrowsing();

	/*
	 * Get ShapeMask element.
	 * @return Mp7JrsRegionShapePtr
	 */
	virtual Mp7JrsRegionShapePtr GetShapeMask() const;

	virtual bool IsValidShapeMask() const;

	/*
	 * Set ShapeMask element.
	 * @param item const Mp7JrsRegionShapePtr &
	 */
	virtual void SetShapeMask(const Mp7JrsRegionShapePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ShapeMask element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateShapeMask();

	/*
	 * Get Contour element.
	 * @return Mp7JrsContourShapePtr
	 */
	virtual Mp7JrsContourShapePtr GetContour() const;

	virtual bool IsValidContour() const;

	/*
	 * Set Contour element.
	 * @param item const Mp7JrsContourShapePtr &
	 */
	virtual void SetContour(const Mp7JrsContourShapePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Contour element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateContour();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of StillRegionFeatureType.
	 * Currently this type contains 12 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>DominantColor</li>
	 * <li>ScalableColor</li>
	 * <li>ColorStructure</li>
	 * <li>ColorLayout</li>
	 * <li>ColorTemperature</li>
	 * <li>IlluminationCompensatedColor</li>
	 * <li>Edge</li>
	 * <li>HomogeneousPattern</li>
	 * <li>TextureBrowsing</li>
	 * <li>ShapeMask</li>
	 * <li>Contour</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsStillRegionFeatureType();

protected:
	Mp7JrsStillRegionFeatureType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsVisualDSType > m_Base;

	Mp7JrsDominantColorPtr m_DominantColor;
	bool m_DominantColor_Exist; // For optional elements 
	Mp7JrsScalableColorPtr m_ScalableColor;
	bool m_ScalableColor_Exist; // For optional elements 
	Mp7JrsColorStructurePtr m_ColorStructure;
	bool m_ColorStructure_Exist; // For optional elements 
	Mp7JrsColorLayoutPtr m_ColorLayout;
	bool m_ColorLayout_Exist; // For optional elements 
	Mp7JrsColorTemperaturePtr m_ColorTemperature;
	bool m_ColorTemperature_Exist; // For optional elements 
	Mp7JrsIlluminationInvariantColorPtr m_IlluminationCompensatedColor;
	bool m_IlluminationCompensatedColor_Exist; // For optional elements 
	Mp7JrsEdgeHistogramPtr m_Edge;
	bool m_Edge_Exist; // For optional elements 
	Mp7JrsHomogeneousTexturePtr m_HomogeneousPattern;
	bool m_HomogeneousPattern_Exist; // For optional elements 
	Mp7JrsTextureBrowsingPtr m_TextureBrowsing;
	bool m_TextureBrowsing_Exist; // For optional elements 
	Mp7JrsRegionShapePtr m_ShapeMask;
	bool m_ShapeMask_Exist; // For optional elements 
	Mp7JrsContourShapePtr m_Contour;
	bool m_Contour_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3481MP7JRSSTILLREGIONFEATURETYPE_H

