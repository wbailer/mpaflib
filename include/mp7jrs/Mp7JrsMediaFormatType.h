/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2299MP7JRSMEDIAFORMATTYPE_H
#define _2299MP7JRSMEDIAFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMediaFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtInclude.h


class IMp7JrsDType;
typedef Dc1Ptr< IMp7JrsDType > Mp7JrsDPtr;
class IMp7JrsMediaFormatType;
typedef Dc1Ptr< IMp7JrsMediaFormatType> Mp7JrsMediaFormatPtr;
class IMp7JrsControlledTermUseType;
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;
class IMp7JrsMediaFormatType_BitRate_LocalType;
typedef Dc1Ptr< IMp7JrsMediaFormatType_BitRate_LocalType > Mp7JrsMediaFormatType_BitRate_LocalPtr;
class IMp7JrsMediaFormatType_ScalableCoding_LocalType2;
typedef Dc1Ptr< IMp7JrsMediaFormatType_ScalableCoding_LocalType2 > Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr;
class IMp7JrsMediaFormatType_VisualCoding_LocalType;
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_LocalType > Mp7JrsMediaFormatType_VisualCoding_LocalPtr;
class IMp7JrsMediaFormatType_AudioCoding_LocalType;
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_LocalType > Mp7JrsMediaFormatType_AudioCoding_LocalPtr;
#include "Mp7JrsDType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsMediaFormatType for class Mp7JrsMediaFormatType<br>
 * Located at: mpeg7-v4.xsd, line 5863<br>
 * Classified: Class<br>
 * Derived from: DType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaFormatType 
 :
		public IMp7JrsDType
{
public:
	// TODO: make these protected?
	IMp7JrsMediaFormatType();
	virtual ~IMp7JrsMediaFormatType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Content element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetContent() const = 0;

	/**
	 * Set Content element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	// Mandatory			
	virtual void SetContent(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Medium element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetMedium() const = 0;

	virtual bool IsValidMedium() const = 0;

	/**
	 * Set Medium element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetMedium(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Medium element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMedium() = 0;

	/**
	 * Get FileFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetFileFormat() const = 0;

	virtual bool IsValidFileFormat() const = 0;

	/**
	 * Set FileFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetFileFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate FileFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFileFormat() = 0;

	/**
	 * Get FileSize element.
	 * @return unsigned
	 */
	virtual unsigned GetFileSize() const = 0;

	virtual bool IsValidFileSize() const = 0;

	/**
	 * Set FileSize element.
	 * @param item unsigned
	 */
	virtual void SetFileSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate FileSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFileSize() = 0;

	/**
	 * Get System element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetSystem() const = 0;

	virtual bool IsValidSystem() const = 0;

	/**
	 * Set System element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetSystem(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate System element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSystem() = 0;

	/**
	 * Get Bandwidth element.
	 * @return float
	 */
	virtual float GetBandwidth() const = 0;

	virtual bool IsValidBandwidth() const = 0;

	/**
	 * Set Bandwidth element.
	 * @param item float
	 */
	virtual void SetBandwidth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Bandwidth element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBandwidth() = 0;

	/**
	 * Get BitRate element.
	 * @return Mp7JrsMediaFormatType_BitRate_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_BitRate_LocalPtr GetBitRate() const = 0;

	virtual bool IsValidBitRate() const = 0;

	/**
	 * Set BitRate element.
	 * @param item const Mp7JrsMediaFormatType_BitRate_LocalPtr &
	 */
	virtual void SetBitRate(const Mp7JrsMediaFormatType_BitRate_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate BitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBitRate() = 0;

	/**
	 * Get TargetChannelBitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetTargetChannelBitRate() const = 0;

	virtual bool IsValidTargetChannelBitRate() const = 0;

	/**
	 * Set TargetChannelBitRate element.
	 * @param item unsigned
	 */
	virtual void SetTargetChannelBitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TargetChannelBitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTargetChannelBitRate() = 0;

	/**
	 * Get ScalableCoding element.
	 * @return Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr
	 */
	virtual Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr GetScalableCoding() const = 0;

	virtual bool IsValidScalableCoding() const = 0;

	/**
	 * Set ScalableCoding element.
	 * @param item const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr &
	 */
	virtual void SetScalableCoding(const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ScalableCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScalableCoding() = 0;

	/**
	 * Get VisualCoding element.
	 * @return Mp7JrsMediaFormatType_VisualCoding_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_VisualCoding_LocalPtr GetVisualCoding() const = 0;

	virtual bool IsValidVisualCoding() const = 0;

	/**
	 * Set VisualCoding element.
	 * @param item const Mp7JrsMediaFormatType_VisualCoding_LocalPtr &
	 */
	virtual void SetVisualCoding(const Mp7JrsMediaFormatType_VisualCoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate VisualCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVisualCoding() = 0;

	/**
	 * Get AudioCoding element.
	 * @return Mp7JrsMediaFormatType_AudioCoding_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_AudioCoding_LocalPtr GetAudioCoding() const = 0;

	virtual bool IsValidAudioCoding() const = 0;

	/**
	 * Set AudioCoding element.
	 * @param item const Mp7JrsMediaFormatType_AudioCoding_LocalPtr &
	 */
	virtual void SetAudioCoding(const Mp7JrsMediaFormatType_AudioCoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate AudioCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateAudioCoding() = 0;

	/**
	 * Get SceneCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetSceneCodingFormat() const = 0;

	virtual bool IsValidSceneCodingFormat() const = 0;

	/**
	 * Set SceneCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetSceneCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate SceneCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSceneCodingFormat() = 0;

	/**
	 * Get GraphicsCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetGraphicsCodingFormat() const = 0;

	virtual bool IsValidGraphicsCodingFormat() const = 0;

	/**
	 * Set GraphicsCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetGraphicsCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate GraphicsCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateGraphicsCodingFormat() = 0;

	/**
	 * Get OtherCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetOtherCodingFormat() const = 0;

	virtual bool IsValidOtherCodingFormat() const = 0;

	/**
	 * Set OtherCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetOtherCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate OtherCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateOtherCodingFormat() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType.
	 * Currently this type contains 11 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Content</li>
	 * <li>Medium</li>
	 * <li>FileFormat</li>
	 * <li>System</li>
	 * <li>BitRate</li>
	 * <li>ScalableCoding</li>
	 * <li>VisualCoding</li>
	 * <li>AudioCoding</li>
	 * <li>SceneCodingFormat</li>
	 * <li>GraphicsCodingFormat</li>
	 * <li>OtherCodingFormat</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMediaFormatType<br>
 * Located at: mpeg7-v4.xsd, line 5863<br>
 * Classified: Class<br>
 * Derived from: DType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 5863.<br>
 */
class DC1_EXPORT Mp7JrsMediaFormatType :
		public IMp7JrsMediaFormatType
{
	friend class Mp7JrsMediaFormatTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsMediaFormatType
	 
	 * @return Dc1Ptr< Mp7JrsDType >
	 */
	virtual Dc1Ptr< Mp7JrsDType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Content element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetContent() const;

	/*
	 * Set Content element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	// Mandatory			
	virtual void SetContent(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Medium element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetMedium() const;

	virtual bool IsValidMedium() const;

	/*
	 * Set Medium element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetMedium(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Medium element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMedium();

	/*
	 * Get FileFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetFileFormat() const;

	virtual bool IsValidFileFormat() const;

	/*
	 * Set FileFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetFileFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate FileFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFileFormat();

	/*
	 * Get FileSize element.
	 * @return unsigned
	 */
	virtual unsigned GetFileSize() const;

	virtual bool IsValidFileSize() const;

	/*
	 * Set FileSize element.
	 * @param item unsigned
	 */
	virtual void SetFileSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate FileSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFileSize();

	/*
	 * Get System element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetSystem() const;

	virtual bool IsValidSystem() const;

	/*
	 * Set System element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetSystem(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate System element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSystem();

	/*
	 * Get Bandwidth element.
	 * @return float
	 */
	virtual float GetBandwidth() const;

	virtual bool IsValidBandwidth() const;

	/*
	 * Set Bandwidth element.
	 * @param item float
	 */
	virtual void SetBandwidth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Bandwidth element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBandwidth();

	/*
	 * Get BitRate element.
	 * @return Mp7JrsMediaFormatType_BitRate_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_BitRate_LocalPtr GetBitRate() const;

	virtual bool IsValidBitRate() const;

	/*
	 * Set BitRate element.
	 * @param item const Mp7JrsMediaFormatType_BitRate_LocalPtr &
	 */
	virtual void SetBitRate(const Mp7JrsMediaFormatType_BitRate_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate BitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBitRate();

	/*
	 * Get TargetChannelBitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetTargetChannelBitRate() const;

	virtual bool IsValidTargetChannelBitRate() const;

	/*
	 * Set TargetChannelBitRate element.
	 * @param item unsigned
	 */
	virtual void SetTargetChannelBitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TargetChannelBitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTargetChannelBitRate();

	/*
	 * Get ScalableCoding element.
	 * @return Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr
	 */
	virtual Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr GetScalableCoding() const;

	virtual bool IsValidScalableCoding() const;

	/*
	 * Set ScalableCoding element.
	 * @param item const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr &
	 */
	virtual void SetScalableCoding(const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ScalableCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScalableCoding();

	/*
	 * Get VisualCoding element.
	 * @return Mp7JrsMediaFormatType_VisualCoding_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_VisualCoding_LocalPtr GetVisualCoding() const;

	virtual bool IsValidVisualCoding() const;

	/*
	 * Set VisualCoding element.
	 * @param item const Mp7JrsMediaFormatType_VisualCoding_LocalPtr &
	 */
	virtual void SetVisualCoding(const Mp7JrsMediaFormatType_VisualCoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate VisualCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVisualCoding();

	/*
	 * Get AudioCoding element.
	 * @return Mp7JrsMediaFormatType_AudioCoding_LocalPtr
	 */
	virtual Mp7JrsMediaFormatType_AudioCoding_LocalPtr GetAudioCoding() const;

	virtual bool IsValidAudioCoding() const;

	/*
	 * Set AudioCoding element.
	 * @param item const Mp7JrsMediaFormatType_AudioCoding_LocalPtr &
	 */
	virtual void SetAudioCoding(const Mp7JrsMediaFormatType_AudioCoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate AudioCoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateAudioCoding();

	/*
	 * Get SceneCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetSceneCodingFormat() const;

	virtual bool IsValidSceneCodingFormat() const;

	/*
	 * Set SceneCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetSceneCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate SceneCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSceneCodingFormat();

	/*
	 * Get GraphicsCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetGraphicsCodingFormat() const;

	virtual bool IsValidGraphicsCodingFormat() const;

	/*
	 * Set GraphicsCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetGraphicsCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate GraphicsCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateGraphicsCodingFormat();

	/*
	 * Get OtherCodingFormat element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetOtherCodingFormat() const;

	virtual bool IsValidOtherCodingFormat() const;

	/*
	 * Set OtherCodingFormat element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetOtherCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate OtherCodingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateOtherCodingFormat();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType.
	 * Currently this type contains 11 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Content</li>
	 * <li>Medium</li>
	 * <li>FileFormat</li>
	 * <li>System</li>
	 * <li>BitRate</li>
	 * <li>ScalableCoding</li>
	 * <li>VisualCoding</li>
	 * <li>AudioCoding</li>
	 * <li>SceneCodingFormat</li>
	 * <li>GraphicsCodingFormat</li>
	 * <li>OtherCodingFormat</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMediaFormatType();

protected:
	Mp7JrsMediaFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsDType > m_Base;

	Mp7JrsControlledTermUsePtr m_Content;
	Mp7JrsControlledTermUsePtr m_Medium;
	bool m_Medium_Exist; // For optional elements 
	Mp7JrsControlledTermUsePtr m_FileFormat;
	bool m_FileFormat_Exist; // For optional elements 
	unsigned m_FileSize;
	bool m_FileSize_Exist; // For optional elements 
	Mp7JrsControlledTermUsePtr m_System;
	bool m_System_Exist; // For optional elements 
	float m_Bandwidth;
	bool m_Bandwidth_Exist; // For optional elements 
	Mp7JrsMediaFormatType_BitRate_LocalPtr m_BitRate;
	bool m_BitRate_Exist; // For optional elements 
	unsigned m_TargetChannelBitRate;
	bool m_TargetChannelBitRate_Exist; // For optional elements 
	Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr m_ScalableCoding;
	bool m_ScalableCoding_Exist; // For optional elements 
	Mp7JrsMediaFormatType_VisualCoding_LocalPtr m_VisualCoding;
	bool m_VisualCoding_Exist; // For optional elements 
	Mp7JrsMediaFormatType_AudioCoding_LocalPtr m_AudioCoding;
	bool m_AudioCoding_Exist; // For optional elements 
	Mp7JrsControlledTermUsePtr m_SceneCodingFormat;
	bool m_SceneCodingFormat_Exist; // For optional elements 
	Mp7JrsControlledTermUsePtr m_GraphicsCodingFormat;
	bool m_GraphicsCodingFormat_Exist; // For optional elements 
	Mp7JrsControlledTermUsePtr m_OtherCodingFormat;
	bool m_OtherCodingFormat_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2299MP7JRSMEDIAFORMATTYPE_H

