/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// PatternType_H

#ifndef _3755MP7JRSTIMEZONETYPE_VALUE_LOCALTYPE_H
#define _3755MP7JRSTIMEZONETYPE_VALUE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"



#include "Mp7JrsTimeZoneType_value_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtInclude.h


class IMp7JrsTimeZoneType_value_LocalType;typedef Dc1Ptr< IMp7JrsTimeZoneType_value_LocalType> Mp7JrsTimeZoneType_value_LocalPtr;
/**
 * Generated interface IMp7JrsTimeZoneType_value_LocalType for Pattern Mp7JrsTimeZoneType_value_LocalType
 */
class DC1_EXPORT IMp7JrsTimeZoneType_value_LocalType :
	public Dc1Node
{
public:
	virtual int GetTimezoneSign() const = 0;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneHours() const = 0;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneMinutes() const = 0;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetTimezone_Exist() const = 0;

	virtual void SetTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtMethodDef.h


};

/*
 * Generated pattern Mp7JrsTimeZoneType_value_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5369<br>
 * Regular expression: <br>
 * Z|((\-|\+)\d{2}:\d{2})<br>
 */
class DC1_EXPORT Mp7JrsTimeZoneType_value_LocalType :
	public IMp7JrsTimeZoneType_value_LocalType
{
	friend class Mp7JrsTimeZoneType_value_LocalTypeFactory;
	
public: 
	virtual int GetTimezoneSign() const;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneHours() const;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneMinutes() const;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetTimezone_Exist() const;

	virtual void SetTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	virtual XMLCh * ToText() const;
	virtual bool Parse(const XMLCh * const txt);
	virtual bool ContentFromString(const XMLCh * const txt);

	virtual XMLCh * ContentToString() const;

	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtMyMethodDef.h


	virtual ~Mp7JrsTimeZoneType_value_LocalType();

protected:
	Mp7JrsTimeZoneType_value_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_TimezoneSign;
	int m_TimezoneHours;
	int m_TimezoneMinutes;
	bool m_Timezone_Exist;

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtPropDef.h


protected:
	void Init();
	void Cleanup();
	
protected:
    const XMLCh * const m_nsURI; // For namespace handling
};

#endif // _3755MP7JRSTIMEZONETYPE_VALUE_LOCALTYPE_H

