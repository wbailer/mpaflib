/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2341MP7JRSMEDIAFORMATTYPE_VISUALCODING_PIXEL_LOCALTYPE_H
#define _2341MP7JRSMEDIAFORMATTYPE_VISUALCODING_PIXEL_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType_ExtInclude.h


class IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType;
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType> Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr;
class IMp7JrsnonNegativeReal;
typedef Dc1Ptr< IMp7JrsnonNegativeReal > Mp7JrsnonNegativeRealPtr;

/** 
 * Generated interface IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType for class Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5938<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType();
	virtual ~IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType();
	/**
	 * Get resolution attribute.
	 * @return unsigned
	 */
	virtual unsigned Getresolution() const = 0;

	/**
	 * Get resolution attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existresolution() const = 0;

	/**
	 * Set resolution attribute.
	 * @param item unsigned
	 */
	virtual void Setresolution(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate resolution attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateresolution() = 0;

	/**
	 * Get aspectRatio attribute.
	 * @return Mp7JrsnonNegativeRealPtr
	 */
	virtual Mp7JrsnonNegativeRealPtr GetaspectRatio() const = 0;

	/**
	 * Get aspectRatio attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaspectRatio() const = 0;

	/**
	 * Set aspectRatio attribute.
	 * @param item const Mp7JrsnonNegativeRealPtr &
	 */
	virtual void SetaspectRatio(const Mp7JrsnonNegativeRealPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate aspectRatio attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaspectRatio() = 0;

	/**
	 * Get bitsPer attribute.
	 * @return unsigned
	 */
	virtual unsigned GetbitsPer() const = 0;

	/**
	 * Get bitsPer attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbitsPer() const = 0;

	/**
	 * Set bitsPer attribute.
	 * @param item unsigned
	 */
	virtual void SetbitsPer(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitsPer attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitsPer() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType_VisualCoding_Pixel_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaFormatType_VisualCoding_Pixel_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5938<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 5938.<br>
 */
class DC1_EXPORT Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType :
		public IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType
{
	friend class Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get resolution attribute.
	 * @return unsigned
	 */
	virtual unsigned Getresolution() const;

	/*
	 * Get resolution attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existresolution() const;

	/*
	 * Set resolution attribute.
	 * @param item unsigned
	 */
	virtual void Setresolution(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate resolution attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateresolution();

	/*
	 * Get aspectRatio attribute.
	 * @return Mp7JrsnonNegativeRealPtr
	 */
	virtual Mp7JrsnonNegativeRealPtr GetaspectRatio() const;

	/*
	 * Get aspectRatio attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaspectRatio() const;

	/*
	 * Set aspectRatio attribute.
	 * @param item const Mp7JrsnonNegativeRealPtr &
	 */
	virtual void SetaspectRatio(const Mp7JrsnonNegativeRealPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate aspectRatio attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaspectRatio();

	/*
	 * Get bitsPer attribute.
	 * @return unsigned
	 */
	virtual unsigned GetbitsPer() const;

	/*
	 * Get bitsPer attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbitsPer() const;

	/*
	 * Set bitsPer attribute.
	 * @param item unsigned
	 */
	virtual void SetbitsPer(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitsPer attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitsPer();

	/*
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType_VisualCoding_Pixel_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaFormatType_VisualCoding_Pixel_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType();

protected:
	Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	unsigned m_resolution;
	bool m_resolution_Exist;
	Mp7JrsnonNegativeRealPtr m_aspectRatio;
	bool m_aspectRatio_Exist;
	unsigned m_bitsPer;
	bool m_bitsPer_Exist;



// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2341MP7JRSMEDIAFORMATTYPE_VISUALCODING_PIXEL_LOCALTYPE_H

