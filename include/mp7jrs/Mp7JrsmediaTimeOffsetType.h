/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// PatternType_H

#ifndef _2407MP7JRSMEDIATIMEOFFSETTYPE_H
#define _2407MP7JRSMEDIATIMEOFFSETTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"



#include "Mp7JrsmediaTimeOffsetTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"

// no includefile for extension defined 
// file Mp7JrsmediaTimeOffsetType_ExtInclude.h


class IMp7JrsmediaTimeOffsetType;typedef Dc1Ptr< IMp7JrsmediaTimeOffsetType> Mp7JrsmediaTimeOffsetPtr;
/**
 * Generated interface IMp7JrsmediaTimeOffsetType for Pattern Mp7JrsmediaTimeOffsetType
 */
class DC1_EXPORT IMp7JrsmediaTimeOffsetType :
	public Dc1Node
{
public:
	virtual int GetOffsetSign() const = 0;

	virtual void SetOffsetSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetDays() const = 0;

	virtual void SetDays(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetHours() const = 0;

	virtual void SetHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMinutes() const = 0;

	virtual void SetMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetSeconds() const = 0;

	virtual void SetSeconds(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetNumberOfFractions() const = 0;

	virtual void SetNumberOfFractions(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetTime_Exist() const = 0;

	virtual void SetTime_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetFractionsPerSecond() const = 0;

	virtual void SetFractionsPerSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


// no includefile for extension defined 
// file Mp7JrsmediaTimeOffsetType_ExtMethodDef.h


};

/*
 * Generated pattern Mp7JrsmediaTimeOffsetType<br>
 * Located at: mpeg7-v4.xsd, line 4435<br>
 * Regular expression: <br>
 * \-?P(\d+D)?(T(\d+H)?(\d+M)?(\d+S)?(\d+N)?)?(\d+F)?<br>
 */
class DC1_EXPORT Mp7JrsmediaTimeOffsetType :
	public IMp7JrsmediaTimeOffsetType
{
	friend class Mp7JrsmediaTimeOffsetTypeFactory;
	
public: 
	virtual int GetOffsetSign() const;

	virtual void SetOffsetSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetDays() const;

	virtual void SetDays(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetHours() const;

	virtual void SetHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMinutes() const;

	virtual void SetMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetSeconds() const;

	virtual void SetSeconds(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetNumberOfFractions() const;

	virtual void SetNumberOfFractions(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetTime_Exist() const;

	virtual void SetTime_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetFractionsPerSecond() const;

	virtual void SetFractionsPerSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	virtual XMLCh * ToText() const;
	virtual bool Parse(const XMLCh * const txt);
	virtual bool ContentFromString(const XMLCh * const txt);

	virtual XMLCh * ContentToString() const;

	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file Mp7JrsmediaTimeOffsetType_ExtMyMethodDef.h


	virtual ~Mp7JrsmediaTimeOffsetType();

protected:
	Mp7JrsmediaTimeOffsetType();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_OffsetSign;
	int m_Days;
	int m_Hours;
	int m_Minutes;
	int m_Seconds;
	int m_NumberOfFractions;
	bool m_Time_Exist;
	int m_FractionsPerSecond;

// no includefile for extension defined 
// file Mp7JrsmediaTimeOffsetType_ExtPropDef.h


protected:
	void Init();
	void Cleanup();
	
protected:
    const XMLCh * const m_nsURI; // For namespace handling
};

#endif // _2407MP7JRSMEDIATIMEOFFSETTYPE_H

