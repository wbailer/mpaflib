/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1845MP7JRSEDITEDVIDEOEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H
#define _1845MP7JRSEDITEDVIDEOEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtInclude.h


class IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType;
typedef Dc1Ptr< IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType> Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr;
class IMp7JrsShotType;
typedef Dc1Ptr< IMp7JrsShotType > Mp7JrsShotPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsGlobalTransitionType;
typedef Dc1Ptr< IMp7JrsGlobalTransitionType > Mp7JrsGlobalTransitionPtr;

/** 
 * Generated interface IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType for class Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType();
	virtual ~IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType();
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get Shot element.
	 * @return Mp7JrsShotPtr
	 */
	virtual Mp7JrsShotPtr GetShot() const = 0;

	/**
	 * Set Shot element.
	 * @param item const Mp7JrsShotPtr &
	 */
	// Mandatory			
	virtual void SetShot(const Mp7JrsShotPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ShotRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetShotRef() const = 0;

	/**
	 * Set ShotRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get GlobalTransition element.
	 * @return Mp7JrsGlobalTransitionPtr
	 */
	virtual Mp7JrsGlobalTransitionPtr GetGlobalTransition() const = 0;

	/**
	 * Set GlobalTransition element.
	 * @param item const Mp7JrsGlobalTransitionPtr &
	 */
	// Mandatory			
	virtual void SetGlobalTransition(const Mp7JrsGlobalTransitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get GlobalTransitionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetGlobalTransitionRef() const = 0;

	/**
	 * Set GlobalTransitionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetGlobalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
		//@}
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 7893.<br>
 */
class DC1_EXPORT Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType :
		public IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType
{
	friend class Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get Shot element.
	 * @return Mp7JrsShotPtr
	 */
	virtual Mp7JrsShotPtr GetShot() const;

	/*
	 * Set Shot element.
	 * @param item const Mp7JrsShotPtr &
	 */
	// Mandatory			
	virtual void SetShot(const Mp7JrsShotPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ShotRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetShotRef() const;

	/*
	 * Set ShotRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get GlobalTransition element.
	 * @return Mp7JrsGlobalTransitionPtr
	 */
	virtual Mp7JrsGlobalTransitionPtr GetGlobalTransition() const;

	/*
	 * Set GlobalTransition element.
	 * @param item const Mp7JrsGlobalTransitionPtr &
	 */
	// Mandatory			
	virtual void SetGlobalTransition(const Mp7JrsGlobalTransitionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get GlobalTransitionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetGlobalTransitionRef() const;

	/*
	 * Set GlobalTransitionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetGlobalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType();

protected:
	Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
// DefinionChoice
	Mp7JrsShotPtr m_Shot;
	Mp7JrsReferencePtr m_ShotRef;
// End DefinionChoice
// DefinionChoice
	Mp7JrsGlobalTransitionPtr m_GlobalTransition;
	Mp7JrsReferencePtr m_GlobalTransitionRef;
// End DefinionChoice
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1845MP7JRSEDITEDVIDEOEDITINGTEMPORALDECOMPOSITIONTYPE_LOCALTYPE_H

