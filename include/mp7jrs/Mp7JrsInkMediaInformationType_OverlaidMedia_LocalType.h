/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2135MP7JRSINKMEDIAINFORMATIONTYPE_OVERLAIDMEDIA_LOCALTYPE_H
#define _2135MP7JRSINKMEDIAINFORMATIONTYPE_OVERLAIDMEDIA_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsInkMediaInformationType_OverlaidMedia_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtInclude.h


class IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType;
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType> Mp7JrsInkMediaInformationType_OverlaidMedia_LocalPtr;
class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsRegionLocatorType;
typedef Dc1Ptr< IMp7JrsRegionLocatorType > Mp7JrsRegionLocatorPtr;

/** 
 * Generated interface IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType for class Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 7679<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType();
	virtual ~IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType();
	/**
	 * Get fieldIDRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfieldIDRef() const = 0;

	/**
	 * Get fieldIDRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfieldIDRef() const = 0;

	/**
	 * Set fieldIDRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfieldIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate fieldIDRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefieldIDRef() = 0;

	/**
	 * Get translationX attribute.
	 * @return int
	 */
	virtual int GettranslationX() const = 0;

	/**
	 * Get translationX attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttranslationX() const = 0;

	/**
	 * Set translationX attribute.
	 * @param item int
	 */
	virtual void SettranslationX(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate translationX attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetranslationX() = 0;

	/**
	 * Get translationY attribute.
	 * @return int
	 */
	virtual int GettranslationY() const = 0;

	/**
	 * Get translationY attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttranslationY() const = 0;

	/**
	 * Set translationY attribute.
	 * @param item int
	 */
	virtual void SettranslationY(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate translationY attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetranslationY() = 0;

	/**
	 * Get rotation attribute.
	 * @return float
	 */
	virtual float Getrotation() const = 0;

	/**
	 * Get rotation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrotation() const = 0;

	/**
	 * Set rotation attribute.
	 * @param item float
	 */
	virtual void Setrotation(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate rotation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterotation() = 0;

	/**
	 * Get scaleX attribute.
	 * @return float
	 */
	virtual float GetscaleX() const = 0;

	/**
	 * Get scaleX attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistscaleX() const = 0;

	/**
	 * Set scaleX attribute.
	 * @param item float
	 */
	virtual void SetscaleX(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate scaleX attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescaleX() = 0;

	/**
	 * Get scaleY attribute.
	 * @return float
	 */
	virtual float GetscaleY() const = 0;

	/**
	 * Get scaleY attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistscaleY() const = 0;

	/**
	 * Set scaleY attribute.
	 * @param item float
	 */
	virtual void SetscaleY(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate scaleY attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescaleY() = 0;

		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get OverlaidMediaLocator element.
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetOverlaidMediaLocator() const = 0;

	/**
	 * Set OverlaidMediaLocator element.
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	// Mandatory			
	virtual void SetOverlaidMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get OverlaidMediaRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetOverlaidMediaRef() const = 0;

	/**
	 * Set OverlaidMediaRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetOverlaidMediaRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get MediaRegionLocator element.
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetMediaRegionLocator() const = 0;

	virtual bool IsValidMediaRegionLocator() const = 0;

	/**
	 * Set MediaRegionLocator element.
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	virtual void SetMediaRegionLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MediaRegionLocator element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMediaRegionLocator() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of InkMediaInformationType_OverlaidMedia_LocalType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>OverlaidMediaLocator</li>
	 * <li>OverlaidMediaRef</li>
	 * <li>MediaRegionLocator</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 7679<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 7679.<br>
 */
class DC1_EXPORT Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType :
		public IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType
{
	friend class Mp7JrsInkMediaInformationType_OverlaidMedia_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get fieldIDRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfieldIDRef() const;

	/*
	 * Get fieldIDRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfieldIDRef() const;

	/*
	 * Set fieldIDRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfieldIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate fieldIDRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefieldIDRef();

	/*
	 * Get translationX attribute.
	 * @return int
	 */
	virtual int GettranslationX() const;

	/*
	 * Get translationX attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttranslationX() const;

	/*
	 * Set translationX attribute.
	 * @param item int
	 */
	virtual void SettranslationX(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate translationX attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetranslationX();

	/*
	 * Get translationY attribute.
	 * @return int
	 */
	virtual int GettranslationY() const;

	/*
	 * Get translationY attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttranslationY() const;

	/*
	 * Set translationY attribute.
	 * @param item int
	 */
	virtual void SettranslationY(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate translationY attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetranslationY();

	/*
	 * Get rotation attribute.
	 * @return float
	 */
	virtual float Getrotation() const;

	/*
	 * Get rotation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrotation() const;

	/*
	 * Set rotation attribute.
	 * @param item float
	 */
	virtual void Setrotation(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate rotation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterotation();

	/*
	 * Get scaleX attribute.
	 * @return float
	 */
	virtual float GetscaleX() const;

	/*
	 * Get scaleX attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistscaleX() const;

	/*
	 * Set scaleX attribute.
	 * @param item float
	 */
	virtual void SetscaleX(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate scaleX attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescaleX();

	/*
	 * Get scaleY attribute.
	 * @return float
	 */
	virtual float GetscaleY() const;

	/*
	 * Get scaleY attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistscaleY() const;

	/*
	 * Set scaleY attribute.
	 * @param item float
	 */
	virtual void SetscaleY(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate scaleY attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescaleY();

		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get OverlaidMediaLocator element.
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetOverlaidMediaLocator() const;

	/*
	 * Set OverlaidMediaLocator element.
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	// Mandatory			
	virtual void SetOverlaidMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get OverlaidMediaRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetOverlaidMediaRef() const;

	/*
	 * Set OverlaidMediaRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetOverlaidMediaRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get MediaRegionLocator element.
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetMediaRegionLocator() const;

	virtual bool IsValidMediaRegionLocator() const;

	/*
	 * Set MediaRegionLocator element.
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	virtual void SetMediaRegionLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MediaRegionLocator element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMediaRegionLocator();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of InkMediaInformationType_OverlaidMedia_LocalType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>OverlaidMediaLocator</li>
	 * <li>OverlaidMediaRef</li>
	 * <li>MediaRegionLocator</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType();

protected:
	Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_fieldIDRef;
	bool m_fieldIDRef_Exist;
	int m_translationX;
	bool m_translationX_Exist;
	int m_translationX_Default;
	int m_translationY;
	bool m_translationY_Exist;
	int m_translationY_Default;
	float m_rotation;
	bool m_rotation_Exist;
	float m_rotation_Default;
	float m_scaleX;
	bool m_scaleX_Exist;
	float m_scaleX_Default;
	float m_scaleY;
	bool m_scaleY_Exist;
	float m_scaleY_Default;


// DefinionChoice
	Mp7JrsMediaLocatorPtr m_OverlaidMediaLocator;
	Mp7JrsReferencePtr m_OverlaidMediaRef;
// End DefinionChoice
	Mp7JrsRegionLocatorPtr m_MediaRegionLocator;
	bool m_MediaRegionLocator_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2135MP7JRSINKMEDIAINFORMATIONTYPE_OVERLAIDMEDIA_LOCALTYPE_H

