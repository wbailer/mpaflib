/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _4057MP7JRSWORDFORMTYPE_H
#define _4057MP7JRSWORDFORMTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsWordFormTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtInclude.h


class IMp7JrsWordFormType;
typedef Dc1Ptr< IMp7JrsWordFormType> Mp7JrsWordFormPtr;
class IMp7JrsWordFormType_terms_CollectionType;
typedef Dc1Ptr< IMp7JrsWordFormType_terms_CollectionType > Mp7JrsWordFormType_terms_CollectionPtr;
class IMp7JrsWordFormType_type_CollectionType;
typedef Dc1Ptr< IMp7JrsWordFormType_type_CollectionType > Mp7JrsWordFormType_type_CollectionPtr;

/** 
 * Generated interface IMp7JrsWordFormType for class Mp7JrsWordFormType<br>
 * Located at: mpeg7-v4.xsd, line 4695<br>
 * Classified: Class<br>
 * Derived from: string (String)<br>
 */
class MP7JRS_EXPORT IMp7JrsWordFormType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsWordFormType();
	virtual ~IMp7JrsWordFormType();
	/**
	 * Sets the content of Mp7JrsWordFormType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of Mp7JrsWordFormType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get terms attribute.
	 * @return Mp7JrsWordFormType_terms_CollectionPtr
	 */
	virtual Mp7JrsWordFormType_terms_CollectionPtr Getterms() const = 0;

	/**
	 * Get terms attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existterms() const = 0;

	/**
	 * Set terms attribute.
	 * @param item const Mp7JrsWordFormType_terms_CollectionPtr &
	 */
	virtual void Setterms(const Mp7JrsWordFormType_terms_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate terms attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateterms() = 0;

	/**
	 * Get id attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get equal attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getequal() const = 0;

	/**
	 * Get equal attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const = 0;

	/**
	 * Set equal attribute.
	 * @param item XMLCh *
	 */
	virtual void Setequal(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate equal attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal() = 0;

	/**
	 * Get type attribute.
	 * @return Mp7JrsWordFormType_type_CollectionPtr
	 */
	virtual Mp7JrsWordFormType_type_CollectionPtr Gettype() const = 0;

	/**
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const = 0;

	/**
	 * Set type attribute.
	 * @param item const Mp7JrsWordFormType_type_CollectionPtr &
	 */
	virtual void Settype(const Mp7JrsWordFormType_type_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype() = 0;

	/**
	 * Get baseForm attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetbaseForm() const = 0;

	/**
	 * Get baseForm attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbaseForm() const = 0;

	/**
	 * Set baseForm attribute.
	 * @param item XMLCh *
	 */
	virtual void SetbaseForm(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate baseForm attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebaseForm() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of WordFormType.
	 * Currently just supports rudimentary XPath expressions as WordFormType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsWordFormType<br>
 * Located at: mpeg7-v4.xsd, line 4695<br>
 * Classified: Class<br>
 * Derived from: string (String)<br>
 * Defined in mpeg7-v4.xsd, line 4695.<br>
 */
class DC1_EXPORT Mp7JrsWordFormType :
		public IMp7JrsWordFormType
{
	friend class Mp7JrsWordFormTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of Mp7JrsWordFormType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of Mp7JrsWordFormType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get terms attribute.
	 * @return Mp7JrsWordFormType_terms_CollectionPtr
	 */
	virtual Mp7JrsWordFormType_terms_CollectionPtr Getterms() const;

	/*
	 * Get terms attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existterms() const;

	/*
	 * Set terms attribute.
	 * @param item const Mp7JrsWordFormType_terms_CollectionPtr &
	 */
	virtual void Setterms(const Mp7JrsWordFormType_terms_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate terms attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateterms();

	/*
	 * Get id attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get equal attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getequal() const;

	/*
	 * Get equal attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const;

	/*
	 * Set equal attribute.
	 * @param item XMLCh *
	 */
	virtual void Setequal(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate equal attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal();

	/*
	 * Get type attribute.
	 * @return Mp7JrsWordFormType_type_CollectionPtr
	 */
	virtual Mp7JrsWordFormType_type_CollectionPtr Gettype() const;

	/*
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
	 * @param item const Mp7JrsWordFormType_type_CollectionPtr &
	 */
	virtual void Settype(const Mp7JrsWordFormType_type_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

	/*
	 * Get baseForm attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetbaseForm() const;

	/*
	 * Get baseForm attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbaseForm() const;

	/*
	 * Set baseForm attribute.
	 * @param item XMLCh *
	 */
	virtual void SetbaseForm(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate baseForm attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebaseForm();

	/*
	 * Gets or creates Dc1NodePtr child elements of WordFormType.
	 * Currently just supports rudimentary XPath expressions as WordFormType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsWordFormType();

protected:
	Mp7JrsWordFormType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsWordFormType_terms_CollectionPtr m_terms;
	bool m_terms_Exist;
	XMLCh * m_id;
	bool m_id_Exist;
	XMLCh * m_equal;
	bool m_equal_Exist;
	Mp7JrsWordFormType_type_CollectionPtr m_type;
	bool m_type_Exist;
	XMLCh * m_baseForm;
	bool m_baseForm_Exist;

	// Base String
	XMLCh * m_Base;


// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _4057MP7JRSWORDFORMTYPE_H

