/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2237MP7JRSLINEARPERCEPTUALATTRIBUTETYPE_H
#define _2237MP7JRSLINEARPERCEPTUALATTRIBUTETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsLinearPerceptualAttributeTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtInclude.h


class IMp7JrsAudioDType;
typedef Dc1Ptr< IMp7JrsAudioDType > Mp7JrsAudioDPtr;
class IMp7JrsLinearPerceptualAttributeType;
typedef Dc1Ptr< IMp7JrsLinearPerceptualAttributeType> Mp7JrsLinearPerceptualAttributePtr;
class IMp7JrsPerceptualEnergyType;
typedef Dc1Ptr< IMp7JrsPerceptualEnergyType > Mp7JrsPerceptualEnergyPtr;
class IMp7JrsPerceptualValenceType;
typedef Dc1Ptr< IMp7JrsPerceptualValenceType > Mp7JrsPerceptualValencePtr;
class IMp7JrsPerceptualBeatType;
typedef Dc1Ptr< IMp7JrsPerceptualBeatType > Mp7JrsPerceptualBeatPtr;
class IMp7JrsPerceptualTempoType;
typedef Dc1Ptr< IMp7JrsPerceptualTempoType > Mp7JrsPerceptualTempoPtr;
#include "Mp7JrsAudioDType.h"
class IMp7JrsintegerVector;
typedef Dc1Ptr< IMp7JrsintegerVector > Mp7JrsintegerVectorPtr;
#include "Mp7JrsDType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsLinearPerceptualAttributeType for class Mp7JrsLinearPerceptualAttributeType<br>
 * Located at: mpeg7-v4.xsd, line 3322<br>
 * Classified: Class<br>
 * Derived from: AudioDType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsLinearPerceptualAttributeType 
 :
		public IMp7JrsAudioDType
{
public:
	// TODO: make these protected?
	IMp7JrsLinearPerceptualAttributeType();
	virtual ~IMp7JrsLinearPerceptualAttributeType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get PerceptualEnergy element.
	 * @return Mp7JrsPerceptualEnergyPtr
	 */
	virtual Mp7JrsPerceptualEnergyPtr GetPerceptualEnergy() const = 0;

	virtual bool IsValidPerceptualEnergy() const = 0;

	/**
	 * Set PerceptualEnergy element.
	 * @param item const Mp7JrsPerceptualEnergyPtr &
	 */
	virtual void SetPerceptualEnergy(const Mp7JrsPerceptualEnergyPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PerceptualEnergy element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualEnergy() = 0;

	/**
	 * Get PerceptualValence element.
	 * @return Mp7JrsPerceptualValencePtr
	 */
	virtual Mp7JrsPerceptualValencePtr GetPerceptualValence() const = 0;

	virtual bool IsValidPerceptualValence() const = 0;

	/**
	 * Set PerceptualValence element.
	 * @param item const Mp7JrsPerceptualValencePtr &
	 */
	virtual void SetPerceptualValence(const Mp7JrsPerceptualValencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PerceptualValence element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualValence() = 0;

	/**
	 * Get PerceptualBeat element.
	 * @return Mp7JrsPerceptualBeatPtr
	 */
	virtual Mp7JrsPerceptualBeatPtr GetPerceptualBeat() const = 0;

	virtual bool IsValidPerceptualBeat() const = 0;

	/**
	 * Set PerceptualBeat element.
	 * @param item const Mp7JrsPerceptualBeatPtr &
	 */
	virtual void SetPerceptualBeat(const Mp7JrsPerceptualBeatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PerceptualBeat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualBeat() = 0;

	/**
	 * Get PerceptualTempo element.
	 * @return Mp7JrsPerceptualTempoPtr
	 */
	virtual Mp7JrsPerceptualTempoPtr GetPerceptualTempo() const = 0;

	virtual bool IsValidPerceptualTempo() const = 0;

	/**
	 * Set PerceptualTempo element.
	 * @param item const Mp7JrsPerceptualTempoPtr &
	 */
	virtual void SetPerceptualTempo(const Mp7JrsPerceptualTempoPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PerceptualTempo element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualTempo() = 0;

		//@}
	/**
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const = 0;

	/**
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const = 0;

	/**
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of LinearPerceptualAttributeType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>PerceptualEnergy</li>
	 * <li>PerceptualValence</li>
	 * <li>PerceptualBeat</li>
	 * <li>PerceptualTempo</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsLinearPerceptualAttributeType<br>
 * Located at: mpeg7-v4.xsd, line 3322<br>
 * Classified: Class<br>
 * Derived from: AudioDType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 3322.<br>
 */
class DC1_EXPORT Mp7JrsLinearPerceptualAttributeType :
		public IMp7JrsLinearPerceptualAttributeType
{
	friend class Mp7JrsLinearPerceptualAttributeTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsLinearPerceptualAttributeType
	 
	 * @return Dc1Ptr< Mp7JrsAudioDType >
	 */
	virtual Dc1Ptr< Mp7JrsAudioDType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get PerceptualEnergy element.
	 * @return Mp7JrsPerceptualEnergyPtr
	 */
	virtual Mp7JrsPerceptualEnergyPtr GetPerceptualEnergy() const;

	virtual bool IsValidPerceptualEnergy() const;

	/*
	 * Set PerceptualEnergy element.
	 * @param item const Mp7JrsPerceptualEnergyPtr &
	 */
	virtual void SetPerceptualEnergy(const Mp7JrsPerceptualEnergyPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PerceptualEnergy element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualEnergy();

	/*
	 * Get PerceptualValence element.
	 * @return Mp7JrsPerceptualValencePtr
	 */
	virtual Mp7JrsPerceptualValencePtr GetPerceptualValence() const;

	virtual bool IsValidPerceptualValence() const;

	/*
	 * Set PerceptualValence element.
	 * @param item const Mp7JrsPerceptualValencePtr &
	 */
	virtual void SetPerceptualValence(const Mp7JrsPerceptualValencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PerceptualValence element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualValence();

	/*
	 * Get PerceptualBeat element.
	 * @return Mp7JrsPerceptualBeatPtr
	 */
	virtual Mp7JrsPerceptualBeatPtr GetPerceptualBeat() const;

	virtual bool IsValidPerceptualBeat() const;

	/*
	 * Set PerceptualBeat element.
	 * @param item const Mp7JrsPerceptualBeatPtr &
	 */
	virtual void SetPerceptualBeat(const Mp7JrsPerceptualBeatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PerceptualBeat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualBeat();

	/*
	 * Get PerceptualTempo element.
	 * @return Mp7JrsPerceptualTempoPtr
	 */
	virtual Mp7JrsPerceptualTempoPtr GetPerceptualTempo() const;

	virtual bool IsValidPerceptualTempo() const;

	/*
	 * Set PerceptualTempo element.
	 * @param item const Mp7JrsPerceptualTempoPtr &
	 */
	virtual void SetPerceptualTempo(const Mp7JrsPerceptualTempoPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PerceptualTempo element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptualTempo();

		//@}
	/*
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const;

	/*
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const;

	/*
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels();

	/*
	 * Gets or creates Dc1NodePtr child elements of LinearPerceptualAttributeType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>PerceptualEnergy</li>
	 * <li>PerceptualValence</li>
	 * <li>PerceptualBeat</li>
	 * <li>PerceptualTempo</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsLinearPerceptualAttributeType();

protected:
	Mp7JrsLinearPerceptualAttributeType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsAudioDType > m_Base;

	Mp7JrsPerceptualEnergyPtr m_PerceptualEnergy;
	bool m_PerceptualEnergy_Exist; // For optional elements 
	Mp7JrsPerceptualValencePtr m_PerceptualValence;
	bool m_PerceptualValence_Exist; // For optional elements 
	Mp7JrsPerceptualBeatPtr m_PerceptualBeat;
	bool m_PerceptualBeat_Exist; // For optional elements 
	Mp7JrsPerceptualTempoPtr m_PerceptualTempo;
	bool m_PerceptualTempo_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2237MP7JRSLINEARPERCEPTUALATTRIBUTETYPE_H

