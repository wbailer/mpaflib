/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2821MP7JRSPERSONNAMETYPE_LOCALTYPE_H
#define _2821MP7JRSPERSONNAMETYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsPersonNameType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtInclude.h


class IMp7JrsPersonNameType_LocalType;
typedef Dc1Ptr< IMp7JrsPersonNameType_LocalType> Mp7JrsPersonNameType_LocalPtr;
class IMp7JrsNameComponentType;
typedef Dc1Ptr< IMp7JrsNameComponentType > Mp7JrsNameComponentPtr;

/** 
 * Generated interface IMp7JrsPersonNameType_LocalType for class Mp7JrsPersonNameType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsPersonNameType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsPersonNameType_LocalType();
	virtual ~IMp7JrsPersonNameType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get GivenName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetGivenName() const = 0;

	/**
	 * Set GivenName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	// Mandatory			
	virtual void SetGivenName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get LinkingName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetLinkingName() const = 0;

	virtual bool IsValidLinkingName() const = 0;

	/**
	 * Set LinkingName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetLinkingName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate LinkingName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLinkingName() = 0;

	/**
	 * Get FamilyName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetFamilyName() const = 0;

	virtual bool IsValidFamilyName() const = 0;

	/**
	 * Set FamilyName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetFamilyName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate FamilyName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFamilyName() = 0;

	/**
	 * Get Title element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetTitle() const = 0;

	virtual bool IsValidTitle() const = 0;

	/**
	 * Set Title element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetTitle(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Title element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTitle() = 0;

	/**
	 * Get Salutation element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetSalutation() const = 0;

	virtual bool IsValidSalutation() const = 0;

	/**
	 * Set Salutation element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetSalutation(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Salutation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSalutation() = 0;

	/**
	 * Get Numeration element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetNumeration() const = 0;

	virtual bool IsValidNumeration() const = 0;

	/**
	 * Set Numeration element.
	 * @param item XMLCh *
	 */
	virtual void SetNumeration(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Numeration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumeration() = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsPersonNameType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 5282.<br>
 */
class DC1_EXPORT Mp7JrsPersonNameType_LocalType :
		public IMp7JrsPersonNameType_LocalType
{
	friend class Mp7JrsPersonNameType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get GivenName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetGivenName() const;

	/*
	 * Set GivenName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	// Mandatory			
	virtual void SetGivenName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get LinkingName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetLinkingName() const;

	virtual bool IsValidLinkingName() const;

	/*
	 * Set LinkingName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetLinkingName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate LinkingName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLinkingName();

	/*
	 * Get FamilyName element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetFamilyName() const;

	virtual bool IsValidFamilyName() const;

	/*
	 * Set FamilyName element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetFamilyName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate FamilyName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFamilyName();

	/*
	 * Get Title element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetTitle() const;

	virtual bool IsValidTitle() const;

	/*
	 * Set Title element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetTitle(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Title element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTitle();

	/*
	 * Get Salutation element.
	 * @return Mp7JrsNameComponentPtr
	 */
	virtual Mp7JrsNameComponentPtr GetSalutation() const;

	virtual bool IsValidSalutation() const;

	/*
	 * Set Salutation element.
	 * @param item const Mp7JrsNameComponentPtr &
	 */
	virtual void SetSalutation(const Mp7JrsNameComponentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Salutation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSalutation();

	/*
	 * Get Numeration element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetNumeration() const;

	virtual bool IsValidNumeration() const;

	/*
	 * Set Numeration element.
	 * @param item XMLCh *
	 */
	virtual void SetNumeration(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Numeration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumeration();

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsPersonNameType_LocalType();

protected:
	Mp7JrsPersonNameType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsNameComponentPtr m_GivenName;
	Mp7JrsNameComponentPtr m_LinkingName;
	bool m_LinkingName_Exist; // For optional elements 
	Mp7JrsNameComponentPtr m_FamilyName;
	bool m_FamilyName_Exist; // For optional elements 
	Mp7JrsNameComponentPtr m_Title;
	bool m_Title_Exist; // For optional elements 
	Mp7JrsNameComponentPtr m_Salutation;
	bool m_Salutation_Exist; // For optional elements 
	XMLCh * m_Numeration;
	bool m_Numeration_Exist; // For optional elements 
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2821MP7JRSPERSONNAMETYPE_LOCALTYPE_H

