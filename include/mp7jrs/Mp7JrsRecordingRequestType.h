/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3007MP7JRSRECORDINGREQUESTTYPE_H
#define _3007MP7JRSRECORDINGREQUESTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsRecordingRequestTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtInclude.h


class IMp7JrsRecordingRequestType;
typedef Dc1Ptr< IMp7JrsRecordingRequestType> Mp7JrsRecordingRequestPtr;
class IMp7JrsUniqueIDType;
typedef Dc1Ptr< IMp7JrsUniqueIDType > Mp7JrsUniqueIDPtr;
class IMp7JrsTextualType;
typedef Dc1Ptr< IMp7JrsTextualType > Mp7JrsTextualPtr;
class IMp7JrsRecordingRequestType_RecordingPeriod_LocalType;
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingPeriod_LocalType > Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr;
class IMp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType;
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType > Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr;
class IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType;
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType > Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr;
class IMp7JrsRecordingRequestType_RecordAssociatedData_LocalType;
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordAssociatedData_LocalType > Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr;

/** 
 * Generated interface IMp7JrsRecordingRequestType for class Mp7JrsRecordingRequestType<br>
 * Located at: mpeg7-v4.xsd, line 10174<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsRecordingRequestType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsRecordingRequestType();
	virtual ~IMp7JrsRecordingRequestType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get ContentIdentifier element.
	 * @return Mp7JrsUniqueIDPtr
	 */
	virtual Mp7JrsUniqueIDPtr GetContentIdentifier() const = 0;

	/**
	 * Set ContentIdentifier element.
	 * @param item const Mp7JrsUniqueIDPtr &
	 */
	// Mandatory			
	virtual void SetContentIdentifier(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get DisseminationSource element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetDisseminationSource() const = 0;

	virtual bool IsValidDisseminationSource() const = 0;

	/**
	 * Set DisseminationSource element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetDisseminationSource(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DisseminationSource element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDisseminationSource() = 0;

	/**
	 * Get Title element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetTitle() const = 0;

	virtual bool IsValidTitle() const = 0;

	/**
	 * Set Title element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetTitle(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Title element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTitle() = 0;

	/**
	 * Get RecordingPeriod element.
	 * @return Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr GetRecordingPeriod() const = 0;

	/**
	 * Set RecordingPeriod element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr &
	 */
	// Mandatory			
	virtual void SetRecordingPeriod(const Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get RecordingLocationPreferences element.
	 * @return Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr GetRecordingLocationPreferences() const = 0;

	/**
	 * Set RecordingLocationPreferences element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRecordingLocationPreferences(const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get RecordingSourceTypePreferences element.
	 * @return Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr GetRecordingSourceTypePreferences() const = 0;

	/**
	 * Set RecordingSourceTypePreferences element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRecordingSourceTypePreferences(const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get RecordAssociatedData element.
	 * @return Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr GetRecordAssociatedData() const = 0;

	virtual bool IsValidRecordAssociatedData() const = 0;

	/**
	 * Set RecordAssociatedData element.
	 * @param item const Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr &
	 */
	virtual void SetRecordAssociatedData(const Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate RecordAssociatedData element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRecordAssociatedData() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of RecordingRequestType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>ContentIdentifier</li>
	 * <li>DisseminationSource</li>
	 * <li>Title</li>
	 * <li>RecordingPeriod</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RecordingLocationPreferences</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RecordingSourceTypePreferences</li>
	 * <li>RecordAssociatedData</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsRecordingRequestType<br>
 * Located at: mpeg7-v4.xsd, line 10174<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 10174.<br>
 */
class DC1_EXPORT Mp7JrsRecordingRequestType :
		public IMp7JrsRecordingRequestType
{
	friend class Mp7JrsRecordingRequestTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get ContentIdentifier element.
	 * @return Mp7JrsUniqueIDPtr
	 */
	virtual Mp7JrsUniqueIDPtr GetContentIdentifier() const;

	/*
	 * Set ContentIdentifier element.
	 * @param item const Mp7JrsUniqueIDPtr &
	 */
	// Mandatory			
	virtual void SetContentIdentifier(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get DisseminationSource element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetDisseminationSource() const;

	virtual bool IsValidDisseminationSource() const;

	/*
	 * Set DisseminationSource element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetDisseminationSource(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DisseminationSource element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDisseminationSource();

	/*
	 * Get Title element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetTitle() const;

	virtual bool IsValidTitle() const;

	/*
	 * Set Title element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetTitle(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Title element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTitle();

	/*
	 * Get RecordingPeriod element.
	 * @return Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr GetRecordingPeriod() const;

	/*
	 * Set RecordingPeriod element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr &
	 */
	// Mandatory			
	virtual void SetRecordingPeriod(const Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get RecordingLocationPreferences element.
	 * @return Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr GetRecordingLocationPreferences() const;

	/*
	 * Set RecordingLocationPreferences element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRecordingLocationPreferences(const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get RecordingSourceTypePreferences element.
	 * @return Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr GetRecordingSourceTypePreferences() const;

	/*
	 * Set RecordingSourceTypePreferences element.
	 * @param item const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRecordingSourceTypePreferences(const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get RecordAssociatedData element.
	 * @return Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr
	 */
	virtual Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr GetRecordAssociatedData() const;

	virtual bool IsValidRecordAssociatedData() const;

	/*
	 * Set RecordAssociatedData element.
	 * @param item const Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr &
	 */
	virtual void SetRecordAssociatedData(const Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate RecordAssociatedData element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRecordAssociatedData();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of RecordingRequestType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>ContentIdentifier</li>
	 * <li>DisseminationSource</li>
	 * <li>Title</li>
	 * <li>RecordingPeriod</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RecordingLocationPreferences</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RecordingSourceTypePreferences</li>
	 * <li>RecordAssociatedData</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsRecordingRequestType();

protected:
	Mp7JrsRecordingRequestType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	Mp7JrsUniqueIDPtr m_ContentIdentifier;
	Mp7JrsTextualPtr m_DisseminationSource;
	bool m_DisseminationSource_Exist; // For optional elements 
	Mp7JrsTextualPtr m_Title;
	bool m_Title_Exist; // For optional elements 
	Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr m_RecordingPeriod;
	Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr m_RecordingLocationPreferences;
	Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr m_RecordingSourceTypePreferences;
	Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr m_RecordAssociatedData;
	bool m_RecordAssociatedData_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3007MP7JRSRECORDINGREQUESTTYPE_H

