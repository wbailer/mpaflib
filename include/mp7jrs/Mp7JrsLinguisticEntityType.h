/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2245MP7JRSLINGUISTICENTITYTYPE_H
#define _2245MP7JRSLINGUISTICENTITYTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsLinguisticEntityTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtInclude.h


class IMp7JrsDSType;
typedef Dc1Ptr< IMp7JrsDSType > Mp7JrsDSPtr;
class IMp7JrsLinguisticEntityType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType> Mp7JrsLinguisticEntityPtr;
class IMp7JrsLinguisticEntityType_start_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_start_LocalType > Mp7JrsLinguisticEntityType_start_LocalPtr;
class IMp7JrsLinguisticEntityType_length_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_length_LocalType > Mp7JrsLinguisticEntityType_length_LocalPtr;
class IMp7JrstermReferenceType;
typedef Dc1Ptr< IMp7JrstermReferenceType > Mp7JrstermReferencePtr;
class IMp7JrstermReferenceListType;
typedef Dc1Ptr< IMp7JrstermReferenceListType > Mp7JrstermReferenceListPtr;
class IMp7JrsLinguisticEntityType_edit_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_edit_LocalType > Mp7JrsLinguisticEntityType_edit_LocalPtr;
class IMp7JrsLinguisticEntityType_MediaLocator_CollectionType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_MediaLocator_CollectionType > Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr;
class IMp7JrsLinguisticEntityType_Relation_CollectionType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_Relation_CollectionType > Mp7JrsLinguisticEntityType_Relation_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsLinguisticEntityType for class Mp7JrsLinguisticEntityType<br>
 * Located at: mpeg7-v4.xsd, line 5707<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 * Abstract class <br>
 */
class MP7JRS_EXPORT IMp7JrsLinguisticEntityType 
 :
		public IMp7JrsDSType
{
public:
	// TODO: make these protected?
	IMp7JrsLinguisticEntityType();
	virtual ~IMp7JrsLinguisticEntityType();
	/**
	 * Get lang attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

	/**
	 * Get start attribute.
	 * @return Mp7JrsLinguisticEntityType_start_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_start_LocalPtr Getstart() const = 0;

	/**
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const = 0;

	/**
	 * Set start attribute.
	 * @param item const Mp7JrsLinguisticEntityType_start_LocalPtr &
	 */
	virtual void Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get length attribute.
	 * @return Mp7JrsLinguisticEntityType_length_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_length_LocalPtr Getlength() const = 0;

	/**
	 * Get length attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const = 0;

	/**
	 * Set length attribute.
	 * @param item const Mp7JrsLinguisticEntityType_length_LocalPtr &
	 */
	virtual void Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate length attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength() = 0;

	/**
	 * Get type attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Gettype() const = 0;

	/**
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const = 0;

	/**
	 * Set type attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype() = 0;

	/**
	 * Get depend attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getdepend() const = 0;

	/**
	 * Get depend attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdepend() const = 0;

	/**
	 * Set depend attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate depend attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepend() = 0;

	/**
	 * Get equal attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getequal() const = 0;

	/**
	 * Get equal attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const = 0;

	/**
	 * Set equal attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate equal attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal() = 0;

	/**
	 * Get semantics attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getsemantics() const = 0;

	/**
	 * Get semantics attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsemantics() const = 0;

	/**
	 * Set semantics attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate semantics attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesemantics() = 0;

	/**
	 * Get compoundSemantics attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetcompoundSemantics() const = 0;

	/**
	 * Get compoundSemantics attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcompoundSemantics() const = 0;

	/**
	 * Set compoundSemantics attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate compoundSemantics attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecompoundSemantics() = 0;

	/**
	 * Get operator attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getoperator() const = 0;

	/**
	 * Get operator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existoperator() const = 0;

	/**
	 * Set operator attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate operator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoperator() = 0;

	/**
	 * Get copy attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getcopy() const = 0;

	/**
	 * Get copy attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcopy() const = 0;

	/**
	 * Set copy attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate copy attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecopy() = 0;

	/**
	 * Get noCopy attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetnoCopy() const = 0;

	/**
	 * Get noCopy attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoCopy() const = 0;

	/**
	 * Set noCopy attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noCopy attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoCopy() = 0;

	/**
	 * Get substitute attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getsubstitute() const = 0;

	/**
	 * Get substitute attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsubstitute() const = 0;

	/**
	 * Set substitute attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate substitute attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesubstitute() = 0;

	/**
	 * Get inScope attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr GetinScope() const = 0;

	/**
	 * Get inScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistinScope() const = 0;

	/**
	 * Set inScope attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate inScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateinScope() = 0;

	/**
	 * Get edit attribute.
	 * @return Mp7JrsLinguisticEntityType_edit_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_edit_LocalPtr Getedit() const = 0;

	/**
	 * Get edit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existedit() const = 0;

	/**
	 * Set edit attribute.
	 * @param item const Mp7JrsLinguisticEntityType_edit_LocalPtr &
	 */
	virtual void Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate edit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateedit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get MediaLocator element.
	 * @return Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr GetMediaLocator() const = 0;

	/**
	 * Set MediaLocator element.
	 * @param item const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Relation element.
	 * @return Mp7JrsLinguisticEntityType_Relation_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_Relation_CollectionPtr GetRelation() const = 0;

	/**
	 * Set Relation element.
	 * @param item const Mp7JrsLinguisticEntityType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 * @endif
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 * @endif
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidateid() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 * @endif
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 * @endif
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 * @endif
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 * @endif
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of LinguisticEntityType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaLocator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsLinguisticEntityType<br>
 * Located at: mpeg7-v4.xsd, line 5707<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 * Abstract class <br>
 * Defined in mpeg7-v4.xsd, line 5707.<br>
 */
class DC1_EXPORT Mp7JrsLinguisticEntityType :
		public IMp7JrsLinguisticEntityType
{
	friend class Mp7JrsLinguisticEntityTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsLinguisticEntityType
	 
	 * @return Dc1Ptr< Mp7JrsDSType >
	 */
	virtual Dc1Ptr< Mp7JrsDSType > GetBase() const;
	/*
	 * Get lang attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

	/*
	 * Get start attribute.
	 * @return Mp7JrsLinguisticEntityType_start_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_start_LocalPtr Getstart() const;

	/*
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const;

	/*
	 * Set start attribute.
	 * @param item const Mp7JrsLinguisticEntityType_start_LocalPtr &
	 */
	virtual void Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get length attribute.
	 * @return Mp7JrsLinguisticEntityType_length_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_length_LocalPtr Getlength() const;

	/*
	 * Get length attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const;

	/*
	 * Set length attribute.
	 * @param item const Mp7JrsLinguisticEntityType_length_LocalPtr &
	 */
	virtual void Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate length attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength();

	/*
	 * Get type attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Gettype() const;

	/*
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

	/*
	 * Get depend attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getdepend() const;

	/*
	 * Get depend attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdepend() const;

	/*
	 * Set depend attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate depend attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepend();

	/*
	 * Get equal attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getequal() const;

	/*
	 * Get equal attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const;

	/*
	 * Set equal attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate equal attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal();

	/*
	 * Get semantics attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getsemantics() const;

	/*
	 * Get semantics attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsemantics() const;

	/*
	 * Set semantics attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate semantics attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesemantics();

	/*
	 * Get compoundSemantics attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetcompoundSemantics() const;

	/*
	 * Get compoundSemantics attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcompoundSemantics() const;

	/*
	 * Set compoundSemantics attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate compoundSemantics attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecompoundSemantics();

	/*
	 * Get operator attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getoperator() const;

	/*
	 * Get operator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existoperator() const;

	/*
	 * Set operator attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate operator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoperator();

	/*
	 * Get copy attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getcopy() const;

	/*
	 * Get copy attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcopy() const;

	/*
	 * Set copy attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate copy attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecopy();

	/*
	 * Get noCopy attribute.
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetnoCopy() const;

	/*
	 * Get noCopy attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoCopy() const;

	/*
	 * Set noCopy attribute.
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noCopy attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoCopy();

	/*
	 * Get substitute attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getsubstitute() const;

	/*
	 * Get substitute attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsubstitute() const;

	/*
	 * Set substitute attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate substitute attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesubstitute();

	/*
	 * Get inScope attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr GetinScope() const;

	/*
	 * Get inScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistinScope() const;

	/*
	 * Set inScope attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate inScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateinScope();

	/*
	 * Get edit attribute.
	 * @return Mp7JrsLinguisticEntityType_edit_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_edit_LocalPtr Getedit() const;

	/*
	 * Get edit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existedit() const;

	/*
	 * Set edit attribute.
	 * @param item const Mp7JrsLinguisticEntityType_edit_LocalPtr &
	 */
	virtual void Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate edit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateedit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get MediaLocator element.
	 * @return Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
	 * @param item const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Relation element.
	 * @return Mp7JrsLinguisticEntityType_Relation_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_Relation_CollectionPtr GetRelation() const;

	/*
	 * Set Relation element.
	 * @param item const Mp7JrsLinguisticEntityType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of LinguisticEntityType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaLocator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsLinguisticEntityType();

protected:
	Mp7JrsLinguisticEntityType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_lang;
	bool m_lang_Exist;
	Mp7JrsLinguisticEntityType_start_LocalPtr m_start;
	bool m_start_Exist;
	Mp7JrsLinguisticEntityType_length_LocalPtr m_length;
	bool m_length_Exist;
	Mp7JrstermReferencePtr m_type;
	bool m_type_Exist;
	Mp7JrstermReferencePtr m_depend;
	bool m_depend_Exist;
	Mp7JrstermReferenceListPtr m_equal;
	bool m_equal_Exist;
	Mp7JrstermReferenceListPtr m_semantics;
	bool m_semantics_Exist;
	Mp7JrstermReferenceListPtr m_compoundSemantics;
	bool m_compoundSemantics_Exist;
	Mp7JrstermReferenceListPtr m_operator;
	bool m_operator_Exist;
	Mp7JrstermReferenceListPtr m_copy;
	bool m_copy_Exist;
	Mp7JrstermReferenceListPtr m_noCopy;
	bool m_noCopy_Exist;
	Mp7JrstermReferencePtr m_substitute;
	bool m_substitute_Exist;
	Mp7JrstermReferencePtr m_inScope;
	bool m_inScope_Exist;
	Mp7JrsLinguisticEntityType_edit_LocalPtr m_edit;
	bool m_edit_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsDSType > m_Base;

	Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr m_MediaLocator;
	Mp7JrsLinguisticEntityType_Relation_CollectionPtr m_Relation;

// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2245MP7JRSLINGUISTICENTITYTYPE_H

