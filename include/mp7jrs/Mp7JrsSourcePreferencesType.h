/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3297MP7JRSSOURCEPREFERENCESTYPE_H
#define _3297MP7JRSSOURCEPREFERENCESTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSourcePreferencesTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtInclude.h


class IMp7JrsDSType;
typedef Dc1Ptr< IMp7JrsDSType > Mp7JrsDSPtr;
class IMp7JrsSourcePreferencesType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType> Mp7JrsSourcePreferencesPtr;
class IMp7JrspreferenceValueType;
typedef Dc1Ptr< IMp7JrspreferenceValueType > Mp7JrspreferenceValuePtr;
class IMp7JrsSourcePreferencesType_DisseminationFormat_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationFormat_CollectionType > Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr;
class IMp7JrsSourcePreferencesType_DisseminationSource_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationSource_CollectionType > Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr;
class IMp7JrsSourcePreferencesType_DisseminationLocation_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationLocation_CollectionType > Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr;
class IMp7JrsSourcePreferencesType_DisseminationDate_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationDate_CollectionType > Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr;
class IMp7JrsSourcePreferencesType_Disseminator_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_Disseminator_CollectionType > Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr;
class IMp7JrsSourcePreferencesType_MediaFormat_CollectionType;
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_MediaFormat_CollectionType > Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSourcePreferencesType for class Mp7JrsSourcePreferencesType<br>
 * Located at: mpeg7-v4.xsd, line 10006<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSourcePreferencesType 
 :
		public IMp7JrsDSType
{
public:
	// TODO: make these protected?
	IMp7JrsSourcePreferencesType();
	virtual ~IMp7JrsSourcePreferencesType();
	/**
	 * Get noRepeat attribute.
	 * @return bool
	 */
	virtual bool GetnoRepeat() const = 0;

	/**
	 * Get noRepeat attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoRepeat() const = 0;

	/**
	 * Set noRepeat attribute.
	 * @param item bool
	 */
	virtual void SetnoRepeat(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noRepeat attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoRepeat() = 0;

	/**
	 * Get noEncryption attribute.
	 * @return bool
	 */
	virtual bool GetnoEncryption() const = 0;

	/**
	 * Get noEncryption attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoEncryption() const = 0;

	/**
	 * Set noEncryption attribute.
	 * @param item bool
	 */
	virtual void SetnoEncryption(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noEncryption attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoEncryption() = 0;

	/**
	 * Get noPayPerUse attribute.
	 * @return bool
	 */
	virtual bool GetnoPayPerUse() const = 0;

	/**
	 * Get noPayPerUse attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoPayPerUse() const = 0;

	/**
	 * Set noPayPerUse attribute.
	 * @param item bool
	 */
	virtual void SetnoPayPerUse(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noPayPerUse attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoPayPerUse() = 0;

	/**
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const = 0;

	/**
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const = 0;

	/**
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get DisseminationFormat element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr GetDisseminationFormat() const = 0;

	/**
	 * Set DisseminationFormat element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationFormat(const Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get DisseminationSource element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr GetDisseminationSource() const = 0;

	/**
	 * Set DisseminationSource element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationSource(const Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get DisseminationLocation element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr GetDisseminationLocation() const = 0;

	/**
	 * Set DisseminationLocation element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationLocation(const Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get DisseminationDate element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr GetDisseminationDate() const = 0;

	/**
	 * Set DisseminationDate element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationDate(const Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Disseminator element.
	 * @return Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr GetDisseminator() const = 0;

	/**
	 * Set Disseminator element.
	 * @param item const Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminator(const Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaFormat element.
	 * @return Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr GetMediaFormat() const = 0;

	/**
	 * Set MediaFormat element.
	 * @param item const Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaFormat(const Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SourcePreferencesType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationFormat</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationSource</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationLocation</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationDate</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Disseminator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaFormat</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSourcePreferencesType<br>
 * Located at: mpeg7-v4.xsd, line 10006<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 10006.<br>
 */
class DC1_EXPORT Mp7JrsSourcePreferencesType :
		public IMp7JrsSourcePreferencesType
{
	friend class Mp7JrsSourcePreferencesTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSourcePreferencesType
	 
	 * @return Dc1Ptr< Mp7JrsDSType >
	 */
	virtual Dc1Ptr< Mp7JrsDSType > GetBase() const;
	/*
	 * Get noRepeat attribute.
	 * @return bool
	 */
	virtual bool GetnoRepeat() const;

	/*
	 * Get noRepeat attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoRepeat() const;

	/*
	 * Set noRepeat attribute.
	 * @param item bool
	 */
	virtual void SetnoRepeat(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noRepeat attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoRepeat();

	/*
	 * Get noEncryption attribute.
	 * @return bool
	 */
	virtual bool GetnoEncryption() const;

	/*
	 * Get noEncryption attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoEncryption() const;

	/*
	 * Set noEncryption attribute.
	 * @param item bool
	 */
	virtual void SetnoEncryption(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noEncryption attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoEncryption();

	/*
	 * Get noPayPerUse attribute.
	 * @return bool
	 */
	virtual bool GetnoPayPerUse() const;

	/*
	 * Get noPayPerUse attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoPayPerUse() const;

	/*
	 * Set noPayPerUse attribute.
	 * @param item bool
	 */
	virtual void SetnoPayPerUse(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noPayPerUse attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoPayPerUse();

	/*
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const;

	/*
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const;

	/*
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get DisseminationFormat element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr GetDisseminationFormat() const;

	/*
	 * Set DisseminationFormat element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationFormat(const Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get DisseminationSource element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr GetDisseminationSource() const;

	/*
	 * Set DisseminationSource element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationSource(const Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get DisseminationLocation element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr GetDisseminationLocation() const;

	/*
	 * Set DisseminationLocation element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationLocation(const Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get DisseminationDate element.
	 * @return Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr GetDisseminationDate() const;

	/*
	 * Set DisseminationDate element.
	 * @param item const Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminationDate(const Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Disseminator element.
	 * @return Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr GetDisseminator() const;

	/*
	 * Set Disseminator element.
	 * @param item const Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDisseminator(const Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaFormat element.
	 * @return Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr
	 */
	virtual Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr GetMediaFormat() const;

	/*
	 * Set MediaFormat element.
	 * @param item const Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaFormat(const Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SourcePreferencesType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationFormat</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationSource</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationLocation</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:DisseminationDate</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Disseminator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaFormat</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSourcePreferencesType();

protected:
	Mp7JrsSourcePreferencesType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	bool m_noRepeat;
	bool m_noRepeat_Exist;
	bool m_noEncryption;
	bool m_noEncryption_Exist;
	bool m_noPayPerUse;
	bool m_noPayPerUse_Exist;
	Mp7JrspreferenceValuePtr m_preferenceValue;
	bool m_preferenceValue_Exist;
	Mp7JrspreferenceValuePtr m_preferenceValue_Default;

	// Base Class
	Dc1Ptr< Mp7JrsDSType > m_Base;

	Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr m_DisseminationFormat;
	Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr m_DisseminationSource;
	Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr m_DisseminationLocation;
	Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr m_DisseminationDate;
	Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr m_Disseminator;
	Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr m_MediaFormat;

// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3297MP7JRSSOURCEPREFERENCESTYPE_H

