/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3225MP7JRSSERIESOFVECTORBINARYTYPE_H
#define _3225MP7JRSSERIESOFVECTORBINARYTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSeriesOfVectorBinaryTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtInclude.h


class IMp7JrsSeriesOfVectorType;
typedef Dc1Ptr< IMp7JrsSeriesOfVectorType > Mp7JrsSeriesOfVectorPtr;
class IMp7JrsSeriesOfVectorBinaryType;
typedef Dc1Ptr< IMp7JrsSeriesOfVectorBinaryType> Mp7JrsSeriesOfVectorBinaryPtr;
class IMp7JrsFloatMatrixType;
typedef Dc1Ptr< IMp7JrsFloatMatrixType > Mp7JrsFloatMatrixPtr;
#include "Mp7JrsSeriesOfVectorType.h"
class IMp7JrsfloatVector;
typedef Dc1Ptr< IMp7JrsfloatVector > Mp7JrsfloatVectorPtr;
#include "Mp7JrsScalableSeriesType.h"
class IMp7JrsScalableSeriesType_Scaling_CollectionType;
typedef Dc1Ptr< IMp7JrsScalableSeriesType_Scaling_CollectionType > Mp7JrsScalableSeriesType_Scaling_CollectionPtr;

/** 
 * Generated interface IMp7JrsSeriesOfVectorBinaryType for class Mp7JrsSeriesOfVectorBinaryType<br>
 * Located at: mpeg7-v4.xsd, line 2114<br>
 * Classified: Class<br>
 * Derived from: SeriesOfVectorType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSeriesOfVectorBinaryType 
 :
		public IMp7JrsSeriesOfVectorType
{
public:
	// TODO: make these protected?
	IMp7JrsSeriesOfVectorBinaryType();
	virtual ~IMp7JrsSeriesOfVectorBinaryType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get VarianceScalewise element.
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetVarianceScalewise() const = 0;

	virtual bool IsValidVarianceScalewise() const = 0;

	/**
	 * Set VarianceScalewise element.
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetVarianceScalewise(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate VarianceScalewise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVarianceScalewise() = 0;

		//@}
	/**
	 * Get vectorSize attribute.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return unsigned
	 */
	virtual unsigned GetvectorSize() const = 0;

	/**
	 * Get vectorSize attribute validity information.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvectorSize() const = 0;

	/**
	 * Set vectorSize attribute.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item unsigned
	 */
	virtual void SetvectorSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate vectorSize attribute.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevectorSize() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Raw element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetRaw() const = 0;

	virtual bool IsValidRaw() const = 0;

	/**
	 * Set Raw element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetRaw(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Raw element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRaw() = 0;

	/**
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMin() const = 0;

	virtual bool IsValidMin() const = 0;

	/**
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMin(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMin() = 0;

	/**
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMax() const = 0;

	virtual bool IsValidMax() const = 0;

	/**
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMax(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMax() = 0;

	/**
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMean() const = 0;

	virtual bool IsValidMean() const = 0;

	/**
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMean(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMean() = 0;

	/**
	 * Get Random element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetRandom() const = 0;

	virtual bool IsValidRandom() const = 0;

	/**
	 * Set Random element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetRandom(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Random element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRandom() = 0;

	/**
	 * Get First element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetFirst() const = 0;

	virtual bool IsValidFirst() const = 0;

	/**
	 * Set First element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetFirst(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate First element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFirst() = 0;

	/**
	 * Get Last element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetLast() const = 0;

	virtual bool IsValidLast() const = 0;

	/**
	 * Set Last element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetLast(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Last element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLast() = 0;

	/**
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetVariance() const = 0;

	virtual bool IsValidVariance() const = 0;

	/**
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetVariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVariance() = 0;

	/**
	 * Get LogBase element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return float
	 */
	virtual float GetLogBase() const = 0;

	virtual bool IsValidLogBase() const = 0;

	/**
	 * Set LogBase element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item float
	 */
	virtual void SetLogBase(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate LogBase element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLogBase() = 0;

	/**
	 * Get Covariance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetCovariance() const = 0;

	virtual bool IsValidCovariance() const = 0;

	/**
	 * Set Covariance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetCovariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Covariance element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCovariance() = 0;

	/**
	 * Get VarianceSummed element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetVarianceSummed() const = 0;

	virtual bool IsValidVarianceSummed() const = 0;

	/**
	 * Set VarianceSummed element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetVarianceSummed(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate VarianceSummed element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVarianceSummed() = 0;

	/**
	 * Get MaxSqDist element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetMaxSqDist() const = 0;

	virtual bool IsValidMaxSqDist() const = 0;

	/**
	 * Set MaxSqDist element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetMaxSqDist(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MaxSqDist element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxSqDist() = 0;

	/**
	 * Get Weight element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetWeight() const = 0;

	virtual bool IsValidWeight() const = 0;

	/**
	 * Set Weight element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Weight element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWeight() = 0;

		//@}
	/**
	 * Get totalNumOfSamples attribute.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @return unsigned
	 */
	virtual unsigned GettotalNumOfSamples() const = 0;

	/**
	 * Set totalNumOfSamples attribute.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @param item unsigned
	 */
	// Mandatory
	virtual void SettotalNumOfSamples(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Scaling element.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @return Mp7JrsScalableSeriesType_Scaling_CollectionPtr
	 */
	virtual Mp7JrsScalableSeriesType_Scaling_CollectionPtr GetScaling() const = 0;

	/**
	 * Set Scaling element.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @param item const Mp7JrsScalableSeriesType_Scaling_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SeriesOfVectorBinaryType.
	 * Currently this type contains 14 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Scaling</li>
	 * <li>Raw</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mean</li>
	 * <li>Random</li>
	 * <li>First</li>
	 * <li>Last</li>
	 * <li>Variance</li>
	 * <li>Covariance</li>
	 * <li>VarianceSummed</li>
	 * <li>MaxSqDist</li>
	 * <li>Weight</li>
	 * <li>VarianceScalewise</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSeriesOfVectorBinaryType<br>
 * Located at: mpeg7-v4.xsd, line 2114<br>
 * Classified: Class<br>
 * Derived from: SeriesOfVectorType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 2114.<br>
 */
class DC1_EXPORT Mp7JrsSeriesOfVectorBinaryType :
		public IMp7JrsSeriesOfVectorBinaryType
{
	friend class Mp7JrsSeriesOfVectorBinaryTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSeriesOfVectorBinaryType
	 
	 * @return Dc1Ptr< Mp7JrsSeriesOfVectorType >
	 */
	virtual Dc1Ptr< Mp7JrsSeriesOfVectorType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get VarianceScalewise element.
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetVarianceScalewise() const;

	virtual bool IsValidVarianceScalewise() const;

	/*
	 * Set VarianceScalewise element.
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetVarianceScalewise(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate VarianceScalewise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVarianceScalewise();

		//@}
	/*
	 * Get vectorSize attribute.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return unsigned
	 */
	virtual unsigned GetvectorSize() const;

	/*
	 * Get vectorSize attribute validity information.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvectorSize() const;

	/*
	 * Set vectorSize attribute.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item unsigned
	 */
	virtual void SetvectorSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate vectorSize attribute.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevectorSize();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Raw element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetRaw() const;

	virtual bool IsValidRaw() const;

	/*
	 * Set Raw element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetRaw(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Raw element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRaw();

	/*
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMin() const;

	virtual bool IsValidMin() const;

	/*
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMin(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMin();

	/*
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMax() const;

	virtual bool IsValidMax() const;

	/*
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMax(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMax();

	/*
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetMean() const;

	virtual bool IsValidMean() const;

	/*
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetMean(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMean();

	/*
	 * Get Random element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetRandom() const;

	virtual bool IsValidRandom() const;

	/*
	 * Set Random element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetRandom(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Random element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRandom();

	/*
	 * Get First element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetFirst() const;

	virtual bool IsValidFirst() const;

	/*
	 * Set First element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetFirst(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate First element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFirst();

	/*
	 * Get Last element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetLast() const;

	virtual bool IsValidLast() const;

	/*
	 * Set Last element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetLast(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Last element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLast();

	/*
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetVariance() const;

	virtual bool IsValidVariance() const;

	/*
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetVariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVariance();

	/*
	 * Get LogBase element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return float
	 */
	virtual float GetLogBase() const;

	virtual bool IsValidLogBase() const;

	/*
	 * Set LogBase element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item float
	 */
	virtual void SetLogBase(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate LogBase element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateLogBase();

	/*
	 * Get Covariance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetCovariance() const;

	virtual bool IsValidCovariance() const;

	/*
	 * Set Covariance element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	virtual void SetCovariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Covariance element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCovariance();

	/*
	 * Get VarianceSummed element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetVarianceSummed() const;

	virtual bool IsValidVarianceSummed() const;

	/*
	 * Set VarianceSummed element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetVarianceSummed(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate VarianceSummed element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVarianceSummed();

	/*
	 * Get MaxSqDist element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetMaxSqDist() const;

	virtual bool IsValidMaxSqDist() const;

	/*
	 * Set MaxSqDist element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetMaxSqDist(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MaxSqDist element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxSqDist();

	/*
	 * Get Weight element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @return Mp7JrsfloatVectorPtr
	 */
	virtual Mp7JrsfloatVectorPtr GetWeight() const;

	virtual bool IsValidWeight() const;

	/*
	 * Set Weight element.
<br>
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 * @param item const Mp7JrsfloatVectorPtr &
	 */
	virtual void SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Weight element.
	 * (Inherited from Mp7JrsSeriesOfVectorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWeight();

		//@}
	/*
	 * Get totalNumOfSamples attribute.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @return unsigned
	 */
	virtual unsigned GettotalNumOfSamples() const;

	/*
	 * Set totalNumOfSamples attribute.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @param item unsigned
	 */
	// Mandatory
	virtual void SettotalNumOfSamples(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Scaling element.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @return Mp7JrsScalableSeriesType_Scaling_CollectionPtr
	 */
	virtual Mp7JrsScalableSeriesType_Scaling_CollectionPtr GetScaling() const;

	/*
	 * Set Scaling element.
<br>
	 * (Inherited from Mp7JrsScalableSeriesType)
	 * @param item const Mp7JrsScalableSeriesType_Scaling_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SeriesOfVectorBinaryType.
	 * Currently this type contains 14 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Scaling</li>
	 * <li>Raw</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mean</li>
	 * <li>Random</li>
	 * <li>First</li>
	 * <li>Last</li>
	 * <li>Variance</li>
	 * <li>Covariance</li>
	 * <li>VarianceSummed</li>
	 * <li>MaxSqDist</li>
	 * <li>Weight</li>
	 * <li>VarianceScalewise</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSeriesOfVectorBinaryType();

protected:
	Mp7JrsSeriesOfVectorBinaryType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsSeriesOfVectorType > m_Base;

	Mp7JrsFloatMatrixPtr m_VarianceScalewise;
	bool m_VarianceScalewise_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3225MP7JRSSERIESOFVECTORBINARYTYPE_H

