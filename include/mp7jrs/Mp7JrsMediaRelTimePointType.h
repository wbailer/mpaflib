/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2385MP7JRSMEDIARELTIMEPOINTTYPE_H
#define _2385MP7JRSMEDIARELTIMEPOINTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMediaRelTimePointTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// begin extension included
// file Mp7JrsMediaRelTimePointType_ExtInclude.h

class Mp7JrsmediaTimePointType;
// end extension included


class IMp7JrsMediaRelTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelTimePointType> Mp7JrsMediaRelTimePointPtr;
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
#include "Mp7JrsmediaTimeOffsetType.h"

/** 
 * Generated interface IMp7JrsMediaRelTimePointType for class Mp7JrsMediaRelTimePointType<br>
 * Located at: mpeg7-v4.xsd, line 4441<br>
 * Classified: Class<br>
 * Derived from: mediaTimeOffsetType (Pattern)<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaRelTimePointType 
 :
		public IMp7JrsmediaTimeOffsetType
{
public:
	// TODO: make these protected?
	IMp7JrsMediaRelTimePointType();
	virtual ~IMp7JrsMediaRelTimePointType();
	/**
	 * Get mediaTimeBase attribute.
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get OffsetSign element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetOffsetSign() const = 0;

	/**
	 * Set OffsetSign element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetOffsetSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Days element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetDays() const = 0;

	/**
	 * Set Days element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetDays(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Hours element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetHours() const = 0;

	/**
	 * Set Hours element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Minutes element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetMinutes() const = 0;

	/**
	 * Set Minutes element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Seconds element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetSeconds() const = 0;

	/**
	 * Set Seconds element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetSeconds(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetNumberOfFractions() const = 0;

	/**
	 * Set NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetNumberOfFractions(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Time_Exist element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTime_Exist() const = 0;

	/**
	 * Set Time_Exist element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTime_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetFractionsPerSecond() const = 0;

	/**
	 * Set FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetFractionsPerSecond(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of MediaRelTimePointType.
	 * Currently just supports rudimentary XPath expressions as MediaRelTimePointType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// begin extension included
// file Mp7JrsMediaRelTimePointType_ExtMethodDef.h


public:

	// NOTE: days are ignored in calculations

	/** Get total number of fractions. In V2.8. the return value is 64 bit long long.
	  * @return Get the total number of fractions with respect to the timepoint's number of fractions/second. */
	virtual long long GetTotalNrFractions();

	/** Set total number of fractions.
	  * @param fractions Total number of fractions with respect to the timepoint's number of fractions/second. */
	virtual void SetTotalNrFractions(long long fractions, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get absolute value of relative time point using the currently valid mediaTimeBase. 
	  */
	virtual Mp7JrsmediaTimePointPtr GetAbsMediaTimePoint();

	/** Set absolute value of relative time point using the currently valid mediaTimeBase. 
	  */
	virtual void SetAbsMediaTimePoint(Mp7JrsmediaTimePointPtr tp, Dc1ClientID client = DC1_UNDEFINED_CLIENT);
	
private:

	Mp7JrsmediaTimePointPtr _GetMediaTimeBase();
// end extension included


// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMediaRelTimePointType<br>
 * Located at: mpeg7-v4.xsd, line 4441<br>
 * Classified: Class<br>
 * Derived from: mediaTimeOffsetType (Pattern)<br>
 * Defined in mpeg7-v4.xsd, line 4441.<br>
 */
class DC1_EXPORT Mp7JrsMediaRelTimePointType :
		public IMp7JrsMediaRelTimePointType
{
	friend class Mp7JrsMediaRelTimePointTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsMediaRelTimePointType
	 
	 * @return Dc1Ptr< Mp7JrsmediaTimeOffsetType >
	 */
	virtual Dc1Ptr< Mp7JrsmediaTimeOffsetType > GetBase() const;
	/*
	 * Get mediaTimeBase attribute.
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get OffsetSign element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetOffsetSign() const;

	/*
	 * Set OffsetSign element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetOffsetSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Days element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetDays() const;

	/*
	 * Set Days element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetDays(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Hours element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetHours() const;

	/*
	 * Set Hours element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Minutes element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetMinutes() const;

	/*
	 * Set Minutes element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Seconds element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetSeconds() const;

	/*
	 * Set Seconds element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetSeconds(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetNumberOfFractions() const;

	/*
	 * Set NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetNumberOfFractions(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Time_Exist element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTime_Exist() const;

	/*
	 * Set Time_Exist element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTime_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @return int
	 */
	virtual int GetFractionsPerSecond() const;

	/*
	 * Set FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrsmediaTimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetFractionsPerSecond(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Gets or creates Dc1NodePtr child elements of MediaRelTimePointType.
	 * Currently just supports rudimentary XPath expressions as MediaRelTimePointType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMediaRelTimePointType();

protected:
	Mp7JrsMediaRelTimePointType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsxPathRefPtr m_mediaTimeBase;
	bool m_mediaTimeBase_Exist;

	// Base Pattern
	Dc1Ptr< Mp7JrsmediaTimeOffsetType > m_Base;


// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2385MP7JRSMEDIARELTIMEPOINTTYPE_H

