/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// PatternType_H

#ifndef _3749MP7JRSTIMEPOINTTYPE_H
#define _3749MP7JRSTIMEPOINTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"



#include "Mp7JrstimePointTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"

// begin extension included
// file Mp7JrstimePointType_ExtInclude.h

class Mp7JrsdurationType;
// end extension included


class IMp7JrstimePointType;typedef Dc1Ptr< IMp7JrstimePointType> Mp7JrstimePointPtr;
/**
 * Generated interface IMp7JrstimePointType for Pattern Mp7JrstimePointType
 */
class DC1_EXPORT IMp7JrstimePointType :
	public Dc1Node
{
public:
	virtual int GetTimepointSign() const = 0;

	virtual void SetTimepointSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetYear() const = 0;

	virtual void SetYear(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMonth() const = 0;

	virtual void SetMonth(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetDay() const = 0;

	virtual void SetDay(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetDate_Exist() const = 0;

	virtual void SetDate_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetHours() const = 0;

	virtual void SetHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMinutes() const = 0;

	virtual void SetMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetSeconds() const = 0;

	virtual void SetSeconds(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetNumberOfFractions() const = 0;

	virtual void SetNumberOfFractions(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetTime_Exist() const = 0;

	virtual void SetTime_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetFractionsPerSecond() const = 0;

	virtual void SetFractionsPerSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneSign() const = 0;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneHours() const = 0;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneMinutes() const = 0;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetTimezone_Exist() const = 0;

	virtual void SetTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


// begin extension included
// file Mp7JrstimePointType_ExtMethodDef.h


public:

	// IMPORTANT:
	// Years and months ARE IGNORED now in all of the calculations!!!!

	/** Get total number of fractions.
	  * @return Get the total number of fractions with respect to the timepoint's number of fractions/second. */
	virtual long long GetTotalNrFractions();

	/** Set total number of fractions.
	  * @param fractions Total number of fractions with respect to the timepoint's number of fractions/second. */
	virtual void SetTotalNrFractions(long long fractions, Dc1ClientID client = DC1_UNDEFINED_CLIENT);
	
	/** Add a duration to this timepoint */
	virtual void Add(Mp7JrsdurationPtr duration, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Subtract a timepoint from this one.
	  * @return The duration representing the difference of the two time points. */
	virtual Mp7JrsdurationPtr Subtract(Mp7JrstimePointPtr timePoint, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


// end extension included


};

/*
 * Generated pattern Mp7JrstimePointType<br>
 * Located at: mpeg7-v4.xsd, line 4323<br>
 * Regular expression: <br>
 * (\-?\d+(\-\d{2}(\-\d{2})?)?)?(T\d{2}(:\d{2}(:\d{2}(:\d+)?)?)?)?(F\d+)?((\-|\+)\d{2}:\d{2})?<br>
 */
class DC1_EXPORT Mp7JrstimePointType :
	public IMp7JrstimePointType
{
	friend class Mp7JrstimePointTypeFactory;
	
public: 
	virtual int GetTimepointSign() const;

	virtual void SetTimepointSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetYear() const;

	virtual void SetYear(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMonth() const;

	virtual void SetMonth(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetDay() const;

	virtual void SetDay(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetDate_Exist() const;

	virtual void SetDate_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetHours() const;

	virtual void SetHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMinutes() const;

	virtual void SetMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetSeconds() const;

	virtual void SetSeconds(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetNumberOfFractions() const;

	virtual void SetNumberOfFractions(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetTime_Exist() const;

	virtual void SetTime_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetFractionsPerSecond() const;

	virtual void SetFractionsPerSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneSign() const;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneHours() const;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneMinutes() const;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetTimezone_Exist() const;

	virtual void SetTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	virtual XMLCh * ToText() const;
	virtual bool Parse(const XMLCh * const txt);
	virtual bool ContentFromString(const XMLCh * const txt);

	virtual XMLCh * ContentToString() const;

	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file Mp7JrstimePointType_ExtMyMethodDef.h


	virtual ~Mp7JrstimePointType();

protected:
	Mp7JrstimePointType();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_TimepointSign;
	int m_Year;
	int m_Month;
	int m_Day;
	bool m_Date_Exist;
	int m_Hours;
	int m_Minutes;
	int m_Seconds;
	int m_NumberOfFractions;
	bool m_Time_Exist;
	int m_FractionsPerSecond;
	int m_TimezoneSign;
	int m_TimezoneHours;
	int m_TimezoneMinutes;
	bool m_Timezone_Exist;

// no includefile for extension defined 
// file Mp7JrstimePointType_ExtPropDef.h


protected:
	void Init();
	void Cleanup();
	
protected:
    const XMLCh * const m_nsURI; // For namespace handling
};

#endif // _3749MP7JRSTIMEPOINTTYPE_H

