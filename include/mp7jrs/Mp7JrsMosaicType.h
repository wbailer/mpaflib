/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2495MP7JRSMOSAICTYPE_H
#define _2495MP7JRSMOSAICTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMosaicTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMosaicType_ExtInclude.h


class IMp7JrsStillRegionType;
typedef Dc1Ptr< IMp7JrsStillRegionType > Mp7JrsStillRegionPtr;
class IMp7JrsMosaicType;
typedef Dc1Ptr< IMp7JrsMosaicType> Mp7JrsMosaicPtr;
class IMp7JrsMosaicType_WarpingParam_LocalType0;
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_LocalType0 > Mp7JrsMosaicType_WarpingParam_Local0Ptr;
#include "Mp7JrsStillRegionType.h"
class IMp7JrsRegionLocatorType;
typedef Dc1Ptr< IMp7JrsRegionLocatorType > Mp7JrsRegionLocatorPtr;
class IMp7JrsSpatialMaskType;
typedef Dc1Ptr< IMp7JrsSpatialMaskType > Mp7JrsSpatialMaskPtr;
class IMp7JrsmediaTimePointType;
typedef Dc1Ptr< IMp7JrsmediaTimePointType > Mp7JrsmediaTimePointPtr;
class IMp7JrsMediaRelTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelTimePointType > Mp7JrsMediaRelTimePointPtr;
class IMp7JrsMediaRelIncrTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelIncrTimePointType > Mp7JrsMediaRelIncrTimePointPtr;
class IMp7JrsStillRegionType_CollectionType;
typedef Dc1Ptr< IMp7JrsStillRegionType_CollectionType > Mp7JrsStillRegionType_CollectionPtr;
class IMp7JrsMultipleViewType;
typedef Dc1Ptr< IMp7JrsMultipleViewType > Mp7JrsMultipleViewPtr;
class IMp7JrsStillRegionType_SpatialDecomposition_CollectionType;
typedef Dc1Ptr< IMp7JrsStillRegionType_SpatialDecomposition_CollectionType > Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr;
#include "Mp7JrsSegmentType.h"
class IMp7JrsMediaInformationType;
typedef Dc1Ptr< IMp7JrsMediaInformationType > Mp7JrsMediaInformationPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsControlledTermUseType;
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;
class IMp7JrsCreationInformationType;
typedef Dc1Ptr< IMp7JrsCreationInformationType > Mp7JrsCreationInformationPtr;
class IMp7JrsUsageInformationType;
typedef Dc1Ptr< IMp7JrsUsageInformationType > Mp7JrsUsageInformationPtr;
class IMp7JrsSegmentType_TextAnnotation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_CollectionType > Mp7JrsSegmentType_TextAnnotation_CollectionPtr;
class IMp7JrsSegmentType_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_CollectionType > Mp7JrsSegmentType_CollectionPtr;
class IMp7JrsSegmentType_MatchingHint_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_MatchingHint_CollectionType > Mp7JrsSegmentType_MatchingHint_CollectionPtr;
class IMp7JrsSegmentType_PointOfView_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_PointOfView_CollectionType > Mp7JrsSegmentType_PointOfView_CollectionPtr;
class IMp7JrsSegmentType_Relation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_Relation_CollectionType > Mp7JrsSegmentType_Relation_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsMosaicType for class Mp7JrsMosaicType<br>
 * Located at: mpeg7-v4.xsd, line 7095<br>
 * Classified: Class<br>
 * Derived from: StillRegionType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsMosaicType 
 :
		public IMp7JrsStillRegionType
{
public:
	// TODO: make these protected?
	IMp7JrsMosaicType();
	virtual ~IMp7JrsMosaicType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get WarpingParam element.
	 * @return Mp7JrsMosaicType_WarpingParam_Local0Ptr
	 */
	virtual Mp7JrsMosaicType_WarpingParam_Local0Ptr GetWarpingParam() const = 0;

	virtual bool IsValidWarpingParam() const = 0;

	/**
	 * Set WarpingParam element.
	 * @param item const Mp7JrsMosaicType_WarpingParam_Local0Ptr &
	 */
	virtual void SetWarpingParam(const Mp7JrsMosaicType_WarpingParam_Local0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate WarpingParam element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWarpingParam() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get SpatialLocator element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetSpatialLocator() const = 0;

	/**
	 * Set SpatialLocator element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	virtual void SetSpatialLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SpatialMask element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsSpatialMaskPtr
	 */
	virtual Mp7JrsSpatialMaskPtr GetSpatialMask() const = 0;

	/**
	 * Set SpatialMask element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsSpatialMaskPtr &
	 */
	virtual void SetSpatialMask(const Mp7JrsSpatialMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const = 0;

	/**
	 * Set MediaTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const = 0;

	/**
	 * Set MediaRelTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelIncrTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const = 0;

	/**
	 * Set MediaRelIncrTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get StillRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsStillRegionType_CollectionPtr
	 */
	virtual Mp7JrsStillRegionType_CollectionPtr GetStillRegionType_LocalType() const = 0;

	/**
	 * Set StillRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsStillRegionType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetStillRegionType_LocalType(const Mp7JrsStillRegionType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMultipleViewPtr
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const = 0;

	virtual bool IsValidMultipleView() const = 0;

	/**
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsStillRegionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMultipleView() = 0;

	/**
	 * Get SpatialDecomposition element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr
	 */
	virtual Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr GetSpatialDecomposition() const = 0;

	/**
	 * Set SpatialDecomposition element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSpatialDecomposition(const Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const = 0;

	/**
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const = 0;

	/**
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const = 0;

	/**
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const = 0;

	virtual bool IsValidStructuralUnit() const = 0;

	/**
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuralUnit() = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const = 0;

	/**
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const = 0;

	/**
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const = 0;

	/**
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const = 0;

	/**
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const = 0;

	/**
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const = 0;

	/**
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const = 0;

	/**
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const = 0;

	/**
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const = 0;

	/**
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of MosaicType.
	 * Currently this type contains 27 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>SpatialLocator</li>
	 * <li>SpatialMask</li>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>GridLayoutDescriptors</li>
	 * <li>IlluminationInvariantColor</li>
	 * <li>MultipleView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SpatialDecomposition</li>
	 * <li>WarpingParam</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMosaicType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMosaicType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMosaicType<br>
 * Located at: mpeg7-v4.xsd, line 7095<br>
 * Classified: Class<br>
 * Derived from: StillRegionType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 7095.<br>
 */
class DC1_EXPORT Mp7JrsMosaicType :
		public IMp7JrsMosaicType
{
	friend class Mp7JrsMosaicTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsMosaicType
	 
	 * @return Dc1Ptr< Mp7JrsStillRegionType >
	 */
	virtual Dc1Ptr< Mp7JrsStillRegionType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get WarpingParam element.
	 * @return Mp7JrsMosaicType_WarpingParam_Local0Ptr
	 */
	virtual Mp7JrsMosaicType_WarpingParam_Local0Ptr GetWarpingParam() const;

	virtual bool IsValidWarpingParam() const;

	/*
	 * Set WarpingParam element.
	 * @param item const Mp7JrsMosaicType_WarpingParam_Local0Ptr &
	 */
	virtual void SetWarpingParam(const Mp7JrsMosaicType_WarpingParam_Local0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate WarpingParam element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWarpingParam();

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get SpatialLocator element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsRegionLocatorPtr
	 */
	virtual Mp7JrsRegionLocatorPtr GetSpatialLocator() const;

	/*
	 * Set SpatialLocator element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsRegionLocatorPtr &
	 */
	virtual void SetSpatialLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SpatialMask element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsSpatialMaskPtr
	 */
	virtual Mp7JrsSpatialMaskPtr GetSpatialMask() const;

	/*
	 * Set SpatialMask element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsSpatialMaskPtr &
	 */
	virtual void SetSpatialMask(const Mp7JrsSpatialMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const;

	/*
	 * Set MediaTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const;

	/*
	 * Set MediaRelTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelIncrTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const;

	/*
	 * Set MediaRelIncrTimePoint element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get StillRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsStillRegionType_CollectionPtr
	 */
	virtual Mp7JrsStillRegionType_CollectionPtr GetStillRegionType_LocalType() const;

	/*
	 * Set StillRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsStillRegionType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetStillRegionType_LocalType(const Mp7JrsStillRegionType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsMultipleViewPtr
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const;

	virtual bool IsValidMultipleView() const;

	/*
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsStillRegionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMultipleView();

	/*
	 * Get SpatialDecomposition element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @return Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr
	 */
	virtual Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr GetSpatialDecomposition() const;

	/*
	 * Set SpatialDecomposition element.
<br>
	 * (Inherited from Mp7JrsStillRegionType)
	 * @param item const Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSpatialDecomposition(const Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const;

	/*
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const;

	/*
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const;

	virtual bool IsValidStructuralUnit() const;

	/*
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuralUnit();

		/* @name Choice

		 */
		//@{
	/*
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const;

	/*
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const;

	/*
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const;

	/*
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const;

	/*
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const;

	/*
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const;

	/*
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const;

	/*
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const;

	/*
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const;

	/*
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of MosaicType.
	 * Currently this type contains 27 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>SpatialLocator</li>
	 * <li>SpatialMask</li>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>GridLayoutDescriptors</li>
	 * <li>IlluminationInvariantColor</li>
	 * <li>MultipleView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SpatialDecomposition</li>
	 * <li>WarpingParam</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMosaicType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMosaicType();

protected:
	Mp7JrsMosaicType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsStillRegionType > m_Base;

	Mp7JrsMosaicType_WarpingParam_Local0Ptr m_WarpingParam;
	bool m_WarpingParam_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsMosaicType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2495MP7JRSMOSAICTYPE_H

