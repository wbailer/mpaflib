/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Initialize section of Dc1Factory.h
// included in Dc1Factory::Initialize()

{
	// Register extension namespace uri and its prefix(es), so that we can resolve it without Xerces XML DOM
	Dc1Factory::SetNamespaceUri(X("mpeg7"), X("urn:mpeg:mpeg7:schema:2004"));

	Dc1Factory * factory;
	
	factory = new Mp7JrsAbstractionLevelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAccessUnitTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAccessUnitType_FragmentUpdateUnit_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAcquaintanceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAffectiveTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAffectiveType_Score_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAffectiveType_Score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAffiliationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAgentObjectTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAgentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAgentType_Icon_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAggregatedMediaReviewDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticClipTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticEditedVideoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticModelType_Label_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticModelType_Semantics_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAnalyticTransitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioBPMTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioChordPatternDSFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioChordPatternDS_Chord_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioChordPatternDS_Chord_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioDSTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioFundamentalFrequencyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioHarmonicityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDScalarTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDVectorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioPowerTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioRhythmicPatternDSFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSegmentType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSignalQualityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSignalQualityType_ErrorEventList_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSignatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumBasisTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumCentroidTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumEnvelopeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumFlatnessTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumProjectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSpectrumSpreadTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSummaryComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioSummaryComponentType_Title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioTempoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatioTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualRegionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualSegmentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioVisualTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAudioWaveformTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsauxiliaryLanguageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsauxiliaryLanguageType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsauxiliaryLanguageType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBackgroundNoiseLevelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBalanceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBandwidthTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsbasicDurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsbasicTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsbeatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBinomialDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBoxListTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBrowsingPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCameraMotionSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCameraMotionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCameraMotionType_Segment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscharacterSetCodeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsChordBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsChordBaseType_NoteAdded_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsChordBaseType_NoteRemoved_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPerceptualAttributeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_AssociatedData_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_AssociatedData_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_CaptionLanguage_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Country_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Country_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_DatePeriod_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_DatePeriod_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Form_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Form_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Genre_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Genre_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Language_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Language_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_LanguageFormat_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_ParentalGuidance_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Review_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Review_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Subject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationPreferencesType_Subject_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeAliasTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeBaseType_domain_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeBaseType_Import_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationSchemeType_Term_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_CaptionLanguage_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_CaptionLanguage_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Form_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Genre_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Genre_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Language_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_MediaReview_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_ParentalGuidance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Purpose_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Release_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Release_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Release_Region_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_SignLanguage_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_SignLanguage_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Subject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Target_Age_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Target_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Target_Market_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClassificationType_Target_Region_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterClassificationModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterClassificationModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterClassificationModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterModelType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsClusterModelType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCollectionModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCollectionModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCollectionModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCollectionType_TextAnnotation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff14_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff14_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff2_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff2_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff20_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff20_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff27_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff27_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff5_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff5_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff63_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff63_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff9_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CbACCoeff9_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff14_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff14_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff2_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff2_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff20_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff20_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff27_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff27_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff5_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff5_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff63_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff63_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff9_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_CrACCoeff9_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff14_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff14_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff2_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff2_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff20_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff20_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff27_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff27_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff5_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff5_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff63_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff63_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff9_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorLayoutType_YACCoeff9_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorQuantizationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorQuantizationType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorQuantizationType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_Component_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_Component_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_Component_Offset_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_Component_Period_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Field_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSamplingType_Lattice_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSpaceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSpaceType_ColorTransMat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorSpaceType_ColorTransMat_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorStructureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorStructureType_Values_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorStructureType_Values_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsColorTemperatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompleteDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompleteDescriptionType_OrderingKey_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompleteDescriptionType_Relationships_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionShotEditingTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionShotTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionTransitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionTransitionType_compositionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionTransitionType_compositionType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCompositionTransitionType_compositionType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptCollectionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptCollectionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConceptTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConfusionCountTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConnectionBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConnectionsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConnectionsType_Acquaintance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsConnectionsType_Affiliation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentAbstractionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_CollectionType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentCollectionType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentDescriptionType_Affective_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentEntityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentEntityType_MultimediaContent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContentManagementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContinuousDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContinuousHiddenMarkovModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContinuousUniformDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContourShapeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContourShapeType_Peak_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsContourShapeType_Peak_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscontourTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscontourType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsControlledTermUseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscountryCodeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationDescriptionType_CreationInformation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationInformationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationInformationType_RelatedMaterial_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Creator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Creator_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_DatePeriod_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_DatePeriod_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Keyword_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Keyword_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Location_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Location_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Title_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Tool_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationPreferencesType_Tool_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationToolTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationToolType_Setting_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationToolType_Setting_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_Abstract_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_CopyrightString_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_CreationCoordinates_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_CreationCoordinates_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_CreationTool_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_Creator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreationType_Title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreatorType_Character_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCreatorType_Instrument_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsCrossChannelCorrelationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscurrencyCodeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscurvatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrscurvatureType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDcOffsetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDecoderInitTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDecoderInitType_SchemaReference_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructurePhraseType_operator_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDependencyStructureType_Sentence_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionMetadataTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionMetadataType_Creator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionMetadataType_Instrument_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionProfileTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorCollectionType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDescriptorModelType_Field_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsdim_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteHiddenMarkovModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteHiddenMarkovModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteHiddenMarkovModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDiscreteUniformDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDisseminationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDistributionPerceptualAttributeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_ColorVariance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_ColorVariance_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_Index_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_Index_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDominantColorType_Value_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoubleDiagonalMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoubleMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoublePullbackDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoublePushoutDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsdoubleVectorFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDSTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDSType_Header_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsdurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEdgeHistogramTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEdgeHistogramType_BinCounts_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEdgeHistogramType_BinCounts_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedMovingRegionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedVideoEditingTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedVideoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressType_Email_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressType_Fax_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressType_Telephone_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressType_Telephone_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsElectronicAddressType_Url_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEnhancedAudioSignatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsErrorEventTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEventTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEventType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEventType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEventType_SemanticPlace_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsEventType_SemanticTime_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExponentialDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtendedElectronicAddressTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtendedLanguageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtendedMediaQualityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsExtentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFaceRecognitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFaceRecognitionType_Feature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFaceRecognitionType_Feature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFigureTrajectoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFigureTrajectoryType_Vertex_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilter1DTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilter2DTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilteringTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilterSeparableTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilterSeparableType_Filter1D_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFilterTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFinancialTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFinancialType_AccountItem_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFinancialType_AccountItem_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFiniteStateModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFloatDiagonalMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFloatMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsfloatVectorFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFocusOfExpansionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFractionalPresenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdateCommandTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdateCommandType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdateContextTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdateContextTypeBaseFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdatePayloadTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFragmentUpdateUnitTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFrequencyTreeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFrequencyTreeType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFrequencyTreeType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsFrequencyViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGammaDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGaussianDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGaussianMixtureModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGaussianMixtureModelType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGaussianMixtureModelType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGeneralizedGaussianDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGeographicPointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGeographicPointType_latitude_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGeographicPointType_longitude_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGeometricDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGlobalTransitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGoFGoPColorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGofGopFeatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphicalClassificationSchemeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphicalRuleDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphicalTermDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGraphType_Node_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGridLayoutTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGridLayoutType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsGridLayoutType_descriptorMask_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogInformationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogInformationType_Recognizer_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogResultTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogResultType_Result_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHandWritingRecogResultType_Result_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHarmonicInstrumentTimbreTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHarmonicSpectralCentroidTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHarmonicSpectralDeviationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHarmonicSpectralSpreadTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHarmonicSpectralVariationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHeaderTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHierarchicalSummaryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHierarchicalSummaryType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHierarchicalSummaryType_components_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHierarchicalSummaryType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHistogramProbabilityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHomogeneousTextureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsHyperGeometricDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIlluminationInvariantColorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageLocatorType_BytePosition_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_GlobalSignatureA_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_GlobalSignatureA_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_GlobalSignatureB_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_GlobalSignatureB_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageSignatureType_LocalSignature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageTextTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageTextType_textType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageTextType_textType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageTextType_textType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsImageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIncrDurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIndividualMediaReviewDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkContentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Brush_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Brush_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_InputDevice_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_OverlaidMedia_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Param_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Param_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Style_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Style_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_Style_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_CollectionType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInkSegmentType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineMediaTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionType_Definition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionType_Name_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionType_Term_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInlineTermDefinitionType_Term_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInstantMessagingScreenNameTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInstrumentTimbreTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntegerDiagonalMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntegerMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsintegerVectorFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsInternalTransitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntraCompositionShotEditingTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntraCompositionShotTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIrregularVisualTimeSeriesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIrregularVisualTimeSeriesType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsIrregularVisualTimeSeriesType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsKeyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsKeyType_KeyNote_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsKeywordAnnotationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsKeywordAnnotationType_Keyword_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsKeywordAnnotationType_Keyword_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLexiconTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinearPerceptualAttributeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticDocumentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticDocumentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticDocumentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityType_edit_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityType_length_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityType_MediaLocator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityType_Relation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticEntityType_start_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLinguisticTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrslistOfPositiveIntegerForDimFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogAttackTimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsLognormalDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMatchingHintTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMatchingHintType_Hint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMatchingHintType_Hint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaAgentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaDescriptionType_MediaInformation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsmediaDurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_AudioCoding_Sample_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_BitRate_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_ScalableCoding_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_ScalableCoding_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_ScalableCoding_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Format_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Frame_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaIdentificationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaIdentificationType_AudioDomain_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaIdentificationType_ImageDomain_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaIdentificationType_VideoDomain_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaIncrDurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaInformationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaInformationType_MediaProfile_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaInstanceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaProfileTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaProfileType_MediaInstance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_PerceptibleDefects_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_QualityRating_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_QualityRating_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaQualityType_RatingInformationLocator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaRelIncrTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaRelTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaReviewType_FreeTextReview_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaSourceSegmentDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaSpaceMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaSpaceMaskType_SubInterval_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaSpaceMaskType_SubInterval_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsmediaTimeOffsetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsmediaTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTranscodingHintsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTranscodingHintsType_CodingHints_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTranscodingHintsType_MotionHint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodyContourTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_NoteArray_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_NoteArray_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_NoteArray_Note_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_NoteArray_Note_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_StartingNote_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMelodyType_MelodySequence_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMeterTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMeterType_Numerator_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsmimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMinusOneToOneMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsminusOneToOneTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsminusOneToOneVectorFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsminusOneToOneVector_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_CollectionType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_CollectionType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixedCollectionType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixtureAmountOfMotionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMixtureCameraMotionSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsModelDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsModelDescriptionType_Model_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsModelStateTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMorphismGraphTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_modelType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_modelType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMosaicType_WarpingParam_modelType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_Intensity_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialDistributionParams_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_TemporalParams_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionActivityType_TemporalParams_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionTrajectoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionTrajectoryType_CoordDef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionTrajectoryType_CoordDef_Repr_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMotionTrajectoryType_CoordRef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionFeatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatioTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionSpatioTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMovingRegionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMpeg7_Description_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMpeg7_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMpeg7BaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMpeg7TypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaCollectionType_Collection_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaContentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaSegmentMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultimediaTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultipleViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultipleViewType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultipleViewType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsMultiResolutionPyramidTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNameComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonDependencyStructurePhraseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonDependencyStructurePhraseType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonDependencyStructurePhraseType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonMixtureAmountOfMotionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsNonMixtureCameraMotionSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsnonNegativeRealFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsObjectTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsObjectType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsObjectType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderedGroupDataSetMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderingKeyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderingKeyType_Field_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderingKeyType_Field_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrderingKeyType_Selector_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_Name_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_NameTerm_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOrganizationType_NameTerm_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsOutputQCValueTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPackageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPackageType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPackageType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPackageType_Scheme_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrspaddingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrspaddingType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrspaddingType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParameterTrajectoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParametricMotionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParametricMotionType_CoordDef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParametricMotionType_CoordRef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParametricMotionType_Parameters_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParentalGuidanceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsParentalGuidanceType_Region_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPartitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPartitionType_dim_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeType_IsConnected_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeType_IsConnected_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptual3DShapeType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualAttributeDSTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualBeatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualBeatType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualEnergyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualEnergyType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualGenreDistributionElementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualGenreDistributionElementType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualGenreDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualInstrumentationDistributionElementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualInstrumentationDistributionElementType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualInstrumentationDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualLanguageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualLyricsDistributionElementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualLyricsDistributionElementType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualLyricsDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualMoodDistributionElementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualMoodDistributionElementType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualMoodDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualRecordingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualSoundTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualTempoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualTempoType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualValenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualValenceType_score_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPerceptualVocalsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPercussiveInstrumentTimbreTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonalInterestsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonalInterestsType_Activity_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonalInterestsType_Favorite_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonalInterestsType_Interest_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_Name_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_NameTerm_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonGroupType_NameTerm_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonNameTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonNameType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonNameType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_Affiliation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_Affiliation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_Citizenship_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_ElectronicAddress_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPersonType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneLexiconIndexTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneLexiconTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneLexiconType_Token_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneticTranscriptionLexiconTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPhoneTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPitchProfileDSTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_AdministrativeUnit_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_AdministrativeUnit_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_AstronomicalBody_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_ElectronicAddress_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_GeographicPosition_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_GeographicPosition_Point_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_NameTerm_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_PlaceDescription_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_PostalAddress_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_Region_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_StructuredInternalCoordinates_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPlaceType_StructuredPostalAddress_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPointOfViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPointOfViewType_Importance_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPointOfViewType_Importance_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPoissonDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionType_Time_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionType_Time_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionType_Time_recurrence_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionType_Time_recurrence_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPreferenceConditionType_Time_recurrence_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrspreferenceValueTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityClassificationModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Cumulant_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Cumulant_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Moment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Moment_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityDistributionType_Moment_Value_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityMatrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityModelClassTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityModelClassType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityModelClassType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProbabilityModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsprobabilityVectorFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsprobabilityVector_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsProfileWeightsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPullbackDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsPushoutDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCItemResultTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCItemResultType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCItemResultType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCItemResultType_VerificationMedia_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_EssenceType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_InputParameter_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_ItemScope_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_QCItem_Relevance_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCProfileType_RelevanceLevel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCValueTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsQCValueType_track_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRatingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRatingType_RatingScheme_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingPreferencesType_RecordingRequest_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordAssociatedData_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordingPeriod_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsReferencePitchTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsReferencePitchType_BaseFrequency_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsReferencePitchType_BasePitchNumber_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsregionCodeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorType_Box_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorType_Box_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorType_CoordRef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorType_Polygon_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionLocatorType_Polygon_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionShapeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionShapeType_MagnitudeOfART_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegionShapeType_MagnitudeOfART_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegularVisualTimeSeriesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRelatedMaterialTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRelationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRelativeDelayTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRelIncrTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRelTimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsResolutionViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRhythmicBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRightsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsRightsType_RightsID_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsScalableColorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsScalableSeriesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsScalableSeriesType_Scaling_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsScalableSeriesType_Scaling_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsscaleTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSceneGraphMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSceneGraphMaskType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSceneGraphMaskType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSchemaReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentCollectionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentCollectionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentOutputQCValueTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentOutputQCValueType_Output_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentQCValueTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_MatchingHint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_PointOfView_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_Relation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_TextAnnotation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_TextAnnotation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSegmentType_TextAnnotation_type_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBagTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBagType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBagType_Graph_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBagType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_Label_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_Property_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticBaseType_Relation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticDescriptionType_Semantics_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticPlaceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticStateTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticStateType_AttributeValuePair_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticStateType_AttributeValuePair_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticStateType_AttributeValuePair_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticStateType_AttributeValuePair_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticTimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSemanticTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSentencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSentencesType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSentencesType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSequentialSummaryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSequentialSummaryType_components_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSeriesOfScalarBinaryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSeriesOfScalarTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSeriesOfVectorBinaryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSeriesOfVectorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShape3DTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShape3DType_Spectrum_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShape3DType_Spectrum_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_DynamicShapeVariation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_DynamicShapeVariation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_StaticShapeVariation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_StaticShapeVariation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_StatisticalVariation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShapeVariationType_StatisticalVariation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShotEditingTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShotEditingTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShotEditingTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShotTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalPlaneFractionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalPlaneOriginTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalPlaneSampleTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalPlaneTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalPlaneType_dim_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSignalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSilenceHeaderTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSilenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundClassificationModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundClassificationModelType_SoundModel_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundModelStateHistogramTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundModelStateHistogramType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundModelStateHistogramType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundModelStatePathTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSoundModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationDate_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationDate_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationFormat_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationLocation_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationSource_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_DisseminationSource_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_Disseminator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_Disseminator_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_MediaFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSourcePreferencesType_MediaFormat_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyGraphTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyGraphType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyGraphType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyGraphType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyGraphType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceFrequencyViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceResolutionViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceTreeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceTreeType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceTreeType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpaceViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatialMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatialMaskType_SubRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatialSegmentDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalLocatorType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalLocatorType_CoordRef_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalLocatorType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpatioTemporalSegmentDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_PhoneIndex_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_WordIndex_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpectralCentroidTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentHeaderTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentHeaderType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentHeaderType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentIndexEntryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSpokenContentLinkTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStateTransitionModelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStateTransitionModelType_State_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionFeatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStillRegionType_SpatialDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStreamLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStreamMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStreamMaskType_StreamSection_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStreamSectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_How_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_WhatAction_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_WhatObject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_When_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_Where_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_Who_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredAnnotationType_Why_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_CollectionType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_CollectionType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsStructuredCollectionType_Relationships_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectClassificationSchemeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectClassificationSchemeType_Term_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Classification_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Note_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Note_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Term_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSubjectTermDefinitionType_Term_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarizationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarizationType_Summary_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrssummaryComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrssummaryComponentType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrssummaryComponentType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryDescriptionType_Summarization_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryPreferencesType_SummaryTheme_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryPreferencesType_SummaryType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryPreferencesType_SummaryType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_Caption_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentGroupType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentType_KeyFrame_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentType_KeySound_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummarySegmentType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryThemeListTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryThemeListType_SummaryTheme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryThemeListType_SummaryTheme_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSummaryType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSyntacticConstituentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSyntacticConstituentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsSyntacticConstituentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalCentroidTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_InterpolationFunctions_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalInterpolationType_WholeInterval_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalMaskTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalMaskType_SubInterval_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalSegmentDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalSegmentLocatorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermAliasReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionBaseType_Definition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionBaseType_Name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionBaseType_Name_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionType_Term_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermDefinitionType_Term_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermNoteQualifierTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermNoteQualifierType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermNoteQualifierType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermReferenceListTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermRelationQualifierTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermRelationQualifierType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermRelationQualifierType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstermURIReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTermUseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextAnnotationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextAnnotationType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextAnnotationType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextSegmentType_TextDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextualBaseTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextualBaseType_phoneticTranscription_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextualSummaryComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextualSummaryComponentType_FreeText_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextualTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextureBrowsingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextureBrowsingType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTextureBrowsingType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstextureListTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstextureListType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstimeOffsetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrstimePointTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTimeZoneTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTimeZoneType_value_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTitleMediaTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTitleTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTitleType_type_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTitleType_type_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTitleType_type_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsTransmissionTechnologyTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUniqueIDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned10Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned11Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned12Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned13Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned14Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned15Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned16Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned17Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned18Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned19Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned20Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned21Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned22Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned23Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned24Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned25Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned26Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned27Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned28Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned29Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned3Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned30Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned31Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned32Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned4Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned5Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned6Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned7Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned8Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7Jrsunsigned9Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsURIFragmentReferenceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageDescriptionType_UsageInformation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageHistoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageHistoryType_UserActionHistory_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageInformationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageInformationType_Availability_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageInformationType_UsageRecord_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUsageRecordTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionHistoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionHistoryType_UserActionList_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionListTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionListType_UserAction_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionType_ActionDataItem_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserActionType_ActionTime_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserDescriptionType_UsageHistory_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserDescriptionType_UserPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserIdentifierTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserPreferencesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserPreferencesType_RecordingPreferences_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserProfileTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsUserProfileType_PublicIdentifier_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationDescriptionType_Variation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationDescriptionType_VariationSet_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationSetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationSetType_Variation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationType_VariationRelationship_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationType_VariationRelationship_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationType_VariationRelationship_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVariationType_VariationRelationship_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentFeatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentMediaSourceDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentMediaSourceDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentMediaSourceDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatialDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatialDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatialDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatioTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentTemporalDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentTemporalDecompositionType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentTemporalDecompositionType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSegmentType_Mosaic_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoTextTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoTextType_textType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoTextType_textType_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoTextType_textType_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoViewGraphTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoViewGraphType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoViewGraphType_CollectionType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoViewGraphType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVideoViewGraphType_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewDecompositionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewDescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewDescriptionType_View_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewDescriptionType_ViewDecomposition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewDescriptionType_ViewSet_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetType_setProperty_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetType_setProperty_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewSetType_setProperty_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsViewTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVisualDSTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVisualDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVisualSummaryComponentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsVisualTimeSeriesTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormType_terms_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormType_type_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormType_type_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormType_type_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordFormType_type_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconIndexTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType0Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType1Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType2Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordLexiconType_Token_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsWordTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsxPathAbsoluteSelectorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsxPathFieldTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsxPathRefTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsxPathSelectorTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrsxPathTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new Mp7JrszeroToOneTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

}
