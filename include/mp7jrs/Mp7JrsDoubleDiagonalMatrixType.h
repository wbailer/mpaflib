/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1809MP7JRSDOUBLEDIAGONALMATRIXTYPE_H
#define _1809MP7JRSDOUBLEDIAGONALMATRIXTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsDoubleDiagonalMatrixTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtInclude.h


class IMp7JrsdoubleVector;
typedef Dc1Ptr< IMp7JrsdoubleVector > Mp7JrsdoubleVectorPtr;
class IMp7JrsDoubleDiagonalMatrixType;
typedef Dc1Ptr< IMp7JrsDoubleDiagonalMatrixType> Mp7JrsDoubleDiagonalMatrixPtr;
class IMp7Jrsdim_LocalType;
typedef Dc1Ptr< IMp7Jrsdim_LocalType > Mp7Jrsdim_LocalPtr;
#include "Mp7JrsdoubleVector.h"

/** 
 * Generated interface IMp7JrsDoubleDiagonalMatrixType for class Mp7JrsDoubleDiagonalMatrixType<br>
 * Located at: mpeg7-v4.xsd, line 4147<br>
 * Classified: Class<br>
 * Derived from: doubleVector (Collection)<br>
 */
class MP7JRS_EXPORT IMp7JrsDoubleDiagonalMatrixType 
 :
		public IMp7JrsdoubleVector
{
public:
	// TODO: make these protected?
	IMp7JrsDoubleDiagonalMatrixType();
	virtual ~IMp7JrsDoubleDiagonalMatrixType();
	/**
	 * Retrieves the base collection of Mp7JrsDoubleDiagonalMatrixType
	 
	 * @return Dc1Ptr< Mp7JrsdoubleVector >
	 */
	virtual Dc1Ptr< Mp7JrsdoubleVector > GetBaseCollection() const = 0;


	/**
	 * Initializes the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param maxElems Number of elements for which memory is initially allocated.
	 */
	virtual void Initialize(unsigned int maxElems) = 0;


	/**
	 * Adds a new element to the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toAdd Element to be added. Default element type: double
	 */
	virtual void addElement(const double &toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Sets the element at the given position in the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toSet New value of the element. Default element type: double
	 * @param setAt Position of the element which shall be set.
	 */
	virtual void setElementAt(const double &toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Inserts the element at the given position in the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toInsert Element to be inserted. Default element type: double
	 * @param insertAt Position where the element will be inserted.
	 */
	virtual void insertElementAt(const double &toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Removes all elements from the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 */
	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Removes the specified element from the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param removeAt Position of the element to be removed. 
	 */
	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;



	/**
	 * Check whether the collection contains the given element.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toCheck Element to be checked for. Default element type: double
	 * @return True if the element is in the collection, false otherwise.
	 */
	virtual bool containsElement(const double &toCheck) = 0;


	/**
	 * Get the current capacity of the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @return The number of elements for which memory has been allocated.
	 */
	virtual unsigned int curCapacity() const = 0;


	/**
	 * Gets a constant pointer to the element at the given position.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param Position of the element to be retrieved.
	 * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: double.
	 */
	virtual const double elementAt(unsigned int getAt) const = 0;


	/**
	 * Gets a pointer to the element at the given position.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param Position of the element to be retrieved.
	 * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: double.
	 */
	virtual double elementAt(unsigned int getAt) = 0;


	/**
	 * Get the size of the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @return The current number of elements in the collection.
	 */
	virtual unsigned int size() const = 0;


	/**
	 * Allocate additional memory for the speciifed number of elements.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param length Number of elements to allocate memory for.
	 */
	virtual void ensureExtraCapacity(unsigned int length) = 0;


	// JRS: addition to Xerces collection
	/**
	 * Get the position of the specified element. 
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toCheck Element to be found in the collection. Default element type: double
	 * @return The position of the specified element or -1 if the element has not been found.
	 */
	virtual int elementIndexOf(const double &toCheck) const = 0;


	/**
	 * Get dim attribute.
	 * @return Mp7Jrsdim_LocalPtr
	 */
	virtual Mp7Jrsdim_LocalPtr Getdim() const = 0;

	/**
	 * Set dim attribute.
	 * @param item const Mp7Jrsdim_LocalPtr &
	 */
	// Mandatory
	virtual void Setdim(const Mp7Jrsdim_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of DoubleDiagonalMatrixType.
	 * Currently just supports rudimentary XPath expressions as DoubleDiagonalMatrixType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsDoubleDiagonalMatrixType<br>
 * Located at: mpeg7-v4.xsd, line 4147<br>
 * Classified: Class<br>
 * Derived from: doubleVector (Collection)<br>
 * Defined in mpeg7-v4.xsd, line 4147.<br>
 */
class DC1_EXPORT Mp7JrsDoubleDiagonalMatrixType :
		public IMp7JrsDoubleDiagonalMatrixType
{
	friend class Mp7JrsDoubleDiagonalMatrixTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsDoubleDiagonalMatrixType
	 
	 * @return Dc1Ptr< Mp7JrsdoubleVector >
	 */
	virtual Dc1Ptr< Mp7JrsdoubleVector > GetBase() const;
	/*
	 * Retrieves the base collection of Mp7JrsDoubleDiagonalMatrixType
	 
	 * @return Dc1Ptr< Mp7JrsdoubleVector >
	 */
	virtual Dc1Ptr< Mp7JrsdoubleVector > GetBaseCollection() const;


	/*
	 * Initializes the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param maxElems Number of elements for which memory is initially allocated.
	 */
	virtual void Initialize(unsigned int maxElems);


	/*
	 * Adds a new element to the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toAdd Element to be added. Default element type: double
	 */
	virtual void addElement(const double &toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Sets the element at the given position in the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toSet New value of the element. Default element type: double
	 * @param setAt Position of the element which shall be set.
	 */
	virtual void setElementAt(const double &toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Inserts the element at the given position in the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toInsert Element to be inserted. Default element type: double
	 * @param insertAt Position where the element will be inserted.
	 */
	virtual void insertElementAt(const double &toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Removes all elements from the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 */
	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Removes the specified element from the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param removeAt Position of the element to be removed. 
	 */
	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);



	/*
	 * Check whether the collection contains the given element.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toCheck Element to be checked for. Default element type: double
	 * @return True if the element is in the collection, false otherwise.
	 */
	virtual bool containsElement(const double &toCheck);


	/*
	 * Get the current capacity of the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @return The number of elements for which memory has been allocated.
	 */
	virtual unsigned int curCapacity() const;


	/*
	 * Gets a constant pointer to the element at the given position.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param Position of the element to be retrieved.
	 * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: double.
	 */
	virtual const double elementAt(unsigned int getAt) const;


	/*
	 * Gets a pointer to the element at the given position.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param Position of the element to be retrieved.
	 * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: double.
	 */
	virtual double elementAt(unsigned int getAt);


	/*
	 * Get the size of the collection.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @return The current number of elements in the collection.
	 */
	virtual unsigned int size() const;


	/*
	 * Allocate additional memory for the speciifed number of elements.
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param length Number of elements to allocate memory for.
	 */
	virtual void ensureExtraCapacity(unsigned int length);


	// JRS: addition to Xerces collection
	/*
	 * Get the position of the specified element. 
	 * (Inherited from Mp7JrsdoubleVector)
	 * @param toCheck Element to be found in the collection. Default element type: double
	 * @return The position of the specified element or -1 if the element has not been found.
	 */
	virtual int elementIndexOf(const double &toCheck) const;


	/*
	 * Get dim attribute.
	 * @return Mp7Jrsdim_LocalPtr
	 */
	virtual Mp7Jrsdim_LocalPtr Getdim() const;

	/*
	 * Set dim attribute.
	 * @param item const Mp7Jrsdim_LocalPtr &
	 */
	// Mandatory
	virtual void Setdim(const Mp7Jrsdim_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Gets or creates Dc1NodePtr child elements of DoubleDiagonalMatrixType.
	 * Currently just supports rudimentary XPath expressions as DoubleDiagonalMatrixType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsDoubleDiagonalMatrixType();

protected:
	Mp7JrsDoubleDiagonalMatrixType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7Jrsdim_LocalPtr m_dim;

	// Base Collection
	Dc1Ptr< Mp7JrsdoubleVector > m_Base;


// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1809MP7JRSDOUBLEDIAGONALMATRIXTYPE_H

