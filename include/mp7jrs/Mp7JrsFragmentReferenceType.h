/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1953MP7JRSFRAGMENTREFERENCETYPE_H
#define _1953MP7JRSFRAGMENTREFERENCETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"

#include "Mp7JrsFragmentReferenceTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsFragmentReferenceType_ExtInclude.h


class IMp7JrsFragmentReferenceType;
typedef Dc1Ptr< IMp7JrsFragmentReferenceType> Mp7JrsFragmentReferencePtr;

/** 
 * Generated interface IMp7JrsFragmentReferenceType for class Mp7JrsFragmentReferenceType<br>
 * Located at: mpeg7-v4.xsd, line 130<br>
 * Classified: Class<br>
 * Abstract class <br>
 */
class MP7JRS_EXPORT IMp7JrsFragmentReferenceType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsFragmentReferenceType();
	virtual ~IMp7JrsFragmentReferenceType();
	/**
	 * Get isDeferred attribute.
	 * @return bool
	 */
	virtual bool GetisDeferred() const = 0;

	/**
	 * Get isDeferred attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistisDeferred() const = 0;

	/**
	 * Set isDeferred attribute.
	 * @param item bool
	 */
	virtual void SetisDeferred(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate isDeferred attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateisDeferred() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of FragmentReferenceType.
	 * Currently just supports rudimentary XPath expressions as FragmentReferenceType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsFragmentReferenceType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsFragmentReferenceType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsFragmentReferenceType<br>
 * Located at: mpeg7-v4.xsd, line 130<br>
 * Classified: Class<br>
 * Abstract class <br>
 * Defined in mpeg7-v4.xsd, line 130.<br>
 */
class DC1_EXPORT Mp7JrsFragmentReferenceType :
		public IMp7JrsFragmentReferenceType
{
	friend class Mp7JrsFragmentReferenceTypeFactory; // constructs objects

public:
	/*
	 * Get isDeferred attribute.
	 * @return bool
	 */
	virtual bool GetisDeferred() const;

	/*
	 * Get isDeferred attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistisDeferred() const;

	/*
	 * Set isDeferred attribute.
	 * @param item bool
	 */
	virtual void SetisDeferred(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate isDeferred attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateisDeferred();

	/*
	 * Gets or creates Dc1NodePtr child elements of FragmentReferenceType.
	 * Currently just supports rudimentary XPath expressions as FragmentReferenceType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsFragmentReferenceType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsFragmentReferenceType();

protected:
	Mp7JrsFragmentReferenceType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	bool m_isDeferred;
	bool m_isDeferred_Exist;
	bool m_isDeferred_Default;



// no includefile for extension defined 
// file Mp7JrsFragmentReferenceType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1953MP7JRSFRAGMENTREFERENCETYPE_H

