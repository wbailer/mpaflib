/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3403MP7JRSSPEAKERINFOTYPE_H
#define _3403MP7JRSSPEAKERINFOTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSpeakerInfoTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtInclude.h


class IMp7JrsHeaderType;
typedef Dc1Ptr< IMp7JrsHeaderType > Mp7JrsHeaderPtr;
class IMp7JrsSpeakerInfoType;
typedef Dc1Ptr< IMp7JrsSpeakerInfoType> Mp7JrsSpeakerInfoPtr;
#include "Mp7JrsSpeakerInfoType_provenance_LocalType.h"
class IMp7JrsPersonType;
typedef Dc1Ptr< IMp7JrsPersonType > Mp7JrsPersonPtr;
class IMp7JrsSpeakerInfoType_WordIndex_LocalType;
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_LocalType > Mp7JrsSpeakerInfoType_WordIndex_LocalPtr;
class IMp7JrsSpeakerInfoType_PhoneIndex_LocalType;
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_LocalType > Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr;
#include "Mp7JrsHeaderType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSpeakerInfoType for class Mp7JrsSpeakerInfoType<br>
 * Located at: mpeg7-v4.xsd, line 2228<br>
 * Classified: Class<br>
 * Derived from: HeaderType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSpeakerInfoType 
 :
		public IMp7JrsHeaderType
{
public:
	// TODO: make these protected?
	IMp7JrsSpeakerInfoType();
	virtual ~IMp7JrsSpeakerInfoType();
	/**
	 * Get phoneLexiconRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetphoneLexiconRef() const = 0;

	/**
	 * Get phoneLexiconRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistphoneLexiconRef() const = 0;

	/**
	 * Set phoneLexiconRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetphoneLexiconRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate phoneLexiconRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatephoneLexiconRef() = 0;

	/**
	 * Get wordLexiconRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetwordLexiconRef() const = 0;

	/**
	 * Get wordLexiconRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistwordLexiconRef() const = 0;

	/**
	 * Set wordLexiconRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetwordLexiconRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate wordLexiconRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatewordLexiconRef() = 0;

	/**
	 * Get confusionInfoRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetconfusionInfoRef() const = 0;

	/**
	 * Get confusionInfoRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistconfusionInfoRef() const = 0;

	/**
	 * Set confusionInfoRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetconfusionInfoRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate confusionInfoRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateconfusionInfoRef() = 0;

	/**
	 * Get descriptionMetadataRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdescriptionMetadataRef() const = 0;

	/**
	 * Get descriptionMetadataRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdescriptionMetadataRef() const = 0;

	/**
	 * Set descriptionMetadataRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdescriptionMetadataRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate descriptionMetadataRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedescriptionMetadataRef() = 0;

	/**
	 * Get provenance attribute.
	 * @return Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration
	 */
	virtual Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration Getprovenance() const = 0;

	/**
	 * Set provenance attribute.
	 * @param item Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration
	 */
	// Mandatory
	virtual void Setprovenance(Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get SpokenLanguage element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetSpokenLanguage() const = 0;

	/**
	 * Set SpokenLanguage element.
	 * @param item XMLCh *
	 */
	// Mandatory			
	virtual void SetSpokenLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Person element.
	 * @return Mp7JrsPersonPtr
	 */
	virtual Mp7JrsPersonPtr GetPerson() const = 0;

	virtual bool IsValidPerson() const = 0;

	/**
	 * Set Person element.
	 * @param item const Mp7JrsPersonPtr &
	 */
	virtual void SetPerson(const Mp7JrsPersonPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Person element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerson() = 0;

	/**
	 * Get WordIndex element.
	 * @return Mp7JrsSpeakerInfoType_WordIndex_LocalPtr
	 */
	virtual Mp7JrsSpeakerInfoType_WordIndex_LocalPtr GetWordIndex() const = 0;

	virtual bool IsValidWordIndex() const = 0;

	/**
	 * Set WordIndex element.
	 * @param item const Mp7JrsSpeakerInfoType_WordIndex_LocalPtr &
	 */
	virtual void SetWordIndex(const Mp7JrsSpeakerInfoType_WordIndex_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate WordIndex element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWordIndex() = 0;

	/**
	 * Get PhoneIndex element.
	 * @return Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr
	 */
	virtual Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr GetPhoneIndex() const = 0;

	virtual bool IsValidPhoneIndex() const = 0;

	/**
	 * Set PhoneIndex element.
	 * @param item const Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr &
	 */
	virtual void SetPhoneIndex(const Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PhoneIndex element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePhoneIndex() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsHeaderType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsHeaderType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of SpeakerInfoType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Person</li>
	 * <li>WordIndex</li>
	 * <li>PhoneIndex</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSpeakerInfoType<br>
 * Located at: mpeg7-v4.xsd, line 2228<br>
 * Classified: Class<br>
 * Derived from: HeaderType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 2228.<br>
 */
class DC1_EXPORT Mp7JrsSpeakerInfoType :
		public IMp7JrsSpeakerInfoType
{
	friend class Mp7JrsSpeakerInfoTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSpeakerInfoType
	 
	 * @return Dc1Ptr< Mp7JrsHeaderType >
	 */
	virtual Dc1Ptr< Mp7JrsHeaderType > GetBase() const;
	/*
	 * Get phoneLexiconRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetphoneLexiconRef() const;

	/*
	 * Get phoneLexiconRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistphoneLexiconRef() const;

	/*
	 * Set phoneLexiconRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetphoneLexiconRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate phoneLexiconRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatephoneLexiconRef();

	/*
	 * Get wordLexiconRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetwordLexiconRef() const;

	/*
	 * Get wordLexiconRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistwordLexiconRef() const;

	/*
	 * Set wordLexiconRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetwordLexiconRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate wordLexiconRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatewordLexiconRef();

	/*
	 * Get confusionInfoRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetconfusionInfoRef() const;

	/*
	 * Get confusionInfoRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistconfusionInfoRef() const;

	/*
	 * Set confusionInfoRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetconfusionInfoRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate confusionInfoRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateconfusionInfoRef();

	/*
	 * Get descriptionMetadataRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdescriptionMetadataRef() const;

	/*
	 * Get descriptionMetadataRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdescriptionMetadataRef() const;

	/*
	 * Set descriptionMetadataRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdescriptionMetadataRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate descriptionMetadataRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedescriptionMetadataRef();

	/*
	 * Get provenance attribute.
	 * @return Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration
	 */
	virtual Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration Getprovenance() const;

	/*
	 * Set provenance attribute.
	 * @param item Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration
	 */
	// Mandatory
	virtual void Setprovenance(Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Sequence

		 */
		//@{
	/*
	 * Get SpokenLanguage element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetSpokenLanguage() const;

	/*
	 * Set SpokenLanguage element.
	 * @param item XMLCh *
	 */
	// Mandatory			
	virtual void SetSpokenLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Person element.
	 * @return Mp7JrsPersonPtr
	 */
	virtual Mp7JrsPersonPtr GetPerson() const;

	virtual bool IsValidPerson() const;

	/*
	 * Set Person element.
	 * @param item const Mp7JrsPersonPtr &
	 */
	virtual void SetPerson(const Mp7JrsPersonPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Person element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerson();

	/*
	 * Get WordIndex element.
	 * @return Mp7JrsSpeakerInfoType_WordIndex_LocalPtr
	 */
	virtual Mp7JrsSpeakerInfoType_WordIndex_LocalPtr GetWordIndex() const;

	virtual bool IsValidWordIndex() const;

	/*
	 * Set WordIndex element.
	 * @param item const Mp7JrsSpeakerInfoType_WordIndex_LocalPtr &
	 */
	virtual void SetWordIndex(const Mp7JrsSpeakerInfoType_WordIndex_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate WordIndex element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateWordIndex();

	/*
	 * Get PhoneIndex element.
	 * @return Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr
	 */
	virtual Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr GetPhoneIndex() const;

	virtual bool IsValidPhoneIndex() const;

	/*
	 * Set PhoneIndex element.
	 * @param item const Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr &
	 */
	virtual void SetPhoneIndex(const Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PhoneIndex element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePhoneIndex();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsHeaderType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsHeaderType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsHeaderType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Gets or creates Dc1NodePtr child elements of SpeakerInfoType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Person</li>
	 * <li>WordIndex</li>
	 * <li>PhoneIndex</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSpeakerInfoType();

protected:
	Mp7JrsSpeakerInfoType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_phoneLexiconRef;
	bool m_phoneLexiconRef_Exist;
	XMLCh * m_wordLexiconRef;
	bool m_wordLexiconRef_Exist;
	XMLCh * m_confusionInfoRef;
	bool m_confusionInfoRef_Exist;
	XMLCh * m_descriptionMetadataRef;
	bool m_descriptionMetadataRef_Exist;
	Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration m_provenance;

	// Base Class
	Dc1Ptr< Mp7JrsHeaderType > m_Base;

	XMLCh * m_SpokenLanguage;
	Mp7JrsPersonPtr m_Person;
	bool m_Person_Exist; // For optional elements 
	Mp7JrsSpeakerInfoType_WordIndex_LocalPtr m_WordIndex;
	bool m_WordIndex_Exist; // For optional elements 
	Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr m_PhoneIndex;
	bool m_PhoneIndex_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3403MP7JRSSPEAKERINFOTYPE_H

