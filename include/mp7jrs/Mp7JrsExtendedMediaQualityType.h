/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1893MP7JRSEXTENDEDMEDIAQUALITYTYPE_H
#define _1893MP7JRSEXTENDEDMEDIAQUALITYTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsExtendedMediaQualityTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtInclude.h


class IMp7JrsMediaQualityType;
typedef Dc1Ptr< IMp7JrsMediaQualityType > Mp7JrsMediaQualityPtr;
class IMp7JrsExtendedMediaQualityType;
typedef Dc1Ptr< IMp7JrsExtendedMediaQualityType> Mp7JrsExtendedMediaQualityPtr;
class IMp7JrsQCProfileType;
typedef Dc1Ptr< IMp7JrsQCProfileType > Mp7JrsQCProfilePtr;
class IMp7JrsExtendedMediaQualityType_QCItemResult_CollectionType;
typedef Dc1Ptr< IMp7JrsExtendedMediaQualityType_QCItemResult_CollectionType > Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr;
class IMp7JrsPersonType;
typedef Dc1Ptr< IMp7JrsPersonType > Mp7JrsPersonPtr;
class IMp7JrsCreationToolType;
typedef Dc1Ptr< IMp7JrsCreationToolType > Mp7JrsCreationToolPtr;
class IMp7JrsTextAnnotationType;
typedef Dc1Ptr< IMp7JrsTextAnnotationType > Mp7JrsTextAnnotationPtr;
#include "Mp7JrsMediaQualityType.h"
class IMp7JrsMediaQualityType_QualityRating_CollectionType;
typedef Dc1Ptr< IMp7JrsMediaQualityType_QualityRating_CollectionType > Mp7JrsMediaQualityType_QualityRating_CollectionPtr;
class IMp7JrsAgentType;
typedef Dc1Ptr< IMp7JrsAgentType > Mp7JrsAgentPtr;
class IMp7JrsMediaQualityType_RatingInformationLocator_CollectionType;
typedef Dc1Ptr< IMp7JrsMediaQualityType_RatingInformationLocator_CollectionType > Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr;
class IMp7JrsMediaQualityType_PerceptibleDefects_LocalType;
typedef Dc1Ptr< IMp7JrsMediaQualityType_PerceptibleDefects_LocalType > Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr;
#include "Mp7JrsDType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsExtendedMediaQualityType for class Mp7JrsExtendedMediaQualityType<br>
 * Located at: mpeg7-v4.xsd, line 6191<br>
 * Classified: Class<br>
 * Derived from: MediaQualityType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsExtendedMediaQualityType 
 :
		public IMp7JrsMediaQualityType
{
public:
	// TODO: make these protected?
	IMp7JrsExtendedMediaQualityType();
	virtual ~IMp7JrsExtendedMediaQualityType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get QCProfile element.
	 * @return Mp7JrsQCProfilePtr
	 */
	virtual Mp7JrsQCProfilePtr GetQCProfile() const = 0;

	virtual bool IsValidQCProfile() const = 0;

	/**
	 * Set QCProfile element.
	 * @param item const Mp7JrsQCProfilePtr &
	 */
	virtual void SetQCProfile(const Mp7JrsQCProfilePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate QCProfile element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateQCProfile() = 0;

	/**
	 * Get QCItemResult element.
	 * @return Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr
	 */
	virtual Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr GetQCItemResult() const = 0;

	/**
	 * Set QCItemResult element.
	 * @param item const Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetQCItemResult(const Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Operator element.
	 * @return Mp7JrsPersonPtr
	 */
	virtual Mp7JrsPersonPtr GetOperator() const = 0;

	virtual bool IsValidOperator() const = 0;

	/**
	 * Set Operator element.
	 * @param item const Mp7JrsPersonPtr &
	 */
	virtual void SetOperator(const Mp7JrsPersonPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Operator element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateOperator() = 0;

	/**
	 * Get UsedTool element.
	 * @return Mp7JrsCreationToolPtr
	 */
	virtual Mp7JrsCreationToolPtr GetUsedTool() const = 0;

	virtual bool IsValidUsedTool() const = 0;

	/**
	 * Set UsedTool element.
	 * @param item const Mp7JrsCreationToolPtr &
	 */
	virtual void SetUsedTool(const Mp7JrsCreationToolPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate UsedTool element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateUsedTool() = 0;

	/**
	 * Get Annotation element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetAnnotation() const = 0;

	virtual bool IsValidAnnotation() const = 0;

	/**
	 * Set Annotation element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetAnnotation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Annotation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateAnnotation() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get QualityRating element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_QualityRating_CollectionPtr
	 */
	virtual Mp7JrsMediaQualityType_QualityRating_CollectionPtr GetQualityRating() const = 0;

	/**
	 * Set QualityRating element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_QualityRating_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetQualityRating(const Mp7JrsMediaQualityType_QualityRating_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get RatingSource element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetRatingSource() const = 0;

	virtual bool IsValidRatingSource() const = 0;

	/**
	 * Set RatingSource element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * </ul>
	 */
	virtual void SetRatingSource(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate RatingSource element.
	 * (Inherited from Mp7JrsMediaQualityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRatingSource() = 0;

	/**
	 * Get RatingInformationLocator element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr
	 */
	virtual Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr GetRatingInformationLocator() const = 0;

	/**
	 * Set RatingInformationLocator element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRatingInformationLocator(const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PerceptibleDefects element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr
	 */
	virtual Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr GetPerceptibleDefects() const = 0;

	virtual bool IsValidPerceptibleDefects() const = 0;

	/**
	 * Set PerceptibleDefects element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr &
	 */
	virtual void SetPerceptibleDefects(const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PerceptibleDefects element.
	 * (Inherited from Mp7JrsMediaQualityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptibleDefects() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of ExtendedMediaQualityType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:QualityRating</li>
	 * <li>RatingSource</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RatingInformationLocator</li>
	 * <li>PerceptibleDefects</li>
	 * <li>QCProfile</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:QCItemResult</li>
	 * <li>Operator</li>
	 * <li>UsedTool</li>
	 * <li>Annotation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsExtendedMediaQualityType<br>
 * Located at: mpeg7-v4.xsd, line 6191<br>
 * Classified: Class<br>
 * Derived from: MediaQualityType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 6191.<br>
 */
class DC1_EXPORT Mp7JrsExtendedMediaQualityType :
		public IMp7JrsExtendedMediaQualityType
{
	friend class Mp7JrsExtendedMediaQualityTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsExtendedMediaQualityType
	 
	 * @return Dc1Ptr< Mp7JrsMediaQualityType >
	 */
	virtual Dc1Ptr< Mp7JrsMediaQualityType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get QCProfile element.
	 * @return Mp7JrsQCProfilePtr
	 */
	virtual Mp7JrsQCProfilePtr GetQCProfile() const;

	virtual bool IsValidQCProfile() const;

	/*
	 * Set QCProfile element.
	 * @param item const Mp7JrsQCProfilePtr &
	 */
	virtual void SetQCProfile(const Mp7JrsQCProfilePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate QCProfile element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateQCProfile();

	/*
	 * Get QCItemResult element.
	 * @return Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr
	 */
	virtual Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr GetQCItemResult() const;

	/*
	 * Set QCItemResult element.
	 * @param item const Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetQCItemResult(const Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Operator element.
	 * @return Mp7JrsPersonPtr
	 */
	virtual Mp7JrsPersonPtr GetOperator() const;

	virtual bool IsValidOperator() const;

	/*
	 * Set Operator element.
	 * @param item const Mp7JrsPersonPtr &
	 */
	virtual void SetOperator(const Mp7JrsPersonPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Operator element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateOperator();

	/*
	 * Get UsedTool element.
	 * @return Mp7JrsCreationToolPtr
	 */
	virtual Mp7JrsCreationToolPtr GetUsedTool() const;

	virtual bool IsValidUsedTool() const;

	/*
	 * Set UsedTool element.
	 * @param item const Mp7JrsCreationToolPtr &
	 */
	virtual void SetUsedTool(const Mp7JrsCreationToolPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate UsedTool element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateUsedTool();

	/*
	 * Get Annotation element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetAnnotation() const;

	virtual bool IsValidAnnotation() const;

	/*
	 * Set Annotation element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetAnnotation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Annotation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateAnnotation();

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get QualityRating element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_QualityRating_CollectionPtr
	 */
	virtual Mp7JrsMediaQualityType_QualityRating_CollectionPtr GetQualityRating() const;

	/*
	 * Set QualityRating element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_QualityRating_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetQualityRating(const Mp7JrsMediaQualityType_QualityRating_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get RatingSource element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetRatingSource() const;

	virtual bool IsValidRatingSource() const;

	/*
	 * Set RatingSource element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * </ul>
	 */
	virtual void SetRatingSource(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate RatingSource element.
	 * (Inherited from Mp7JrsMediaQualityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRatingSource();

	/*
	 * Get RatingInformationLocator element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr
	 */
	virtual Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr GetRatingInformationLocator() const;

	/*
	 * Set RatingInformationLocator element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRatingInformationLocator(const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PerceptibleDefects element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @return Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr
	 */
	virtual Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr GetPerceptibleDefects() const;

	virtual bool IsValidPerceptibleDefects() const;

	/*
	 * Set PerceptibleDefects element.
<br>
	 * (Inherited from Mp7JrsMediaQualityType)
	 * @param item const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr &
	 */
	virtual void SetPerceptibleDefects(const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PerceptibleDefects element.
	 * (Inherited from Mp7JrsMediaQualityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePerceptibleDefects();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of ExtendedMediaQualityType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:QualityRating</li>
	 * <li>RatingSource</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:RatingInformationLocator</li>
	 * <li>PerceptibleDefects</li>
	 * <li>QCProfile</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:QCItemResult</li>
	 * <li>Operator</li>
	 * <li>UsedTool</li>
	 * <li>Annotation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsExtendedMediaQualityType();

protected:
	Mp7JrsExtendedMediaQualityType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsMediaQualityType > m_Base;

	Mp7JrsQCProfilePtr m_QCProfile;
	bool m_QCProfile_Exist; // For optional elements 
	Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr m_QCItemResult;
	Mp7JrsPersonPtr m_Operator;
	bool m_Operator_Exist; // For optional elements 
	Mp7JrsCreationToolPtr m_UsedTool;
	bool m_UsedTool_Exist; // For optional elements 
	Mp7JrsTextAnnotationPtr m_Annotation;
	bool m_Annotation_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1893MP7JRSEXTENDEDMEDIAQUALITYTYPE_H

