/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2581MP7JRSMOVINGREGIONTYPE_LOCALTYPE_H
#define _2581MP7JRSMOVINGREGIONTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMovingRegionType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtInclude.h


class IMp7JrsMovingRegionType_LocalType;
typedef Dc1Ptr< IMp7JrsMovingRegionType_LocalType> Mp7JrsMovingRegionType_LocalPtr;
class IMp7JrsVisualDType;
typedef Dc1Ptr< IMp7JrsVisualDType > Mp7JrsVisualDPtr;
class IMp7JrsVisualDSType;
typedef Dc1Ptr< IMp7JrsVisualDSType > Mp7JrsVisualDSPtr;
class IMp7JrsVisualTimeSeriesType;
typedef Dc1Ptr< IMp7JrsVisualTimeSeriesType > Mp7JrsVisualTimeSeriesPtr;
class IMp7JrsGofGopFeatureType;
typedef Dc1Ptr< IMp7JrsGofGopFeatureType > Mp7JrsGofGopFeaturePtr;

/** 
 * Generated interface IMp7JrsMovingRegionType_LocalType for class Mp7JrsMovingRegionType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsMovingRegionType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsMovingRegionType_LocalType();
	virtual ~IMp7JrsMovingRegionType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get VisualDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualDescriptor() const = 0;

	/**
	 * Set VisualDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * </ul>
	 */
	virtual void SetVisualDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VisualDescriptionScheme element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualDescriptionScheme() const = 0;

	/**
	 * Set VisualDescriptionScheme element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * </ul>
	 */
	virtual void SetVisualDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VisualTimeSeriesDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsIrregularVisualTimeSeriesPtr</li>
	 * <li>IMp7JrsRegularVisualTimeSeriesPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualTimeSeriesDescriptor() const = 0;

	/**
	 * Set VisualTimeSeriesDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsIrregularVisualTimeSeriesPtr</li>
	 * <li>IMp7JrsRegularVisualTimeSeriesPtr</li>
	 * </ul>
	 */
	virtual void SetVisualTimeSeriesDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get GofGopFeature element.
	 * @return Mp7JrsGofGopFeaturePtr
	 */
	virtual Mp7JrsGofGopFeaturePtr GetGofGopFeature() const = 0;

	/**
	 * Set GofGopFeature element.
	 * @param item const Mp7JrsGofGopFeaturePtr &
	 */
	virtual void SetGofGopFeature(const Mp7JrsGofGopFeaturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMovingRegionType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 7269.<br>
 */
class DC1_EXPORT Mp7JrsMovingRegionType_LocalType :
		public IMp7JrsMovingRegionType_LocalType
{
	friend class Mp7JrsMovingRegionType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get VisualDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualDescriptor() const;

	/*
	 * Set VisualDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * </ul>
	 */
	virtual void SetVisualDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VisualDescriptionScheme element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualDescriptionScheme() const;

	/*
	 * Set VisualDescriptionScheme element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * </ul>
	 */
	virtual void SetVisualDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VisualTimeSeriesDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsIrregularVisualTimeSeriesPtr</li>
	 * <li>IMp7JrsRegularVisualTimeSeriesPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetVisualTimeSeriesDescriptor() const;

	/*
	 * Set VisualTimeSeriesDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsIrregularVisualTimeSeriesPtr</li>
	 * <li>IMp7JrsRegularVisualTimeSeriesPtr</li>
	 * </ul>
	 */
	virtual void SetVisualTimeSeriesDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get GofGopFeature element.
	 * @return Mp7JrsGofGopFeaturePtr
	 */
	virtual Mp7JrsGofGopFeaturePtr GetGofGopFeature() const;

	/*
	 * Set GofGopFeature element.
	 * @param item const Mp7JrsGofGopFeaturePtr &
	 */
	virtual void SetGofGopFeature(const Mp7JrsGofGopFeaturePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMovingRegionType_LocalType();

protected:
	Mp7JrsMovingRegionType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Dc1Ptr< Dc1Node > m_VisualDescriptor;
	Dc1Ptr< Dc1Node > m_VisualDescriptionScheme;
	Dc1Ptr< Dc1Node > m_VisualTimeSeriesDescriptor;
	Mp7JrsGofGopFeaturePtr m_GofGopFeature;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2581MP7JRSMOVINGREGIONTYPE_LOCALTYPE_H

