/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1951MP7JRSFRACTIONALPRESENCETYPE_H
#define _1951MP7JRSFRACTIONALPRESENCETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsFractionalPresenceTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtInclude.h


class IMp7JrsFractionalPresenceType;
typedef Dc1Ptr< IMp7JrsFractionalPresenceType> Mp7JrsFractionalPresencePtr;
class IMp7Jrsunsigned7;
typedef Dc1Ptr< IMp7Jrsunsigned7 > Mp7Jrsunsigned7Ptr;

/** 
 * Generated interface IMp7JrsFractionalPresenceType for class Mp7JrsFractionalPresenceType<br>
 * Located at: mpeg7-v4.xsd, line 1024<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsFractionalPresenceType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsFractionalPresenceType();
	virtual ~IMp7JrsFractionalPresenceType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get TrackLeft element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTrackLeft() const = 0;

	virtual bool IsValidTrackLeft() const = 0;

	/**
	 * Set TrackLeft element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTrackLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TrackLeft element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTrackLeft() = 0;

	/**
	 * Get TrackRight element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTrackRight() const = 0;

	virtual bool IsValidTrackRight() const = 0;

	/**
	 * Set TrackRight element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTrackRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TrackRight element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTrackRight() = 0;

	/**
	 * Get BoomDown element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetBoomDown() const = 0;

	virtual bool IsValidBoomDown() const = 0;

	/**
	 * Set BoomDown element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetBoomDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate BoomDown element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBoomDown() = 0;

	/**
	 * Get BoomUp element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetBoomUp() const = 0;

	virtual bool IsValidBoomUp() const = 0;

	/**
	 * Set BoomUp element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetBoomUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate BoomUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBoomUp() = 0;

	/**
	 * Get DollyForward element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetDollyForward() const = 0;

	virtual bool IsValidDollyForward() const = 0;

	/**
	 * Set DollyForward element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetDollyForward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DollyForward element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDollyForward() = 0;

	/**
	 * Get DollyBackward element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetDollyBackward() const = 0;

	virtual bool IsValidDollyBackward() const = 0;

	/**
	 * Set DollyBackward element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetDollyBackward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DollyBackward element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDollyBackward() = 0;

	/**
	 * Get PanLeft element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetPanLeft() const = 0;

	virtual bool IsValidPanLeft() const = 0;

	/**
	 * Set PanLeft element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetPanLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PanLeft element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePanLeft() = 0;

	/**
	 * Get PanRight element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetPanRight() const = 0;

	virtual bool IsValidPanRight() const = 0;

	/**
	 * Set PanRight element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetPanRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PanRight element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePanRight() = 0;

	/**
	 * Get TiltDown element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTiltDown() const = 0;

	virtual bool IsValidTiltDown() const = 0;

	/**
	 * Set TiltDown element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTiltDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TiltDown element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTiltDown() = 0;

	/**
	 * Get TiltUp element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTiltUp() const = 0;

	virtual bool IsValidTiltUp() const = 0;

	/**
	 * Set TiltUp element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTiltUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TiltUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTiltUp() = 0;

	/**
	 * Get RollClockwise element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetRollClockwise() const = 0;

	virtual bool IsValidRollClockwise() const = 0;

	/**
	 * Set RollClockwise element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetRollClockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate RollClockwise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRollClockwise() = 0;

	/**
	 * Get RollAnticlockwise element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetRollAnticlockwise() const = 0;

	virtual bool IsValidRollAnticlockwise() const = 0;

	/**
	 * Set RollAnticlockwise element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetRollAnticlockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate RollAnticlockwise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRollAnticlockwise() = 0;

	/**
	 * Get ZoomIn element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetZoomIn() const = 0;

	virtual bool IsValidZoomIn() const = 0;

	/**
	 * Set ZoomIn element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetZoomIn(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ZoomIn element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateZoomIn() = 0;

	/**
	 * Get ZoomOut element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetZoomOut() const = 0;

	virtual bool IsValidZoomOut() const = 0;

	/**
	 * Set ZoomOut element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetZoomOut(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate ZoomOut element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateZoomOut() = 0;

	/**
	 * Get Fixed element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetFixed() const = 0;

	virtual bool IsValidFixed() const = 0;

	/**
	 * Set Fixed element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetFixed(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Fixed element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFixed() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of FractionalPresenceType.
	 * Currently this type contains 15 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>TrackLeft</li>
	 * <li>TrackRight</li>
	 * <li>BoomDown</li>
	 * <li>BoomUp</li>
	 * <li>DollyForward</li>
	 * <li>DollyBackward</li>
	 * <li>PanLeft</li>
	 * <li>PanRight</li>
	 * <li>TiltDown</li>
	 * <li>TiltUp</li>
	 * <li>RollClockwise</li>
	 * <li>RollAnticlockwise</li>
	 * <li>ZoomIn</li>
	 * <li>ZoomOut</li>
	 * <li>Fixed</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsFractionalPresenceType<br>
 * Located at: mpeg7-v4.xsd, line 1024<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 1024.<br>
 */
class DC1_EXPORT Mp7JrsFractionalPresenceType :
		public IMp7JrsFractionalPresenceType
{
	friend class Mp7JrsFractionalPresenceTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get TrackLeft element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTrackLeft() const;

	virtual bool IsValidTrackLeft() const;

	/*
	 * Set TrackLeft element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTrackLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TrackLeft element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTrackLeft();

	/*
	 * Get TrackRight element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTrackRight() const;

	virtual bool IsValidTrackRight() const;

	/*
	 * Set TrackRight element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTrackRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TrackRight element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTrackRight();

	/*
	 * Get BoomDown element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetBoomDown() const;

	virtual bool IsValidBoomDown() const;

	/*
	 * Set BoomDown element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetBoomDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate BoomDown element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBoomDown();

	/*
	 * Get BoomUp element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetBoomUp() const;

	virtual bool IsValidBoomUp() const;

	/*
	 * Set BoomUp element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetBoomUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate BoomUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateBoomUp();

	/*
	 * Get DollyForward element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetDollyForward() const;

	virtual bool IsValidDollyForward() const;

	/*
	 * Set DollyForward element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetDollyForward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DollyForward element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDollyForward();

	/*
	 * Get DollyBackward element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetDollyBackward() const;

	virtual bool IsValidDollyBackward() const;

	/*
	 * Set DollyBackward element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetDollyBackward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DollyBackward element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDollyBackward();

	/*
	 * Get PanLeft element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetPanLeft() const;

	virtual bool IsValidPanLeft() const;

	/*
	 * Set PanLeft element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetPanLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PanLeft element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePanLeft();

	/*
	 * Get PanRight element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetPanRight() const;

	virtual bool IsValidPanRight() const;

	/*
	 * Set PanRight element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetPanRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PanRight element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePanRight();

	/*
	 * Get TiltDown element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTiltDown() const;

	virtual bool IsValidTiltDown() const;

	/*
	 * Set TiltDown element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTiltDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TiltDown element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTiltDown();

	/*
	 * Get TiltUp element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetTiltUp() const;

	virtual bool IsValidTiltUp() const;

	/*
	 * Set TiltUp element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetTiltUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TiltUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTiltUp();

	/*
	 * Get RollClockwise element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetRollClockwise() const;

	virtual bool IsValidRollClockwise() const;

	/*
	 * Set RollClockwise element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetRollClockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate RollClockwise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRollClockwise();

	/*
	 * Get RollAnticlockwise element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetRollAnticlockwise() const;

	virtual bool IsValidRollAnticlockwise() const;

	/*
	 * Set RollAnticlockwise element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetRollAnticlockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate RollAnticlockwise element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRollAnticlockwise();

	/*
	 * Get ZoomIn element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetZoomIn() const;

	virtual bool IsValidZoomIn() const;

	/*
	 * Set ZoomIn element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetZoomIn(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ZoomIn element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateZoomIn();

	/*
	 * Get ZoomOut element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetZoomOut() const;

	virtual bool IsValidZoomOut() const;

	/*
	 * Set ZoomOut element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetZoomOut(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate ZoomOut element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateZoomOut();

	/*
	 * Get Fixed element.
	 * @return Mp7Jrsunsigned7Ptr
	 */
	virtual Mp7Jrsunsigned7Ptr GetFixed() const;

	virtual bool IsValidFixed() const;

	/*
	 * Set Fixed element.
	 * @param item const Mp7Jrsunsigned7Ptr &
	 */
	virtual void SetFixed(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Fixed element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateFixed();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of FractionalPresenceType.
	 * Currently this type contains 15 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>TrackLeft</li>
	 * <li>TrackRight</li>
	 * <li>BoomDown</li>
	 * <li>BoomUp</li>
	 * <li>DollyForward</li>
	 * <li>DollyBackward</li>
	 * <li>PanLeft</li>
	 * <li>PanRight</li>
	 * <li>TiltDown</li>
	 * <li>TiltUp</li>
	 * <li>RollClockwise</li>
	 * <li>RollAnticlockwise</li>
	 * <li>ZoomIn</li>
	 * <li>ZoomOut</li>
	 * <li>Fixed</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsFractionalPresenceType();

protected:
	Mp7JrsFractionalPresenceType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	Mp7Jrsunsigned7Ptr m_TrackLeft;
	bool m_TrackLeft_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_TrackRight;
	bool m_TrackRight_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_BoomDown;
	bool m_BoomDown_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_BoomUp;
	bool m_BoomUp_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_DollyForward;
	bool m_DollyForward_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_DollyBackward;
	bool m_DollyBackward_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_PanLeft;
	bool m_PanLeft_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_PanRight;
	bool m_PanRight_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_TiltDown;
	bool m_TiltDown_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_TiltUp;
	bool m_TiltUp_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_RollClockwise;
	bool m_RollClockwise_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_RollAnticlockwise;
	bool m_RollAnticlockwise_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_ZoomIn;
	bool m_ZoomIn_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_ZoomOut;
	bool m_ZoomOut_Exist; // For optional elements 
	Mp7Jrsunsigned7Ptr m_Fixed;
	bool m_Fixed_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1951MP7JRSFRACTIONALPRESENCETYPE_H

