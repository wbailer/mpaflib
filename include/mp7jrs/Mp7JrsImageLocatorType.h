/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2079MP7JRSIMAGELOCATORTYPE_H
#define _2079MP7JRSIMAGELOCATORTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsImageLocatorTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtInclude.h


class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsImageLocatorType;
typedef Dc1Ptr< IMp7JrsImageLocatorType> Mp7JrsImageLocatorPtr;
class IMp7JrsmediaTimePointType;
typedef Dc1Ptr< IMp7JrsmediaTimePointType > Mp7JrsmediaTimePointPtr;
class IMp7JrsMediaRelTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelTimePointType > Mp7JrsMediaRelTimePointPtr;
class IMp7JrsMediaRelIncrTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelIncrTimePointType > Mp7JrsMediaRelIncrTimePointPtr;
class IMp7JrsImageLocatorType_BytePosition_LocalType;
typedef Dc1Ptr< IMp7JrsImageLocatorType_BytePosition_LocalType > Mp7JrsImageLocatorType_BytePosition_LocalPtr;
#include "Mp7JrsMediaLocatorType.h"
class IMp7JrsInlineMediaType;
typedef Dc1Ptr< IMp7JrsInlineMediaType > Mp7JrsInlineMediaPtr;

/** 
 * Generated interface IMp7JrsImageLocatorType for class Mp7JrsImageLocatorType<br>
 * Located at: mpeg7-v4.xsd, line 4515<br>
 * Classified: Class<br>
 * Derived from: MediaLocatorType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsImageLocatorType 
 :
		public IMp7JrsMediaLocatorType
{
public:
	// TODO: make these protected?
	IMp7JrsImageLocatorType();
	virtual ~IMp7JrsImageLocatorType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaTimePoint element.
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const = 0;

	/**
	 * Set MediaTimePoint element.
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelTimePoint element.
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const = 0;

	/**
	 * Set MediaRelTimePoint element.
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelIncrTimePoint element.
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const = 0;

	/**
	 * Set MediaRelIncrTimePoint element.
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get BytePosition element.
	 * @return Mp7JrsImageLocatorType_BytePosition_LocalPtr
	 */
	virtual Mp7JrsImageLocatorType_BytePosition_LocalPtr GetBytePosition() const = 0;

	/**
	 * Set BytePosition element.
	 * @param item const Mp7JrsImageLocatorType_BytePosition_LocalPtr &
	 */
	virtual void SetBytePosition(const Mp7JrsImageLocatorType_BytePosition_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaUri element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetMediaUri() const = 0;

	/**
	 * Set MediaUri element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item XMLCh *
	 */
	virtual void SetMediaUri(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get InlineMedia element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return Mp7JrsInlineMediaPtr
	 */
	virtual Mp7JrsInlineMediaPtr GetInlineMedia() const = 0;

	/**
	 * Set InlineMedia element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item const Mp7JrsInlineMediaPtr &
	 */
	virtual void SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get StreamID element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return unsigned
	 */
	virtual unsigned GetStreamID() const = 0;

	virtual bool IsValidStreamID() const = 0;

	/**
	 * Set StreamID element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item unsigned
	 */
	virtual void SetStreamID(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate StreamID element.
	 * (Inherited from Mp7JrsMediaLocatorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStreamID() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of ImageLocatorType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>InlineMedia</li>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>BytePosition</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// begin extension included
// file Mp7JrsImageLocatorType_ExtMethodDef.h
	virtual Dc1Ptr< Mp7JrsMediaLocatorType > GetBase() const = 0;
// end extension included


// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsImageLocatorType<br>
 * Located at: mpeg7-v4.xsd, line 4515<br>
 * Classified: Class<br>
 * Derived from: MediaLocatorType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 4515.<br>
 */
class DC1_EXPORT Mp7JrsImageLocatorType :
		public IMp7JrsImageLocatorType
{
	friend class Mp7JrsImageLocatorTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsImageLocatorType
	 
	 * @return Dc1Ptr< Mp7JrsMediaLocatorType >
	 */
	virtual Dc1Ptr< Mp7JrsMediaLocatorType > GetBase() const;
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaTimePoint element.
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const;

	/*
	 * Set MediaTimePoint element.
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelTimePoint element.
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const;

	/*
	 * Set MediaRelTimePoint element.
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelIncrTimePoint element.
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const;

	/*
	 * Set MediaRelIncrTimePoint element.
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get BytePosition element.
	 * @return Mp7JrsImageLocatorType_BytePosition_LocalPtr
	 */
	virtual Mp7JrsImageLocatorType_BytePosition_LocalPtr GetBytePosition() const;

	/*
	 * Set BytePosition element.
	 * @param item const Mp7JrsImageLocatorType_BytePosition_LocalPtr &
	 */
	virtual void SetBytePosition(const Mp7JrsImageLocatorType_BytePosition_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaUri element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetMediaUri() const;

	/*
	 * Set MediaUri element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item XMLCh *
	 */
	virtual void SetMediaUri(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get InlineMedia element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return Mp7JrsInlineMediaPtr
	 */
	virtual Mp7JrsInlineMediaPtr GetInlineMedia() const;

	/*
	 * Set InlineMedia element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item const Mp7JrsInlineMediaPtr &
	 */
	virtual void SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get StreamID element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @return unsigned
	 */
	virtual unsigned GetStreamID() const;

	virtual bool IsValidStreamID() const;

	/*
	 * Set StreamID element.
<br>
	 * (Inherited from Mp7JrsMediaLocatorType)
	 * @param item unsigned
	 */
	virtual void SetStreamID(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StreamID element.
	 * (Inherited from Mp7JrsMediaLocatorType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStreamID();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of ImageLocatorType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>InlineMedia</li>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>BytePosition</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// begin extension included
// file Mp7JrsImageLocatorType_ExtMyMethodDef.h
Mp7JrsmediaTimePointPtr GetMediaTimeBase(void) const;
void SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client = DC1_UNDEFINED_CLIENT);
// end extension included


public:

	virtual ~Mp7JrsImageLocatorType();

protected:
	Mp7JrsImageLocatorType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsMediaLocatorType > m_Base;

// DefinionChoice
	Mp7JrsmediaTimePointPtr m_MediaTimePoint;
	Mp7JrsMediaRelTimePointPtr m_MediaRelTimePoint;
	Mp7JrsMediaRelIncrTimePointPtr m_MediaRelIncrTimePoint;
	Mp7JrsImageLocatorType_BytePosition_LocalPtr m_BytePosition;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2079MP7JRSIMAGELOCATORTYPE_H

