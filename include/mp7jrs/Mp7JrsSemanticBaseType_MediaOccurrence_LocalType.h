/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3155MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_LOCALTYPE_H
#define _3155MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSemanticBaseType_MediaOccurrence_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtInclude.h


class IMp7JrsSemanticBaseType_MediaOccurrence_LocalType;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_LocalType> Mp7JrsSemanticBaseType_MediaOccurrence_LocalPtr;
class IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2 > Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr;
class IMp7JrsMediaInformationType;
typedef Dc1Ptr< IMp7JrsMediaInformationType > Mp7JrsMediaInformationPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsMaskType;
typedef Dc1Ptr< IMp7JrsMaskType > Mp7JrsMaskPtr;
class IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr;
class IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr;
class IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr;
class IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType;
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr;

/** 
 * Generated interface IMp7JrsSemanticBaseType_MediaOccurrence_LocalType for class Mp7JrsSemanticBaseType_MediaOccurrence_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 8138<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsSemanticBaseType_MediaOccurrence_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsSemanticBaseType_MediaOccurrence_LocalType();
	virtual ~IMp7JrsSemanticBaseType_MediaOccurrence_LocalType();
	/**
	 * Get type attribute.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr Gettype() const = 0;

	/**
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const = 0;

	/**
	 * Set type attribute.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr &
	 */
	virtual void Settype(const Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype() = 0;

		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaInformation element.
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const = 0;

	/**
	 * Set MediaInformation element.
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaInformationRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const = 0;

	/**
	 * Set MediaInformationRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaLocator element.
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const = 0;

	/**
	 * Set MediaLocator element.
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get Mask element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetMask() const = 0;

	virtual bool IsValidMask() const = 0;

	/**
	 * Set Mask element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * </ul>
	 */
	virtual void SetMask(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Mask element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMask() = 0;

	/**
	 * Get AudioDescriptor element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr GetAudioDescriptor() const = 0;

	/**
	 * Set AudioDescriptor element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAudioDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AudioDescriptionScheme element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr GetAudioDescriptionScheme() const = 0;

	/**
	 * Set AudioDescriptionScheme element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAudioDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VisualDescriptor element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr GetVisualDescriptor() const = 0;

	/**
	 * Set VisualDescriptor element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetVisualDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VisualDescriptionScheme element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr GetVisualDescriptionScheme() const = 0;

	/**
	 * Set VisualDescriptionScheme element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetVisualDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SemanticBaseType_MediaOccurrence_LocalType.
	 * Currently this type contains 8 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>Mask</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AudioDescriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:VisualDescriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSemanticBaseType_MediaOccurrence_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 8138<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 8138.<br>
 */
class DC1_EXPORT Mp7JrsSemanticBaseType_MediaOccurrence_LocalType :
		public IMp7JrsSemanticBaseType_MediaOccurrence_LocalType
{
	friend class Mp7JrsSemanticBaseType_MediaOccurrence_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get type attribute.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr Gettype() const;

	/*
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr &
	 */
	virtual void Settype(const Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaInformation element.
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const;

	/*
	 * Set MediaInformation element.
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaInformationRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const;

	/*
	 * Set MediaInformationRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaLocator element.
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get Mask element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetMask() const;

	virtual bool IsValidMask() const;

	/*
	 * Set Mask element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * </ul>
	 */
	virtual void SetMask(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mask element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMask();

	/*
	 * Get AudioDescriptor element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr GetAudioDescriptor() const;

	/*
	 * Set AudioDescriptor element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAudioDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AudioDescriptionScheme element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr GetAudioDescriptionScheme() const;

	/*
	 * Set AudioDescriptionScheme element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAudioDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VisualDescriptor element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr GetVisualDescriptor() const;

	/*
	 * Set VisualDescriptor element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetVisualDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VisualDescriptionScheme element.
	 * @return Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr
	 */
	virtual Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr GetVisualDescriptionScheme() const;

	/*
	 * Set VisualDescriptionScheme element.
	 * @param item const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetVisualDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SemanticBaseType_MediaOccurrence_LocalType.
	 * Currently this type contains 8 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>Mask</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AudioDescriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:VisualDescriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSemanticBaseType_MediaOccurrence_LocalType();

protected:
	Mp7JrsSemanticBaseType_MediaOccurrence_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr m_type;
	bool m_type_Exist;
	Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr m_type_Default;


// DefinionChoice
	Mp7JrsMediaInformationPtr m_MediaInformation;
	Mp7JrsReferencePtr m_MediaInformationRef;
	Mp7JrsMediaLocatorPtr m_MediaLocator;
// End DefinionChoice
	Dc1Ptr< Dc1Node > m_Mask;
	bool m_Mask_Exist; // For optional elements 
	Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr m_AudioDescriptor;
	Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr m_AudioDescriptionScheme;
	Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr m_VisualDescriptor;
	Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr m_VisualDescriptionScheme;

// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3155MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_LOCALTYPE_H

