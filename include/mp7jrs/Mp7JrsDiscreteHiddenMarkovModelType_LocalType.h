/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1785MP7JRSDISCRETEHIDDENMARKOVMODELTYPE_LOCALTYPE_H
#define _1785MP7JRSDISCRETEHIDDENMARKOVMODELTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsDiscreteHiddenMarkovModelType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtInclude.h


class IMp7JrsDiscreteHiddenMarkovModelType_LocalType;
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType_LocalType> Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr;
class IMp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType;
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType > Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr;
class IMp7JrsDiscreteDistributionType;
typedef Dc1Ptr< IMp7JrsDiscreteDistributionType > Mp7JrsDiscreteDistributionPtr;

/** 
 * Generated interface IMp7JrsDiscreteHiddenMarkovModelType_LocalType for class Mp7JrsDiscreteHiddenMarkovModelType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsDiscreteHiddenMarkovModelType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsDiscreteHiddenMarkovModelType_LocalType();
	virtual ~IMp7JrsDiscreteHiddenMarkovModelType_LocalType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get NumOfObservationsPerState element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfObservationsPerState() const = 0;

	/**
	 * Set NumOfObservationsPerState element.
	 * @param item unsigned
	 */
	// Mandatory			
	virtual void SetNumOfObservationsPerState(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Observation element.
	 * @return Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr
	 */
	virtual Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr GetObservation() const = 0;

	/**
	 * Set Observation element.
	 * @param item const Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetObservation(const Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ObservationDistribution element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetObservationDistribution() const = 0;

	/**
	 * Set ObservationDistribution element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsBinomialDistributionPtr
	// Mp7JrsDiscreteUniformDistributionPtr
	// Mp7JrsGeometricDistributionPtr
	// Mp7JrsHistogramProbabilityPtr
	// Mp7JrsHyperGeometricDistributionPtr
	// Mp7JrsPoissonDistributionPtr
	virtual void SetObservationDistribution(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsDiscreteHiddenMarkovModelType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 9527.<br>
 */
class DC1_EXPORT Mp7JrsDiscreteHiddenMarkovModelType_LocalType :
		public IMp7JrsDiscreteHiddenMarkovModelType_LocalType
{
	friend class Mp7JrsDiscreteHiddenMarkovModelType_LocalTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get NumOfObservationsPerState element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfObservationsPerState() const;

	/*
	 * Set NumOfObservationsPerState element.
	 * @param item unsigned
	 */
	// Mandatory			
	virtual void SetNumOfObservationsPerState(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Observation element.
	 * @return Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr
	 */
	virtual Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr GetObservation() const;

	/*
	 * Set Observation element.
	 * @param item const Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetObservation(const Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ObservationDistribution element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetObservationDistribution() const;

	/*
	 * Set ObservationDistribution element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsBinomialDistributionPtr
	// Mp7JrsDiscreteUniformDistributionPtr
	// Mp7JrsGeometricDistributionPtr
	// Mp7JrsHistogramProbabilityPtr
	// Mp7JrsHyperGeometricDistributionPtr
	// Mp7JrsPoissonDistributionPtr
	virtual void SetObservationDistribution(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsDiscreteHiddenMarkovModelType_LocalType();

protected:
	Mp7JrsDiscreteHiddenMarkovModelType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	unsigned m_NumOfObservationsPerState;
	Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr m_Observation;
	Dc1Ptr< Dc1Node > m_ObservationDistribution;

// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1785MP7JRSDISCRETEHIDDENMARKOVMODELTYPE_LOCALTYPE_H

