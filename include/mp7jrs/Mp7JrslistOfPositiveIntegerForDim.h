/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ValCollectionType_H

#ifndef _2259MP7JRSLISTOFPOSITIVEINTEGERFORDIM_H
#define _2259MP7JRSLISTOFPOSITIVEINTEGERFORDIM_H



#include "Dc1Defines.h"
#include "Mp7JrslistOfPositiveIntegerForDimFactory.h"
#include "Dc1Node.h"
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include "Dc1ValueVectorOf.h"

// no includefile for extension defined 
// file Mp7JrslistOfPositiveIntegerForDim_ExtInclude.h


class IMp7JrslistOfPositiveIntegerForDim;
typedef Dc1Ptr< IMp7JrslistOfPositiveIntegerForDim> Mp7JrslistOfPositiveIntegerForDimPtr;


class Mp7JrslistOfPositiveIntegerForDim;

/**
 * Generated interface IMp7JrslistOfPositiveIntegerForDim for collection Mp7JrslistOfPositiveIntegerForDim<br>
 * Located at: mpeg7-v4.xsd, line 153<br>
 * Item type: positiveInteger (Value)<br>
 */
class DC1_EXPORT IMp7JrslistOfPositiveIntegerForDim :
	public Dc1Node
{
public:

	/** Initializes the collection.
	  * @param maxElems Number of elements for which memory is initially allocated.
	  * @param adoptElems if true, the collection is responsible for deallocating the memory used by its elements. if false, the application has to deallocate the collection elements.
	  */
	virtual void Initialize(unsigned int maxElems) = 0;

	/** Adds a new element to the collection.
	  * @param toAdd Element to be added.
	  */
	virtual void addElement(const unsigned & toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Sets the element at the given position in the collection.
	  * @param toSet New value of the element.
	  * @param setAt Position of the element which shall be set.
	  */
	virtual void setElementAt(const unsigned & toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Inserts the element at the given position in the collection.
	  * @param toInsert Element to be inserted.
	  * @param insertAt Position where the element will be inserted.
	  */
	virtual void insertElementAt(const unsigned & toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes all elements from the collection.
	  */
	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes the specified element from the collection.
	  * @param removeAt Position of the element to be removed. 
	  */
	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Check whether the collection contains the given element.
	  * @param toCheck Element to be checked for.
	  * @return True if the element is in the collection, false otherwise.
	  */
	virtual bool containsElement(const unsigned &toCheck) = 0;

	/** Get the current capacity of the collection.
	  * @return The number of elements for which memory has been allocated.
	  */
	virtual unsigned int curCapacity() const = 0;

	/** Gets the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return the given element.
	  */
	virtual const unsigned elementAt(unsigned int getAt) const = 0;

	/** Gets the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return The given element.
	  */
	virtual unsigned elementAt(unsigned int getAt) = 0;

	/** Get the size of the collection.
	  * @return The current number of elements in the collection.
	  */
	virtual unsigned int size() const = 0;

	/** Allocate additional memory for the speciifed number of elements.
	  * @param length Number of elements to allcate memory for.
	  */
	virtual void ensureExtraCapacity(unsigned int length) = 0;

	// JRS: addition to Xerces collection
	/** Get the position of the specified element. 
	  * @param toCheck Element to be found in the collection.
	  * @return The position of the specified element or -1 if the element has not been found.
	  */
	virtual int elementIndexOf(const unsigned &toCheck) const = 0;

};


class DC1_EXPORT Mp7JrslistOfPositiveIntegerForDim :
	public IMp7JrslistOfPositiveIntegerForDim
{
	friend class Mp7JrslistOfPositiveIntegerForDimFactory;

public:
	virtual void Initialize(unsigned int maxElems);

	virtual void addElement(const unsigned & toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void setElementAt(const unsigned & toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void insertElementAt(const unsigned & toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool containsElement(const unsigned &toCheck);

	virtual unsigned int curCapacity() const;

	virtual const unsigned elementAt(unsigned int getAt) const;

	virtual unsigned elementAt(unsigned int getAt);

	virtual unsigned int size() const;

	virtual void ensureExtraCapacity(unsigned int length);

	/* Get the underlying array of the collection.
	  */
	virtual const unsigned * rawData() const;

	virtual int elementIndexOf(const unsigned &toCheck) const;

	/* Get the class name.
	 * @return The name of this class.
     */
	virtual XMLCh * ToText() const;
 
	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

    /* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
     * @param txt buffer to parse from.
     * @return true if parsing was successful, false otherwise.
     */
	virtual bool ContentFromString(const XMLCh * const txt);

    /* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
     * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
     */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file Mp7JrslistOfPositiveIntegerForDim_ExtMethodDef.h


	virtual ~Mp7JrslistOfPositiveIntegerForDim();

protected:
	Mp7JrslistOfPositiveIntegerForDim();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

private:
	Dc1ValueVectorOf <unsigned> * m_Item;

// no includefile for extension defined 
// file Mp7JrslistOfPositiveIntegerForDim_ExtPropDef.h


};

#endif // _2259MP7JRSLISTOFPOSITIVEINTEGERFORDIM_H
