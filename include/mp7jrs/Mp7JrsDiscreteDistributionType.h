/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1779MP7JRSDISCRETEDISTRIBUTIONTYPE_H
#define _1779MP7JRSDISCRETEDISTRIBUTIONTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsDiscreteDistributionTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtInclude.h


class IMp7JrsProbabilityDistributionType;
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType > Mp7JrsProbabilityDistributionPtr;
class IMp7JrsDiscreteDistributionType;
typedef Dc1Ptr< IMp7JrsDiscreteDistributionType> Mp7JrsDiscreteDistributionPtr;
#include "Mp7JrsProbabilityDistributionType.h"
class IMp7JrsDoubleMatrixType;
typedef Dc1Ptr< IMp7JrsDoubleMatrixType > Mp7JrsDoubleMatrixPtr;
class IMp7JrsProbabilityDistributionType_Moment_CollectionType;
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Moment_CollectionType > Mp7JrsProbabilityDistributionType_Moment_CollectionPtr;
class IMp7JrsProbabilityDistributionType_Cumulant_CollectionType;
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Cumulant_CollectionType > Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr;
#include "Mp7JrsProbabilityModelType.h"
#include "Mp7JrsModelType.h"
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsDiscreteDistributionType for class Mp7JrsDiscreteDistributionType<br>
 * Located at: mpeg7-v4.xsd, line 9344<br>
 * Classified: Class<br>
 * Derived from: ProbabilityDistributionType (Class)<br>
 * Abstract class <br>
 */
class MP7JRS_EXPORT IMp7JrsDiscreteDistributionType 
 :
		public IMp7JrsProbabilityDistributionType
{
public:
	// TODO: make these protected?
	IMp7JrsDiscreteDistributionType();
	virtual ~IMp7JrsDiscreteDistributionType();
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return unsigned
	 * @endif
	 */
	virtual unsigned Getdim() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get dim attribute validity information.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existdim() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item unsigned
	 * @endif
	 */
	virtual void Setdim(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate dim attribute.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidatedim() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMean() const = 0;

	virtual bool IsValidMean() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMean() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetVariance() const = 0;

	virtual bool IsValidVariance() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateVariance() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMin() const = 0;

	virtual bool IsValidMin() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMin() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMax() const = 0;

	virtual bool IsValidMax() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMax() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMode() const = 0;

	virtual bool IsValidMode() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Mode element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMode() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 * @endif
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMedian() const = 0;

	virtual bool IsValidMedian() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 * @endif
	 */
	virtual void SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate Median element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMedian() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Moment_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsProbabilityDistributionType_Moment_CollectionPtr GetMoment() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr GetCumulant() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 * @endif
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existconfidence() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 * @endif
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidateconfidence() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 * @endif
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existreliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 * @endif
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidatereliability() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 * @endif
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 * @endif
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidateid() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 * @endif
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 * @endif
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 * @endif
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 * @endif
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of DiscreteDistributionType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Mean</li>
	 * <li>Variance</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mode</li>
	 * <li>Median</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Moment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Cumulant</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsDiscreteDistributionType<br>
 * Located at: mpeg7-v4.xsd, line 9344<br>
 * Classified: Class<br>
 * Derived from: ProbabilityDistributionType (Class)<br>
 * Abstract class <br>
 * Defined in mpeg7-v4.xsd, line 9344.<br>
 */
class DC1_EXPORT Mp7JrsDiscreteDistributionType :
		public IMp7JrsDiscreteDistributionType
{
	friend class Mp7JrsDiscreteDistributionTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsDiscreteDistributionType
	 
	 * @return Dc1Ptr< Mp7JrsProbabilityDistributionType >
	 */
	virtual Dc1Ptr< Mp7JrsProbabilityDistributionType > GetBase() const;
	/*
	 * Get dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return unsigned
	 */
	virtual unsigned Getdim() const;

	/*
	 * Get dim attribute validity information.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdim() const;

	/*
	 * Set dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item unsigned
	 */
	virtual void Setdim(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dim attribute.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedim();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMean() const;

	virtual bool IsValidMean() const;

	/*
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMean();

	/*
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetVariance() const;

	virtual bool IsValidVariance() const;

	/*
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVariance();

	/*
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMin() const;

	virtual bool IsValidMin() const;

	/*
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMin();

	/*
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMax() const;

	virtual bool IsValidMax() const;

	/*
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMax();

	/*
	 * Get Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMode() const;

	virtual bool IsValidMode() const;

	/*
	 * Set Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mode element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMode();

	/*
	 * Get Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMedian() const;

	virtual bool IsValidMedian() const;

	/*
	 * Set Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Median element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMedian();

	/*
	 * Get Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Moment_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Moment_CollectionPtr GetMoment() const;

	/*
	 * Set Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr GetCumulant() const;

	/*
	 * Set Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const;

	/*
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existconfidence() const;

	/*
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateconfidence();

	/*
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const;

	/*
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existreliability() const;

	/*
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatereliability();

	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of DiscreteDistributionType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Mean</li>
	 * <li>Variance</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mode</li>
	 * <li>Median</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Moment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Cumulant</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsDiscreteDistributionType();

protected:
	Mp7JrsDiscreteDistributionType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsProbabilityDistributionType > m_Base;


// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1779MP7JRSDISCRETEDISTRIBUTIONTYPE_H

