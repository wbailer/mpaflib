/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3621MP7JRSSYNTACTICCONSTITUENTTYPE_H
#define _3621MP7JRSSYNTACTICCONSTITUENTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSyntacticConstituentTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtInclude.h


class IMp7JrsLinguisticEntityType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType > Mp7JrsLinguisticEntityPtr;
class IMp7JrsSyntacticConstituentType;
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType> Mp7JrsSyntacticConstituentPtr;
#include "Mp7JrssynthesisType.h"
class IMp7JrsSyntacticConstituentType_CollectionType;
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType_CollectionType > Mp7JrsSyntacticConstituentType_CollectionPtr;
#include "Mp7JrsLinguisticEntityType.h"
class IMp7JrsLinguisticEntityType_start_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_start_LocalType > Mp7JrsLinguisticEntityType_start_LocalPtr;
class IMp7JrsLinguisticEntityType_length_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_length_LocalType > Mp7JrsLinguisticEntityType_length_LocalPtr;
class IMp7JrstermReferenceType;
typedef Dc1Ptr< IMp7JrstermReferenceType > Mp7JrstermReferencePtr;
class IMp7JrstermReferenceListType;
typedef Dc1Ptr< IMp7JrstermReferenceListType > Mp7JrstermReferenceListPtr;
class IMp7JrsLinguisticEntityType_edit_LocalType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_edit_LocalType > Mp7JrsLinguisticEntityType_edit_LocalPtr;
class IMp7JrsLinguisticEntityType_MediaLocator_CollectionType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_MediaLocator_CollectionType > Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr;
class IMp7JrsLinguisticEntityType_Relation_CollectionType;
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_Relation_CollectionType > Mp7JrsLinguisticEntityType_Relation_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSyntacticConstituentType for class Mp7JrsSyntacticConstituentType<br>
 * Located at: mpeg7-v4.xsd, line 5782<br>
 * Classified: Class<br>
 * Derived from: LinguisticEntityType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSyntacticConstituentType 
 :
		public IMp7JrsLinguisticEntityType
{
public:
	// TODO: make these protected?
	IMp7JrsSyntacticConstituentType();
	virtual ~IMp7JrsSyntacticConstituentType();
	/**
	 * Get baseForm attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetbaseForm() const = 0;

	/**
	 * Get baseForm attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbaseForm() const = 0;

	/**
	 * Set baseForm attribute.
	 * @param item XMLCh *
	 */
	virtual void SetbaseForm(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate baseForm attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebaseForm() = 0;

	/**
	 * Get synthesis attribute.
	 * @return Mp7JrssynthesisType::Enumeration
	 */
	virtual Mp7JrssynthesisType::Enumeration Getsynthesis() const = 0;

	/**
	 * Get synthesis attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsynthesis() const = 0;

	/**
	 * Set synthesis attribute.
	 * @param item Mp7JrssynthesisType::Enumeration
	 */
	virtual void Setsynthesis(Mp7JrssynthesisType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate synthesis attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesynthesis() = 0;

	/**
	 * Get functionWord attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfunctionWord() const = 0;

	/**
	 * Get functionWord attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfunctionWord() const = 0;

	/**
	 * Set functionWord attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfunctionWord(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate functionWord attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefunctionWord() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get SyntacticConstituentType_LocalType element.
	 * @return Mp7JrsSyntacticConstituentType_CollectionPtr
	 */
	virtual Mp7JrsSyntacticConstituentType_CollectionPtr GetSyntacticConstituentType_LocalType() const = 0;

	/**
	 * Set SyntacticConstituentType_LocalType element.
	 * @param item const Mp7JrsSyntacticConstituentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSyntacticConstituentType_LocalType(const Mp7JrsSyntacticConstituentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

	/**
	 * Get start attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_start_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_start_LocalPtr Getstart() const = 0;

	/**
	 * Get start attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const = 0;

	/**
	 * Set start attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_start_LocalPtr &
	 */
	virtual void Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get length attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_length_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_length_LocalPtr Getlength() const = 0;

	/**
	 * Get length attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const = 0;

	/**
	 * Set length attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_length_LocalPtr &
	 */
	virtual void Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate length attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength() = 0;

	/**
	 * Get type attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Gettype() const = 0;

	/**
	 * Get type attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const = 0;

	/**
	 * Set type attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate type attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype() = 0;

	/**
	 * Get depend attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getdepend() const = 0;

	/**
	 * Get depend attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdepend() const = 0;

	/**
	 * Set depend attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate depend attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepend() = 0;

	/**
	 * Get equal attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getequal() const = 0;

	/**
	 * Get equal attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const = 0;

	/**
	 * Set equal attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate equal attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal() = 0;

	/**
	 * Get semantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getsemantics() const = 0;

	/**
	 * Get semantics attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsemantics() const = 0;

	/**
	 * Set semantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate semantics attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesemantics() = 0;

	/**
	 * Get compoundSemantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetcompoundSemantics() const = 0;

	/**
	 * Get compoundSemantics attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcompoundSemantics() const = 0;

	/**
	 * Set compoundSemantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate compoundSemantics attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecompoundSemantics() = 0;

	/**
	 * Get operator attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getoperator() const = 0;

	/**
	 * Get operator attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existoperator() const = 0;

	/**
	 * Set operator attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate operator attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoperator() = 0;

	/**
	 * Get copy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getcopy() const = 0;

	/**
	 * Get copy attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcopy() const = 0;

	/**
	 * Set copy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate copy attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecopy() = 0;

	/**
	 * Get noCopy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetnoCopy() const = 0;

	/**
	 * Get noCopy attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoCopy() const = 0;

	/**
	 * Set noCopy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noCopy attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoCopy() = 0;

	/**
	 * Get substitute attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getsubstitute() const = 0;

	/**
	 * Get substitute attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsubstitute() const = 0;

	/**
	 * Set substitute attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate substitute attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesubstitute() = 0;

	/**
	 * Get inScope attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr GetinScope() const = 0;

	/**
	 * Get inScope attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistinScope() const = 0;

	/**
	 * Set inScope attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate inScope attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateinScope() = 0;

	/**
	 * Get edit attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_edit_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_edit_LocalPtr Getedit() const = 0;

	/**
	 * Get edit attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existedit() const = 0;

	/**
	 * Set edit attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_edit_LocalPtr &
	 */
	virtual void Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate edit attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateedit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr GetMediaLocator() const = 0;

	/**
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_Relation_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_Relation_CollectionPtr GetRelation() const = 0;

	/**
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SyntacticConstituentType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaLocator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>Head</li>
	 * <li>Phrase</li>
	 * <li>Quotation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSyntacticConstituentType<br>
 * Located at: mpeg7-v4.xsd, line 5782<br>
 * Classified: Class<br>
 * Derived from: LinguisticEntityType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 5782.<br>
 */
class DC1_EXPORT Mp7JrsSyntacticConstituentType :
		public IMp7JrsSyntacticConstituentType
{
	friend class Mp7JrsSyntacticConstituentTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSyntacticConstituentType
	 
	 * @return Dc1Ptr< Mp7JrsLinguisticEntityType >
	 */
	virtual Dc1Ptr< Mp7JrsLinguisticEntityType > GetBase() const;
	/*
	 * Get baseForm attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetbaseForm() const;

	/*
	 * Get baseForm attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbaseForm() const;

	/*
	 * Set baseForm attribute.
	 * @param item XMLCh *
	 */
	virtual void SetbaseForm(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate baseForm attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebaseForm();

	/*
	 * Get synthesis attribute.
	 * @return Mp7JrssynthesisType::Enumeration
	 */
	virtual Mp7JrssynthesisType::Enumeration Getsynthesis() const;

	/*
	 * Get synthesis attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsynthesis() const;

	/*
	 * Set synthesis attribute.
	 * @param item Mp7JrssynthesisType::Enumeration
	 */
	virtual void Setsynthesis(Mp7JrssynthesisType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate synthesis attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesynthesis();

	/*
	 * Get functionWord attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfunctionWord() const;

	/*
	 * Get functionWord attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfunctionWord() const;

	/*
	 * Set functionWord attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfunctionWord(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate functionWord attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefunctionWord();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get SyntacticConstituentType_LocalType element.
	 * @return Mp7JrsSyntacticConstituentType_CollectionPtr
	 */
	virtual Mp7JrsSyntacticConstituentType_CollectionPtr GetSyntacticConstituentType_LocalType() const;

	/*
	 * Set SyntacticConstituentType_LocalType element.
	 * @param item const Mp7JrsSyntacticConstituentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSyntacticConstituentType_LocalType(const Mp7JrsSyntacticConstituentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

	/*
	 * Get start attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_start_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_start_LocalPtr Getstart() const;

	/*
	 * Get start attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const;

	/*
	 * Set start attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_start_LocalPtr &
	 */
	virtual void Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get length attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_length_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_length_LocalPtr Getlength() const;

	/*
	 * Get length attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const;

	/*
	 * Set length attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_length_LocalPtr &
	 */
	virtual void Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate length attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength();

	/*
	 * Get type attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Gettype() const;

	/*
	 * Get type attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

	/*
	 * Get depend attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getdepend() const;

	/*
	 * Get depend attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdepend() const;

	/*
	 * Set depend attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate depend attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepend();

	/*
	 * Get equal attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getequal() const;

	/*
	 * Get equal attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existequal() const;

	/*
	 * Set equal attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate equal attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequal();

	/*
	 * Get semantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getsemantics() const;

	/*
	 * Get semantics attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsemantics() const;

	/*
	 * Set semantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate semantics attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesemantics();

	/*
	 * Get compoundSemantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetcompoundSemantics() const;

	/*
	 * Get compoundSemantics attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcompoundSemantics() const;

	/*
	 * Set compoundSemantics attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate compoundSemantics attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecompoundSemantics();

	/*
	 * Get operator attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getoperator() const;

	/*
	 * Get operator attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existoperator() const;

	/*
	 * Set operator attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate operator attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoperator();

	/*
	 * Get copy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr Getcopy() const;

	/*
	 * Get copy attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcopy() const;

	/*
	 * Set copy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate copy attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecopy();

	/*
	 * Get noCopy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferenceListPtr
	 */
	virtual Mp7JrstermReferenceListPtr GetnoCopy() const;

	/*
	 * Get noCopy attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnoCopy() const;

	/*
	 * Set noCopy attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferenceListPtr &
	 */
	virtual void SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noCopy attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoCopy();

	/*
	 * Get substitute attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getsubstitute() const;

	/*
	 * Get substitute attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsubstitute() const;

	/*
	 * Set substitute attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate substitute attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesubstitute();

	/*
	 * Get inScope attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr GetinScope() const;

	/*
	 * Get inScope attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistinScope() const;

	/*
	 * Set inScope attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrstermReferencePtr &
	 */
	virtual void SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate inScope attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateinScope();

	/*
	 * Get edit attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_edit_LocalPtr
	 */
	virtual Mp7JrsLinguisticEntityType_edit_LocalPtr Getedit() const;

	/*
	 * Get edit attribute validity information.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existedit() const;

	/*
	 * Set edit attribute.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_edit_LocalPtr &
	 */
	virtual void Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate edit attribute.
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateedit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @return Mp7JrsLinguisticEntityType_Relation_CollectionPtr
	 */
	virtual Mp7JrsLinguisticEntityType_Relation_CollectionPtr GetRelation() const;

	/*
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsLinguisticEntityType)
	 * @param item const Mp7JrsLinguisticEntityType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SyntacticConstituentType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MediaLocator</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>Head</li>
	 * <li>Phrase</li>
	 * <li>Quotation</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSyntacticConstituentType();

protected:
	Mp7JrsSyntacticConstituentType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_baseForm;
	bool m_baseForm_Exist;
	Mp7JrssynthesisType::Enumeration m_synthesis;
	bool m_synthesis_Exist;
	Mp7JrssynthesisType::Enumeration m_synthesis_Default;
	XMLCh * m_functionWord;
	bool m_functionWord_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsLinguisticEntityType > m_Base;

	Mp7JrsSyntacticConstituentType_CollectionPtr m_SyntacticConstituentType_LocalType;

// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3621MP7JRSSYNTACTICCONSTITUENTTYPE_H

