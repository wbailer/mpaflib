/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// NodeRefCollectionType_H.template
#ifndef _1769MP7JRSDESCRIPTORCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPE_H
#define _1769MP7JRSDESCRIPTORCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"



#include "Mp7JrsDescriptorCollectionType_Descriptor_CollectionTypeFactory.h"
#include "Dc1NodeCollection.h"
#include "Dc1ValueVectorOf.h"
#include "Dc1Enumerator.h"
#include "Dc1FactoryDefines.h"


// no includefile for extension defined 
// file Mp7JrsDescriptorCollectionType_Descriptor_CollectionType_ExtInclude.h


class IMp7JrsDescriptorCollectionType_Descriptor_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptorCollectionType_Descriptor_CollectionType> Mp7JrsDescriptorCollectionType_Descriptor_CollectionPtr;

class Dc1Node;
typedef Dc1Enumerator< Dc1Ptr<Dc1Node> > Dc1NodeEnum;

class Mp7JrsDescriptorCollectionType_Descriptor_CollectionType;

/**
 * Generated interface IMp7JrsDescriptorCollectionType_Descriptor_CollectionType for collection Mp7JrsDescriptorCollectionType_Descriptor_CollectionType<br>
 * Located at: mpeg7-v4.xsd, line 9181<br>
 * Abstract item type: DType<br>
 * Possible classes:<br>
 * \li AdvancedFaceRecognitionType (Class)<br>
 * \li AudioBPMType (Class)<br>
 * \li AudioFundamentalFrequencyType (Class)<br>
 * \li AudioHarmonicityType (Class)<br>
 * \li AudioPowerType (Class)<br>
 * \li AudioSpectrumBasisType (Class)<br>
 * \li AudioSpectrumCentroidType (Class)<br>
 * \li AudioSpectrumEnvelopeType (Class)<br>
 * \li AudioSpectrumFlatnessType (Class)<br>
 * \li AudioSpectrumProjectionType (Class)<br>
 * \li AudioSpectrumSpreadType (Class)<br>
 * \li AudioSummaryComponentType (Class)<br>
 * \li AudioWaveformType (Class)<br>
 * \li BackgroundNoiseLevelType (Class)<br>
 * \li BalanceType (Class)<br>
 * \li BandwidthType (Class)<br>
 * \li BrowsingPreferencesType_PreferenceCondition_LocalType (Class)<br>
 * \li CameraMotionType (Class)<br>
 * \li ChordBaseType (Class)<br>
 * \li ClassificationPerceptualAttributeType (Class)<br>
 * \li ColorLayoutType (Class)<br>
 * \li ColorSamplingType (Class)<br>
 * \li ColorStructureType (Class)<br>
 * \li ColorTemperatureType (Class)<br>
 * \li ContourShapeType (Class)<br>
 * \li CrossChannelCorrelationType (Class)<br>
 * \li DcOffsetType (Class)<br>
 * \li DistributionPerceptualAttributeType (Class)<br>
 * \li DominantColorType (Class)<br>
 * \li EdgeHistogramType (Class)<br>
 * \li ExtendedMediaQualityType (Class)<br>
 * \li FaceRecognitionType (Class)<br>
 * \li GoFGoPColorType (Class)<br>
 * \li HarmonicSpectralCentroidType (Class)<br>
 * \li HarmonicSpectralDeviationType (Class)<br>
 * \li HarmonicSpectralSpreadType (Class)<br>
 * \li HarmonicSpectralVariationType (Class)<br>
 * \li HomogeneousTextureType (Class)<br>
 * \li ImageSignatureType (Class)<br>
 * \li LinearPerceptualAttributeType (Class)<br>
 * \li LogAttackTimeType (Class)<br>
 * \li MatchingHintType (Class)<br>
 * \li MediaFormatType (Class)<br>
 * \li MediaIdentificationType (Class)<br>
 * \li MediaQualityType (Class)<br>
 * \li MediaSpaceMaskType (Class)<br>
 * \li MediaTranscodingHintsType (Class)<br>
 * \li MeterType (Class)<br>
 * \li MotionActivityType (Class)<br>
 * \li MotionTrajectoryType (Class)<br>
 * \li OrderedGroupDataSetMaskType (Class)<br>
 * \li ParametricMotionType (Class)<br>
 * \li Perceptual3DShapeType (Class)<br>
 * \li PerceptualBeatType (Class)<br>
 * \li PerceptualEnergyType (Class)<br>
 * \li PerceptualGenreDistributionElementType (Class)<br>
 * \li PerceptualGenreDistributionType (Class)<br>
 * \li PerceptualInstrumentationDistributionElementType (Class)<br>
 * \li PerceptualInstrumentationDistributionType (Class)<br>
 * \li PerceptualLanguageType (Class)<br>
 * \li PerceptualLyricsDistributionElementType (Class)<br>
 * \li PerceptualLyricsDistributionType (Class)<br>
 * \li PerceptualMoodDistributionElementType (Class)<br>
 * \li PerceptualMoodDistributionType (Class)<br>
 * \li PerceptualRecordingType (Class)<br>
 * \li PerceptualSoundType (Class)<br>
 * \li PerceptualTempoType (Class)<br>
 * \li PerceptualValenceType (Class)<br>
 * \li PerceptualVocalsType (Class)<br>
 * \li PointOfViewType (Class)<br>
 * \li PreferenceConditionType (Class)<br>
 * \li ReferencePitchType (Class)<br>
 * \li RegionShapeType (Class)<br>
 * \li RelativeDelayType (Class)<br>
 * \li RhythmicBaseType (Class)<br>
 * \li ScalableColorType (Class)<br>
 * \li SceneGraphMaskType (Class)<br>
 * \li Shape3DType (Class)<br>
 * \li ShapeVariationType (Class)<br>
 * \li SilenceType (Class)<br>
 * \li SoundModelStateHistogramType (Class)<br>
 * \li SourcePreferencesType_MediaFormat_LocalType (Class)<br>
 * \li SpatialMaskType (Class)<br>
 * \li SpatioTemporalMaskType (Class)<br>
 * \li SpectralCentroidType (Class)<br>
 * \li StreamMaskType (Class)<br>
 * \li TemporalCentroidType (Class)<br>
 * \li TemporalMaskType (Class)<br>
 * \li TextualSummaryComponentType (Class)<br>
 * \li TextureBrowsingType (Class)<br>
 * \li VideoSignatureType (Class)<br>
 * \li VisualSummaryComponentType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsDescriptorCollectionType_Descriptor_CollectionType :
	public Dc1NodeCollection
{
public:

	/** Initializes the collection.
	  * @param maxElems Number of elements for which memory is initially allocated.
	  */
	virtual void Initialize(unsigned int maxElems) = 0;

	/** Adds a new element to the collection.
	  * @param toAdd Element to be added. Default element type: IMp7JrsDType
	  */
	virtual void addElement(const Dc1NodePtr &toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Sets the element at the given position in the collection.
	  * @param toSet New value of the element. Default element type: IMp7JrsDType
	  * @param setAt Position of the element which shall be set.
	  */
	virtual void setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Inserts the element at the given position in the collection.
	  * @param toInsert Element to be inserted. Default element type: IMp7JrsDType
	  * @param insertAt Position where the element will be inserted.
	  */
	virtual void insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes all elements from the collection.
	  */
	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Removes the specified element from the collection.
	  * @param removeAt Position of the element to be removed. 
	  */
	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/** Check whether the collection contains the given element.
	  * @param toCheck Element to be checked for. Default element type: IMp7JrsDType
	  * @return True if the element is in the collection, false otherwise.
	  */
	virtual bool containsElement(const Dc1NodePtr &toCheck) = 0;

	/** Get the current capacity of the collection.
	  * @return The number of elements for which memory has been allocated.
	  */
	virtual unsigned int curCapacity() const = 0;

	/** Gets a constant pointer to the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: Mp7JrsDType.
	  */
	virtual const Dc1NodePtr elementAt(unsigned int getAt) const = 0;

	/** Gets a pointer to the element at the given position.
	  * @param Position of the element to be retrieved.
	  * @return A pointer to the given element or a null pointer if there is no element at the specified position. Default element type: Mp7JrsDType.
	  */
	virtual Dc1NodePtr elementAt(unsigned int getAt) = 0;

	/** Gets an enumeration of all contained elements of the specified type.
	  * @param The class id of the wanted elements.
	  * @return A (possibly empty) enumeration of all contained elements of type id. 
	  */
	virtual Dc1NodeEnum GetElementsOfType(unsigned int id) const = 0;

	/** Gets an enumeration of all contained elements.
	  * @return A (possibly empty) enumeration of all contained elements.
	  */
	virtual Dc1NodeEnum GetElements() const = 0;

	/** Get the size of the collection.
	  * @return The current number of elements in the collection.
	  */
	virtual unsigned int size() const = 0;

	/** Allocate additional memory for the speciifed number of elements.
	  * @param length Number of elements to allocate memory for.
	  */
	virtual void ensureExtraCapacity(unsigned int length) = 0;

	// JRS: addition to Xerces collection
	/** Get the position of the specified element. 
	  * @param toCheck Element to be found in the collection. Default element type: IMp7JrsDType
	  * @return The position of the specified element or -1 if the element has not been found.
	  */
	virtual int elementIndexOf(const Dc1NodePtr &toCheck) const = 0;

};


class MP7JRS_EXPORT Mp7JrsDescriptorCollectionType_Descriptor_CollectionType :
	public IMp7JrsDescriptorCollectionType_Descriptor_CollectionType
{
	friend class Mp7JrsDescriptorCollectionType_Descriptor_CollectionTypeFactory;

public:
	virtual void Initialize(unsigned int maxElems);

	virtual void addElement(const Dc1NodePtr &toAdd, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void removeAllElements(Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual void removeElementAt(unsigned int removeAt, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool containsElement(const Dc1NodePtr &toCheck);

	virtual unsigned int curCapacity() const;

	virtual const Dc1NodePtr elementAt(unsigned int getAt) const;

	virtual Dc1NodePtr elementAt(unsigned int getAt);

	virtual Dc1NodeEnum GetElementsOfType(unsigned int id) const;

	virtual Dc1NodeEnum GetElements() const;

	virtual unsigned int size() const;

	virtual void ensureExtraCapacity(unsigned int length);

	virtual int elementIndexOf(const Dc1NodePtr &toCheck) const;

	/** Get the class name.
	 * @return The name of this class.
     */
	virtual XMLCh * ToText() const;

	/** Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/** Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/** Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	virtual Dc1NodePtr NodeFromXPath(const XMLCh *xpath) const;

// no includefile for extension defined 
// file Mp7JrsDescriptorCollectionType_Descriptor_CollectionType_ExtMethodDef.h


	virtual ~Mp7JrsDescriptorCollectionType_Descriptor_CollectionType();

protected:
	Mp7JrsDescriptorCollectionType_Descriptor_CollectionType();
	virtual void DeepCopy(const Dc1NodePtr & original);
	virtual Dc1NodeEnum GetAllChildElements() const { return GetElements(); }

private:
	Dc1ValueVectorOf <Dc1NodePtr> * m_Item;
	bool dontSerializeWithContentName; // HACK


// no includefile for extension defined 
// file Mp7JrsDescriptorCollectionType_Descriptor_CollectionType_ExtPropDef.h


};


#endif // _1769MP7JRSDESCRIPTORCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPE_H
