/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2441MP7JRSMELODYTYPE_H
#define _2441MP7JRSMELODYTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMelodyTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMelodyType_ExtInclude.h


class IMp7JrsAudioDSType;
typedef Dc1Ptr< IMp7JrsAudioDSType > Mp7JrsAudioDSPtr;
class IMp7JrsMelodyType;
typedef Dc1Ptr< IMp7JrsMelodyType> Mp7JrsMelodyPtr;
class IMp7JrsMeterType;
typedef Dc1Ptr< IMp7JrsMeterType > Mp7JrsMeterPtr;
class IMp7JrsscaleType;
typedef Dc1Ptr< IMp7JrsscaleType > Mp7JrsscalePtr;
class IMp7JrsKeyType;
typedef Dc1Ptr< IMp7JrsKeyType > Mp7JrsKeyPtr;
class IMp7JrsMelodyContourType;
typedef Dc1Ptr< IMp7JrsMelodyContourType > Mp7JrsMelodyContourPtr;
class IMp7JrsMelodyType_MelodySequence_CollectionType;
typedef Dc1Ptr< IMp7JrsMelodyType_MelodySequence_CollectionType > Mp7JrsMelodyType_MelodySequence_CollectionPtr;
#include "Mp7JrsAudioDSType.h"
class IMp7JrsintegerVector;
typedef Dc1Ptr< IMp7JrsintegerVector > Mp7JrsintegerVectorPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsMelodyType for class Mp7JrsMelodyType<br>
 * Located at: mpeg7-v4.xsd, line 2552<br>
 * Classified: Class<br>
 * Derived from: AudioDSType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsMelodyType 
 :
		public IMp7JrsAudioDSType
{
public:
	// TODO: make these protected?
	IMp7JrsMelodyType();
	virtual ~IMp7JrsMelodyType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Meter element.
	 * @return Mp7JrsMeterPtr
	 */
	virtual Mp7JrsMeterPtr GetMeter() const = 0;

	virtual bool IsValidMeter() const = 0;

	/**
	 * Set Meter element.
	 * @param item const Mp7JrsMeterPtr &
	 */
	virtual void SetMeter(const Mp7JrsMeterPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Meter element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMeter() = 0;

	/**
	 * Get Scale element.
	 * @return Mp7JrsscalePtr
	 */
	virtual Mp7JrsscalePtr GetScale() const = 0;

	virtual bool IsValidScale() const = 0;

	/**
	 * Set Scale element.
	 * @param item const Mp7JrsscalePtr &
	 */
	virtual void SetScale(const Mp7JrsscalePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Scale element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScale() = 0;

	/**
	 * Get Key element.
	 * @return Mp7JrsKeyPtr
	 */
	virtual Mp7JrsKeyPtr GetKey() const = 0;

	virtual bool IsValidKey() const = 0;

	/**
	 * Set Key element.
	 * @param item const Mp7JrsKeyPtr &
	 */
	virtual void SetKey(const Mp7JrsKeyPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Key element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateKey() = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get MelodyContour element.
	 * @return Mp7JrsMelodyContourPtr
	 */
	virtual Mp7JrsMelodyContourPtr GetMelodyContour() const = 0;

	/**
	 * Set MelodyContour element.
	 * @param item const Mp7JrsMelodyContourPtr &
	 */
	// Mandatory			
	virtual void SetMelodyContour(const Mp7JrsMelodyContourPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MelodySequence element.
	 * @return Mp7JrsMelodyType_MelodySequence_CollectionPtr
	 */
	virtual Mp7JrsMelodyType_MelodySequence_CollectionPtr GetMelodySequence() const = 0;

	/**
	 * Set MelodySequence element.
	 * @param item const Mp7JrsMelodyType_MelodySequence_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMelodySequence(const Mp7JrsMelodyType_MelodySequence_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
	/**
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDSType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const = 0;

	/**
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const = 0;

	/**
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDSType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels() = 0;

	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of MelodyType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Meter</li>
	 * <li>Scale</li>
	 * <li>Key</li>
	 * <li>MelodyContour</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MelodySequence</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMelodyType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMelodyType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMelodyType<br>
 * Located at: mpeg7-v4.xsd, line 2552<br>
 * Classified: Class<br>
 * Derived from: AudioDSType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 2552.<br>
 */
class DC1_EXPORT Mp7JrsMelodyType :
		public IMp7JrsMelodyType
{
	friend class Mp7JrsMelodyTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsMelodyType
	 
	 * @return Dc1Ptr< Mp7JrsAudioDSType >
	 */
	virtual Dc1Ptr< Mp7JrsAudioDSType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Meter element.
	 * @return Mp7JrsMeterPtr
	 */
	virtual Mp7JrsMeterPtr GetMeter() const;

	virtual bool IsValidMeter() const;

	/*
	 * Set Meter element.
	 * @param item const Mp7JrsMeterPtr &
	 */
	virtual void SetMeter(const Mp7JrsMeterPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Meter element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMeter();

	/*
	 * Get Scale element.
	 * @return Mp7JrsscalePtr
	 */
	virtual Mp7JrsscalePtr GetScale() const;

	virtual bool IsValidScale() const;

	/*
	 * Set Scale element.
	 * @param item const Mp7JrsscalePtr &
	 */
	virtual void SetScale(const Mp7JrsscalePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Scale element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateScale();

	/*
	 * Get Key element.
	 * @return Mp7JrsKeyPtr
	 */
	virtual Mp7JrsKeyPtr GetKey() const;

	virtual bool IsValidKey() const;

	/*
	 * Set Key element.
	 * @param item const Mp7JrsKeyPtr &
	 */
	virtual void SetKey(const Mp7JrsKeyPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Key element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateKey();

		/* @name Choice

		 */
		//@{
	/*
	 * Get MelodyContour element.
	 * @return Mp7JrsMelodyContourPtr
	 */
	virtual Mp7JrsMelodyContourPtr GetMelodyContour() const;

	/*
	 * Set MelodyContour element.
	 * @param item const Mp7JrsMelodyContourPtr &
	 */
	// Mandatory			
	virtual void SetMelodyContour(const Mp7JrsMelodyContourPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MelodySequence element.
	 * @return Mp7JrsMelodyType_MelodySequence_CollectionPtr
	 */
	virtual Mp7JrsMelodyType_MelodySequence_CollectionPtr GetMelodySequence() const;

	/*
	 * Set MelodySequence element.
	 * @param item const Mp7JrsMelodyType_MelodySequence_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMelodySequence(const Mp7JrsMelodyType_MelodySequence_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
	/*
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDSType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const;

	/*
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const;

	/*
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDSType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels();

	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of MelodyType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Meter</li>
	 * <li>Scale</li>
	 * <li>Key</li>
	 * <li>MelodyContour</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MelodySequence</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMelodyType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMelodyType();

protected:
	Mp7JrsMelodyType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsAudioDSType > m_Base;

	Mp7JrsMeterPtr m_Meter;
	bool m_Meter_Exist; // For optional elements 
	Mp7JrsscalePtr m_Scale;
	bool m_Scale_Exist; // For optional elements 
	Mp7JrsKeyPtr m_Key;
	bool m_Key_Exist; // For optional elements 
// DefinionChoice
	Mp7JrsMelodyContourPtr m_MelodyContour;
	Mp7JrsMelodyType_MelodySequence_CollectionPtr m_MelodySequence;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsMelodyType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2441MP7JRSMELODYTYPE_H

