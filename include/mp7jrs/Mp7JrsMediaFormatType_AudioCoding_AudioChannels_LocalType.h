/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2301MP7JRSMEDIAFORMATTYPE_AUDIOCODING_AUDIOCHANNELS_LOCALTYPE_H
#define _2301MP7JRSMEDIAFORMATTYPE_AUDIOCODING_AUDIOCHANNELS_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"

#include "Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtInclude.h


class IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType;
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType> Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr;

/** 
 * Generated interface IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType for class Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5969<br>
 * Classified: Class<br>
 * Derived from: nonNegativeInteger (Value)<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType();
	virtual ~IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType();
	/**
	 * Sets the content of Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType	 * @param item unsigned
	 */
	virtual void SetContent(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType 
	 * @return unsigned
	 */
	virtual unsigned GetContent() const = 0;

	/**
	 * Get front attribute.
	 * @return unsigned
	 */
	virtual unsigned Getfront() const = 0;

	/**
	 * Get front attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfront() const = 0;

	/**
	 * Set front attribute.
	 * @param item unsigned
	 */
	virtual void Setfront(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate front attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefront() = 0;

	/**
	 * Get side attribute.
	 * @return unsigned
	 */
	virtual unsigned Getside() const = 0;

	/**
	 * Get side attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existside() const = 0;

	/**
	 * Set side attribute.
	 * @param item unsigned
	 */
	virtual void Setside(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate side attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateside() = 0;

	/**
	 * Get rear attribute.
	 * @return unsigned
	 */
	virtual unsigned Getrear() const = 0;

	/**
	 * Get rear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrear() const = 0;

	/**
	 * Set rear attribute.
	 * @param item unsigned
	 */
	virtual void Setrear(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate rear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterear() = 0;

	/**
	 * Get lfe attribute.
	 * @return unsigned
	 */
	virtual unsigned Getlfe() const = 0;

	/**
	 * Get lfe attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlfe() const = 0;

	/**
	 * Set lfe attribute.
	 * @param item unsigned
	 */
	virtual void Setlfe(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lfe attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelfe() = 0;

	/**
	 * Get track attribute.
	 * @return unsigned
	 */
	virtual unsigned Gettrack() const = 0;

	/**
	 * Get track attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtrack() const = 0;

	/**
	 * Set track attribute.
	 * @param item unsigned
	 */
	virtual void Settrack(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate track attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetrack() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType_AudioCoding_AudioChannels_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaFormatType_AudioCoding_AudioChannels_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5969<br>
 * Classified: Class<br>
 * Derived from: nonNegativeInteger (Value)<br>
 * Defined in mpeg7-v4.xsd, line 5969.<br>
 */
class DC1_EXPORT Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType :
		public IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType
{
	friend class Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType	 * @param item unsigned
	 */
	virtual void SetContent(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType 
	 * @return unsigned
	 */
	virtual unsigned GetContent() const;

	/*
	 * Get front attribute.
	 * @return unsigned
	 */
	virtual unsigned Getfront() const;

	/*
	 * Get front attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfront() const;

	/*
	 * Set front attribute.
	 * @param item unsigned
	 */
	virtual void Setfront(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate front attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefront();

	/*
	 * Get side attribute.
	 * @return unsigned
	 */
	virtual unsigned Getside() const;

	/*
	 * Get side attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existside() const;

	/*
	 * Set side attribute.
	 * @param item unsigned
	 */
	virtual void Setside(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate side attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateside();

	/*
	 * Get rear attribute.
	 * @return unsigned
	 */
	virtual unsigned Getrear() const;

	/*
	 * Get rear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrear() const;

	/*
	 * Set rear attribute.
	 * @param item unsigned
	 */
	virtual void Setrear(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate rear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterear();

	/*
	 * Get lfe attribute.
	 * @return unsigned
	 */
	virtual unsigned Getlfe() const;

	/*
	 * Get lfe attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlfe() const;

	/*
	 * Set lfe attribute.
	 * @param item unsigned
	 */
	virtual void Setlfe(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lfe attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelfe();

	/*
	 * Get track attribute.
	 * @return unsigned
	 */
	virtual unsigned Gettrack() const;

	/*
	 * Get track attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtrack() const;

	/*
	 * Set track attribute.
	 * @param item unsigned
	 */
	virtual void Settrack(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate track attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetrack();

	/*
	 * Gets or creates Dc1NodePtr child elements of MediaFormatType_AudioCoding_AudioChannels_LocalType.
	 * Currently just supports rudimentary XPath expressions as MediaFormatType_AudioCoding_AudioChannels_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType();

protected:
	Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	unsigned m_front;
	bool m_front_Exist;
	unsigned m_side;
	bool m_side_Exist;
	unsigned m_rear;
	bool m_rear_Exist;
	unsigned m_lfe;
	bool m_lfe_Exist;
	unsigned m_track;
	bool m_track_Exist;

	// Base Value
	unsigned m_Base;


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2301MP7JRSMEDIAFORMATTYPE_AUDIOCODING_AUDIOCHANNELS_LOCALTYPE_H

