/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2023MP7JRSGRAPHTYPE_NODE_LOCALTYPE_H
#define _2023MP7JRSGRAPHTYPE_NODE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsGraphType_Node_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsGraphType_Node_LocalType_ExtInclude.h


class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsGraphType_Node_LocalType;
typedef Dc1Ptr< IMp7JrsGraphType_Node_LocalType> Mp7JrsGraphType_Node_LocalPtr;
class IMp7JrsTermUseType;
typedef Dc1Ptr< IMp7JrsTermUseType > Mp7JrsTermUsePtr;
#include "Mp7JrsReferenceType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;

/** 
 * Generated interface IMp7JrsGraphType_Node_LocalType for class Mp7JrsGraphType_Node_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5568<br>
 * Classified: Class<br>
 * Derived from: ReferenceType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsGraphType_Node_LocalType 
 :
		public IMp7JrsReferenceType
{
public:
	// TODO: make these protected?
	IMp7JrsGraphType_Node_LocalType();
	virtual ~IMp7JrsGraphType_Node_LocalType();
	/**
	 * Get id attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Name element.
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetName() const = 0;

	virtual bool IsValidName() const = 0;

	/**
	 * Set Name element.
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetName(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Name element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateName() = 0;

		//@}
	/**
	 * Get idref attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getidref() const = 0;

	/**
	 * Get idref attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existidref() const = 0;

	/**
	 * Set idref attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item XMLCh *
	 */
	virtual void Setidref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate idref attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateidref() = 0;

	/**
	 * Get xpath attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr Getxpath() const = 0;

	/**
	 * Get xpath attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existxpath() const = 0;

	/**
	 * Set xpath attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void Setxpath(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate xpath attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatexpath() = 0;

	/**
	 * Get href attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gethref() const = 0;

	/**
	 * Get href attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existhref() const = 0;

	/**
	 * Set href attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item XMLCh *
	 */
	virtual void Sethref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate href attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatehref() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of GraphType_Node_LocalType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>Name</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsGraphType_Node_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsGraphType_Node_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsGraphType_Node_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 5568<br>
 * Classified: Class<br>
 * Derived from: ReferenceType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 5568.<br>
 */
class DC1_EXPORT Mp7JrsGraphType_Node_LocalType :
		public IMp7JrsGraphType_Node_LocalType
{
	friend class Mp7JrsGraphType_Node_LocalTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsGraphType_Node_LocalType
	 
	 * @return Dc1Ptr< Mp7JrsReferenceType >
	 */
	virtual Dc1Ptr< Mp7JrsReferenceType > GetBase() const;
	/*
	 * Get id attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Name element.
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetName() const;

	virtual bool IsValidName() const;

	/*
	 * Set Name element.
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetName(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Name element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateName();

		//@}
	/*
	 * Get idref attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getidref() const;

	/*
	 * Get idref attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existidref() const;

	/*
	 * Set idref attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item XMLCh *
	 */
	virtual void Setidref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate idref attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateidref();

	/*
	 * Get xpath attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr Getxpath() const;

	/*
	 * Get xpath attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existxpath() const;

	/*
	 * Set xpath attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void Setxpath(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate xpath attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatexpath();

	/*
	 * Get href attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gethref() const;

	/*
	 * Get href attribute validity information.
	 * (Inherited from Mp7JrsReferenceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existhref() const;

	/*
	 * Set href attribute.
<br>
	 * (Inherited from Mp7JrsReferenceType)
	 * @param item XMLCh *
	 */
	virtual void Sethref(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate href attribute.
	 * (Inherited from Mp7JrsReferenceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatehref();

	/*
	 * Gets or creates Dc1NodePtr child elements of GraphType_Node_LocalType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>Name</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsGraphType_Node_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsGraphType_Node_LocalType();

protected:
	Mp7JrsGraphType_Node_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_id;
	bool m_id_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsReferenceType > m_Base;

	Mp7JrsTermUsePtr m_Name;
	bool m_Name_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsGraphType_Node_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2023MP7JRSGRAPHTYPE_NODE_LOCALTYPE_H

