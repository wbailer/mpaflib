/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1125MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE_H
#define _1125MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsAudioSegmentType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType_ExtInclude.h


class IMp7JrsAudioSegmentType_LocalType;
typedef Dc1Ptr< IMp7JrsAudioSegmentType_LocalType> Mp7JrsAudioSegmentType_LocalPtr;
class IMp7JrsAudioDType;
typedef Dc1Ptr< IMp7JrsAudioDType > Mp7JrsAudioDPtr;
class IMp7JrsAudioDSType;
typedef Dc1Ptr< IMp7JrsAudioDSType > Mp7JrsAudioDSPtr;

/** 
 * Generated interface IMp7JrsAudioSegmentType_LocalType for class Mp7JrsAudioSegmentType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsAudioSegmentType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsAudioSegmentType_LocalType();
	virtual ~IMp7JrsAudioSegmentType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get AudioDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetAudioDescriptor() const = 0;

	/**
	 * Set AudioDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * </ul>
	 */
	virtual void SetAudioDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AudioDescriptionScheme element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetAudioDescriptionScheme() const = 0;

	/**
	 * Set AudioDescriptionScheme element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * </ul>
	 */
	virtual void SetAudioDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsAudioSegmentType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 7382.<br>
 */
class DC1_EXPORT Mp7JrsAudioSegmentType_LocalType :
		public IMp7JrsAudioSegmentType_LocalType
{
	friend class Mp7JrsAudioSegmentType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get AudioDescriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetAudioDescriptor() const;

	/*
	 * Set AudioDescriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * </ul>
	 */
	virtual void SetAudioDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AudioDescriptionScheme element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetAudioDescriptionScheme() const;

	/*
	 * Set AudioDescriptionScheme element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * </ul>
	 */
	virtual void SetAudioDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsAudioSegmentType_LocalType();

protected:
	Mp7JrsAudioSegmentType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Dc1Ptr< Dc1Node > m_AudioDescriptor;
	Dc1Ptr< Dc1Node > m_AudioDescriptionScheme;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1125MP7JRSAUDIOSEGMENTTYPE_LOCALTYPE_H

