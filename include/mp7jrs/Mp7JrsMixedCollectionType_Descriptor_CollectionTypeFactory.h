/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef _2472MP7JRSMIXEDCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPEFACTORY_H
#define _2472MP7JRSMIXEDCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPEFACTORY_H

#include "Dc1Defines.h"
#include "Dc1Node.h"
#include "Dc1Factory.h"
#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"

/*
 * Generated factory Mp7JrsMixedCollectionType_Descriptor_CollectionTypeFactory<br>
 * for class Mp7JrsMixedCollectionType_Descriptor_CollectionType:<br>
 * Located at: mpeg7-v4.xsd, line 9226<br>
 * Abstract item type: DType<br>
 * Possible classes:<br>
 * \li AdvancedFaceRecognitionType (Class)<br>
 * \li AudioBPMType (Class)<br>
 * \li AudioFundamentalFrequencyType (Class)<br>
 * \li AudioHarmonicityType (Class)<br>
 * \li AudioPowerType (Class)<br>
 * \li AudioSpectrumBasisType (Class)<br>
 * \li AudioSpectrumCentroidType (Class)<br>
 * \li AudioSpectrumEnvelopeType (Class)<br>
 * \li AudioSpectrumFlatnessType (Class)<br>
 * \li AudioSpectrumProjectionType (Class)<br>
 * \li AudioSpectrumSpreadType (Class)<br>
 * \li AudioSummaryComponentType (Class)<br>
 * \li AudioWaveformType (Class)<br>
 * \li BackgroundNoiseLevelType (Class)<br>
 * \li BalanceType (Class)<br>
 * \li BandwidthType (Class)<br>
 * \li BrowsingPreferencesType_PreferenceCondition_LocalType (Class)<br>
 * \li CameraMotionType (Class)<br>
 * \li ChordBaseType (Class)<br>
 * \li ClassificationPerceptualAttributeType (Class)<br>
 * \li ColorLayoutType (Class)<br>
 * \li ColorSamplingType (Class)<br>
 * \li ColorStructureType (Class)<br>
 * \li ColorTemperatureType (Class)<br>
 * \li ContourShapeType (Class)<br>
 * \li CrossChannelCorrelationType (Class)<br>
 * \li DcOffsetType (Class)<br>
 * \li DistributionPerceptualAttributeType (Class)<br>
 * \li DominantColorType (Class)<br>
 * \li EdgeHistogramType (Class)<br>
 * \li ExtendedMediaQualityType (Class)<br>
 * \li FaceRecognitionType (Class)<br>
 * \li GoFGoPColorType (Class)<br>
 * \li HarmonicSpectralCentroidType (Class)<br>
 * \li HarmonicSpectralDeviationType (Class)<br>
 * \li HarmonicSpectralSpreadType (Class)<br>
 * \li HarmonicSpectralVariationType (Class)<br>
 * \li HomogeneousTextureType (Class)<br>
 * \li ImageSignatureType (Class)<br>
 * \li LinearPerceptualAttributeType (Class)<br>
 * \li LogAttackTimeType (Class)<br>
 * \li MatchingHintType (Class)<br>
 * \li MediaFormatType (Class)<br>
 * \li MediaIdentificationType (Class)<br>
 * \li MediaQualityType (Class)<br>
 * \li MediaSpaceMaskType (Class)<br>
 * \li MediaTranscodingHintsType (Class)<br>
 * \li MeterType (Class)<br>
 * \li MotionActivityType (Class)<br>
 * \li MotionTrajectoryType (Class)<br>
 * \li OrderedGroupDataSetMaskType (Class)<br>
 * \li ParametricMotionType (Class)<br>
 * \li Perceptual3DShapeType (Class)<br>
 * \li PerceptualBeatType (Class)<br>
 * \li PerceptualEnergyType (Class)<br>
 * \li PerceptualGenreDistributionElementType (Class)<br>
 * \li PerceptualGenreDistributionType (Class)<br>
 * \li PerceptualInstrumentationDistributionElementType (Class)<br>
 * \li PerceptualInstrumentationDistributionType (Class)<br>
 * \li PerceptualLanguageType (Class)<br>
 * \li PerceptualLyricsDistributionElementType (Class)<br>
 * \li PerceptualLyricsDistributionType (Class)<br>
 * \li PerceptualMoodDistributionElementType (Class)<br>
 * \li PerceptualMoodDistributionType (Class)<br>
 * \li PerceptualRecordingType (Class)<br>
 * \li PerceptualSoundType (Class)<br>
 * \li PerceptualTempoType (Class)<br>
 * \li PerceptualValenceType (Class)<br>
 * \li PerceptualVocalsType (Class)<br>
 * \li PointOfViewType (Class)<br>
 * \li PreferenceConditionType (Class)<br>
 * \li ReferencePitchType (Class)<br>
 * \li RegionShapeType (Class)<br>
 * \li RelativeDelayType (Class)<br>
 * \li RhythmicBaseType (Class)<br>
 * \li ScalableColorType (Class)<br>
 * \li SceneGraphMaskType (Class)<br>
 * \li Shape3DType (Class)<br>
 * \li ShapeVariationType (Class)<br>
 * \li SilenceType (Class)<br>
 * \li SoundModelStateHistogramType (Class)<br>
 * \li SourcePreferencesType_MediaFormat_LocalType (Class)<br>
 * \li SpatialMaskType (Class)<br>
 * \li SpatioTemporalMaskType (Class)<br>
 * \li SpectralCentroidType (Class)<br>
 * \li StreamMaskType (Class)<br>
 * \li TemporalCentroidType (Class)<br>
 * \li TemporalMaskType (Class)<br>
 * \li TextualSummaryComponentType (Class)<br>
 * \li TextureBrowsingType (Class)<br>
 * \li VideoSignatureType (Class)<br>
 * \li VisualSummaryComponentType (Class)<br>
 */

class DC1_EXPORT Mp7JrsMixedCollectionType_Descriptor_CollectionTypeFactory :
	public Dc1Factory
{
public:
	Mp7JrsMixedCollectionType_Descriptor_CollectionTypeFactory();
	virtual ~Mp7JrsMixedCollectionType_Descriptor_CollectionTypeFactory();

	virtual const char * GetTypeName() const;
	virtual Dc1NodePtr CreateObject();
};

#endif // _2472MP7JRSMIXEDCOLLECTIONTYPE_DESCRIPTOR_COLLECTIONTYPEFACTORY_H
