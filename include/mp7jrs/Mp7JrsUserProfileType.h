/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3893MP7JRSUSERPROFILETYPE_H
#define _3893MP7JRSUSERPROFILETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsUserProfileTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtInclude.h


class IMp7JrsPersonType;
typedef Dc1Ptr< IMp7JrsPersonType > Mp7JrsPersonPtr;
class IMp7JrsUserProfileType;
typedef Dc1Ptr< IMp7JrsUserProfileType> Mp7JrsUserProfilePtr;
#include "Mp7JrsUserProfileType_sex_LocalType.h"
class IMp7JrsUserProfileType_PublicIdentifier_CollectionType;
typedef Dc1Ptr< IMp7JrsUserProfileType_PublicIdentifier_CollectionType > Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr;
class IMp7JrsTextAnnotationType;
typedef Dc1Ptr< IMp7JrsTextAnnotationType > Mp7JrsTextAnnotationPtr;
class IMp7JrsTimeZoneType;
typedef Dc1Ptr< IMp7JrsTimeZoneType > Mp7JrsTimeZonePtr;
class IMp7JrsConnectionsType;
typedef Dc1Ptr< IMp7JrsConnectionsType > Mp7JrsConnectionsPtr;
class IMp7JrsPersonalInterestsType;
typedef Dc1Ptr< IMp7JrsPersonalInterestsType > Mp7JrsPersonalInterestsPtr;
#include "Mp7JrsPersonType.h"
class IMp7JrsPersonType_CollectionType;
typedef Dc1Ptr< IMp7JrsPersonType_CollectionType > Mp7JrsPersonType_CollectionPtr;
class IMp7JrsPersonType_Affiliation_CollectionType;
typedef Dc1Ptr< IMp7JrsPersonType_Affiliation_CollectionType > Mp7JrsPersonType_Affiliation_CollectionPtr;
class IMp7JrsPersonType_Citizenship_CollectionType;
typedef Dc1Ptr< IMp7JrsPersonType_Citizenship_CollectionType > Mp7JrsPersonType_Citizenship_CollectionPtr;
class IMp7JrsPlaceType;
typedef Dc1Ptr< IMp7JrsPlaceType > Mp7JrsPlacePtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsPersonType_ElectronicAddress_CollectionType;
typedef Dc1Ptr< IMp7JrsPersonType_ElectronicAddress_CollectionType > Mp7JrsPersonType_ElectronicAddress_CollectionPtr;
class IMp7JrsTextualType;
typedef Dc1Ptr< IMp7JrsTextualType > Mp7JrsTextualPtr;
class IMp7JrscountryCode;
typedef Dc1Ptr< IMp7JrscountryCode > Mp7JrscountryCodePtr;
#include "Mp7JrsAgentType.h"
class IMp7JrsAgentType_Icon_CollectionType;
typedef Dc1Ptr< IMp7JrsAgentType_Icon_CollectionType > Mp7JrsAgentType_Icon_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsUserProfileType for class Mp7JrsUserProfileType<br>
 * Located at: mpeg7-v4.xsd, line 5344<br>
 * Classified: Class<br>
 * Derived from: PersonType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsUserProfileType 
 :
		public IMp7JrsPersonType
{
public:
	// TODO: make these protected?
	IMp7JrsUserProfileType();
	virtual ~IMp7JrsUserProfileType();
	/**
	 * Get sex attribute.
	 * @return Mp7JrsUserProfileType_sex_LocalType::Enumeration
	 */
	virtual Mp7JrsUserProfileType_sex_LocalType::Enumeration Getsex() const = 0;

	/**
	 * Get sex attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsex() const = 0;

	/**
	 * Set sex attribute.
	 * @param item Mp7JrsUserProfileType_sex_LocalType::Enumeration
	 */
	virtual void Setsex(Mp7JrsUserProfileType_sex_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sex attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesex() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get PublicIdentifier element.
	 * @return Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr
	 */
	virtual Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr GetPublicIdentifier() const = 0;

	/**
	 * Set PublicIdentifier element.
	 * @param item const Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPublicIdentifier(const Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Description element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetDescription() const = 0;

	virtual bool IsValidDescription() const = 0;

	/**
	 * Set Description element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetDescription(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Description element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescription() = 0;

	/**
	 * Get TimeZone element.
	 * @return Mp7JrsTimeZonePtr
	 */
	virtual Mp7JrsTimeZonePtr GetTimeZone() const = 0;

	virtual bool IsValidTimeZone() const = 0;

	/**
	 * Set TimeZone element.
	 * @param item const Mp7JrsTimeZonePtr &
	 */
	virtual void SetTimeZone(const Mp7JrsTimeZonePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate TimeZone element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTimeZone() = 0;

	/**
	 * Get PrimaryLanguage element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetPrimaryLanguage() const = 0;

	virtual bool IsValidPrimaryLanguage() const = 0;

	/**
	 * Set PrimaryLanguage element.
	 * @param item XMLCh *
	 */
	virtual void SetPrimaryLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PrimaryLanguage element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePrimaryLanguage() = 0;

	/**
	 * Get Connections element.
	 * @return Mp7JrsConnectionsPtr
	 */
	virtual Mp7JrsConnectionsPtr GetConnections() const = 0;

	virtual bool IsValidConnections() const = 0;

	/**
	 * Set Connections element.
	 * @param item const Mp7JrsConnectionsPtr &
	 */
	virtual void SetConnections(const Mp7JrsConnectionsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Connections element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateConnections() = 0;

	/**
	 * Get PersonalInterests element.
	 * @return Mp7JrsPersonalInterestsPtr
	 */
	virtual Mp7JrsPersonalInterestsPtr GetPersonalInterests() const = 0;

	virtual bool IsValidPersonalInterests() const = 0;

	/**
	 * Set PersonalInterests element.
	 * @param item const Mp7JrsPersonalInterestsPtr &
	 */
	virtual void SetPersonalInterests(const Mp7JrsPersonalInterestsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PersonalInterests element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePersonalInterests() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get PersonType_LocalType element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_CollectionPtr
	 */
	virtual Mp7JrsPersonType_CollectionPtr GetPersonType_LocalType() const = 0;

	/**
	 * Set PersonType_LocalType element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPersonType_LocalType(const Mp7JrsPersonType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Affiliation element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_Affiliation_CollectionPtr
	 */
	virtual Mp7JrsPersonType_Affiliation_CollectionPtr GetAffiliation() const = 0;

	/**
	 * Set Affiliation element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_Affiliation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAffiliation(const Mp7JrsPersonType_Affiliation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Citizenship element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_Citizenship_CollectionPtr
	 */
	virtual Mp7JrsPersonType_Citizenship_CollectionPtr GetCitizenship() const = 0;

	/**
	 * Set Citizenship element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_Citizenship_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCitizenship(const Mp7JrsPersonType_Citizenship_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get Address element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetAddress() const = 0;

	/**
	 * Set Address element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetAddress(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AddressRef element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAddressRef() const = 0;

	/**
	 * Set AddressRef element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetAddressRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_ElectronicAddress_CollectionPtr
	 */
	virtual Mp7JrsPersonType_ElectronicAddress_CollectionPtr GetElectronicAddress() const = 0;

	/**
	 * Set ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_ElectronicAddress_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetElectronicAddress(const Mp7JrsPersonType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PersonDescription element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetPersonDescription() const = 0;

	virtual bool IsValidPersonDescription() const = 0;

	/**
	 * Set PersonDescription element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetPersonDescription(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PersonDescription element.
	 * (Inherited from Mp7JrsPersonType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePersonDescription() = 0;

	/**
	 * Get Nationality element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrscountryCodePtr
	 */
	virtual Mp7JrscountryCodePtr GetNationality() const = 0;

	virtual bool IsValidNationality() const = 0;

	/**
	 * Set Nationality element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrscountryCodePtr &
	 */
	virtual void SetNationality(const Mp7JrscountryCodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Nationality element.
	 * (Inherited from Mp7JrsPersonType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNationality() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Icon element.
<br>
	 * (Inherited from Mp7JrsAgentType)
	 * @return Mp7JrsAgentType_Icon_CollectionPtr
	 */
	virtual Mp7JrsAgentType_Icon_CollectionPtr GetIcon() const = 0;

	/**
	 * Set Icon element.
<br>
	 * (Inherited from Mp7JrsAgentType)
	 * @param item const Mp7JrsAgentType_Icon_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetIcon(const Mp7JrsAgentType_Icon_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of UserProfileType.
	 * Currently this type contains 16 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Icon</li>
	 * <li>Name</li>
	 * <li>NameTerm</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Affiliation</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Citizenship</li>
	 * <li>Address</li>
	 * <li>AddressRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:ElectronicAddress</li>
	 * <li>PersonDescription</li>
	 * <li>Nationality</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PublicIdentifier</li>
	 * <li>Description</li>
	 * <li>TimeZone</li>
	 * <li>Connections</li>
	 * <li>PersonalInterests</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsUserProfileType<br>
 * Located at: mpeg7-v4.xsd, line 5344<br>
 * Classified: Class<br>
 * Derived from: PersonType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 5344.<br>
 */
class DC1_EXPORT Mp7JrsUserProfileType :
		public IMp7JrsUserProfileType
{
	friend class Mp7JrsUserProfileTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsUserProfileType
	 
	 * @return Dc1Ptr< Mp7JrsPersonType >
	 */
	virtual Dc1Ptr< Mp7JrsPersonType > GetBase() const;
	/*
	 * Get sex attribute.
	 * @return Mp7JrsUserProfileType_sex_LocalType::Enumeration
	 */
	virtual Mp7JrsUserProfileType_sex_LocalType::Enumeration Getsex() const;

	/*
	 * Get sex attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existsex() const;

	/*
	 * Set sex attribute.
	 * @param item Mp7JrsUserProfileType_sex_LocalType::Enumeration
	 */
	virtual void Setsex(Mp7JrsUserProfileType_sex_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sex attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesex();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get PublicIdentifier element.
	 * @return Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr
	 */
	virtual Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr GetPublicIdentifier() const;

	/*
	 * Set PublicIdentifier element.
	 * @param item const Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPublicIdentifier(const Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Description element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetDescription() const;

	virtual bool IsValidDescription() const;

	/*
	 * Set Description element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	virtual void SetDescription(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Description element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescription();

	/*
	 * Get TimeZone element.
	 * @return Mp7JrsTimeZonePtr
	 */
	virtual Mp7JrsTimeZonePtr GetTimeZone() const;

	virtual bool IsValidTimeZone() const;

	/*
	 * Set TimeZone element.
	 * @param item const Mp7JrsTimeZonePtr &
	 */
	virtual void SetTimeZone(const Mp7JrsTimeZonePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate TimeZone element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateTimeZone();

	/*
	 * Get PrimaryLanguage element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetPrimaryLanguage() const;

	virtual bool IsValidPrimaryLanguage() const;

	/*
	 * Set PrimaryLanguage element.
	 * @param item XMLCh *
	 */
	virtual void SetPrimaryLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PrimaryLanguage element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePrimaryLanguage();

	/*
	 * Get Connections element.
	 * @return Mp7JrsConnectionsPtr
	 */
	virtual Mp7JrsConnectionsPtr GetConnections() const;

	virtual bool IsValidConnections() const;

	/*
	 * Set Connections element.
	 * @param item const Mp7JrsConnectionsPtr &
	 */
	virtual void SetConnections(const Mp7JrsConnectionsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Connections element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateConnections();

	/*
	 * Get PersonalInterests element.
	 * @return Mp7JrsPersonalInterestsPtr
	 */
	virtual Mp7JrsPersonalInterestsPtr GetPersonalInterests() const;

	virtual bool IsValidPersonalInterests() const;

	/*
	 * Set PersonalInterests element.
	 * @param item const Mp7JrsPersonalInterestsPtr &
	 */
	virtual void SetPersonalInterests(const Mp7JrsPersonalInterestsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PersonalInterests element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePersonalInterests();

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get PersonType_LocalType element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_CollectionPtr
	 */
	virtual Mp7JrsPersonType_CollectionPtr GetPersonType_LocalType() const;

	/*
	 * Set PersonType_LocalType element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPersonType_LocalType(const Mp7JrsPersonType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Affiliation element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_Affiliation_CollectionPtr
	 */
	virtual Mp7JrsPersonType_Affiliation_CollectionPtr GetAffiliation() const;

	/*
	 * Set Affiliation element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_Affiliation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAffiliation(const Mp7JrsPersonType_Affiliation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Citizenship element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_Citizenship_CollectionPtr
	 */
	virtual Mp7JrsPersonType_Citizenship_CollectionPtr GetCitizenship() const;

	/*
	 * Set Citizenship element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_Citizenship_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCitizenship(const Mp7JrsPersonType_Citizenship_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Choice

		 */
		//@{
	/*
	 * Get Address element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPlacePtr
	 */
	virtual Mp7JrsPlacePtr GetAddress() const;

	/*
	 * Set Address element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPlacePtr &
	 */
	virtual void SetAddress(const Mp7JrsPlacePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AddressRef element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAddressRef() const;

	/*
	 * Set AddressRef element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetAddressRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsPersonType_ElectronicAddress_CollectionPtr
	 */
	virtual Mp7JrsPersonType_ElectronicAddress_CollectionPtr GetElectronicAddress() const;

	/*
	 * Set ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsPersonType_ElectronicAddress_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetElectronicAddress(const Mp7JrsPersonType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PersonDescription element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetPersonDescription() const;

	virtual bool IsValidPersonDescription() const;

	/*
	 * Set PersonDescription element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetPersonDescription(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PersonDescription element.
	 * (Inherited from Mp7JrsPersonType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePersonDescription();

	/*
	 * Get Nationality element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @return Mp7JrscountryCodePtr
	 */
	virtual Mp7JrscountryCodePtr GetNationality() const;

	virtual bool IsValidNationality() const;

	/*
	 * Set Nationality element.
<br>
	 * (Inherited from Mp7JrsPersonType)
	 * @param item const Mp7JrscountryCodePtr &
	 */
	virtual void SetNationality(const Mp7JrscountryCodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Nationality element.
	 * (Inherited from Mp7JrsPersonType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNationality();

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Icon element.
<br>
	 * (Inherited from Mp7JrsAgentType)
	 * @return Mp7JrsAgentType_Icon_CollectionPtr
	 */
	virtual Mp7JrsAgentType_Icon_CollectionPtr GetIcon() const;

	/*
	 * Set Icon element.
<br>
	 * (Inherited from Mp7JrsAgentType)
	 * @param item const Mp7JrsAgentType_Icon_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetIcon(const Mp7JrsAgentType_Icon_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of UserProfileType.
	 * Currently this type contains 16 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Icon</li>
	 * <li>Name</li>
	 * <li>NameTerm</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Affiliation</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Citizenship</li>
	 * <li>Address</li>
	 * <li>AddressRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:ElectronicAddress</li>
	 * <li>PersonDescription</li>
	 * <li>Nationality</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PublicIdentifier</li>
	 * <li>Description</li>
	 * <li>TimeZone</li>
	 * <li>Connections</li>
	 * <li>PersonalInterests</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsUserProfileType();

protected:
	Mp7JrsUserProfileType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsUserProfileType_sex_LocalType::Enumeration m_sex;
	bool m_sex_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsPersonType > m_Base;

	Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr m_PublicIdentifier;
	Mp7JrsTextAnnotationPtr m_Description;
	bool m_Description_Exist; // For optional elements 
	Mp7JrsTimeZonePtr m_TimeZone;
	bool m_TimeZone_Exist; // For optional elements 
	XMLCh * m_PrimaryLanguage;
	bool m_PrimaryLanguage_Exist; // For optional elements 
	Mp7JrsConnectionsPtr m_Connections;
	bool m_Connections_Exist; // For optional elements 
	Mp7JrsPersonalInterestsPtr m_PersonalInterests;
	bool m_PersonalInterests_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3893MP7JRSUSERPROFILETYPE_H

