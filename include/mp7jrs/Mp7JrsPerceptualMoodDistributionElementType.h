/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2767MP7JRSPERCEPTUALMOODDISTRIBUTIONELEMENTTYPE_H
#define _2767MP7JRSPERCEPTUALMOODDISTRIBUTIONELEMENTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsPerceptualMoodDistributionElementTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtInclude.h


class IMp7JrsAudioDType;
typedef Dc1Ptr< IMp7JrsAudioDType > Mp7JrsAudioDPtr;
class IMp7JrsPerceptualMoodDistributionElementType;
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionElementType> Mp7JrsPerceptualMoodDistributionElementPtr;
class IMp7JrstermReferenceType;
typedef Dc1Ptr< IMp7JrstermReferenceType > Mp7JrstermReferencePtr;
class IMp7JrsPerceptualMoodDistributionElementType_score_LocalType;
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionElementType_score_LocalType > Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr;
#include "Mp7JrsAudioDType.h"
class IMp7JrsintegerVector;
typedef Dc1Ptr< IMp7JrsintegerVector > Mp7JrsintegerVectorPtr;
#include "Mp7JrsDType.h"
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsPerceptualMoodDistributionElementType for class Mp7JrsPerceptualMoodDistributionElementType<br>
 * Located at: mpeg7-v4.xsd, line 3094<br>
 * Classified: Class<br>
 * Derived from: AudioDType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsPerceptualMoodDistributionElementType 
 :
		public IMp7JrsAudioDType
{
public:
	// TODO: make these protected?
	IMp7JrsPerceptualMoodDistributionElementType();
	virtual ~IMp7JrsPerceptualMoodDistributionElementType();
	/**
	 * Get id attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getid() const = 0;

	/**
	 * Set id attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	// Mandatory
	virtual void Setid(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get score attribute.
	 * @return Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr
	 */
	virtual Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr Getscore() const = 0;

	/**
	 * Set score attribute.
	 * @param item const Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr &
	 */
	// Mandatory
	virtual void Setscore(const Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const = 0;

	/**
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const = 0;

	/**
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of PerceptualMoodDistributionElementType.
	 * Currently just supports rudimentary XPath expressions as PerceptualMoodDistributionElementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsPerceptualMoodDistributionElementType<br>
 * Located at: mpeg7-v4.xsd, line 3094<br>
 * Classified: Class<br>
 * Derived from: AudioDType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 3094.<br>
 */
class DC1_EXPORT Mp7JrsPerceptualMoodDistributionElementType :
		public IMp7JrsPerceptualMoodDistributionElementType
{
	friend class Mp7JrsPerceptualMoodDistributionElementTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsPerceptualMoodDistributionElementType
	 
	 * @return Dc1Ptr< Mp7JrsAudioDType >
	 */
	virtual Dc1Ptr< Mp7JrsAudioDType > GetBase() const;
	/*
	 * Get id attribute.
	 * @return Mp7JrstermReferencePtr
	 */
	virtual Mp7JrstermReferencePtr Getid() const;

	/*
	 * Set id attribute.
	 * @param item const Mp7JrstermReferencePtr &
	 */
	// Mandatory
	virtual void Setid(const Mp7JrstermReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get score attribute.
	 * @return Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr
	 */
	virtual Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr Getscore() const;

	/*
	 * Set score attribute.
	 * @param item const Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr &
	 */
	// Mandatory
	virtual void Setscore(const Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @return Mp7JrsintegerVectorPtr
	 */
	virtual Mp7JrsintegerVectorPtr Getchannels() const;

	/*
	 * Get channels attribute validity information.
	 * (Inherited from Mp7JrsAudioDType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existchannels() const;

	/*
	 * Set channels attribute.
<br>
	 * (Inherited from Mp7JrsAudioDType)
	 * @param item const Mp7JrsintegerVectorPtr &
	 */
	virtual void Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate channels attribute.
	 * (Inherited from Mp7JrsAudioDType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels();

	/*
	 * Gets or creates Dc1NodePtr child elements of PerceptualMoodDistributionElementType.
	 * Currently just supports rudimentary XPath expressions as PerceptualMoodDistributionElementType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsPerceptualMoodDistributionElementType();

protected:
	Mp7JrsPerceptualMoodDistributionElementType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrstermReferencePtr m_id;
	Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr m_score;

	// Base Class
	Dc1Ptr< Mp7JrsAudioDType > m_Base;


// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2767MP7JRSPERCEPTUALMOODDISTRIBUTIONELEMENTTYPE_H

