/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _3761MP7JRSTITLETYPE_TYPE_LOCALTYPE_H
#define _3761MP7JRSTITLETYPE_TYPE_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file Mp7JrsTitleType_type_LocalType_ExtInclude.h




/**
 * Generated enumeration Mp7JrsTitleType_type_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 6423<br>
 * Enumerations: <br>
 * main (mapped to XML main) <br>
 * secondary (mapped to XML secondary) <br>
 * alternative (mapped to XML alternative) <br>
 * original (mapped to XML original) <br>
 * popular (mapped to XML popular) <br>
 * opusNumber (mapped to XML opusNumber) <br>
 * songTitle (mapped to XML songTitle) <br>
 * albumTitle (mapped to XML albumTitle) <br>
 * seriesTitle (mapped to XML seriesTitle) <br>
 * episodeTitle (mapped to XML episodeTitle) <br>
 */
class DC1_EXPORT Mp7JrsTitleType_type_LocalType
{
public:
	/** Mp7JrsTitleType_type_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		main, // Mapped to main
		secondary, // Mapped to secondary
		alternative, // Mapped to alternative
		original, // Mapped to original
		popular, // Mapped to popular
		opusNumber, // Mapped to opusNumber
		songTitle, // Mapped to songTitle
		albumTitle, // Mapped to albumTitle
		seriesTitle, // Mapped to seriesTitle
		episodeTitle, // Mapped to episodeTitle
	};
	

// no includefile for extension defined 
// file Mp7JrsTitleType_type_LocalType_ExtMethodDef.h


	static XMLCh * ToText(Mp7JrsTitleType_type_LocalType::Enumeration item);
	static Mp7JrsTitleType_type_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file Mp7JrsTitleType_type_LocalType_ExtPropDef.h



};
#endif // _3761MP7JRSTITLETYPE_TYPE_LOCALTYPE_H
