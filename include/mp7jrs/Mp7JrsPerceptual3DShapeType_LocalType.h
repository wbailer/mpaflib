/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2729MP7JRSPERCEPTUAL3DSHAPETYPE_LOCALTYPE_H
#define _2729MP7JRSPERCEPTUAL3DSHAPETYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsPerceptual3DShapeType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtInclude.h


class IMp7JrsPerceptual3DShapeType_LocalType;
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_LocalType> Mp7JrsPerceptual3DShapeType_LocalPtr;
class IMp7Jrsunsigned15;
typedef Dc1Ptr< IMp7Jrsunsigned15 > Mp7Jrsunsigned15Ptr;

/** 
 * Generated interface IMp7JrsPerceptual3DShapeType_LocalType for class Mp7JrsPerceptual3DShapeType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsPerceptual3DShapeType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsPerceptual3DShapeType_LocalType();
	virtual ~IMp7JrsPerceptual3DShapeType_LocalType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Volume element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetVolume() const = 0;

	/**
	 * Set Volume element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetVolume(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Center_X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_X() const = 0;

	/**
	 * Set Center_X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Center_Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_Y() const = 0;

	/**
	 * Set Center_Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Center_Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_Z() const = 0;

	/**
	 * Set Center_Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_1X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1X() const = 0;

	/**
	 * Set PCA_Axis_1X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_1Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1Y() const = 0;

	/**
	 * Set PCA_Axis_1Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_1Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1Z() const = 0;

	/**
	 * Set PCA_Axis_1Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_2X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2X() const = 0;

	/**
	 * Set PCA_Axis_2X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_2Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2Y() const = 0;

	/**
	 * Set PCA_Axis_2Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PCA_Axis_2Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2Z() const = 0;

	/**
	 * Set PCA_Axis_2Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Max_1 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_1() const = 0;

	/**
	 * Set Max_1 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_1(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Max_2 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_2() const = 0;

	/**
	 * Set Max_2 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_2(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Max_3 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_3() const = 0;

	/**
	 * Set Max_3 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_3(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Convexity element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetConvexity() const = 0;

	/**
	 * Set Convexity element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetConvexity(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsPerceptual3DShapeType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 1627.<br>
 */
class DC1_EXPORT Mp7JrsPerceptual3DShapeType_LocalType :
		public IMp7JrsPerceptual3DShapeType_LocalType
{
	friend class Mp7JrsPerceptual3DShapeType_LocalTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Volume element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetVolume() const;

	/*
	 * Set Volume element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetVolume(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Center_X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_X() const;

	/*
	 * Set Center_X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Center_Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_Y() const;

	/*
	 * Set Center_Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Center_Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetCenter_Z() const;

	/*
	 * Set Center_Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetCenter_Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_1X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1X() const;

	/*
	 * Set PCA_Axis_1X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_1Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1Y() const;

	/*
	 * Set PCA_Axis_1Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_1Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_1Z() const;

	/*
	 * Set PCA_Axis_1Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_1Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_2X element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2X() const;

	/*
	 * Set PCA_Axis_2X element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_2Y element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2Y() const;

	/*
	 * Set PCA_Axis_2Y element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PCA_Axis_2Z element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetPCA_Axis_2Z() const;

	/*
	 * Set PCA_Axis_2Z element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetPCA_Axis_2Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Max_1 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_1() const;

	/*
	 * Set Max_1 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_1(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Max_2 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_2() const;

	/*
	 * Set Max_2 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_2(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Max_3 element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetMax_3() const;

	/*
	 * Set Max_3 element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetMax_3(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Convexity element.
	 * @return Mp7Jrsunsigned15Ptr
	 */
	virtual Mp7Jrsunsigned15Ptr GetConvexity() const;

	/*
	 * Set Convexity element.
	 * @param item const Mp7Jrsunsigned15Ptr &
	 */
	// Mandatory			
	virtual void SetConvexity(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsPerceptual3DShapeType_LocalType();

protected:
	Mp7JrsPerceptual3DShapeType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	Mp7Jrsunsigned15Ptr m_Volume;
	Mp7Jrsunsigned15Ptr m_Center_X;
	Mp7Jrsunsigned15Ptr m_Center_Y;
	Mp7Jrsunsigned15Ptr m_Center_Z;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_1X;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_1Y;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_1Z;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_2X;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_2Y;
	Mp7Jrsunsigned15Ptr m_PCA_Axis_2Z;
	Mp7Jrsunsigned15Ptr m_Max_1;
	Mp7Jrsunsigned15Ptr m_Max_2;
	Mp7Jrsunsigned15Ptr m_Max_3;
	Mp7Jrsunsigned15Ptr m_Convexity;

// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2729MP7JRSPERCEPTUAL3DSHAPETYPE_LOCALTYPE_H

