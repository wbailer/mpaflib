/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3395MP7JRSSPATIOTEMPORALLOCATORTYPE_LOCALTYPE_H
#define _3395MP7JRSSPATIOTEMPORALLOCATORTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSpatioTemporalLocatorType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtInclude.h


class IMp7JrsSpatioTemporalLocatorType_LocalType;
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType_LocalType> Mp7JrsSpatioTemporalLocatorType_LocalPtr;
class IMp7JrsFigureTrajectoryType;
typedef Dc1Ptr< IMp7JrsFigureTrajectoryType > Mp7JrsFigureTrajectoryPtr;
class IMp7JrsParameterTrajectoryType;
typedef Dc1Ptr< IMp7JrsParameterTrajectoryType > Mp7JrsParameterTrajectoryPtr;
class IMp7JrsMediaTimeType;
typedef Dc1Ptr< IMp7JrsMediaTimeType > Mp7JrsMediaTimePtr;

/** 
 * Generated interface IMp7JrsSpatioTemporalLocatorType_LocalType for class Mp7JrsSpatioTemporalLocatorType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsSpatioTemporalLocatorType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsSpatioTemporalLocatorType_LocalType();
	virtual ~IMp7JrsSpatioTemporalLocatorType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get FigureTrajectory element.
	 * @return Mp7JrsFigureTrajectoryPtr
	 */
	virtual Mp7JrsFigureTrajectoryPtr GetFigureTrajectory() const = 0;

	/**
	 * Set FigureTrajectory element.
	 * @param item const Mp7JrsFigureTrajectoryPtr &
	 */
	// Mandatory			
	virtual void SetFigureTrajectory(const Mp7JrsFigureTrajectoryPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ParameterTrajectory element.
	 * @return Mp7JrsParameterTrajectoryPtr
	 */
	virtual Mp7JrsParameterTrajectoryPtr GetParameterTrajectory() const = 0;

	/**
	 * Set ParameterTrajectory element.
	 * @param item const Mp7JrsParameterTrajectoryPtr &
	 */
	// Mandatory			
	virtual void SetParameterTrajectory(const Mp7JrsParameterTrajectoryPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaTime element.
	 * @return Mp7JrsMediaTimePtr
	 */
	virtual Mp7JrsMediaTimePtr GetMediaTime() const = 0;

	/**
	 * Set MediaTime element.
	 * @param item const Mp7JrsMediaTimePtr &
	 */
	// Mandatory			
	virtual void SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSpatioTemporalLocatorType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 1303.<br>
 */
class DC1_EXPORT Mp7JrsSpatioTemporalLocatorType_LocalType :
		public IMp7JrsSpatioTemporalLocatorType_LocalType
{
	friend class Mp7JrsSpatioTemporalLocatorType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get FigureTrajectory element.
	 * @return Mp7JrsFigureTrajectoryPtr
	 */
	virtual Mp7JrsFigureTrajectoryPtr GetFigureTrajectory() const;

	/*
	 * Set FigureTrajectory element.
	 * @param item const Mp7JrsFigureTrajectoryPtr &
	 */
	// Mandatory			
	virtual void SetFigureTrajectory(const Mp7JrsFigureTrajectoryPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ParameterTrajectory element.
	 * @return Mp7JrsParameterTrajectoryPtr
	 */
	virtual Mp7JrsParameterTrajectoryPtr GetParameterTrajectory() const;

	/*
	 * Set ParameterTrajectory element.
	 * @param item const Mp7JrsParameterTrajectoryPtr &
	 */
	// Mandatory			
	virtual void SetParameterTrajectory(const Mp7JrsParameterTrajectoryPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaTime element.
	 * @return Mp7JrsMediaTimePtr
	 */
	virtual Mp7JrsMediaTimePtr GetMediaTime() const;

	/*
	 * Set MediaTime element.
	 * @param item const Mp7JrsMediaTimePtr &
	 */
	// Mandatory			
	virtual void SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSpatioTemporalLocatorType_LocalType();

protected:
	Mp7JrsSpatioTemporalLocatorType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsFigureTrajectoryPtr m_FigureTrajectory;
	Mp7JrsParameterTrajectoryPtr m_ParameterTrajectory;
	Mp7JrsMediaTimePtr m_MediaTime;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3395MP7JRSSPATIOTEMPORALLOCATORTYPE_LOCALTYPE_H

