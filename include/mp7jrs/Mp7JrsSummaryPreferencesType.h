/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3579MP7JRSSUMMARYPREFERENCESTYPE_H
#define _3579MP7JRSSUMMARYPREFERENCESTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSummaryPreferencesTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtInclude.h


class IMp7JrsDSType;
typedef Dc1Ptr< IMp7JrsDSType > Mp7JrsDSPtr;
class IMp7JrsSummaryPreferencesType;
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType> Mp7JrsSummaryPreferencesPtr;
class IMp7JrspreferenceValueType;
typedef Dc1Ptr< IMp7JrspreferenceValueType > Mp7JrspreferenceValuePtr;
class IMp7JrsSummaryPreferencesType_SummaryType_CollectionType;
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryType_CollectionType > Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr;
class IMp7JrsSummaryPreferencesType_SummaryTheme_CollectionType;
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryTheme_CollectionType > Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSummaryPreferencesType for class Mp7JrsSummaryPreferencesType<br>
 * Located at: mpeg7-v4.xsd, line 10103<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSummaryPreferencesType 
 :
		public IMp7JrsDSType
{
public:
	// TODO: make these protected?
	IMp7JrsSummaryPreferencesType();
	virtual ~IMp7JrsSummaryPreferencesType();
	/**
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const = 0;

	/**
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const = 0;

	/**
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get SummaryType element.
	 * @return Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr
	 */
	virtual Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr GetSummaryType() const = 0;

	/**
	 * Set SummaryType element.
	 * @param item const Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummaryType(const Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SummaryTheme element.
	 * @return Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr
	 */
	virtual Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr GetSummaryTheme() const = 0;

	/**
	 * Set SummaryTheme element.
	 * @param item const Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummaryTheme(const Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetSummaryDuration() const = 0;

	virtual bool IsValidSummaryDuration() const = 0;

	/**
	 * Set SummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate SummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSummaryDuration() = 0;

	/**
	 * Get MinSummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMinSummaryDuration() const = 0;

	virtual bool IsValidMinSummaryDuration() const = 0;

	/**
	 * Set MinSummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMinSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MinSummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinSummaryDuration() = 0;

	/**
	 * Get MaxSummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMaxSummaryDuration() const = 0;

	virtual bool IsValidMaxSummaryDuration() const = 0;

	/**
	 * Set MaxSummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMaxSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MaxSummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxSummaryDuration() = 0;

	/**
	 * Get NumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfKeyFrames() const = 0;

	virtual bool IsValidNumOfKeyFrames() const = 0;

	/**
	 * Set NumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate NumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumOfKeyFrames() = 0;

	/**
	 * Get MinNumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetMinNumOfKeyFrames() const = 0;

	virtual bool IsValidMinNumOfKeyFrames() const = 0;

	/**
	 * Set MinNumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetMinNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MinNumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinNumOfKeyFrames() = 0;

	/**
	 * Get MaxNumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetMaxNumOfKeyFrames() const = 0;

	virtual bool IsValidMaxNumOfKeyFrames() const = 0;

	/**
	 * Set MaxNumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetMaxNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MaxNumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxNumOfKeyFrames() = 0;

	/**
	 * Get NumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfChars() const = 0;

	virtual bool IsValidNumOfChars() const = 0;

	/**
	 * Set NumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate NumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumOfChars() = 0;

	/**
	 * Get MinNumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetMinNumOfChars() const = 0;

	virtual bool IsValidMinNumOfChars() const = 0;

	/**
	 * Set MinNumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetMinNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MinNumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinNumOfChars() = 0;

	/**
	 * Get MaxNumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetMaxNumOfChars() const = 0;

	virtual bool IsValidMaxNumOfChars() const = 0;

	/**
	 * Set MaxNumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetMaxNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MaxNumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxNumOfChars() = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SummaryPreferencesType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SummaryType</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SummaryTheme</li>
	 * <li>SummaryDuration</li>
	 * <li>MinSummaryDuration</li>
	 * <li>MaxSummaryDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSummaryPreferencesType<br>
 * Located at: mpeg7-v4.xsd, line 10103<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 10103.<br>
 */
class DC1_EXPORT Mp7JrsSummaryPreferencesType :
		public IMp7JrsSummaryPreferencesType
{
	friend class Mp7JrsSummaryPreferencesTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSummaryPreferencesType
	 
	 * @return Dc1Ptr< Mp7JrsDSType >
	 */
	virtual Dc1Ptr< Mp7JrsDSType > GetBase() const;
	/*
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const;

	/*
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const;

	/*
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get SummaryType element.
	 * @return Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr
	 */
	virtual Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr GetSummaryType() const;

	/*
	 * Set SummaryType element.
	 * @param item const Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummaryType(const Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SummaryTheme element.
	 * @return Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr
	 */
	virtual Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr GetSummaryTheme() const;

	/*
	 * Set SummaryTheme element.
	 * @param item const Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummaryTheme(const Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetSummaryDuration() const;

	virtual bool IsValidSummaryDuration() const;

	/*
	 * Set SummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate SummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateSummaryDuration();

	/*
	 * Get MinSummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMinSummaryDuration() const;

	virtual bool IsValidMinSummaryDuration() const;

	/*
	 * Set MinSummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMinSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MinSummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinSummaryDuration();

	/*
	 * Get MaxSummaryDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMaxSummaryDuration() const;

	virtual bool IsValidMaxSummaryDuration() const;

	/*
	 * Set MaxSummaryDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMaxSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MaxSummaryDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxSummaryDuration();

	/*
	 * Get NumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfKeyFrames() const;

	virtual bool IsValidNumOfKeyFrames() const;

	/*
	 * Set NumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate NumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumOfKeyFrames();

	/*
	 * Get MinNumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetMinNumOfKeyFrames() const;

	virtual bool IsValidMinNumOfKeyFrames() const;

	/*
	 * Set MinNumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetMinNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MinNumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinNumOfKeyFrames();

	/*
	 * Get MaxNumOfKeyFrames element.
	 * @return unsigned
	 */
	virtual unsigned GetMaxNumOfKeyFrames() const;

	virtual bool IsValidMaxNumOfKeyFrames() const;

	/*
	 * Set MaxNumOfKeyFrames element.
	 * @param item unsigned
	 */
	virtual void SetMaxNumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MaxNumOfKeyFrames element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxNumOfKeyFrames();

	/*
	 * Get NumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetNumOfChars() const;

	virtual bool IsValidNumOfChars() const;

	/*
	 * Set NumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate NumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateNumOfChars();

	/*
	 * Get MinNumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetMinNumOfChars() const;

	virtual bool IsValidMinNumOfChars() const;

	/*
	 * Set MinNumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetMinNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MinNumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMinNumOfChars();

	/*
	 * Get MaxNumOfChars element.
	 * @return unsigned
	 */
	virtual unsigned GetMaxNumOfChars() const;

	virtual bool IsValidMaxNumOfChars() const;

	/*
	 * Set MaxNumOfChars element.
	 * @param item unsigned
	 */
	virtual void SetMaxNumOfChars(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MaxNumOfChars element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMaxNumOfChars();

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SummaryPreferencesType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SummaryType</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:SummaryTheme</li>
	 * <li>SummaryDuration</li>
	 * <li>MinSummaryDuration</li>
	 * <li>MaxSummaryDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSummaryPreferencesType();

protected:
	Mp7JrsSummaryPreferencesType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrspreferenceValuePtr m_preferenceValue;
	bool m_preferenceValue_Exist;
	Mp7JrspreferenceValuePtr m_preferenceValue_Default;

	// Base Class
	Dc1Ptr< Mp7JrsDSType > m_Base;

	Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr m_SummaryType;
	Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr m_SummaryTheme;
	Mp7JrsmediaDurationPtr m_SummaryDuration;
	bool m_SummaryDuration_Exist; // For optional elements 
	Mp7JrsmediaDurationPtr m_MinSummaryDuration;
	bool m_MinSummaryDuration_Exist; // For optional elements 
	Mp7JrsmediaDurationPtr m_MaxSummaryDuration;
	bool m_MaxSummaryDuration_Exist; // For optional elements 
	unsigned m_NumOfKeyFrames;
	bool m_NumOfKeyFrames_Exist; // For optional elements 
	unsigned m_MinNumOfKeyFrames;
	bool m_MinNumOfKeyFrames_Exist; // For optional elements 
	unsigned m_MaxNumOfKeyFrames;
	bool m_MaxNumOfKeyFrames_Exist; // For optional elements 
	unsigned m_NumOfChars;
	bool m_NumOfChars_Exist; // For optional elements 
	unsigned m_MinNumOfChars;
	bool m_MinNumOfChars_Exist; // For optional elements 
	unsigned m_MaxNumOfChars;
	bool m_MaxNumOfChars_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3579MP7JRSSUMMARYPREFERENCESTYPE_H

