/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1069MP7JRSANALYTICTRANSITIONTYPE_H
#define _1069MP7JRSANALYTICTRANSITIONTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsAnalyticTransitionTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsAnalyticTransitionType_ExtInclude.h


class IMp7JrsAnalyticEditedVideoSegmentType;
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentType > Mp7JrsAnalyticEditedVideoSegmentPtr;
class IMp7JrsAnalyticTransitionType;
typedef Dc1Ptr< IMp7JrsAnalyticTransitionType> Mp7JrsAnalyticTransitionPtr;
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
class IMp7JrsTermUseType;
typedef Dc1Ptr< IMp7JrsTermUseType > Mp7JrsTermUsePtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
#include "Mp7JrsAnalyticEditedVideoSegmentType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType.h"
class IMp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType;
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType > Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr;
#include "Mp7JrsVideoSegmentType.h"
class IMp7JrsMediaTimeType;
typedef Dc1Ptr< IMp7JrsMediaTimeType > Mp7JrsMediaTimePtr;
class IMp7JrsTemporalMaskType;
typedef Dc1Ptr< IMp7JrsTemporalMaskType > Mp7JrsTemporalMaskPtr;
class IMp7JrsVideoSegmentType_CollectionType;
typedef Dc1Ptr< IMp7JrsVideoSegmentType_CollectionType > Mp7JrsVideoSegmentType_CollectionPtr;
class IMp7JrsMultipleViewType;
typedef Dc1Ptr< IMp7JrsMultipleViewType > Mp7JrsMultipleViewPtr;
class IMp7JrsVideoSegmentType_Mosaic_CollectionType;
typedef Dc1Ptr< IMp7JrsVideoSegmentType_Mosaic_CollectionType > Mp7JrsVideoSegmentType_Mosaic_CollectionPtr;
class IMp7JrsVideoSegmentType_CollectionType0;
typedef Dc1Ptr< IMp7JrsVideoSegmentType_CollectionType0 > Mp7JrsVideoSegmentType_Collection0Ptr;
#include "Mp7JrsSegmentType.h"
class IMp7JrsMediaInformationType;
typedef Dc1Ptr< IMp7JrsMediaInformationType > Mp7JrsMediaInformationPtr;
class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsControlledTermUseType;
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;
class IMp7JrsCreationInformationType;
typedef Dc1Ptr< IMp7JrsCreationInformationType > Mp7JrsCreationInformationPtr;
class IMp7JrsUsageInformationType;
typedef Dc1Ptr< IMp7JrsUsageInformationType > Mp7JrsUsageInformationPtr;
class IMp7JrsSegmentType_TextAnnotation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_CollectionType > Mp7JrsSegmentType_TextAnnotation_CollectionPtr;
class IMp7JrsSegmentType_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_CollectionType > Mp7JrsSegmentType_CollectionPtr;
class IMp7JrsSegmentType_MatchingHint_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_MatchingHint_CollectionType > Mp7JrsSegmentType_MatchingHint_CollectionPtr;
class IMp7JrsSegmentType_PointOfView_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_PointOfView_CollectionType > Mp7JrsSegmentType_PointOfView_CollectionPtr;
class IMp7JrsSegmentType_Relation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_Relation_CollectionType > Mp7JrsSegmentType_Relation_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsAnalyticTransitionType for class Mp7JrsAnalyticTransitionType<br>
 * Located at: mpeg7-v4.xsd, line 8025<br>
 * Classified: Class<br>
 * Derived from: AnalyticEditedVideoSegmentType (Class)<br>
 * Abstract class <br>
 */
class MP7JRS_EXPORT IMp7JrsAnalyticTransitionType 
 :
		public IMp7JrsAnalyticEditedVideoSegmentType
{
public:
	// TODO: make these protected?
	IMp7JrsAnalyticTransitionType();
	virtual ~IMp7JrsAnalyticTransitionType();
	/**
	 * Get evolutionReliability attribute.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GetevolutionReliability() const = 0;

	/**
	 * Get evolutionReliability attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistevolutionReliability() const = 0;

	/**
	 * Set evolutionReliability attribute.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SetevolutionReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate evolutionReliability attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateevolutionReliability() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get EvolutionType element.
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetEvolutionType() const = 0;

	virtual bool IsValidEvolutionType() const = 0;

	/**
	 * Set EvolutionType element.
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetEvolutionType(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate EvolutionType element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEvolutionType() = 0;

	/**
	 * Get EditedMovingRegionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetEditedMovingRegionRef() const = 0;

	virtual bool IsValidEditedMovingRegionRef() const = 0;

	/**
	 * Set EditedMovingRegionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetEditedMovingRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate EditedMovingRegionRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEditedMovingRegionRef() = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get locationReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrszeroToOnePtr
	 * @endif
	 */
	virtual Mp7JrszeroToOnePtr GetlocationReliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get locationReliability attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistlocationReliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set locationReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrszeroToOnePtr &
	 * @endif
	 */
	virtual void SetlocationReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate locationReliability attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatelocationReliability() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get editingLevelReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrszeroToOnePtr
	 * @endif
	 */
	virtual Mp7JrszeroToOnePtr GeteditingLevelReliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get editingLevelReliability attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisteditingLevelReliability() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set editingLevelReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrszeroToOnePtr &
	 * @endif
	 */
	virtual void SeteditingLevelReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate editingLevelReliability attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateeditingLevelReliability() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get type attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration
	 * @endif
	 */
	virtual Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration Gettype() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get type attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existtype() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set type attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration
	 * @endif
	 */
	virtual void Settype(Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate type attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidatetype() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get AnalyticEditionAreaDecomposition element.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr GetAnalyticEditionAreaDecomposition() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set AnalyticEditionAreaDecomposition element.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetAnalyticEditionAreaDecomposition(const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MediaTime element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsMediaTimePtr
	 * @endif
	 */
	virtual Mp7JrsMediaTimePtr GetMediaTime() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MediaTime element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsMediaTimePtr &
	 * @endif
	 */
	virtual void SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get TemporalMask element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsTemporalMaskPtr
	 * @endif
	 */
	virtual Mp7JrsTemporalMaskPtr GetTemporalMask() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set TemporalMask element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsTemporalMaskPtr &
	 * @endif
	 */
	virtual void SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get VideoSegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsVideoSegmentType_CollectionPtr GetVideoSegmentType_LocalType() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set VideoSegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetVideoSegmentType_LocalType(const Mp7JrsVideoSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsMultipleViewPtr
	 * @endif
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const = 0;

	virtual bool IsValidMultipleView() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 * @endif
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateMultipleView() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Mosaic element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_Mosaic_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsVideoSegmentType_Mosaic_CollectionPtr GetMosaic() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Mosaic element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetMosaic(const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get VideoSegmentType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_Collection0Ptr
	 * @endif
	 */
	virtual Mp7JrsVideoSegmentType_Collection0Ptr GetVideoSegmentType_LocalType0() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set VideoSegmentType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_Collection0Ptr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetVideoSegmentType_LocalType0(const Mp7JrsVideoSegmentType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 * @endif
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 * @endif
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 * @endif
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 * @endif
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 * @endif
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 * @endif
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 * @endif
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const = 0;

	virtual bool IsValidStructuralUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 * @endif
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidateStructuralUnit() = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 * @endif
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 * @endif
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 * @endif
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 * @endif
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 * @endif
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 * @endif
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 * @endif
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 * @endif
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 * @endif
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool Existid() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 * @endif
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void Invalidateid() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 * @endif
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 * @endif
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 * @endif
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 * @endif
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 * @endif
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 * @endif
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 * @endif
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * @if Show_All_Abstract_Class_Members
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 * @endif
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * @if Show_All_Abstract_Class_Members
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 * @endif
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * @if Show_All_Abstract_Class_Members
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 * @endif
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of AnalyticTransitionType.
	 * Currently this type contains 30 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>MediaTime</li>
	 * <li>TemporalMask</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>VisualTimeSeriesDescriptor</li>
	 * <li>GofGopFeature</li>
	 * <li>MultipleView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Mosaic</li>
	 * <li>SpatialDecomposition</li>
	 * <li>TemporalDecomposition</li>
	 * <li>SpatioTemporalDecomposition</li>
	 * <li>MediaSourceDecomposition</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AnalyticEditionAreaDecomposition</li>
	 * <li>EvolutionType</li>
	 * <li>EditedMovingRegionRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsAnalyticTransitionType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsAnalyticTransitionType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsAnalyticTransitionType<br>
 * Located at: mpeg7-v4.xsd, line 8025<br>
 * Classified: Class<br>
 * Derived from: AnalyticEditedVideoSegmentType (Class)<br>
 * Abstract class <br>
 * Defined in mpeg7-v4.xsd, line 8025.<br>
 */
class DC1_EXPORT Mp7JrsAnalyticTransitionType :
		public IMp7JrsAnalyticTransitionType
{
	friend class Mp7JrsAnalyticTransitionTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsAnalyticTransitionType
	 
	 * @return Dc1Ptr< Mp7JrsAnalyticEditedVideoSegmentType >
	 */
	virtual Dc1Ptr< Mp7JrsAnalyticEditedVideoSegmentType > GetBase() const;
	/*
	 * Get evolutionReliability attribute.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GetevolutionReliability() const;

	/*
	 * Get evolutionReliability attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistevolutionReliability() const;

	/*
	 * Set evolutionReliability attribute.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SetevolutionReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate evolutionReliability attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateevolutionReliability();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get EvolutionType element.
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetEvolutionType() const;

	virtual bool IsValidEvolutionType() const;

	/*
	 * Set EvolutionType element.
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetEvolutionType(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate EvolutionType element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEvolutionType();

	/*
	 * Get EditedMovingRegionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetEditedMovingRegionRef() const;

	virtual bool IsValidEditedMovingRegionRef() const;

	/*
	 * Set EditedMovingRegionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetEditedMovingRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate EditedMovingRegionRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateEditedMovingRegionRef();

		//@}
	/*
	 * Get locationReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GetlocationReliability() const;

	/*
	 * Get locationReliability attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlocationReliability() const;

	/*
	 * Set locationReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SetlocationReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate locationReliability attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelocationReliability();

	/*
	 * Get editingLevelReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr GeteditingLevelReliability() const;

	/*
	 * Get editingLevelReliability attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisteditingLevelReliability() const;

	/*
	 * Set editingLevelReliability attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void SeteditingLevelReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate editingLevelReliability attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateeditingLevelReliability();

	/*
	 * Get type attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration
	 */
	virtual Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration Gettype() const;

	/*
	 * Get type attribute validity information.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration
	 */
	virtual void Settype(Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get AnalyticEditionAreaDecomposition element.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @return Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr
	 */
	virtual Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr GetAnalyticEditionAreaDecomposition() const;

	/*
	 * Set AnalyticEditionAreaDecomposition element.
<br>
	 * (Inherited from Mp7JrsAnalyticEditedVideoSegmentType)
	 * @param item const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAnalyticEditionAreaDecomposition(const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaTime element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsMediaTimePtr
	 */
	virtual Mp7JrsMediaTimePtr GetMediaTime() const;

	/*
	 * Set MediaTime element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsMediaTimePtr &
	 */
	virtual void SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TemporalMask element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsTemporalMaskPtr
	 */
	virtual Mp7JrsTemporalMaskPtr GetTemporalMask() const;

	/*
	 * Set TemporalMask element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsTemporalMaskPtr &
	 */
	virtual void SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get VideoSegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_CollectionPtr
	 */
	virtual Mp7JrsVideoSegmentType_CollectionPtr GetVideoSegmentType_LocalType() const;

	/*
	 * Set VideoSegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetVideoSegmentType_LocalType(const Mp7JrsVideoSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsMultipleViewPtr
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const;

	virtual bool IsValidMultipleView() const;

	/*
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsVideoSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMultipleView();

	/*
	 * Get Mosaic element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_Mosaic_CollectionPtr
	 */
	virtual Mp7JrsVideoSegmentType_Mosaic_CollectionPtr GetMosaic() const;

	/*
	 * Set Mosaic element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMosaic(const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VideoSegmentType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @return Mp7JrsVideoSegmentType_Collection0Ptr
	 */
	virtual Mp7JrsVideoSegmentType_Collection0Ptr GetVideoSegmentType_LocalType0() const;

	/*
	 * Set VideoSegmentType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsVideoSegmentType)
	 * @param item const Mp7JrsVideoSegmentType_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetVideoSegmentType_LocalType0(const Mp7JrsVideoSegmentType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const;

	/*
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const;

	/*
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const;

	virtual bool IsValidStructuralUnit() const;

	/*
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuralUnit();

		/* @name Choice

		 */
		//@{
	/*
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const;

	/*
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const;

	/*
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const;

	/*
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const;

	/*
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const;

	/*
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const;

	/*
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const;

	/*
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const;

	/*
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const;

	/*
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of AnalyticTransitionType.
	 * Currently this type contains 30 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>MediaTime</li>
	 * <li>TemporalMask</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>VisualTimeSeriesDescriptor</li>
	 * <li>GofGopFeature</li>
	 * <li>MultipleView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Mosaic</li>
	 * <li>SpatialDecomposition</li>
	 * <li>TemporalDecomposition</li>
	 * <li>SpatioTemporalDecomposition</li>
	 * <li>MediaSourceDecomposition</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AnalyticEditionAreaDecomposition</li>
	 * <li>EvolutionType</li>
	 * <li>EditedMovingRegionRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsAnalyticTransitionType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsAnalyticTransitionType();

protected:
	Mp7JrsAnalyticTransitionType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrszeroToOnePtr m_evolutionReliability;
	bool m_evolutionReliability_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsAnalyticEditedVideoSegmentType > m_Base;

	Mp7JrsTermUsePtr m_EvolutionType;
	bool m_EvolutionType_Exist; // For optional elements 
	Mp7JrsReferencePtr m_EditedMovingRegionRef;
	bool m_EditedMovingRegionRef_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsAnalyticTransitionType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1069MP7JRSANALYTICTRANSITIONTYPE_H

