/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1927MP7JRSFILTERINGTYPE_H
#define _1927MP7JRSFILTERINGTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsFilteringTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtInclude.h


class IMp7JrsFilteringType;
typedef Dc1Ptr< IMp7JrsFilteringType> Mp7JrsFilteringPtr;
class IMp7JrspaddingType;
typedef Dc1Ptr< IMp7JrspaddingType > Mp7JrspaddingPtr;
class IMp7JrsFilterType;
typedef Dc1Ptr< IMp7JrsFilterType > Mp7JrsFilterPtr;
class IMp7JrsSignalPlaneSampleType;
typedef Dc1Ptr< IMp7JrsSignalPlaneSampleType > Mp7JrsSignalPlaneSamplePtr;

/** 
 * Generated interface IMp7JrsFilteringType for class Mp7JrsFilteringType<br>
 * Located at: mpeg7-v4.xsd, line 8728<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsFilteringType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsFilteringType();
	virtual ~IMp7JrsFilteringType();
	/**
	 * Get xPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetxPad() const = 0;

	/**
	 * Get xPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistxPad() const = 0;

	/**
	 * Set xPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetxPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate xPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatexPad() = 0;

	/**
	 * Get yPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetyPad() const = 0;

	/**
	 * Get yPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistyPad() const = 0;

	/**
	 * Set yPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetyPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate yPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateyPad() = 0;

	/**
	 * Get zPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetzPad() const = 0;

	/**
	 * Get zPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistzPad() const = 0;

	/**
	 * Set zPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetzPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate zPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatezPad() = 0;

	/**
	 * Get tPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GettPad() const = 0;

	/**
	 * Get tPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttPad() const = 0;

	/**
	 * Set tPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SettPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate tPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetPad() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Filter element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsFilter1DPtr</li>
	 * <li>IMp7JrsFilter2DPtr</li>
	 * <li>IMp7JrsFilterSeparablePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetFilter() const = 0;

	/**
	 * Set Filter element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsFilter1DPtr</li>
	 * <li>IMp7JrsFilter2DPtr</li>
	 * <li>IMp7JrsFilterSeparablePtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsFilter1DPtr
	// Mp7JrsFilter2DPtr
	// Mp7JrsFilterSeparablePtr
	virtual void SetFilter(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PadSize element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetPadSize() const = 0;

	virtual bool IsValidPadSize() const = 0;

	/**
	 * Set PadSize element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetPadSize(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PadSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePadSize() = 0;

	/**
	 * Get Shift element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetShift() const = 0;

	virtual bool IsValidShift() const = 0;

	/**
	 * Set Shift element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetShift(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Shift element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateShift() = 0;

	/**
	 * Get CropStart element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetCropStart() const = 0;

	virtual bool IsValidCropStart() const = 0;

	/**
	 * Set CropStart element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetCropStart(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate CropStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCropStart() = 0;

	/**
	 * Get CropEnd element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetCropEnd() const = 0;

	virtual bool IsValidCropEnd() const = 0;

	/**
	 * Set CropEnd element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetCropEnd(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate CropEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCropEnd() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of FilteringType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Filter</li>
	 * <li>PadSize</li>
	 * <li>Shift</li>
	 * <li>CropStart</li>
	 * <li>CropEnd</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsFilteringType<br>
 * Located at: mpeg7-v4.xsd, line 8728<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 8728.<br>
 */
class DC1_EXPORT Mp7JrsFilteringType :
		public IMp7JrsFilteringType
{
	friend class Mp7JrsFilteringTypeFactory; // constructs objects

public:
	/*
	 * Get xPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetxPad() const;

	/*
	 * Get xPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistxPad() const;

	/*
	 * Set xPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetxPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate xPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatexPad();

	/*
	 * Get yPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetyPad() const;

	/*
	 * Get yPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistyPad() const;

	/*
	 * Set yPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetyPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate yPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateyPad();

	/*
	 * Get zPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GetzPad() const;

	/*
	 * Get zPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistzPad() const;

	/*
	 * Set zPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SetzPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate zPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatezPad();

	/*
	 * Get tPad attribute.
	 * @return Mp7JrspaddingPtr
	 */
	virtual Mp7JrspaddingPtr GettPad() const;

	/*
	 * Get tPad attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttPad() const;

	/*
	 * Set tPad attribute.
	 * @param item const Mp7JrspaddingPtr &
	 */
	virtual void SettPad(const Mp7JrspaddingPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate tPad attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetPad();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Filter element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsFilter1DPtr</li>
	 * <li>IMp7JrsFilter2DPtr</li>
	 * <li>IMp7JrsFilterSeparablePtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetFilter() const;

	/*
	 * Set Filter element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsFilter1DPtr</li>
	 * <li>IMp7JrsFilter2DPtr</li>
	 * <li>IMp7JrsFilterSeparablePtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsFilter1DPtr
	// Mp7JrsFilter2DPtr
	// Mp7JrsFilterSeparablePtr
	virtual void SetFilter(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PadSize element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetPadSize() const;

	virtual bool IsValidPadSize() const;

	/*
	 * Set PadSize element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetPadSize(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PadSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePadSize();

	/*
	 * Get Shift element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetShift() const;

	virtual bool IsValidShift() const;

	/*
	 * Set Shift element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetShift(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Shift element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateShift();

	/*
	 * Get CropStart element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetCropStart() const;

	virtual bool IsValidCropStart() const;

	/*
	 * Set CropStart element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetCropStart(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate CropStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCropStart();

	/*
	 * Get CropEnd element.
	 * @return Mp7JrsSignalPlaneSamplePtr
	 */
	virtual Mp7JrsSignalPlaneSamplePtr GetCropEnd() const;

	virtual bool IsValidCropEnd() const;

	/*
	 * Set CropEnd element.
	 * @param item const Mp7JrsSignalPlaneSamplePtr &
	 */
	virtual void SetCropEnd(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate CropEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateCropEnd();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of FilteringType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>Filter</li>
	 * <li>PadSize</li>
	 * <li>Shift</li>
	 * <li>CropStart</li>
	 * <li>CropEnd</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsFilteringType();

protected:
	Mp7JrsFilteringType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrspaddingPtr m_xPad;
	bool m_xPad_Exist;
	Mp7JrspaddingPtr m_xPad_Default;
	Mp7JrspaddingPtr m_yPad;
	bool m_yPad_Exist;
	Mp7JrspaddingPtr m_yPad_Default;
	Mp7JrspaddingPtr m_zPad;
	bool m_zPad_Exist;
	Mp7JrspaddingPtr m_zPad_Default;
	Mp7JrspaddingPtr m_tPad;
	bool m_tPad_Exist;
	Mp7JrspaddingPtr m_tPad_Default;


	Dc1Ptr< Dc1Node > m_Filter;
	Mp7JrsSignalPlaneSamplePtr m_PadSize;
	bool m_PadSize_Exist; // For optional elements 
	Mp7JrsSignalPlaneSamplePtr m_Shift;
	bool m_Shift_Exist; // For optional elements 
	Mp7JrsSignalPlaneSamplePtr m_CropStart;
	bool m_CropStart_Exist; // For optional elements 
	Mp7JrsSignalPlaneSamplePtr m_CropEnd;
	bool m_CropEnd_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1927MP7JRSFILTERINGTYPE_H

