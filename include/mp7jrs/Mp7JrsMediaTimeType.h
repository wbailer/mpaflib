/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2411MP7JRSMEDIATIMETYPE_H
#define _2411MP7JRSMEDIATIMETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMediaTimeTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// begin extension included
// file Mp7JrsMediaTimeType_ExtInclude.h

class Mp7JrsmediaTimePointType;
class Mp7JrsmediaDurationType;
#include "Mp7JrsmediaTimePointType.h"
// end extension included


class IMp7JrsMediaTimeType;
typedef Dc1Ptr< IMp7JrsMediaTimeType> Mp7JrsMediaTimePtr;
class IMp7JrsmediaTimePointType;
typedef Dc1Ptr< IMp7JrsmediaTimePointType > Mp7JrsmediaTimePointPtr;
class IMp7JrsMediaRelTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelTimePointType > Mp7JrsMediaRelTimePointPtr;
class IMp7JrsMediaRelIncrTimePointType;
typedef Dc1Ptr< IMp7JrsMediaRelIncrTimePointType > Mp7JrsMediaRelIncrTimePointPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsMediaIncrDurationType;
typedef Dc1Ptr< IMp7JrsMediaIncrDurationType > Mp7JrsMediaIncrDurationPtr;

/** 
 * Generated interface IMp7JrsMediaTimeType for class Mp7JrsMediaTimeType<br>
 * Located at: mpeg7-v4.xsd, line 4389<br>
 * Classified: Class<br>
 */
class MP7JRS_EXPORT IMp7JrsMediaTimeType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsMediaTimeType();
	virtual ~IMp7JrsMediaTimeType();
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaTimePoint element.
	 * If Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute == true
	 * then
	 * the MediaRelIncrTime is converted into absolute MediaTimePoint
	 * using the mediaTimeUnit and mediaTimeBase looked up in DOM.
	 * See extension information in documentation.
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const = 0;

	/**
	 * Set MediaTimePoint element.
	 * If Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute == true
	 * then
	 * the MediaRelIncrTime is converted into absolute MediaTimePoint
	 * using the mediaTimeUnit and mediaTimeBase looked up in DOM.
	 * See extension information in documentation.
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelTimePoint element.
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const = 0;

	/**
	 * Set MediaRelTimePoint element.
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaRelIncrTimePoint element.
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const = 0;

	/**
	 * Set MediaRelIncrTimePoint element.
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMediaDuration() const = 0;

	/**
	 * Set MediaDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMediaDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaIncrDuration element.
	 * @return Mp7JrsMediaIncrDurationPtr
	 */
	virtual Mp7JrsMediaIncrDurationPtr GetMediaIncrDuration() const = 0;

	/**
	 * Set MediaIncrDuration element.
	 * @param item const Mp7JrsMediaIncrDurationPtr &
	 */
	virtual void SetMediaIncrDuration(const Mp7JrsMediaIncrDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of MediaTimeType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>MediaDuration</li>
	 * <li>MediaIncrDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// begin extension included
// file Mp7JrsMediaTimeType_ExtMethodDef.h


public:

	/** Get the begin time.
	  * @return Pointer to begin time.
	  */
	virtual Mp7JrsmediaTimePointPtr GetBeginTime();

	/** Set begin time.
	  *	@param beginTime Begin time to be set (will be copied).
	  */
	virtual void SetBeginTime(const Mp7JrsmediaTimePointPtr &beginTime, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get the end time.
	  * @return Pointer to end time.
	  */
	virtual Mp7JrsmediaTimePointPtr GetEndTime();

	/** Set end time.
	  *	@param endTime End time to be set (will be copied).
	  */
	virtual void SetEndTime(const Mp7JrsmediaTimePointPtr &endTime, Dc1ClientID client = DC1_UNDEFINED_CLIENT); 

	/** Get begin and end time. 
	  * beginTime and endTime contain the time points after return.
	  */
	virtual void GetTime(Mp7JrsmediaTimePointPtr& beginTime, Mp7JrsmediaTimePointPtr& endTime);

	/** Set begin and end time. The time points will be copied. */
	virtual void SetTime(const Mp7JrsmediaTimePointPtr &beginTime, const Mp7JrsmediaTimePointPtr &endTime, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Checks whether the given time point is within the media time. */
	virtual bool IsInside(const Mp7JrsmediaTimePointPtr &timePoint);

	/** Checks if there is a related mediaTimeBase and/or a related mediaTimeUnit 
	 *  If not found, searches the DOM
	 *  If no timebase can be found, assumes 0 as default timebase.
	 */
	virtual void resolveBaseAndUnit(Mp7JrsmediaTimePointPtr &base, Mp7JrsmediaDurationPtr &unit) = 0;
// end extension included


// begin extension included
// file Mp7JrsMediaTimeType_ExtPropDef.h
private:
	Mp7JrsmediaTimePointPtr  _endTime;   // end time
// end extension included

};




/*
 * Generated class Mp7JrsMediaTimeType<br>
 * Located at: mpeg7-v4.xsd, line 4389<br>
 * Classified: Class<br>
 * Defined in mpeg7-v4.xsd, line 4389.<br>
 */
class DC1_EXPORT Mp7JrsMediaTimeType :
		public IMp7JrsMediaTimeType
{
	friend class Mp7JrsMediaTimeTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaTimePoint element.
	 * If Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute == true
	 * then
	 * the MediaRelIncrTime is converted into absolute MediaTimePoint
	 * using the mediaTimeUnit and mediaTimeBase looked up in DOM.
	 * See extension information in documentation.
	 * @return Mp7JrsmediaTimePointPtr
	 */
	virtual Mp7JrsmediaTimePointPtr GetMediaTimePoint() const;

	/*
	 * Set MediaTimePoint element.
	 * If Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute == true
	 * then
	 * the MediaRelIncrTime is converted into absolute MediaTimePoint
	 * using the mediaTimeUnit and mediaTimeBase looked up in DOM.
	 * See extension information in documentation.
	 * @param item const Mp7JrsmediaTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelTimePoint element.
	 * @return Mp7JrsMediaRelTimePointPtr
	 */
	virtual Mp7JrsMediaRelTimePointPtr GetMediaRelTimePoint() const;

	/*
	 * Set MediaRelTimePoint element.
	 * @param item const Mp7JrsMediaRelTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaRelIncrTimePoint element.
	 * @return Mp7JrsMediaRelIncrTimePointPtr
	 */
	virtual Mp7JrsMediaRelIncrTimePointPtr GetMediaRelIncrTimePoint() const;

	/*
	 * Set MediaRelIncrTimePoint element.
	 * @param item const Mp7JrsMediaRelIncrTimePointPtr &
	 */
	// Mandatory			
	virtual void SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaDuration element.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetMediaDuration() const;

	/*
	 * Set MediaDuration element.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetMediaDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaIncrDuration element.
	 * @return Mp7JrsMediaIncrDurationPtr
	 */
	virtual Mp7JrsMediaIncrDurationPtr GetMediaIncrDuration() const;

	/*
	 * Set MediaIncrDuration element.
	 * @param item const Mp7JrsMediaIncrDurationPtr &
	 */
	virtual void SetMediaIncrDuration(const Mp7JrsMediaIncrDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of MediaTimeType.
	 * Currently this type contains 5 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>MediaTimePoint</li>
	 * <li>MediaRelTimePoint</li>
	 * <li>MediaRelIncrTimePoint</li>
	 * <li>MediaDuration</li>
	 * <li>MediaIncrDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// begin extension included
// file Mp7JrsMediaTimeType_ExtMyMethodDef.h
public:
	virtual void resolveBaseAndUnit(Mp7JrsmediaTimePointPtr &base, Mp7JrsmediaDurationPtr &unit);	
// end extension included


public:

	virtual ~Mp7JrsMediaTimeType();

protected:
	Mp7JrsMediaTimeType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsmediaTimePointPtr m_MediaTimePoint;
	Mp7JrsMediaRelTimePointPtr m_MediaRelTimePoint;
	Mp7JrsMediaRelIncrTimePointPtr m_MediaRelIncrTimePoint;
// End DefinionChoice
// DefinionChoice
	Mp7JrsmediaDurationPtr m_MediaDuration;
	Mp7JrsMediaIncrDurationPtr m_MediaIncrDuration;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsMediaTimeType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2411MP7JRSMEDIATIMETYPE_H

