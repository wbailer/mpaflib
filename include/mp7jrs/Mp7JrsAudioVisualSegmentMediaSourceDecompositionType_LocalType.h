/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1193MP7JRSAUDIOVISUALSEGMENTMEDIASOURCEDECOMPOSITIONTYPE_LOCALTYPE_H
#define _1193MP7JRSAUDIOVISUALSEGMENTMEDIASOURCEDECOMPOSITIONTYPE_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType_ExtInclude.h


class IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType;
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType> Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalPtr;
class IMp7JrsAudioVisualSegmentType;
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentType > Mp7JrsAudioVisualSegmentPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsVideoSegmentType;
typedef Dc1Ptr< IMp7JrsVideoSegmentType > Mp7JrsVideoSegmentPtr;
class IMp7JrsStillRegionType;
typedef Dc1Ptr< IMp7JrsStillRegionType > Mp7JrsStillRegionPtr;
class IMp7JrsAudioSegmentType;
typedef Dc1Ptr< IMp7JrsAudioSegmentType > Mp7JrsAudioSegmentPtr;

/** 
 * Generated interface IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType for class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType();
	virtual ~IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get AudioVisualSegment element.
	 * @return Mp7JrsAudioVisualSegmentPtr
	 */
	virtual Mp7JrsAudioVisualSegmentPtr GetAudioVisualSegment() const = 0;

	/**
	 * Set AudioVisualSegment element.
	 * @param item const Mp7JrsAudioVisualSegmentPtr &
	 */
	// Mandatory			
	virtual void SetAudioVisualSegment(const Mp7JrsAudioVisualSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AudioVisualSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAudioVisualSegmentRef() const = 0;

	/**
	 * Set AudioVisualSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetAudioVisualSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VideoSegment element.
	 * @return Mp7JrsVideoSegmentPtr
	 */
	virtual Mp7JrsVideoSegmentPtr GetVideoSegment() const = 0;

	/**
	 * Set VideoSegment element.
	 * @param item const Mp7JrsVideoSegmentPtr &
	 */
	// Mandatory			
	virtual void SetVideoSegment(const Mp7JrsVideoSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get VideoSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetVideoSegmentRef() const = 0;

	/**
	 * Set VideoSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetVideoSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get StillRegion element.
	 * @return Mp7JrsStillRegionPtr
	 */
	virtual Mp7JrsStillRegionPtr GetStillRegion() const = 0;

	/**
	 * Set StillRegion element.
	 * @param item const Mp7JrsStillRegionPtr &
	 */
	// Mandatory			
	virtual void SetStillRegion(const Mp7JrsStillRegionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get StillRegionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetStillRegionRef() const = 0;

	/**
	 * Set StillRegionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetStillRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AudioSegment element.
	 * @return Mp7JrsAudioSegmentPtr
	 */
	virtual Mp7JrsAudioSegmentPtr GetAudioSegment() const = 0;

	/**
	 * Set AudioSegment element.
	 * @param item const Mp7JrsAudioSegmentPtr &
	 */
	// Mandatory			
	virtual void SetAudioSegment(const Mp7JrsAudioSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AudioSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAudioSegmentRef() const = 0;

	/**
	 * Set AudioSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetAudioSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 7483.<br>
 */
class DC1_EXPORT Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType :
		public IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType
{
	friend class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get AudioVisualSegment element.
	 * @return Mp7JrsAudioVisualSegmentPtr
	 */
	virtual Mp7JrsAudioVisualSegmentPtr GetAudioVisualSegment() const;

	/*
	 * Set AudioVisualSegment element.
	 * @param item const Mp7JrsAudioVisualSegmentPtr &
	 */
	// Mandatory			
	virtual void SetAudioVisualSegment(const Mp7JrsAudioVisualSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AudioVisualSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAudioVisualSegmentRef() const;

	/*
	 * Set AudioVisualSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetAudioVisualSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VideoSegment element.
	 * @return Mp7JrsVideoSegmentPtr
	 */
	virtual Mp7JrsVideoSegmentPtr GetVideoSegment() const;

	/*
	 * Set VideoSegment element.
	 * @param item const Mp7JrsVideoSegmentPtr &
	 */
	// Mandatory			
	virtual void SetVideoSegment(const Mp7JrsVideoSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get VideoSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetVideoSegmentRef() const;

	/*
	 * Set VideoSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetVideoSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get StillRegion element.
	 * @return Mp7JrsStillRegionPtr
	 */
	virtual Mp7JrsStillRegionPtr GetStillRegion() const;

	/*
	 * Set StillRegion element.
	 * @param item const Mp7JrsStillRegionPtr &
	 */
	// Mandatory			
	virtual void SetStillRegion(const Mp7JrsStillRegionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get StillRegionRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetStillRegionRef() const;

	/*
	 * Set StillRegionRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetStillRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AudioSegment element.
	 * @return Mp7JrsAudioSegmentPtr
	 */
	virtual Mp7JrsAudioSegmentPtr GetAudioSegment() const;

	/*
	 * Set AudioSegment element.
	 * @param item const Mp7JrsAudioSegmentPtr &
	 */
	// Mandatory			
	virtual void SetAudioSegment(const Mp7JrsAudioSegmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AudioSegmentRef element.
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetAudioSegmentRef() const;

	/*
	 * Set AudioSegmentRef element.
	 * @param item const Mp7JrsReferencePtr &
	 */
	// Mandatory			
	virtual void SetAudioSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType();

protected:
	Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	Mp7JrsAudioVisualSegmentPtr m_AudioVisualSegment;
	Mp7JrsReferencePtr m_AudioVisualSegmentRef;
	Mp7JrsVideoSegmentPtr m_VideoSegment;
	Mp7JrsReferencePtr m_VideoSegmentRef;
	Mp7JrsStillRegionPtr m_StillRegion;
	Mp7JrsReferencePtr m_StillRegionRef;
	Mp7JrsAudioSegmentPtr m_AudioSegment;
	Mp7JrsReferencePtr m_AudioSegmentRef;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1193MP7JRSAUDIOVISUALSEGMENTMEDIASOURCEDECOMPOSITIONTYPE_LOCALTYPE_H

