/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _3569MP7JRSSUMMARYCOMPONENTTYPE_LOCALTYPE_H
#define _3569MP7JRSSUMMARYCOMPONENTTYPE_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file Mp7JrssummaryComponentType_LocalType_ExtInclude.h




/**
 * Generated enumeration Mp7JrssummaryComponentType_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 10142<br>
 * Enumerations: <br>
 * audio (mapped to XML audio) <br>
 * visual (mapped to XML visual) <br>
 * audioVisual (mapped to XML audioVisual) <br>
 * textual (mapped to XML textual) <br>
 * keyAudioClips (mapped to XML keyAudioClips) <br>
 * keyVideoClips (mapped to XML keyVideoClips) <br>
 * keyAudioVisualClips (mapped to XML keyAudioVisualClips) <br>
 * keyFrames (mapped to XML keyFrames) <br>
 * keySounds (mapped to XML keySounds) <br>
 * keyThemes (mapped to XML keyThemes) <br>
 */
class DC1_EXPORT Mp7JrssummaryComponentType_LocalType
{
public:
	/** Mp7JrssummaryComponentType_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		audio, // Mapped to audio
		visual, // Mapped to visual
		audioVisual, // Mapped to audioVisual
		textual, // Mapped to textual
		keyAudioClips, // Mapped to keyAudioClips
		keyVideoClips, // Mapped to keyVideoClips
		keyAudioVisualClips, // Mapped to keyAudioVisualClips
		keyFrames, // Mapped to keyFrames
		keySounds, // Mapped to keySounds
		keyThemes, // Mapped to keyThemes
	};
	

// no includefile for extension defined 
// file Mp7JrssummaryComponentType_LocalType_ExtMethodDef.h


	static XMLCh * ToText(Mp7JrssummaryComponentType_LocalType::Enumeration item);
	static Mp7JrssummaryComponentType_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file Mp7JrssummaryComponentType_LocalType_ExtPropDef.h



};
#endif // _3569MP7JRSSUMMARYCOMPONENTTYPE_LOCALTYPE_H
