/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3069MP7JRSRELTIMEPOINTTYPE_H
#define _3069MP7JRSRELTIMEPOINTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsRelTimePointTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtInclude.h


class IMp7JrsRelTimePointType;
typedef Dc1Ptr< IMp7JrsRelTimePointType> Mp7JrsRelTimePointPtr;
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
#include "Mp7JrstimeOffsetType.h"

/** 
 * Generated interface IMp7JrsRelTimePointType for class Mp7JrsRelTimePointType<br>
 * Located at: mpeg7-v4.xsd, line 4358<br>
 * Classified: Class<br>
 * Derived from: timeOffsetType (Pattern)<br>
 */
class MP7JRS_EXPORT IMp7JrsRelTimePointType 
 :
		public IMp7JrstimeOffsetType
{
public:
	// TODO: make these protected?
	IMp7JrsRelTimePointType();
	virtual ~IMp7JrsRelTimePointType();
	/**
	 * Get timeBase attribute.
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get OffsetSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetOffsetSign() const = 0;

	/**
	 * Set OffsetSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetOffsetSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Days element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetDays() const = 0;

	/**
	 * Set Days element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetDays(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Hours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetHours() const = 0;

	/**
	 * Set Hours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Minutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetMinutes() const = 0;

	/**
	 * Set Minutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Seconds element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetSeconds() const = 0;

	/**
	 * Set Seconds element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetSeconds(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetNumberOfFractions() const = 0;

	/**
	 * Set NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetNumberOfFractions(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Time_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTime_Exist() const = 0;

	/**
	 * Set Time_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTime_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetFractionsPerSecond() const = 0;

	/**
	 * Set FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetFractionsPerSecond(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get TimezoneSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneSign() const = 0;

	/**
	 * Set TimezoneSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get TimezoneHours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneHours() const = 0;

	/**
	 * Set TimezoneHours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get TimezoneMinutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneMinutes() const = 0;

	/**
	 * Set TimezoneMinutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Timezone_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTimezone_Exist() const = 0;

	/**
	 * Set Timezone_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTimezone_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of RelTimePointType.
	 * Currently just supports rudimentary XPath expressions as RelTimePointType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsRelTimePointType<br>
 * Located at: mpeg7-v4.xsd, line 4358<br>
 * Classified: Class<br>
 * Derived from: timeOffsetType (Pattern)<br>
 * Defined in mpeg7-v4.xsd, line 4358.<br>
 */
class DC1_EXPORT Mp7JrsRelTimePointType :
		public IMp7JrsRelTimePointType
{
	friend class Mp7JrsRelTimePointTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsRelTimePointType
	 
	 * @return Dc1Ptr< Mp7JrstimeOffsetType >
	 */
	virtual Dc1Ptr< Mp7JrstimeOffsetType > GetBase() const;
	/*
	 * Get timeBase attribute.
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get OffsetSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetOffsetSign() const;

	/*
	 * Set OffsetSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetOffsetSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Days element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetDays() const;

	/*
	 * Set Days element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetDays(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Hours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetHours() const;

	/*
	 * Set Hours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Minutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetMinutes() const;

	/*
	 * Set Minutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Seconds element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetSeconds() const;

	/*
	 * Set Seconds element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetSeconds(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetNumberOfFractions() const;

	/*
	 * Set NumberOfFractions element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetNumberOfFractions(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Time_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTime_Exist() const;

	/*
	 * Set Time_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTime_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetFractionsPerSecond() const;

	/*
	 * Set FractionsPerSecond element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetFractionsPerSecond(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TimezoneSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneSign() const;

	/*
	 * Set TimezoneSign element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneSign(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TimezoneHours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneHours() const;

	/*
	 * Set TimezoneHours element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneHours(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TimezoneMinutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return int
	 */
	virtual int GetTimezoneMinutes() const;

	/*
	 * Set TimezoneMinutes element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item int
	 */
	// Mandatory			
	virtual void SetTimezoneMinutes(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Timezone_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @return bool
	 */
	virtual bool GetTimezone_Exist() const;

	/*
	 * Set Timezone_Exist element.
<br>
	 * (Inherited from Mp7JrstimeOffsetType)
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetTimezone_Exist(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Gets or creates Dc1NodePtr child elements of RelTimePointType.
	 * Currently just supports rudimentary XPath expressions as RelTimePointType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsRelTimePointType();

protected:
	Mp7JrsRelTimePointType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsxPathRefPtr m_timeBase;
	bool m_timeBase_Exist;

	// Base Pattern
	Dc1Ptr< Mp7JrstimeOffsetType > m_Base;


// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3069MP7JRSRELTIMEPOINTTYPE_H

