/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _2447MP7JRSMETERTYPE_DENOMINATOR_LOCALTYPE_H
#define _2447MP7JRSMETERTYPE_DENOMINATOR_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file Mp7JrsMeterType_Denominator_LocalType_ExtInclude.h




/**
 * Generated enumeration Mp7JrsMeterType_Denominator_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 2596<br>
 * Enumerations: <br>
 * _1 (mapped to XML 1) <br>
 * _2 (mapped to XML 2) <br>
 * _4 (mapped to XML 4) <br>
 * _8 (mapped to XML 8) <br>
 * _16 (mapped to XML 16) <br>
 * _32 (mapped to XML 32) <br>
 * _64 (mapped to XML 64) <br>
 * _128 (mapped to XML 128) <br>
 */
class DC1_EXPORT Mp7JrsMeterType_Denominator_LocalType
{
public:
	/** Mp7JrsMeterType_Denominator_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		_1, // Mapped to 1
		_2, // Mapped to 2
		_4, // Mapped to 4
		_8, // Mapped to 8
		_16, // Mapped to 16
		_32, // Mapped to 32
		_64, // Mapped to 64
		_128, // Mapped to 128
	};
	

// no includefile for extension defined 
// file Mp7JrsMeterType_Denominator_LocalType_ExtMethodDef.h


	static XMLCh * ToText(Mp7JrsMeterType_Denominator_LocalType::Enumeration item);
	static Mp7JrsMeterType_Denominator_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file Mp7JrsMeterType_Denominator_LocalType_ExtPropDef.h



};
#endif // _2447MP7JRSMETERTYPE_DENOMINATOR_LOCALTYPE_H
