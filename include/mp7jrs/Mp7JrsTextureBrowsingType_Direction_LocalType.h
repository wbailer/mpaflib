/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _3735MP7JRSTEXTUREBROWSINGTYPE_DIRECTION_LOCALTYPE_H
#define _3735MP7JRSTEXTUREBROWSINGTYPE_DIRECTION_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_Direction_LocalType_ExtInclude.h




/**
 * Generated enumeration Mp7JrsTextureBrowsingType_Direction_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 851<br>
 * Enumerations: <br>
 * noDirectionality (mapped to XML noDirectionality) <br>
 * _0Degree (mapped to XML 0Degree) <br>
 * _30Degree (mapped to XML 30Degree) <br>
 * _60Degree (mapped to XML 60Degree) <br>
 * _90Degree (mapped to XML 90Degree) <br>
 * _120Degree (mapped to XML 120Degree) <br>
 * _150Degree (mapped to XML 150Degree) <br>
 */
class DC1_EXPORT Mp7JrsTextureBrowsingType_Direction_LocalType
{
public:
	/** Mp7JrsTextureBrowsingType_Direction_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		noDirectionality, // Mapped to noDirectionality
		_0Degree, // Mapped to 0Degree
		_30Degree, // Mapped to 30Degree
		_60Degree, // Mapped to 60Degree
		_90Degree, // Mapped to 90Degree
		_120Degree, // Mapped to 120Degree
		_150Degree, // Mapped to 150Degree
	};
	

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_Direction_LocalType_ExtMethodDef.h


	static XMLCh * ToText(Mp7JrsTextureBrowsingType_Direction_LocalType::Enumeration item);
	static Mp7JrsTextureBrowsingType_Direction_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_Direction_LocalType_ExtPropDef.h



};
#endif // _3735MP7JRSTEXTUREBROWSINGTYPE_DIRECTION_LOCALTYPE_H
