/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef MP7JRSFACTORYDEFINES_H
#define MP7JRSFACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for extension classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfAbstractionLevelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AbstractionLevelType")))
#define CreateAbstractionLevelType (Dc1Factory::CreateObject(TypeOfAbstractionLevelType))
class Mp7JrsAbstractionLevelType;
class IMp7JrsAbstractionLevelType;
/** Smart pointer for instance of IMp7JrsAbstractionLevelType */
typedef Dc1Ptr< IMp7JrsAbstractionLevelType > Mp7JrsAbstractionLevelPtr;

#define TypeOfAccessUnitType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AccessUnitType")))
#define CreateAccessUnitType (Dc1Factory::CreateObject(TypeOfAccessUnitType))
class Mp7JrsAccessUnitType;
class IMp7JrsAccessUnitType;
/** Smart pointer for instance of IMp7JrsAccessUnitType */
typedef Dc1Ptr< IMp7JrsAccessUnitType > Mp7JrsAccessUnitPtr;

#define TypeOfAccessUnitType_FragmentUpdateUnit_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AccessUnitType_FragmentUpdateUnit_CollectionType")))
#define CreateAccessUnitType_FragmentUpdateUnit_CollectionType (Dc1Factory::CreateObject(TypeOfAccessUnitType_FragmentUpdateUnit_CollectionType))
class Mp7JrsAccessUnitType_FragmentUpdateUnit_CollectionType;
class IMp7JrsAccessUnitType_FragmentUpdateUnit_CollectionType;
/** Smart pointer for instance of IMp7JrsAccessUnitType_FragmentUpdateUnit_CollectionType */
typedef Dc1Ptr< IMp7JrsAccessUnitType_FragmentUpdateUnit_CollectionType > Mp7JrsAccessUnitType_FragmentUpdateUnit_CollectionPtr;

#define TypeOfAcquaintanceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AcquaintanceType")))
#define CreateAcquaintanceType (Dc1Factory::CreateObject(TypeOfAcquaintanceType))
class Mp7JrsAcquaintanceType;
class IMp7JrsAcquaintanceType;
/** Smart pointer for instance of IMp7JrsAcquaintanceType */
typedef Dc1Ptr< IMp7JrsAcquaintanceType > Mp7JrsAcquaintancePtr;

#define TypeOfAdvancedFaceRecognitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType")))
#define CreateAdvancedFaceRecognitionType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType))
class Mp7JrsAdvancedFaceRecognitionType;
class IMp7JrsAdvancedFaceRecognitionType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType > Mp7JrsAdvancedFaceRecognitionPtr;

#define TypeOfAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CentralFourierFeature_CollectionType")))
#define CreateAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType))
class Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType;
class IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionType > Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_CollectionPtr;

#define TypeOfAdvancedFaceRecognitionType_CentralFourierFeature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CentralFourierFeature_LocalType")))
#define CreateAdvancedFaceRecognitionType_CentralFourierFeature_LocalType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_CentralFourierFeature_LocalType))
class Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalType;
class IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalType > Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr;

#define TypeOfAdvancedFaceRecognitionType_CompositeFeature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CompositeFeature_CollectionType")))
#define CreateAdvancedFaceRecognitionType_CompositeFeature_CollectionType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_CompositeFeature_CollectionType))
class Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionType;
class IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionType > Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_CollectionPtr;

#define TypeOfAdvancedFaceRecognitionType_CompositeFeature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CompositeFeature_LocalType")))
#define CreateAdvancedFaceRecognitionType_CompositeFeature_LocalType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_CompositeFeature_LocalType))
class Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalType;
class IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalType > Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr;

#define TypeOfAdvancedFaceRecognitionType_FourierFeature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_FourierFeature_CollectionType")))
#define CreateAdvancedFaceRecognitionType_FourierFeature_CollectionType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_FourierFeature_CollectionType))
class Mp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionType;
class IMp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionType > Mp7JrsAdvancedFaceRecognitionType_FourierFeature_CollectionPtr;

#define TypeOfAdvancedFaceRecognitionType_FourierFeature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_FourierFeature_LocalType")))
#define CreateAdvancedFaceRecognitionType_FourierFeature_LocalType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_FourierFeature_LocalType))
class Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalType;
class IMp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalType > Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr;

#define TypeOfAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType")))
#define CreateAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType))
class Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType;
class IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionType > Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_CollectionPtr;

#define TypeOfAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType")))
#define CreateAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType (Dc1Factory::CreateObject(TypeOfAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType))
class Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType;
class IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType;
/** Smart pointer for instance of IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType */
typedef Dc1Ptr< IMp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType > Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr;

#define TypeOfAffectiveType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AffectiveType")))
#define CreateAffectiveType (Dc1Factory::CreateObject(TypeOfAffectiveType))
class Mp7JrsAffectiveType;
class IMp7JrsAffectiveType;
/** Smart pointer for instance of IMp7JrsAffectiveType */
typedef Dc1Ptr< IMp7JrsAffectiveType > Mp7JrsAffectivePtr;

#define TypeOfAffectiveType_Score_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AffectiveType_Score_CollectionType")))
#define CreateAffectiveType_Score_CollectionType (Dc1Factory::CreateObject(TypeOfAffectiveType_Score_CollectionType))
class Mp7JrsAffectiveType_Score_CollectionType;
class IMp7JrsAffectiveType_Score_CollectionType;
/** Smart pointer for instance of IMp7JrsAffectiveType_Score_CollectionType */
typedef Dc1Ptr< IMp7JrsAffectiveType_Score_CollectionType > Mp7JrsAffectiveType_Score_CollectionPtr;

#define TypeOfAffectiveType_Score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AffectiveType_Score_LocalType")))
#define CreateAffectiveType_Score_LocalType (Dc1Factory::CreateObject(TypeOfAffectiveType_Score_LocalType))
class Mp7JrsAffectiveType_Score_LocalType;
class IMp7JrsAffectiveType_Score_LocalType;
/** Smart pointer for instance of IMp7JrsAffectiveType_Score_LocalType */
typedef Dc1Ptr< IMp7JrsAffectiveType_Score_LocalType > Mp7JrsAffectiveType_Score_LocalPtr;

#define TypeOfAffiliationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AffiliationType")))
#define CreateAffiliationType (Dc1Factory::CreateObject(TypeOfAffiliationType))
class Mp7JrsAffiliationType;
class IMp7JrsAffiliationType;
/** Smart pointer for instance of IMp7JrsAffiliationType */
typedef Dc1Ptr< IMp7JrsAffiliationType > Mp7JrsAffiliationPtr;

#define TypeOfAgentObjectType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AgentObjectType")))
#define CreateAgentObjectType (Dc1Factory::CreateObject(TypeOfAgentObjectType))
class Mp7JrsAgentObjectType;
class IMp7JrsAgentObjectType;
/** Smart pointer for instance of IMp7JrsAgentObjectType */
typedef Dc1Ptr< IMp7JrsAgentObjectType > Mp7JrsAgentObjectPtr;

#define TypeOfAgentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AgentType")))
#define CreateAgentType (Dc1Factory::CreateObject(TypeOfAgentType))
class Mp7JrsAgentType;
class IMp7JrsAgentType;
/** Smart pointer for instance of IMp7JrsAgentType */
typedef Dc1Ptr< IMp7JrsAgentType > Mp7JrsAgentPtr;

#define TypeOfAgentType_Icon_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AgentType_Icon_CollectionType")))
#define CreateAgentType_Icon_CollectionType (Dc1Factory::CreateObject(TypeOfAgentType_Icon_CollectionType))
class Mp7JrsAgentType_Icon_CollectionType;
class IMp7JrsAgentType_Icon_CollectionType;
/** Smart pointer for instance of IMp7JrsAgentType_Icon_CollectionType */
typedef Dc1Ptr< IMp7JrsAgentType_Icon_CollectionType > Mp7JrsAgentType_Icon_CollectionPtr;

#define TypeOfAggregatedMediaReviewDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AggregatedMediaReviewDescriptionType")))
#define CreateAggregatedMediaReviewDescriptionType (Dc1Factory::CreateObject(TypeOfAggregatedMediaReviewDescriptionType))
class Mp7JrsAggregatedMediaReviewDescriptionType;
class IMp7JrsAggregatedMediaReviewDescriptionType;
/** Smart pointer for instance of IMp7JrsAggregatedMediaReviewDescriptionType */
typedef Dc1Ptr< IMp7JrsAggregatedMediaReviewDescriptionType > Mp7JrsAggregatedMediaReviewDescriptionPtr;

#define TypeOfAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType")))
#define CreateAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType (Dc1Factory::CreateObject(TypeOfAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType))
class Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType;
class IMp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType;
/** Smart pointer for instance of IMp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType */
typedef Dc1Ptr< IMp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType > Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr;

#define TypeOfAnalyticClipType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticClipType")))
#define CreateAnalyticClipType (Dc1Factory::CreateObject(TypeOfAnalyticClipType))
class Mp7JrsAnalyticClipType;
class IMp7JrsAnalyticClipType;
/** Smart pointer for instance of IMp7JrsAnalyticClipType */
typedef Dc1Ptr< IMp7JrsAnalyticClipType > Mp7JrsAnalyticClipPtr;

#define TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentSpatioTemporalDecompositionType")))
#define CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType))
class Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType;
class IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType > Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr;

#define TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType")))
#define CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType))
class Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType;
class IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType > Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr;

#define TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType")))
#define CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType))
class Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType;
class IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType > Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr;

#define TypeOfAnalyticEditedVideoSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentType")))
#define CreateAnalyticEditedVideoSegmentType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentType))
class Mp7JrsAnalyticEditedVideoSegmentType;
class IMp7JrsAnalyticEditedVideoSegmentType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoSegmentType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentType > Mp7JrsAnalyticEditedVideoSegmentPtr;

#define TypeOfAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType")))
#define CreateAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType))
class Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType;
class IMp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType > Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr;

#define TypeOfAnalyticEditedVideoSegmentType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoSegmentType_type_LocalType")))
#define CreateAnalyticEditedVideoSegmentType_type_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoSegmentType_type_LocalType))
class Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType;
class IMp7JrsAnalyticEditedVideoSegmentType_type_LocalType;
// No smart pointer instance for IMp7JrsAnalyticEditedVideoSegmentType_type_LocalType

#define TypeOfAnalyticEditedVideoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticEditedVideoType")))
#define CreateAnalyticEditedVideoType (Dc1Factory::CreateObject(TypeOfAnalyticEditedVideoType))
class Mp7JrsAnalyticEditedVideoType;
class IMp7JrsAnalyticEditedVideoType;
/** Smart pointer for instance of IMp7JrsAnalyticEditedVideoType */
typedef Dc1Ptr< IMp7JrsAnalyticEditedVideoType > Mp7JrsAnalyticEditedVideoPtr;

#define TypeOfAnalyticModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType")))
#define CreateAnalyticModelType (Dc1Factory::CreateObject(TypeOfAnalyticModelType))
class Mp7JrsAnalyticModelType;
class IMp7JrsAnalyticModelType;
/** Smart pointer for instance of IMp7JrsAnalyticModelType */
typedef Dc1Ptr< IMp7JrsAnalyticModelType > Mp7JrsAnalyticModelPtr;

#define TypeOfAnalyticModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_CollectionType")))
#define CreateAnalyticModelType_CollectionType (Dc1Factory::CreateObject(TypeOfAnalyticModelType_CollectionType))
class Mp7JrsAnalyticModelType_CollectionType;
class IMp7JrsAnalyticModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsAnalyticModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsAnalyticModelType_CollectionType > Mp7JrsAnalyticModelType_CollectionPtr;

#define TypeOfAnalyticModelType_function_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_function_LocalType")))
#define CreateAnalyticModelType_function_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticModelType_function_LocalType))
class Mp7JrsAnalyticModelType_function_LocalType;
class IMp7JrsAnalyticModelType_function_LocalType;
// No smart pointer instance for IMp7JrsAnalyticModelType_function_LocalType

#define TypeOfAnalyticModelType_Label_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Label_LocalType")))
#define CreateAnalyticModelType_Label_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticModelType_Label_LocalType))
class Mp7JrsAnalyticModelType_Label_LocalType;
class IMp7JrsAnalyticModelType_Label_LocalType;
/** Smart pointer for instance of IMp7JrsAnalyticModelType_Label_LocalType */
typedef Dc1Ptr< IMp7JrsAnalyticModelType_Label_LocalType > Mp7JrsAnalyticModelType_Label_LocalPtr;

#define TypeOfAnalyticModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_LocalType")))
#define CreateAnalyticModelType_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticModelType_LocalType))
class Mp7JrsAnalyticModelType_LocalType;
class IMp7JrsAnalyticModelType_LocalType;
/** Smart pointer for instance of IMp7JrsAnalyticModelType_LocalType */
typedef Dc1Ptr< IMp7JrsAnalyticModelType_LocalType > Mp7JrsAnalyticModelType_LocalPtr;

#define TypeOfAnalyticModelType_Semantics_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Semantics_LocalType")))
#define CreateAnalyticModelType_Semantics_LocalType (Dc1Factory::CreateObject(TypeOfAnalyticModelType_Semantics_LocalType))
class Mp7JrsAnalyticModelType_Semantics_LocalType;
class IMp7JrsAnalyticModelType_Semantics_LocalType;
/** Smart pointer for instance of IMp7JrsAnalyticModelType_Semantics_LocalType */
typedef Dc1Ptr< IMp7JrsAnalyticModelType_Semantics_LocalType > Mp7JrsAnalyticModelType_Semantics_LocalPtr;

#define TypeOfAnalyticTransitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticTransitionType")))
#define CreateAnalyticTransitionType (Dc1Factory::CreateObject(TypeOfAnalyticTransitionType))
class Mp7JrsAnalyticTransitionType;
class IMp7JrsAnalyticTransitionType;
/** Smart pointer for instance of IMp7JrsAnalyticTransitionType */
typedef Dc1Ptr< IMp7JrsAnalyticTransitionType > Mp7JrsAnalyticTransitionPtr;

#define TypeOfAudioBPMType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioBPMType")))
#define CreateAudioBPMType (Dc1Factory::CreateObject(TypeOfAudioBPMType))
class Mp7JrsAudioBPMType;
class IMp7JrsAudioBPMType;
/** Smart pointer for instance of IMp7JrsAudioBPMType */
typedef Dc1Ptr< IMp7JrsAudioBPMType > Mp7JrsAudioBPMPtr;

#define TypeOfAudioChordPatternDS (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioChordPatternDS")))
#define CreateAudioChordPatternDS (Dc1Factory::CreateObject(TypeOfAudioChordPatternDS))
class Mp7JrsAudioChordPatternDS;
class IMp7JrsAudioChordPatternDS;
/** Smart pointer for instance of IMp7JrsAudioChordPatternDS */
typedef Dc1Ptr< IMp7JrsAudioChordPatternDS > Mp7JrsAudioChordPatternDSPtr;

#define TypeOfAudioChordPatternDS_Chord_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioChordPatternDS_Chord_CollectionType")))
#define CreateAudioChordPatternDS_Chord_CollectionType (Dc1Factory::CreateObject(TypeOfAudioChordPatternDS_Chord_CollectionType))
class Mp7JrsAudioChordPatternDS_Chord_CollectionType;
class IMp7JrsAudioChordPatternDS_Chord_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioChordPatternDS_Chord_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioChordPatternDS_Chord_CollectionType > Mp7JrsAudioChordPatternDS_Chord_CollectionPtr;

#define TypeOfAudioChordPatternDS_Chord_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioChordPatternDS_Chord_LocalType")))
#define CreateAudioChordPatternDS_Chord_LocalType (Dc1Factory::CreateObject(TypeOfAudioChordPatternDS_Chord_LocalType))
class Mp7JrsAudioChordPatternDS_Chord_LocalType;
class IMp7JrsAudioChordPatternDS_Chord_LocalType;
/** Smart pointer for instance of IMp7JrsAudioChordPatternDS_Chord_LocalType */
typedef Dc1Ptr< IMp7JrsAudioChordPatternDS_Chord_LocalType > Mp7JrsAudioChordPatternDS_Chord_LocalPtr;

#define TypeOfAudioDSType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioDSType")))
#define CreateAudioDSType (Dc1Factory::CreateObject(TypeOfAudioDSType))
class Mp7JrsAudioDSType;
class IMp7JrsAudioDSType;
/** Smart pointer for instance of IMp7JrsAudioDSType */
typedef Dc1Ptr< IMp7JrsAudioDSType > Mp7JrsAudioDSPtr;

#define TypeOfAudioDType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioDType")))
#define CreateAudioDType (Dc1Factory::CreateObject(TypeOfAudioDType))
class Mp7JrsAudioDType;
class IMp7JrsAudioDType;
/** Smart pointer for instance of IMp7JrsAudioDType */
typedef Dc1Ptr< IMp7JrsAudioDType > Mp7JrsAudioDPtr;

#define TypeOfAudioFundamentalFrequencyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioFundamentalFrequencyType")))
#define CreateAudioFundamentalFrequencyType (Dc1Factory::CreateObject(TypeOfAudioFundamentalFrequencyType))
class Mp7JrsAudioFundamentalFrequencyType;
class IMp7JrsAudioFundamentalFrequencyType;
/** Smart pointer for instance of IMp7JrsAudioFundamentalFrequencyType */
typedef Dc1Ptr< IMp7JrsAudioFundamentalFrequencyType > Mp7JrsAudioFundamentalFrequencyPtr;

#define TypeOfAudioHarmonicityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioHarmonicityType")))
#define CreateAudioHarmonicityType (Dc1Factory::CreateObject(TypeOfAudioHarmonicityType))
class Mp7JrsAudioHarmonicityType;
class IMp7JrsAudioHarmonicityType;
/** Smart pointer for instance of IMp7JrsAudioHarmonicityType */
typedef Dc1Ptr< IMp7JrsAudioHarmonicityType > Mp7JrsAudioHarmonicityPtr;

#define TypeOfAudioLLDScalarType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDScalarType")))
#define CreateAudioLLDScalarType (Dc1Factory::CreateObject(TypeOfAudioLLDScalarType))
class Mp7JrsAudioLLDScalarType;
class IMp7JrsAudioLLDScalarType;
/** Smart pointer for instance of IMp7JrsAudioLLDScalarType */
typedef Dc1Ptr< IMp7JrsAudioLLDScalarType > Mp7JrsAudioLLDScalarPtr;

#define TypeOfAudioLLDScalarType_SeriesOfScalar_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDScalarType_SeriesOfScalar_CollectionType")))
#define CreateAudioLLDScalarType_SeriesOfScalar_CollectionType (Dc1Factory::CreateObject(TypeOfAudioLLDScalarType_SeriesOfScalar_CollectionType))
class Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType;
class IMp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType > Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr;

#define TypeOfAudioLLDScalarType_SeriesOfScalar_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDScalarType_SeriesOfScalar_LocalType")))
#define CreateAudioLLDScalarType_SeriesOfScalar_LocalType (Dc1Factory::CreateObject(TypeOfAudioLLDScalarType_SeriesOfScalar_LocalType))
class Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType;
class IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType;
/** Smart pointer for instance of IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType */
typedef Dc1Ptr< IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType > Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalPtr;

#define TypeOfAudioLLDVectorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDVectorType")))
#define CreateAudioLLDVectorType (Dc1Factory::CreateObject(TypeOfAudioLLDVectorType))
class Mp7JrsAudioLLDVectorType;
class IMp7JrsAudioLLDVectorType;
/** Smart pointer for instance of IMp7JrsAudioLLDVectorType */
typedef Dc1Ptr< IMp7JrsAudioLLDVectorType > Mp7JrsAudioLLDVectorPtr;

#define TypeOfAudioLLDVectorType_SeriesOfVector_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDVectorType_SeriesOfVector_CollectionType")))
#define CreateAudioLLDVectorType_SeriesOfVector_CollectionType (Dc1Factory::CreateObject(TypeOfAudioLLDVectorType_SeriesOfVector_CollectionType))
class Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType;
class IMp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType > Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr;

#define TypeOfAudioLLDVectorType_SeriesOfVector_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDVectorType_SeriesOfVector_LocalType")))
#define CreateAudioLLDVectorType_SeriesOfVector_LocalType (Dc1Factory::CreateObject(TypeOfAudioLLDVectorType_SeriesOfVector_LocalType))
class Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalType;
class IMp7JrsAudioLLDVectorType_SeriesOfVector_LocalType;
/** Smart pointer for instance of IMp7JrsAudioLLDVectorType_SeriesOfVector_LocalType */
typedef Dc1Ptr< IMp7JrsAudioLLDVectorType_SeriesOfVector_LocalType > Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalPtr;

#define TypeOfAudioPowerType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioPowerType")))
#define CreateAudioPowerType (Dc1Factory::CreateObject(TypeOfAudioPowerType))
class Mp7JrsAudioPowerType;
class IMp7JrsAudioPowerType;
/** Smart pointer for instance of IMp7JrsAudioPowerType */
typedef Dc1Ptr< IMp7JrsAudioPowerType > Mp7JrsAudioPowerPtr;

#define TypeOfAudioRhythmicPatternDS (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioRhythmicPatternDS")))
#define CreateAudioRhythmicPatternDS (Dc1Factory::CreateObject(TypeOfAudioRhythmicPatternDS))
class Mp7JrsAudioRhythmicPatternDS;
class IMp7JrsAudioRhythmicPatternDS;
/** Smart pointer for instance of IMp7JrsAudioRhythmicPatternDS */
typedef Dc1Ptr< IMp7JrsAudioRhythmicPatternDS > Mp7JrsAudioRhythmicPatternDSPtr;

#define TypeOfAudioRhythmicPatternDS_SinglePattern_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioRhythmicPatternDS_SinglePattern_LocalType")))
#define CreateAudioRhythmicPatternDS_SinglePattern_LocalType (Dc1Factory::CreateObject(TypeOfAudioRhythmicPatternDS_SinglePattern_LocalType))
class Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType;
class IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType;
/** Smart pointer for instance of IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType */
typedef Dc1Ptr< IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType > Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalPtr;

#define TypeOfAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType")))
#define CreateAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType (Dc1Factory::CreateObject(TypeOfAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType))
class Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType;
class IMp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType;
/** Smart pointer for instance of IMp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType */
typedef Dc1Ptr< IMp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType > Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr;

#define TypeOfAudioSegmentMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentMediaSourceDecompositionType")))
#define CreateAudioSegmentMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfAudioSegmentMediaSourceDecompositionType))
class Mp7JrsAudioSegmentMediaSourceDecompositionType;
class IMp7JrsAudioSegmentMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioSegmentMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioSegmentMediaSourceDecompositionType > Mp7JrsAudioSegmentMediaSourceDecompositionPtr;

#define TypeOfAudioSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentMediaSourceDecompositionType_CollectionType")))
#define CreateAudioSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioSegmentMediaSourceDecompositionType_CollectionType))
class Mp7JrsAudioSegmentMediaSourceDecompositionType_CollectionType;
class IMp7JrsAudioSegmentMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioSegmentMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioSegmentMediaSourceDecompositionType_CollectionType > Mp7JrsAudioSegmentMediaSourceDecompositionType_CollectionPtr;

#define TypeOfAudioSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentMediaSourceDecompositionType_LocalType")))
#define CreateAudioSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioSegmentMediaSourceDecompositionType_LocalType))
class Mp7JrsAudioSegmentMediaSourceDecompositionType_LocalType;
class IMp7JrsAudioSegmentMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioSegmentMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioSegmentMediaSourceDecompositionType_LocalType > Mp7JrsAudioSegmentMediaSourceDecompositionType_LocalPtr;

#define TypeOfAudioSegmentTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentTemporalDecompositionType")))
#define CreateAudioSegmentTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAudioSegmentTemporalDecompositionType))
class Mp7JrsAudioSegmentTemporalDecompositionType;
class IMp7JrsAudioSegmentTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioSegmentTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioSegmentTemporalDecompositionType > Mp7JrsAudioSegmentTemporalDecompositionPtr;

#define TypeOfAudioSegmentTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentTemporalDecompositionType_CollectionType")))
#define CreateAudioSegmentTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioSegmentTemporalDecompositionType_CollectionType))
class Mp7JrsAudioSegmentTemporalDecompositionType_CollectionType;
class IMp7JrsAudioSegmentTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioSegmentTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioSegmentTemporalDecompositionType_CollectionType > Mp7JrsAudioSegmentTemporalDecompositionType_CollectionPtr;

#define TypeOfAudioSegmentTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentTemporalDecompositionType_LocalType")))
#define CreateAudioSegmentTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioSegmentTemporalDecompositionType_LocalType))
class Mp7JrsAudioSegmentTemporalDecompositionType_LocalType;
class IMp7JrsAudioSegmentTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioSegmentTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioSegmentTemporalDecompositionType_LocalType > Mp7JrsAudioSegmentTemporalDecompositionType_LocalPtr;

#define TypeOfAudioSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType")))
#define CreateAudioSegmentType (Dc1Factory::CreateObject(TypeOfAudioSegmentType))
class Mp7JrsAudioSegmentType;
class IMp7JrsAudioSegmentType;
/** Smart pointer for instance of IMp7JrsAudioSegmentType */
typedef Dc1Ptr< IMp7JrsAudioSegmentType > Mp7JrsAudioSegmentPtr;

#define TypeOfAudioSegmentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType_CollectionType")))
#define CreateAudioSegmentType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioSegmentType_CollectionType))
class Mp7JrsAudioSegmentType_CollectionType;
class IMp7JrsAudioSegmentType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioSegmentType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioSegmentType_CollectionType > Mp7JrsAudioSegmentType_CollectionPtr;

#define TypeOfAudioSegmentType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType_CollectionType0")))
#define CreateAudioSegmentType_CollectionType0 (Dc1Factory::CreateObject(TypeOfAudioSegmentType_CollectionType0))
class Mp7JrsAudioSegmentType_CollectionType0;
class IMp7JrsAudioSegmentType_CollectionType0;
/** Smart pointer for instance of IMp7JrsAudioSegmentType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsAudioSegmentType_CollectionType0 > Mp7JrsAudioSegmentType_Collection0Ptr;

#define TypeOfAudioSegmentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType_LocalType")))
#define CreateAudioSegmentType_LocalType (Dc1Factory::CreateObject(TypeOfAudioSegmentType_LocalType))
class Mp7JrsAudioSegmentType_LocalType;
class IMp7JrsAudioSegmentType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioSegmentType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioSegmentType_LocalType > Mp7JrsAudioSegmentType_LocalPtr;

#define TypeOfAudioSegmentType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType_LocalType0")))
#define CreateAudioSegmentType_LocalType0 (Dc1Factory::CreateObject(TypeOfAudioSegmentType_LocalType0))
class Mp7JrsAudioSegmentType_LocalType0;
class IMp7JrsAudioSegmentType_LocalType0;
/** Smart pointer for instance of IMp7JrsAudioSegmentType_LocalType0 */
typedef Dc1Ptr< IMp7JrsAudioSegmentType_LocalType0 > Mp7JrsAudioSegmentType_Local0Ptr;

#define TypeOfAudioSignalQualityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignalQualityType")))
#define CreateAudioSignalQualityType (Dc1Factory::CreateObject(TypeOfAudioSignalQualityType))
class Mp7JrsAudioSignalQualityType;
class IMp7JrsAudioSignalQualityType;
/** Smart pointer for instance of IMp7JrsAudioSignalQualityType */
typedef Dc1Ptr< IMp7JrsAudioSignalQualityType > Mp7JrsAudioSignalQualityPtr;

#define TypeOfAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType")))
#define CreateAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType (Dc1Factory::CreateObject(TypeOfAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType))
class Mp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType;
class IMp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionType > Mp7JrsAudioSignalQualityType_ErrorEventList_ErrorEvent_CollectionPtr;

#define TypeOfAudioSignalQualityType_ErrorEventList_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignalQualityType_ErrorEventList_LocalType")))
#define CreateAudioSignalQualityType_ErrorEventList_LocalType (Dc1Factory::CreateObject(TypeOfAudioSignalQualityType_ErrorEventList_LocalType))
class Mp7JrsAudioSignalQualityType_ErrorEventList_LocalType;
class IMp7JrsAudioSignalQualityType_ErrorEventList_LocalType;
/** Smart pointer for instance of IMp7JrsAudioSignalQualityType_ErrorEventList_LocalType */
typedef Dc1Ptr< IMp7JrsAudioSignalQualityType_ErrorEventList_LocalType > Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr;

#define TypeOfAudioSignatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignatureType")))
#define CreateAudioSignatureType (Dc1Factory::CreateObject(TypeOfAudioSignatureType))
class Mp7JrsAudioSignatureType;
class IMp7JrsAudioSignatureType;
/** Smart pointer for instance of IMp7JrsAudioSignatureType */
typedef Dc1Ptr< IMp7JrsAudioSignatureType > Mp7JrsAudioSignaturePtr;

#define TypeOfaudioSpectrumAttributeGrp_octaveResolution_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:audioSpectrumAttributeGrp_octaveResolution_LocalType")))
#define CreateaudioSpectrumAttributeGrp_octaveResolution_LocalType (Dc1Factory::CreateObject(TypeOfaudioSpectrumAttributeGrp_octaveResolution_LocalType))
class Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType;
class IMp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType;
// No smart pointer instance for IMp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType

#define TypeOfAudioSpectrumBasisType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumBasisType")))
#define CreateAudioSpectrumBasisType (Dc1Factory::CreateObject(TypeOfAudioSpectrumBasisType))
class Mp7JrsAudioSpectrumBasisType;
class IMp7JrsAudioSpectrumBasisType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumBasisType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumBasisType > Mp7JrsAudioSpectrumBasisPtr;

#define TypeOfAudioSpectrumCentroidType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumCentroidType")))
#define CreateAudioSpectrumCentroidType (Dc1Factory::CreateObject(TypeOfAudioSpectrumCentroidType))
class Mp7JrsAudioSpectrumCentroidType;
class IMp7JrsAudioSpectrumCentroidType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumCentroidType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumCentroidType > Mp7JrsAudioSpectrumCentroidPtr;

#define TypeOfAudioSpectrumEnvelopeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumEnvelopeType")))
#define CreateAudioSpectrumEnvelopeType (Dc1Factory::CreateObject(TypeOfAudioSpectrumEnvelopeType))
class Mp7JrsAudioSpectrumEnvelopeType;
class IMp7JrsAudioSpectrumEnvelopeType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumEnvelopeType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumEnvelopeType > Mp7JrsAudioSpectrumEnvelopePtr;

#define TypeOfAudioSpectrumFlatnessType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumFlatnessType")))
#define CreateAudioSpectrumFlatnessType (Dc1Factory::CreateObject(TypeOfAudioSpectrumFlatnessType))
class Mp7JrsAudioSpectrumFlatnessType;
class IMp7JrsAudioSpectrumFlatnessType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumFlatnessType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumFlatnessType > Mp7JrsAudioSpectrumFlatnessPtr;

#define TypeOfAudioSpectrumProjectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumProjectionType")))
#define CreateAudioSpectrumProjectionType (Dc1Factory::CreateObject(TypeOfAudioSpectrumProjectionType))
class Mp7JrsAudioSpectrumProjectionType;
class IMp7JrsAudioSpectrumProjectionType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumProjectionType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumProjectionType > Mp7JrsAudioSpectrumProjectionPtr;

#define TypeOfAudioSpectrumSpreadType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumSpreadType")))
#define CreateAudioSpectrumSpreadType (Dc1Factory::CreateObject(TypeOfAudioSpectrumSpreadType))
class Mp7JrsAudioSpectrumSpreadType;
class IMp7JrsAudioSpectrumSpreadType;
/** Smart pointer for instance of IMp7JrsAudioSpectrumSpreadType */
typedef Dc1Ptr< IMp7JrsAudioSpectrumSpreadType > Mp7JrsAudioSpectrumSpreadPtr;

#define TypeOfAudioSummaryComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSummaryComponentType")))
#define CreateAudioSummaryComponentType (Dc1Factory::CreateObject(TypeOfAudioSummaryComponentType))
class Mp7JrsAudioSummaryComponentType;
class IMp7JrsAudioSummaryComponentType;
/** Smart pointer for instance of IMp7JrsAudioSummaryComponentType */
typedef Dc1Ptr< IMp7JrsAudioSummaryComponentType > Mp7JrsAudioSummaryComponentPtr;

#define TypeOfAudioSummaryComponentType_Title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSummaryComponentType_Title_CollectionType")))
#define CreateAudioSummaryComponentType_Title_CollectionType (Dc1Factory::CreateObject(TypeOfAudioSummaryComponentType_Title_CollectionType))
class Mp7JrsAudioSummaryComponentType_Title_CollectionType;
class IMp7JrsAudioSummaryComponentType_Title_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioSummaryComponentType_Title_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioSummaryComponentType_Title_CollectionType > Mp7JrsAudioSummaryComponentType_Title_CollectionPtr;

#define TypeOfAudioTempoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioTempoType")))
#define CreateAudioTempoType (Dc1Factory::CreateObject(TypeOfAudioTempoType))
class Mp7JrsAudioTempoType;
class IMp7JrsAudioTempoType;
/** Smart pointer for instance of IMp7JrsAudioTempoType */
typedef Dc1Ptr< IMp7JrsAudioTempoType > Mp7JrsAudioTempoPtr;

#define TypeOfAudioType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioType")))
#define CreateAudioType (Dc1Factory::CreateObject(TypeOfAudioType))
class Mp7JrsAudioType;
class IMp7JrsAudioType;
/** Smart pointer for instance of IMp7JrsAudioType */
typedef Dc1Ptr< IMp7JrsAudioType > Mp7JrsAudioPtr;

#define TypeOfAudioVisualRegionMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionMediaSourceDecompositionType")))
#define CreateAudioVisualRegionMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionMediaSourceDecompositionType))
class Mp7JrsAudioVisualRegionMediaSourceDecompositionType;
class IMp7JrsAudioVisualRegionMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionMediaSourceDecompositionType > Mp7JrsAudioVisualRegionMediaSourceDecompositionPtr;

#define TypeOfAudioVisualRegionMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionMediaSourceDecompositionType_CollectionType")))
#define CreateAudioVisualRegionMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionMediaSourceDecompositionType_CollectionType))
class Mp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionType;
class IMp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionType > Mp7JrsAudioVisualRegionMediaSourceDecompositionType_CollectionPtr;

#define TypeOfAudioVisualRegionMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionMediaSourceDecompositionType_LocalType")))
#define CreateAudioVisualRegionMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionMediaSourceDecompositionType_LocalType))
class Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType;
class IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType > Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalPtr;

#define TypeOfAudioVisualRegionSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatialDecompositionType")))
#define CreateAudioVisualRegionSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatialDecompositionType))
class Mp7JrsAudioVisualRegionSpatialDecompositionType;
class IMp7JrsAudioVisualRegionSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatialDecompositionType > Mp7JrsAudioVisualRegionSpatialDecompositionPtr;

#define TypeOfAudioVisualRegionSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatialDecompositionType_CollectionType")))
#define CreateAudioVisualRegionSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatialDecompositionType_CollectionType))
class Mp7JrsAudioVisualRegionSpatialDecompositionType_CollectionType;
class IMp7JrsAudioVisualRegionSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatialDecompositionType_CollectionType > Mp7JrsAudioVisualRegionSpatialDecompositionType_CollectionPtr;

#define TypeOfAudioVisualRegionSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatialDecompositionType_LocalType")))
#define CreateAudioVisualRegionSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatialDecompositionType_LocalType))
class Mp7JrsAudioVisualRegionSpatialDecompositionType_LocalType;
class IMp7JrsAudioVisualRegionSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatialDecompositionType_LocalType > Mp7JrsAudioVisualRegionSpatialDecompositionType_LocalPtr;

#define TypeOfAudioVisualRegionSpatioTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatioTemporalDecompositionType")))
#define CreateAudioVisualRegionSpatioTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatioTemporalDecompositionType))
class Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType;
class IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType > Mp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr;

#define TypeOfAudioVisualRegionSpatioTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatioTemporalDecompositionType_CollectionType")))
#define CreateAudioVisualRegionSpatioTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatioTemporalDecompositionType_CollectionType))
class Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionType;
class IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionType > Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_CollectionPtr;

#define TypeOfAudioVisualRegionSpatioTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionSpatioTemporalDecompositionType_LocalType")))
#define CreateAudioVisualRegionSpatioTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionSpatioTemporalDecompositionType_LocalType))
class Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalType;
class IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalType > Mp7JrsAudioVisualRegionSpatioTemporalDecompositionType_LocalPtr;

#define TypeOfAudioVisualRegionTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionTemporalDecompositionType")))
#define CreateAudioVisualRegionTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionTemporalDecompositionType))
class Mp7JrsAudioVisualRegionTemporalDecompositionType;
class IMp7JrsAudioVisualRegionTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionTemporalDecompositionType > Mp7JrsAudioVisualRegionTemporalDecompositionPtr;

#define TypeOfAudioVisualRegionTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionTemporalDecompositionType_CollectionType")))
#define CreateAudioVisualRegionTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionTemporalDecompositionType_CollectionType))
class Mp7JrsAudioVisualRegionTemporalDecompositionType_CollectionType;
class IMp7JrsAudioVisualRegionTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionTemporalDecompositionType_CollectionType > Mp7JrsAudioVisualRegionTemporalDecompositionType_CollectionPtr;

#define TypeOfAudioVisualRegionTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionTemporalDecompositionType_LocalType")))
#define CreateAudioVisualRegionTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionTemporalDecompositionType_LocalType))
class Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType;
class IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType > Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalPtr;

#define TypeOfAudioVisualRegionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionType")))
#define CreateAudioVisualRegionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionType))
class Mp7JrsAudioVisualRegionType;
class IMp7JrsAudioVisualRegionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionType > Mp7JrsAudioVisualRegionPtr;

#define TypeOfAudioVisualRegionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionType_CollectionType")))
#define CreateAudioVisualRegionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionType_CollectionType))
class Mp7JrsAudioVisualRegionType_CollectionType;
class IMp7JrsAudioVisualRegionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionType_CollectionType > Mp7JrsAudioVisualRegionType_CollectionPtr;

#define TypeOfAudioVisualRegionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionType_LocalType")))
#define CreateAudioVisualRegionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualRegionType_LocalType))
class Mp7JrsAudioVisualRegionType_LocalType;
class IMp7JrsAudioVisualRegionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualRegionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualRegionType_LocalType > Mp7JrsAudioVisualRegionType_LocalPtr;

#define TypeOfAudioVisualSegmentMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentMediaSourceDecompositionType")))
#define CreateAudioVisualSegmentMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentMediaSourceDecompositionType))
class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType;
class IMp7JrsAudioVisualSegmentMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentMediaSourceDecompositionType > Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr;

#define TypeOfAudioVisualSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentMediaSourceDecompositionType_CollectionType")))
#define CreateAudioVisualSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentMediaSourceDecompositionType_CollectionType))
class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionType;
class IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionType > Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_CollectionPtr;

#define TypeOfAudioVisualSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentMediaSourceDecompositionType_LocalType")))
#define CreateAudioVisualSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentMediaSourceDecompositionType_LocalType))
class Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType;
class IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalType > Mp7JrsAudioVisualSegmentMediaSourceDecompositionType_LocalPtr;

#define TypeOfAudioVisualSegmentSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatialDecompositionType")))
#define CreateAudioVisualSegmentSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatialDecompositionType))
class Mp7JrsAudioVisualSegmentSpatialDecompositionType;
class IMp7JrsAudioVisualSegmentSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatialDecompositionType > Mp7JrsAudioVisualSegmentSpatialDecompositionPtr;

#define TypeOfAudioVisualSegmentSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatialDecompositionType_CollectionType")))
#define CreateAudioVisualSegmentSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatialDecompositionType_CollectionType))
class Mp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionType;
class IMp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionType > Mp7JrsAudioVisualSegmentSpatialDecompositionType_CollectionPtr;

#define TypeOfAudioVisualSegmentSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatialDecompositionType_LocalType")))
#define CreateAudioVisualSegmentSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatialDecompositionType_LocalType))
class Mp7JrsAudioVisualSegmentSpatialDecompositionType_LocalType;
class IMp7JrsAudioVisualSegmentSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatialDecompositionType_LocalType > Mp7JrsAudioVisualSegmentSpatialDecompositionType_LocalPtr;

#define TypeOfAudioVisualSegmentSpatioTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatioTemporalDecompositionType")))
#define CreateAudioVisualSegmentSpatioTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatioTemporalDecompositionType))
class Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType;
class IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType > Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr;

#define TypeOfAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatioTemporalDecompositionType_CollectionType")))
#define CreateAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType))
class Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType;
class IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionType > Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_CollectionPtr;

#define TypeOfAudioVisualSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatioTemporalDecompositionType_LocalType")))
#define CreateAudioVisualSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentSpatioTemporalDecompositionType_LocalType))
class Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalType;
class IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalType > Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType_LocalPtr;

#define TypeOfAudioVisualSegmentTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentTemporalDecompositionType")))
#define CreateAudioVisualSegmentTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentTemporalDecompositionType))
class Mp7JrsAudioVisualSegmentTemporalDecompositionType;
class IMp7JrsAudioVisualSegmentTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentTemporalDecompositionType > Mp7JrsAudioVisualSegmentTemporalDecompositionPtr;

#define TypeOfAudioVisualSegmentTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentTemporalDecompositionType_CollectionType")))
#define CreateAudioVisualSegmentTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentTemporalDecompositionType_CollectionType))
class Mp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionType;
class IMp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionType > Mp7JrsAudioVisualSegmentTemporalDecompositionType_CollectionPtr;

#define TypeOfAudioVisualSegmentTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentTemporalDecompositionType_LocalType")))
#define CreateAudioVisualSegmentTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentTemporalDecompositionType_LocalType))
class Mp7JrsAudioVisualSegmentTemporalDecompositionType_LocalType;
class IMp7JrsAudioVisualSegmentTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentTemporalDecompositionType_LocalType > Mp7JrsAudioVisualSegmentTemporalDecompositionType_LocalPtr;

#define TypeOfAudioVisualSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentType")))
#define CreateAudioVisualSegmentType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentType))
class Mp7JrsAudioVisualSegmentType;
class IMp7JrsAudioVisualSegmentType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentType > Mp7JrsAudioVisualSegmentPtr;

#define TypeOfAudioVisualSegmentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentType_CollectionType")))
#define CreateAudioVisualSegmentType_CollectionType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentType_CollectionType))
class Mp7JrsAudioVisualSegmentType_CollectionType;
class IMp7JrsAudioVisualSegmentType_CollectionType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentType_CollectionType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentType_CollectionType > Mp7JrsAudioVisualSegmentType_CollectionPtr;

#define TypeOfAudioVisualSegmentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentType_LocalType")))
#define CreateAudioVisualSegmentType_LocalType (Dc1Factory::CreateObject(TypeOfAudioVisualSegmentType_LocalType))
class Mp7JrsAudioVisualSegmentType_LocalType;
class IMp7JrsAudioVisualSegmentType_LocalType;
/** Smart pointer for instance of IMp7JrsAudioVisualSegmentType_LocalType */
typedef Dc1Ptr< IMp7JrsAudioVisualSegmentType_LocalType > Mp7JrsAudioVisualSegmentType_LocalPtr;

#define TypeOfAudioVisualType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualType")))
#define CreateAudioVisualType (Dc1Factory::CreateObject(TypeOfAudioVisualType))
class Mp7JrsAudioVisualType;
class IMp7JrsAudioVisualType;
/** Smart pointer for instance of IMp7JrsAudioVisualType */
typedef Dc1Ptr< IMp7JrsAudioVisualType > Mp7JrsAudioVisualPtr;

#define TypeOfAudioWaveformType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioWaveformType")))
#define CreateAudioWaveformType (Dc1Factory::CreateObject(TypeOfAudioWaveformType))
class Mp7JrsAudioWaveformType;
class IMp7JrsAudioWaveformType;
/** Smart pointer for instance of IMp7JrsAudioWaveformType */
typedef Dc1Ptr< IMp7JrsAudioWaveformType > Mp7JrsAudioWaveformPtr;

#define TypeOfauxiliaryLanguageType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType")))
#define CreateauxiliaryLanguageType (Dc1Factory::CreateObject(TypeOfauxiliaryLanguageType))
class Mp7JrsauxiliaryLanguageType;
class IMp7JrsauxiliaryLanguageType;
/** Smart pointer for instance of IMp7JrsauxiliaryLanguageType */
typedef Dc1Ptr< IMp7JrsauxiliaryLanguageType > Mp7JrsauxiliaryLanguagePtr;

#define TypeOfauxiliaryLanguageType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType")))
#define CreateauxiliaryLanguageType_LocalType (Dc1Factory::CreateObject(TypeOfauxiliaryLanguageType_LocalType))
class Mp7JrsauxiliaryLanguageType_LocalType;
class IMp7JrsauxiliaryLanguageType_LocalType;
// No smart pointer instance for IMp7JrsauxiliaryLanguageType_LocalType

#define TypeOfauxiliaryLanguageType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType0")))
#define CreateauxiliaryLanguageType_LocalType0 (Dc1Factory::CreateObject(TypeOfauxiliaryLanguageType_LocalType0))
class Mp7JrsauxiliaryLanguageType_LocalType0;
class IMp7JrsauxiliaryLanguageType_LocalType0;
/** Smart pointer for instance of IMp7JrsauxiliaryLanguageType_LocalType0 */
typedef Dc1Ptr< IMp7JrsauxiliaryLanguageType_LocalType0 > Mp7JrsauxiliaryLanguageType_Local0Ptr;

#define TypeOfauxiliaryLanguageType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType1")))
#define CreateauxiliaryLanguageType_LocalType1 (Dc1Factory::CreateObject(TypeOfauxiliaryLanguageType_LocalType1))
class Mp7JrsauxiliaryLanguageType_LocalType1;
class IMp7JrsauxiliaryLanguageType_LocalType1;
/** Smart pointer for instance of IMp7JrsauxiliaryLanguageType_LocalType1 */
typedef Dc1Ptr< IMp7JrsauxiliaryLanguageType_LocalType1 > Mp7JrsauxiliaryLanguageType_Local1Ptr;

#define TypeOfAvailabilityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType")))
#define CreateAvailabilityType (Dc1Factory::CreateObject(TypeOfAvailabilityType))
class Mp7JrsAvailabilityType;
class IMp7JrsAvailabilityType;
/** Smart pointer for instance of IMp7JrsAvailabilityType */
typedef Dc1Ptr< IMp7JrsAvailabilityType > Mp7JrsAvailabilityPtr;

#define TypeOfAvailabilityType_AvailabilityPeriod_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_CollectionType")))
#define CreateAvailabilityType_AvailabilityPeriod_CollectionType (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_CollectionType))
class Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionType;
class IMp7JrsAvailabilityType_AvailabilityPeriod_CollectionType;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_CollectionType */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_CollectionType > Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr;

#define TypeOfAvailabilityType_AvailabilityPeriod_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_LocalType")))
#define CreateAvailabilityType_AvailabilityPeriod_LocalType (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_LocalType))
class Mp7JrsAvailabilityType_AvailabilityPeriod_LocalType;
class IMp7JrsAvailabilityType_AvailabilityPeriod_LocalType;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_LocalType */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_LocalType > Mp7JrsAvailabilityType_AvailabilityPeriod_LocalPtr;

#define TypeOfAvailabilityType_AvailabilityPeriod_type_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_type_CollectionType")))
#define CreateAvailabilityType_AvailabilityPeriod_type_CollectionType (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_type_CollectionType))
class Mp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionType;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionType;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionType */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionType > Mp7JrsAvailabilityType_AvailabilityPeriod_type_CollectionPtr;

#define TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_type_LocalType")))
#define CreateAvailabilityType_AvailabilityPeriod_type_LocalType (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType))
class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType;
// No smart pointer instance for IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType

#define TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_type_LocalType0")))
#define CreateAvailabilityType_AvailabilityPeriod_type_LocalType0 (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType0))
class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType0 > Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local0Ptr;

#define TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_type_LocalType1")))
#define CreateAvailabilityType_AvailabilityPeriod_type_LocalType1 (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType1))
class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType1 > Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local1Ptr;

#define TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_type_LocalType2")))
#define CreateAvailabilityType_AvailabilityPeriod_type_LocalType2 (Dc1Factory::CreateObject(TypeOfAvailabilityType_AvailabilityPeriod_type_LocalType2))
class Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2;
class IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2;
/** Smart pointer for instance of IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2 */
typedef Dc1Ptr< IMp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType2 > Mp7JrsAvailabilityType_AvailabilityPeriod_type_Local2Ptr;

#define TypeOfBackgroundNoiseLevelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BackgroundNoiseLevelType")))
#define CreateBackgroundNoiseLevelType (Dc1Factory::CreateObject(TypeOfBackgroundNoiseLevelType))
class Mp7JrsBackgroundNoiseLevelType;
class IMp7JrsBackgroundNoiseLevelType;
/** Smart pointer for instance of IMp7JrsBackgroundNoiseLevelType */
typedef Dc1Ptr< IMp7JrsBackgroundNoiseLevelType > Mp7JrsBackgroundNoiseLevelPtr;

#define TypeOfBalanceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BalanceType")))
#define CreateBalanceType (Dc1Factory::CreateObject(TypeOfBalanceType))
class Mp7JrsBalanceType;
class IMp7JrsBalanceType;
/** Smart pointer for instance of IMp7JrsBalanceType */
typedef Dc1Ptr< IMp7JrsBalanceType > Mp7JrsBalancePtr;

#define TypeOfBandwidthType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BandwidthType")))
#define CreateBandwidthType (Dc1Factory::CreateObject(TypeOfBandwidthType))
class Mp7JrsBandwidthType;
class IMp7JrsBandwidthType;
/** Smart pointer for instance of IMp7JrsBandwidthType */
typedef Dc1Ptr< IMp7JrsBandwidthType > Mp7JrsBandwidthPtr;

#define TypeOfbasicDurationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:basicDurationType")))
#define CreatebasicDurationType (Dc1Factory::CreateObject(TypeOfbasicDurationType))
class Mp7JrsbasicDurationType;
class IMp7JrsbasicDurationType;
/** Smart pointer for instance of IMp7JrsbasicDurationType */
typedef Dc1Ptr< IMp7JrsbasicDurationType > Mp7JrsbasicDurationPtr;

#define TypeOfbasicTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:basicTimePointType")))
#define CreatebasicTimePointType (Dc1Factory::CreateObject(TypeOfbasicTimePointType))
class Mp7JrsbasicTimePointType;
class IMp7JrsbasicTimePointType;
/** Smart pointer for instance of IMp7JrsbasicTimePointType */
typedef Dc1Ptr< IMp7JrsbasicTimePointType > Mp7JrsbasicTimePointPtr;

#define TypeOfbeatType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:beatType")))
#define CreatebeatType (Dc1Factory::CreateObject(TypeOfbeatType))
class Mp7JrsbeatType;
class IMp7JrsbeatType;
/** Smart pointer for instance of IMp7JrsbeatType */
typedef Dc1Ptr< IMp7JrsbeatType > Mp7JrsbeatPtr;

#define TypeOfBinomialDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BinomialDistributionType")))
#define CreateBinomialDistributionType (Dc1Factory::CreateObject(TypeOfBinomialDistributionType))
class Mp7JrsBinomialDistributionType;
class IMp7JrsBinomialDistributionType;
/** Smart pointer for instance of IMp7JrsBinomialDistributionType */
typedef Dc1Ptr< IMp7JrsBinomialDistributionType > Mp7JrsBinomialDistributionPtr;

#define TypeOfBoxListType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BoxListType")))
#define CreateBoxListType (Dc1Factory::CreateObject(TypeOfBoxListType))
class Mp7JrsBoxListType;
class IMp7JrsBoxListType;
/** Smart pointer for instance of IMp7JrsBoxListType */
typedef Dc1Ptr< IMp7JrsBoxListType > Mp7JrsBoxListPtr;

#define TypeOfBrowsingPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType")))
#define CreateBrowsingPreferencesType (Dc1Factory::CreateObject(TypeOfBrowsingPreferencesType))
class Mp7JrsBrowsingPreferencesType;
class IMp7JrsBrowsingPreferencesType;
/** Smart pointer for instance of IMp7JrsBrowsingPreferencesType */
typedef Dc1Ptr< IMp7JrsBrowsingPreferencesType > Mp7JrsBrowsingPreferencesPtr;

#define TypeOfBrowsingPreferencesType_BrowsingLocation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType_BrowsingLocation_CollectionType")))
#define CreateBrowsingPreferencesType_BrowsingLocation_CollectionType (Dc1Factory::CreateObject(TypeOfBrowsingPreferencesType_BrowsingLocation_CollectionType))
class Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionType;
class IMp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionType;
/** Smart pointer for instance of IMp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionType */
typedef Dc1Ptr< IMp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionType > Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr;

#define TypeOfBrowsingPreferencesType_PreferenceCondition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType_PreferenceCondition_CollectionType")))
#define CreateBrowsingPreferencesType_PreferenceCondition_CollectionType (Dc1Factory::CreateObject(TypeOfBrowsingPreferencesType_PreferenceCondition_CollectionType))
class Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionType;
class IMp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionType;
/** Smart pointer for instance of IMp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionType */
typedef Dc1Ptr< IMp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionType > Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr;

#define TypeOfBrowsingPreferencesType_PreferenceCondition_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType_PreferenceCondition_LocalType")))
#define CreateBrowsingPreferencesType_PreferenceCondition_LocalType (Dc1Factory::CreateObject(TypeOfBrowsingPreferencesType_PreferenceCondition_LocalType))
class Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType;
class IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType;
/** Smart pointer for instance of IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType */
typedef Dc1Ptr< IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType > Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr;

#define TypeOfBrowsingPreferencesType_SummaryPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType_SummaryPreferences_CollectionType")))
#define CreateBrowsingPreferencesType_SummaryPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfBrowsingPreferencesType_SummaryPreferences_CollectionType))
class Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionType;
class IMp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionType > Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr;

#define TypeOfCameraMotionSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CameraMotionSegmentType")))
#define CreateCameraMotionSegmentType (Dc1Factory::CreateObject(TypeOfCameraMotionSegmentType))
class Mp7JrsCameraMotionSegmentType;
class IMp7JrsCameraMotionSegmentType;
/** Smart pointer for instance of IMp7JrsCameraMotionSegmentType */
typedef Dc1Ptr< IMp7JrsCameraMotionSegmentType > Mp7JrsCameraMotionSegmentPtr;

#define TypeOfCameraMotionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CameraMotionType")))
#define CreateCameraMotionType (Dc1Factory::CreateObject(TypeOfCameraMotionType))
class Mp7JrsCameraMotionType;
class IMp7JrsCameraMotionType;
/** Smart pointer for instance of IMp7JrsCameraMotionType */
typedef Dc1Ptr< IMp7JrsCameraMotionType > Mp7JrsCameraMotionPtr;

#define TypeOfCameraMotionType_Segment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CameraMotionType_Segment_CollectionType")))
#define CreateCameraMotionType_Segment_CollectionType (Dc1Factory::CreateObject(TypeOfCameraMotionType_Segment_CollectionType))
class Mp7JrsCameraMotionType_Segment_CollectionType;
class IMp7JrsCameraMotionType_Segment_CollectionType;
/** Smart pointer for instance of IMp7JrsCameraMotionType_Segment_CollectionType */
typedef Dc1Ptr< IMp7JrsCameraMotionType_Segment_CollectionType > Mp7JrsCameraMotionType_Segment_CollectionPtr;

#define TypeOfcharacterSetCode (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:characterSetCode")))
#define CreatecharacterSetCode (Dc1Factory::CreateObject(TypeOfcharacterSetCode))
class Mp7JrscharacterSetCode;
class IMp7JrscharacterSetCode;
/** Smart pointer for instance of IMp7JrscharacterSetCode */
typedef Dc1Ptr< IMp7JrscharacterSetCode > Mp7JrscharacterSetCodePtr;

#define TypeOfChordBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType")))
#define CreateChordBaseType (Dc1Factory::CreateObject(TypeOfChordBaseType))
class Mp7JrsChordBaseType;
class IMp7JrsChordBaseType;
/** Smart pointer for instance of IMp7JrsChordBaseType */
typedef Dc1Ptr< IMp7JrsChordBaseType > Mp7JrsChordBasePtr;

#define TypeOfChordBaseType_NoteAdded_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteAdded_LocalType")))
#define CreateChordBaseType_NoteAdded_LocalType (Dc1Factory::CreateObject(TypeOfChordBaseType_NoteAdded_LocalType))
class Mp7JrsChordBaseType_NoteAdded_LocalType;
class IMp7JrsChordBaseType_NoteAdded_LocalType;
/** Smart pointer for instance of IMp7JrsChordBaseType_NoteAdded_LocalType */
typedef Dc1Ptr< IMp7JrsChordBaseType_NoteAdded_LocalType > Mp7JrsChordBaseType_NoteAdded_LocalPtr;

#define TypeOfChordBaseType_NoteRemoved_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteRemoved_LocalType")))
#define CreateChordBaseType_NoteRemoved_LocalType (Dc1Factory::CreateObject(TypeOfChordBaseType_NoteRemoved_LocalType))
class Mp7JrsChordBaseType_NoteRemoved_LocalType;
class IMp7JrsChordBaseType_NoteRemoved_LocalType;
/** Smart pointer for instance of IMp7JrsChordBaseType_NoteRemoved_LocalType */
typedef Dc1Ptr< IMp7JrsChordBaseType_NoteRemoved_LocalType > Mp7JrsChordBaseType_NoteRemoved_LocalPtr;

#define TypeOfChordBaseType_SeventhChord_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_SeventhChord_LocalType")))
#define CreateChordBaseType_SeventhChord_LocalType (Dc1Factory::CreateObject(TypeOfChordBaseType_SeventhChord_LocalType))
class Mp7JrsChordBaseType_SeventhChord_LocalType;
class IMp7JrsChordBaseType_SeventhChord_LocalType;
// No smart pointer instance for IMp7JrsChordBaseType_SeventhChord_LocalType

#define TypeOfChordBaseType_Triad_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_Triad_LocalType")))
#define CreateChordBaseType_Triad_LocalType (Dc1Factory::CreateObject(TypeOfChordBaseType_Triad_LocalType))
class Mp7JrsChordBaseType_Triad_LocalType;
class IMp7JrsChordBaseType_Triad_LocalType;
// No smart pointer instance for IMp7JrsChordBaseType_Triad_LocalType

#define TypeOfClassificationModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationModelType")))
#define CreateClassificationModelType (Dc1Factory::CreateObject(TypeOfClassificationModelType))
class Mp7JrsClassificationModelType;
class IMp7JrsClassificationModelType;
/** Smart pointer for instance of IMp7JrsClassificationModelType */
typedef Dc1Ptr< IMp7JrsClassificationModelType > Mp7JrsClassificationModelPtr;

#define TypeOfClassificationPerceptualAttributeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPerceptualAttributeType")))
#define CreateClassificationPerceptualAttributeType (Dc1Factory::CreateObject(TypeOfClassificationPerceptualAttributeType))
class Mp7JrsClassificationPerceptualAttributeType;
class IMp7JrsClassificationPerceptualAttributeType;
/** Smart pointer for instance of IMp7JrsClassificationPerceptualAttributeType */
typedef Dc1Ptr< IMp7JrsClassificationPerceptualAttributeType > Mp7JrsClassificationPerceptualAttributePtr;

#define TypeOfClassificationPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType")))
#define CreateClassificationPreferencesType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType))
class Mp7JrsClassificationPreferencesType;
class IMp7JrsClassificationPreferencesType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType > Mp7JrsClassificationPreferencesPtr;

#define TypeOfClassificationPreferencesType_AssociatedData_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_AssociatedData_CollectionType")))
#define CreateClassificationPreferencesType_AssociatedData_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_AssociatedData_CollectionType))
class Mp7JrsClassificationPreferencesType_AssociatedData_CollectionType;
class IMp7JrsClassificationPreferencesType_AssociatedData_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_AssociatedData_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_AssociatedData_CollectionType > Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr;

#define TypeOfClassificationPreferencesType_AssociatedData_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_AssociatedData_LocalType")))
#define CreateClassificationPreferencesType_AssociatedData_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_AssociatedData_LocalType))
class Mp7JrsClassificationPreferencesType_AssociatedData_LocalType;
class IMp7JrsClassificationPreferencesType_AssociatedData_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_AssociatedData_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_AssociatedData_LocalType > Mp7JrsClassificationPreferencesType_AssociatedData_LocalPtr;

#define TypeOfClassificationPreferencesType_CaptionLanguage_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_CaptionLanguage_CollectionType")))
#define CreateClassificationPreferencesType_CaptionLanguage_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_CaptionLanguage_CollectionType))
class Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionType;
class IMp7JrsClassificationPreferencesType_CaptionLanguage_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_CaptionLanguage_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_CaptionLanguage_CollectionType > Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr;

#define TypeOfClassificationPreferencesType_CaptionLanguage_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_CaptionLanguage_LocalType")))
#define CreateClassificationPreferencesType_CaptionLanguage_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_CaptionLanguage_LocalType))
class Mp7JrsClassificationPreferencesType_CaptionLanguage_LocalType;
class IMp7JrsClassificationPreferencesType_CaptionLanguage_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_CaptionLanguage_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_CaptionLanguage_LocalType > Mp7JrsClassificationPreferencesType_CaptionLanguage_LocalPtr;

#define TypeOfClassificationPreferencesType_Country_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Country_CollectionType")))
#define CreateClassificationPreferencesType_Country_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Country_CollectionType))
class Mp7JrsClassificationPreferencesType_Country_CollectionType;
class IMp7JrsClassificationPreferencesType_Country_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Country_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Country_CollectionType > Mp7JrsClassificationPreferencesType_Country_CollectionPtr;

#define TypeOfClassificationPreferencesType_Country_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Country_LocalType")))
#define CreateClassificationPreferencesType_Country_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Country_LocalType))
class Mp7JrsClassificationPreferencesType_Country_LocalType;
class IMp7JrsClassificationPreferencesType_Country_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Country_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Country_LocalType > Mp7JrsClassificationPreferencesType_Country_LocalPtr;

#define TypeOfClassificationPreferencesType_DatePeriod_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_DatePeriod_CollectionType")))
#define CreateClassificationPreferencesType_DatePeriod_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_DatePeriod_CollectionType))
class Mp7JrsClassificationPreferencesType_DatePeriod_CollectionType;
class IMp7JrsClassificationPreferencesType_DatePeriod_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_DatePeriod_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_DatePeriod_CollectionType > Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr;

#define TypeOfClassificationPreferencesType_DatePeriod_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_DatePeriod_LocalType")))
#define CreateClassificationPreferencesType_DatePeriod_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_DatePeriod_LocalType))
class Mp7JrsClassificationPreferencesType_DatePeriod_LocalType;
class IMp7JrsClassificationPreferencesType_DatePeriod_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_DatePeriod_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_DatePeriod_LocalType > Mp7JrsClassificationPreferencesType_DatePeriod_LocalPtr;

#define TypeOfClassificationPreferencesType_Form_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Form_CollectionType")))
#define CreateClassificationPreferencesType_Form_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Form_CollectionType))
class Mp7JrsClassificationPreferencesType_Form_CollectionType;
class IMp7JrsClassificationPreferencesType_Form_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Form_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Form_CollectionType > Mp7JrsClassificationPreferencesType_Form_CollectionPtr;

#define TypeOfClassificationPreferencesType_Form_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Form_LocalType")))
#define CreateClassificationPreferencesType_Form_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Form_LocalType))
class Mp7JrsClassificationPreferencesType_Form_LocalType;
class IMp7JrsClassificationPreferencesType_Form_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Form_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Form_LocalType > Mp7JrsClassificationPreferencesType_Form_LocalPtr;

#define TypeOfClassificationPreferencesType_Genre_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Genre_CollectionType")))
#define CreateClassificationPreferencesType_Genre_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Genre_CollectionType))
class Mp7JrsClassificationPreferencesType_Genre_CollectionType;
class IMp7JrsClassificationPreferencesType_Genre_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Genre_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Genre_CollectionType > Mp7JrsClassificationPreferencesType_Genre_CollectionPtr;

#define TypeOfClassificationPreferencesType_Genre_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Genre_LocalType")))
#define CreateClassificationPreferencesType_Genre_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Genre_LocalType))
class Mp7JrsClassificationPreferencesType_Genre_LocalType;
class IMp7JrsClassificationPreferencesType_Genre_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Genre_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Genre_LocalType > Mp7JrsClassificationPreferencesType_Genre_LocalPtr;

#define TypeOfClassificationPreferencesType_Language_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Language_CollectionType")))
#define CreateClassificationPreferencesType_Language_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Language_CollectionType))
class Mp7JrsClassificationPreferencesType_Language_CollectionType;
class IMp7JrsClassificationPreferencesType_Language_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Language_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Language_CollectionType > Mp7JrsClassificationPreferencesType_Language_CollectionPtr;

#define TypeOfClassificationPreferencesType_Language_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Language_LocalType")))
#define CreateClassificationPreferencesType_Language_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Language_LocalType))
class Mp7JrsClassificationPreferencesType_Language_LocalType;
class IMp7JrsClassificationPreferencesType_Language_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Language_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Language_LocalType > Mp7JrsClassificationPreferencesType_Language_LocalPtr;

#define TypeOfClassificationPreferencesType_LanguageFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_LanguageFormat_CollectionType")))
#define CreateClassificationPreferencesType_LanguageFormat_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_LanguageFormat_CollectionType))
class Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionType;
class IMp7JrsClassificationPreferencesType_LanguageFormat_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_LanguageFormat_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_LanguageFormat_CollectionType > Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr;

#define TypeOfClassificationPreferencesType_LanguageFormat_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_LanguageFormat_LocalType")))
#define CreateClassificationPreferencesType_LanguageFormat_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_LanguageFormat_LocalType))
class Mp7JrsClassificationPreferencesType_LanguageFormat_LocalType;
class IMp7JrsClassificationPreferencesType_LanguageFormat_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_LanguageFormat_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_LanguageFormat_LocalType > Mp7JrsClassificationPreferencesType_LanguageFormat_LocalPtr;

#define TypeOfClassificationPreferencesType_ParentalGuidance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_ParentalGuidance_CollectionType")))
#define CreateClassificationPreferencesType_ParentalGuidance_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_ParentalGuidance_CollectionType))
class Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionType;
class IMp7JrsClassificationPreferencesType_ParentalGuidance_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_ParentalGuidance_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_ParentalGuidance_CollectionType > Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr;

#define TypeOfClassificationPreferencesType_ParentalGuidance_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_ParentalGuidance_LocalType")))
#define CreateClassificationPreferencesType_ParentalGuidance_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_ParentalGuidance_LocalType))
class Mp7JrsClassificationPreferencesType_ParentalGuidance_LocalType;
class IMp7JrsClassificationPreferencesType_ParentalGuidance_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_ParentalGuidance_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_ParentalGuidance_LocalType > Mp7JrsClassificationPreferencesType_ParentalGuidance_LocalPtr;

#define TypeOfClassificationPreferencesType_Review_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Review_CollectionType")))
#define CreateClassificationPreferencesType_Review_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Review_CollectionType))
class Mp7JrsClassificationPreferencesType_Review_CollectionType;
class IMp7JrsClassificationPreferencesType_Review_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Review_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Review_CollectionType > Mp7JrsClassificationPreferencesType_Review_CollectionPtr;

#define TypeOfClassificationPreferencesType_Review_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Review_LocalType")))
#define CreateClassificationPreferencesType_Review_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Review_LocalType))
class Mp7JrsClassificationPreferencesType_Review_LocalType;
class IMp7JrsClassificationPreferencesType_Review_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Review_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Review_LocalType > Mp7JrsClassificationPreferencesType_Review_LocalPtr;

#define TypeOfClassificationPreferencesType_Subject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Subject_CollectionType")))
#define CreateClassificationPreferencesType_Subject_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Subject_CollectionType))
class Mp7JrsClassificationPreferencesType_Subject_CollectionType;
class IMp7JrsClassificationPreferencesType_Subject_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Subject_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Subject_CollectionType > Mp7JrsClassificationPreferencesType_Subject_CollectionPtr;

#define TypeOfClassificationPreferencesType_Subject_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Subject_LocalType")))
#define CreateClassificationPreferencesType_Subject_LocalType (Dc1Factory::CreateObject(TypeOfClassificationPreferencesType_Subject_LocalType))
class Mp7JrsClassificationPreferencesType_Subject_LocalType;
class IMp7JrsClassificationPreferencesType_Subject_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationPreferencesType_Subject_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationPreferencesType_Subject_LocalType > Mp7JrsClassificationPreferencesType_Subject_LocalPtr;

#define TypeOfClassificationSchemeAliasType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeAliasType")))
#define CreateClassificationSchemeAliasType (Dc1Factory::CreateObject(TypeOfClassificationSchemeAliasType))
class Mp7JrsClassificationSchemeAliasType;
class IMp7JrsClassificationSchemeAliasType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeAliasType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeAliasType > Mp7JrsClassificationSchemeAliasPtr;

#define TypeOfClassificationSchemeBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeBaseType")))
#define CreateClassificationSchemeBaseType (Dc1Factory::CreateObject(TypeOfClassificationSchemeBaseType))
class Mp7JrsClassificationSchemeBaseType;
class IMp7JrsClassificationSchemeBaseType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeBaseType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeBaseType > Mp7JrsClassificationSchemeBasePtr;

#define TypeOfClassificationSchemeBaseType_domain_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeBaseType_domain_CollectionType")))
#define CreateClassificationSchemeBaseType_domain_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeBaseType_domain_CollectionType))
class Mp7JrsClassificationSchemeBaseType_domain_CollectionType;
class IMp7JrsClassificationSchemeBaseType_domain_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeBaseType_domain_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeBaseType_domain_CollectionType > Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr;

#define TypeOfClassificationSchemeBaseType_Import_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeBaseType_Import_CollectionType")))
#define CreateClassificationSchemeBaseType_Import_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeBaseType_Import_CollectionType))
class Mp7JrsClassificationSchemeBaseType_Import_CollectionType;
class IMp7JrsClassificationSchemeBaseType_Import_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeBaseType_Import_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeBaseType_Import_CollectionType > Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr;

#define TypeOfClassificationSchemeDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeDescriptionType")))
#define CreateClassificationSchemeDescriptionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeDescriptionType))
class Mp7JrsClassificationSchemeDescriptionType;
class IMp7JrsClassificationSchemeDescriptionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeDescriptionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeDescriptionType > Mp7JrsClassificationSchemeDescriptionPtr;

#define TypeOfClassificationSchemeDescriptionType_ClassificationScheme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeDescriptionType_ClassificationScheme_CollectionType")))
#define CreateClassificationSchemeDescriptionType_ClassificationScheme_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeDescriptionType_ClassificationScheme_CollectionType))
class Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionType;
class IMp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionType > Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr;

#define TypeOfClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType")))
#define CreateClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType))
class Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType;
class IMp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType > Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr;

#define TypeOfClassificationSchemeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeType")))
#define CreateClassificationSchemeType (Dc1Factory::CreateObject(TypeOfClassificationSchemeType))
class Mp7JrsClassificationSchemeType;
class IMp7JrsClassificationSchemeType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeType > Mp7JrsClassificationSchemePtr;

#define TypeOfClassificationSchemeType_Term_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeType_Term_CollectionType")))
#define CreateClassificationSchemeType_Term_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationSchemeType_Term_CollectionType))
class Mp7JrsClassificationSchemeType_Term_CollectionType;
class IMp7JrsClassificationSchemeType_Term_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationSchemeType_Term_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationSchemeType_Term_CollectionType > Mp7JrsClassificationSchemeType_Term_CollectionPtr;

#define TypeOfClassificationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType")))
#define CreateClassificationType (Dc1Factory::CreateObject(TypeOfClassificationType))
class Mp7JrsClassificationType;
class IMp7JrsClassificationType;
/** Smart pointer for instance of IMp7JrsClassificationType */
typedef Dc1Ptr< IMp7JrsClassificationType > Mp7JrsClassificationPtr;

#define TypeOfClassificationType_CaptionLanguage_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_CaptionLanguage_CollectionType")))
#define CreateClassificationType_CaptionLanguage_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_CaptionLanguage_CollectionType))
class Mp7JrsClassificationType_CaptionLanguage_CollectionType;
class IMp7JrsClassificationType_CaptionLanguage_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_CaptionLanguage_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_CaptionLanguage_CollectionType > Mp7JrsClassificationType_CaptionLanguage_CollectionPtr;

#define TypeOfClassificationType_CaptionLanguage_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_CaptionLanguage_LocalType")))
#define CreateClassificationType_CaptionLanguage_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_CaptionLanguage_LocalType))
class Mp7JrsClassificationType_CaptionLanguage_LocalType;
class IMp7JrsClassificationType_CaptionLanguage_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_CaptionLanguage_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_CaptionLanguage_LocalType > Mp7JrsClassificationType_CaptionLanguage_LocalPtr;

#define TypeOfClassificationType_Form_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Form_CollectionType")))
#define CreateClassificationType_Form_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Form_CollectionType))
class Mp7JrsClassificationType_Form_CollectionType;
class IMp7JrsClassificationType_Form_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Form_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Form_CollectionType > Mp7JrsClassificationType_Form_CollectionPtr;

#define TypeOfClassificationType_Genre_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Genre_CollectionType")))
#define CreateClassificationType_Genre_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Genre_CollectionType))
class Mp7JrsClassificationType_Genre_CollectionType;
class IMp7JrsClassificationType_Genre_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Genre_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Genre_CollectionType > Mp7JrsClassificationType_Genre_CollectionPtr;

#define TypeOfClassificationType_Genre_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Genre_LocalType")))
#define CreateClassificationType_Genre_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_Genre_LocalType))
class Mp7JrsClassificationType_Genre_LocalType;
class IMp7JrsClassificationType_Genre_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_Genre_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_Genre_LocalType > Mp7JrsClassificationType_Genre_LocalPtr;

#define TypeOfClassificationType_Genre_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Genre_type_LocalType")))
#define CreateClassificationType_Genre_type_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_Genre_type_LocalType))
class Mp7JrsClassificationType_Genre_type_LocalType;
class IMp7JrsClassificationType_Genre_type_LocalType;
// No smart pointer instance for IMp7JrsClassificationType_Genre_type_LocalType

#define TypeOfClassificationType_Language_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Language_CollectionType")))
#define CreateClassificationType_Language_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Language_CollectionType))
class Mp7JrsClassificationType_Language_CollectionType;
class IMp7JrsClassificationType_Language_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Language_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Language_CollectionType > Mp7JrsClassificationType_Language_CollectionPtr;

#define TypeOfClassificationType_MediaReview_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_MediaReview_CollectionType")))
#define CreateClassificationType_MediaReview_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_MediaReview_CollectionType))
class Mp7JrsClassificationType_MediaReview_CollectionType;
class IMp7JrsClassificationType_MediaReview_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_MediaReview_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_MediaReview_CollectionType > Mp7JrsClassificationType_MediaReview_CollectionPtr;

#define TypeOfClassificationType_ParentalGuidance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_ParentalGuidance_CollectionType")))
#define CreateClassificationType_ParentalGuidance_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_ParentalGuidance_CollectionType))
class Mp7JrsClassificationType_ParentalGuidance_CollectionType;
class IMp7JrsClassificationType_ParentalGuidance_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_ParentalGuidance_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_ParentalGuidance_CollectionType > Mp7JrsClassificationType_ParentalGuidance_CollectionPtr;

#define TypeOfClassificationType_Purpose_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Purpose_CollectionType")))
#define CreateClassificationType_Purpose_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Purpose_CollectionType))
class Mp7JrsClassificationType_Purpose_CollectionType;
class IMp7JrsClassificationType_Purpose_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Purpose_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Purpose_CollectionType > Mp7JrsClassificationType_Purpose_CollectionPtr;

#define TypeOfClassificationType_Release_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Release_CollectionType")))
#define CreateClassificationType_Release_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Release_CollectionType))
class Mp7JrsClassificationType_Release_CollectionType;
class IMp7JrsClassificationType_Release_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Release_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Release_CollectionType > Mp7JrsClassificationType_Release_CollectionPtr;

#define TypeOfClassificationType_Release_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Release_LocalType")))
#define CreateClassificationType_Release_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_Release_LocalType))
class Mp7JrsClassificationType_Release_LocalType;
class IMp7JrsClassificationType_Release_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_Release_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_Release_LocalType > Mp7JrsClassificationType_Release_LocalPtr;

#define TypeOfClassificationType_Release_Region_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Release_Region_CollectionType")))
#define CreateClassificationType_Release_Region_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Release_Region_CollectionType))
class Mp7JrsClassificationType_Release_Region_CollectionType;
class IMp7JrsClassificationType_Release_Region_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Release_Region_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Release_Region_CollectionType > Mp7JrsClassificationType_Release_Region_CollectionPtr;

#define TypeOfClassificationType_SignLanguage_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_SignLanguage_CollectionType")))
#define CreateClassificationType_SignLanguage_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_SignLanguage_CollectionType))
class Mp7JrsClassificationType_SignLanguage_CollectionType;
class IMp7JrsClassificationType_SignLanguage_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_SignLanguage_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_SignLanguage_CollectionType > Mp7JrsClassificationType_SignLanguage_CollectionPtr;

#define TypeOfClassificationType_SignLanguage_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_SignLanguage_LocalType")))
#define CreateClassificationType_SignLanguage_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_SignLanguage_LocalType))
class Mp7JrsClassificationType_SignLanguage_LocalType;
class IMp7JrsClassificationType_SignLanguage_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_SignLanguage_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_SignLanguage_LocalType > Mp7JrsClassificationType_SignLanguage_LocalPtr;

#define TypeOfClassificationType_Subject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Subject_CollectionType")))
#define CreateClassificationType_Subject_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Subject_CollectionType))
class Mp7JrsClassificationType_Subject_CollectionType;
class IMp7JrsClassificationType_Subject_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Subject_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Subject_CollectionType > Mp7JrsClassificationType_Subject_CollectionPtr;

#define TypeOfClassificationType_Target_Age_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_Age_LocalType")))
#define CreateClassificationType_Target_Age_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_Target_Age_LocalType))
class Mp7JrsClassificationType_Target_Age_LocalType;
class IMp7JrsClassificationType_Target_Age_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_Target_Age_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_Target_Age_LocalType > Mp7JrsClassificationType_Target_Age_LocalPtr;

#define TypeOfClassificationType_Target_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_LocalType")))
#define CreateClassificationType_Target_LocalType (Dc1Factory::CreateObject(TypeOfClassificationType_Target_LocalType))
class Mp7JrsClassificationType_Target_LocalType;
class IMp7JrsClassificationType_Target_LocalType;
/** Smart pointer for instance of IMp7JrsClassificationType_Target_LocalType */
typedef Dc1Ptr< IMp7JrsClassificationType_Target_LocalType > Mp7JrsClassificationType_Target_LocalPtr;

#define TypeOfClassificationType_Target_Market_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_Market_CollectionType")))
#define CreateClassificationType_Target_Market_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Target_Market_CollectionType))
class Mp7JrsClassificationType_Target_Market_CollectionType;
class IMp7JrsClassificationType_Target_Market_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Target_Market_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Target_Market_CollectionType > Mp7JrsClassificationType_Target_Market_CollectionPtr;

#define TypeOfClassificationType_Target_Region_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_Region_CollectionType")))
#define CreateClassificationType_Target_Region_CollectionType (Dc1Factory::CreateObject(TypeOfClassificationType_Target_Region_CollectionType))
class Mp7JrsClassificationType_Target_Region_CollectionType;
class IMp7JrsClassificationType_Target_Region_CollectionType;
/** Smart pointer for instance of IMp7JrsClassificationType_Target_Region_CollectionType */
typedef Dc1Ptr< IMp7JrsClassificationType_Target_Region_CollectionType > Mp7JrsClassificationType_Target_Region_CollectionPtr;

#define TypeOfClusterClassificationModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterClassificationModelType")))
#define CreateClusterClassificationModelType (Dc1Factory::CreateObject(TypeOfClusterClassificationModelType))
class Mp7JrsClusterClassificationModelType;
class IMp7JrsClusterClassificationModelType;
/** Smart pointer for instance of IMp7JrsClusterClassificationModelType */
typedef Dc1Ptr< IMp7JrsClusterClassificationModelType > Mp7JrsClusterClassificationModelPtr;

#define TypeOfClusterClassificationModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterClassificationModelType_CollectionType")))
#define CreateClusterClassificationModelType_CollectionType (Dc1Factory::CreateObject(TypeOfClusterClassificationModelType_CollectionType))
class Mp7JrsClusterClassificationModelType_CollectionType;
class IMp7JrsClusterClassificationModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsClusterClassificationModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsClusterClassificationModelType_CollectionType > Mp7JrsClusterClassificationModelType_CollectionPtr;

#define TypeOfClusterClassificationModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterClassificationModelType_LocalType")))
#define CreateClusterClassificationModelType_LocalType (Dc1Factory::CreateObject(TypeOfClusterClassificationModelType_LocalType))
class Mp7JrsClusterClassificationModelType_LocalType;
class IMp7JrsClusterClassificationModelType_LocalType;
/** Smart pointer for instance of IMp7JrsClusterClassificationModelType_LocalType */
typedef Dc1Ptr< IMp7JrsClusterClassificationModelType_LocalType > Mp7JrsClusterClassificationModelType_LocalPtr;

#define TypeOfClusterModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType")))
#define CreateClusterModelType (Dc1Factory::CreateObject(TypeOfClusterModelType))
class Mp7JrsClusterModelType;
class IMp7JrsClusterModelType;
/** Smart pointer for instance of IMp7JrsClusterModelType */
typedef Dc1Ptr< IMp7JrsClusterModelType > Mp7JrsClusterModelPtr;

#define TypeOfClusterModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType_CollectionType")))
#define CreateClusterModelType_CollectionType (Dc1Factory::CreateObject(TypeOfClusterModelType_CollectionType))
class Mp7JrsClusterModelType_CollectionType;
class IMp7JrsClusterModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsClusterModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsClusterModelType_CollectionType > Mp7JrsClusterModelType_CollectionPtr;

#define TypeOfClusterModelType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType_CollectionType0")))
#define CreateClusterModelType_CollectionType0 (Dc1Factory::CreateObject(TypeOfClusterModelType_CollectionType0))
class Mp7JrsClusterModelType_CollectionType0;
class IMp7JrsClusterModelType_CollectionType0;
/** Smart pointer for instance of IMp7JrsClusterModelType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsClusterModelType_CollectionType0 > Mp7JrsClusterModelType_Collection0Ptr;

#define TypeOfClusterModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType_LocalType")))
#define CreateClusterModelType_LocalType (Dc1Factory::CreateObject(TypeOfClusterModelType_LocalType))
class Mp7JrsClusterModelType_LocalType;
class IMp7JrsClusterModelType_LocalType;
/** Smart pointer for instance of IMp7JrsClusterModelType_LocalType */
typedef Dc1Ptr< IMp7JrsClusterModelType_LocalType > Mp7JrsClusterModelType_LocalPtr;

#define TypeOfClusterModelType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType_LocalType0")))
#define CreateClusterModelType_LocalType0 (Dc1Factory::CreateObject(TypeOfClusterModelType_LocalType0))
class Mp7JrsClusterModelType_LocalType0;
class IMp7JrsClusterModelType_LocalType0;
/** Smart pointer for instance of IMp7JrsClusterModelType_LocalType0 */
typedef Dc1Ptr< IMp7JrsClusterModelType_LocalType0 > Mp7JrsClusterModelType_Local0Ptr;

#define TypeOfCollectionModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionModelType")))
#define CreateCollectionModelType (Dc1Factory::CreateObject(TypeOfCollectionModelType))
class Mp7JrsCollectionModelType;
class IMp7JrsCollectionModelType;
/** Smart pointer for instance of IMp7JrsCollectionModelType */
typedef Dc1Ptr< IMp7JrsCollectionModelType > Mp7JrsCollectionModelPtr;

#define TypeOfCollectionModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionModelType_CollectionType")))
#define CreateCollectionModelType_CollectionType (Dc1Factory::CreateObject(TypeOfCollectionModelType_CollectionType))
class Mp7JrsCollectionModelType_CollectionType;
class IMp7JrsCollectionModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsCollectionModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsCollectionModelType_CollectionType > Mp7JrsCollectionModelType_CollectionPtr;

#define TypeOfCollectionModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionModelType_LocalType")))
#define CreateCollectionModelType_LocalType (Dc1Factory::CreateObject(TypeOfCollectionModelType_LocalType))
class Mp7JrsCollectionModelType_LocalType;
class IMp7JrsCollectionModelType_LocalType;
/** Smart pointer for instance of IMp7JrsCollectionModelType_LocalType */
typedef Dc1Ptr< IMp7JrsCollectionModelType_LocalType > Mp7JrsCollectionModelType_LocalPtr;

#define TypeOfCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionType")))
#define CreateCollectionType (Dc1Factory::CreateObject(TypeOfCollectionType))
class Mp7JrsCollectionType;
class IMp7JrsCollectionType;
/** Smart pointer for instance of IMp7JrsCollectionType */
typedef Dc1Ptr< IMp7JrsCollectionType > Mp7JrsCollectionPtr;

#define TypeOfCollectionType_TextAnnotation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionType_TextAnnotation_CollectionType")))
#define CreateCollectionType_TextAnnotation_CollectionType (Dc1Factory::CreateObject(TypeOfCollectionType_TextAnnotation_CollectionType))
class Mp7JrsCollectionType_TextAnnotation_CollectionType;
class IMp7JrsCollectionType_TextAnnotation_CollectionType;
/** Smart pointer for instance of IMp7JrsCollectionType_TextAnnotation_CollectionType */
typedef Dc1Ptr< IMp7JrsCollectionType_TextAnnotation_CollectionType > Mp7JrsCollectionType_TextAnnotation_CollectionPtr;

#define TypeOfColorLayoutType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType")))
#define CreateColorLayoutType (Dc1Factory::CreateObject(TypeOfColorLayoutType))
class Mp7JrsColorLayoutType;
class IMp7JrsColorLayoutType;
/** Smart pointer for instance of IMp7JrsColorLayoutType */
typedef Dc1Ptr< IMp7JrsColorLayoutType > Mp7JrsColorLayoutPtr;

#define TypeOfColorLayoutType_CbACCoeff14_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff14_CollectionType")))
#define CreateColorLayoutType_CbACCoeff14_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff14_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff14_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff14_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff14_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff14_CollectionType > Mp7JrsColorLayoutType_CbACCoeff14_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff14_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff14_LocalType")))
#define CreateColorLayoutType_CbACCoeff14_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff14_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff14_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff14_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff14_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff14_LocalType > Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff2_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff2_CollectionType")))
#define CreateColorLayoutType_CbACCoeff2_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff2_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff2_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff2_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff2_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff2_CollectionType > Mp7JrsColorLayoutType_CbACCoeff2_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff2_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff2_LocalType")))
#define CreateColorLayoutType_CbACCoeff2_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff2_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff2_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff2_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff2_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff2_LocalType > Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff20_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff20_CollectionType")))
#define CreateColorLayoutType_CbACCoeff20_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff20_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff20_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff20_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff20_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff20_CollectionType > Mp7JrsColorLayoutType_CbACCoeff20_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff20_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff20_LocalType")))
#define CreateColorLayoutType_CbACCoeff20_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff20_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff20_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff20_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff20_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff20_LocalType > Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff27_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff27_CollectionType")))
#define CreateColorLayoutType_CbACCoeff27_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff27_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff27_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff27_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff27_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff27_CollectionType > Mp7JrsColorLayoutType_CbACCoeff27_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff27_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff27_LocalType")))
#define CreateColorLayoutType_CbACCoeff27_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff27_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff27_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff27_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff27_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff27_LocalType > Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff5_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff5_CollectionType")))
#define CreateColorLayoutType_CbACCoeff5_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff5_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff5_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff5_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff5_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff5_CollectionType > Mp7JrsColorLayoutType_CbACCoeff5_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff5_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff5_LocalType")))
#define CreateColorLayoutType_CbACCoeff5_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff5_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff5_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff5_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff5_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff5_LocalType > Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff63_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff63_CollectionType")))
#define CreateColorLayoutType_CbACCoeff63_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff63_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff63_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff63_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff63_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff63_CollectionType > Mp7JrsColorLayoutType_CbACCoeff63_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff63_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff63_LocalType")))
#define CreateColorLayoutType_CbACCoeff63_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff63_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff63_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff63_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff63_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff63_LocalType > Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr;

#define TypeOfColorLayoutType_CbACCoeff9_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff9_CollectionType")))
#define CreateColorLayoutType_CbACCoeff9_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff9_CollectionType))
class Mp7JrsColorLayoutType_CbACCoeff9_CollectionType;
class IMp7JrsColorLayoutType_CbACCoeff9_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff9_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff9_CollectionType > Mp7JrsColorLayoutType_CbACCoeff9_CollectionPtr;

#define TypeOfColorLayoutType_CbACCoeff9_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff9_LocalType")))
#define CreateColorLayoutType_CbACCoeff9_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CbACCoeff9_LocalType))
class Mp7JrsColorLayoutType_CbACCoeff9_LocalType;
class IMp7JrsColorLayoutType_CbACCoeff9_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CbACCoeff9_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CbACCoeff9_LocalType > Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff14_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff14_CollectionType")))
#define CreateColorLayoutType_CrACCoeff14_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff14_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff14_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff14_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff14_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff14_CollectionType > Mp7JrsColorLayoutType_CrACCoeff14_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff14_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff14_LocalType")))
#define CreateColorLayoutType_CrACCoeff14_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff14_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff14_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff14_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff14_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff14_LocalType > Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff2_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff2_CollectionType")))
#define CreateColorLayoutType_CrACCoeff2_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff2_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff2_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff2_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff2_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff2_CollectionType > Mp7JrsColorLayoutType_CrACCoeff2_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff2_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff2_LocalType")))
#define CreateColorLayoutType_CrACCoeff2_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff2_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff2_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff2_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff2_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff2_LocalType > Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff20_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff20_CollectionType")))
#define CreateColorLayoutType_CrACCoeff20_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff20_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff20_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff20_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff20_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff20_CollectionType > Mp7JrsColorLayoutType_CrACCoeff20_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff20_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff20_LocalType")))
#define CreateColorLayoutType_CrACCoeff20_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff20_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff20_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff20_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff20_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff20_LocalType > Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff27_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff27_CollectionType")))
#define CreateColorLayoutType_CrACCoeff27_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff27_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff27_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff27_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff27_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff27_CollectionType > Mp7JrsColorLayoutType_CrACCoeff27_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff27_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff27_LocalType")))
#define CreateColorLayoutType_CrACCoeff27_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff27_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff27_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff27_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff27_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff27_LocalType > Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff5_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff5_CollectionType")))
#define CreateColorLayoutType_CrACCoeff5_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff5_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff5_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff5_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff5_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff5_CollectionType > Mp7JrsColorLayoutType_CrACCoeff5_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff5_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff5_LocalType")))
#define CreateColorLayoutType_CrACCoeff5_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff5_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff5_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff5_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff5_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff5_LocalType > Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff63_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff63_CollectionType")))
#define CreateColorLayoutType_CrACCoeff63_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff63_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff63_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff63_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff63_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff63_CollectionType > Mp7JrsColorLayoutType_CrACCoeff63_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff63_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff63_LocalType")))
#define CreateColorLayoutType_CrACCoeff63_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff63_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff63_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff63_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff63_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff63_LocalType > Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr;

#define TypeOfColorLayoutType_CrACCoeff9_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff9_CollectionType")))
#define CreateColorLayoutType_CrACCoeff9_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff9_CollectionType))
class Mp7JrsColorLayoutType_CrACCoeff9_CollectionType;
class IMp7JrsColorLayoutType_CrACCoeff9_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff9_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff9_CollectionType > Mp7JrsColorLayoutType_CrACCoeff9_CollectionPtr;

#define TypeOfColorLayoutType_CrACCoeff9_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff9_LocalType")))
#define CreateColorLayoutType_CrACCoeff9_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_CrACCoeff9_LocalType))
class Mp7JrsColorLayoutType_CrACCoeff9_LocalType;
class IMp7JrsColorLayoutType_CrACCoeff9_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_CrACCoeff9_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_CrACCoeff9_LocalType > Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff14_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff14_CollectionType")))
#define CreateColorLayoutType_YACCoeff14_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff14_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff14_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff14_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff14_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff14_CollectionType > Mp7JrsColorLayoutType_YACCoeff14_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff14_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff14_LocalType")))
#define CreateColorLayoutType_YACCoeff14_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff14_LocalType))
class Mp7JrsColorLayoutType_YACCoeff14_LocalType;
class IMp7JrsColorLayoutType_YACCoeff14_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff14_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff14_LocalType > Mp7JrsColorLayoutType_YACCoeff14_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff2_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff2_CollectionType")))
#define CreateColorLayoutType_YACCoeff2_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff2_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff2_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff2_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff2_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff2_CollectionType > Mp7JrsColorLayoutType_YACCoeff2_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff2_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff2_LocalType")))
#define CreateColorLayoutType_YACCoeff2_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff2_LocalType))
class Mp7JrsColorLayoutType_YACCoeff2_LocalType;
class IMp7JrsColorLayoutType_YACCoeff2_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff2_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff2_LocalType > Mp7JrsColorLayoutType_YACCoeff2_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff20_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff20_CollectionType")))
#define CreateColorLayoutType_YACCoeff20_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff20_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff20_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff20_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff20_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff20_CollectionType > Mp7JrsColorLayoutType_YACCoeff20_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff20_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff20_LocalType")))
#define CreateColorLayoutType_YACCoeff20_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff20_LocalType))
class Mp7JrsColorLayoutType_YACCoeff20_LocalType;
class IMp7JrsColorLayoutType_YACCoeff20_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff20_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff20_LocalType > Mp7JrsColorLayoutType_YACCoeff20_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff27_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff27_CollectionType")))
#define CreateColorLayoutType_YACCoeff27_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff27_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff27_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff27_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff27_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff27_CollectionType > Mp7JrsColorLayoutType_YACCoeff27_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff27_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff27_LocalType")))
#define CreateColorLayoutType_YACCoeff27_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff27_LocalType))
class Mp7JrsColorLayoutType_YACCoeff27_LocalType;
class IMp7JrsColorLayoutType_YACCoeff27_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff27_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff27_LocalType > Mp7JrsColorLayoutType_YACCoeff27_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff5_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff5_CollectionType")))
#define CreateColorLayoutType_YACCoeff5_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff5_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff5_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff5_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff5_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff5_CollectionType > Mp7JrsColorLayoutType_YACCoeff5_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff5_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff5_LocalType")))
#define CreateColorLayoutType_YACCoeff5_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff5_LocalType))
class Mp7JrsColorLayoutType_YACCoeff5_LocalType;
class IMp7JrsColorLayoutType_YACCoeff5_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff5_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff5_LocalType > Mp7JrsColorLayoutType_YACCoeff5_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff63_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff63_CollectionType")))
#define CreateColorLayoutType_YACCoeff63_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff63_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff63_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff63_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff63_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff63_CollectionType > Mp7JrsColorLayoutType_YACCoeff63_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff63_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff63_LocalType")))
#define CreateColorLayoutType_YACCoeff63_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff63_LocalType))
class Mp7JrsColorLayoutType_YACCoeff63_LocalType;
class IMp7JrsColorLayoutType_YACCoeff63_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff63_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff63_LocalType > Mp7JrsColorLayoutType_YACCoeff63_LocalPtr;

#define TypeOfColorLayoutType_YACCoeff9_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff9_CollectionType")))
#define CreateColorLayoutType_YACCoeff9_CollectionType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff9_CollectionType))
class Mp7JrsColorLayoutType_YACCoeff9_CollectionType;
class IMp7JrsColorLayoutType_YACCoeff9_CollectionType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff9_CollectionType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff9_CollectionType > Mp7JrsColorLayoutType_YACCoeff9_CollectionPtr;

#define TypeOfColorLayoutType_YACCoeff9_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff9_LocalType")))
#define CreateColorLayoutType_YACCoeff9_LocalType (Dc1Factory::CreateObject(TypeOfColorLayoutType_YACCoeff9_LocalType))
class Mp7JrsColorLayoutType_YACCoeff9_LocalType;
class IMp7JrsColorLayoutType_YACCoeff9_LocalType;
/** Smart pointer for instance of IMp7JrsColorLayoutType_YACCoeff9_LocalType */
typedef Dc1Ptr< IMp7JrsColorLayoutType_YACCoeff9_LocalType > Mp7JrsColorLayoutType_YACCoeff9_LocalPtr;

#define TypeOfColorQuantizationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType")))
#define CreateColorQuantizationType (Dc1Factory::CreateObject(TypeOfColorQuantizationType))
class Mp7JrsColorQuantizationType;
class IMp7JrsColorQuantizationType;
/** Smart pointer for instance of IMp7JrsColorQuantizationType */
typedef Dc1Ptr< IMp7JrsColorQuantizationType > Mp7JrsColorQuantizationPtr;

#define TypeOfColorQuantizationType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType_CollectionType")))
#define CreateColorQuantizationType_CollectionType (Dc1Factory::CreateObject(TypeOfColorQuantizationType_CollectionType))
class Mp7JrsColorQuantizationType_CollectionType;
class IMp7JrsColorQuantizationType_CollectionType;
/** Smart pointer for instance of IMp7JrsColorQuantizationType_CollectionType */
typedef Dc1Ptr< IMp7JrsColorQuantizationType_CollectionType > Mp7JrsColorQuantizationType_CollectionPtr;

#define TypeOfColorQuantizationType_Component_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType_Component_LocalType")))
#define CreateColorQuantizationType_Component_LocalType (Dc1Factory::CreateObject(TypeOfColorQuantizationType_Component_LocalType))
class Mp7JrsColorQuantizationType_Component_LocalType;
class IMp7JrsColorQuantizationType_Component_LocalType;
// No smart pointer instance for IMp7JrsColorQuantizationType_Component_LocalType

#define TypeOfColorQuantizationType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType_LocalType")))
#define CreateColorQuantizationType_LocalType (Dc1Factory::CreateObject(TypeOfColorQuantizationType_LocalType))
class Mp7JrsColorQuantizationType_LocalType;
class IMp7JrsColorQuantizationType_LocalType;
/** Smart pointer for instance of IMp7JrsColorQuantizationType_LocalType */
typedef Dc1Ptr< IMp7JrsColorQuantizationType_LocalType > Mp7JrsColorQuantizationType_LocalPtr;

#define TypeOfColorSamplingType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType")))
#define CreateColorSamplingType (Dc1Factory::CreateObject(TypeOfColorSamplingType))
class Mp7JrsColorSamplingType;
class IMp7JrsColorSamplingType;
/** Smart pointer for instance of IMp7JrsColorSamplingType */
typedef Dc1Ptr< IMp7JrsColorSamplingType > Mp7JrsColorSamplingPtr;

#define TypeOfColorSamplingType_Field_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_CollectionType")))
#define CreateColorSamplingType_Field_CollectionType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_CollectionType))
class Mp7JrsColorSamplingType_Field_CollectionType;
class IMp7JrsColorSamplingType_Field_CollectionType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_CollectionType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_CollectionType > Mp7JrsColorSamplingType_Field_CollectionPtr;

#define TypeOfColorSamplingType_Field_Component_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_CollectionType")))
#define CreateColorSamplingType_Field_Component_CollectionType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_Component_CollectionType))
class Mp7JrsColorSamplingType_Field_Component_CollectionType;
class IMp7JrsColorSamplingType_Field_Component_CollectionType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_Component_CollectionType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_Component_CollectionType > Mp7JrsColorSamplingType_Field_Component_CollectionPtr;

#define TypeOfColorSamplingType_Field_Component_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_LocalType")))
#define CreateColorSamplingType_Field_Component_LocalType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_Component_LocalType))
class Mp7JrsColorSamplingType_Field_Component_LocalType;
class IMp7JrsColorSamplingType_Field_Component_LocalType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_Component_LocalType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_Component_LocalType > Mp7JrsColorSamplingType_Field_Component_LocalPtr;

#define TypeOfColorSamplingType_Field_Component_Offset_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Offset_LocalType")))
#define CreateColorSamplingType_Field_Component_Offset_LocalType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_Component_Offset_LocalType))
class Mp7JrsColorSamplingType_Field_Component_Offset_LocalType;
class IMp7JrsColorSamplingType_Field_Component_Offset_LocalType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_Component_Offset_LocalType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_Component_Offset_LocalType > Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr;

#define TypeOfColorSamplingType_Field_Component_Period_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Period_LocalType")))
#define CreateColorSamplingType_Field_Component_Period_LocalType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_Component_Period_LocalType))
class Mp7JrsColorSamplingType_Field_Component_Period_LocalType;
class IMp7JrsColorSamplingType_Field_Component_Period_LocalType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_Component_Period_LocalType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_Component_Period_LocalType > Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr;

#define TypeOfColorSamplingType_Field_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_LocalType")))
#define CreateColorSamplingType_Field_LocalType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Field_LocalType))
class Mp7JrsColorSamplingType_Field_LocalType;
class IMp7JrsColorSamplingType_Field_LocalType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Field_LocalType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Field_LocalType > Mp7JrsColorSamplingType_Field_LocalPtr;

#define TypeOfColorSamplingType_Lattice_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Lattice_LocalType")))
#define CreateColorSamplingType_Lattice_LocalType (Dc1Factory::CreateObject(TypeOfColorSamplingType_Lattice_LocalType))
class Mp7JrsColorSamplingType_Lattice_LocalType;
class IMp7JrsColorSamplingType_Lattice_LocalType;
/** Smart pointer for instance of IMp7JrsColorSamplingType_Lattice_LocalType */
typedef Dc1Ptr< IMp7JrsColorSamplingType_Lattice_LocalType > Mp7JrsColorSamplingType_Lattice_LocalPtr;

#define TypeOfColorSpaceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType")))
#define CreateColorSpaceType (Dc1Factory::CreateObject(TypeOfColorSpaceType))
class Mp7JrsColorSpaceType;
class IMp7JrsColorSpaceType;
/** Smart pointer for instance of IMp7JrsColorSpaceType */
typedef Dc1Ptr< IMp7JrsColorSpaceType > Mp7JrsColorSpacePtr;

#define TypeOfColorSpaceType_ColorTransMat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType_ColorTransMat_CollectionType")))
#define CreateColorSpaceType_ColorTransMat_CollectionType (Dc1Factory::CreateObject(TypeOfColorSpaceType_ColorTransMat_CollectionType))
class Mp7JrsColorSpaceType_ColorTransMat_CollectionType;
class IMp7JrsColorSpaceType_ColorTransMat_CollectionType;
/** Smart pointer for instance of IMp7JrsColorSpaceType_ColorTransMat_CollectionType */
typedef Dc1Ptr< IMp7JrsColorSpaceType_ColorTransMat_CollectionType > Mp7JrsColorSpaceType_ColorTransMat_CollectionPtr;

#define TypeOfColorSpaceType_ColorTransMat_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType_ColorTransMat_LocalType")))
#define CreateColorSpaceType_ColorTransMat_LocalType (Dc1Factory::CreateObject(TypeOfColorSpaceType_ColorTransMat_LocalType))
class Mp7JrsColorSpaceType_ColorTransMat_LocalType;
class IMp7JrsColorSpaceType_ColorTransMat_LocalType;
/** Smart pointer for instance of IMp7JrsColorSpaceType_ColorTransMat_LocalType */
typedef Dc1Ptr< IMp7JrsColorSpaceType_ColorTransMat_LocalType > Mp7JrsColorSpaceType_ColorTransMat_LocalPtr;

#define TypeOfColorSpaceType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType_type_LocalType")))
#define CreateColorSpaceType_type_LocalType (Dc1Factory::CreateObject(TypeOfColorSpaceType_type_LocalType))
class Mp7JrsColorSpaceType_type_LocalType;
class IMp7JrsColorSpaceType_type_LocalType;
// No smart pointer instance for IMp7JrsColorSpaceType_type_LocalType

#define TypeOfColorStructureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorStructureType")))
#define CreateColorStructureType (Dc1Factory::CreateObject(TypeOfColorStructureType))
class Mp7JrsColorStructureType;
class IMp7JrsColorStructureType;
/** Smart pointer for instance of IMp7JrsColorStructureType */
typedef Dc1Ptr< IMp7JrsColorStructureType > Mp7JrsColorStructurePtr;

#define TypeOfColorStructureType_Values_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorStructureType_Values_CollectionType")))
#define CreateColorStructureType_Values_CollectionType (Dc1Factory::CreateObject(TypeOfColorStructureType_Values_CollectionType))
class Mp7JrsColorStructureType_Values_CollectionType;
class IMp7JrsColorStructureType_Values_CollectionType;
/** Smart pointer for instance of IMp7JrsColorStructureType_Values_CollectionType */
typedef Dc1Ptr< IMp7JrsColorStructureType_Values_CollectionType > Mp7JrsColorStructureType_Values_CollectionPtr;

#define TypeOfColorStructureType_Values_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorStructureType_Values_LocalType")))
#define CreateColorStructureType_Values_LocalType (Dc1Factory::CreateObject(TypeOfColorStructureType_Values_LocalType))
class Mp7JrsColorStructureType_Values_LocalType;
class IMp7JrsColorStructureType_Values_LocalType;
/** Smart pointer for instance of IMp7JrsColorStructureType_Values_LocalType */
typedef Dc1Ptr< IMp7JrsColorStructureType_Values_LocalType > Mp7JrsColorStructureType_Values_LocalPtr;

#define TypeOfColorTemperatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorTemperatureType")))
#define CreateColorTemperatureType (Dc1Factory::CreateObject(TypeOfColorTemperatureType))
class Mp7JrsColorTemperatureType;
class IMp7JrsColorTemperatureType;
/** Smart pointer for instance of IMp7JrsColorTemperatureType */
typedef Dc1Ptr< IMp7JrsColorTemperatureType > Mp7JrsColorTemperaturePtr;

#define TypeOfColorTemperatureType_BrowsingCategory_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorTemperatureType_BrowsingCategory_LocalType")))
#define CreateColorTemperatureType_BrowsingCategory_LocalType (Dc1Factory::CreateObject(TypeOfColorTemperatureType_BrowsingCategory_LocalType))
class Mp7JrsColorTemperatureType_BrowsingCategory_LocalType;
class IMp7JrsColorTemperatureType_BrowsingCategory_LocalType;
// No smart pointer instance for IMp7JrsColorTemperatureType_BrowsingCategory_LocalType

#define TypeOfCompleteDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompleteDescriptionType")))
#define CreateCompleteDescriptionType (Dc1Factory::CreateObject(TypeOfCompleteDescriptionType))
class Mp7JrsCompleteDescriptionType;
class IMp7JrsCompleteDescriptionType;
/** Smart pointer for instance of IMp7JrsCompleteDescriptionType */
typedef Dc1Ptr< IMp7JrsCompleteDescriptionType > Mp7JrsCompleteDescriptionPtr;

#define TypeOfCompleteDescriptionType_OrderingKey_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompleteDescriptionType_OrderingKey_CollectionType")))
#define CreateCompleteDescriptionType_OrderingKey_CollectionType (Dc1Factory::CreateObject(TypeOfCompleteDescriptionType_OrderingKey_CollectionType))
class Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType;
class IMp7JrsCompleteDescriptionType_OrderingKey_CollectionType;
/** Smart pointer for instance of IMp7JrsCompleteDescriptionType_OrderingKey_CollectionType */
typedef Dc1Ptr< IMp7JrsCompleteDescriptionType_OrderingKey_CollectionType > Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr;

#define TypeOfCompleteDescriptionType_Relationships_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompleteDescriptionType_Relationships_CollectionType")))
#define CreateCompleteDescriptionType_Relationships_CollectionType (Dc1Factory::CreateObject(TypeOfCompleteDescriptionType_Relationships_CollectionType))
class Mp7JrsCompleteDescriptionType_Relationships_CollectionType;
class IMp7JrsCompleteDescriptionType_Relationships_CollectionType;
/** Smart pointer for instance of IMp7JrsCompleteDescriptionType_Relationships_CollectionType */
typedef Dc1Ptr< IMp7JrsCompleteDescriptionType_Relationships_CollectionType > Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr;

#define TypeOfCompositionShotEditingTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotEditingTemporalDecompositionType")))
#define CreateCompositionShotEditingTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfCompositionShotEditingTemporalDecompositionType))
class Mp7JrsCompositionShotEditingTemporalDecompositionType;
class IMp7JrsCompositionShotEditingTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsCompositionShotEditingTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsCompositionShotEditingTemporalDecompositionType > Mp7JrsCompositionShotEditingTemporalDecompositionPtr;

#define TypeOfCompositionShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotEditingTemporalDecompositionType_CollectionType")))
#define CreateCompositionShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfCompositionShotEditingTemporalDecompositionType_CollectionType))
class Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionType;
class IMp7JrsCompositionShotEditingTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsCompositionShotEditingTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsCompositionShotEditingTemporalDecompositionType_CollectionType > Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr;

#define TypeOfCompositionShotEditingTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotEditingTemporalDecompositionType_LocalType")))
#define CreateCompositionShotEditingTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfCompositionShotEditingTemporalDecompositionType_LocalType))
class Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType;
class IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsCompositionShotEditingTemporalDecompositionType_LocalType > Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr;

#define TypeOfCompositionShotType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotType")))
#define CreateCompositionShotType (Dc1Factory::CreateObject(TypeOfCompositionShotType))
class Mp7JrsCompositionShotType;
class IMp7JrsCompositionShotType;
/** Smart pointer for instance of IMp7JrsCompositionShotType */
typedef Dc1Ptr< IMp7JrsCompositionShotType > Mp7JrsCompositionShotPtr;

#define TypeOfCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType")))
#define CreateCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType))
class Mp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType;
class IMp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType > Mp7JrsCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr;

#define TypeOfCompositionTransitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType")))
#define CreateCompositionTransitionType (Dc1Factory::CreateObject(TypeOfCompositionTransitionType))
class Mp7JrsCompositionTransitionType;
class IMp7JrsCompositionTransitionType;
/** Smart pointer for instance of IMp7JrsCompositionTransitionType */
typedef Dc1Ptr< IMp7JrsCompositionTransitionType > Mp7JrsCompositionTransitionPtr;

#define TypeOfCompositionTransitionType_compositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType_compositionType_LocalType")))
#define CreateCompositionTransitionType_compositionType_LocalType (Dc1Factory::CreateObject(TypeOfCompositionTransitionType_compositionType_LocalType))
class Mp7JrsCompositionTransitionType_compositionType_LocalType;
class IMp7JrsCompositionTransitionType_compositionType_LocalType;
// No smart pointer instance for IMp7JrsCompositionTransitionType_compositionType_LocalType

#define TypeOfCompositionTransitionType_compositionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType_compositionType_LocalType0")))
#define CreateCompositionTransitionType_compositionType_LocalType0 (Dc1Factory::CreateObject(TypeOfCompositionTransitionType_compositionType_LocalType0))
class Mp7JrsCompositionTransitionType_compositionType_LocalType0;
class IMp7JrsCompositionTransitionType_compositionType_LocalType0;
/** Smart pointer for instance of IMp7JrsCompositionTransitionType_compositionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsCompositionTransitionType_compositionType_LocalType0 > Mp7JrsCompositionTransitionType_compositionType_Local0Ptr;

#define TypeOfCompositionTransitionType_compositionType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType_compositionType_LocalType1")))
#define CreateCompositionTransitionType_compositionType_LocalType1 (Dc1Factory::CreateObject(TypeOfCompositionTransitionType_compositionType_LocalType1))
class Mp7JrsCompositionTransitionType_compositionType_LocalType1;
class IMp7JrsCompositionTransitionType_compositionType_LocalType1;
/** Smart pointer for instance of IMp7JrsCompositionTransitionType_compositionType_LocalType1 */
typedef Dc1Ptr< IMp7JrsCompositionTransitionType_compositionType_LocalType1 > Mp7JrsCompositionTransitionType_compositionType_Local1Ptr;

#define TypeOfCompositionTransitionType_compositionType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType_compositionType_LocalType2")))
#define CreateCompositionTransitionType_compositionType_LocalType2 (Dc1Factory::CreateObject(TypeOfCompositionTransitionType_compositionType_LocalType2))
class Mp7JrsCompositionTransitionType_compositionType_LocalType2;
class IMp7JrsCompositionTransitionType_compositionType_LocalType2;
/** Smart pointer for instance of IMp7JrsCompositionTransitionType_compositionType_LocalType2 */
typedef Dc1Ptr< IMp7JrsCompositionTransitionType_compositionType_LocalType2 > Mp7JrsCompositionTransitionType_compositionType_Local2Ptr;

#define TypeOfConceptCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType")))
#define CreateConceptCollectionType (Dc1Factory::CreateObject(TypeOfConceptCollectionType))
class Mp7JrsConceptCollectionType;
class IMp7JrsConceptCollectionType;
/** Smart pointer for instance of IMp7JrsConceptCollectionType */
typedef Dc1Ptr< IMp7JrsConceptCollectionType > Mp7JrsConceptCollectionPtr;

#define TypeOfConceptCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType_CollectionType")))
#define CreateConceptCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfConceptCollectionType_CollectionType))
class Mp7JrsConceptCollectionType_CollectionType;
class IMp7JrsConceptCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsConceptCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsConceptCollectionType_CollectionType > Mp7JrsConceptCollectionType_CollectionPtr;

#define TypeOfConceptCollectionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType_CollectionType0")))
#define CreateConceptCollectionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfConceptCollectionType_CollectionType0))
class Mp7JrsConceptCollectionType_CollectionType0;
class IMp7JrsConceptCollectionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsConceptCollectionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsConceptCollectionType_CollectionType0 > Mp7JrsConceptCollectionType_Collection0Ptr;

#define TypeOfConceptCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType_LocalType")))
#define CreateConceptCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfConceptCollectionType_LocalType))
class Mp7JrsConceptCollectionType_LocalType;
class IMp7JrsConceptCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsConceptCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsConceptCollectionType_LocalType > Mp7JrsConceptCollectionType_LocalPtr;

#define TypeOfConceptCollectionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType_LocalType0")))
#define CreateConceptCollectionType_LocalType0 (Dc1Factory::CreateObject(TypeOfConceptCollectionType_LocalType0))
class Mp7JrsConceptCollectionType_LocalType0;
class IMp7JrsConceptCollectionType_LocalType0;
/** Smart pointer for instance of IMp7JrsConceptCollectionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsConceptCollectionType_LocalType0 > Mp7JrsConceptCollectionType_Local0Ptr;

#define TypeOfConceptType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptType")))
#define CreateConceptType (Dc1Factory::CreateObject(TypeOfConceptType))
class Mp7JrsConceptType;
class IMp7JrsConceptType;
/** Smart pointer for instance of IMp7JrsConceptType */
typedef Dc1Ptr< IMp7JrsConceptType > Mp7JrsConceptPtr;

#define TypeOfConfusionCountType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConfusionCountType")))
#define CreateConfusionCountType (Dc1Factory::CreateObject(TypeOfConfusionCountType))
class Mp7JrsConfusionCountType;
class IMp7JrsConfusionCountType;
/** Smart pointer for instance of IMp7JrsConfusionCountType */
typedef Dc1Ptr< IMp7JrsConfusionCountType > Mp7JrsConfusionCountPtr;

#define TypeOfConnectionBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionBaseType")))
#define CreateConnectionBaseType (Dc1Factory::CreateObject(TypeOfConnectionBaseType))
class Mp7JrsConnectionBaseType;
class IMp7JrsConnectionBaseType;
/** Smart pointer for instance of IMp7JrsConnectionBaseType */
typedef Dc1Ptr< IMp7JrsConnectionBaseType > Mp7JrsConnectionBasePtr;

#define TypeOfConnectionsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionsType")))
#define CreateConnectionsType (Dc1Factory::CreateObject(TypeOfConnectionsType))
class Mp7JrsConnectionsType;
class IMp7JrsConnectionsType;
/** Smart pointer for instance of IMp7JrsConnectionsType */
typedef Dc1Ptr< IMp7JrsConnectionsType > Mp7JrsConnectionsPtr;

#define TypeOfConnectionsType_Acquaintance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionsType_Acquaintance_CollectionType")))
#define CreateConnectionsType_Acquaintance_CollectionType (Dc1Factory::CreateObject(TypeOfConnectionsType_Acquaintance_CollectionType))
class Mp7JrsConnectionsType_Acquaintance_CollectionType;
class IMp7JrsConnectionsType_Acquaintance_CollectionType;
/** Smart pointer for instance of IMp7JrsConnectionsType_Acquaintance_CollectionType */
typedef Dc1Ptr< IMp7JrsConnectionsType_Acquaintance_CollectionType > Mp7JrsConnectionsType_Acquaintance_CollectionPtr;

#define TypeOfConnectionsType_Affiliation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionsType_Affiliation_CollectionType")))
#define CreateConnectionsType_Affiliation_CollectionType (Dc1Factory::CreateObject(TypeOfConnectionsType_Affiliation_CollectionType))
class Mp7JrsConnectionsType_Affiliation_CollectionType;
class IMp7JrsConnectionsType_Affiliation_CollectionType;
/** Smart pointer for instance of IMp7JrsConnectionsType_Affiliation_CollectionType */
typedef Dc1Ptr< IMp7JrsConnectionsType_Affiliation_CollectionType > Mp7JrsConnectionsType_Affiliation_CollectionPtr;

#define TypeOfContentAbstractionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentAbstractionType")))
#define CreateContentAbstractionType (Dc1Factory::CreateObject(TypeOfContentAbstractionType))
class Mp7JrsContentAbstractionType;
class IMp7JrsContentAbstractionType;
/** Smart pointer for instance of IMp7JrsContentAbstractionType */
typedef Dc1Ptr< IMp7JrsContentAbstractionType > Mp7JrsContentAbstractionPtr;

#define TypeOfContentCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType")))
#define CreateContentCollectionType (Dc1Factory::CreateObject(TypeOfContentCollectionType))
class Mp7JrsContentCollectionType;
class IMp7JrsContentCollectionType;
/** Smart pointer for instance of IMp7JrsContentCollectionType */
typedef Dc1Ptr< IMp7JrsContentCollectionType > Mp7JrsContentCollectionPtr;

#define TypeOfContentCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_CollectionType")))
#define CreateContentCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfContentCollectionType_CollectionType))
class Mp7JrsContentCollectionType_CollectionType;
class IMp7JrsContentCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsContentCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsContentCollectionType_CollectionType > Mp7JrsContentCollectionType_CollectionPtr;

#define TypeOfContentCollectionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_CollectionType0")))
#define CreateContentCollectionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfContentCollectionType_CollectionType0))
class Mp7JrsContentCollectionType_CollectionType0;
class IMp7JrsContentCollectionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsContentCollectionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsContentCollectionType_CollectionType0 > Mp7JrsContentCollectionType_Collection0Ptr;

#define TypeOfContentCollectionType_CollectionType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_CollectionType1")))
#define CreateContentCollectionType_CollectionType1 (Dc1Factory::CreateObject(TypeOfContentCollectionType_CollectionType1))
class Mp7JrsContentCollectionType_CollectionType1;
class IMp7JrsContentCollectionType_CollectionType1;
/** Smart pointer for instance of IMp7JrsContentCollectionType_CollectionType1 */
typedef Dc1Ptr< IMp7JrsContentCollectionType_CollectionType1 > Mp7JrsContentCollectionType_Collection1Ptr;

#define TypeOfContentCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_LocalType")))
#define CreateContentCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfContentCollectionType_LocalType))
class Mp7JrsContentCollectionType_LocalType;
class IMp7JrsContentCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsContentCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsContentCollectionType_LocalType > Mp7JrsContentCollectionType_LocalPtr;

#define TypeOfContentCollectionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_LocalType0")))
#define CreateContentCollectionType_LocalType0 (Dc1Factory::CreateObject(TypeOfContentCollectionType_LocalType0))
class Mp7JrsContentCollectionType_LocalType0;
class IMp7JrsContentCollectionType_LocalType0;
/** Smart pointer for instance of IMp7JrsContentCollectionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsContentCollectionType_LocalType0 > Mp7JrsContentCollectionType_Local0Ptr;

#define TypeOfContentCollectionType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType_LocalType1")))
#define CreateContentCollectionType_LocalType1 (Dc1Factory::CreateObject(TypeOfContentCollectionType_LocalType1))
class Mp7JrsContentCollectionType_LocalType1;
class IMp7JrsContentCollectionType_LocalType1;
/** Smart pointer for instance of IMp7JrsContentCollectionType_LocalType1 */
typedef Dc1Ptr< IMp7JrsContentCollectionType_LocalType1 > Mp7JrsContentCollectionType_Local1Ptr;

#define TypeOfContentDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentDescriptionType")))
#define CreateContentDescriptionType (Dc1Factory::CreateObject(TypeOfContentDescriptionType))
class Mp7JrsContentDescriptionType;
class IMp7JrsContentDescriptionType;
/** Smart pointer for instance of IMp7JrsContentDescriptionType */
typedef Dc1Ptr< IMp7JrsContentDescriptionType > Mp7JrsContentDescriptionPtr;

#define TypeOfContentDescriptionType_Affective_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentDescriptionType_Affective_CollectionType")))
#define CreateContentDescriptionType_Affective_CollectionType (Dc1Factory::CreateObject(TypeOfContentDescriptionType_Affective_CollectionType))
class Mp7JrsContentDescriptionType_Affective_CollectionType;
class IMp7JrsContentDescriptionType_Affective_CollectionType;
/** Smart pointer for instance of IMp7JrsContentDescriptionType_Affective_CollectionType */
typedef Dc1Ptr< IMp7JrsContentDescriptionType_Affective_CollectionType > Mp7JrsContentDescriptionType_Affective_CollectionPtr;

#define TypeOfContentEntityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentEntityType")))
#define CreateContentEntityType (Dc1Factory::CreateObject(TypeOfContentEntityType))
class Mp7JrsContentEntityType;
class IMp7JrsContentEntityType;
/** Smart pointer for instance of IMp7JrsContentEntityType */
typedef Dc1Ptr< IMp7JrsContentEntityType > Mp7JrsContentEntityPtr;

#define TypeOfContentEntityType_MultimediaContent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentEntityType_MultimediaContent_CollectionType")))
#define CreateContentEntityType_MultimediaContent_CollectionType (Dc1Factory::CreateObject(TypeOfContentEntityType_MultimediaContent_CollectionType))
class Mp7JrsContentEntityType_MultimediaContent_CollectionType;
class IMp7JrsContentEntityType_MultimediaContent_CollectionType;
/** Smart pointer for instance of IMp7JrsContentEntityType_MultimediaContent_CollectionType */
typedef Dc1Ptr< IMp7JrsContentEntityType_MultimediaContent_CollectionType > Mp7JrsContentEntityType_MultimediaContent_CollectionPtr;

#define TypeOfContentManagementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentManagementType")))
#define CreateContentManagementType (Dc1Factory::CreateObject(TypeOfContentManagementType))
class Mp7JrsContentManagementType;
class IMp7JrsContentManagementType;
/** Smart pointer for instance of IMp7JrsContentManagementType */
typedef Dc1Ptr< IMp7JrsContentManagementType > Mp7JrsContentManagementPtr;

#define TypeOfContinuousDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContinuousDistributionType")))
#define CreateContinuousDistributionType (Dc1Factory::CreateObject(TypeOfContinuousDistributionType))
class Mp7JrsContinuousDistributionType;
class IMp7JrsContinuousDistributionType;
/** Smart pointer for instance of IMp7JrsContinuousDistributionType */
typedef Dc1Ptr< IMp7JrsContinuousDistributionType > Mp7JrsContinuousDistributionPtr;

#define TypeOfContinuousHiddenMarkovModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContinuousHiddenMarkovModelType")))
#define CreateContinuousHiddenMarkovModelType (Dc1Factory::CreateObject(TypeOfContinuousHiddenMarkovModelType))
class Mp7JrsContinuousHiddenMarkovModelType;
class IMp7JrsContinuousHiddenMarkovModelType;
/** Smart pointer for instance of IMp7JrsContinuousHiddenMarkovModelType */
typedef Dc1Ptr< IMp7JrsContinuousHiddenMarkovModelType > Mp7JrsContinuousHiddenMarkovModelPtr;

#define TypeOfContinuousHiddenMarkovModelType_DescriptorModel_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContinuousHiddenMarkovModelType_DescriptorModel_CollectionType")))
#define CreateContinuousHiddenMarkovModelType_DescriptorModel_CollectionType (Dc1Factory::CreateObject(TypeOfContinuousHiddenMarkovModelType_DescriptorModel_CollectionType))
class Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType;
class IMp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType;
/** Smart pointer for instance of IMp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType */
typedef Dc1Ptr< IMp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType > Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr;

#define TypeOfContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType")))
#define CreateContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType (Dc1Factory::CreateObject(TypeOfContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType))
class Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType;
class IMp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType;
/** Smart pointer for instance of IMp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType */
typedef Dc1Ptr< IMp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType > Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr;

#define TypeOfContinuousUniformDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContinuousUniformDistributionType")))
#define CreateContinuousUniformDistributionType (Dc1Factory::CreateObject(TypeOfContinuousUniformDistributionType))
class Mp7JrsContinuousUniformDistributionType;
class IMp7JrsContinuousUniformDistributionType;
/** Smart pointer for instance of IMp7JrsContinuousUniformDistributionType */
typedef Dc1Ptr< IMp7JrsContinuousUniformDistributionType > Mp7JrsContinuousUniformDistributionPtr;

#define TypeOfContourShapeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType")))
#define CreateContourShapeType (Dc1Factory::CreateObject(TypeOfContourShapeType))
class Mp7JrsContourShapeType;
class IMp7JrsContourShapeType;
/** Smart pointer for instance of IMp7JrsContourShapeType */
typedef Dc1Ptr< IMp7JrsContourShapeType > Mp7JrsContourShapePtr;

#define TypeOfContourShapeType_Peak_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType_Peak_CollectionType")))
#define CreateContourShapeType_Peak_CollectionType (Dc1Factory::CreateObject(TypeOfContourShapeType_Peak_CollectionType))
class Mp7JrsContourShapeType_Peak_CollectionType;
class IMp7JrsContourShapeType_Peak_CollectionType;
/** Smart pointer for instance of IMp7JrsContourShapeType_Peak_CollectionType */
typedef Dc1Ptr< IMp7JrsContourShapeType_Peak_CollectionType > Mp7JrsContourShapeType_Peak_CollectionPtr;

#define TypeOfContourShapeType_Peak_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType_Peak_LocalType")))
#define CreateContourShapeType_Peak_LocalType (Dc1Factory::CreateObject(TypeOfContourShapeType_Peak_LocalType))
class Mp7JrsContourShapeType_Peak_LocalType;
class IMp7JrsContourShapeType_Peak_LocalType;
/** Smart pointer for instance of IMp7JrsContourShapeType_Peak_LocalType */
typedef Dc1Ptr< IMp7JrsContourShapeType_Peak_LocalType > Mp7JrsContourShapeType_Peak_LocalPtr;

#define TypeOfcontourType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:contourType")))
#define CreatecontourType (Dc1Factory::CreateObject(TypeOfcontourType))
class Mp7JrscontourType;
class IMp7JrscontourType;
/** Smart pointer for instance of IMp7JrscontourType */
typedef Dc1Ptr< IMp7JrscontourType > Mp7JrscontourPtr;

#define TypeOfcontourType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:contourType_LocalType")))
#define CreatecontourType_LocalType (Dc1Factory::CreateObject(TypeOfcontourType_LocalType))
class Mp7JrscontourType_LocalType;
class IMp7JrscontourType_LocalType;
/** Smart pointer for instance of IMp7JrscontourType_LocalType */
typedef Dc1Ptr< IMp7JrscontourType_LocalType > Mp7JrscontourType_LocalPtr;

#define TypeOfControlledTermUseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))
#define CreateControlledTermUseType (Dc1Factory::CreateObject(TypeOfControlledTermUseType))
class Mp7JrsControlledTermUseType;
class IMp7JrsControlledTermUseType;
/** Smart pointer for instance of IMp7JrsControlledTermUseType */
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;

#define TypeOfcountryCode (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode")))
#define CreatecountryCode (Dc1Factory::CreateObject(TypeOfcountryCode))
class Mp7JrscountryCode;
class IMp7JrscountryCode;
/** Smart pointer for instance of IMp7JrscountryCode */
typedef Dc1Ptr< IMp7JrscountryCode > Mp7JrscountryCodePtr;

#define TypeOfCreationDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationDescriptionType")))
#define CreateCreationDescriptionType (Dc1Factory::CreateObject(TypeOfCreationDescriptionType))
class Mp7JrsCreationDescriptionType;
class IMp7JrsCreationDescriptionType;
/** Smart pointer for instance of IMp7JrsCreationDescriptionType */
typedef Dc1Ptr< IMp7JrsCreationDescriptionType > Mp7JrsCreationDescriptionPtr;

#define TypeOfCreationDescriptionType_CreationInformation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationDescriptionType_CreationInformation_CollectionType")))
#define CreateCreationDescriptionType_CreationInformation_CollectionType (Dc1Factory::CreateObject(TypeOfCreationDescriptionType_CreationInformation_CollectionType))
class Mp7JrsCreationDescriptionType_CreationInformation_CollectionType;
class IMp7JrsCreationDescriptionType_CreationInformation_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationDescriptionType_CreationInformation_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationDescriptionType_CreationInformation_CollectionType > Mp7JrsCreationDescriptionType_CreationInformation_CollectionPtr;

#define TypeOfCreationInformationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationInformationType")))
#define CreateCreationInformationType (Dc1Factory::CreateObject(TypeOfCreationInformationType))
class Mp7JrsCreationInformationType;
class IMp7JrsCreationInformationType;
/** Smart pointer for instance of IMp7JrsCreationInformationType */
typedef Dc1Ptr< IMp7JrsCreationInformationType > Mp7JrsCreationInformationPtr;

#define TypeOfCreationInformationType_RelatedMaterial_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationInformationType_RelatedMaterial_CollectionType")))
#define CreateCreationInformationType_RelatedMaterial_CollectionType (Dc1Factory::CreateObject(TypeOfCreationInformationType_RelatedMaterial_CollectionType))
class Mp7JrsCreationInformationType_RelatedMaterial_CollectionType;
class IMp7JrsCreationInformationType_RelatedMaterial_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationInformationType_RelatedMaterial_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationInformationType_RelatedMaterial_CollectionType > Mp7JrsCreationInformationType_RelatedMaterial_CollectionPtr;

#define TypeOfCreationPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType")))
#define CreateCreationPreferencesType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType))
class Mp7JrsCreationPreferencesType;
class IMp7JrsCreationPreferencesType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType > Mp7JrsCreationPreferencesPtr;

#define TypeOfCreationPreferencesType_Creator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Creator_CollectionType")))
#define CreateCreationPreferencesType_Creator_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Creator_CollectionType))
class Mp7JrsCreationPreferencesType_Creator_CollectionType;
class IMp7JrsCreationPreferencesType_Creator_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Creator_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Creator_CollectionType > Mp7JrsCreationPreferencesType_Creator_CollectionPtr;

#define TypeOfCreationPreferencesType_Creator_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Creator_LocalType")))
#define CreateCreationPreferencesType_Creator_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Creator_LocalType))
class Mp7JrsCreationPreferencesType_Creator_LocalType;
class IMp7JrsCreationPreferencesType_Creator_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Creator_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Creator_LocalType > Mp7JrsCreationPreferencesType_Creator_LocalPtr;

#define TypeOfCreationPreferencesType_DatePeriod_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_DatePeriod_CollectionType")))
#define CreateCreationPreferencesType_DatePeriod_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_DatePeriod_CollectionType))
class Mp7JrsCreationPreferencesType_DatePeriod_CollectionType;
class IMp7JrsCreationPreferencesType_DatePeriod_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_DatePeriod_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_DatePeriod_CollectionType > Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr;

#define TypeOfCreationPreferencesType_DatePeriod_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_DatePeriod_LocalType")))
#define CreateCreationPreferencesType_DatePeriod_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_DatePeriod_LocalType))
class Mp7JrsCreationPreferencesType_DatePeriod_LocalType;
class IMp7JrsCreationPreferencesType_DatePeriod_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_DatePeriod_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_DatePeriod_LocalType > Mp7JrsCreationPreferencesType_DatePeriod_LocalPtr;

#define TypeOfCreationPreferencesType_Keyword_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Keyword_CollectionType")))
#define CreateCreationPreferencesType_Keyword_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Keyword_CollectionType))
class Mp7JrsCreationPreferencesType_Keyword_CollectionType;
class IMp7JrsCreationPreferencesType_Keyword_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Keyword_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Keyword_CollectionType > Mp7JrsCreationPreferencesType_Keyword_CollectionPtr;

#define TypeOfCreationPreferencesType_Keyword_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Keyword_LocalType")))
#define CreateCreationPreferencesType_Keyword_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Keyword_LocalType))
class Mp7JrsCreationPreferencesType_Keyword_LocalType;
class IMp7JrsCreationPreferencesType_Keyword_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Keyword_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Keyword_LocalType > Mp7JrsCreationPreferencesType_Keyword_LocalPtr;

#define TypeOfCreationPreferencesType_Location_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Location_CollectionType")))
#define CreateCreationPreferencesType_Location_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Location_CollectionType))
class Mp7JrsCreationPreferencesType_Location_CollectionType;
class IMp7JrsCreationPreferencesType_Location_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Location_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Location_CollectionType > Mp7JrsCreationPreferencesType_Location_CollectionPtr;

#define TypeOfCreationPreferencesType_Location_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Location_LocalType")))
#define CreateCreationPreferencesType_Location_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Location_LocalType))
class Mp7JrsCreationPreferencesType_Location_LocalType;
class IMp7JrsCreationPreferencesType_Location_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Location_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Location_LocalType > Mp7JrsCreationPreferencesType_Location_LocalPtr;

#define TypeOfCreationPreferencesType_Title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Title_CollectionType")))
#define CreateCreationPreferencesType_Title_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Title_CollectionType))
class Mp7JrsCreationPreferencesType_Title_CollectionType;
class IMp7JrsCreationPreferencesType_Title_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Title_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Title_CollectionType > Mp7JrsCreationPreferencesType_Title_CollectionPtr;

#define TypeOfCreationPreferencesType_Title_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Title_LocalType")))
#define CreateCreationPreferencesType_Title_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Title_LocalType))
class Mp7JrsCreationPreferencesType_Title_LocalType;
class IMp7JrsCreationPreferencesType_Title_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Title_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Title_LocalType > Mp7JrsCreationPreferencesType_Title_LocalPtr;

#define TypeOfCreationPreferencesType_Tool_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Tool_CollectionType")))
#define CreateCreationPreferencesType_Tool_CollectionType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Tool_CollectionType))
class Mp7JrsCreationPreferencesType_Tool_CollectionType;
class IMp7JrsCreationPreferencesType_Tool_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Tool_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Tool_CollectionType > Mp7JrsCreationPreferencesType_Tool_CollectionPtr;

#define TypeOfCreationPreferencesType_Tool_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Tool_LocalType")))
#define CreateCreationPreferencesType_Tool_LocalType (Dc1Factory::CreateObject(TypeOfCreationPreferencesType_Tool_LocalType))
class Mp7JrsCreationPreferencesType_Tool_LocalType;
class IMp7JrsCreationPreferencesType_Tool_LocalType;
/** Smart pointer for instance of IMp7JrsCreationPreferencesType_Tool_LocalType */
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Tool_LocalType > Mp7JrsCreationPreferencesType_Tool_LocalPtr;

#define TypeOfCreationToolType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))
#define CreateCreationToolType (Dc1Factory::CreateObject(TypeOfCreationToolType))
class Mp7JrsCreationToolType;
class IMp7JrsCreationToolType;
/** Smart pointer for instance of IMp7JrsCreationToolType */
typedef Dc1Ptr< IMp7JrsCreationToolType > Mp7JrsCreationToolPtr;

#define TypeOfCreationToolType_Setting_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType_Setting_CollectionType")))
#define CreateCreationToolType_Setting_CollectionType (Dc1Factory::CreateObject(TypeOfCreationToolType_Setting_CollectionType))
class Mp7JrsCreationToolType_Setting_CollectionType;
class IMp7JrsCreationToolType_Setting_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationToolType_Setting_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationToolType_Setting_CollectionType > Mp7JrsCreationToolType_Setting_CollectionPtr;

#define TypeOfCreationToolType_Setting_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType_Setting_LocalType")))
#define CreateCreationToolType_Setting_LocalType (Dc1Factory::CreateObject(TypeOfCreationToolType_Setting_LocalType))
class Mp7JrsCreationToolType_Setting_LocalType;
class IMp7JrsCreationToolType_Setting_LocalType;
/** Smart pointer for instance of IMp7JrsCreationToolType_Setting_LocalType */
typedef Dc1Ptr< IMp7JrsCreationToolType_Setting_LocalType > Mp7JrsCreationToolType_Setting_LocalPtr;

#define TypeOfCreationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType")))
#define CreateCreationType (Dc1Factory::CreateObject(TypeOfCreationType))
class Mp7JrsCreationType;
class IMp7JrsCreationType;
/** Smart pointer for instance of IMp7JrsCreationType */
typedef Dc1Ptr< IMp7JrsCreationType > Mp7JrsCreationPtr;

#define TypeOfCreationType_Abstract_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_Abstract_CollectionType")))
#define CreateCreationType_Abstract_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_Abstract_CollectionType))
class Mp7JrsCreationType_Abstract_CollectionType;
class IMp7JrsCreationType_Abstract_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_Abstract_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_Abstract_CollectionType > Mp7JrsCreationType_Abstract_CollectionPtr;

#define TypeOfCreationType_CopyrightString_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_CopyrightString_CollectionType")))
#define CreateCreationType_CopyrightString_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_CopyrightString_CollectionType))
class Mp7JrsCreationType_CopyrightString_CollectionType;
class IMp7JrsCreationType_CopyrightString_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_CopyrightString_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_CopyrightString_CollectionType > Mp7JrsCreationType_CopyrightString_CollectionPtr;

#define TypeOfCreationType_CreationCoordinates_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_CreationCoordinates_CollectionType")))
#define CreateCreationType_CreationCoordinates_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_CreationCoordinates_CollectionType))
class Mp7JrsCreationType_CreationCoordinates_CollectionType;
class IMp7JrsCreationType_CreationCoordinates_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_CreationCoordinates_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_CreationCoordinates_CollectionType > Mp7JrsCreationType_CreationCoordinates_CollectionPtr;

#define TypeOfCreationType_CreationCoordinates_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_CreationCoordinates_LocalType")))
#define CreateCreationType_CreationCoordinates_LocalType (Dc1Factory::CreateObject(TypeOfCreationType_CreationCoordinates_LocalType))
class Mp7JrsCreationType_CreationCoordinates_LocalType;
class IMp7JrsCreationType_CreationCoordinates_LocalType;
/** Smart pointer for instance of IMp7JrsCreationType_CreationCoordinates_LocalType */
typedef Dc1Ptr< IMp7JrsCreationType_CreationCoordinates_LocalType > Mp7JrsCreationType_CreationCoordinates_LocalPtr;

#define TypeOfCreationType_CreationTool_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_CreationTool_CollectionType")))
#define CreateCreationType_CreationTool_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_CreationTool_CollectionType))
class Mp7JrsCreationType_CreationTool_CollectionType;
class IMp7JrsCreationType_CreationTool_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_CreationTool_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_CreationTool_CollectionType > Mp7JrsCreationType_CreationTool_CollectionPtr;

#define TypeOfCreationType_Creator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_Creator_CollectionType")))
#define CreateCreationType_Creator_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_Creator_CollectionType))
class Mp7JrsCreationType_Creator_CollectionType;
class IMp7JrsCreationType_Creator_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_Creator_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_Creator_CollectionType > Mp7JrsCreationType_Creator_CollectionPtr;

#define TypeOfCreationType_Title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_Title_CollectionType")))
#define CreateCreationType_Title_CollectionType (Dc1Factory::CreateObject(TypeOfCreationType_Title_CollectionType))
class Mp7JrsCreationType_Title_CollectionType;
class IMp7JrsCreationType_Title_CollectionType;
/** Smart pointer for instance of IMp7JrsCreationType_Title_CollectionType */
typedef Dc1Ptr< IMp7JrsCreationType_Title_CollectionType > Mp7JrsCreationType_Title_CollectionPtr;

#define TypeOfCreatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreatorType")))
#define CreateCreatorType (Dc1Factory::CreateObject(TypeOfCreatorType))
class Mp7JrsCreatorType;
class IMp7JrsCreatorType;
/** Smart pointer for instance of IMp7JrsCreatorType */
typedef Dc1Ptr< IMp7JrsCreatorType > Mp7JrsCreatorPtr;

#define TypeOfCreatorType_Character_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreatorType_Character_CollectionType")))
#define CreateCreatorType_Character_CollectionType (Dc1Factory::CreateObject(TypeOfCreatorType_Character_CollectionType))
class Mp7JrsCreatorType_Character_CollectionType;
class IMp7JrsCreatorType_Character_CollectionType;
/** Smart pointer for instance of IMp7JrsCreatorType_Character_CollectionType */
typedef Dc1Ptr< IMp7JrsCreatorType_Character_CollectionType > Mp7JrsCreatorType_Character_CollectionPtr;

#define TypeOfCreatorType_Instrument_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreatorType_Instrument_CollectionType")))
#define CreateCreatorType_Instrument_CollectionType (Dc1Factory::CreateObject(TypeOfCreatorType_Instrument_CollectionType))
class Mp7JrsCreatorType_Instrument_CollectionType;
class IMp7JrsCreatorType_Instrument_CollectionType;
/** Smart pointer for instance of IMp7JrsCreatorType_Instrument_CollectionType */
typedef Dc1Ptr< IMp7JrsCreatorType_Instrument_CollectionType > Mp7JrsCreatorType_Instrument_CollectionPtr;

#define TypeOfCrossChannelCorrelationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CrossChannelCorrelationType")))
#define CreateCrossChannelCorrelationType (Dc1Factory::CreateObject(TypeOfCrossChannelCorrelationType))
class Mp7JrsCrossChannelCorrelationType;
class IMp7JrsCrossChannelCorrelationType;
/** Smart pointer for instance of IMp7JrsCrossChannelCorrelationType */
typedef Dc1Ptr< IMp7JrsCrossChannelCorrelationType > Mp7JrsCrossChannelCorrelationPtr;

#define TypeOfcurrencyCode (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:currencyCode")))
#define CreatecurrencyCode (Dc1Factory::CreateObject(TypeOfcurrencyCode))
class Mp7JrscurrencyCode;
class IMp7JrscurrencyCode;
/** Smart pointer for instance of IMp7JrscurrencyCode */
typedef Dc1Ptr< IMp7JrscurrencyCode > Mp7JrscurrencyCodePtr;

#define TypeOfcurvatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType")))
#define CreatecurvatureType (Dc1Factory::CreateObject(TypeOfcurvatureType))
class Mp7JrscurvatureType;
class IMp7JrscurvatureType;
/** Smart pointer for instance of IMp7JrscurvatureType */
typedef Dc1Ptr< IMp7JrscurvatureType > Mp7JrscurvaturePtr;

#define TypeOfcurvatureType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType_CollectionType")))
#define CreatecurvatureType_CollectionType (Dc1Factory::CreateObject(TypeOfcurvatureType_CollectionType))
class Mp7JrscurvatureType_CollectionType;
class IMp7JrscurvatureType_CollectionType;
/** Smart pointer for instance of IMp7JrscurvatureType_CollectionType */
typedef Dc1Ptr< IMp7JrscurvatureType_CollectionType > Mp7JrscurvatureType_CollectionPtr;

#define TypeOfDcOffsetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DcOffsetType")))
#define CreateDcOffsetType (Dc1Factory::CreateObject(TypeOfDcOffsetType))
class Mp7JrsDcOffsetType;
class IMp7JrsDcOffsetType;
/** Smart pointer for instance of IMp7JrsDcOffsetType */
typedef Dc1Ptr< IMp7JrsDcOffsetType > Mp7JrsDcOffsetPtr;

#define TypeOfDecoderInitType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DecoderInitType")))
#define CreateDecoderInitType (Dc1Factory::CreateObject(TypeOfDecoderInitType))
class Mp7JrsDecoderInitType;
class IMp7JrsDecoderInitType;
/** Smart pointer for instance of IMp7JrsDecoderInitType */
typedef Dc1Ptr< IMp7JrsDecoderInitType > Mp7JrsDecoderInitPtr;

#define TypeOfDecoderInitType_SchemaReference_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DecoderInitType_SchemaReference_CollectionType")))
#define CreateDecoderInitType_SchemaReference_CollectionType (Dc1Factory::CreateObject(TypeOfDecoderInitType_SchemaReference_CollectionType))
class Mp7JrsDecoderInitType_SchemaReference_CollectionType;
class IMp7JrsDecoderInitType_SchemaReference_CollectionType;
/** Smart pointer for instance of IMp7JrsDecoderInitType_SchemaReference_CollectionType */
typedef Dc1Ptr< IMp7JrsDecoderInitType_SchemaReference_CollectionType > Mp7JrsDecoderInitType_SchemaReference_CollectionPtr;

#define TypeOfdegreeAccidentalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:degreeAccidentalType")))
#define CreatedegreeAccidentalType (Dc1Factory::CreateObject(TypeOfdegreeAccidentalType))
class Mp7JrsdegreeAccidentalType;
class IMp7JrsdegreeAccidentalType;
// No smart pointer instance for IMp7JrsdegreeAccidentalType

#define TypeOfdegreeNoteType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:degreeNoteType")))
#define CreatedegreeNoteType (Dc1Factory::CreateObject(TypeOfdegreeNoteType))
class Mp7JrsdegreeNoteType;
class IMp7JrsdegreeNoteType;
// No smart pointer instance for IMp7JrsdegreeNoteType

#define TypeOfdependencyOperatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:dependencyOperatorType")))
#define CreatedependencyOperatorType (Dc1Factory::CreateObject(TypeOfdependencyOperatorType))
class Mp7JrsdependencyOperatorType;
class IMp7JrsdependencyOperatorType;
// No smart pointer instance for IMp7JrsdependencyOperatorType

#define TypeOfDependencyStructurePhraseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))
#define CreateDependencyStructurePhraseType (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType))
class Mp7JrsDependencyStructurePhraseType;
class IMp7JrsDependencyStructurePhraseType;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType > Mp7JrsDependencyStructurePhrasePtr;

#define TypeOfDependencyStructurePhraseType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType_CollectionType")))
#define CreateDependencyStructurePhraseType_CollectionType (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType_CollectionType))
class Mp7JrsDependencyStructurePhraseType_CollectionType;
class IMp7JrsDependencyStructurePhraseType_CollectionType;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType_CollectionType */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType_CollectionType > Mp7JrsDependencyStructurePhraseType_CollectionPtr;

#define TypeOfDependencyStructurePhraseType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType_CollectionType0")))
#define CreateDependencyStructurePhraseType_CollectionType0 (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType_CollectionType0))
class Mp7JrsDependencyStructurePhraseType_CollectionType0;
class IMp7JrsDependencyStructurePhraseType_CollectionType0;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType_CollectionType0 > Mp7JrsDependencyStructurePhraseType_Collection0Ptr;

#define TypeOfDependencyStructurePhraseType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType_LocalType")))
#define CreateDependencyStructurePhraseType_LocalType (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType_LocalType))
class Mp7JrsDependencyStructurePhraseType_LocalType;
class IMp7JrsDependencyStructurePhraseType_LocalType;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType_LocalType */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType_LocalType > Mp7JrsDependencyStructurePhraseType_LocalPtr;

#define TypeOfDependencyStructurePhraseType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType_LocalType0")))
#define CreateDependencyStructurePhraseType_LocalType0 (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType_LocalType0))
class Mp7JrsDependencyStructurePhraseType_LocalType0;
class IMp7JrsDependencyStructurePhraseType_LocalType0;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType_LocalType0 */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType_LocalType0 > Mp7JrsDependencyStructurePhraseType_Local0Ptr;

#define TypeOfDependencyStructurePhraseType_operator_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType_operator_LocalType")))
#define CreateDependencyStructurePhraseType_operator_LocalType (Dc1Factory::CreateObject(TypeOfDependencyStructurePhraseType_operator_LocalType))
class Mp7JrsDependencyStructurePhraseType_operator_LocalType;
class IMp7JrsDependencyStructurePhraseType_operator_LocalType;
/** Smart pointer for instance of IMp7JrsDependencyStructurePhraseType_operator_LocalType */
typedef Dc1Ptr< IMp7JrsDependencyStructurePhraseType_operator_LocalType > Mp7JrsDependencyStructurePhraseType_operator_LocalPtr;

#define TypeOfDependencyStructureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructureType")))
#define CreateDependencyStructureType (Dc1Factory::CreateObject(TypeOfDependencyStructureType))
class Mp7JrsDependencyStructureType;
class IMp7JrsDependencyStructureType;
/** Smart pointer for instance of IMp7JrsDependencyStructureType */
typedef Dc1Ptr< IMp7JrsDependencyStructureType > Mp7JrsDependencyStructurePtr;

#define TypeOfDependencyStructureType_Sentence_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructureType_Sentence_CollectionType")))
#define CreateDependencyStructureType_Sentence_CollectionType (Dc1Factory::CreateObject(TypeOfDependencyStructureType_Sentence_CollectionType))
class Mp7JrsDependencyStructureType_Sentence_CollectionType;
class IMp7JrsDependencyStructureType_Sentence_CollectionType;
/** Smart pointer for instance of IMp7JrsDependencyStructureType_Sentence_CollectionType */
typedef Dc1Ptr< IMp7JrsDependencyStructureType_Sentence_CollectionType > Mp7JrsDependencyStructureType_Sentence_CollectionPtr;

#define TypeOfDescriptionMetadataType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType")))
#define CreateDescriptionMetadataType (Dc1Factory::CreateObject(TypeOfDescriptionMetadataType))
class Mp7JrsDescriptionMetadataType;
class IMp7JrsDescriptionMetadataType;
/** Smart pointer for instance of IMp7JrsDescriptionMetadataType */
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType > Mp7JrsDescriptionMetadataPtr;

#define TypeOfDescriptionMetadataType_Creator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType_Creator_CollectionType")))
#define CreateDescriptionMetadataType_Creator_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptionMetadataType_Creator_CollectionType))
class Mp7JrsDescriptionMetadataType_Creator_CollectionType;
class IMp7JrsDescriptionMetadataType_Creator_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptionMetadataType_Creator_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_Creator_CollectionType > Mp7JrsDescriptionMetadataType_Creator_CollectionPtr;

#define TypeOfDescriptionMetadataType_Instrument_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType_Instrument_CollectionType")))
#define CreateDescriptionMetadataType_Instrument_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptionMetadataType_Instrument_CollectionType))
class Mp7JrsDescriptionMetadataType_Instrument_CollectionType;
class IMp7JrsDescriptionMetadataType_Instrument_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptionMetadataType_Instrument_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_Instrument_CollectionType > Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr;

#define TypeOfDescriptionMetadataType_PrivateIdentifier_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType_PrivateIdentifier_CollectionType")))
#define CreateDescriptionMetadataType_PrivateIdentifier_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptionMetadataType_PrivateIdentifier_CollectionType))
class Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType;
class IMp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType > Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr;

#define TypeOfDescriptionMetadataType_PublicIdentifier_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType_PublicIdentifier_CollectionType")))
#define CreateDescriptionMetadataType_PublicIdentifier_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptionMetadataType_PublicIdentifier_CollectionType))
class Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType;
class IMp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType > Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr;

#define TypeOfDescriptionProfileType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionProfileType")))
#define CreateDescriptionProfileType (Dc1Factory::CreateObject(TypeOfDescriptionProfileType))
class Mp7JrsDescriptionProfileType;
class IMp7JrsDescriptionProfileType;
/** Smart pointer for instance of IMp7JrsDescriptionProfileType */
typedef Dc1Ptr< IMp7JrsDescriptionProfileType > Mp7JrsDescriptionProfilePtr;

#define TypeOfDescriptionProfileType_profileAndLevelIndication_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionProfileType_profileAndLevelIndication_CollectionType")))
#define CreateDescriptionProfileType_profileAndLevelIndication_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptionProfileType_profileAndLevelIndication_CollectionType))
class Mp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionType;
class IMp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionType > Mp7JrsDescriptionProfileType_profileAndLevelIndication_CollectionPtr;

#define TypeOfDescriptorCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorCollectionType")))
#define CreateDescriptorCollectionType (Dc1Factory::CreateObject(TypeOfDescriptorCollectionType))
class Mp7JrsDescriptorCollectionType;
class IMp7JrsDescriptorCollectionType;
/** Smart pointer for instance of IMp7JrsDescriptorCollectionType */
typedef Dc1Ptr< IMp7JrsDescriptorCollectionType > Mp7JrsDescriptorCollectionPtr;

#define TypeOfDescriptorCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorCollectionType_CollectionType")))
#define CreateDescriptorCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptorCollectionType_CollectionType))
class Mp7JrsDescriptorCollectionType_CollectionType;
class IMp7JrsDescriptorCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptorCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptorCollectionType_CollectionType > Mp7JrsDescriptorCollectionType_CollectionPtr;

#define TypeOfDescriptorCollectionType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorCollectionType_Descriptor_CollectionType")))
#define CreateDescriptorCollectionType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptorCollectionType_Descriptor_CollectionType))
class Mp7JrsDescriptorCollectionType_Descriptor_CollectionType;
class IMp7JrsDescriptorCollectionType_Descriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptorCollectionType_Descriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptorCollectionType_Descriptor_CollectionType > Mp7JrsDescriptorCollectionType_Descriptor_CollectionPtr;

#define TypeOfDescriptorCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorCollectionType_LocalType")))
#define CreateDescriptorCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfDescriptorCollectionType_LocalType))
class Mp7JrsDescriptorCollectionType_LocalType;
class IMp7JrsDescriptorCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsDescriptorCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsDescriptorCollectionType_LocalType > Mp7JrsDescriptorCollectionType_LocalPtr;

#define TypeOfDescriptorModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorModelType")))
#define CreateDescriptorModelType (Dc1Factory::CreateObject(TypeOfDescriptorModelType))
class Mp7JrsDescriptorModelType;
class IMp7JrsDescriptorModelType;
/** Smart pointer for instance of IMp7JrsDescriptorModelType */
typedef Dc1Ptr< IMp7JrsDescriptorModelType > Mp7JrsDescriptorModelPtr;

#define TypeOfDescriptorModelType_Field_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorModelType_Field_CollectionType")))
#define CreateDescriptorModelType_Field_CollectionType (Dc1Factory::CreateObject(TypeOfDescriptorModelType_Field_CollectionType))
class Mp7JrsDescriptorModelType_Field_CollectionType;
class IMp7JrsDescriptorModelType_Field_CollectionType;
/** Smart pointer for instance of IMp7JrsDescriptorModelType_Field_CollectionType */
typedef Dc1Ptr< IMp7JrsDescriptorModelType_Field_CollectionType > Mp7JrsDescriptorModelType_Field_CollectionPtr;

#define TypeOfdim_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:dim_LocalType")))
#define Createdim_LocalType (Dc1Factory::CreateObject(TypeOfdim_LocalType))
class Mp7Jrsdim_LocalType;
class IMp7Jrsdim_LocalType;
/** Smart pointer for instance of IMp7Jrsdim_LocalType */
typedef Dc1Ptr< IMp7Jrsdim_LocalType > Mp7Jrsdim_LocalPtr;

#define TypeOfDiscreteDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteDistributionType")))
#define CreateDiscreteDistributionType (Dc1Factory::CreateObject(TypeOfDiscreteDistributionType))
class Mp7JrsDiscreteDistributionType;
class IMp7JrsDiscreteDistributionType;
/** Smart pointer for instance of IMp7JrsDiscreteDistributionType */
typedef Dc1Ptr< IMp7JrsDiscreteDistributionType > Mp7JrsDiscreteDistributionPtr;

#define TypeOfDiscreteHiddenMarkovModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteHiddenMarkovModelType")))
#define CreateDiscreteHiddenMarkovModelType (Dc1Factory::CreateObject(TypeOfDiscreteHiddenMarkovModelType))
class Mp7JrsDiscreteHiddenMarkovModelType;
class IMp7JrsDiscreteHiddenMarkovModelType;
/** Smart pointer for instance of IMp7JrsDiscreteHiddenMarkovModelType */
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType > Mp7JrsDiscreteHiddenMarkovModelPtr;

#define TypeOfDiscreteHiddenMarkovModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteHiddenMarkovModelType_CollectionType")))
#define CreateDiscreteHiddenMarkovModelType_CollectionType (Dc1Factory::CreateObject(TypeOfDiscreteHiddenMarkovModelType_CollectionType))
class Mp7JrsDiscreteHiddenMarkovModelType_CollectionType;
class IMp7JrsDiscreteHiddenMarkovModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsDiscreteHiddenMarkovModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType_CollectionType > Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr;

#define TypeOfDiscreteHiddenMarkovModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteHiddenMarkovModelType_LocalType")))
#define CreateDiscreteHiddenMarkovModelType_LocalType (Dc1Factory::CreateObject(TypeOfDiscreteHiddenMarkovModelType_LocalType))
class Mp7JrsDiscreteHiddenMarkovModelType_LocalType;
class IMp7JrsDiscreteHiddenMarkovModelType_LocalType;
/** Smart pointer for instance of IMp7JrsDiscreteHiddenMarkovModelType_LocalType */
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType_LocalType > Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr;

#define TypeOfDiscreteHiddenMarkovModelType_Observation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteHiddenMarkovModelType_Observation_CollectionType")))
#define CreateDiscreteHiddenMarkovModelType_Observation_CollectionType (Dc1Factory::CreateObject(TypeOfDiscreteHiddenMarkovModelType_Observation_CollectionType))
class Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType;
class IMp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType;
/** Smart pointer for instance of IMp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType */
typedef Dc1Ptr< IMp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType > Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr;

#define TypeOfDiscreteUniformDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DiscreteUniformDistributionType")))
#define CreateDiscreteUniformDistributionType (Dc1Factory::CreateObject(TypeOfDiscreteUniformDistributionType))
class Mp7JrsDiscreteUniformDistributionType;
class IMp7JrsDiscreteUniformDistributionType;
/** Smart pointer for instance of IMp7JrsDiscreteUniformDistributionType */
typedef Dc1Ptr< IMp7JrsDiscreteUniformDistributionType > Mp7JrsDiscreteUniformDistributionPtr;

#define TypeOfDisseminationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DisseminationType")))
#define CreateDisseminationType (Dc1Factory::CreateObject(TypeOfDisseminationType))
class Mp7JrsDisseminationType;
class IMp7JrsDisseminationType;
/** Smart pointer for instance of IMp7JrsDisseminationType */
typedef Dc1Ptr< IMp7JrsDisseminationType > Mp7JrsDisseminationPtr;

#define TypeOfDistributionPerceptualAttributeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DistributionPerceptualAttributeType")))
#define CreateDistributionPerceptualAttributeType (Dc1Factory::CreateObject(TypeOfDistributionPerceptualAttributeType))
class Mp7JrsDistributionPerceptualAttributeType;
class IMp7JrsDistributionPerceptualAttributeType;
/** Smart pointer for instance of IMp7JrsDistributionPerceptualAttributeType */
typedef Dc1Ptr< IMp7JrsDistributionPerceptualAttributeType > Mp7JrsDistributionPerceptualAttributePtr;

#define TypeOfDominantColorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType")))
#define CreateDominantColorType (Dc1Factory::CreateObject(TypeOfDominantColorType))
class Mp7JrsDominantColorType;
class IMp7JrsDominantColorType;
/** Smart pointer for instance of IMp7JrsDominantColorType */
typedef Dc1Ptr< IMp7JrsDominantColorType > Mp7JrsDominantColorPtr;

#define TypeOfDominantColorType_Value_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_CollectionType")))
#define CreateDominantColorType_Value_CollectionType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_CollectionType))
class Mp7JrsDominantColorType_Value_CollectionType;
class IMp7JrsDominantColorType_Value_CollectionType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_CollectionType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_CollectionType > Mp7JrsDominantColorType_Value_CollectionPtr;

#define TypeOfDominantColorType_Value_ColorVariance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_ColorVariance_CollectionType")))
#define CreateDominantColorType_Value_ColorVariance_CollectionType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_ColorVariance_CollectionType))
class Mp7JrsDominantColorType_Value_ColorVariance_CollectionType;
class IMp7JrsDominantColorType_Value_ColorVariance_CollectionType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_ColorVariance_CollectionType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_ColorVariance_CollectionType > Mp7JrsDominantColorType_Value_ColorVariance_CollectionPtr;

#define TypeOfDominantColorType_Value_ColorVariance_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_ColorVariance_LocalType")))
#define CreateDominantColorType_Value_ColorVariance_LocalType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_ColorVariance_LocalType))
class Mp7JrsDominantColorType_Value_ColorVariance_LocalType;
class IMp7JrsDominantColorType_Value_ColorVariance_LocalType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_ColorVariance_LocalType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_ColorVariance_LocalType > Mp7JrsDominantColorType_Value_ColorVariance_LocalPtr;

#define TypeOfDominantColorType_Value_Index_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_Index_CollectionType")))
#define CreateDominantColorType_Value_Index_CollectionType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_Index_CollectionType))
class Mp7JrsDominantColorType_Value_Index_CollectionType;
class IMp7JrsDominantColorType_Value_Index_CollectionType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_Index_CollectionType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_Index_CollectionType > Mp7JrsDominantColorType_Value_Index_CollectionPtr;

#define TypeOfDominantColorType_Value_Index_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_Index_LocalType")))
#define CreateDominantColorType_Value_Index_LocalType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_Index_LocalType))
class Mp7JrsDominantColorType_Value_Index_LocalType;
class IMp7JrsDominantColorType_Value_Index_LocalType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_Index_LocalType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_Index_LocalType > Mp7JrsDominantColorType_Value_Index_LocalPtr;

#define TypeOfDominantColorType_Value_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_LocalType")))
#define CreateDominantColorType_Value_LocalType (Dc1Factory::CreateObject(TypeOfDominantColorType_Value_LocalType))
class Mp7JrsDominantColorType_Value_LocalType;
class IMp7JrsDominantColorType_Value_LocalType;
/** Smart pointer for instance of IMp7JrsDominantColorType_Value_LocalType */
typedef Dc1Ptr< IMp7JrsDominantColorType_Value_LocalType > Mp7JrsDominantColorType_Value_LocalPtr;

#define TypeOfDoubleDiagonalMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleDiagonalMatrixType")))
#define CreateDoubleDiagonalMatrixType (Dc1Factory::CreateObject(TypeOfDoubleDiagonalMatrixType))
class Mp7JrsDoubleDiagonalMatrixType;
class IMp7JrsDoubleDiagonalMatrixType;
/** Smart pointer for instance of IMp7JrsDoubleDiagonalMatrixType */
typedef Dc1Ptr< IMp7JrsDoubleDiagonalMatrixType > Mp7JrsDoubleDiagonalMatrixPtr;

#define TypeOfDoubleMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))
#define CreateDoubleMatrixType (Dc1Factory::CreateObject(TypeOfDoubleMatrixType))
class Mp7JrsDoubleMatrixType;
class IMp7JrsDoubleMatrixType;
/** Smart pointer for instance of IMp7JrsDoubleMatrixType */
typedef Dc1Ptr< IMp7JrsDoubleMatrixType > Mp7JrsDoubleMatrixPtr;

#define TypeOfDoublePullbackDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoublePullbackDefinitionType")))
#define CreateDoublePullbackDefinitionType (Dc1Factory::CreateObject(TypeOfDoublePullbackDefinitionType))
class Mp7JrsDoublePullbackDefinitionType;
class IMp7JrsDoublePullbackDefinitionType;
/** Smart pointer for instance of IMp7JrsDoublePullbackDefinitionType */
typedef Dc1Ptr< IMp7JrsDoublePullbackDefinitionType > Mp7JrsDoublePullbackDefinitionPtr;

#define TypeOfDoublePullbackDefinitionType_MorphismGraph_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoublePullbackDefinitionType_MorphismGraph_CollectionType")))
#define CreateDoublePullbackDefinitionType_MorphismGraph_CollectionType (Dc1Factory::CreateObject(TypeOfDoublePullbackDefinitionType_MorphismGraph_CollectionType))
class Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionType;
class IMp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionType;
/** Smart pointer for instance of IMp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionType */
typedef Dc1Ptr< IMp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionType > Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr;

#define TypeOfDoublePullbackDefinitionType_RuleGraph_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoublePullbackDefinitionType_RuleGraph_CollectionType")))
#define CreateDoublePullbackDefinitionType_RuleGraph_CollectionType (Dc1Factory::CreateObject(TypeOfDoublePullbackDefinitionType_RuleGraph_CollectionType))
class Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionType;
class IMp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionType;
/** Smart pointer for instance of IMp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionType */
typedef Dc1Ptr< IMp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionType > Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr;

#define TypeOfDoublePushoutDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoublePushoutDefinitionType")))
#define CreateDoublePushoutDefinitionType (Dc1Factory::CreateObject(TypeOfDoublePushoutDefinitionType))
class Mp7JrsDoublePushoutDefinitionType;
class IMp7JrsDoublePushoutDefinitionType;
/** Smart pointer for instance of IMp7JrsDoublePushoutDefinitionType */
typedef Dc1Ptr< IMp7JrsDoublePushoutDefinitionType > Mp7JrsDoublePushoutDefinitionPtr;

#define TypeOfDoublePushoutDefinitionType_MorphismGraph_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoublePushoutDefinitionType_MorphismGraph_CollectionType")))
#define CreateDoublePushoutDefinitionType_MorphismGraph_CollectionType (Dc1Factory::CreateObject(TypeOfDoublePushoutDefinitionType_MorphismGraph_CollectionType))
class Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionType;
class IMp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionType;
/** Smart pointer for instance of IMp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionType */
typedef Dc1Ptr< IMp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionType > Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr;

#define TypeOfdoubleVector (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:doubleVector")))
#define CreatedoubleVector (Dc1Factory::CreateObject(TypeOfdoubleVector))
class Mp7JrsdoubleVector;
class IMp7JrsdoubleVector;
/** Smart pointer for instance of IMp7JrsdoubleVector */
typedef Dc1Ptr< IMp7JrsdoubleVector > Mp7JrsdoubleVectorPtr;

#define TypeOfDSType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DSType")))
#define CreateDSType (Dc1Factory::CreateObject(TypeOfDSType))
class Mp7JrsDSType;
class IMp7JrsDSType;
/** Smart pointer for instance of IMp7JrsDSType */
typedef Dc1Ptr< IMp7JrsDSType > Mp7JrsDSPtr;

#define TypeOfDSType_Header_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DSType_Header_CollectionType")))
#define CreateDSType_Header_CollectionType (Dc1Factory::CreateObject(TypeOfDSType_Header_CollectionType))
class Mp7JrsDSType_Header_CollectionType;
class IMp7JrsDSType_Header_CollectionType;
/** Smart pointer for instance of IMp7JrsDSType_Header_CollectionType */
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;

#define TypeOfDType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DType")))
#define CreateDType (Dc1Factory::CreateObject(TypeOfDType))
class Mp7JrsDType;
class IMp7JrsDType;
/** Smart pointer for instance of IMp7JrsDType */
typedef Dc1Ptr< IMp7JrsDType > Mp7JrsDPtr;

#define TypeOfdurationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:durationType")))
#define CreatedurationType (Dc1Factory::CreateObject(TypeOfdurationType))
class Mp7JrsdurationType;
class IMp7JrsdurationType;
/** Smart pointer for instance of IMp7JrsdurationType */
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;

#define TypeOfEdgeHistogramType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EdgeHistogramType")))
#define CreateEdgeHistogramType (Dc1Factory::CreateObject(TypeOfEdgeHistogramType))
class Mp7JrsEdgeHistogramType;
class IMp7JrsEdgeHistogramType;
/** Smart pointer for instance of IMp7JrsEdgeHistogramType */
typedef Dc1Ptr< IMp7JrsEdgeHistogramType > Mp7JrsEdgeHistogramPtr;

#define TypeOfEdgeHistogramType_BinCounts_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EdgeHistogramType_BinCounts_CollectionType")))
#define CreateEdgeHistogramType_BinCounts_CollectionType (Dc1Factory::CreateObject(TypeOfEdgeHistogramType_BinCounts_CollectionType))
class Mp7JrsEdgeHistogramType_BinCounts_CollectionType;
class IMp7JrsEdgeHistogramType_BinCounts_CollectionType;
/** Smart pointer for instance of IMp7JrsEdgeHistogramType_BinCounts_CollectionType */
typedef Dc1Ptr< IMp7JrsEdgeHistogramType_BinCounts_CollectionType > Mp7JrsEdgeHistogramType_BinCounts_CollectionPtr;

#define TypeOfEdgeHistogramType_BinCounts_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EdgeHistogramType_BinCounts_LocalType")))
#define CreateEdgeHistogramType_BinCounts_LocalType (Dc1Factory::CreateObject(TypeOfEdgeHistogramType_BinCounts_LocalType))
class Mp7JrsEdgeHistogramType_BinCounts_LocalType;
class IMp7JrsEdgeHistogramType_BinCounts_LocalType;
/** Smart pointer for instance of IMp7JrsEdgeHistogramType_BinCounts_LocalType */
typedef Dc1Ptr< IMp7JrsEdgeHistogramType_BinCounts_LocalType > Mp7JrsEdgeHistogramType_BinCounts_LocalPtr;

#define TypeOfEditedMovingRegionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedMovingRegionType")))
#define CreateEditedMovingRegionType (Dc1Factory::CreateObject(TypeOfEditedMovingRegionType))
class Mp7JrsEditedMovingRegionType;
class IMp7JrsEditedMovingRegionType;
/** Smart pointer for instance of IMp7JrsEditedMovingRegionType */
typedef Dc1Ptr< IMp7JrsEditedMovingRegionType > Mp7JrsEditedMovingRegionPtr;

#define TypeOfEditedVideoEditingTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedVideoEditingTemporalDecompositionType")))
#define CreateEditedVideoEditingTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfEditedVideoEditingTemporalDecompositionType))
class Mp7JrsEditedVideoEditingTemporalDecompositionType;
class IMp7JrsEditedVideoEditingTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsEditedVideoEditingTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsEditedVideoEditingTemporalDecompositionType > Mp7JrsEditedVideoEditingTemporalDecompositionPtr;

#define TypeOfEditedVideoEditingTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedVideoEditingTemporalDecompositionType_CollectionType")))
#define CreateEditedVideoEditingTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfEditedVideoEditingTemporalDecompositionType_CollectionType))
class Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionType;
class IMp7JrsEditedVideoEditingTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsEditedVideoEditingTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsEditedVideoEditingTemporalDecompositionType_CollectionType > Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr;

#define TypeOfEditedVideoEditingTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedVideoEditingTemporalDecompositionType_LocalType")))
#define CreateEditedVideoEditingTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfEditedVideoEditingTemporalDecompositionType_LocalType))
class Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType;
class IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType > Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr;

#define TypeOfEditedVideoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedVideoType")))
#define CreateEditedVideoType (Dc1Factory::CreateObject(TypeOfEditedVideoType))
class Mp7JrsEditedVideoType;
class IMp7JrsEditedVideoType;
/** Smart pointer for instance of IMp7JrsEditedVideoType */
typedef Dc1Ptr< IMp7JrsEditedVideoType > Mp7JrsEditedVideoPtr;

#define TypeOfEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType")))
#define CreateEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType))
class Mp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType;
class IMp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionType > Mp7JrsEditedVideoType_AnalyticEditingTemporalDecomposition_CollectionPtr;

#define TypeOfElectronicAddressType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType")))
#define CreateElectronicAddressType (Dc1Factory::CreateObject(TypeOfElectronicAddressType))
class Mp7JrsElectronicAddressType;
class IMp7JrsElectronicAddressType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType > Mp7JrsElectronicAddressPtr;

#define TypeOfElectronicAddressType_Email_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Email_CollectionType")))
#define CreateElectronicAddressType_Email_CollectionType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Email_CollectionType))
class Mp7JrsElectronicAddressType_Email_CollectionType;
class IMp7JrsElectronicAddressType_Email_CollectionType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType_Email_CollectionType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType_Email_CollectionType > Mp7JrsElectronicAddressType_Email_CollectionPtr;

#define TypeOfElectronicAddressType_Fax_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Fax_CollectionType")))
#define CreateElectronicAddressType_Fax_CollectionType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Fax_CollectionType))
class Mp7JrsElectronicAddressType_Fax_CollectionType;
class IMp7JrsElectronicAddressType_Fax_CollectionType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType_Fax_CollectionType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType_Fax_CollectionType > Mp7JrsElectronicAddressType_Fax_CollectionPtr;

#define TypeOfElectronicAddressType_Telephone_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Telephone_CollectionType")))
#define CreateElectronicAddressType_Telephone_CollectionType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Telephone_CollectionType))
class Mp7JrsElectronicAddressType_Telephone_CollectionType;
class IMp7JrsElectronicAddressType_Telephone_CollectionType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType_Telephone_CollectionType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType_Telephone_CollectionType > Mp7JrsElectronicAddressType_Telephone_CollectionPtr;

#define TypeOfElectronicAddressType_Telephone_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Telephone_LocalType")))
#define CreateElectronicAddressType_Telephone_LocalType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Telephone_LocalType))
class Mp7JrsElectronicAddressType_Telephone_LocalType;
class IMp7JrsElectronicAddressType_Telephone_LocalType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType_Telephone_LocalType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType_Telephone_LocalType > Mp7JrsElectronicAddressType_Telephone_LocalPtr;

#define TypeOfElectronicAddressType_Telephone_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Telephone_type_LocalType")))
#define CreateElectronicAddressType_Telephone_type_LocalType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Telephone_type_LocalType))
class Mp7JrsElectronicAddressType_Telephone_type_LocalType;
class IMp7JrsElectronicAddressType_Telephone_type_LocalType;
// No smart pointer instance for IMp7JrsElectronicAddressType_Telephone_type_LocalType

#define TypeOfElectronicAddressType_Url_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType_Url_CollectionType")))
#define CreateElectronicAddressType_Url_CollectionType (Dc1Factory::CreateObject(TypeOfElectronicAddressType_Url_CollectionType))
class Mp7JrsElectronicAddressType_Url_CollectionType;
class IMp7JrsElectronicAddressType_Url_CollectionType;
/** Smart pointer for instance of IMp7JrsElectronicAddressType_Url_CollectionType */
typedef Dc1Ptr< IMp7JrsElectronicAddressType_Url_CollectionType > Mp7JrsElectronicAddressType_Url_CollectionPtr;

#define TypeOfEnhancedAudioSignatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EnhancedAudioSignatureType")))
#define CreateEnhancedAudioSignatureType (Dc1Factory::CreateObject(TypeOfEnhancedAudioSignatureType))
class Mp7JrsEnhancedAudioSignatureType;
class IMp7JrsEnhancedAudioSignatureType;
/** Smart pointer for instance of IMp7JrsEnhancedAudioSignatureType */
typedef Dc1Ptr< IMp7JrsEnhancedAudioSignatureType > Mp7JrsEnhancedAudioSignaturePtr;

#define TypeOfErrorEventType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ErrorEventType")))
#define CreateErrorEventType (Dc1Factory::CreateObject(TypeOfErrorEventType))
class Mp7JrsErrorEventType;
class IMp7JrsErrorEventType;
/** Smart pointer for instance of IMp7JrsErrorEventType */
typedef Dc1Ptr< IMp7JrsErrorEventType > Mp7JrsErrorEventPtr;

#define TypeOfErrorEventType_DetectionProcess_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ErrorEventType_DetectionProcess_LocalType")))
#define CreateErrorEventType_DetectionProcess_LocalType (Dc1Factory::CreateObject(TypeOfErrorEventType_DetectionProcess_LocalType))
class Mp7JrsErrorEventType_DetectionProcess_LocalType;
class IMp7JrsErrorEventType_DetectionProcess_LocalType;
// No smart pointer instance for IMp7JrsErrorEventType_DetectionProcess_LocalType

#define TypeOfErrorEventType_Status_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ErrorEventType_Status_LocalType")))
#define CreateErrorEventType_Status_LocalType (Dc1Factory::CreateObject(TypeOfErrorEventType_Status_LocalType))
class Mp7JrsErrorEventType_Status_LocalType;
class IMp7JrsErrorEventType_Status_LocalType;
// No smart pointer instance for IMp7JrsErrorEventType_Status_LocalType

#define TypeOfEventType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType")))
#define CreateEventType (Dc1Factory::CreateObject(TypeOfEventType))
class Mp7JrsEventType;
class IMp7JrsEventType;
/** Smart pointer for instance of IMp7JrsEventType */
typedef Dc1Ptr< IMp7JrsEventType > Mp7JrsEventPtr;

#define TypeOfEventType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType_CollectionType")))
#define CreateEventType_CollectionType (Dc1Factory::CreateObject(TypeOfEventType_CollectionType))
class Mp7JrsEventType_CollectionType;
class IMp7JrsEventType_CollectionType;
/** Smart pointer for instance of IMp7JrsEventType_CollectionType */
typedef Dc1Ptr< IMp7JrsEventType_CollectionType > Mp7JrsEventType_CollectionPtr;

#define TypeOfEventType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType_LocalType")))
#define CreateEventType_LocalType (Dc1Factory::CreateObject(TypeOfEventType_LocalType))
class Mp7JrsEventType_LocalType;
class IMp7JrsEventType_LocalType;
/** Smart pointer for instance of IMp7JrsEventType_LocalType */
typedef Dc1Ptr< IMp7JrsEventType_LocalType > Mp7JrsEventType_LocalPtr;

#define TypeOfEventType_SemanticPlace_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType_SemanticPlace_CollectionType")))
#define CreateEventType_SemanticPlace_CollectionType (Dc1Factory::CreateObject(TypeOfEventType_SemanticPlace_CollectionType))
class Mp7JrsEventType_SemanticPlace_CollectionType;
class IMp7JrsEventType_SemanticPlace_CollectionType;
/** Smart pointer for instance of IMp7JrsEventType_SemanticPlace_CollectionType */
typedef Dc1Ptr< IMp7JrsEventType_SemanticPlace_CollectionType > Mp7JrsEventType_SemanticPlace_CollectionPtr;

#define TypeOfEventType_SemanticTime_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType_SemanticTime_CollectionType")))
#define CreateEventType_SemanticTime_CollectionType (Dc1Factory::CreateObject(TypeOfEventType_SemanticTime_CollectionType))
class Mp7JrsEventType_SemanticTime_CollectionType;
class IMp7JrsEventType_SemanticTime_CollectionType;
/** Smart pointer for instance of IMp7JrsEventType_SemanticTime_CollectionType */
typedef Dc1Ptr< IMp7JrsEventType_SemanticTime_CollectionType > Mp7JrsEventType_SemanticTime_CollectionPtr;

#define TypeOfExponentialDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExponentialDistributionType")))
#define CreateExponentialDistributionType (Dc1Factory::CreateObject(TypeOfExponentialDistributionType))
class Mp7JrsExponentialDistributionType;
class IMp7JrsExponentialDistributionType;
/** Smart pointer for instance of IMp7JrsExponentialDistributionType */
typedef Dc1Ptr< IMp7JrsExponentialDistributionType > Mp7JrsExponentialDistributionPtr;

#define TypeOfExtendedElectronicAddressType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedElectronicAddressType")))
#define CreateExtendedElectronicAddressType (Dc1Factory::CreateObject(TypeOfExtendedElectronicAddressType))
class Mp7JrsExtendedElectronicAddressType;
class IMp7JrsExtendedElectronicAddressType;
/** Smart pointer for instance of IMp7JrsExtendedElectronicAddressType */
typedef Dc1Ptr< IMp7JrsExtendedElectronicAddressType > Mp7JrsExtendedElectronicAddressPtr;

#define TypeOfExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType")))
#define CreateExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType (Dc1Factory::CreateObject(TypeOfExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType))
class Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType;
class IMp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType;
/** Smart pointer for instance of IMp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType */
typedef Dc1Ptr< IMp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType > Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr;

#define TypeOfExtendedLanguageType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedLanguageType")))
#define CreateExtendedLanguageType (Dc1Factory::CreateObject(TypeOfExtendedLanguageType))
class Mp7JrsExtendedLanguageType;
class IMp7JrsExtendedLanguageType;
/** Smart pointer for instance of IMp7JrsExtendedLanguageType */
typedef Dc1Ptr< IMp7JrsExtendedLanguageType > Mp7JrsExtendedLanguagePtr;

#define TypeOfExtendedLanguageType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedLanguageType_type_LocalType")))
#define CreateExtendedLanguageType_type_LocalType (Dc1Factory::CreateObject(TypeOfExtendedLanguageType_type_LocalType))
class Mp7JrsExtendedLanguageType_type_LocalType;
class IMp7JrsExtendedLanguageType_type_LocalType;
// No smart pointer instance for IMp7JrsExtendedLanguageType_type_LocalType

#define TypeOfExtendedMediaQualityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedMediaQualityType")))
#define CreateExtendedMediaQualityType (Dc1Factory::CreateObject(TypeOfExtendedMediaQualityType))
class Mp7JrsExtendedMediaQualityType;
class IMp7JrsExtendedMediaQualityType;
/** Smart pointer for instance of IMp7JrsExtendedMediaQualityType */
typedef Dc1Ptr< IMp7JrsExtendedMediaQualityType > Mp7JrsExtendedMediaQualityPtr;

#define TypeOfExtendedMediaQualityType_QCItemResult_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedMediaQualityType_QCItemResult_CollectionType")))
#define CreateExtendedMediaQualityType_QCItemResult_CollectionType (Dc1Factory::CreateObject(TypeOfExtendedMediaQualityType_QCItemResult_CollectionType))
class Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionType;
class IMp7JrsExtendedMediaQualityType_QCItemResult_CollectionType;
/** Smart pointer for instance of IMp7JrsExtendedMediaQualityType_QCItemResult_CollectionType */
typedef Dc1Ptr< IMp7JrsExtendedMediaQualityType_QCItemResult_CollectionType > Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr;

#define TypeOfExtentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType")))
#define CreateExtentType (Dc1Factory::CreateObject(TypeOfExtentType))
class Mp7JrsExtentType;
class IMp7JrsExtentType;
/** Smart pointer for instance of IMp7JrsExtentType */
typedef Dc1Ptr< IMp7JrsExtentType > Mp7JrsExtentPtr;

#define TypeOfFaceRecognitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FaceRecognitionType")))
#define CreateFaceRecognitionType (Dc1Factory::CreateObject(TypeOfFaceRecognitionType))
class Mp7JrsFaceRecognitionType;
class IMp7JrsFaceRecognitionType;
/** Smart pointer for instance of IMp7JrsFaceRecognitionType */
typedef Dc1Ptr< IMp7JrsFaceRecognitionType > Mp7JrsFaceRecognitionPtr;

#define TypeOfFaceRecognitionType_Feature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FaceRecognitionType_Feature_CollectionType")))
#define CreateFaceRecognitionType_Feature_CollectionType (Dc1Factory::CreateObject(TypeOfFaceRecognitionType_Feature_CollectionType))
class Mp7JrsFaceRecognitionType_Feature_CollectionType;
class IMp7JrsFaceRecognitionType_Feature_CollectionType;
/** Smart pointer for instance of IMp7JrsFaceRecognitionType_Feature_CollectionType */
typedef Dc1Ptr< IMp7JrsFaceRecognitionType_Feature_CollectionType > Mp7JrsFaceRecognitionType_Feature_CollectionPtr;

#define TypeOfFaceRecognitionType_Feature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FaceRecognitionType_Feature_LocalType")))
#define CreateFaceRecognitionType_Feature_LocalType (Dc1Factory::CreateObject(TypeOfFaceRecognitionType_Feature_LocalType))
class Mp7JrsFaceRecognitionType_Feature_LocalType;
class IMp7JrsFaceRecognitionType_Feature_LocalType;
/** Smart pointer for instance of IMp7JrsFaceRecognitionType_Feature_LocalType */
typedef Dc1Ptr< IMp7JrsFaceRecognitionType_Feature_LocalType > Mp7JrsFaceRecognitionType_Feature_LocalPtr;

#define TypeOfFigureTrajectoryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FigureTrajectoryType")))
#define CreateFigureTrajectoryType (Dc1Factory::CreateObject(TypeOfFigureTrajectoryType))
class Mp7JrsFigureTrajectoryType;
class IMp7JrsFigureTrajectoryType;
/** Smart pointer for instance of IMp7JrsFigureTrajectoryType */
typedef Dc1Ptr< IMp7JrsFigureTrajectoryType > Mp7JrsFigureTrajectoryPtr;

#define TypeOfFigureTrajectoryType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FigureTrajectoryType_type_LocalType")))
#define CreateFigureTrajectoryType_type_LocalType (Dc1Factory::CreateObject(TypeOfFigureTrajectoryType_type_LocalType))
class Mp7JrsFigureTrajectoryType_type_LocalType;
class IMp7JrsFigureTrajectoryType_type_LocalType;
// No smart pointer instance for IMp7JrsFigureTrajectoryType_type_LocalType

#define TypeOfFigureTrajectoryType_Vertex_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FigureTrajectoryType_Vertex_CollectionType")))
#define CreateFigureTrajectoryType_Vertex_CollectionType (Dc1Factory::CreateObject(TypeOfFigureTrajectoryType_Vertex_CollectionType))
class Mp7JrsFigureTrajectoryType_Vertex_CollectionType;
class IMp7JrsFigureTrajectoryType_Vertex_CollectionType;
/** Smart pointer for instance of IMp7JrsFigureTrajectoryType_Vertex_CollectionType */
typedef Dc1Ptr< IMp7JrsFigureTrajectoryType_Vertex_CollectionType > Mp7JrsFigureTrajectoryType_Vertex_CollectionPtr;

#define TypeOfFilter1DType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Filter1DType")))
#define CreateFilter1DType (Dc1Factory::CreateObject(TypeOfFilter1DType))
class Mp7JrsFilter1DType;
class IMp7JrsFilter1DType;
/** Smart pointer for instance of IMp7JrsFilter1DType */
typedef Dc1Ptr< IMp7JrsFilter1DType > Mp7JrsFilter1DPtr;

#define TypeOfFilter2DType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Filter2DType")))
#define CreateFilter2DType (Dc1Factory::CreateObject(TypeOfFilter2DType))
class Mp7JrsFilter2DType;
class IMp7JrsFilter2DType;
/** Smart pointer for instance of IMp7JrsFilter2DType */
typedef Dc1Ptr< IMp7JrsFilter2DType > Mp7JrsFilter2DPtr;

#define TypeOfFilteringAndSearchPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType")))
#define CreateFilteringAndSearchPreferencesType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType))
class Mp7JrsFilteringAndSearchPreferencesType;
class IMp7JrsFilteringAndSearchPreferencesType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType > Mp7JrsFilteringAndSearchPreferencesPtr;

#define TypeOfFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType")))
#define CreateFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType))
class Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType;
class IMp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType > Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr;

#define TypeOfFilteringAndSearchPreferencesType_CreationPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType_CreationPreferences_CollectionType")))
#define CreateFilteringAndSearchPreferencesType_CreationPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType_CreationPreferences_CollectionType))
class Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionType;
class IMp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionType > Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr;

#define TypeOfFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType")))
#define CreateFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType))
class Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType;
class IMp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType > Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr;

#define TypeOfFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType_PreferenceCondition_CollectionType")))
#define CreateFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType))
class Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType;
class IMp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType > Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr;

#define TypeOfFilteringAndSearchPreferencesType_SourcePreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType_SourcePreferences_CollectionType")))
#define CreateFilteringAndSearchPreferencesType_SourcePreferences_CollectionType (Dc1Factory::CreateObject(TypeOfFilteringAndSearchPreferencesType_SourcePreferences_CollectionType))
class Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionType;
class IMp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionType > Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr;

#define TypeOfFilteringType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringType")))
#define CreateFilteringType (Dc1Factory::CreateObject(TypeOfFilteringType))
class Mp7JrsFilteringType;
class IMp7JrsFilteringType;
/** Smart pointer for instance of IMp7JrsFilteringType */
typedef Dc1Ptr< IMp7JrsFilteringType > Mp7JrsFilteringPtr;

#define TypeOfFilterSeparableType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilterSeparableType")))
#define CreateFilterSeparableType (Dc1Factory::CreateObject(TypeOfFilterSeparableType))
class Mp7JrsFilterSeparableType;
class IMp7JrsFilterSeparableType;
/** Smart pointer for instance of IMp7JrsFilterSeparableType */
typedef Dc1Ptr< IMp7JrsFilterSeparableType > Mp7JrsFilterSeparablePtr;

#define TypeOfFilterSeparableType_Filter1D_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilterSeparableType_Filter1D_CollectionType")))
#define CreateFilterSeparableType_Filter1D_CollectionType (Dc1Factory::CreateObject(TypeOfFilterSeparableType_Filter1D_CollectionType))
class Mp7JrsFilterSeparableType_Filter1D_CollectionType;
class IMp7JrsFilterSeparableType_Filter1D_CollectionType;
/** Smart pointer for instance of IMp7JrsFilterSeparableType_Filter1D_CollectionType */
typedef Dc1Ptr< IMp7JrsFilterSeparableType_Filter1D_CollectionType > Mp7JrsFilterSeparableType_Filter1D_CollectionPtr;

#define TypeOfFilterType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilterType")))
#define CreateFilterType (Dc1Factory::CreateObject(TypeOfFilterType))
class Mp7JrsFilterType;
class IMp7JrsFilterType;
/** Smart pointer for instance of IMp7JrsFilterType */
typedef Dc1Ptr< IMp7JrsFilterType > Mp7JrsFilterPtr;

#define TypeOfFinancialType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType")))
#define CreateFinancialType (Dc1Factory::CreateObject(TypeOfFinancialType))
class Mp7JrsFinancialType;
class IMp7JrsFinancialType;
/** Smart pointer for instance of IMp7JrsFinancialType */
typedef Dc1Ptr< IMp7JrsFinancialType > Mp7JrsFinancialPtr;

#define TypeOfFinancialType_AccountItem_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType_AccountItem_CollectionType")))
#define CreateFinancialType_AccountItem_CollectionType (Dc1Factory::CreateObject(TypeOfFinancialType_AccountItem_CollectionType))
class Mp7JrsFinancialType_AccountItem_CollectionType;
class IMp7JrsFinancialType_AccountItem_CollectionType;
/** Smart pointer for instance of IMp7JrsFinancialType_AccountItem_CollectionType */
typedef Dc1Ptr< IMp7JrsFinancialType_AccountItem_CollectionType > Mp7JrsFinancialType_AccountItem_CollectionPtr;

#define TypeOfFinancialType_AccountItem_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType_AccountItem_LocalType")))
#define CreateFinancialType_AccountItem_LocalType (Dc1Factory::CreateObject(TypeOfFinancialType_AccountItem_LocalType))
class Mp7JrsFinancialType_AccountItem_LocalType;
class IMp7JrsFinancialType_AccountItem_LocalType;
/** Smart pointer for instance of IMp7JrsFinancialType_AccountItem_LocalType */
typedef Dc1Ptr< IMp7JrsFinancialType_AccountItem_LocalType > Mp7JrsFinancialType_AccountItem_LocalPtr;

#define TypeOfFiniteStateModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FiniteStateModelType")))
#define CreateFiniteStateModelType (Dc1Factory::CreateObject(TypeOfFiniteStateModelType))
class Mp7JrsFiniteStateModelType;
class IMp7JrsFiniteStateModelType;
/** Smart pointer for instance of IMp7JrsFiniteStateModelType */
typedef Dc1Ptr< IMp7JrsFiniteStateModelType > Mp7JrsFiniteStateModelPtr;

#define TypeOfFloatDiagonalMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatDiagonalMatrixType")))
#define CreateFloatDiagonalMatrixType (Dc1Factory::CreateObject(TypeOfFloatDiagonalMatrixType))
class Mp7JrsFloatDiagonalMatrixType;
class IMp7JrsFloatDiagonalMatrixType;
/** Smart pointer for instance of IMp7JrsFloatDiagonalMatrixType */
typedef Dc1Ptr< IMp7JrsFloatDiagonalMatrixType > Mp7JrsFloatDiagonalMatrixPtr;

#define TypeOfFloatMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))
#define CreateFloatMatrixType (Dc1Factory::CreateObject(TypeOfFloatMatrixType))
class Mp7JrsFloatMatrixType;
class IMp7JrsFloatMatrixType;
/** Smart pointer for instance of IMp7JrsFloatMatrixType */
typedef Dc1Ptr< IMp7JrsFloatMatrixType > Mp7JrsFloatMatrixPtr;

#define TypeOffloatVector (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))
#define CreatefloatVector (Dc1Factory::CreateObject(TypeOffloatVector))
class Mp7JrsfloatVector;
class IMp7JrsfloatVector;
/** Smart pointer for instance of IMp7JrsfloatVector */
typedef Dc1Ptr< IMp7JrsfloatVector > Mp7JrsfloatVectorPtr;

#define TypeOfFocusOfExpansionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FocusOfExpansionType")))
#define CreateFocusOfExpansionType (Dc1Factory::CreateObject(TypeOfFocusOfExpansionType))
class Mp7JrsFocusOfExpansionType;
class IMp7JrsFocusOfExpansionType;
/** Smart pointer for instance of IMp7JrsFocusOfExpansionType */
typedef Dc1Ptr< IMp7JrsFocusOfExpansionType > Mp7JrsFocusOfExpansionPtr;

#define TypeOfFractionalPresenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FractionalPresenceType")))
#define CreateFractionalPresenceType (Dc1Factory::CreateObject(TypeOfFractionalPresenceType))
class Mp7JrsFractionalPresenceType;
class IMp7JrsFractionalPresenceType;
/** Smart pointer for instance of IMp7JrsFractionalPresenceType */
typedef Dc1Ptr< IMp7JrsFractionalPresenceType > Mp7JrsFractionalPresencePtr;

#define TypeOfFragmentReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentReferenceType")))
#define CreateFragmentReferenceType (Dc1Factory::CreateObject(TypeOfFragmentReferenceType))
class Mp7JrsFragmentReferenceType;
class IMp7JrsFragmentReferenceType;
/** Smart pointer for instance of IMp7JrsFragmentReferenceType */
typedef Dc1Ptr< IMp7JrsFragmentReferenceType > Mp7JrsFragmentReferencePtr;

#define TypeOfFragmentUpdateCommandType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType")))
#define CreateFragmentUpdateCommandType (Dc1Factory::CreateObject(TypeOfFragmentUpdateCommandType))
class Mp7JrsFragmentUpdateCommandType;
class IMp7JrsFragmentUpdateCommandType;
/** Smart pointer for instance of IMp7JrsFragmentUpdateCommandType */
typedef Dc1Ptr< IMp7JrsFragmentUpdateCommandType > Mp7JrsFragmentUpdateCommandPtr;

#define TypeOfFragmentUpdateCommandType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType_LocalType")))
#define CreateFragmentUpdateCommandType_LocalType (Dc1Factory::CreateObject(TypeOfFragmentUpdateCommandType_LocalType))
class Mp7JrsFragmentUpdateCommandType_LocalType;
class IMp7JrsFragmentUpdateCommandType_LocalType;
// No smart pointer instance for IMp7JrsFragmentUpdateCommandType_LocalType

#define TypeOfFragmentUpdateCommandType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType_LocalType0")))
#define CreateFragmentUpdateCommandType_LocalType0 (Dc1Factory::CreateObject(TypeOfFragmentUpdateCommandType_LocalType0))
class Mp7JrsFragmentUpdateCommandType_LocalType0;
class IMp7JrsFragmentUpdateCommandType_LocalType0;
/** Smart pointer for instance of IMp7JrsFragmentUpdateCommandType_LocalType0 */
typedef Dc1Ptr< IMp7JrsFragmentUpdateCommandType_LocalType0 > Mp7JrsFragmentUpdateCommandType_Local0Ptr;

#define TypeOfFragmentUpdateContextType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateContextType")))
#define CreateFragmentUpdateContextType (Dc1Factory::CreateObject(TypeOfFragmentUpdateContextType))
class Mp7JrsFragmentUpdateContextType;
class IMp7JrsFragmentUpdateContextType;
/** Smart pointer for instance of IMp7JrsFragmentUpdateContextType */
typedef Dc1Ptr< IMp7JrsFragmentUpdateContextType > Mp7JrsFragmentUpdateContextPtr;

#define TypeOfFragmentUpdateContextType_position_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateContextType_position_LocalType")))
#define CreateFragmentUpdateContextType_position_LocalType (Dc1Factory::CreateObject(TypeOfFragmentUpdateContextType_position_LocalType))
class Mp7JrsFragmentUpdateContextType_position_LocalType;
class IMp7JrsFragmentUpdateContextType_position_LocalType;
// No smart pointer instance for IMp7JrsFragmentUpdateContextType_position_LocalType

#define TypeOfFragmentUpdateContextTypeBase (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateContextTypeBase")))
#define CreateFragmentUpdateContextTypeBase (Dc1Factory::CreateObject(TypeOfFragmentUpdateContextTypeBase))
class Mp7JrsFragmentUpdateContextTypeBase;
class IMp7JrsFragmentUpdateContextTypeBase;
/** Smart pointer for instance of IMp7JrsFragmentUpdateContextTypeBase */
typedef Dc1Ptr< IMp7JrsFragmentUpdateContextTypeBase > Mp7JrsFragmentUpdateContextTypeBasePtr;

#define TypeOfFragmentUpdatePayloadType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdatePayloadType")))
#define CreateFragmentUpdatePayloadType (Dc1Factory::CreateObject(TypeOfFragmentUpdatePayloadType))
class Mp7JrsFragmentUpdatePayloadType;
class IMp7JrsFragmentUpdatePayloadType;
/** Smart pointer for instance of IMp7JrsFragmentUpdatePayloadType */
typedef Dc1Ptr< IMp7JrsFragmentUpdatePayloadType > Mp7JrsFragmentUpdatePayloadPtr;

#define TypeOfFragmentUpdateUnitType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateUnitType")))
#define CreateFragmentUpdateUnitType (Dc1Factory::CreateObject(TypeOfFragmentUpdateUnitType))
class Mp7JrsFragmentUpdateUnitType;
class IMp7JrsFragmentUpdateUnitType;
/** Smart pointer for instance of IMp7JrsFragmentUpdateUnitType */
typedef Dc1Ptr< IMp7JrsFragmentUpdateUnitType > Mp7JrsFragmentUpdateUnitPtr;

#define TypeOfFrequencyTreeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyTreeType")))
#define CreateFrequencyTreeType (Dc1Factory::CreateObject(TypeOfFrequencyTreeType))
class Mp7JrsFrequencyTreeType;
class IMp7JrsFrequencyTreeType;
/** Smart pointer for instance of IMp7JrsFrequencyTreeType */
typedef Dc1Ptr< IMp7JrsFrequencyTreeType > Mp7JrsFrequencyTreePtr;

#define TypeOfFrequencyTreeType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyTreeType_CollectionType")))
#define CreateFrequencyTreeType_CollectionType (Dc1Factory::CreateObject(TypeOfFrequencyTreeType_CollectionType))
class Mp7JrsFrequencyTreeType_CollectionType;
class IMp7JrsFrequencyTreeType_CollectionType;
/** Smart pointer for instance of IMp7JrsFrequencyTreeType_CollectionType */
typedef Dc1Ptr< IMp7JrsFrequencyTreeType_CollectionType > Mp7JrsFrequencyTreeType_CollectionPtr;

#define TypeOfFrequencyTreeType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyTreeType_LocalType")))
#define CreateFrequencyTreeType_LocalType (Dc1Factory::CreateObject(TypeOfFrequencyTreeType_LocalType))
class Mp7JrsFrequencyTreeType_LocalType;
class IMp7JrsFrequencyTreeType_LocalType;
/** Smart pointer for instance of IMp7JrsFrequencyTreeType_LocalType */
typedef Dc1Ptr< IMp7JrsFrequencyTreeType_LocalType > Mp7JrsFrequencyTreeType_LocalPtr;

#define TypeOfFrequencyViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyViewType")))
#define CreateFrequencyViewType (Dc1Factory::CreateObject(TypeOfFrequencyViewType))
class Mp7JrsFrequencyViewType;
class IMp7JrsFrequencyViewType;
/** Smart pointer for instance of IMp7JrsFrequencyViewType */
typedef Dc1Ptr< IMp7JrsFrequencyViewType > Mp7JrsFrequencyViewPtr;

#define TypeOfGammaDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GammaDistributionType")))
#define CreateGammaDistributionType (Dc1Factory::CreateObject(TypeOfGammaDistributionType))
class Mp7JrsGammaDistributionType;
class IMp7JrsGammaDistributionType;
/** Smart pointer for instance of IMp7JrsGammaDistributionType */
typedef Dc1Ptr< IMp7JrsGammaDistributionType > Mp7JrsGammaDistributionPtr;

#define TypeOfGaussianDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GaussianDistributionType")))
#define CreateGaussianDistributionType (Dc1Factory::CreateObject(TypeOfGaussianDistributionType))
class Mp7JrsGaussianDistributionType;
class IMp7JrsGaussianDistributionType;
/** Smart pointer for instance of IMp7JrsGaussianDistributionType */
typedef Dc1Ptr< IMp7JrsGaussianDistributionType > Mp7JrsGaussianDistributionPtr;

#define TypeOfGaussianMixtureModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GaussianMixtureModelType")))
#define CreateGaussianMixtureModelType (Dc1Factory::CreateObject(TypeOfGaussianMixtureModelType))
class Mp7JrsGaussianMixtureModelType;
class IMp7JrsGaussianMixtureModelType;
/** Smart pointer for instance of IMp7JrsGaussianMixtureModelType */
typedef Dc1Ptr< IMp7JrsGaussianMixtureModelType > Mp7JrsGaussianMixtureModelPtr;

#define TypeOfGaussianMixtureModelType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GaussianMixtureModelType_CollectionType")))
#define CreateGaussianMixtureModelType_CollectionType (Dc1Factory::CreateObject(TypeOfGaussianMixtureModelType_CollectionType))
class Mp7JrsGaussianMixtureModelType_CollectionType;
class IMp7JrsGaussianMixtureModelType_CollectionType;
/** Smart pointer for instance of IMp7JrsGaussianMixtureModelType_CollectionType */
typedef Dc1Ptr< IMp7JrsGaussianMixtureModelType_CollectionType > Mp7JrsGaussianMixtureModelType_CollectionPtr;

#define TypeOfGaussianMixtureModelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GaussianMixtureModelType_LocalType")))
#define CreateGaussianMixtureModelType_LocalType (Dc1Factory::CreateObject(TypeOfGaussianMixtureModelType_LocalType))
class Mp7JrsGaussianMixtureModelType_LocalType;
class IMp7JrsGaussianMixtureModelType_LocalType;
/** Smart pointer for instance of IMp7JrsGaussianMixtureModelType_LocalType */
typedef Dc1Ptr< IMp7JrsGaussianMixtureModelType_LocalType > Mp7JrsGaussianMixtureModelType_LocalPtr;

#define TypeOfGeneralizedGaussianDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeneralizedGaussianDistributionType")))
#define CreateGeneralizedGaussianDistributionType (Dc1Factory::CreateObject(TypeOfGeneralizedGaussianDistributionType))
class Mp7JrsGeneralizedGaussianDistributionType;
class IMp7JrsGeneralizedGaussianDistributionType;
/** Smart pointer for instance of IMp7JrsGeneralizedGaussianDistributionType */
typedef Dc1Ptr< IMp7JrsGeneralizedGaussianDistributionType > Mp7JrsGeneralizedGaussianDistributionPtr;

#define TypeOfGeographicPointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeographicPointType")))
#define CreateGeographicPointType (Dc1Factory::CreateObject(TypeOfGeographicPointType))
class Mp7JrsGeographicPointType;
class IMp7JrsGeographicPointType;
/** Smart pointer for instance of IMp7JrsGeographicPointType */
typedef Dc1Ptr< IMp7JrsGeographicPointType > Mp7JrsGeographicPointPtr;

#define TypeOfGeographicPointType_latitude_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeographicPointType_latitude_LocalType")))
#define CreateGeographicPointType_latitude_LocalType (Dc1Factory::CreateObject(TypeOfGeographicPointType_latitude_LocalType))
class Mp7JrsGeographicPointType_latitude_LocalType;
class IMp7JrsGeographicPointType_latitude_LocalType;
/** Smart pointer for instance of IMp7JrsGeographicPointType_latitude_LocalType */
typedef Dc1Ptr< IMp7JrsGeographicPointType_latitude_LocalType > Mp7JrsGeographicPointType_latitude_LocalPtr;

#define TypeOfGeographicPointType_longitude_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeographicPointType_longitude_LocalType")))
#define CreateGeographicPointType_longitude_LocalType (Dc1Factory::CreateObject(TypeOfGeographicPointType_longitude_LocalType))
class Mp7JrsGeographicPointType_longitude_LocalType;
class IMp7JrsGeographicPointType_longitude_LocalType;
/** Smart pointer for instance of IMp7JrsGeographicPointType_longitude_LocalType */
typedef Dc1Ptr< IMp7JrsGeographicPointType_longitude_LocalType > Mp7JrsGeographicPointType_longitude_LocalPtr;

#define TypeOfGeometricDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeometricDistributionType")))
#define CreateGeometricDistributionType (Dc1Factory::CreateObject(TypeOfGeometricDistributionType))
class Mp7JrsGeometricDistributionType;
class IMp7JrsGeometricDistributionType;
/** Smart pointer for instance of IMp7JrsGeometricDistributionType */
typedef Dc1Ptr< IMp7JrsGeometricDistributionType > Mp7JrsGeometricDistributionPtr;

#define TypeOfGlobalTransitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GlobalTransitionType")))
#define CreateGlobalTransitionType (Dc1Factory::CreateObject(TypeOfGlobalTransitionType))
class Mp7JrsGlobalTransitionType;
class IMp7JrsGlobalTransitionType;
/** Smart pointer for instance of IMp7JrsGlobalTransitionType */
typedef Dc1Ptr< IMp7JrsGlobalTransitionType > Mp7JrsGlobalTransitionPtr;

#define TypeOfGoFGoPColorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GoFGoPColorType")))
#define CreateGoFGoPColorType (Dc1Factory::CreateObject(TypeOfGoFGoPColorType))
class Mp7JrsGoFGoPColorType;
class IMp7JrsGoFGoPColorType;
/** Smart pointer for instance of IMp7JrsGoFGoPColorType */
typedef Dc1Ptr< IMp7JrsGoFGoPColorType > Mp7JrsGoFGoPColorPtr;

#define TypeOfGoFGoPColorType_aggregation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GoFGoPColorType_aggregation_LocalType")))
#define CreateGoFGoPColorType_aggregation_LocalType (Dc1Factory::CreateObject(TypeOfGoFGoPColorType_aggregation_LocalType))
class Mp7JrsGoFGoPColorType_aggregation_LocalType;
class IMp7JrsGoFGoPColorType_aggregation_LocalType;
// No smart pointer instance for IMp7JrsGoFGoPColorType_aggregation_LocalType

#define TypeOfGofGopFeatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType")))
#define CreateGofGopFeatureType (Dc1Factory::CreateObject(TypeOfGofGopFeatureType))
class Mp7JrsGofGopFeatureType;
class IMp7JrsGofGopFeatureType;
/** Smart pointer for instance of IMp7JrsGofGopFeatureType */
typedef Dc1Ptr< IMp7JrsGofGopFeatureType > Mp7JrsGofGopFeaturePtr;

#define TypeOfGofGopFeatureType_aggregation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType_aggregation_LocalType")))
#define CreateGofGopFeatureType_aggregation_LocalType (Dc1Factory::CreateObject(TypeOfGofGopFeatureType_aggregation_LocalType))
class Mp7JrsGofGopFeatureType_aggregation_LocalType;
class IMp7JrsGofGopFeatureType_aggregation_LocalType;
// No smart pointer instance for IMp7JrsGofGopFeatureType_aggregation_LocalType

#define TypeOfGraphicalClassificationSchemeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphicalClassificationSchemeType")))
#define CreateGraphicalClassificationSchemeType (Dc1Factory::CreateObject(TypeOfGraphicalClassificationSchemeType))
class Mp7JrsGraphicalClassificationSchemeType;
class IMp7JrsGraphicalClassificationSchemeType;
/** Smart pointer for instance of IMp7JrsGraphicalClassificationSchemeType */
typedef Dc1Ptr< IMp7JrsGraphicalClassificationSchemeType > Mp7JrsGraphicalClassificationSchemePtr;

#define TypeOfGraphicalClassificationSchemeType_GraphicalTerm_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphicalClassificationSchemeType_GraphicalTerm_CollectionType")))
#define CreateGraphicalClassificationSchemeType_GraphicalTerm_CollectionType (Dc1Factory::CreateObject(TypeOfGraphicalClassificationSchemeType_GraphicalTerm_CollectionType))
class Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionType;
class IMp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionType;
/** Smart pointer for instance of IMp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionType */
typedef Dc1Ptr< IMp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionType > Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr;

#define TypeOfGraphicalRuleDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphicalRuleDefinitionType")))
#define CreateGraphicalRuleDefinitionType (Dc1Factory::CreateObject(TypeOfGraphicalRuleDefinitionType))
class Mp7JrsGraphicalRuleDefinitionType;
class IMp7JrsGraphicalRuleDefinitionType;
/** Smart pointer for instance of IMp7JrsGraphicalRuleDefinitionType */
typedef Dc1Ptr< IMp7JrsGraphicalRuleDefinitionType > Mp7JrsGraphicalRuleDefinitionPtr;

#define TypeOfGraphicalTermDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphicalTermDefinitionType")))
#define CreateGraphicalTermDefinitionType (Dc1Factory::CreateObject(TypeOfGraphicalTermDefinitionType))
class Mp7JrsGraphicalTermDefinitionType;
class IMp7JrsGraphicalTermDefinitionType;
/** Smart pointer for instance of IMp7JrsGraphicalTermDefinitionType */
typedef Dc1Ptr< IMp7JrsGraphicalTermDefinitionType > Mp7JrsGraphicalTermDefinitionPtr;

#define TypeOfGraphType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))
#define CreateGraphType (Dc1Factory::CreateObject(TypeOfGraphType))
class Mp7JrsGraphType;
class IMp7JrsGraphType;
/** Smart pointer for instance of IMp7JrsGraphType */
typedef Dc1Ptr< IMp7JrsGraphType > Mp7JrsGraphPtr;

#define TypeOfGraphType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType_CollectionType")))
#define CreateGraphType_CollectionType (Dc1Factory::CreateObject(TypeOfGraphType_CollectionType))
class Mp7JrsGraphType_CollectionType;
class IMp7JrsGraphType_CollectionType;
/** Smart pointer for instance of IMp7JrsGraphType_CollectionType */
typedef Dc1Ptr< IMp7JrsGraphType_CollectionType > Mp7JrsGraphType_CollectionPtr;

#define TypeOfGraphType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType_LocalType")))
#define CreateGraphType_LocalType (Dc1Factory::CreateObject(TypeOfGraphType_LocalType))
class Mp7JrsGraphType_LocalType;
class IMp7JrsGraphType_LocalType;
/** Smart pointer for instance of IMp7JrsGraphType_LocalType */
typedef Dc1Ptr< IMp7JrsGraphType_LocalType > Mp7JrsGraphType_LocalPtr;

#define TypeOfGraphType_Node_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType_Node_LocalType")))
#define CreateGraphType_Node_LocalType (Dc1Factory::CreateObject(TypeOfGraphType_Node_LocalType))
class Mp7JrsGraphType_Node_LocalType;
class IMp7JrsGraphType_Node_LocalType;
/** Smart pointer for instance of IMp7JrsGraphType_Node_LocalType */
typedef Dc1Ptr< IMp7JrsGraphType_Node_LocalType > Mp7JrsGraphType_Node_LocalPtr;

#define TypeOfGridLayoutType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GridLayoutType")))
#define CreateGridLayoutType (Dc1Factory::CreateObject(TypeOfGridLayoutType))
class Mp7JrsGridLayoutType;
class IMp7JrsGridLayoutType;
/** Smart pointer for instance of IMp7JrsGridLayoutType */
typedef Dc1Ptr< IMp7JrsGridLayoutType > Mp7JrsGridLayoutPtr;

#define TypeOfGridLayoutType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GridLayoutType_Descriptor_CollectionType")))
#define CreateGridLayoutType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfGridLayoutType_Descriptor_CollectionType))
class Mp7JrsGridLayoutType_Descriptor_CollectionType;
class IMp7JrsGridLayoutType_Descriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsGridLayoutType_Descriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsGridLayoutType_Descriptor_CollectionType > Mp7JrsGridLayoutType_Descriptor_CollectionPtr;

#define TypeOfGridLayoutType_descriptorMask_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GridLayoutType_descriptorMask_LocalType")))
#define CreateGridLayoutType_descriptorMask_LocalType (Dc1Factory::CreateObject(TypeOfGridLayoutType_descriptorMask_LocalType))
class Mp7JrsGridLayoutType_descriptorMask_LocalType;
class IMp7JrsGridLayoutType_descriptorMask_LocalType;
/** Smart pointer for instance of IMp7JrsGridLayoutType_descriptorMask_LocalType */
typedef Dc1Ptr< IMp7JrsGridLayoutType_descriptorMask_LocalType > Mp7JrsGridLayoutType_descriptorMask_LocalPtr;

#define TypeOfHandWritingRecogInformationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType")))
#define CreateHandWritingRecogInformationType (Dc1Factory::CreateObject(TypeOfHandWritingRecogInformationType))
class Mp7JrsHandWritingRecogInformationType;
class IMp7JrsHandWritingRecogInformationType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogInformationType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogInformationType > Mp7JrsHandWritingRecogInformationPtr;

#define TypeOfHandWritingRecogInformationType_InkLexicon_Entry_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType_InkLexicon_Entry_CollectionType")))
#define CreateHandWritingRecogInformationType_InkLexicon_Entry_CollectionType (Dc1Factory::CreateObject(TypeOfHandWritingRecogInformationType_InkLexicon_Entry_CollectionType))
class Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionType;
class IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionType > Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr;

#define TypeOfHandWritingRecogInformationType_InkLexicon_Entry_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType_InkLexicon_Entry_LocalType")))
#define CreateHandWritingRecogInformationType_InkLexicon_Entry_LocalType (Dc1Factory::CreateObject(TypeOfHandWritingRecogInformationType_InkLexicon_Entry_LocalType))
class Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType;
class IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType > Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalPtr;

#define TypeOfHandWritingRecogInformationType_InkLexicon_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType_InkLexicon_LocalType")))
#define CreateHandWritingRecogInformationType_InkLexicon_LocalType (Dc1Factory::CreateObject(TypeOfHandWritingRecogInformationType_InkLexicon_LocalType))
class Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType;
class IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType > Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalPtr;

#define TypeOfHandWritingRecogInformationType_Recognizer_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType_Recognizer_LocalType")))
#define CreateHandWritingRecogInformationType_Recognizer_LocalType (Dc1Factory::CreateObject(TypeOfHandWritingRecogInformationType_Recognizer_LocalType))
class Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType;
class IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType > Mp7JrsHandWritingRecogInformationType_Recognizer_LocalPtr;

#define TypeOfHandWritingRecogResultType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogResultType")))
#define CreateHandWritingRecogResultType (Dc1Factory::CreateObject(TypeOfHandWritingRecogResultType))
class Mp7JrsHandWritingRecogResultType;
class IMp7JrsHandWritingRecogResultType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogResultType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogResultType > Mp7JrsHandWritingRecogResultPtr;

#define TypeOfHandWritingRecogResultType_Result_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogResultType_Result_CollectionType")))
#define CreateHandWritingRecogResultType_Result_CollectionType (Dc1Factory::CreateObject(TypeOfHandWritingRecogResultType_Result_CollectionType))
class Mp7JrsHandWritingRecogResultType_Result_CollectionType;
class IMp7JrsHandWritingRecogResultType_Result_CollectionType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogResultType_Result_CollectionType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogResultType_Result_CollectionType > Mp7JrsHandWritingRecogResultType_Result_CollectionPtr;

#define TypeOfHandWritingRecogResultType_Result_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogResultType_Result_LocalType")))
#define CreateHandWritingRecogResultType_Result_LocalType (Dc1Factory::CreateObject(TypeOfHandWritingRecogResultType_Result_LocalType))
class Mp7JrsHandWritingRecogResultType_Result_LocalType;
class IMp7JrsHandWritingRecogResultType_Result_LocalType;
/** Smart pointer for instance of IMp7JrsHandWritingRecogResultType_Result_LocalType */
typedef Dc1Ptr< IMp7JrsHandWritingRecogResultType_Result_LocalType > Mp7JrsHandWritingRecogResultType_Result_LocalPtr;

#define TypeOfHarmonicInstrumentTimbreType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicInstrumentTimbreType")))
#define CreateHarmonicInstrumentTimbreType (Dc1Factory::CreateObject(TypeOfHarmonicInstrumentTimbreType))
class Mp7JrsHarmonicInstrumentTimbreType;
class IMp7JrsHarmonicInstrumentTimbreType;
/** Smart pointer for instance of IMp7JrsHarmonicInstrumentTimbreType */
typedef Dc1Ptr< IMp7JrsHarmonicInstrumentTimbreType > Mp7JrsHarmonicInstrumentTimbrePtr;

#define TypeOfHarmonicSpectralCentroidType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralCentroidType")))
#define CreateHarmonicSpectralCentroidType (Dc1Factory::CreateObject(TypeOfHarmonicSpectralCentroidType))
class Mp7JrsHarmonicSpectralCentroidType;
class IMp7JrsHarmonicSpectralCentroidType;
/** Smart pointer for instance of IMp7JrsHarmonicSpectralCentroidType */
typedef Dc1Ptr< IMp7JrsHarmonicSpectralCentroidType > Mp7JrsHarmonicSpectralCentroidPtr;

#define TypeOfHarmonicSpectralDeviationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralDeviationType")))
#define CreateHarmonicSpectralDeviationType (Dc1Factory::CreateObject(TypeOfHarmonicSpectralDeviationType))
class Mp7JrsHarmonicSpectralDeviationType;
class IMp7JrsHarmonicSpectralDeviationType;
/** Smart pointer for instance of IMp7JrsHarmonicSpectralDeviationType */
typedef Dc1Ptr< IMp7JrsHarmonicSpectralDeviationType > Mp7JrsHarmonicSpectralDeviationPtr;

#define TypeOfHarmonicSpectralSpreadType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralSpreadType")))
#define CreateHarmonicSpectralSpreadType (Dc1Factory::CreateObject(TypeOfHarmonicSpectralSpreadType))
class Mp7JrsHarmonicSpectralSpreadType;
class IMp7JrsHarmonicSpectralSpreadType;
/** Smart pointer for instance of IMp7JrsHarmonicSpectralSpreadType */
typedef Dc1Ptr< IMp7JrsHarmonicSpectralSpreadType > Mp7JrsHarmonicSpectralSpreadPtr;

#define TypeOfHarmonicSpectralVariationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralVariationType")))
#define CreateHarmonicSpectralVariationType (Dc1Factory::CreateObject(TypeOfHarmonicSpectralVariationType))
class Mp7JrsHarmonicSpectralVariationType;
class IMp7JrsHarmonicSpectralVariationType;
/** Smart pointer for instance of IMp7JrsHarmonicSpectralVariationType */
typedef Dc1Ptr< IMp7JrsHarmonicSpectralVariationType > Mp7JrsHarmonicSpectralVariationPtr;

#define TypeOfHeaderType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HeaderType")))
#define CreateHeaderType (Dc1Factory::CreateObject(TypeOfHeaderType))
class Mp7JrsHeaderType;
class IMp7JrsHeaderType;
/** Smart pointer for instance of IMp7JrsHeaderType */
typedef Dc1Ptr< IMp7JrsHeaderType > Mp7JrsHeaderPtr;

#define TypeOfHierarchicalSummaryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType")))
#define CreateHierarchicalSummaryType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType))
class Mp7JrsHierarchicalSummaryType;
class IMp7JrsHierarchicalSummaryType;
/** Smart pointer for instance of IMp7JrsHierarchicalSummaryType */
typedef Dc1Ptr< IMp7JrsHierarchicalSummaryType > Mp7JrsHierarchicalSummaryPtr;

#define TypeOfHierarchicalSummaryType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType_CollectionType")))
#define CreateHierarchicalSummaryType_CollectionType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType_CollectionType))
class Mp7JrsHierarchicalSummaryType_CollectionType;
class IMp7JrsHierarchicalSummaryType_CollectionType;
/** Smart pointer for instance of IMp7JrsHierarchicalSummaryType_CollectionType */
typedef Dc1Ptr< IMp7JrsHierarchicalSummaryType_CollectionType > Mp7JrsHierarchicalSummaryType_CollectionPtr;

#define TypeOfHierarchicalSummaryType_components_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType_components_CollectionType")))
#define CreateHierarchicalSummaryType_components_CollectionType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType_components_CollectionType))
class Mp7JrsHierarchicalSummaryType_components_CollectionType;
class IMp7JrsHierarchicalSummaryType_components_CollectionType;
/** Smart pointer for instance of IMp7JrsHierarchicalSummaryType_components_CollectionType */
typedef Dc1Ptr< IMp7JrsHierarchicalSummaryType_components_CollectionType > Mp7JrsHierarchicalSummaryType_components_CollectionPtr;

#define TypeOfHierarchicalSummaryType_components_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType_components_LocalType")))
#define CreateHierarchicalSummaryType_components_LocalType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType_components_LocalType))
class Mp7JrsHierarchicalSummaryType_components_LocalType;
class IMp7JrsHierarchicalSummaryType_components_LocalType;
// No smart pointer instance for IMp7JrsHierarchicalSummaryType_components_LocalType

#define TypeOfHierarchicalSummaryType_hierarchy_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType_hierarchy_LocalType")))
#define CreateHierarchicalSummaryType_hierarchy_LocalType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType_hierarchy_LocalType))
class Mp7JrsHierarchicalSummaryType_hierarchy_LocalType;
class IMp7JrsHierarchicalSummaryType_hierarchy_LocalType;
// No smart pointer instance for IMp7JrsHierarchicalSummaryType_hierarchy_LocalType

#define TypeOfHierarchicalSummaryType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HierarchicalSummaryType_LocalType")))
#define CreateHierarchicalSummaryType_LocalType (Dc1Factory::CreateObject(TypeOfHierarchicalSummaryType_LocalType))
class Mp7JrsHierarchicalSummaryType_LocalType;
class IMp7JrsHierarchicalSummaryType_LocalType;
/** Smart pointer for instance of IMp7JrsHierarchicalSummaryType_LocalType */
typedef Dc1Ptr< IMp7JrsHierarchicalSummaryType_LocalType > Mp7JrsHierarchicalSummaryType_LocalPtr;

#define TypeOfHistogramProbabilityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HistogramProbabilityType")))
#define CreateHistogramProbabilityType (Dc1Factory::CreateObject(TypeOfHistogramProbabilityType))
class Mp7JrsHistogramProbabilityType;
class IMp7JrsHistogramProbabilityType;
/** Smart pointer for instance of IMp7JrsHistogramProbabilityType */
typedef Dc1Ptr< IMp7JrsHistogramProbabilityType > Mp7JrsHistogramProbabilityPtr;

#define TypeOfHomogeneousTextureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HomogeneousTextureType")))
#define CreateHomogeneousTextureType (Dc1Factory::CreateObject(TypeOfHomogeneousTextureType))
class Mp7JrsHomogeneousTextureType;
class IMp7JrsHomogeneousTextureType;
/** Smart pointer for instance of IMp7JrsHomogeneousTextureType */
typedef Dc1Ptr< IMp7JrsHomogeneousTextureType > Mp7JrsHomogeneousTexturePtr;

#define TypeOfHyperGeometricDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HyperGeometricDistributionType")))
#define CreateHyperGeometricDistributionType (Dc1Factory::CreateObject(TypeOfHyperGeometricDistributionType))
class Mp7JrsHyperGeometricDistributionType;
class IMp7JrsHyperGeometricDistributionType;
/** Smart pointer for instance of IMp7JrsHyperGeometricDistributionType */
typedef Dc1Ptr< IMp7JrsHyperGeometricDistributionType > Mp7JrsHyperGeometricDistributionPtr;

#define TypeOfIlluminationInvariantColorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColorType")))
#define CreateIlluminationInvariantColorType (Dc1Factory::CreateObject(TypeOfIlluminationInvariantColorType))
class Mp7JrsIlluminationInvariantColorType;
class IMp7JrsIlluminationInvariantColorType;
/** Smart pointer for instance of IMp7JrsIlluminationInvariantColorType */
typedef Dc1Ptr< IMp7JrsIlluminationInvariantColorType > Mp7JrsIlluminationInvariantColorPtr;

#define TypeOfImageLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType")))
#define CreateImageLocatorType (Dc1Factory::CreateObject(TypeOfImageLocatorType))
class Mp7JrsImageLocatorType;
class IMp7JrsImageLocatorType;
/** Smart pointer for instance of IMp7JrsImageLocatorType */
typedef Dc1Ptr< IMp7JrsImageLocatorType > Mp7JrsImageLocatorPtr;

#define TypeOfImageLocatorType_BytePosition_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType_BytePosition_LocalType")))
#define CreateImageLocatorType_BytePosition_LocalType (Dc1Factory::CreateObject(TypeOfImageLocatorType_BytePosition_LocalType))
class Mp7JrsImageLocatorType_BytePosition_LocalType;
class IMp7JrsImageLocatorType_BytePosition_LocalType;
/** Smart pointer for instance of IMp7JrsImageLocatorType_BytePosition_LocalType */
typedef Dc1Ptr< IMp7JrsImageLocatorType_BytePosition_LocalType > Mp7JrsImageLocatorType_BytePosition_LocalPtr;

#define TypeOfImageSignatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType")))
#define CreateImageSignatureType (Dc1Factory::CreateObject(TypeOfImageSignatureType))
class Mp7JrsImageSignatureType;
class IMp7JrsImageSignatureType;
/** Smart pointer for instance of IMp7JrsImageSignatureType */
typedef Dc1Ptr< IMp7JrsImageSignatureType > Mp7JrsImageSignaturePtr;

#define TypeOfImageSignatureType_GlobalSignatureA_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureA_CollectionType")))
#define CreateImageSignatureType_GlobalSignatureA_CollectionType (Dc1Factory::CreateObject(TypeOfImageSignatureType_GlobalSignatureA_CollectionType))
class Mp7JrsImageSignatureType_GlobalSignatureA_CollectionType;
class IMp7JrsImageSignatureType_GlobalSignatureA_CollectionType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_GlobalSignatureA_CollectionType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_GlobalSignatureA_CollectionType > Mp7JrsImageSignatureType_GlobalSignatureA_CollectionPtr;

#define TypeOfImageSignatureType_GlobalSignatureA_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureA_LocalType")))
#define CreateImageSignatureType_GlobalSignatureA_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_GlobalSignatureA_LocalType))
class Mp7JrsImageSignatureType_GlobalSignatureA_LocalType;
class IMp7JrsImageSignatureType_GlobalSignatureA_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_GlobalSignatureA_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_GlobalSignatureA_LocalType > Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr;

#define TypeOfImageSignatureType_GlobalSignatureB_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureB_CollectionType")))
#define CreateImageSignatureType_GlobalSignatureB_CollectionType (Dc1Factory::CreateObject(TypeOfImageSignatureType_GlobalSignatureB_CollectionType))
class Mp7JrsImageSignatureType_GlobalSignatureB_CollectionType;
class IMp7JrsImageSignatureType_GlobalSignatureB_CollectionType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_GlobalSignatureB_CollectionType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_GlobalSignatureB_CollectionType > Mp7JrsImageSignatureType_GlobalSignatureB_CollectionPtr;

#define TypeOfImageSignatureType_GlobalSignatureB_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureB_LocalType")))
#define CreateImageSignatureType_GlobalSignatureB_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_GlobalSignatureB_LocalType))
class Mp7JrsImageSignatureType_GlobalSignatureB_LocalType;
class IMp7JrsImageSignatureType_GlobalSignatureB_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_GlobalSignatureB_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_GlobalSignatureB_LocalType > Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr;

#define TypeOfImageSignatureType_LocalSignature_FeaturePoint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_CollectionType")))
#define CreateImageSignatureType_LocalSignature_FeaturePoint_CollectionType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_FeaturePoint_CollectionType))
class Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionType;
class IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionType > Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr;

#define TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType")))
#define CreateImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType))
class Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType;
class IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionType > Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_CollectionPtr;

#define TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType")))
#define CreateImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType))
class Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType;
class IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType > Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr;

#define TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalType")))
#define CreateImageSignatureType_LocalSignature_FeaturePoint_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_FeaturePoint_LocalType))
class Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType;
class IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType > Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalPtr;

#define TypeOfImageSignatureType_LocalSignature_FeaturePointCount_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePointCount_LocalType")))
#define CreateImageSignatureType_LocalSignature_FeaturePointCount_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_FeaturePointCount_LocalType))
class Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalType;
class IMp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalType > Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr;

#define TypeOfImageSignatureType_LocalSignature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_LocalType")))
#define CreateImageSignatureType_LocalSignature_LocalType (Dc1Factory::CreateObject(TypeOfImageSignatureType_LocalSignature_LocalType))
class Mp7JrsImageSignatureType_LocalSignature_LocalType;
class IMp7JrsImageSignatureType_LocalSignature_LocalType;
/** Smart pointer for instance of IMp7JrsImageSignatureType_LocalSignature_LocalType */
typedef Dc1Ptr< IMp7JrsImageSignatureType_LocalSignature_LocalType > Mp7JrsImageSignatureType_LocalSignature_LocalPtr;

#define TypeOfImageTextType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageTextType")))
#define CreateImageTextType (Dc1Factory::CreateObject(TypeOfImageTextType))
class Mp7JrsImageTextType;
class IMp7JrsImageTextType;
/** Smart pointer for instance of IMp7JrsImageTextType */
typedef Dc1Ptr< IMp7JrsImageTextType > Mp7JrsImageTextPtr;

#define TypeOfImageTextType_textType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageTextType_textType_LocalType")))
#define CreateImageTextType_textType_LocalType (Dc1Factory::CreateObject(TypeOfImageTextType_textType_LocalType))
class Mp7JrsImageTextType_textType_LocalType;
class IMp7JrsImageTextType_textType_LocalType;
// No smart pointer instance for IMp7JrsImageTextType_textType_LocalType

#define TypeOfImageTextType_textType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageTextType_textType_LocalType0")))
#define CreateImageTextType_textType_LocalType0 (Dc1Factory::CreateObject(TypeOfImageTextType_textType_LocalType0))
class Mp7JrsImageTextType_textType_LocalType0;
class IMp7JrsImageTextType_textType_LocalType0;
/** Smart pointer for instance of IMp7JrsImageTextType_textType_LocalType0 */
typedef Dc1Ptr< IMp7JrsImageTextType_textType_LocalType0 > Mp7JrsImageTextType_textType_Local0Ptr;

#define TypeOfImageTextType_textType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageTextType_textType_LocalType1")))
#define CreateImageTextType_textType_LocalType1 (Dc1Factory::CreateObject(TypeOfImageTextType_textType_LocalType1))
class Mp7JrsImageTextType_textType_LocalType1;
class IMp7JrsImageTextType_textType_LocalType1;
/** Smart pointer for instance of IMp7JrsImageTextType_textType_LocalType1 */
typedef Dc1Ptr< IMp7JrsImageTextType_textType_LocalType1 > Mp7JrsImageTextType_textType_Local1Ptr;

#define TypeOfImageTextType_textType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageTextType_textType_LocalType2")))
#define CreateImageTextType_textType_LocalType2 (Dc1Factory::CreateObject(TypeOfImageTextType_textType_LocalType2))
class Mp7JrsImageTextType_textType_LocalType2;
class IMp7JrsImageTextType_textType_LocalType2;
/** Smart pointer for instance of IMp7JrsImageTextType_textType_LocalType2 */
typedef Dc1Ptr< IMp7JrsImageTextType_textType_LocalType2 > Mp7JrsImageTextType_textType_Local2Ptr;

#define TypeOfImageType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageType")))
#define CreateImageType (Dc1Factory::CreateObject(TypeOfImageType))
class Mp7JrsImageType;
class IMp7JrsImageType;
/** Smart pointer for instance of IMp7JrsImageType */
typedef Dc1Ptr< IMp7JrsImageType > Mp7JrsImagePtr;

#define TypeOfIncrDurationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IncrDurationType")))
#define CreateIncrDurationType (Dc1Factory::CreateObject(TypeOfIncrDurationType))
class Mp7JrsIncrDurationType;
class IMp7JrsIncrDurationType;
/** Smart pointer for instance of IMp7JrsIncrDurationType */
typedef Dc1Ptr< IMp7JrsIncrDurationType > Mp7JrsIncrDurationPtr;

#define TypeOfIndividualMediaReviewDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IndividualMediaReviewDescriptionType")))
#define CreateIndividualMediaReviewDescriptionType (Dc1Factory::CreateObject(TypeOfIndividualMediaReviewDescriptionType))
class Mp7JrsIndividualMediaReviewDescriptionType;
class IMp7JrsIndividualMediaReviewDescriptionType;
/** Smart pointer for instance of IMp7JrsIndividualMediaReviewDescriptionType */
typedef Dc1Ptr< IMp7JrsIndividualMediaReviewDescriptionType > Mp7JrsIndividualMediaReviewDescriptionPtr;

#define TypeOfInkContentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkContentType")))
#define CreateInkContentType (Dc1Factory::CreateObject(TypeOfInkContentType))
class Mp7JrsInkContentType;
class IMp7JrsInkContentType;
/** Smart pointer for instance of IMp7JrsInkContentType */
typedef Dc1Ptr< IMp7JrsInkContentType > Mp7JrsInkContentPtr;

#define TypeOfInkMediaInformationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType")))
#define CreateInkMediaInformationType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType))
class Mp7JrsInkMediaInformationType;
class IMp7JrsInkMediaInformationType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType > Mp7JrsInkMediaInformationPtr;

#define TypeOfInkMediaInformationType_Brush_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Brush_CollectionType")))
#define CreateInkMediaInformationType_Brush_CollectionType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Brush_CollectionType))
class Mp7JrsInkMediaInformationType_Brush_CollectionType;
class IMp7JrsInkMediaInformationType_Brush_CollectionType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Brush_CollectionType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Brush_CollectionType > Mp7JrsInkMediaInformationType_Brush_CollectionPtr;

#define TypeOfInkMediaInformationType_Brush_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Brush_LocalType")))
#define CreateInkMediaInformationType_Brush_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Brush_LocalType))
class Mp7JrsInkMediaInformationType_Brush_LocalType;
class IMp7JrsInkMediaInformationType_Brush_LocalType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Brush_LocalType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Brush_LocalType > Mp7JrsInkMediaInformationType_Brush_LocalPtr;

#define TypeOfInkMediaInformationType_Handedness_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Handedness_LocalType")))
#define CreateInkMediaInformationType_Handedness_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Handedness_LocalType))
class Mp7JrsInkMediaInformationType_Handedness_LocalType;
class IMp7JrsInkMediaInformationType_Handedness_LocalType;
// No smart pointer instance for IMp7JrsInkMediaInformationType_Handedness_LocalType

#define TypeOfInkMediaInformationType_InputDevice_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_InputDevice_LocalType")))
#define CreateInkMediaInformationType_InputDevice_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_InputDevice_LocalType))
class Mp7JrsInkMediaInformationType_InputDevice_LocalType;
class IMp7JrsInkMediaInformationType_InputDevice_LocalType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_InputDevice_LocalType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_InputDevice_LocalType > Mp7JrsInkMediaInformationType_InputDevice_LocalPtr;

#define TypeOfInkMediaInformationType_OverlaidMedia_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_OverlaidMedia_CollectionType")))
#define CreateInkMediaInformationType_OverlaidMedia_CollectionType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_OverlaidMedia_CollectionType))
class Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionType;
class IMp7JrsInkMediaInformationType_OverlaidMedia_CollectionType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_OverlaidMedia_CollectionType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_OverlaidMedia_CollectionType > Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr;

#define TypeOfInkMediaInformationType_OverlaidMedia_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_OverlaidMedia_LocalType")))
#define CreateInkMediaInformationType_OverlaidMedia_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_OverlaidMedia_LocalType))
class Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType;
class IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType > Mp7JrsInkMediaInformationType_OverlaidMedia_LocalPtr;

#define TypeOfInkMediaInformationType_Param_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Param_CollectionType")))
#define CreateInkMediaInformationType_Param_CollectionType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Param_CollectionType))
class Mp7JrsInkMediaInformationType_Param_CollectionType;
class IMp7JrsInkMediaInformationType_Param_CollectionType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Param_CollectionType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Param_CollectionType > Mp7JrsInkMediaInformationType_Param_CollectionPtr;

#define TypeOfInkMediaInformationType_Param_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Param_LocalType")))
#define CreateInkMediaInformationType_Param_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Param_LocalType))
class Mp7JrsInkMediaInformationType_Param_LocalType;
class IMp7JrsInkMediaInformationType_Param_LocalType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Param_LocalType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Param_LocalType > Mp7JrsInkMediaInformationType_Param_LocalPtr;

#define TypeOfInkMediaInformationType_Style_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType")))
#define CreateInkMediaInformationType_Style_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Style_LocalType))
class Mp7JrsInkMediaInformationType_Style_LocalType;
class IMp7JrsInkMediaInformationType_Style_LocalType;
// No smart pointer instance for IMp7JrsInkMediaInformationType_Style_LocalType

#define TypeOfInkMediaInformationType_Style_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType0")))
#define CreateInkMediaInformationType_Style_LocalType0 (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Style_LocalType0))
class Mp7JrsInkMediaInformationType_Style_LocalType0;
class IMp7JrsInkMediaInformationType_Style_LocalType0;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Style_LocalType0 */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Style_LocalType0 > Mp7JrsInkMediaInformationType_Style_Local0Ptr;

#define TypeOfInkMediaInformationType_Style_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType1")))
#define CreateInkMediaInformationType_Style_LocalType1 (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Style_LocalType1))
class Mp7JrsInkMediaInformationType_Style_LocalType1;
class IMp7JrsInkMediaInformationType_Style_LocalType1;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Style_LocalType1 */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Style_LocalType1 > Mp7JrsInkMediaInformationType_Style_Local1Ptr;

#define TypeOfInkMediaInformationType_Style_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType2")))
#define CreateInkMediaInformationType_Style_LocalType2 (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_Style_LocalType2))
class Mp7JrsInkMediaInformationType_Style_LocalType2;
class IMp7JrsInkMediaInformationType_Style_LocalType2;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_Style_LocalType2 */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_Style_LocalType2 > Mp7JrsInkMediaInformationType_Style_Local2Ptr;

#define TypeOfInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType")))
#define CreateInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType))
class Mp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType;
class IMp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionType > Mp7JrsInkMediaInformationType_WritingFieldLayout_fieldIDList_CollectionPtr;

#define TypeOfInkMediaInformationType_WritingFieldLayout_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_WritingFieldLayout_LocalType")))
#define CreateInkMediaInformationType_WritingFieldLayout_LocalType (Dc1Factory::CreateObject(TypeOfInkMediaInformationType_WritingFieldLayout_LocalType))
class Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalType;
class IMp7JrsInkMediaInformationType_WritingFieldLayout_LocalType;
/** Smart pointer for instance of IMp7JrsInkMediaInformationType_WritingFieldLayout_LocalType */
typedef Dc1Ptr< IMp7JrsInkMediaInformationType_WritingFieldLayout_LocalType > Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr;

#define TypeOfInkSegmentSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentSpatialDecompositionType")))
#define CreateInkSegmentSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfInkSegmentSpatialDecompositionType))
class Mp7JrsInkSegmentSpatialDecompositionType;
class IMp7JrsInkSegmentSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsInkSegmentSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsInkSegmentSpatialDecompositionType > Mp7JrsInkSegmentSpatialDecompositionPtr;

#define TypeOfInkSegmentSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentSpatialDecompositionType_CollectionType")))
#define CreateInkSegmentSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfInkSegmentSpatialDecompositionType_CollectionType))
class Mp7JrsInkSegmentSpatialDecompositionType_CollectionType;
class IMp7JrsInkSegmentSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsInkSegmentSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsInkSegmentSpatialDecompositionType_CollectionType > Mp7JrsInkSegmentSpatialDecompositionType_CollectionPtr;

#define TypeOfInkSegmentSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentSpatialDecompositionType_LocalType")))
#define CreateInkSegmentSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfInkSegmentSpatialDecompositionType_LocalType))
class Mp7JrsInkSegmentSpatialDecompositionType_LocalType;
class IMp7JrsInkSegmentSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsInkSegmentSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsInkSegmentSpatialDecompositionType_LocalType > Mp7JrsInkSegmentSpatialDecompositionType_LocalPtr;

#define TypeOfInkSegmentTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentTemporalDecompositionType")))
#define CreateInkSegmentTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfInkSegmentTemporalDecompositionType))
class Mp7JrsInkSegmentTemporalDecompositionType;
class IMp7JrsInkSegmentTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsInkSegmentTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsInkSegmentTemporalDecompositionType > Mp7JrsInkSegmentTemporalDecompositionPtr;

#define TypeOfInkSegmentTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentTemporalDecompositionType_CollectionType")))
#define CreateInkSegmentTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfInkSegmentTemporalDecompositionType_CollectionType))
class Mp7JrsInkSegmentTemporalDecompositionType_CollectionType;
class IMp7JrsInkSegmentTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsInkSegmentTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsInkSegmentTemporalDecompositionType_CollectionType > Mp7JrsInkSegmentTemporalDecompositionType_CollectionPtr;

#define TypeOfInkSegmentTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentTemporalDecompositionType_LocalType")))
#define CreateInkSegmentTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfInkSegmentTemporalDecompositionType_LocalType))
class Mp7JrsInkSegmentTemporalDecompositionType_LocalType;
class IMp7JrsInkSegmentTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsInkSegmentTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsInkSegmentTemporalDecompositionType_LocalType > Mp7JrsInkSegmentTemporalDecompositionType_LocalPtr;

#define TypeOfInkSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType")))
#define CreateInkSegmentType (Dc1Factory::CreateObject(TypeOfInkSegmentType))
class Mp7JrsInkSegmentType;
class IMp7JrsInkSegmentType;
/** Smart pointer for instance of IMp7JrsInkSegmentType */
typedef Dc1Ptr< IMp7JrsInkSegmentType > Mp7JrsInkSegmentPtr;

#define TypeOfInkSegmentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_CollectionType")))
#define CreateInkSegmentType_CollectionType (Dc1Factory::CreateObject(TypeOfInkSegmentType_CollectionType))
class Mp7JrsInkSegmentType_CollectionType;
class IMp7JrsInkSegmentType_CollectionType;
/** Smart pointer for instance of IMp7JrsInkSegmentType_CollectionType */
typedef Dc1Ptr< IMp7JrsInkSegmentType_CollectionType > Mp7JrsInkSegmentType_CollectionPtr;

#define TypeOfInkSegmentType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_CollectionType0")))
#define CreateInkSegmentType_CollectionType0 (Dc1Factory::CreateObject(TypeOfInkSegmentType_CollectionType0))
class Mp7JrsInkSegmentType_CollectionType0;
class IMp7JrsInkSegmentType_CollectionType0;
/** Smart pointer for instance of IMp7JrsInkSegmentType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsInkSegmentType_CollectionType0 > Mp7JrsInkSegmentType_Collection0Ptr;

#define TypeOfInkSegmentType_CollectionType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_CollectionType1")))
#define CreateInkSegmentType_CollectionType1 (Dc1Factory::CreateObject(TypeOfInkSegmentType_CollectionType1))
class Mp7JrsInkSegmentType_CollectionType1;
class IMp7JrsInkSegmentType_CollectionType1;
/** Smart pointer for instance of IMp7JrsInkSegmentType_CollectionType1 */
typedef Dc1Ptr< IMp7JrsInkSegmentType_CollectionType1 > Mp7JrsInkSegmentType_Collection1Ptr;

#define TypeOfInkSegmentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_LocalType")))
#define CreateInkSegmentType_LocalType (Dc1Factory::CreateObject(TypeOfInkSegmentType_LocalType))
class Mp7JrsInkSegmentType_LocalType;
class IMp7JrsInkSegmentType_LocalType;
/** Smart pointer for instance of IMp7JrsInkSegmentType_LocalType */
typedef Dc1Ptr< IMp7JrsInkSegmentType_LocalType > Mp7JrsInkSegmentType_LocalPtr;

#define TypeOfInkSegmentType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_LocalType0")))
#define CreateInkSegmentType_LocalType0 (Dc1Factory::CreateObject(TypeOfInkSegmentType_LocalType0))
class Mp7JrsInkSegmentType_LocalType0;
class IMp7JrsInkSegmentType_LocalType0;
/** Smart pointer for instance of IMp7JrsInkSegmentType_LocalType0 */
typedef Dc1Ptr< IMp7JrsInkSegmentType_LocalType0 > Mp7JrsInkSegmentType_Local0Ptr;

#define TypeOfInkSegmentType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_LocalType1")))
#define CreateInkSegmentType_LocalType1 (Dc1Factory::CreateObject(TypeOfInkSegmentType_LocalType1))
class Mp7JrsInkSegmentType_LocalType1;
class IMp7JrsInkSegmentType_LocalType1;
/** Smart pointer for instance of IMp7JrsInkSegmentType_LocalType1 */
typedef Dc1Ptr< IMp7JrsInkSegmentType_LocalType1 > Mp7JrsInkSegmentType_Local1Ptr;

#define TypeOfInkSegmentType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkSegmentType_type_LocalType")))
#define CreateInkSegmentType_type_LocalType (Dc1Factory::CreateObject(TypeOfInkSegmentType_type_LocalType))
class Mp7JrsInkSegmentType_type_LocalType;
class IMp7JrsInkSegmentType_type_LocalType;
// No smart pointer instance for IMp7JrsInkSegmentType_type_LocalType

#define TypeOfInlineMediaType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineMediaType")))
#define CreateInlineMediaType (Dc1Factory::CreateObject(TypeOfInlineMediaType))
class Mp7JrsInlineMediaType;
class IMp7JrsInlineMediaType;
/** Smart pointer for instance of IMp7JrsInlineMediaType */
typedef Dc1Ptr< IMp7JrsInlineMediaType > Mp7JrsInlineMediaPtr;

#define TypeOfInlineTermDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType")))
#define CreateInlineTermDefinitionType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType))
class Mp7JrsInlineTermDefinitionType;
class IMp7JrsInlineTermDefinitionType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType > Mp7JrsInlineTermDefinitionPtr;

#define TypeOfInlineTermDefinitionType_Definition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Definition_CollectionType")))
#define CreateInlineTermDefinitionType_Definition_CollectionType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType_Definition_CollectionType))
class Mp7JrsInlineTermDefinitionType_Definition_CollectionType;
class IMp7JrsInlineTermDefinitionType_Definition_CollectionType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType_Definition_CollectionType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType_Definition_CollectionType > Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr;

#define TypeOfInlineTermDefinitionType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Name_CollectionType")))
#define CreateInlineTermDefinitionType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType_Name_CollectionType))
class Mp7JrsInlineTermDefinitionType_Name_CollectionType;
class IMp7JrsInlineTermDefinitionType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType_Name_CollectionType > Mp7JrsInlineTermDefinitionType_Name_CollectionPtr;

#define TypeOfInlineTermDefinitionType_Name_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Name_LocalType")))
#define CreateInlineTermDefinitionType_Name_LocalType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType_Name_LocalType))
class Mp7JrsInlineTermDefinitionType_Name_LocalType;
class IMp7JrsInlineTermDefinitionType_Name_LocalType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType_Name_LocalType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType_Name_LocalType > Mp7JrsInlineTermDefinitionType_Name_LocalPtr;

#define TypeOfInlineTermDefinitionType_Term_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Term_CollectionType")))
#define CreateInlineTermDefinitionType_Term_CollectionType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType_Term_CollectionType))
class Mp7JrsInlineTermDefinitionType_Term_CollectionType;
class IMp7JrsInlineTermDefinitionType_Term_CollectionType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType_Term_CollectionType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType_Term_CollectionType > Mp7JrsInlineTermDefinitionType_Term_CollectionPtr;

#define TypeOfInlineTermDefinitionType_Term_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Term_LocalType")))
#define CreateInlineTermDefinitionType_Term_LocalType (Dc1Factory::CreateObject(TypeOfInlineTermDefinitionType_Term_LocalType))
class Mp7JrsInlineTermDefinitionType_Term_LocalType;
class IMp7JrsInlineTermDefinitionType_Term_LocalType;
/** Smart pointer for instance of IMp7JrsInlineTermDefinitionType_Term_LocalType */
typedef Dc1Ptr< IMp7JrsInlineTermDefinitionType_Term_LocalType > Mp7JrsInlineTermDefinitionType_Term_LocalPtr;

#define TypeOfInstantMessagingScreenNameType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InstantMessagingScreenNameType")))
#define CreateInstantMessagingScreenNameType (Dc1Factory::CreateObject(TypeOfInstantMessagingScreenNameType))
class Mp7JrsInstantMessagingScreenNameType;
class IMp7JrsInstantMessagingScreenNameType;
/** Smart pointer for instance of IMp7JrsInstantMessagingScreenNameType */
typedef Dc1Ptr< IMp7JrsInstantMessagingScreenNameType > Mp7JrsInstantMessagingScreenNamePtr;

#define TypeOfInstrumentTimbreType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InstrumentTimbreType")))
#define CreateInstrumentTimbreType (Dc1Factory::CreateObject(TypeOfInstrumentTimbreType))
class Mp7JrsInstrumentTimbreType;
class IMp7JrsInstrumentTimbreType;
/** Smart pointer for instance of IMp7JrsInstrumentTimbreType */
typedef Dc1Ptr< IMp7JrsInstrumentTimbreType > Mp7JrsInstrumentTimbrePtr;

#define TypeOfIntegerDiagonalMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerDiagonalMatrixType")))
#define CreateIntegerDiagonalMatrixType (Dc1Factory::CreateObject(TypeOfIntegerDiagonalMatrixType))
class Mp7JrsIntegerDiagonalMatrixType;
class IMp7JrsIntegerDiagonalMatrixType;
/** Smart pointer for instance of IMp7JrsIntegerDiagonalMatrixType */
typedef Dc1Ptr< IMp7JrsIntegerDiagonalMatrixType > Mp7JrsIntegerDiagonalMatrixPtr;

#define TypeOfIntegerMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType")))
#define CreateIntegerMatrixType (Dc1Factory::CreateObject(TypeOfIntegerMatrixType))
class Mp7JrsIntegerMatrixType;
class IMp7JrsIntegerMatrixType;
/** Smart pointer for instance of IMp7JrsIntegerMatrixType */
typedef Dc1Ptr< IMp7JrsIntegerMatrixType > Mp7JrsIntegerMatrixPtr;

#define TypeOfintegerVector (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:integerVector")))
#define CreateintegerVector (Dc1Factory::CreateObject(TypeOfintegerVector))
class Mp7JrsintegerVector;
class IMp7JrsintegerVector;
/** Smart pointer for instance of IMp7JrsintegerVector */
typedef Dc1Ptr< IMp7JrsintegerVector > Mp7JrsintegerVectorPtr;

#define TypeOfInternalTransitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InternalTransitionType")))
#define CreateInternalTransitionType (Dc1Factory::CreateObject(TypeOfInternalTransitionType))
class Mp7JrsInternalTransitionType;
class IMp7JrsInternalTransitionType;
/** Smart pointer for instance of IMp7JrsInternalTransitionType */
typedef Dc1Ptr< IMp7JrsInternalTransitionType > Mp7JrsInternalTransitionPtr;

#define TypeOfIntraCompositionShotEditingTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotEditingTemporalDecompositionType")))
#define CreateIntraCompositionShotEditingTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfIntraCompositionShotEditingTemporalDecompositionType))
class Mp7JrsIntraCompositionShotEditingTemporalDecompositionType;
class IMp7JrsIntraCompositionShotEditingTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsIntraCompositionShotEditingTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsIntraCompositionShotEditingTemporalDecompositionType > Mp7JrsIntraCompositionShotEditingTemporalDecompositionPtr;

#define TypeOfIntraCompositionShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotEditingTemporalDecompositionType_CollectionType")))
#define CreateIntraCompositionShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfIntraCompositionShotEditingTemporalDecompositionType_CollectionType))
class Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionType;
class IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionType > Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_CollectionPtr;

#define TypeOfIntraCompositionShotEditingTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotEditingTemporalDecompositionType_LocalType")))
#define CreateIntraCompositionShotEditingTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfIntraCompositionShotEditingTemporalDecompositionType_LocalType))
class Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType;
class IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType > Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalPtr;

#define TypeOfIntraCompositionShotType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotType")))
#define CreateIntraCompositionShotType (Dc1Factory::CreateObject(TypeOfIntraCompositionShotType))
class Mp7JrsIntraCompositionShotType;
class IMp7JrsIntraCompositionShotType;
/** Smart pointer for instance of IMp7JrsIntraCompositionShotType */
typedef Dc1Ptr< IMp7JrsIntraCompositionShotType > Mp7JrsIntraCompositionShotPtr;

#define TypeOfIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType")))
#define CreateIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType))
class Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType;
class IMp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType > Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr;

#define TypeOfIrregularVisualTimeSeriesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IrregularVisualTimeSeriesType")))
#define CreateIrregularVisualTimeSeriesType (Dc1Factory::CreateObject(TypeOfIrregularVisualTimeSeriesType))
class Mp7JrsIrregularVisualTimeSeriesType;
class IMp7JrsIrregularVisualTimeSeriesType;
/** Smart pointer for instance of IMp7JrsIrregularVisualTimeSeriesType */
typedef Dc1Ptr< IMp7JrsIrregularVisualTimeSeriesType > Mp7JrsIrregularVisualTimeSeriesPtr;

#define TypeOfIrregularVisualTimeSeriesType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IrregularVisualTimeSeriesType_CollectionType")))
#define CreateIrregularVisualTimeSeriesType_CollectionType (Dc1Factory::CreateObject(TypeOfIrregularVisualTimeSeriesType_CollectionType))
class Mp7JrsIrregularVisualTimeSeriesType_CollectionType;
class IMp7JrsIrregularVisualTimeSeriesType_CollectionType;
/** Smart pointer for instance of IMp7JrsIrregularVisualTimeSeriesType_CollectionType */
typedef Dc1Ptr< IMp7JrsIrregularVisualTimeSeriesType_CollectionType > Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr;

#define TypeOfIrregularVisualTimeSeriesType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IrregularVisualTimeSeriesType_LocalType")))
#define CreateIrregularVisualTimeSeriesType_LocalType (Dc1Factory::CreateObject(TypeOfIrregularVisualTimeSeriesType_LocalType))
class Mp7JrsIrregularVisualTimeSeriesType_LocalType;
class IMp7JrsIrregularVisualTimeSeriesType_LocalType;
/** Smart pointer for instance of IMp7JrsIrregularVisualTimeSeriesType_LocalType */
typedef Dc1Ptr< IMp7JrsIrregularVisualTimeSeriesType_LocalType > Mp7JrsIrregularVisualTimeSeriesType_LocalPtr;

#define TypeOfKeyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeyType")))
#define CreateKeyType (Dc1Factory::CreateObject(TypeOfKeyType))
class Mp7JrsKeyType;
class IMp7JrsKeyType;
/** Smart pointer for instance of IMp7JrsKeyType */
typedef Dc1Ptr< IMp7JrsKeyType > Mp7JrsKeyPtr;

#define TypeOfKeyType_KeyNote_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeyType_KeyNote_LocalType")))
#define CreateKeyType_KeyNote_LocalType (Dc1Factory::CreateObject(TypeOfKeyType_KeyNote_LocalType))
class Mp7JrsKeyType_KeyNote_LocalType;
class IMp7JrsKeyType_KeyNote_LocalType;
/** Smart pointer for instance of IMp7JrsKeyType_KeyNote_LocalType */
typedef Dc1Ptr< IMp7JrsKeyType_KeyNote_LocalType > Mp7JrsKeyType_KeyNote_LocalPtr;

#define TypeOfKeywordAnnotationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType")))
#define CreateKeywordAnnotationType (Dc1Factory::CreateObject(TypeOfKeywordAnnotationType))
class Mp7JrsKeywordAnnotationType;
class IMp7JrsKeywordAnnotationType;
/** Smart pointer for instance of IMp7JrsKeywordAnnotationType */
typedef Dc1Ptr< IMp7JrsKeywordAnnotationType > Mp7JrsKeywordAnnotationPtr;

#define TypeOfKeywordAnnotationType_Keyword_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType_Keyword_CollectionType")))
#define CreateKeywordAnnotationType_Keyword_CollectionType (Dc1Factory::CreateObject(TypeOfKeywordAnnotationType_Keyword_CollectionType))
class Mp7JrsKeywordAnnotationType_Keyword_CollectionType;
class IMp7JrsKeywordAnnotationType_Keyword_CollectionType;
/** Smart pointer for instance of IMp7JrsKeywordAnnotationType_Keyword_CollectionType */
typedef Dc1Ptr< IMp7JrsKeywordAnnotationType_Keyword_CollectionType > Mp7JrsKeywordAnnotationType_Keyword_CollectionPtr;

#define TypeOfKeywordAnnotationType_Keyword_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType_Keyword_LocalType")))
#define CreateKeywordAnnotationType_Keyword_LocalType (Dc1Factory::CreateObject(TypeOfKeywordAnnotationType_Keyword_LocalType))
class Mp7JrsKeywordAnnotationType_Keyword_LocalType;
class IMp7JrsKeywordAnnotationType_Keyword_LocalType;
/** Smart pointer for instance of IMp7JrsKeywordAnnotationType_Keyword_LocalType */
typedef Dc1Ptr< IMp7JrsKeywordAnnotationType_Keyword_LocalType > Mp7JrsKeywordAnnotationType_Keyword_LocalPtr;

#define TypeOfKeywordAnnotationType_Keyword_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType_Keyword_type_LocalType")))
#define CreateKeywordAnnotationType_Keyword_type_LocalType (Dc1Factory::CreateObject(TypeOfKeywordAnnotationType_Keyword_type_LocalType))
class Mp7JrsKeywordAnnotationType_Keyword_type_LocalType;
class IMp7JrsKeywordAnnotationType_Keyword_type_LocalType;
// No smart pointer instance for IMp7JrsKeywordAnnotationType_Keyword_type_LocalType

#define TypeOfLexiconType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LexiconType")))
#define CreateLexiconType (Dc1Factory::CreateObject(TypeOfLexiconType))
class Mp7JrsLexiconType;
class IMp7JrsLexiconType;
/** Smart pointer for instance of IMp7JrsLexiconType */
typedef Dc1Ptr< IMp7JrsLexiconType > Mp7JrsLexiconPtr;

#define TypeOfLinearPerceptualAttributeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinearPerceptualAttributeType")))
#define CreateLinearPerceptualAttributeType (Dc1Factory::CreateObject(TypeOfLinearPerceptualAttributeType))
class Mp7JrsLinearPerceptualAttributeType;
class IMp7JrsLinearPerceptualAttributeType;
/** Smart pointer for instance of IMp7JrsLinearPerceptualAttributeType */
typedef Dc1Ptr< IMp7JrsLinearPerceptualAttributeType > Mp7JrsLinearPerceptualAttributePtr;

#define TypeOfLinguisticDocumentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType")))
#define CreateLinguisticDocumentType (Dc1Factory::CreateObject(TypeOfLinguisticDocumentType))
class Mp7JrsLinguisticDocumentType;
class IMp7JrsLinguisticDocumentType;
/** Smart pointer for instance of IMp7JrsLinguisticDocumentType */
typedef Dc1Ptr< IMp7JrsLinguisticDocumentType > Mp7JrsLinguisticDocumentPtr;

#define TypeOfLinguisticDocumentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType_CollectionType")))
#define CreateLinguisticDocumentType_CollectionType (Dc1Factory::CreateObject(TypeOfLinguisticDocumentType_CollectionType))
class Mp7JrsLinguisticDocumentType_CollectionType;
class IMp7JrsLinguisticDocumentType_CollectionType;
/** Smart pointer for instance of IMp7JrsLinguisticDocumentType_CollectionType */
typedef Dc1Ptr< IMp7JrsLinguisticDocumentType_CollectionType > Mp7JrsLinguisticDocumentType_CollectionPtr;

#define TypeOfLinguisticDocumentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType_LocalType")))
#define CreateLinguisticDocumentType_LocalType (Dc1Factory::CreateObject(TypeOfLinguisticDocumentType_LocalType))
class Mp7JrsLinguisticDocumentType_LocalType;
class IMp7JrsLinguisticDocumentType_LocalType;
/** Smart pointer for instance of IMp7JrsLinguisticDocumentType_LocalType */
typedef Dc1Ptr< IMp7JrsLinguisticDocumentType_LocalType > Mp7JrsLinguisticDocumentType_LocalPtr;

#define TypeOfLinguisticEntityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType")))
#define CreateLinguisticEntityType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType))
class Mp7JrsLinguisticEntityType;
class IMp7JrsLinguisticEntityType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType > Mp7JrsLinguisticEntityPtr;

#define TypeOfLinguisticEntityType_edit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType_edit_LocalType")))
#define CreateLinguisticEntityType_edit_LocalType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType_edit_LocalType))
class Mp7JrsLinguisticEntityType_edit_LocalType;
class IMp7JrsLinguisticEntityType_edit_LocalType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType_edit_LocalType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_edit_LocalType > Mp7JrsLinguisticEntityType_edit_LocalPtr;

#define TypeOfLinguisticEntityType_length_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType_length_LocalType")))
#define CreateLinguisticEntityType_length_LocalType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType_length_LocalType))
class Mp7JrsLinguisticEntityType_length_LocalType;
class IMp7JrsLinguisticEntityType_length_LocalType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType_length_LocalType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_length_LocalType > Mp7JrsLinguisticEntityType_length_LocalPtr;

#define TypeOfLinguisticEntityType_MediaLocator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType_MediaLocator_CollectionType")))
#define CreateLinguisticEntityType_MediaLocator_CollectionType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType_MediaLocator_CollectionType))
class Mp7JrsLinguisticEntityType_MediaLocator_CollectionType;
class IMp7JrsLinguisticEntityType_MediaLocator_CollectionType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType_MediaLocator_CollectionType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_MediaLocator_CollectionType > Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr;

#define TypeOfLinguisticEntityType_Relation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType_Relation_CollectionType")))
#define CreateLinguisticEntityType_Relation_CollectionType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType_Relation_CollectionType))
class Mp7JrsLinguisticEntityType_Relation_CollectionType;
class IMp7JrsLinguisticEntityType_Relation_CollectionType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType_Relation_CollectionType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_Relation_CollectionType > Mp7JrsLinguisticEntityType_Relation_CollectionPtr;

#define TypeOfLinguisticEntityType_start_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticEntityType_start_LocalType")))
#define CreateLinguisticEntityType_start_LocalType (Dc1Factory::CreateObject(TypeOfLinguisticEntityType_start_LocalType))
class Mp7JrsLinguisticEntityType_start_LocalType;
class IMp7JrsLinguisticEntityType_start_LocalType;
/** Smart pointer for instance of IMp7JrsLinguisticEntityType_start_LocalType */
typedef Dc1Ptr< IMp7JrsLinguisticEntityType_start_LocalType > Mp7JrsLinguisticEntityType_start_LocalPtr;

#define TypeOfLinguisticType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticType")))
#define CreateLinguisticType (Dc1Factory::CreateObject(TypeOfLinguisticType))
class Mp7JrsLinguisticType;
class IMp7JrsLinguisticType;
/** Smart pointer for instance of IMp7JrsLinguisticType */
typedef Dc1Ptr< IMp7JrsLinguisticType > Mp7JrsLinguisticPtr;

#define TypeOflistOfPositiveIntegerForDim (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:listOfPositiveIntegerForDim")))
#define CreatelistOfPositiveIntegerForDim (Dc1Factory::CreateObject(TypeOflistOfPositiveIntegerForDim))
class Mp7JrslistOfPositiveIntegerForDim;
class IMp7JrslistOfPositiveIntegerForDim;
/** Smart pointer for instance of IMp7JrslistOfPositiveIntegerForDim */
typedef Dc1Ptr< IMp7JrslistOfPositiveIntegerForDim > Mp7JrslistOfPositiveIntegerForDimPtr;

#define TypeOfLogAttackTimeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogAttackTimeType")))
#define CreateLogAttackTimeType (Dc1Factory::CreateObject(TypeOfLogAttackTimeType))
class Mp7JrsLogAttackTimeType;
class IMp7JrsLogAttackTimeType;
/** Smart pointer for instance of IMp7JrsLogAttackTimeType */
typedef Dc1Ptr< IMp7JrsLogAttackTimeType > Mp7JrsLogAttackTimePtr;

#define TypeOfLogicalUnitLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType")))
#define CreateLogicalUnitLocatorType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType))
class Mp7JrsLogicalUnitLocatorType;
class IMp7JrsLogicalUnitLocatorType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType > Mp7JrsLogicalUnitLocatorPtr;

#define TypeOfLogicalUnitLocatorType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_CollectionType")))
#define CreateLogicalUnitLocatorType_CollectionType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_CollectionType))
class Mp7JrsLogicalUnitLocatorType_CollectionType;
class IMp7JrsLogicalUnitLocatorType_CollectionType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_CollectionType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_CollectionType > Mp7JrsLogicalUnitLocatorType_CollectionPtr;

#define TypeOfLogicalUnitLocatorType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_LocalType")))
#define CreateLogicalUnitLocatorType_LocalType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_LocalType))
class Mp7JrsLogicalUnitLocatorType_LocalType;
class IMp7JrsLogicalUnitLocatorType_LocalType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_LocalType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_LocalType > Mp7JrsLogicalUnitLocatorType_LocalPtr;

#define TypeOfLogicalUnitLocatorType_LogicalUnit_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_LogicalUnit_CollectionType")))
#define CreateLogicalUnitLocatorType_LogicalUnit_CollectionType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_LogicalUnit_CollectionType))
class Mp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionType;
class IMp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionType > Mp7JrsLogicalUnitLocatorType_LogicalUnit_CollectionPtr;

#define TypeOfLogicalUnitLocatorType_LogicalUnit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_LogicalUnit_LocalType")))
#define CreateLogicalUnitLocatorType_LogicalUnit_LocalType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_LogicalUnit_LocalType))
class Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType;
class IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType > Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalPtr;

#define TypeOfLogicalUnitLocatorType_LogicalUnit_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_LogicalUnit_LocalType0")))
#define CreateLogicalUnitLocatorType_LogicalUnit_LocalType0 (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_LogicalUnit_LocalType0))
class Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0;
class IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0 */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0 > Mp7JrsLogicalUnitLocatorType_LogicalUnit_Local0Ptr;

#define TypeOfLogicalUnitLocatorType_ReferenceUnit_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_ReferenceUnit_CollectionType")))
#define CreateLogicalUnitLocatorType_ReferenceUnit_CollectionType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_ReferenceUnit_CollectionType))
class Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionType;
class IMp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionType > Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr;

#define TypeOfLogicalUnitLocatorType_ReferenceUnit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_ReferenceUnit_LocalType")))
#define CreateLogicalUnitLocatorType_ReferenceUnit_LocalType (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_ReferenceUnit_LocalType))
class Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType;
class IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType > Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr;

#define TypeOfLogicalUnitLocatorType_ReferenceUnit_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_ReferenceUnit_LocalType0")))
#define CreateLogicalUnitLocatorType_ReferenceUnit_LocalType0 (Dc1Factory::CreateObject(TypeOfLogicalUnitLocatorType_ReferenceUnit_LocalType0))
class Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0;
class IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0;
/** Smart pointer for instance of IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0 */
typedef Dc1Ptr< IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0 > Mp7JrsLogicalUnitLocatorType_ReferenceUnit_Local0Ptr;

#define TypeOfLognormalDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LognormalDistributionType")))
#define CreateLognormalDistributionType (Dc1Factory::CreateObject(TypeOfLognormalDistributionType))
class Mp7JrsLognormalDistributionType;
class IMp7JrsLognormalDistributionType;
/** Smart pointer for instance of IMp7JrsLognormalDistributionType */
typedef Dc1Ptr< IMp7JrsLognormalDistributionType > Mp7JrsLognormalDistributionPtr;

#define TypeOfMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MaskType")))
#define CreateMaskType (Dc1Factory::CreateObject(TypeOfMaskType))
class Mp7JrsMaskType;
class IMp7JrsMaskType;
/** Smart pointer for instance of IMp7JrsMaskType */
typedef Dc1Ptr< IMp7JrsMaskType > Mp7JrsMaskPtr;

#define TypeOfMatchingHintType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MatchingHintType")))
#define CreateMatchingHintType (Dc1Factory::CreateObject(TypeOfMatchingHintType))
class Mp7JrsMatchingHintType;
class IMp7JrsMatchingHintType;
/** Smart pointer for instance of IMp7JrsMatchingHintType */
typedef Dc1Ptr< IMp7JrsMatchingHintType > Mp7JrsMatchingHintPtr;

#define TypeOfMatchingHintType_Hint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MatchingHintType_Hint_CollectionType")))
#define CreateMatchingHintType_Hint_CollectionType (Dc1Factory::CreateObject(TypeOfMatchingHintType_Hint_CollectionType))
class Mp7JrsMatchingHintType_Hint_CollectionType;
class IMp7JrsMatchingHintType_Hint_CollectionType;
/** Smart pointer for instance of IMp7JrsMatchingHintType_Hint_CollectionType */
typedef Dc1Ptr< IMp7JrsMatchingHintType_Hint_CollectionType > Mp7JrsMatchingHintType_Hint_CollectionPtr;

#define TypeOfMatchingHintType_Hint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MatchingHintType_Hint_LocalType")))
#define CreateMatchingHintType_Hint_LocalType (Dc1Factory::CreateObject(TypeOfMatchingHintType_Hint_LocalType))
class Mp7JrsMatchingHintType_Hint_LocalType;
class IMp7JrsMatchingHintType_Hint_LocalType;
/** Smart pointer for instance of IMp7JrsMatchingHintType_Hint_LocalType */
typedef Dc1Ptr< IMp7JrsMatchingHintType_Hint_LocalType > Mp7JrsMatchingHintType_Hint_LocalPtr;

#define TypeOfMediaAgentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaAgentType")))
#define CreateMediaAgentType (Dc1Factory::CreateObject(TypeOfMediaAgentType))
class Mp7JrsMediaAgentType;
class IMp7JrsMediaAgentType;
/** Smart pointer for instance of IMp7JrsMediaAgentType */
typedef Dc1Ptr< IMp7JrsMediaAgentType > Mp7JrsMediaAgentPtr;

#define TypeOfMediaDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaDescriptionType")))
#define CreateMediaDescriptionType (Dc1Factory::CreateObject(TypeOfMediaDescriptionType))
class Mp7JrsMediaDescriptionType;
class IMp7JrsMediaDescriptionType;
/** Smart pointer for instance of IMp7JrsMediaDescriptionType */
typedef Dc1Ptr< IMp7JrsMediaDescriptionType > Mp7JrsMediaDescriptionPtr;

#define TypeOfMediaDescriptionType_MediaInformation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaDescriptionType_MediaInformation_CollectionType")))
#define CreateMediaDescriptionType_MediaInformation_CollectionType (Dc1Factory::CreateObject(TypeOfMediaDescriptionType_MediaInformation_CollectionType))
class Mp7JrsMediaDescriptionType_MediaInformation_CollectionType;
class IMp7JrsMediaDescriptionType_MediaInformation_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaDescriptionType_MediaInformation_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaDescriptionType_MediaInformation_CollectionType > Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr;

#define TypeOfmediaDurationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))
#define CreatemediaDurationType (Dc1Factory::CreateObject(TypeOfmediaDurationType))
class Mp7JrsmediaDurationType;
class IMp7JrsmediaDurationType;
/** Smart pointer for instance of IMp7JrsmediaDurationType */
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;

#define TypeOfMediaFormatType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType")))
#define CreateMediaFormatType (Dc1Factory::CreateObject(TypeOfMediaFormatType))
class Mp7JrsMediaFormatType;
class IMp7JrsMediaFormatType;
/** Smart pointer for instance of IMp7JrsMediaFormatType */
typedef Dc1Ptr< IMp7JrsMediaFormatType > Mp7JrsMediaFormatPtr;

#define TypeOfMediaFormatType_AudioCoding_AudioChannels_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_AudioChannels_LocalType")))
#define CreateMediaFormatType_AudioCoding_AudioChannels_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_AudioChannels_LocalType))
class Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType;
class IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType > Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr;

#define TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType")))
#define CreateMediaFormatType_AudioCoding_Emphasis_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType))
class Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType;
class IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType;
// No smart pointer instance for IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType

#define TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType0")))
#define CreateMediaFormatType_AudioCoding_Emphasis_LocalType0 (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType0))
class Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType0;
class IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType0;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType0 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType0 > Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local0Ptr;

#define TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType1")))
#define CreateMediaFormatType_AudioCoding_Emphasis_LocalType1 (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType1))
class Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType1;
class IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType1;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType1 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType1 > Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local1Ptr;

#define TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType2")))
#define CreateMediaFormatType_AudioCoding_Emphasis_LocalType2 (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_Emphasis_LocalType2))
class Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2;
class IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2 > Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr;

#define TypeOfMediaFormatType_AudioCoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_LocalType")))
#define CreateMediaFormatType_AudioCoding_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_LocalType))
class Mp7JrsMediaFormatType_AudioCoding_LocalType;
class IMp7JrsMediaFormatType_AudioCoding_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_LocalType > Mp7JrsMediaFormatType_AudioCoding_LocalPtr;

#define TypeOfMediaFormatType_AudioCoding_Sample_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Sample_LocalType")))
#define CreateMediaFormatType_AudioCoding_Sample_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_AudioCoding_Sample_LocalType))
class Mp7JrsMediaFormatType_AudioCoding_Sample_LocalType;
class IMp7JrsMediaFormatType_AudioCoding_Sample_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_AudioCoding_Sample_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_AudioCoding_Sample_LocalType > Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr;

#define TypeOfMediaFormatType_BitRate_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_BitRate_LocalType")))
#define CreateMediaFormatType_BitRate_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_BitRate_LocalType))
class Mp7JrsMediaFormatType_BitRate_LocalType;
class IMp7JrsMediaFormatType_BitRate_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_BitRate_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_BitRate_LocalType > Mp7JrsMediaFormatType_BitRate_LocalPtr;

#define TypeOfMediaFormatType_ScalableCoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType")))
#define CreateMediaFormatType_ScalableCoding_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_ScalableCoding_LocalType))
class Mp7JrsMediaFormatType_ScalableCoding_LocalType;
class IMp7JrsMediaFormatType_ScalableCoding_LocalType;
// No smart pointer instance for IMp7JrsMediaFormatType_ScalableCoding_LocalType

#define TypeOfMediaFormatType_ScalableCoding_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType0")))
#define CreateMediaFormatType_ScalableCoding_LocalType0 (Dc1Factory::CreateObject(TypeOfMediaFormatType_ScalableCoding_LocalType0))
class Mp7JrsMediaFormatType_ScalableCoding_LocalType0;
class IMp7JrsMediaFormatType_ScalableCoding_LocalType0;
/** Smart pointer for instance of IMp7JrsMediaFormatType_ScalableCoding_LocalType0 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_ScalableCoding_LocalType0 > Mp7JrsMediaFormatType_ScalableCoding_Local0Ptr;

#define TypeOfMediaFormatType_ScalableCoding_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType1")))
#define CreateMediaFormatType_ScalableCoding_LocalType1 (Dc1Factory::CreateObject(TypeOfMediaFormatType_ScalableCoding_LocalType1))
class Mp7JrsMediaFormatType_ScalableCoding_LocalType1;
class IMp7JrsMediaFormatType_ScalableCoding_LocalType1;
/** Smart pointer for instance of IMp7JrsMediaFormatType_ScalableCoding_LocalType1 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_ScalableCoding_LocalType1 > Mp7JrsMediaFormatType_ScalableCoding_Local1Ptr;

#define TypeOfMediaFormatType_ScalableCoding_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType2")))
#define CreateMediaFormatType_ScalableCoding_LocalType2 (Dc1Factory::CreateObject(TypeOfMediaFormatType_ScalableCoding_LocalType2))
class Mp7JrsMediaFormatType_ScalableCoding_LocalType2;
class IMp7JrsMediaFormatType_ScalableCoding_LocalType2;
/** Smart pointer for instance of IMp7JrsMediaFormatType_ScalableCoding_LocalType2 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_ScalableCoding_LocalType2 > Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr;

#define TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_colorDomain_LocalType")))
#define CreateMediaFormatType_VisualCoding_Format_colorDomain_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType;
// No smart pointer instance for IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType

#define TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_colorDomain_LocalType0")))
#define CreateMediaFormatType_VisualCoding_Format_colorDomain_LocalType0 (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType0))
class Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType0;
class IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType0;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType0 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType0 > Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_Local0Ptr;

#define TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_colorDomain_LocalType1")))
#define CreateMediaFormatType_VisualCoding_Format_colorDomain_LocalType1 (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType1))
class Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType1;
class IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType1;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType1 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType1 > Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_Local1Ptr;

#define TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_colorDomain_LocalType2")))
#define CreateMediaFormatType_VisualCoding_Format_colorDomain_LocalType2 (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Format_colorDomain_LocalType2))
class Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType2;
class IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType2;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType2 */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Format_colorDomain_LocalType2 > Mp7JrsMediaFormatType_VisualCoding_Format_colorDomain_Local2Ptr;

#define TypeOfMediaFormatType_VisualCoding_Format_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_LocalType")))
#define CreateMediaFormatType_VisualCoding_Format_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Format_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_Format_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_Format_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Format_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Format_LocalType > Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr;

#define TypeOfMediaFormatType_VisualCoding_Frame_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Frame_LocalType")))
#define CreateMediaFormatType_VisualCoding_Frame_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Frame_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_Frame_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_Frame_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Frame_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Frame_LocalType > Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr;

#define TypeOfMediaFormatType_VisualCoding_Frame_structure_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Frame_structure_LocalType")))
#define CreateMediaFormatType_VisualCoding_Frame_structure_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Frame_structure_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_Frame_structure_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_Frame_structure_LocalType;
// No smart pointer instance for IMp7JrsMediaFormatType_VisualCoding_Frame_structure_LocalType

#define TypeOfMediaFormatType_VisualCoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_LocalType")))
#define CreateMediaFormatType_VisualCoding_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_LocalType > Mp7JrsMediaFormatType_VisualCoding_LocalPtr;

#define TypeOfMediaFormatType_VisualCoding_Pixel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Pixel_LocalType")))
#define CreateMediaFormatType_VisualCoding_Pixel_LocalType (Dc1Factory::CreateObject(TypeOfMediaFormatType_VisualCoding_Pixel_LocalType))
class Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType;
class IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType;
/** Smart pointer for instance of IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType */
typedef Dc1Ptr< IMp7JrsMediaFormatType_VisualCoding_Pixel_LocalType > Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr;

#define TypeOfMediaIdentificationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIdentificationType")))
#define CreateMediaIdentificationType (Dc1Factory::CreateObject(TypeOfMediaIdentificationType))
class Mp7JrsMediaIdentificationType;
class IMp7JrsMediaIdentificationType;
/** Smart pointer for instance of IMp7JrsMediaIdentificationType */
typedef Dc1Ptr< IMp7JrsMediaIdentificationType > Mp7JrsMediaIdentificationPtr;

#define TypeOfMediaIdentificationType_AudioDomain_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIdentificationType_AudioDomain_CollectionType")))
#define CreateMediaIdentificationType_AudioDomain_CollectionType (Dc1Factory::CreateObject(TypeOfMediaIdentificationType_AudioDomain_CollectionType))
class Mp7JrsMediaIdentificationType_AudioDomain_CollectionType;
class IMp7JrsMediaIdentificationType_AudioDomain_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaIdentificationType_AudioDomain_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaIdentificationType_AudioDomain_CollectionType > Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr;

#define TypeOfMediaIdentificationType_ImageDomain_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIdentificationType_ImageDomain_CollectionType")))
#define CreateMediaIdentificationType_ImageDomain_CollectionType (Dc1Factory::CreateObject(TypeOfMediaIdentificationType_ImageDomain_CollectionType))
class Mp7JrsMediaIdentificationType_ImageDomain_CollectionType;
class IMp7JrsMediaIdentificationType_ImageDomain_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaIdentificationType_ImageDomain_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaIdentificationType_ImageDomain_CollectionType > Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr;

#define TypeOfMediaIdentificationType_VideoDomain_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIdentificationType_VideoDomain_CollectionType")))
#define CreateMediaIdentificationType_VideoDomain_CollectionType (Dc1Factory::CreateObject(TypeOfMediaIdentificationType_VideoDomain_CollectionType))
class Mp7JrsMediaIdentificationType_VideoDomain_CollectionType;
class IMp7JrsMediaIdentificationType_VideoDomain_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaIdentificationType_VideoDomain_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaIdentificationType_VideoDomain_CollectionType > Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr;

#define TypeOfMediaIncrDurationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType")))
#define CreateMediaIncrDurationType (Dc1Factory::CreateObject(TypeOfMediaIncrDurationType))
class Mp7JrsMediaIncrDurationType;
class IMp7JrsMediaIncrDurationType;
/** Smart pointer for instance of IMp7JrsMediaIncrDurationType */
typedef Dc1Ptr< IMp7JrsMediaIncrDurationType > Mp7JrsMediaIncrDurationPtr;

#define TypeOfMediaInformationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType")))
#define CreateMediaInformationType (Dc1Factory::CreateObject(TypeOfMediaInformationType))
class Mp7JrsMediaInformationType;
class IMp7JrsMediaInformationType;
/** Smart pointer for instance of IMp7JrsMediaInformationType */
typedef Dc1Ptr< IMp7JrsMediaInformationType > Mp7JrsMediaInformationPtr;

#define TypeOfMediaInformationType_MediaProfile_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType_MediaProfile_CollectionType")))
#define CreateMediaInformationType_MediaProfile_CollectionType (Dc1Factory::CreateObject(TypeOfMediaInformationType_MediaProfile_CollectionType))
class Mp7JrsMediaInformationType_MediaProfile_CollectionType;
class IMp7JrsMediaInformationType_MediaProfile_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaInformationType_MediaProfile_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaInformationType_MediaProfile_CollectionType > Mp7JrsMediaInformationType_MediaProfile_CollectionPtr;

#define TypeOfMediaInstanceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInstanceType")))
#define CreateMediaInstanceType (Dc1Factory::CreateObject(TypeOfMediaInstanceType))
class Mp7JrsMediaInstanceType;
class IMp7JrsMediaInstanceType;
/** Smart pointer for instance of IMp7JrsMediaInstanceType */
typedef Dc1Ptr< IMp7JrsMediaInstanceType > Mp7JrsMediaInstancePtr;

#define TypeOfMediaLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))
#define CreateMediaLocatorType (Dc1Factory::CreateObject(TypeOfMediaLocatorType))
class Mp7JrsMediaLocatorType;
class IMp7JrsMediaLocatorType;
/** Smart pointer for instance of IMp7JrsMediaLocatorType */
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;

#define TypeOfMediaProfileType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaProfileType")))
#define CreateMediaProfileType (Dc1Factory::CreateObject(TypeOfMediaProfileType))
class Mp7JrsMediaProfileType;
class IMp7JrsMediaProfileType;
/** Smart pointer for instance of IMp7JrsMediaProfileType */
typedef Dc1Ptr< IMp7JrsMediaProfileType > Mp7JrsMediaProfilePtr;

#define TypeOfMediaProfileType_ComponentMediaProfile_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaProfileType_ComponentMediaProfile_CollectionType")))
#define CreateMediaProfileType_ComponentMediaProfile_CollectionType (Dc1Factory::CreateObject(TypeOfMediaProfileType_ComponentMediaProfile_CollectionType))
class Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionType;
class IMp7JrsMediaProfileType_ComponentMediaProfile_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaProfileType_ComponentMediaProfile_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaProfileType_ComponentMediaProfile_CollectionType > Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr;

#define TypeOfMediaProfileType_MediaInstance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaProfileType_MediaInstance_CollectionType")))
#define CreateMediaProfileType_MediaInstance_CollectionType (Dc1Factory::CreateObject(TypeOfMediaProfileType_MediaInstance_CollectionType))
class Mp7JrsMediaProfileType_MediaInstance_CollectionType;
class IMp7JrsMediaProfileType_MediaInstance_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaProfileType_MediaInstance_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaProfileType_MediaInstance_CollectionType > Mp7JrsMediaProfileType_MediaInstance_CollectionPtr;

#define TypeOfMediaQualityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType")))
#define CreateMediaQualityType (Dc1Factory::CreateObject(TypeOfMediaQualityType))
class Mp7JrsMediaQualityType;
class IMp7JrsMediaQualityType;
/** Smart pointer for instance of IMp7JrsMediaQualityType */
typedef Dc1Ptr< IMp7JrsMediaQualityType > Mp7JrsMediaQualityPtr;

#define TypeOfMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_PerceptibleDefects_AudioDefects_CollectionType")))
#define CreateMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType (Dc1Factory::CreateObject(TypeOfMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType))
class Mp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType;
class IMp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionType > Mp7JrsMediaQualityType_PerceptibleDefects_AudioDefects_CollectionPtr;

#define TypeOfMediaQualityType_PerceptibleDefects_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_PerceptibleDefects_LocalType")))
#define CreateMediaQualityType_PerceptibleDefects_LocalType (Dc1Factory::CreateObject(TypeOfMediaQualityType_PerceptibleDefects_LocalType))
class Mp7JrsMediaQualityType_PerceptibleDefects_LocalType;
class IMp7JrsMediaQualityType_PerceptibleDefects_LocalType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_PerceptibleDefects_LocalType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_PerceptibleDefects_LocalType > Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr;

#define TypeOfMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_PerceptibleDefects_VisualDefects_CollectionType")))
#define CreateMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType (Dc1Factory::CreateObject(TypeOfMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType))
class Mp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType;
class IMp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionType > Mp7JrsMediaQualityType_PerceptibleDefects_VisualDefects_CollectionPtr;

#define TypeOfMediaQualityType_QualityRating_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_QualityRating_CollectionType")))
#define CreateMediaQualityType_QualityRating_CollectionType (Dc1Factory::CreateObject(TypeOfMediaQualityType_QualityRating_CollectionType))
class Mp7JrsMediaQualityType_QualityRating_CollectionType;
class IMp7JrsMediaQualityType_QualityRating_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_QualityRating_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_QualityRating_CollectionType > Mp7JrsMediaQualityType_QualityRating_CollectionPtr;

#define TypeOfMediaQualityType_QualityRating_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_QualityRating_LocalType")))
#define CreateMediaQualityType_QualityRating_LocalType (Dc1Factory::CreateObject(TypeOfMediaQualityType_QualityRating_LocalType))
class Mp7JrsMediaQualityType_QualityRating_LocalType;
class IMp7JrsMediaQualityType_QualityRating_LocalType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_QualityRating_LocalType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_QualityRating_LocalType > Mp7JrsMediaQualityType_QualityRating_LocalPtr;

#define TypeOfMediaQualityType_QualityRating_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_QualityRating_type_LocalType")))
#define CreateMediaQualityType_QualityRating_type_LocalType (Dc1Factory::CreateObject(TypeOfMediaQualityType_QualityRating_type_LocalType))
class Mp7JrsMediaQualityType_QualityRating_type_LocalType;
class IMp7JrsMediaQualityType_QualityRating_type_LocalType;
// No smart pointer instance for IMp7JrsMediaQualityType_QualityRating_type_LocalType

#define TypeOfMediaQualityType_RatingInformationLocator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_RatingInformationLocator_CollectionType")))
#define CreateMediaQualityType_RatingInformationLocator_CollectionType (Dc1Factory::CreateObject(TypeOfMediaQualityType_RatingInformationLocator_CollectionType))
class Mp7JrsMediaQualityType_RatingInformationLocator_CollectionType;
class IMp7JrsMediaQualityType_RatingInformationLocator_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaQualityType_RatingInformationLocator_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaQualityType_RatingInformationLocator_CollectionType > Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr;

#define TypeOfMediaRelIncrTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))
#define CreateMediaRelIncrTimePointType (Dc1Factory::CreateObject(TypeOfMediaRelIncrTimePointType))
class Mp7JrsMediaRelIncrTimePointType;
class IMp7JrsMediaRelIncrTimePointType;
/** Smart pointer for instance of IMp7JrsMediaRelIncrTimePointType */
typedef Dc1Ptr< IMp7JrsMediaRelIncrTimePointType > Mp7JrsMediaRelIncrTimePointPtr;

#define TypeOfMediaRelTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))
#define CreateMediaRelTimePointType (Dc1Factory::CreateObject(TypeOfMediaRelTimePointType))
class Mp7JrsMediaRelTimePointType;
class IMp7JrsMediaRelTimePointType;
/** Smart pointer for instance of IMp7JrsMediaRelTimePointType */
typedef Dc1Ptr< IMp7JrsMediaRelTimePointType > Mp7JrsMediaRelTimePointPtr;

#define TypeOfMediaReviewDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewDescriptionType")))
#define CreateMediaReviewDescriptionType (Dc1Factory::CreateObject(TypeOfMediaReviewDescriptionType))
class Mp7JrsMediaReviewDescriptionType;
class IMp7JrsMediaReviewDescriptionType;
/** Smart pointer for instance of IMp7JrsMediaReviewDescriptionType */
typedef Dc1Ptr< IMp7JrsMediaReviewDescriptionType > Mp7JrsMediaReviewDescriptionPtr;

#define TypeOfMediaReviewDescriptionType_FreeTextReview_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewDescriptionType_FreeTextReview_CollectionType")))
#define CreateMediaReviewDescriptionType_FreeTextReview_CollectionType (Dc1Factory::CreateObject(TypeOfMediaReviewDescriptionType_FreeTextReview_CollectionType))
class Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType;
class IMp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType > Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr;

#define TypeOfMediaReviewDescriptionType_MediaRating_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewDescriptionType_MediaRating_CollectionType")))
#define CreateMediaReviewDescriptionType_MediaRating_CollectionType (Dc1Factory::CreateObject(TypeOfMediaReviewDescriptionType_MediaRating_CollectionType))
class Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionType;
class IMp7JrsMediaReviewDescriptionType_MediaRating_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaReviewDescriptionType_MediaRating_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaReviewDescriptionType_MediaRating_CollectionType > Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr;

#define TypeOfMediaReviewDescriptionType_QualityRating_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewDescriptionType_QualityRating_CollectionType")))
#define CreateMediaReviewDescriptionType_QualityRating_CollectionType (Dc1Factory::CreateObject(TypeOfMediaReviewDescriptionType_QualityRating_CollectionType))
class Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionType;
class IMp7JrsMediaReviewDescriptionType_QualityRating_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaReviewDescriptionType_QualityRating_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaReviewDescriptionType_QualityRating_CollectionType > Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr;

#define TypeOfMediaReviewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewType")))
#define CreateMediaReviewType (Dc1Factory::CreateObject(TypeOfMediaReviewType))
class Mp7JrsMediaReviewType;
class IMp7JrsMediaReviewType;
/** Smart pointer for instance of IMp7JrsMediaReviewType */
typedef Dc1Ptr< IMp7JrsMediaReviewType > Mp7JrsMediaReviewPtr;

#define TypeOfMediaReviewType_FreeTextReview_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewType_FreeTextReview_CollectionType")))
#define CreateMediaReviewType_FreeTextReview_CollectionType (Dc1Factory::CreateObject(TypeOfMediaReviewType_FreeTextReview_CollectionType))
class Mp7JrsMediaReviewType_FreeTextReview_CollectionType;
class IMp7JrsMediaReviewType_FreeTextReview_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaReviewType_FreeTextReview_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaReviewType_FreeTextReview_CollectionType > Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr;

#define TypeOfMediaSourceSegmentDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaSourceSegmentDecompositionType")))
#define CreateMediaSourceSegmentDecompositionType (Dc1Factory::CreateObject(TypeOfMediaSourceSegmentDecompositionType))
class Mp7JrsMediaSourceSegmentDecompositionType;
class IMp7JrsMediaSourceSegmentDecompositionType;
/** Smart pointer for instance of IMp7JrsMediaSourceSegmentDecompositionType */
typedef Dc1Ptr< IMp7JrsMediaSourceSegmentDecompositionType > Mp7JrsMediaSourceSegmentDecompositionPtr;

#define TypeOfMediaSpaceMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaSpaceMaskType")))
#define CreateMediaSpaceMaskType (Dc1Factory::CreateObject(TypeOfMediaSpaceMaskType))
class Mp7JrsMediaSpaceMaskType;
class IMp7JrsMediaSpaceMaskType;
/** Smart pointer for instance of IMp7JrsMediaSpaceMaskType */
typedef Dc1Ptr< IMp7JrsMediaSpaceMaskType > Mp7JrsMediaSpaceMaskPtr;

#define TypeOfMediaSpaceMaskType_SubInterval_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaSpaceMaskType_SubInterval_CollectionType")))
#define CreateMediaSpaceMaskType_SubInterval_CollectionType (Dc1Factory::CreateObject(TypeOfMediaSpaceMaskType_SubInterval_CollectionType))
class Mp7JrsMediaSpaceMaskType_SubInterval_CollectionType;
class IMp7JrsMediaSpaceMaskType_SubInterval_CollectionType;
/** Smart pointer for instance of IMp7JrsMediaSpaceMaskType_SubInterval_CollectionType */
typedef Dc1Ptr< IMp7JrsMediaSpaceMaskType_SubInterval_CollectionType > Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr;

#define TypeOfMediaSpaceMaskType_SubInterval_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaSpaceMaskType_SubInterval_LocalType")))
#define CreateMediaSpaceMaskType_SubInterval_LocalType (Dc1Factory::CreateObject(TypeOfMediaSpaceMaskType_SubInterval_LocalType))
class Mp7JrsMediaSpaceMaskType_SubInterval_LocalType;
class IMp7JrsMediaSpaceMaskType_SubInterval_LocalType;
/** Smart pointer for instance of IMp7JrsMediaSpaceMaskType_SubInterval_LocalType */
typedef Dc1Ptr< IMp7JrsMediaSpaceMaskType_SubInterval_LocalType > Mp7JrsMediaSpaceMaskType_SubInterval_LocalPtr;

#define TypeOfmediaTimeOffsetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimeOffsetType")))
#define CreatemediaTimeOffsetType (Dc1Factory::CreateObject(TypeOfmediaTimeOffsetType))
class Mp7JrsmediaTimeOffsetType;
class IMp7JrsmediaTimeOffsetType;
/** Smart pointer for instance of IMp7JrsmediaTimeOffsetType */
typedef Dc1Ptr< IMp7JrsmediaTimeOffsetType > Mp7JrsmediaTimeOffsetPtr;

#define TypeOfmediaTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))
#define CreatemediaTimePointType (Dc1Factory::CreateObject(TypeOfmediaTimePointType))
class Mp7JrsmediaTimePointType;
class IMp7JrsmediaTimePointType;
/** Smart pointer for instance of IMp7JrsmediaTimePointType */
typedef Dc1Ptr< IMp7JrsmediaTimePointType > Mp7JrsmediaTimePointPtr;

#define TypeOfMediaTimeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))
#define CreateMediaTimeType (Dc1Factory::CreateObject(TypeOfMediaTimeType))
class Mp7JrsMediaTimeType;
class IMp7JrsMediaTimeType;
/** Smart pointer for instance of IMp7JrsMediaTimeType */
typedef Dc1Ptr< IMp7JrsMediaTimeType > Mp7JrsMediaTimePtr;

#define TypeOfMediaTranscodingHintsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType")))
#define CreateMediaTranscodingHintsType (Dc1Factory::CreateObject(TypeOfMediaTranscodingHintsType))
class Mp7JrsMediaTranscodingHintsType;
class IMp7JrsMediaTranscodingHintsType;
/** Smart pointer for instance of IMp7JrsMediaTranscodingHintsType */
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType > Mp7JrsMediaTranscodingHintsPtr;

#define TypeOfMediaTranscodingHintsType_CodingHints_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_CodingHints_LocalType")))
#define CreateMediaTranscodingHintsType_CodingHints_LocalType (Dc1Factory::CreateObject(TypeOfMediaTranscodingHintsType_CodingHints_LocalType))
class Mp7JrsMediaTranscodingHintsType_CodingHints_LocalType;
class IMp7JrsMediaTranscodingHintsType_CodingHints_LocalType;
/** Smart pointer for instance of IMp7JrsMediaTranscodingHintsType_CodingHints_LocalType */
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType_CodingHints_LocalType > Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr;

#define TypeOfMediaTranscodingHintsType_MotionHint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_LocalType")))
#define CreateMediaTranscodingHintsType_MotionHint_LocalType (Dc1Factory::CreateObject(TypeOfMediaTranscodingHintsType_MotionHint_LocalType))
class Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType;
class IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType;
/** Smart pointer for instance of IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType */
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType > Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr;

#define TypeOfMediaTranscodingHintsType_MotionHint_MotionRange_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_MotionRange_LocalType")))
#define CreateMediaTranscodingHintsType_MotionHint_MotionRange_LocalType (Dc1Factory::CreateObject(TypeOfMediaTranscodingHintsType_MotionHint_MotionRange_LocalType))
class Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType;
class IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType;
/** Smart pointer for instance of IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType */
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType > Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr;

#define TypeOfMediaTranscodingHintsType_ShapeHint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_ShapeHint_LocalType")))
#define CreateMediaTranscodingHintsType_ShapeHint_LocalType (Dc1Factory::CreateObject(TypeOfMediaTranscodingHintsType_ShapeHint_LocalType))
class Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType;
class IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType;
/** Smart pointer for instance of IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType */
typedef Dc1Ptr< IMp7JrsMediaTranscodingHintsType_ShapeHint_LocalType > Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr;

#define TypeOfMelodyContourType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodyContourType")))
#define CreateMelodyContourType (Dc1Factory::CreateObject(TypeOfMelodyContourType))
class Mp7JrsMelodyContourType;
class IMp7JrsMelodyContourType;
/** Smart pointer for instance of IMp7JrsMelodyContourType */
typedef Dc1Ptr< IMp7JrsMelodyContourType > Mp7JrsMelodyContourPtr;

#define TypeOfMelodySequenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType")))
#define CreateMelodySequenceType (Dc1Factory::CreateObject(TypeOfMelodySequenceType))
class Mp7JrsMelodySequenceType;
class IMp7JrsMelodySequenceType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType > Mp7JrsMelodySequencePtr;

#define TypeOfMelodySequenceType_NoteArray_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_CollectionType")))
#define CreateMelodySequenceType_NoteArray_CollectionType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_NoteArray_CollectionType))
class Mp7JrsMelodySequenceType_NoteArray_CollectionType;
class IMp7JrsMelodySequenceType_NoteArray_CollectionType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_NoteArray_CollectionType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_NoteArray_CollectionType > Mp7JrsMelodySequenceType_NoteArray_CollectionPtr;

#define TypeOfMelodySequenceType_NoteArray_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_LocalType")))
#define CreateMelodySequenceType_NoteArray_LocalType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_NoteArray_LocalType))
class Mp7JrsMelodySequenceType_NoteArray_LocalType;
class IMp7JrsMelodySequenceType_NoteArray_LocalType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_NoteArray_LocalType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_NoteArray_LocalType > Mp7JrsMelodySequenceType_NoteArray_LocalPtr;

#define TypeOfMelodySequenceType_NoteArray_Note_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_Note_CollectionType")))
#define CreateMelodySequenceType_NoteArray_Note_CollectionType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_NoteArray_Note_CollectionType))
class Mp7JrsMelodySequenceType_NoteArray_Note_CollectionType;
class IMp7JrsMelodySequenceType_NoteArray_Note_CollectionType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_NoteArray_Note_CollectionType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_NoteArray_Note_CollectionType > Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr;

#define TypeOfMelodySequenceType_NoteArray_Note_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_Note_LocalType")))
#define CreateMelodySequenceType_NoteArray_Note_LocalType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_NoteArray_Note_LocalType))
class Mp7JrsMelodySequenceType_NoteArray_Note_LocalType;
class IMp7JrsMelodySequenceType_NoteArray_Note_LocalType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_NoteArray_Note_LocalType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_NoteArray_Note_LocalType > Mp7JrsMelodySequenceType_NoteArray_Note_LocalPtr;

#define TypeOfMelodySequenceType_StartingNote_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_LocalType")))
#define CreateMelodySequenceType_StartingNote_LocalType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_StartingNote_LocalType))
class Mp7JrsMelodySequenceType_StartingNote_LocalType;
class IMp7JrsMelodySequenceType_StartingNote_LocalType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_StartingNote_LocalType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_StartingNote_LocalType > Mp7JrsMelodySequenceType_StartingNote_LocalPtr;

#define TypeOfMelodySequenceType_StartingNote_StartingPitch_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_StartingPitch_LocalType")))
#define CreateMelodySequenceType_StartingNote_StartingPitch_LocalType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_StartingNote_StartingPitch_LocalType))
class Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType;
class IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType > Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalPtr;

#define TypeOfMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType")))
#define CreateMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType (Dc1Factory::CreateObject(TypeOfMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType))
class Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType;
class IMp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType;
/** Smart pointer for instance of IMp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType */
typedef Dc1Ptr< IMp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType > Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr;

#define TypeOfMelodyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodyType")))
#define CreateMelodyType (Dc1Factory::CreateObject(TypeOfMelodyType))
class Mp7JrsMelodyType;
class IMp7JrsMelodyType;
/** Smart pointer for instance of IMp7JrsMelodyType */
typedef Dc1Ptr< IMp7JrsMelodyType > Mp7JrsMelodyPtr;

#define TypeOfMelodyType_MelodySequence_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodyType_MelodySequence_CollectionType")))
#define CreateMelodyType_MelodySequence_CollectionType (Dc1Factory::CreateObject(TypeOfMelodyType_MelodySequence_CollectionType))
class Mp7JrsMelodyType_MelodySequence_CollectionType;
class IMp7JrsMelodyType_MelodySequence_CollectionType;
/** Smart pointer for instance of IMp7JrsMelodyType_MelodySequence_CollectionType */
typedef Dc1Ptr< IMp7JrsMelodyType_MelodySequence_CollectionType > Mp7JrsMelodyType_MelodySequence_CollectionPtr;

#define TypeOfMeterType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MeterType")))
#define CreateMeterType (Dc1Factory::CreateObject(TypeOfMeterType))
class Mp7JrsMeterType;
class IMp7JrsMeterType;
/** Smart pointer for instance of IMp7JrsMeterType */
typedef Dc1Ptr< IMp7JrsMeterType > Mp7JrsMeterPtr;

#define TypeOfMeterType_Denominator_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MeterType_Denominator_LocalType")))
#define CreateMeterType_Denominator_LocalType (Dc1Factory::CreateObject(TypeOfMeterType_Denominator_LocalType))
class Mp7JrsMeterType_Denominator_LocalType;
class IMp7JrsMeterType_Denominator_LocalType;
// No smart pointer instance for IMp7JrsMeterType_Denominator_LocalType

#define TypeOfMeterType_Numerator_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MeterType_Numerator_LocalType")))
#define CreateMeterType_Numerator_LocalType (Dc1Factory::CreateObject(TypeOfMeterType_Numerator_LocalType))
class Mp7JrsMeterType_Numerator_LocalType;
class IMp7JrsMeterType_Numerator_LocalType;
/** Smart pointer for instance of IMp7JrsMeterType_Numerator_LocalType */
typedef Dc1Ptr< IMp7JrsMeterType_Numerator_LocalType > Mp7JrsMeterType_Numerator_LocalPtr;

#define TypeOfmimeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mimeType")))
#define CreatemimeType (Dc1Factory::CreateObject(TypeOfmimeType))
class Mp7JrsmimeType;
class IMp7JrsmimeType;
/** Smart pointer for instance of IMp7JrsmimeType */
typedef Dc1Ptr< IMp7JrsmimeType > Mp7JrsmimePtr;

#define TypeOfMinusOneToOneMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MinusOneToOneMatrixType")))
#define CreateMinusOneToOneMatrixType (Dc1Factory::CreateObject(TypeOfMinusOneToOneMatrixType))
class Mp7JrsMinusOneToOneMatrixType;
class IMp7JrsMinusOneToOneMatrixType;
/** Smart pointer for instance of IMp7JrsMinusOneToOneMatrixType */
typedef Dc1Ptr< IMp7JrsMinusOneToOneMatrixType > Mp7JrsMinusOneToOneMatrixPtr;

#define TypeOfminusOneToOneType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:minusOneToOneType")))
#define CreateminusOneToOneType (Dc1Factory::CreateObject(TypeOfminusOneToOneType))
class Mp7JrsminusOneToOneType;
class IMp7JrsminusOneToOneType;
/** Smart pointer for instance of IMp7JrsminusOneToOneType */
typedef Dc1Ptr< IMp7JrsminusOneToOneType > Mp7JrsminusOneToOnePtr;

#define TypeOfminusOneToOneVector (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:minusOneToOneVector")))
#define CreateminusOneToOneVector (Dc1Factory::CreateObject(TypeOfminusOneToOneVector))
class Mp7JrsminusOneToOneVector;
class IMp7JrsminusOneToOneVector;
/** Smart pointer for instance of IMp7JrsminusOneToOneVector */
typedef Dc1Ptr< IMp7JrsminusOneToOneVector > Mp7JrsminusOneToOneVectorPtr;

#define TypeOfminusOneToOneVector_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:minusOneToOneVector_LocalType")))
#define CreateminusOneToOneVector_LocalType (Dc1Factory::CreateObject(TypeOfminusOneToOneVector_LocalType))
class Mp7JrsminusOneToOneVector_LocalType;
class IMp7JrsminusOneToOneVector_LocalType;
/** Smart pointer for instance of IMp7JrsminusOneToOneVector_LocalType */
typedef Dc1Ptr< IMp7JrsminusOneToOneVector_LocalType > Mp7JrsminusOneToOneVector_LocalPtr;

#define TypeOfMixedCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType")))
#define CreateMixedCollectionType (Dc1Factory::CreateObject(TypeOfMixedCollectionType))
class Mp7JrsMixedCollectionType;
class IMp7JrsMixedCollectionType;
/** Smart pointer for instance of IMp7JrsMixedCollectionType */
typedef Dc1Ptr< IMp7JrsMixedCollectionType > Mp7JrsMixedCollectionPtr;

#define TypeOfMixedCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_CollectionType")))
#define CreateMixedCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfMixedCollectionType_CollectionType))
class Mp7JrsMixedCollectionType_CollectionType;
class IMp7JrsMixedCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_CollectionType > Mp7JrsMixedCollectionType_CollectionPtr;

#define TypeOfMixedCollectionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_CollectionType0")))
#define CreateMixedCollectionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_CollectionType0))
class Mp7JrsMixedCollectionType_CollectionType0;
class IMp7JrsMixedCollectionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_CollectionType0 > Mp7JrsMixedCollectionType_Collection0Ptr;

#define TypeOfMixedCollectionType_CollectionType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_CollectionType1")))
#define CreateMixedCollectionType_CollectionType1 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_CollectionType1))
class Mp7JrsMixedCollectionType_CollectionType1;
class IMp7JrsMixedCollectionType_CollectionType1;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_CollectionType1 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_CollectionType1 > Mp7JrsMixedCollectionType_Collection1Ptr;

#define TypeOfMixedCollectionType_CollectionType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_CollectionType2")))
#define CreateMixedCollectionType_CollectionType2 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_CollectionType2))
class Mp7JrsMixedCollectionType_CollectionType2;
class IMp7JrsMixedCollectionType_CollectionType2;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_CollectionType2 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_CollectionType2 > Mp7JrsMixedCollectionType_Collection2Ptr;

#define TypeOfMixedCollectionType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_Descriptor_CollectionType")))
#define CreateMixedCollectionType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfMixedCollectionType_Descriptor_CollectionType))
class Mp7JrsMixedCollectionType_Descriptor_CollectionType;
class IMp7JrsMixedCollectionType_Descriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_Descriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_Descriptor_CollectionType > Mp7JrsMixedCollectionType_Descriptor_CollectionPtr;

#define TypeOfMixedCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_LocalType")))
#define CreateMixedCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfMixedCollectionType_LocalType))
class Mp7JrsMixedCollectionType_LocalType;
class IMp7JrsMixedCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_LocalType > Mp7JrsMixedCollectionType_LocalPtr;

#define TypeOfMixedCollectionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_LocalType0")))
#define CreateMixedCollectionType_LocalType0 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_LocalType0))
class Mp7JrsMixedCollectionType_LocalType0;
class IMp7JrsMixedCollectionType_LocalType0;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_LocalType0 > Mp7JrsMixedCollectionType_Local0Ptr;

#define TypeOfMixedCollectionType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_LocalType1")))
#define CreateMixedCollectionType_LocalType1 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_LocalType1))
class Mp7JrsMixedCollectionType_LocalType1;
class IMp7JrsMixedCollectionType_LocalType1;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_LocalType1 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_LocalType1 > Mp7JrsMixedCollectionType_Local1Ptr;

#define TypeOfMixedCollectionType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType_LocalType2")))
#define CreateMixedCollectionType_LocalType2 (Dc1Factory::CreateObject(TypeOfMixedCollectionType_LocalType2))
class Mp7JrsMixedCollectionType_LocalType2;
class IMp7JrsMixedCollectionType_LocalType2;
/** Smart pointer for instance of IMp7JrsMixedCollectionType_LocalType2 */
typedef Dc1Ptr< IMp7JrsMixedCollectionType_LocalType2 > Mp7JrsMixedCollectionType_Local2Ptr;

#define TypeOfMixtureAmountOfMotionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixtureAmountOfMotionType")))
#define CreateMixtureAmountOfMotionType (Dc1Factory::CreateObject(TypeOfMixtureAmountOfMotionType))
class Mp7JrsMixtureAmountOfMotionType;
class IMp7JrsMixtureAmountOfMotionType;
/** Smart pointer for instance of IMp7JrsMixtureAmountOfMotionType */
typedef Dc1Ptr< IMp7JrsMixtureAmountOfMotionType > Mp7JrsMixtureAmountOfMotionPtr;

#define TypeOfMixtureCameraMotionSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixtureCameraMotionSegmentType")))
#define CreateMixtureCameraMotionSegmentType (Dc1Factory::CreateObject(TypeOfMixtureCameraMotionSegmentType))
class Mp7JrsMixtureCameraMotionSegmentType;
class IMp7JrsMixtureCameraMotionSegmentType;
/** Smart pointer for instance of IMp7JrsMixtureCameraMotionSegmentType */
typedef Dc1Ptr< IMp7JrsMixtureCameraMotionSegmentType > Mp7JrsMixtureCameraMotionSegmentPtr;

#define TypeOfModelDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ModelDescriptionType")))
#define CreateModelDescriptionType (Dc1Factory::CreateObject(TypeOfModelDescriptionType))
class Mp7JrsModelDescriptionType;
class IMp7JrsModelDescriptionType;
/** Smart pointer for instance of IMp7JrsModelDescriptionType */
typedef Dc1Ptr< IMp7JrsModelDescriptionType > Mp7JrsModelDescriptionPtr;

#define TypeOfModelDescriptionType_Model_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ModelDescriptionType_Model_CollectionType")))
#define CreateModelDescriptionType_Model_CollectionType (Dc1Factory::CreateObject(TypeOfModelDescriptionType_Model_CollectionType))
class Mp7JrsModelDescriptionType_Model_CollectionType;
class IMp7JrsModelDescriptionType_Model_CollectionType;
/** Smart pointer for instance of IMp7JrsModelDescriptionType_Model_CollectionType */
typedef Dc1Ptr< IMp7JrsModelDescriptionType_Model_CollectionType > Mp7JrsModelDescriptionType_Model_CollectionPtr;

#define TypeOfModelStateType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ModelStateType")))
#define CreateModelStateType (Dc1Factory::CreateObject(TypeOfModelStateType))
class Mp7JrsModelStateType;
class IMp7JrsModelStateType;
/** Smart pointer for instance of IMp7JrsModelStateType */
typedef Dc1Ptr< IMp7JrsModelStateType > Mp7JrsModelStatePtr;

#define TypeOfModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ModelType")))
#define CreateModelType (Dc1Factory::CreateObject(TypeOfModelType))
class Mp7JrsModelType;
class IMp7JrsModelType;
/** Smart pointer for instance of IMp7JrsModelType */
typedef Dc1Ptr< IMp7JrsModelType > Mp7JrsModelPtr;

#define TypeOfMorphismGraphType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MorphismGraphType")))
#define CreateMorphismGraphType (Dc1Factory::CreateObject(TypeOfMorphismGraphType))
class Mp7JrsMorphismGraphType;
class IMp7JrsMorphismGraphType;
/** Smart pointer for instance of IMp7JrsMorphismGraphType */
typedef Dc1Ptr< IMp7JrsMorphismGraphType > Mp7JrsMorphismGraphPtr;

#define TypeOfMosaicType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType")))
#define CreateMosaicType (Dc1Factory::CreateObject(TypeOfMosaicType))
class Mp7JrsMosaicType;
class IMp7JrsMosaicType;
/** Smart pointer for instance of IMp7JrsMosaicType */
typedef Dc1Ptr< IMp7JrsMosaicType > Mp7JrsMosaicPtr;

#define TypeOfMosaicType_WarpingParam_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_CollectionType")))
#define CreateMosaicType_WarpingParam_CollectionType (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_CollectionType))
class Mp7JrsMosaicType_WarpingParam_CollectionType;
class IMp7JrsMosaicType_WarpingParam_CollectionType;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_CollectionType */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_CollectionType > Mp7JrsMosaicType_WarpingParam_CollectionPtr;

#define TypeOfMosaicType_WarpingParam_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_LocalType")))
#define CreateMosaicType_WarpingParam_LocalType (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_LocalType))
class Mp7JrsMosaicType_WarpingParam_LocalType;
class IMp7JrsMosaicType_WarpingParam_LocalType;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_LocalType */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_LocalType > Mp7JrsMosaicType_WarpingParam_LocalPtr;

#define TypeOfMosaicType_WarpingParam_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_LocalType0")))
#define CreateMosaicType_WarpingParam_LocalType0 (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_LocalType0))
class Mp7JrsMosaicType_WarpingParam_LocalType0;
class IMp7JrsMosaicType_WarpingParam_LocalType0;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_LocalType0 */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_LocalType0 > Mp7JrsMosaicType_WarpingParam_Local0Ptr;

#define TypeOfMosaicType_WarpingParam_modelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_modelType_LocalType")))
#define CreateMosaicType_WarpingParam_modelType_LocalType (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_modelType_LocalType))
class Mp7JrsMosaicType_WarpingParam_modelType_LocalType;
class IMp7JrsMosaicType_WarpingParam_modelType_LocalType;
// No smart pointer instance for IMp7JrsMosaicType_WarpingParam_modelType_LocalType

#define TypeOfMosaicType_WarpingParam_modelType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_modelType_LocalType0")))
#define CreateMosaicType_WarpingParam_modelType_LocalType0 (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_modelType_LocalType0))
class Mp7JrsMosaicType_WarpingParam_modelType_LocalType0;
class IMp7JrsMosaicType_WarpingParam_modelType_LocalType0;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_modelType_LocalType0 */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_modelType_LocalType0 > Mp7JrsMosaicType_WarpingParam_modelType_Local0Ptr;

#define TypeOfMosaicType_WarpingParam_modelType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_modelType_LocalType1")))
#define CreateMosaicType_WarpingParam_modelType_LocalType1 (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_modelType_LocalType1))
class Mp7JrsMosaicType_WarpingParam_modelType_LocalType1;
class IMp7JrsMosaicType_WarpingParam_modelType_LocalType1;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_modelType_LocalType1 */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_modelType_LocalType1 > Mp7JrsMosaicType_WarpingParam_modelType_Local1Ptr;

#define TypeOfMosaicType_WarpingParam_modelType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MosaicType_WarpingParam_modelType_LocalType2")))
#define CreateMosaicType_WarpingParam_modelType_LocalType2 (Dc1Factory::CreateObject(TypeOfMosaicType_WarpingParam_modelType_LocalType2))
class Mp7JrsMosaicType_WarpingParam_modelType_LocalType2;
class IMp7JrsMosaicType_WarpingParam_modelType_LocalType2;
/** Smart pointer for instance of IMp7JrsMosaicType_WarpingParam_modelType_LocalType2 */
typedef Dc1Ptr< IMp7JrsMosaicType_WarpingParam_modelType_LocalType2 > Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr;

#define TypeOfMotionActivityType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType")))
#define CreateMotionActivityType (Dc1Factory::CreateObject(TypeOfMotionActivityType))
class Mp7JrsMotionActivityType;
class IMp7JrsMotionActivityType;
/** Smart pointer for instance of IMp7JrsMotionActivityType */
typedef Dc1Ptr< IMp7JrsMotionActivityType > Mp7JrsMotionActivityPtr;

#define TypeOfMotionActivityType_Intensity_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_Intensity_LocalType")))
#define CreateMotionActivityType_Intensity_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_Intensity_LocalType))
class Mp7JrsMotionActivityType_Intensity_LocalType;
class IMp7JrsMotionActivityType_Intensity_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_Intensity_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_Intensity_LocalType > Mp7JrsMotionActivityType_Intensity_LocalPtr;

#define TypeOfMotionActivityType_SpatialDistributionParams_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialDistributionParams_LocalType")))
#define CreateMotionActivityType_SpatialDistributionParams_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialDistributionParams_LocalType))
class Mp7JrsMotionActivityType_SpatialDistributionParams_LocalType;
class IMp7JrsMotionActivityType_SpatialDistributionParams_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialDistributionParams_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialDistributionParams_LocalType > Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_LocalType")))
#define CreateMotionActivityType_SpatialLocalizationParams_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_LocalType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType > Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector16_CollectionType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_CollectionPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector16_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector16_LocalType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector16_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector16_LocalType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector256_CollectionType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_CollectionPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector256_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector256_LocalType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector256_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector256_LocalType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_CollectionType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_CollectionPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector4_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector4_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector4_LocalType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector64_CollectionType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_CollectionPtr;

#define TypeOfMotionActivityType_SpatialLocalizationParams_Vector64_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector64_LocalType")))
#define CreateMotionActivityType_SpatialLocalizationParams_Vector64_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_SpatialLocalizationParams_Vector64_LocalType))
class Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalType;
class IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalType > Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr;

#define TypeOfMotionActivityType_TemporalParams_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_TemporalParams_CollectionType")))
#define CreateMotionActivityType_TemporalParams_CollectionType (Dc1Factory::CreateObject(TypeOfMotionActivityType_TemporalParams_CollectionType))
class Mp7JrsMotionActivityType_TemporalParams_CollectionType;
class IMp7JrsMotionActivityType_TemporalParams_CollectionType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_TemporalParams_CollectionType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_TemporalParams_CollectionType > Mp7JrsMotionActivityType_TemporalParams_CollectionPtr;

#define TypeOfMotionActivityType_TemporalParams_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_TemporalParams_LocalType")))
#define CreateMotionActivityType_TemporalParams_LocalType (Dc1Factory::CreateObject(TypeOfMotionActivityType_TemporalParams_LocalType))
class Mp7JrsMotionActivityType_TemporalParams_LocalType;
class IMp7JrsMotionActivityType_TemporalParams_LocalType;
/** Smart pointer for instance of IMp7JrsMotionActivityType_TemporalParams_LocalType */
typedef Dc1Ptr< IMp7JrsMotionActivityType_TemporalParams_LocalType > Mp7JrsMotionActivityType_TemporalParams_LocalPtr;

#define TypeOfMotionTrajectoryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType")))
#define CreateMotionTrajectoryType (Dc1Factory::CreateObject(TypeOfMotionTrajectoryType))
class Mp7JrsMotionTrajectoryType;
class IMp7JrsMotionTrajectoryType;
/** Smart pointer for instance of IMp7JrsMotionTrajectoryType */
typedef Dc1Ptr< IMp7JrsMotionTrajectoryType > Mp7JrsMotionTrajectoryPtr;

#define TypeOfMotionTrajectoryType_CoordDef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordDef_LocalType")))
#define CreateMotionTrajectoryType_CoordDef_LocalType (Dc1Factory::CreateObject(TypeOfMotionTrajectoryType_CoordDef_LocalType))
class Mp7JrsMotionTrajectoryType_CoordDef_LocalType;
class IMp7JrsMotionTrajectoryType_CoordDef_LocalType;
/** Smart pointer for instance of IMp7JrsMotionTrajectoryType_CoordDef_LocalType */
typedef Dc1Ptr< IMp7JrsMotionTrajectoryType_CoordDef_LocalType > Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr;

#define TypeOfMotionTrajectoryType_CoordDef_Repr_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordDef_Repr_LocalType")))
#define CreateMotionTrajectoryType_CoordDef_Repr_LocalType (Dc1Factory::CreateObject(TypeOfMotionTrajectoryType_CoordDef_Repr_LocalType))
class Mp7JrsMotionTrajectoryType_CoordDef_Repr_LocalType;
class IMp7JrsMotionTrajectoryType_CoordDef_Repr_LocalType;
/** Smart pointer for instance of IMp7JrsMotionTrajectoryType_CoordDef_Repr_LocalType */
typedef Dc1Ptr< IMp7JrsMotionTrajectoryType_CoordDef_Repr_LocalType > Mp7JrsMotionTrajectoryType_CoordDef_Repr_LocalPtr;

#define TypeOfMotionTrajectoryType_CoordDef_units_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordDef_units_LocalType")))
#define CreateMotionTrajectoryType_CoordDef_units_LocalType (Dc1Factory::CreateObject(TypeOfMotionTrajectoryType_CoordDef_units_LocalType))
class Mp7JrsMotionTrajectoryType_CoordDef_units_LocalType;
class IMp7JrsMotionTrajectoryType_CoordDef_units_LocalType;
// No smart pointer instance for IMp7JrsMotionTrajectoryType_CoordDef_units_LocalType

#define TypeOfMotionTrajectoryType_CoordRef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordRef_LocalType")))
#define CreateMotionTrajectoryType_CoordRef_LocalType (Dc1Factory::CreateObject(TypeOfMotionTrajectoryType_CoordRef_LocalType))
class Mp7JrsMotionTrajectoryType_CoordRef_LocalType;
class IMp7JrsMotionTrajectoryType_CoordRef_LocalType;
/** Smart pointer for instance of IMp7JrsMotionTrajectoryType_CoordRef_LocalType */
typedef Dc1Ptr< IMp7JrsMotionTrajectoryType_CoordRef_LocalType > Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr;

#define TypeOfMovingRegionFeatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionFeatureType")))
#define CreateMovingRegionFeatureType (Dc1Factory::CreateObject(TypeOfMovingRegionFeatureType))
class Mp7JrsMovingRegionFeatureType;
class IMp7JrsMovingRegionFeatureType;
/** Smart pointer for instance of IMp7JrsMovingRegionFeatureType */
typedef Dc1Ptr< IMp7JrsMovingRegionFeatureType > Mp7JrsMovingRegionFeaturePtr;

#define TypeOfMovingRegionMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionMediaSourceDecompositionType")))
#define CreateMovingRegionMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfMovingRegionMediaSourceDecompositionType))
class Mp7JrsMovingRegionMediaSourceDecompositionType;
class IMp7JrsMovingRegionMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsMovingRegionMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsMovingRegionMediaSourceDecompositionType > Mp7JrsMovingRegionMediaSourceDecompositionPtr;

#define TypeOfMovingRegionMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionMediaSourceDecompositionType_CollectionType")))
#define CreateMovingRegionMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfMovingRegionMediaSourceDecompositionType_CollectionType))
class Mp7JrsMovingRegionMediaSourceDecompositionType_CollectionType;
class IMp7JrsMovingRegionMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMovingRegionMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMovingRegionMediaSourceDecompositionType_CollectionType > Mp7JrsMovingRegionMediaSourceDecompositionType_CollectionPtr;

#define TypeOfMovingRegionMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionMediaSourceDecompositionType_LocalType")))
#define CreateMovingRegionMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfMovingRegionMediaSourceDecompositionType_LocalType))
class Mp7JrsMovingRegionMediaSourceDecompositionType_LocalType;
class IMp7JrsMovingRegionMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsMovingRegionMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsMovingRegionMediaSourceDecompositionType_LocalType > Mp7JrsMovingRegionMediaSourceDecompositionType_LocalPtr;

#define TypeOfMovingRegionSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatialDecompositionType")))
#define CreateMovingRegionSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatialDecompositionType))
class Mp7JrsMovingRegionSpatialDecompositionType;
class IMp7JrsMovingRegionSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatialDecompositionType > Mp7JrsMovingRegionSpatialDecompositionPtr;

#define TypeOfMovingRegionSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatialDecompositionType_CollectionType")))
#define CreateMovingRegionSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatialDecompositionType_CollectionType))
class Mp7JrsMovingRegionSpatialDecompositionType_CollectionType;
class IMp7JrsMovingRegionSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatialDecompositionType_CollectionType > Mp7JrsMovingRegionSpatialDecompositionType_CollectionPtr;

#define TypeOfMovingRegionSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatialDecompositionType_LocalType")))
#define CreateMovingRegionSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatialDecompositionType_LocalType))
class Mp7JrsMovingRegionSpatialDecompositionType_LocalType;
class IMp7JrsMovingRegionSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatialDecompositionType_LocalType > Mp7JrsMovingRegionSpatialDecompositionType_LocalPtr;

#define TypeOfMovingRegionSpatioTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatioTemporalDecompositionType")))
#define CreateMovingRegionSpatioTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatioTemporalDecompositionType))
class Mp7JrsMovingRegionSpatioTemporalDecompositionType;
class IMp7JrsMovingRegionSpatioTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatioTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatioTemporalDecompositionType > Mp7JrsMovingRegionSpatioTemporalDecompositionPtr;

#define TypeOfMovingRegionSpatioTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatioTemporalDecompositionType_CollectionType")))
#define CreateMovingRegionSpatioTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatioTemporalDecompositionType_CollectionType))
class Mp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionType;
class IMp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionType > Mp7JrsMovingRegionSpatioTemporalDecompositionType_CollectionPtr;

#define TypeOfMovingRegionSpatioTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatioTemporalDecompositionType_LocalType")))
#define CreateMovingRegionSpatioTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfMovingRegionSpatioTemporalDecompositionType_LocalType))
class Mp7JrsMovingRegionSpatioTemporalDecompositionType_LocalType;
class IMp7JrsMovingRegionSpatioTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsMovingRegionSpatioTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsMovingRegionSpatioTemporalDecompositionType_LocalType > Mp7JrsMovingRegionSpatioTemporalDecompositionType_LocalPtr;

#define TypeOfMovingRegionTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionTemporalDecompositionType")))
#define CreateMovingRegionTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfMovingRegionTemporalDecompositionType))
class Mp7JrsMovingRegionTemporalDecompositionType;
class IMp7JrsMovingRegionTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsMovingRegionTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsMovingRegionTemporalDecompositionType > Mp7JrsMovingRegionTemporalDecompositionPtr;

#define TypeOfMovingRegionTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionTemporalDecompositionType_CollectionType")))
#define CreateMovingRegionTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfMovingRegionTemporalDecompositionType_CollectionType))
class Mp7JrsMovingRegionTemporalDecompositionType_CollectionType;
class IMp7JrsMovingRegionTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMovingRegionTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMovingRegionTemporalDecompositionType_CollectionType > Mp7JrsMovingRegionTemporalDecompositionType_CollectionPtr;

#define TypeOfMovingRegionTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionTemporalDecompositionType_LocalType")))
#define CreateMovingRegionTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfMovingRegionTemporalDecompositionType_LocalType))
class Mp7JrsMovingRegionTemporalDecompositionType_LocalType;
class IMp7JrsMovingRegionTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsMovingRegionTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsMovingRegionTemporalDecompositionType_LocalType > Mp7JrsMovingRegionTemporalDecompositionType_LocalPtr;

#define TypeOfMovingRegionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType")))
#define CreateMovingRegionType (Dc1Factory::CreateObject(TypeOfMovingRegionType))
class Mp7JrsMovingRegionType;
class IMp7JrsMovingRegionType;
/** Smart pointer for instance of IMp7JrsMovingRegionType */
typedef Dc1Ptr< IMp7JrsMovingRegionType > Mp7JrsMovingRegionPtr;

#define TypeOfMovingRegionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType_CollectionType")))
#define CreateMovingRegionType_CollectionType (Dc1Factory::CreateObject(TypeOfMovingRegionType_CollectionType))
class Mp7JrsMovingRegionType_CollectionType;
class IMp7JrsMovingRegionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMovingRegionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMovingRegionType_CollectionType > Mp7JrsMovingRegionType_CollectionPtr;

#define TypeOfMovingRegionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType_CollectionType0")))
#define CreateMovingRegionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfMovingRegionType_CollectionType0))
class Mp7JrsMovingRegionType_CollectionType0;
class IMp7JrsMovingRegionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsMovingRegionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsMovingRegionType_CollectionType0 > Mp7JrsMovingRegionType_Collection0Ptr;

#define TypeOfMovingRegionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType_LocalType")))
#define CreateMovingRegionType_LocalType (Dc1Factory::CreateObject(TypeOfMovingRegionType_LocalType))
class Mp7JrsMovingRegionType_LocalType;
class IMp7JrsMovingRegionType_LocalType;
/** Smart pointer for instance of IMp7JrsMovingRegionType_LocalType */
typedef Dc1Ptr< IMp7JrsMovingRegionType_LocalType > Mp7JrsMovingRegionType_LocalPtr;

#define TypeOfMovingRegionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType_LocalType0")))
#define CreateMovingRegionType_LocalType0 (Dc1Factory::CreateObject(TypeOfMovingRegionType_LocalType0))
class Mp7JrsMovingRegionType_LocalType0;
class IMp7JrsMovingRegionType_LocalType0;
/** Smart pointer for instance of IMp7JrsMovingRegionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsMovingRegionType_LocalType0 > Mp7JrsMovingRegionType_Local0Ptr;

#define TypeOfMpeg7_Description_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Mpeg7_Description_CollectionType")))
#define CreateMpeg7_Description_CollectionType (Dc1Factory::CreateObject(TypeOfMpeg7_Description_CollectionType))
class Mp7JrsMpeg7_Description_CollectionType;
class IMp7JrsMpeg7_Description_CollectionType;
/** Smart pointer for instance of IMp7JrsMpeg7_Description_CollectionType */
typedef Dc1Ptr< IMp7JrsMpeg7_Description_CollectionType > Mp7JrsMpeg7_Description_CollectionPtr;

#define TypeOfMpeg7_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Mpeg7_LocalType")))
#define CreateMpeg7_LocalType (Dc1Factory::CreateObject(TypeOfMpeg7_LocalType))
class Mp7JrsMpeg7_LocalType;
class IMp7JrsMpeg7_LocalType;
/** Smart pointer for instance of IMp7JrsMpeg7_LocalType */
typedef Dc1Ptr< IMp7JrsMpeg7_LocalType > Mp7JrsMpeg7_LocalPtr;

#define TypeOfMpeg7BaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Mpeg7BaseType")))
#define CreateMpeg7BaseType (Dc1Factory::CreateObject(TypeOfMpeg7BaseType))
class Mp7JrsMpeg7BaseType;
class IMp7JrsMpeg7BaseType;
/** Smart pointer for instance of IMp7JrsMpeg7BaseType */
typedef Dc1Ptr< IMp7JrsMpeg7BaseType > Mp7JrsMpeg7BasePtr;

#define TypeOfMpeg7Type (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Mpeg7Type")))
#define CreateMpeg7Type (Dc1Factory::CreateObject(TypeOfMpeg7Type))
class Mp7JrsMpeg7Type;
class IMp7JrsMpeg7Type;
/** Smart pointer for instance of IMp7JrsMpeg7Type */
typedef Dc1Ptr< IMp7JrsMpeg7Type > Mp7JrsMpeg7Ptr;

#define TypeOfMultimediaCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaCollectionType")))
#define CreateMultimediaCollectionType (Dc1Factory::CreateObject(TypeOfMultimediaCollectionType))
class Mp7JrsMultimediaCollectionType;
class IMp7JrsMultimediaCollectionType;
/** Smart pointer for instance of IMp7JrsMultimediaCollectionType */
typedef Dc1Ptr< IMp7JrsMultimediaCollectionType > Mp7JrsMultimediaCollectionPtr;

#define TypeOfMultimediaCollectionType_Collection_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaCollectionType_Collection_CollectionType")))
#define CreateMultimediaCollectionType_Collection_CollectionType (Dc1Factory::CreateObject(TypeOfMultimediaCollectionType_Collection_CollectionType))
class Mp7JrsMultimediaCollectionType_Collection_CollectionType;
class IMp7JrsMultimediaCollectionType_Collection_CollectionType;
/** Smart pointer for instance of IMp7JrsMultimediaCollectionType_Collection_CollectionType */
typedef Dc1Ptr< IMp7JrsMultimediaCollectionType_Collection_CollectionType > Mp7JrsMultimediaCollectionType_Collection_CollectionPtr;

#define TypeOfMultimediaCollectionType_StructuredCollection_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaCollectionType_StructuredCollection_CollectionType")))
#define CreateMultimediaCollectionType_StructuredCollection_CollectionType (Dc1Factory::CreateObject(TypeOfMultimediaCollectionType_StructuredCollection_CollectionType))
class Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionType;
class IMp7JrsMultimediaCollectionType_StructuredCollection_CollectionType;
/** Smart pointer for instance of IMp7JrsMultimediaCollectionType_StructuredCollection_CollectionType */
typedef Dc1Ptr< IMp7JrsMultimediaCollectionType_StructuredCollection_CollectionType > Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr;

#define TypeOfMultimediaContentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaContentType")))
#define CreateMultimediaContentType (Dc1Factory::CreateObject(TypeOfMultimediaContentType))
class Mp7JrsMultimediaContentType;
class IMp7JrsMultimediaContentType;
/** Smart pointer for instance of IMp7JrsMultimediaContentType */
typedef Dc1Ptr< IMp7JrsMultimediaContentType > Mp7JrsMultimediaContentPtr;

#define TypeOfMultimediaSegmentMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaSegmentMediaSourceDecompositionType")))
#define CreateMultimediaSegmentMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfMultimediaSegmentMediaSourceDecompositionType))
class Mp7JrsMultimediaSegmentMediaSourceDecompositionType;
class IMp7JrsMultimediaSegmentMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsMultimediaSegmentMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsMultimediaSegmentMediaSourceDecompositionType > Mp7JrsMultimediaSegmentMediaSourceDecompositionPtr;

#define TypeOfMultimediaSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaSegmentMediaSourceDecompositionType_CollectionType")))
#define CreateMultimediaSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfMultimediaSegmentMediaSourceDecompositionType_CollectionType))
class Mp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionType;
class IMp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionType > Mp7JrsMultimediaSegmentMediaSourceDecompositionType_CollectionPtr;

#define TypeOfMultimediaSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaSegmentMediaSourceDecompositionType_LocalType")))
#define CreateMultimediaSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfMultimediaSegmentMediaSourceDecompositionType_LocalType))
class Mp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalType;
class IMp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalType > Mp7JrsMultimediaSegmentMediaSourceDecompositionType_LocalPtr;

#define TypeOfMultimediaSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaSegmentType")))
#define CreateMultimediaSegmentType (Dc1Factory::CreateObject(TypeOfMultimediaSegmentType))
class Mp7JrsMultimediaSegmentType;
class IMp7JrsMultimediaSegmentType;
/** Smart pointer for instance of IMp7JrsMultimediaSegmentType */
typedef Dc1Ptr< IMp7JrsMultimediaSegmentType > Mp7JrsMultimediaSegmentPtr;

#define TypeOfMultimediaSegmentType_MediaSourceDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaSegmentType_MediaSourceDecomposition_CollectionType")))
#define CreateMultimediaSegmentType_MediaSourceDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfMultimediaSegmentType_MediaSourceDecomposition_CollectionType))
class Mp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionType;
class IMp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionType > Mp7JrsMultimediaSegmentType_MediaSourceDecomposition_CollectionPtr;

#define TypeOfMultimediaType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultimediaType")))
#define CreateMultimediaType (Dc1Factory::CreateObject(TypeOfMultimediaType))
class Mp7JrsMultimediaType;
class IMp7JrsMultimediaType;
/** Smart pointer for instance of IMp7JrsMultimediaType */
typedef Dc1Ptr< IMp7JrsMultimediaType > Mp7JrsMultimediaPtr;

#define TypeOfMultipleViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType")))
#define CreateMultipleViewType (Dc1Factory::CreateObject(TypeOfMultipleViewType))
class Mp7JrsMultipleViewType;
class IMp7JrsMultipleViewType;
/** Smart pointer for instance of IMp7JrsMultipleViewType */
typedef Dc1Ptr< IMp7JrsMultipleViewType > Mp7JrsMultipleViewPtr;

#define TypeOfMultipleViewType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType_CollectionType")))
#define CreateMultipleViewType_CollectionType (Dc1Factory::CreateObject(TypeOfMultipleViewType_CollectionType))
class Mp7JrsMultipleViewType_CollectionType;
class IMp7JrsMultipleViewType_CollectionType;
/** Smart pointer for instance of IMp7JrsMultipleViewType_CollectionType */
typedef Dc1Ptr< IMp7JrsMultipleViewType_CollectionType > Mp7JrsMultipleViewType_CollectionPtr;

#define TypeOfMultipleViewType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType_LocalType")))
#define CreateMultipleViewType_LocalType (Dc1Factory::CreateObject(TypeOfMultipleViewType_LocalType))
class Mp7JrsMultipleViewType_LocalType;
class IMp7JrsMultipleViewType_LocalType;
/** Smart pointer for instance of IMp7JrsMultipleViewType_LocalType */
typedef Dc1Ptr< IMp7JrsMultipleViewType_LocalType > Mp7JrsMultipleViewType_LocalPtr;

#define TypeOfMultiResolutionPyramidType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultiResolutionPyramidType")))
#define CreateMultiResolutionPyramidType (Dc1Factory::CreateObject(TypeOfMultiResolutionPyramidType))
class Mp7JrsMultiResolutionPyramidType;
class IMp7JrsMultiResolutionPyramidType;
/** Smart pointer for instance of IMp7JrsMultiResolutionPyramidType */
typedef Dc1Ptr< IMp7JrsMultiResolutionPyramidType > Mp7JrsMultiResolutionPyramidPtr;

#define TypeOfNameComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))
#define CreateNameComponentType (Dc1Factory::CreateObject(TypeOfNameComponentType))
class Mp7JrsNameComponentType;
class IMp7JrsNameComponentType;
/** Smart pointer for instance of IMp7JrsNameComponentType */
typedef Dc1Ptr< IMp7JrsNameComponentType > Mp7JrsNameComponentPtr;

#define TypeOfNonDependencyStructurePhraseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType")))
#define CreateNonDependencyStructurePhraseType (Dc1Factory::CreateObject(TypeOfNonDependencyStructurePhraseType))
class Mp7JrsNonDependencyStructurePhraseType;
class IMp7JrsNonDependencyStructurePhraseType;
/** Smart pointer for instance of IMp7JrsNonDependencyStructurePhraseType */
typedef Dc1Ptr< IMp7JrsNonDependencyStructurePhraseType > Mp7JrsNonDependencyStructurePhrasePtr;

#define TypeOfNonDependencyStructurePhraseType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType_CollectionType")))
#define CreateNonDependencyStructurePhraseType_CollectionType (Dc1Factory::CreateObject(TypeOfNonDependencyStructurePhraseType_CollectionType))
class Mp7JrsNonDependencyStructurePhraseType_CollectionType;
class IMp7JrsNonDependencyStructurePhraseType_CollectionType;
/** Smart pointer for instance of IMp7JrsNonDependencyStructurePhraseType_CollectionType */
typedef Dc1Ptr< IMp7JrsNonDependencyStructurePhraseType_CollectionType > Mp7JrsNonDependencyStructurePhraseType_CollectionPtr;

#define TypeOfNonDependencyStructurePhraseType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType_LocalType")))
#define CreateNonDependencyStructurePhraseType_LocalType (Dc1Factory::CreateObject(TypeOfNonDependencyStructurePhraseType_LocalType))
class Mp7JrsNonDependencyStructurePhraseType_LocalType;
class IMp7JrsNonDependencyStructurePhraseType_LocalType;
/** Smart pointer for instance of IMp7JrsNonDependencyStructurePhraseType_LocalType */
typedef Dc1Ptr< IMp7JrsNonDependencyStructurePhraseType_LocalType > Mp7JrsNonDependencyStructurePhraseType_LocalPtr;

#define TypeOfNonDependencyStructurePhraseType_synthesis_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType_synthesis_LocalType")))
#define CreateNonDependencyStructurePhraseType_synthesis_LocalType (Dc1Factory::CreateObject(TypeOfNonDependencyStructurePhraseType_synthesis_LocalType))
class Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType;
class IMp7JrsNonDependencyStructurePhraseType_synthesis_LocalType;
// No smart pointer instance for IMp7JrsNonDependencyStructurePhraseType_synthesis_LocalType

#define TypeOfNonMixtureAmountOfMotionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonMixtureAmountOfMotionType")))
#define CreateNonMixtureAmountOfMotionType (Dc1Factory::CreateObject(TypeOfNonMixtureAmountOfMotionType))
class Mp7JrsNonMixtureAmountOfMotionType;
class IMp7JrsNonMixtureAmountOfMotionType;
/** Smart pointer for instance of IMp7JrsNonMixtureAmountOfMotionType */
typedef Dc1Ptr< IMp7JrsNonMixtureAmountOfMotionType > Mp7JrsNonMixtureAmountOfMotionPtr;

#define TypeOfNonMixtureAmountOfMotionType_Fixed_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonMixtureAmountOfMotionType_Fixed_LocalType")))
#define CreateNonMixtureAmountOfMotionType_Fixed_LocalType (Dc1Factory::CreateObject(TypeOfNonMixtureAmountOfMotionType_Fixed_LocalType))
class Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalType;
class IMp7JrsNonMixtureAmountOfMotionType_Fixed_LocalType;
/** Smart pointer for instance of IMp7JrsNonMixtureAmountOfMotionType_Fixed_LocalType */
typedef Dc1Ptr< IMp7JrsNonMixtureAmountOfMotionType_Fixed_LocalType > Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr;

#define TypeOfNonMixtureCameraMotionSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonMixtureCameraMotionSegmentType")))
#define CreateNonMixtureCameraMotionSegmentType (Dc1Factory::CreateObject(TypeOfNonMixtureCameraMotionSegmentType))
class Mp7JrsNonMixtureCameraMotionSegmentType;
class IMp7JrsNonMixtureCameraMotionSegmentType;
/** Smart pointer for instance of IMp7JrsNonMixtureCameraMotionSegmentType */
typedef Dc1Ptr< IMp7JrsNonMixtureCameraMotionSegmentType > Mp7JrsNonMixtureCameraMotionSegmentPtr;

#define TypeOfnonNegativeReal (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:nonNegativeReal")))
#define CreatenonNegativeReal (Dc1Factory::CreateObject(TypeOfnonNegativeReal))
class Mp7JrsnonNegativeReal;
class IMp7JrsnonNegativeReal;
/** Smart pointer for instance of IMp7JrsnonNegativeReal */
typedef Dc1Ptr< IMp7JrsnonNegativeReal > Mp7JrsnonNegativeRealPtr;

#define TypeOfObjectType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ObjectType")))
#define CreateObjectType (Dc1Factory::CreateObject(TypeOfObjectType))
class Mp7JrsObjectType;
class IMp7JrsObjectType;
/** Smart pointer for instance of IMp7JrsObjectType */
typedef Dc1Ptr< IMp7JrsObjectType > Mp7JrsObjectPtr;

#define TypeOfObjectType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ObjectType_CollectionType")))
#define CreateObjectType_CollectionType (Dc1Factory::CreateObject(TypeOfObjectType_CollectionType))
class Mp7JrsObjectType_CollectionType;
class IMp7JrsObjectType_CollectionType;
/** Smart pointer for instance of IMp7JrsObjectType_CollectionType */
typedef Dc1Ptr< IMp7JrsObjectType_CollectionType > Mp7JrsObjectType_CollectionPtr;

#define TypeOfObjectType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ObjectType_LocalType")))
#define CreateObjectType_LocalType (Dc1Factory::CreateObject(TypeOfObjectType_LocalType))
class Mp7JrsObjectType_LocalType;
class IMp7JrsObjectType_LocalType;
/** Smart pointer for instance of IMp7JrsObjectType_LocalType */
typedef Dc1Ptr< IMp7JrsObjectType_LocalType > Mp7JrsObjectType_LocalPtr;

#define TypeOfOrderedGroupDataSetMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderedGroupDataSetMaskType")))
#define CreateOrderedGroupDataSetMaskType (Dc1Factory::CreateObject(TypeOfOrderedGroupDataSetMaskType))
class Mp7JrsOrderedGroupDataSetMaskType;
class IMp7JrsOrderedGroupDataSetMaskType;
/** Smart pointer for instance of IMp7JrsOrderedGroupDataSetMaskType */
typedef Dc1Ptr< IMp7JrsOrderedGroupDataSetMaskType > Mp7JrsOrderedGroupDataSetMaskPtr;

#define TypeOfOrderedGroupDataSetMaskType_SubInterval_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderedGroupDataSetMaskType_SubInterval_CollectionType")))
#define CreateOrderedGroupDataSetMaskType_SubInterval_CollectionType (Dc1Factory::CreateObject(TypeOfOrderedGroupDataSetMaskType_SubInterval_CollectionType))
class Mp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionType;
class IMp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionType;
/** Smart pointer for instance of IMp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionType */
typedef Dc1Ptr< IMp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionType > Mp7JrsOrderedGroupDataSetMaskType_SubInterval_CollectionPtr;

#define TypeOfOrderedGroupDataSetMaskType_SubInterval_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderedGroupDataSetMaskType_SubInterval_LocalType")))
#define CreateOrderedGroupDataSetMaskType_SubInterval_LocalType (Dc1Factory::CreateObject(TypeOfOrderedGroupDataSetMaskType_SubInterval_LocalType))
class Mp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalType;
class IMp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalType;
/** Smart pointer for instance of IMp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalType */
typedef Dc1Ptr< IMp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalType > Mp7JrsOrderedGroupDataSetMaskType_SubInterval_LocalPtr;

#define TypeOfOrderingKeyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType")))
#define CreateOrderingKeyType (Dc1Factory::CreateObject(TypeOfOrderingKeyType))
class Mp7JrsOrderingKeyType;
class IMp7JrsOrderingKeyType;
/** Smart pointer for instance of IMp7JrsOrderingKeyType */
typedef Dc1Ptr< IMp7JrsOrderingKeyType > Mp7JrsOrderingKeyPtr;

#define TypeOfOrderingKeyType_direction_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_direction_LocalType")))
#define CreateOrderingKeyType_direction_LocalType (Dc1Factory::CreateObject(TypeOfOrderingKeyType_direction_LocalType))
class Mp7JrsOrderingKeyType_direction_LocalType;
class IMp7JrsOrderingKeyType_direction_LocalType;
// No smart pointer instance for IMp7JrsOrderingKeyType_direction_LocalType

#define TypeOfOrderingKeyType_Field_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Field_CollectionType")))
#define CreateOrderingKeyType_Field_CollectionType (Dc1Factory::CreateObject(TypeOfOrderingKeyType_Field_CollectionType))
class Mp7JrsOrderingKeyType_Field_CollectionType;
class IMp7JrsOrderingKeyType_Field_CollectionType;
/** Smart pointer for instance of IMp7JrsOrderingKeyType_Field_CollectionType */
typedef Dc1Ptr< IMp7JrsOrderingKeyType_Field_CollectionType > Mp7JrsOrderingKeyType_Field_CollectionPtr;

#define TypeOfOrderingKeyType_Field_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Field_LocalType")))
#define CreateOrderingKeyType_Field_LocalType (Dc1Factory::CreateObject(TypeOfOrderingKeyType_Field_LocalType))
class Mp7JrsOrderingKeyType_Field_LocalType;
class IMp7JrsOrderingKeyType_Field_LocalType;
/** Smart pointer for instance of IMp7JrsOrderingKeyType_Field_LocalType */
typedef Dc1Ptr< IMp7JrsOrderingKeyType_Field_LocalType > Mp7JrsOrderingKeyType_Field_LocalPtr;

#define TypeOfOrderingKeyType_Selector_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Selector_LocalType")))
#define CreateOrderingKeyType_Selector_LocalType (Dc1Factory::CreateObject(TypeOfOrderingKeyType_Selector_LocalType))
class Mp7JrsOrderingKeyType_Selector_LocalType;
class IMp7JrsOrderingKeyType_Selector_LocalType;
/** Smart pointer for instance of IMp7JrsOrderingKeyType_Selector_LocalType */
typedef Dc1Ptr< IMp7JrsOrderingKeyType_Selector_LocalType > Mp7JrsOrderingKeyType_Selector_LocalPtr;

#define TypeOfOrganizationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType")))
#define CreateOrganizationType (Dc1Factory::CreateObject(TypeOfOrganizationType))
class Mp7JrsOrganizationType;
class IMp7JrsOrganizationType;
/** Smart pointer for instance of IMp7JrsOrganizationType */
typedef Dc1Ptr< IMp7JrsOrganizationType > Mp7JrsOrganizationPtr;

#define TypeOfOrganizationType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_CollectionType")))
#define CreateOrganizationType_CollectionType (Dc1Factory::CreateObject(TypeOfOrganizationType_CollectionType))
class Mp7JrsOrganizationType_CollectionType;
class IMp7JrsOrganizationType_CollectionType;
/** Smart pointer for instance of IMp7JrsOrganizationType_CollectionType */
typedef Dc1Ptr< IMp7JrsOrganizationType_CollectionType > Mp7JrsOrganizationType_CollectionPtr;

#define TypeOfOrganizationType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_LocalType")))
#define CreateOrganizationType_LocalType (Dc1Factory::CreateObject(TypeOfOrganizationType_LocalType))
class Mp7JrsOrganizationType_LocalType;
class IMp7JrsOrganizationType_LocalType;
/** Smart pointer for instance of IMp7JrsOrganizationType_LocalType */
typedef Dc1Ptr< IMp7JrsOrganizationType_LocalType > Mp7JrsOrganizationType_LocalPtr;

#define TypeOfOrganizationType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_Name_CollectionType")))
#define CreateOrganizationType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_CollectionType))
class Mp7JrsOrganizationType_Name_CollectionType;
class IMp7JrsOrganizationType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsOrganizationType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsOrganizationType_Name_CollectionType > Mp7JrsOrganizationType_Name_CollectionPtr;

#define TypeOfOrganizationType_Name_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_Name_LocalType")))
#define CreateOrganizationType_Name_LocalType (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_LocalType))
class Mp7JrsOrganizationType_Name_LocalType;
class IMp7JrsOrganizationType_Name_LocalType;
/** Smart pointer for instance of IMp7JrsOrganizationType_Name_LocalType */
typedef Dc1Ptr< IMp7JrsOrganizationType_Name_LocalType > Mp7JrsOrganizationType_Name_LocalPtr;

#define TypeOfOrganizationType_Name_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_Name_type_LocalType")))
#define CreateOrganizationType_Name_type_LocalType (Dc1Factory::CreateObject(TypeOfOrganizationType_Name_type_LocalType))
class Mp7JrsOrganizationType_Name_type_LocalType;
class IMp7JrsOrganizationType_Name_type_LocalType;
// No smart pointer instance for IMp7JrsOrganizationType_Name_type_LocalType

#define TypeOfOrganizationType_NameTerm_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_NameTerm_CollectionType")))
#define CreateOrganizationType_NameTerm_CollectionType (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_CollectionType))
class Mp7JrsOrganizationType_NameTerm_CollectionType;
class IMp7JrsOrganizationType_NameTerm_CollectionType;
/** Smart pointer for instance of IMp7JrsOrganizationType_NameTerm_CollectionType */
typedef Dc1Ptr< IMp7JrsOrganizationType_NameTerm_CollectionType > Mp7JrsOrganizationType_NameTerm_CollectionPtr;

#define TypeOfOrganizationType_NameTerm_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_NameTerm_LocalType")))
#define CreateOrganizationType_NameTerm_LocalType (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_LocalType))
class Mp7JrsOrganizationType_NameTerm_LocalType;
class IMp7JrsOrganizationType_NameTerm_LocalType;
/** Smart pointer for instance of IMp7JrsOrganizationType_NameTerm_LocalType */
typedef Dc1Ptr< IMp7JrsOrganizationType_NameTerm_LocalType > Mp7JrsOrganizationType_NameTerm_LocalPtr;

#define TypeOfOrganizationType_NameTerm_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrganizationType_NameTerm_type_LocalType")))
#define CreateOrganizationType_NameTerm_type_LocalType (Dc1Factory::CreateObject(TypeOfOrganizationType_NameTerm_type_LocalType))
class Mp7JrsOrganizationType_NameTerm_type_LocalType;
class IMp7JrsOrganizationType_NameTerm_type_LocalType;
// No smart pointer instance for IMp7JrsOrganizationType_NameTerm_type_LocalType

#define TypeOfOutputQCValueType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OutputQCValueType")))
#define CreateOutputQCValueType (Dc1Factory::CreateObject(TypeOfOutputQCValueType))
class Mp7JrsOutputQCValueType;
class IMp7JrsOutputQCValueType;
/** Smart pointer for instance of IMp7JrsOutputQCValueType */
typedef Dc1Ptr< IMp7JrsOutputQCValueType > Mp7JrsOutputQCValuePtr;

#define TypeOfPackageType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType")))
#define CreatePackageType (Dc1Factory::CreateObject(TypeOfPackageType))
class Mp7JrsPackageType;
class IMp7JrsPackageType;
/** Smart pointer for instance of IMp7JrsPackageType */
typedef Dc1Ptr< IMp7JrsPackageType > Mp7JrsPackagePtr;

#define TypeOfPackageType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType_CollectionType")))
#define CreatePackageType_CollectionType (Dc1Factory::CreateObject(TypeOfPackageType_CollectionType))
class Mp7JrsPackageType_CollectionType;
class IMp7JrsPackageType_CollectionType;
/** Smart pointer for instance of IMp7JrsPackageType_CollectionType */
typedef Dc1Ptr< IMp7JrsPackageType_CollectionType > Mp7JrsPackageType_CollectionPtr;

#define TypeOfPackageType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType_LocalType")))
#define CreatePackageType_LocalType (Dc1Factory::CreateObject(TypeOfPackageType_LocalType))
class Mp7JrsPackageType_LocalType;
class IMp7JrsPackageType_LocalType;
/** Smart pointer for instance of IMp7JrsPackageType_LocalType */
typedef Dc1Ptr< IMp7JrsPackageType_LocalType > Mp7JrsPackageType_LocalPtr;

#define TypeOfPackageType_Scheme_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType_Scheme_LocalType")))
#define CreatePackageType_Scheme_LocalType (Dc1Factory::CreateObject(TypeOfPackageType_Scheme_LocalType))
class Mp7JrsPackageType_Scheme_LocalType;
class IMp7JrsPackageType_Scheme_LocalType;
/** Smart pointer for instance of IMp7JrsPackageType_Scheme_LocalType */
typedef Dc1Ptr< IMp7JrsPackageType_Scheme_LocalType > Mp7JrsPackageType_Scheme_LocalPtr;

#define TypeOfpaddingType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:paddingType")))
#define CreatepaddingType (Dc1Factory::CreateObject(TypeOfpaddingType))
class Mp7JrspaddingType;
class IMp7JrspaddingType;
/** Smart pointer for instance of IMp7JrspaddingType */
typedef Dc1Ptr< IMp7JrspaddingType > Mp7JrspaddingPtr;

#define TypeOfpaddingType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:paddingType_LocalType")))
#define CreatepaddingType_LocalType (Dc1Factory::CreateObject(TypeOfpaddingType_LocalType))
class Mp7JrspaddingType_LocalType;
class IMp7JrspaddingType_LocalType;
// No smart pointer instance for IMp7JrspaddingType_LocalType

#define TypeOfpaddingType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:paddingType_LocalType0")))
#define CreatepaddingType_LocalType0 (Dc1Factory::CreateObject(TypeOfpaddingType_LocalType0))
class Mp7JrspaddingType_LocalType0;
class IMp7JrspaddingType_LocalType0;
/** Smart pointer for instance of IMp7JrspaddingType_LocalType0 */
typedef Dc1Ptr< IMp7JrspaddingType_LocalType0 > Mp7JrspaddingType_Local0Ptr;

#define TypeOfpaddingType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:paddingType_LocalType1")))
#define CreatepaddingType_LocalType1 (Dc1Factory::CreateObject(TypeOfpaddingType_LocalType1))
class Mp7JrspaddingType_LocalType1;
class IMp7JrspaddingType_LocalType1;
/** Smart pointer for instance of IMp7JrspaddingType_LocalType1 */
typedef Dc1Ptr< IMp7JrspaddingType_LocalType1 > Mp7JrspaddingType_Local1Ptr;

#define TypeOfParameterTrajectoryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParameterTrajectoryType")))
#define CreateParameterTrajectoryType (Dc1Factory::CreateObject(TypeOfParameterTrajectoryType))
class Mp7JrsParameterTrajectoryType;
class IMp7JrsParameterTrajectoryType;
/** Smart pointer for instance of IMp7JrsParameterTrajectoryType */
typedef Dc1Ptr< IMp7JrsParameterTrajectoryType > Mp7JrsParameterTrajectoryPtr;

#define TypeOfParameterTrajectoryType_motionModel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParameterTrajectoryType_motionModel_LocalType")))
#define CreateParameterTrajectoryType_motionModel_LocalType (Dc1Factory::CreateObject(TypeOfParameterTrajectoryType_motionModel_LocalType))
class Mp7JrsParameterTrajectoryType_motionModel_LocalType;
class IMp7JrsParameterTrajectoryType_motionModel_LocalType;
// No smart pointer instance for IMp7JrsParameterTrajectoryType_motionModel_LocalType

#define TypeOfParametricMotionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType")))
#define CreateParametricMotionType (Dc1Factory::CreateObject(TypeOfParametricMotionType))
class Mp7JrsParametricMotionType;
class IMp7JrsParametricMotionType;
/** Smart pointer for instance of IMp7JrsParametricMotionType */
typedef Dc1Ptr< IMp7JrsParametricMotionType > Mp7JrsParametricMotionPtr;

#define TypeOfParametricMotionType_CoordDef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordDef_LocalType")))
#define CreateParametricMotionType_CoordDef_LocalType (Dc1Factory::CreateObject(TypeOfParametricMotionType_CoordDef_LocalType))
class Mp7JrsParametricMotionType_CoordDef_LocalType;
class IMp7JrsParametricMotionType_CoordDef_LocalType;
/** Smart pointer for instance of IMp7JrsParametricMotionType_CoordDef_LocalType */
typedef Dc1Ptr< IMp7JrsParametricMotionType_CoordDef_LocalType > Mp7JrsParametricMotionType_CoordDef_LocalPtr;

#define TypeOfParametricMotionType_CoordRef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordRef_LocalType")))
#define CreateParametricMotionType_CoordRef_LocalType (Dc1Factory::CreateObject(TypeOfParametricMotionType_CoordRef_LocalType))
class Mp7JrsParametricMotionType_CoordRef_LocalType;
class IMp7JrsParametricMotionType_CoordRef_LocalType;
/** Smart pointer for instance of IMp7JrsParametricMotionType_CoordRef_LocalType */
typedef Dc1Ptr< IMp7JrsParametricMotionType_CoordRef_LocalType > Mp7JrsParametricMotionType_CoordRef_LocalPtr;

#define TypeOfParametricMotionType_motionModel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_motionModel_LocalType")))
#define CreateParametricMotionType_motionModel_LocalType (Dc1Factory::CreateObject(TypeOfParametricMotionType_motionModel_LocalType))
class Mp7JrsParametricMotionType_motionModel_LocalType;
class IMp7JrsParametricMotionType_motionModel_LocalType;
// No smart pointer instance for IMp7JrsParametricMotionType_motionModel_LocalType

#define TypeOfParametricMotionType_Parameters_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_Parameters_LocalType")))
#define CreateParametricMotionType_Parameters_LocalType (Dc1Factory::CreateObject(TypeOfParametricMotionType_Parameters_LocalType))
class Mp7JrsParametricMotionType_Parameters_LocalType;
class IMp7JrsParametricMotionType_Parameters_LocalType;
/** Smart pointer for instance of IMp7JrsParametricMotionType_Parameters_LocalType */
typedef Dc1Ptr< IMp7JrsParametricMotionType_Parameters_LocalType > Mp7JrsParametricMotionType_Parameters_LocalPtr;

#define TypeOfParentalGuidanceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParentalGuidanceType")))
#define CreateParentalGuidanceType (Dc1Factory::CreateObject(TypeOfParentalGuidanceType))
class Mp7JrsParentalGuidanceType;
class IMp7JrsParentalGuidanceType;
/** Smart pointer for instance of IMp7JrsParentalGuidanceType */
typedef Dc1Ptr< IMp7JrsParentalGuidanceType > Mp7JrsParentalGuidancePtr;

#define TypeOfParentalGuidanceType_Region_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParentalGuidanceType_Region_CollectionType")))
#define CreateParentalGuidanceType_Region_CollectionType (Dc1Factory::CreateObject(TypeOfParentalGuidanceType_Region_CollectionType))
class Mp7JrsParentalGuidanceType_Region_CollectionType;
class IMp7JrsParentalGuidanceType_Region_CollectionType;
/** Smart pointer for instance of IMp7JrsParentalGuidanceType_Region_CollectionType */
typedef Dc1Ptr< IMp7JrsParentalGuidanceType_Region_CollectionType > Mp7JrsParentalGuidanceType_Region_CollectionPtr;

#define TypeOfPartitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType")))
#define CreatePartitionType (Dc1Factory::CreateObject(TypeOfPartitionType))
class Mp7JrsPartitionType;
class IMp7JrsPartitionType;
/** Smart pointer for instance of IMp7JrsPartitionType */
typedef Dc1Ptr< IMp7JrsPartitionType > Mp7JrsPartitionPtr;

#define TypeOfPartitionType_dim_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType_dim_LocalType")))
#define CreatePartitionType_dim_LocalType (Dc1Factory::CreateObject(TypeOfPartitionType_dim_LocalType))
class Mp7JrsPartitionType_dim_LocalType;
class IMp7JrsPartitionType_dim_LocalType;
/** Smart pointer for instance of IMp7JrsPartitionType_dim_LocalType */
typedef Dc1Ptr< IMp7JrsPartitionType_dim_LocalType > Mp7JrsPartitionType_dim_LocalPtr;

#define TypeOfPerceptual3DShapeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType")))
#define CreatePerceptual3DShapeType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType))
class Mp7JrsPerceptual3DShapeType;
class IMp7JrsPerceptual3DShapeType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType > Mp7JrsPerceptual3DShapePtr;

#define TypeOfPerceptual3DShapeType_BitsPerAttribute_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_BitsPerAttribute_LocalType")))
#define CreatePerceptual3DShapeType_BitsPerAttribute_LocalType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType_BitsPerAttribute_LocalType))
class Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalType;
class IMp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalType > Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr;

#define TypeOfPerceptual3DShapeType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_CollectionType")))
#define CreatePerceptual3DShapeType_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType_CollectionType))
class Mp7JrsPerceptual3DShapeType_CollectionType;
class IMp7JrsPerceptual3DShapeType_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_CollectionType > Mp7JrsPerceptual3DShapeType_CollectionPtr;

#define TypeOfPerceptual3DShapeType_IsConnected_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_IsConnected_CollectionType")))
#define CreatePerceptual3DShapeType_IsConnected_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType_IsConnected_CollectionType))
class Mp7JrsPerceptual3DShapeType_IsConnected_CollectionType;
class IMp7JrsPerceptual3DShapeType_IsConnected_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType_IsConnected_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_IsConnected_CollectionType > Mp7JrsPerceptual3DShapeType_IsConnected_CollectionPtr;

#define TypeOfPerceptual3DShapeType_IsConnected_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_IsConnected_LocalType")))
#define CreatePerceptual3DShapeType_IsConnected_LocalType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType_IsConnected_LocalType))
class Mp7JrsPerceptual3DShapeType_IsConnected_LocalType;
class IMp7JrsPerceptual3DShapeType_IsConnected_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType_IsConnected_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_IsConnected_LocalType > Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr;

#define TypeOfPerceptual3DShapeType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_LocalType")))
#define CreatePerceptual3DShapeType_LocalType (Dc1Factory::CreateObject(TypeOfPerceptual3DShapeType_LocalType))
class Mp7JrsPerceptual3DShapeType_LocalType;
class IMp7JrsPerceptual3DShapeType_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptual3DShapeType_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptual3DShapeType_LocalType > Mp7JrsPerceptual3DShapeType_LocalPtr;

#define TypeOfPerceptualAttributeDSType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualAttributeDSType")))
#define CreatePerceptualAttributeDSType (Dc1Factory::CreateObject(TypeOfPerceptualAttributeDSType))
class Mp7JrsPerceptualAttributeDSType;
class IMp7JrsPerceptualAttributeDSType;
/** Smart pointer for instance of IMp7JrsPerceptualAttributeDSType */
typedef Dc1Ptr< IMp7JrsPerceptualAttributeDSType > Mp7JrsPerceptualAttributeDSPtr;

#define TypeOfPerceptualBeatType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualBeatType")))
#define CreatePerceptualBeatType (Dc1Factory::CreateObject(TypeOfPerceptualBeatType))
class Mp7JrsPerceptualBeatType;
class IMp7JrsPerceptualBeatType;
/** Smart pointer for instance of IMp7JrsPerceptualBeatType */
typedef Dc1Ptr< IMp7JrsPerceptualBeatType > Mp7JrsPerceptualBeatPtr;

#define TypeOfPerceptualBeatType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualBeatType_score_LocalType")))
#define CreatePerceptualBeatType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualBeatType_score_LocalType))
class Mp7JrsPerceptualBeatType_score_LocalType;
class IMp7JrsPerceptualBeatType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualBeatType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualBeatType_score_LocalType > Mp7JrsPerceptualBeatType_score_LocalPtr;

#define TypeOfPerceptualEnergyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualEnergyType")))
#define CreatePerceptualEnergyType (Dc1Factory::CreateObject(TypeOfPerceptualEnergyType))
class Mp7JrsPerceptualEnergyType;
class IMp7JrsPerceptualEnergyType;
/** Smart pointer for instance of IMp7JrsPerceptualEnergyType */
typedef Dc1Ptr< IMp7JrsPerceptualEnergyType > Mp7JrsPerceptualEnergyPtr;

#define TypeOfPerceptualEnergyType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualEnergyType_score_LocalType")))
#define CreatePerceptualEnergyType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualEnergyType_score_LocalType))
class Mp7JrsPerceptualEnergyType_score_LocalType;
class IMp7JrsPerceptualEnergyType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualEnergyType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualEnergyType_score_LocalType > Mp7JrsPerceptualEnergyType_score_LocalPtr;

#define TypeOfPerceptualGenreDistributionElementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionElementType")))
#define CreatePerceptualGenreDistributionElementType (Dc1Factory::CreateObject(TypeOfPerceptualGenreDistributionElementType))
class Mp7JrsPerceptualGenreDistributionElementType;
class IMp7JrsPerceptualGenreDistributionElementType;
/** Smart pointer for instance of IMp7JrsPerceptualGenreDistributionElementType */
typedef Dc1Ptr< IMp7JrsPerceptualGenreDistributionElementType > Mp7JrsPerceptualGenreDistributionElementPtr;

#define TypeOfPerceptualGenreDistributionElementType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionElementType_score_LocalType")))
#define CreatePerceptualGenreDistributionElementType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualGenreDistributionElementType_score_LocalType))
class Mp7JrsPerceptualGenreDistributionElementType_score_LocalType;
class IMp7JrsPerceptualGenreDistributionElementType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualGenreDistributionElementType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualGenreDistributionElementType_score_LocalType > Mp7JrsPerceptualGenreDistributionElementType_score_LocalPtr;

#define TypeOfPerceptualGenreDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionType")))
#define CreatePerceptualGenreDistributionType (Dc1Factory::CreateObject(TypeOfPerceptualGenreDistributionType))
class Mp7JrsPerceptualGenreDistributionType;
class IMp7JrsPerceptualGenreDistributionType;
/** Smart pointer for instance of IMp7JrsPerceptualGenreDistributionType */
typedef Dc1Ptr< IMp7JrsPerceptualGenreDistributionType > Mp7JrsPerceptualGenreDistributionPtr;

#define TypeOfPerceptualGenreDistributionType_PerceptualGenre_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionType_PerceptualGenre_CollectionType")))
#define CreatePerceptualGenreDistributionType_PerceptualGenre_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptualGenreDistributionType_PerceptualGenre_CollectionType))
class Mp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionType;
class IMp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionType > Mp7JrsPerceptualGenreDistributionType_PerceptualGenre_CollectionPtr;

#define TypeOfPerceptualInstrumentationDistributionElementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionElementType")))
#define CreatePerceptualInstrumentationDistributionElementType (Dc1Factory::CreateObject(TypeOfPerceptualInstrumentationDistributionElementType))
class Mp7JrsPerceptualInstrumentationDistributionElementType;
class IMp7JrsPerceptualInstrumentationDistributionElementType;
/** Smart pointer for instance of IMp7JrsPerceptualInstrumentationDistributionElementType */
typedef Dc1Ptr< IMp7JrsPerceptualInstrumentationDistributionElementType > Mp7JrsPerceptualInstrumentationDistributionElementPtr;

#define TypeOfPerceptualInstrumentationDistributionElementType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionElementType_score_LocalType")))
#define CreatePerceptualInstrumentationDistributionElementType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualInstrumentationDistributionElementType_score_LocalType))
class Mp7JrsPerceptualInstrumentationDistributionElementType_score_LocalType;
class IMp7JrsPerceptualInstrumentationDistributionElementType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualInstrumentationDistributionElementType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualInstrumentationDistributionElementType_score_LocalType > Mp7JrsPerceptualInstrumentationDistributionElementType_score_LocalPtr;

#define TypeOfPerceptualInstrumentationDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionType")))
#define CreatePerceptualInstrumentationDistributionType (Dc1Factory::CreateObject(TypeOfPerceptualInstrumentationDistributionType))
class Mp7JrsPerceptualInstrumentationDistributionType;
class IMp7JrsPerceptualInstrumentationDistributionType;
/** Smart pointer for instance of IMp7JrsPerceptualInstrumentationDistributionType */
typedef Dc1Ptr< IMp7JrsPerceptualInstrumentationDistributionType > Mp7JrsPerceptualInstrumentationDistributionPtr;

#define TypeOfPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType")))
#define CreatePerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType))
class Mp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType;
class IMp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionType > Mp7JrsPerceptualInstrumentationDistributionType_PerceptualInstrumentation_CollectionPtr;

#define TypeOfPerceptualLanguageType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLanguageType")))
#define CreatePerceptualLanguageType (Dc1Factory::CreateObject(TypeOfPerceptualLanguageType))
class Mp7JrsPerceptualLanguageType;
class IMp7JrsPerceptualLanguageType;
/** Smart pointer for instance of IMp7JrsPerceptualLanguageType */
typedef Dc1Ptr< IMp7JrsPerceptualLanguageType > Mp7JrsPerceptualLanguagePtr;

#define TypeOfPerceptualLyricsDistributionElementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionElementType")))
#define CreatePerceptualLyricsDistributionElementType (Dc1Factory::CreateObject(TypeOfPerceptualLyricsDistributionElementType))
class Mp7JrsPerceptualLyricsDistributionElementType;
class IMp7JrsPerceptualLyricsDistributionElementType;
/** Smart pointer for instance of IMp7JrsPerceptualLyricsDistributionElementType */
typedef Dc1Ptr< IMp7JrsPerceptualLyricsDistributionElementType > Mp7JrsPerceptualLyricsDistributionElementPtr;

#define TypeOfPerceptualLyricsDistributionElementType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionElementType_score_LocalType")))
#define CreatePerceptualLyricsDistributionElementType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualLyricsDistributionElementType_score_LocalType))
class Mp7JrsPerceptualLyricsDistributionElementType_score_LocalType;
class IMp7JrsPerceptualLyricsDistributionElementType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualLyricsDistributionElementType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualLyricsDistributionElementType_score_LocalType > Mp7JrsPerceptualLyricsDistributionElementType_score_LocalPtr;

#define TypeOfPerceptualLyricsDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionType")))
#define CreatePerceptualLyricsDistributionType (Dc1Factory::CreateObject(TypeOfPerceptualLyricsDistributionType))
class Mp7JrsPerceptualLyricsDistributionType;
class IMp7JrsPerceptualLyricsDistributionType;
/** Smart pointer for instance of IMp7JrsPerceptualLyricsDistributionType */
typedef Dc1Ptr< IMp7JrsPerceptualLyricsDistributionType > Mp7JrsPerceptualLyricsDistributionPtr;

#define TypeOfPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionType_PerceptualLyrics_CollectionType")))
#define CreatePerceptualLyricsDistributionType_PerceptualLyrics_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType))
class Mp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType;
class IMp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionType > Mp7JrsPerceptualLyricsDistributionType_PerceptualLyrics_CollectionPtr;

#define TypeOfPerceptualMoodDistributionElementType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionElementType")))
#define CreatePerceptualMoodDistributionElementType (Dc1Factory::CreateObject(TypeOfPerceptualMoodDistributionElementType))
class Mp7JrsPerceptualMoodDistributionElementType;
class IMp7JrsPerceptualMoodDistributionElementType;
/** Smart pointer for instance of IMp7JrsPerceptualMoodDistributionElementType */
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionElementType > Mp7JrsPerceptualMoodDistributionElementPtr;

#define TypeOfPerceptualMoodDistributionElementType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionElementType_score_LocalType")))
#define CreatePerceptualMoodDistributionElementType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualMoodDistributionElementType_score_LocalType))
class Mp7JrsPerceptualMoodDistributionElementType_score_LocalType;
class IMp7JrsPerceptualMoodDistributionElementType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualMoodDistributionElementType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionElementType_score_LocalType > Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr;

#define TypeOfPerceptualMoodDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionType")))
#define CreatePerceptualMoodDistributionType (Dc1Factory::CreateObject(TypeOfPerceptualMoodDistributionType))
class Mp7JrsPerceptualMoodDistributionType;
class IMp7JrsPerceptualMoodDistributionType;
/** Smart pointer for instance of IMp7JrsPerceptualMoodDistributionType */
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionType > Mp7JrsPerceptualMoodDistributionPtr;

#define TypeOfPerceptualMoodDistributionType_PerceptualMood_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionType_PerceptualMood_CollectionType")))
#define CreatePerceptualMoodDistributionType_PerceptualMood_CollectionType (Dc1Factory::CreateObject(TypeOfPerceptualMoodDistributionType_PerceptualMood_CollectionType))
class Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionType;
class IMp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionType;
/** Smart pointer for instance of IMp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionType */
typedef Dc1Ptr< IMp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionType > Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr;

#define TypeOfPerceptualRecordingType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualRecordingType")))
#define CreatePerceptualRecordingType (Dc1Factory::CreateObject(TypeOfPerceptualRecordingType))
class Mp7JrsPerceptualRecordingType;
class IMp7JrsPerceptualRecordingType;
/** Smart pointer for instance of IMp7JrsPerceptualRecordingType */
typedef Dc1Ptr< IMp7JrsPerceptualRecordingType > Mp7JrsPerceptualRecordingPtr;

#define TypeOfPerceptualSoundType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualSoundType")))
#define CreatePerceptualSoundType (Dc1Factory::CreateObject(TypeOfPerceptualSoundType))
class Mp7JrsPerceptualSoundType;
class IMp7JrsPerceptualSoundType;
/** Smart pointer for instance of IMp7JrsPerceptualSoundType */
typedef Dc1Ptr< IMp7JrsPerceptualSoundType > Mp7JrsPerceptualSoundPtr;

#define TypeOfPerceptualTempoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualTempoType")))
#define CreatePerceptualTempoType (Dc1Factory::CreateObject(TypeOfPerceptualTempoType))
class Mp7JrsPerceptualTempoType;
class IMp7JrsPerceptualTempoType;
/** Smart pointer for instance of IMp7JrsPerceptualTempoType */
typedef Dc1Ptr< IMp7JrsPerceptualTempoType > Mp7JrsPerceptualTempoPtr;

#define TypeOfPerceptualTempoType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualTempoType_score_LocalType")))
#define CreatePerceptualTempoType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualTempoType_score_LocalType))
class Mp7JrsPerceptualTempoType_score_LocalType;
class IMp7JrsPerceptualTempoType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualTempoType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualTempoType_score_LocalType > Mp7JrsPerceptualTempoType_score_LocalPtr;

#define TypeOfPerceptualValenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualValenceType")))
#define CreatePerceptualValenceType (Dc1Factory::CreateObject(TypeOfPerceptualValenceType))
class Mp7JrsPerceptualValenceType;
class IMp7JrsPerceptualValenceType;
/** Smart pointer for instance of IMp7JrsPerceptualValenceType */
typedef Dc1Ptr< IMp7JrsPerceptualValenceType > Mp7JrsPerceptualValencePtr;

#define TypeOfPerceptualValenceType_score_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualValenceType_score_LocalType")))
#define CreatePerceptualValenceType_score_LocalType (Dc1Factory::CreateObject(TypeOfPerceptualValenceType_score_LocalType))
class Mp7JrsPerceptualValenceType_score_LocalType;
class IMp7JrsPerceptualValenceType_score_LocalType;
/** Smart pointer for instance of IMp7JrsPerceptualValenceType_score_LocalType */
typedef Dc1Ptr< IMp7JrsPerceptualValenceType_score_LocalType > Mp7JrsPerceptualValenceType_score_LocalPtr;

#define TypeOfPerceptualVocalsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualVocalsType")))
#define CreatePerceptualVocalsType (Dc1Factory::CreateObject(TypeOfPerceptualVocalsType))
class Mp7JrsPerceptualVocalsType;
class IMp7JrsPerceptualVocalsType;
/** Smart pointer for instance of IMp7JrsPerceptualVocalsType */
typedef Dc1Ptr< IMp7JrsPerceptualVocalsType > Mp7JrsPerceptualVocalsPtr;

#define TypeOfPercussiveInstrumentTimbreType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PercussiveInstrumentTimbreType")))
#define CreatePercussiveInstrumentTimbreType (Dc1Factory::CreateObject(TypeOfPercussiveInstrumentTimbreType))
class Mp7JrsPercussiveInstrumentTimbreType;
class IMp7JrsPercussiveInstrumentTimbreType;
/** Smart pointer for instance of IMp7JrsPercussiveInstrumentTimbreType */
typedef Dc1Ptr< IMp7JrsPercussiveInstrumentTimbreType > Mp7JrsPercussiveInstrumentTimbrePtr;

#define TypeOfPersonalInterestsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType")))
#define CreatePersonalInterestsType (Dc1Factory::CreateObject(TypeOfPersonalInterestsType))
class Mp7JrsPersonalInterestsType;
class IMp7JrsPersonalInterestsType;
/** Smart pointer for instance of IMp7JrsPersonalInterestsType */
typedef Dc1Ptr< IMp7JrsPersonalInterestsType > Mp7JrsPersonalInterestsPtr;

#define TypeOfPersonalInterestsType_Activity_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType_Activity_CollectionType")))
#define CreatePersonalInterestsType_Activity_CollectionType (Dc1Factory::CreateObject(TypeOfPersonalInterestsType_Activity_CollectionType))
class Mp7JrsPersonalInterestsType_Activity_CollectionType;
class IMp7JrsPersonalInterestsType_Activity_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonalInterestsType_Activity_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonalInterestsType_Activity_CollectionType > Mp7JrsPersonalInterestsType_Activity_CollectionPtr;

#define TypeOfPersonalInterestsType_Favorite_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType_Favorite_CollectionType")))
#define CreatePersonalInterestsType_Favorite_CollectionType (Dc1Factory::CreateObject(TypeOfPersonalInterestsType_Favorite_CollectionType))
class Mp7JrsPersonalInterestsType_Favorite_CollectionType;
class IMp7JrsPersonalInterestsType_Favorite_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonalInterestsType_Favorite_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonalInterestsType_Favorite_CollectionType > Mp7JrsPersonalInterestsType_Favorite_CollectionPtr;

#define TypeOfPersonalInterestsType_Interest_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType_Interest_CollectionType")))
#define CreatePersonalInterestsType_Interest_CollectionType (Dc1Factory::CreateObject(TypeOfPersonalInterestsType_Interest_CollectionType))
class Mp7JrsPersonalInterestsType_Interest_CollectionType;
class IMp7JrsPersonalInterestsType_Interest_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonalInterestsType_Interest_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonalInterestsType_Interest_CollectionType > Mp7JrsPersonalInterestsType_Interest_CollectionPtr;

#define TypeOfPersonGroupType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType")))
#define CreatePersonGroupType (Dc1Factory::CreateObject(TypeOfPersonGroupType))
class Mp7JrsPersonGroupType;
class IMp7JrsPersonGroupType;
/** Smart pointer for instance of IMp7JrsPersonGroupType */
typedef Dc1Ptr< IMp7JrsPersonGroupType > Mp7JrsPersonGroupPtr;

#define TypeOfPersonGroupType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_CollectionType")))
#define CreatePersonGroupType_CollectionType (Dc1Factory::CreateObject(TypeOfPersonGroupType_CollectionType))
class Mp7JrsPersonGroupType_CollectionType;
class IMp7JrsPersonGroupType_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_CollectionType > Mp7JrsPersonGroupType_CollectionPtr;

#define TypeOfPersonGroupType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_LocalType")))
#define CreatePersonGroupType_LocalType (Dc1Factory::CreateObject(TypeOfPersonGroupType_LocalType))
class Mp7JrsPersonGroupType_LocalType;
class IMp7JrsPersonGroupType_LocalType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_LocalType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_LocalType > Mp7JrsPersonGroupType_LocalPtr;

#define TypeOfPersonGroupType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_Name_CollectionType")))
#define CreatePersonGroupType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_CollectionType))
class Mp7JrsPersonGroupType_Name_CollectionType;
class IMp7JrsPersonGroupType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_Name_CollectionType > Mp7JrsPersonGroupType_Name_CollectionPtr;

#define TypeOfPersonGroupType_Name_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_Name_LocalType")))
#define CreatePersonGroupType_Name_LocalType (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_LocalType))
class Mp7JrsPersonGroupType_Name_LocalType;
class IMp7JrsPersonGroupType_Name_LocalType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_Name_LocalType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_Name_LocalType > Mp7JrsPersonGroupType_Name_LocalPtr;

#define TypeOfPersonGroupType_Name_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_Name_type_LocalType")))
#define CreatePersonGroupType_Name_type_LocalType (Dc1Factory::CreateObject(TypeOfPersonGroupType_Name_type_LocalType))
class Mp7JrsPersonGroupType_Name_type_LocalType;
class IMp7JrsPersonGroupType_Name_type_LocalType;
// No smart pointer instance for IMp7JrsPersonGroupType_Name_type_LocalType

#define TypeOfPersonGroupType_NameTerm_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_NameTerm_CollectionType")))
#define CreatePersonGroupType_NameTerm_CollectionType (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_CollectionType))
class Mp7JrsPersonGroupType_NameTerm_CollectionType;
class IMp7JrsPersonGroupType_NameTerm_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_NameTerm_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_NameTerm_CollectionType > Mp7JrsPersonGroupType_NameTerm_CollectionPtr;

#define TypeOfPersonGroupType_NameTerm_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_NameTerm_LocalType")))
#define CreatePersonGroupType_NameTerm_LocalType (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_LocalType))
class Mp7JrsPersonGroupType_NameTerm_LocalType;
class IMp7JrsPersonGroupType_NameTerm_LocalType;
/** Smart pointer for instance of IMp7JrsPersonGroupType_NameTerm_LocalType */
typedef Dc1Ptr< IMp7JrsPersonGroupType_NameTerm_LocalType > Mp7JrsPersonGroupType_NameTerm_LocalPtr;

#define TypeOfPersonGroupType_NameTerm_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonGroupType_NameTerm_type_LocalType")))
#define CreatePersonGroupType_NameTerm_type_LocalType (Dc1Factory::CreateObject(TypeOfPersonGroupType_NameTerm_type_LocalType))
class Mp7JrsPersonGroupType_NameTerm_type_LocalType;
class IMp7JrsPersonGroupType_NameTerm_type_LocalType;
// No smart pointer instance for IMp7JrsPersonGroupType_NameTerm_type_LocalType

#define TypeOfPersonNameType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType")))
#define CreatePersonNameType (Dc1Factory::CreateObject(TypeOfPersonNameType))
class Mp7JrsPersonNameType;
class IMp7JrsPersonNameType;
/** Smart pointer for instance of IMp7JrsPersonNameType */
typedef Dc1Ptr< IMp7JrsPersonNameType > Mp7JrsPersonNamePtr;

#define TypeOfPersonNameType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType_CollectionType")))
#define CreatePersonNameType_CollectionType (Dc1Factory::CreateObject(TypeOfPersonNameType_CollectionType))
class Mp7JrsPersonNameType_CollectionType;
class IMp7JrsPersonNameType_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonNameType_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonNameType_CollectionType > Mp7JrsPersonNameType_CollectionPtr;

#define TypeOfPersonNameType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType_LocalType")))
#define CreatePersonNameType_LocalType (Dc1Factory::CreateObject(TypeOfPersonNameType_LocalType))
class Mp7JrsPersonNameType_LocalType;
class IMp7JrsPersonNameType_LocalType;
/** Smart pointer for instance of IMp7JrsPersonNameType_LocalType */
typedef Dc1Ptr< IMp7JrsPersonNameType_LocalType > Mp7JrsPersonNameType_LocalPtr;

#define TypeOfPersonNameType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType_type_LocalType")))
#define CreatePersonNameType_type_LocalType (Dc1Factory::CreateObject(TypeOfPersonNameType_type_LocalType))
class Mp7JrsPersonNameType_type_LocalType;
class IMp7JrsPersonNameType_type_LocalType;
// No smart pointer instance for IMp7JrsPersonNameType_type_LocalType

#define TypeOfPersonType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType")))
#define CreatePersonType (Dc1Factory::CreateObject(TypeOfPersonType))
class Mp7JrsPersonType;
class IMp7JrsPersonType;
/** Smart pointer for instance of IMp7JrsPersonType */
typedef Dc1Ptr< IMp7JrsPersonType > Mp7JrsPersonPtr;

#define TypeOfPersonType_Affiliation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_Affiliation_CollectionType")))
#define CreatePersonType_Affiliation_CollectionType (Dc1Factory::CreateObject(TypeOfPersonType_Affiliation_CollectionType))
class Mp7JrsPersonType_Affiliation_CollectionType;
class IMp7JrsPersonType_Affiliation_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonType_Affiliation_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonType_Affiliation_CollectionType > Mp7JrsPersonType_Affiliation_CollectionPtr;

#define TypeOfPersonType_Affiliation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_Affiliation_LocalType")))
#define CreatePersonType_Affiliation_LocalType (Dc1Factory::CreateObject(TypeOfPersonType_Affiliation_LocalType))
class Mp7JrsPersonType_Affiliation_LocalType;
class IMp7JrsPersonType_Affiliation_LocalType;
/** Smart pointer for instance of IMp7JrsPersonType_Affiliation_LocalType */
typedef Dc1Ptr< IMp7JrsPersonType_Affiliation_LocalType > Mp7JrsPersonType_Affiliation_LocalPtr;

#define TypeOfPersonType_Citizenship_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_Citizenship_CollectionType")))
#define CreatePersonType_Citizenship_CollectionType (Dc1Factory::CreateObject(TypeOfPersonType_Citizenship_CollectionType))
class Mp7JrsPersonType_Citizenship_CollectionType;
class IMp7JrsPersonType_Citizenship_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonType_Citizenship_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonType_Citizenship_CollectionType > Mp7JrsPersonType_Citizenship_CollectionPtr;

#define TypeOfPersonType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_CollectionType")))
#define CreatePersonType_CollectionType (Dc1Factory::CreateObject(TypeOfPersonType_CollectionType))
class Mp7JrsPersonType_CollectionType;
class IMp7JrsPersonType_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonType_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonType_CollectionType > Mp7JrsPersonType_CollectionPtr;

#define TypeOfPersonType_ElectronicAddress_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_ElectronicAddress_CollectionType")))
#define CreatePersonType_ElectronicAddress_CollectionType (Dc1Factory::CreateObject(TypeOfPersonType_ElectronicAddress_CollectionType))
class Mp7JrsPersonType_ElectronicAddress_CollectionType;
class IMp7JrsPersonType_ElectronicAddress_CollectionType;
/** Smart pointer for instance of IMp7JrsPersonType_ElectronicAddress_CollectionType */
typedef Dc1Ptr< IMp7JrsPersonType_ElectronicAddress_CollectionType > Mp7JrsPersonType_ElectronicAddress_CollectionPtr;

#define TypeOfPersonType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_LocalType")))
#define CreatePersonType_LocalType (Dc1Factory::CreateObject(TypeOfPersonType_LocalType))
class Mp7JrsPersonType_LocalType;
class IMp7JrsPersonType_LocalType;
/** Smart pointer for instance of IMp7JrsPersonType_LocalType */
typedef Dc1Ptr< IMp7JrsPersonType_LocalType > Mp7JrsPersonType_LocalPtr;

#define TypeOfPhoneLexiconIndexType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneLexiconIndexType")))
#define CreatePhoneLexiconIndexType (Dc1Factory::CreateObject(TypeOfPhoneLexiconIndexType))
class Mp7JrsPhoneLexiconIndexType;
class IMp7JrsPhoneLexiconIndexType;
/** Smart pointer for instance of IMp7JrsPhoneLexiconIndexType */
typedef Dc1Ptr< IMp7JrsPhoneLexiconIndexType > Mp7JrsPhoneLexiconIndexPtr;

#define TypeOfPhoneLexiconType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneLexiconType")))
#define CreatePhoneLexiconType (Dc1Factory::CreateObject(TypeOfPhoneLexiconType))
class Mp7JrsPhoneLexiconType;
class IMp7JrsPhoneLexiconType;
/** Smart pointer for instance of IMp7JrsPhoneLexiconType */
typedef Dc1Ptr< IMp7JrsPhoneLexiconType > Mp7JrsPhoneLexiconPtr;

#define TypeOfPhoneLexiconType_Token_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneLexiconType_Token_CollectionType")))
#define CreatePhoneLexiconType_Token_CollectionType (Dc1Factory::CreateObject(TypeOfPhoneLexiconType_Token_CollectionType))
class Mp7JrsPhoneLexiconType_Token_CollectionType;
class IMp7JrsPhoneLexiconType_Token_CollectionType;
/** Smart pointer for instance of IMp7JrsPhoneLexiconType_Token_CollectionType */
typedef Dc1Ptr< IMp7JrsPhoneLexiconType_Token_CollectionType > Mp7JrsPhoneLexiconType_Token_CollectionPtr;

#define TypeOfphoneticAlphabetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:phoneticAlphabetType")))
#define CreatephoneticAlphabetType (Dc1Factory::CreateObject(TypeOfphoneticAlphabetType))
class Mp7JrsphoneticAlphabetType;
class IMp7JrsphoneticAlphabetType;
// No smart pointer instance for IMp7JrsphoneticAlphabetType

#define TypeOfPhoneticTranscriptionLexiconType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneticTranscriptionLexiconType")))
#define CreatePhoneticTranscriptionLexiconType (Dc1Factory::CreateObject(TypeOfPhoneticTranscriptionLexiconType))
class Mp7JrsPhoneticTranscriptionLexiconType;
class IMp7JrsPhoneticTranscriptionLexiconType;
/** Smart pointer for instance of IMp7JrsPhoneticTranscriptionLexiconType */
typedef Dc1Ptr< IMp7JrsPhoneticTranscriptionLexiconType > Mp7JrsPhoneticTranscriptionLexiconPtr;

#define TypeOfPhoneticTranscriptionLexiconType_Token_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneticTranscriptionLexiconType_Token_CollectionType")))
#define CreatePhoneticTranscriptionLexiconType_Token_CollectionType (Dc1Factory::CreateObject(TypeOfPhoneticTranscriptionLexiconType_Token_CollectionType))
class Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionType;
class IMp7JrsPhoneticTranscriptionLexiconType_Token_CollectionType;
/** Smart pointer for instance of IMp7JrsPhoneticTranscriptionLexiconType_Token_CollectionType */
typedef Dc1Ptr< IMp7JrsPhoneticTranscriptionLexiconType_Token_CollectionType > Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr;

#define TypeOfPhoneticTranscriptionLexiconType_Token_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneticTranscriptionLexiconType_Token_LocalType")))
#define CreatePhoneticTranscriptionLexiconType_Token_LocalType (Dc1Factory::CreateObject(TypeOfPhoneticTranscriptionLexiconType_Token_LocalType))
class Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType;
class IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType;
/** Smart pointer for instance of IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType */
typedef Dc1Ptr< IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType > Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalPtr;

#define TypeOfPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType")))
#define CreatePhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType (Dc1Factory::CreateObject(TypeOfPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType))
class Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType;
class IMp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType;
/** Smart pointer for instance of IMp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType */
typedef Dc1Ptr< IMp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType > Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr;

#define TypeOfPhoneType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneType")))
#define CreatePhoneType (Dc1Factory::CreateObject(TypeOfPhoneType))
class Mp7JrsPhoneType;
class IMp7JrsPhoneType;
/** Smart pointer for instance of IMp7JrsPhoneType */
typedef Dc1Ptr< IMp7JrsPhoneType > Mp7JrsPhonePtr;

#define TypeOfPitchProfileDSType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PitchProfileDSType")))
#define CreatePitchProfileDSType (Dc1Factory::CreateObject(TypeOfPitchProfileDSType))
class Mp7JrsPitchProfileDSType;
class IMp7JrsPitchProfileDSType;
/** Smart pointer for instance of IMp7JrsPitchProfileDSType */
typedef Dc1Ptr< IMp7JrsPitchProfileDSType > Mp7JrsPitchProfileDSPtr;

#define TypeOfPlaceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))
#define CreatePlaceType (Dc1Factory::CreateObject(TypeOfPlaceType))
class Mp7JrsPlaceType;
class IMp7JrsPlaceType;
/** Smart pointer for instance of IMp7JrsPlaceType */
typedef Dc1Ptr< IMp7JrsPlaceType > Mp7JrsPlacePtr;

#define TypeOfPlaceType_AdministrativeUnit_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_AdministrativeUnit_CollectionType")))
#define CreatePlaceType_AdministrativeUnit_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_AdministrativeUnit_CollectionType))
class Mp7JrsPlaceType_AdministrativeUnit_CollectionType;
class IMp7JrsPlaceType_AdministrativeUnit_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_AdministrativeUnit_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_AdministrativeUnit_CollectionType > Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr;

#define TypeOfPlaceType_AdministrativeUnit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_AdministrativeUnit_LocalType")))
#define CreatePlaceType_AdministrativeUnit_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_AdministrativeUnit_LocalType))
class Mp7JrsPlaceType_AdministrativeUnit_LocalType;
class IMp7JrsPlaceType_AdministrativeUnit_LocalType;
/** Smart pointer for instance of IMp7JrsPlaceType_AdministrativeUnit_LocalType */
typedef Dc1Ptr< IMp7JrsPlaceType_AdministrativeUnit_LocalType > Mp7JrsPlaceType_AdministrativeUnit_LocalPtr;

#define TypeOfPlaceType_AstronomicalBody_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_AstronomicalBody_CollectionType")))
#define CreatePlaceType_AstronomicalBody_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_AstronomicalBody_CollectionType))
class Mp7JrsPlaceType_AstronomicalBody_CollectionType;
class IMp7JrsPlaceType_AstronomicalBody_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_AstronomicalBody_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_AstronomicalBody_CollectionType > Mp7JrsPlaceType_AstronomicalBody_CollectionPtr;

#define TypeOfPlaceType_ElectronicAddress_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_ElectronicAddress_CollectionType")))
#define CreatePlaceType_ElectronicAddress_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_ElectronicAddress_CollectionType))
class Mp7JrsPlaceType_ElectronicAddress_CollectionType;
class IMp7JrsPlaceType_ElectronicAddress_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_ElectronicAddress_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_ElectronicAddress_CollectionType > Mp7JrsPlaceType_ElectronicAddress_CollectionPtr;

#define TypeOfPlaceType_GeographicPosition_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_GeographicPosition_LocalType")))
#define CreatePlaceType_GeographicPosition_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_GeographicPosition_LocalType))
class Mp7JrsPlaceType_GeographicPosition_LocalType;
class IMp7JrsPlaceType_GeographicPosition_LocalType;
/** Smart pointer for instance of IMp7JrsPlaceType_GeographicPosition_LocalType */
typedef Dc1Ptr< IMp7JrsPlaceType_GeographicPosition_LocalType > Mp7JrsPlaceType_GeographicPosition_LocalPtr;

#define TypeOfPlaceType_GeographicPosition_Point_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_GeographicPosition_Point_CollectionType")))
#define CreatePlaceType_GeographicPosition_Point_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_GeographicPosition_Point_CollectionType))
class Mp7JrsPlaceType_GeographicPosition_Point_CollectionType;
class IMp7JrsPlaceType_GeographicPosition_Point_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_GeographicPosition_Point_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_GeographicPosition_Point_CollectionType > Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr;

#define TypeOfPlaceType_GeographicPosition_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_GeographicPosition_type_LocalType")))
#define CreatePlaceType_GeographicPosition_type_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_GeographicPosition_type_LocalType))
class Mp7JrsPlaceType_GeographicPosition_type_LocalType;
class IMp7JrsPlaceType_GeographicPosition_type_LocalType;
// No smart pointer instance for IMp7JrsPlaceType_GeographicPosition_type_LocalType

#define TypeOfPlaceType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_Name_CollectionType")))
#define CreatePlaceType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_Name_CollectionType))
class Mp7JrsPlaceType_Name_CollectionType;
class IMp7JrsPlaceType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_Name_CollectionType > Mp7JrsPlaceType_Name_CollectionPtr;

#define TypeOfPlaceType_NameTerm_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_NameTerm_CollectionType")))
#define CreatePlaceType_NameTerm_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_NameTerm_CollectionType))
class Mp7JrsPlaceType_NameTerm_CollectionType;
class IMp7JrsPlaceType_NameTerm_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_NameTerm_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_NameTerm_CollectionType > Mp7JrsPlaceType_NameTerm_CollectionPtr;

#define TypeOfPlaceType_PlaceDescription_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_PlaceDescription_CollectionType")))
#define CreatePlaceType_PlaceDescription_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_PlaceDescription_CollectionType))
class Mp7JrsPlaceType_PlaceDescription_CollectionType;
class IMp7JrsPlaceType_PlaceDescription_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_PlaceDescription_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_PlaceDescription_CollectionType > Mp7JrsPlaceType_PlaceDescription_CollectionPtr;

#define TypeOfPlaceType_PostalAddress_AddressLine_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_PostalAddress_AddressLine_CollectionType")))
#define CreatePlaceType_PostalAddress_AddressLine_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_PostalAddress_AddressLine_CollectionType))
class Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionType;
class IMp7JrsPlaceType_PostalAddress_AddressLine_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_PostalAddress_AddressLine_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_PostalAddress_AddressLine_CollectionType > Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr;

#define TypeOfPlaceType_PostalAddress_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_PostalAddress_LocalType")))
#define CreatePlaceType_PostalAddress_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_PostalAddress_LocalType))
class Mp7JrsPlaceType_PostalAddress_LocalType;
class IMp7JrsPlaceType_PostalAddress_LocalType;
/** Smart pointer for instance of IMp7JrsPlaceType_PostalAddress_LocalType */
typedef Dc1Ptr< IMp7JrsPlaceType_PostalAddress_LocalType > Mp7JrsPlaceType_PostalAddress_LocalPtr;

#define TypeOfPlaceType_Region_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_Region_CollectionType")))
#define CreatePlaceType_Region_CollectionType (Dc1Factory::CreateObject(TypeOfPlaceType_Region_CollectionType))
class Mp7JrsPlaceType_Region_CollectionType;
class IMp7JrsPlaceType_Region_CollectionType;
/** Smart pointer for instance of IMp7JrsPlaceType_Region_CollectionType */
typedef Dc1Ptr< IMp7JrsPlaceType_Region_CollectionType > Mp7JrsPlaceType_Region_CollectionPtr;

#define TypeOfPlaceType_StructuredInternalCoordinates_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredInternalCoordinates_LocalType")))
#define CreatePlaceType_StructuredInternalCoordinates_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_StructuredInternalCoordinates_LocalType))
class Mp7JrsPlaceType_StructuredInternalCoordinates_LocalType;
class IMp7JrsPlaceType_StructuredInternalCoordinates_LocalType;
/** Smart pointer for instance of IMp7JrsPlaceType_StructuredInternalCoordinates_LocalType */
typedef Dc1Ptr< IMp7JrsPlaceType_StructuredInternalCoordinates_LocalType > Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr;

#define TypeOfPlaceType_StructuredPostalAddress_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredPostalAddress_LocalType")))
#define CreatePlaceType_StructuredPostalAddress_LocalType (Dc1Factory::CreateObject(TypeOfPlaceType_StructuredPostalAddress_LocalType))
class Mp7JrsPlaceType_StructuredPostalAddress_LocalType;
class IMp7JrsPlaceType_StructuredPostalAddress_LocalType;
/** Smart pointer for instance of IMp7JrsPlaceType_StructuredPostalAddress_LocalType */
typedef Dc1Ptr< IMp7JrsPlaceType_StructuredPostalAddress_LocalType > Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr;

#define TypeOfPointOfViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PointOfViewType")))
#define CreatePointOfViewType (Dc1Factory::CreateObject(TypeOfPointOfViewType))
class Mp7JrsPointOfViewType;
class IMp7JrsPointOfViewType;
/** Smart pointer for instance of IMp7JrsPointOfViewType */
typedef Dc1Ptr< IMp7JrsPointOfViewType > Mp7JrsPointOfViewPtr;

#define TypeOfPointOfViewType_Importance_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PointOfViewType_Importance_CollectionType")))
#define CreatePointOfViewType_Importance_CollectionType (Dc1Factory::CreateObject(TypeOfPointOfViewType_Importance_CollectionType))
class Mp7JrsPointOfViewType_Importance_CollectionType;
class IMp7JrsPointOfViewType_Importance_CollectionType;
/** Smart pointer for instance of IMp7JrsPointOfViewType_Importance_CollectionType */
typedef Dc1Ptr< IMp7JrsPointOfViewType_Importance_CollectionType > Mp7JrsPointOfViewType_Importance_CollectionPtr;

#define TypeOfPointOfViewType_Importance_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PointOfViewType_Importance_LocalType")))
#define CreatePointOfViewType_Importance_LocalType (Dc1Factory::CreateObject(TypeOfPointOfViewType_Importance_LocalType))
class Mp7JrsPointOfViewType_Importance_LocalType;
class IMp7JrsPointOfViewType_Importance_LocalType;
/** Smart pointer for instance of IMp7JrsPointOfViewType_Importance_LocalType */
typedef Dc1Ptr< IMp7JrsPointOfViewType_Importance_LocalType > Mp7JrsPointOfViewType_Importance_LocalPtr;

#define TypeOfPoissonDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PoissonDistributionType")))
#define CreatePoissonDistributionType (Dc1Factory::CreateObject(TypeOfPoissonDistributionType))
class Mp7JrsPoissonDistributionType;
class IMp7JrsPoissonDistributionType;
/** Smart pointer for instance of IMp7JrsPoissonDistributionType */
typedef Dc1Ptr< IMp7JrsPoissonDistributionType > Mp7JrsPoissonDistributionPtr;

#define TypeOfPositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PositionType")))
#define CreatePositionType (Dc1Factory::CreateObject(TypeOfPositionType))
class Mp7JrsPositionType;
class IMp7JrsPositionType;
/** Smart pointer for instance of IMp7JrsPositionType */
typedef Dc1Ptr< IMp7JrsPositionType > Mp7JrsPositionPtr;

#define TypeOfPreferenceConditionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType")))
#define CreatePreferenceConditionType (Dc1Factory::CreateObject(TypeOfPreferenceConditionType))
class Mp7JrsPreferenceConditionType;
class IMp7JrsPreferenceConditionType;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType > Mp7JrsPreferenceConditionPtr;

#define TypeOfPreferenceConditionType_Time_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_CollectionType")))
#define CreatePreferenceConditionType_Time_CollectionType (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_CollectionType))
class Mp7JrsPreferenceConditionType_Time_CollectionType;
class IMp7JrsPreferenceConditionType_Time_CollectionType;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType_Time_CollectionType */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType_Time_CollectionType > Mp7JrsPreferenceConditionType_Time_CollectionPtr;

#define TypeOfPreferenceConditionType_Time_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_LocalType")))
#define CreatePreferenceConditionType_Time_LocalType (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_LocalType))
class Mp7JrsPreferenceConditionType_Time_LocalType;
class IMp7JrsPreferenceConditionType_Time_LocalType;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType_Time_LocalType */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType_Time_LocalType > Mp7JrsPreferenceConditionType_Time_LocalPtr;

#define TypeOfPreferenceConditionType_Time_recurrence_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_recurrence_LocalType")))
#define CreatePreferenceConditionType_Time_recurrence_LocalType (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_recurrence_LocalType))
class Mp7JrsPreferenceConditionType_Time_recurrence_LocalType;
class IMp7JrsPreferenceConditionType_Time_recurrence_LocalType;
// No smart pointer instance for IMp7JrsPreferenceConditionType_Time_recurrence_LocalType

#define TypeOfPreferenceConditionType_Time_recurrence_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_recurrence_LocalType0")))
#define CreatePreferenceConditionType_Time_recurrence_LocalType0 (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_recurrence_LocalType0))
class Mp7JrsPreferenceConditionType_Time_recurrence_LocalType0;
class IMp7JrsPreferenceConditionType_Time_recurrence_LocalType0;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType_Time_recurrence_LocalType0 */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType_Time_recurrence_LocalType0 > Mp7JrsPreferenceConditionType_Time_recurrence_Local0Ptr;

#define TypeOfPreferenceConditionType_Time_recurrence_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_recurrence_LocalType1")))
#define CreatePreferenceConditionType_Time_recurrence_LocalType1 (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_recurrence_LocalType1))
class Mp7JrsPreferenceConditionType_Time_recurrence_LocalType1;
class IMp7JrsPreferenceConditionType_Time_recurrence_LocalType1;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType_Time_recurrence_LocalType1 */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType_Time_recurrence_LocalType1 > Mp7JrsPreferenceConditionType_Time_recurrence_Local1Ptr;

#define TypeOfPreferenceConditionType_Time_recurrence_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_recurrence_LocalType2")))
#define CreatePreferenceConditionType_Time_recurrence_LocalType2 (Dc1Factory::CreateObject(TypeOfPreferenceConditionType_Time_recurrence_LocalType2))
class Mp7JrsPreferenceConditionType_Time_recurrence_LocalType2;
class IMp7JrsPreferenceConditionType_Time_recurrence_LocalType2;
/** Smart pointer for instance of IMp7JrsPreferenceConditionType_Time_recurrence_LocalType2 */
typedef Dc1Ptr< IMp7JrsPreferenceConditionType_Time_recurrence_LocalType2 > Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr;

#define TypeOfpreferenceValueType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:preferenceValueType")))
#define CreatepreferenceValueType (Dc1Factory::CreateObject(TypeOfpreferenceValueType))
class Mp7JrspreferenceValueType;
class IMp7JrspreferenceValueType;
/** Smart pointer for instance of IMp7JrspreferenceValueType */
typedef Dc1Ptr< IMp7JrspreferenceValueType > Mp7JrspreferenceValuePtr;

#define TypeOfProbabilityClassificationModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityClassificationModelType")))
#define CreateProbabilityClassificationModelType (Dc1Factory::CreateObject(TypeOfProbabilityClassificationModelType))
class Mp7JrsProbabilityClassificationModelType;
class IMp7JrsProbabilityClassificationModelType;
/** Smart pointer for instance of IMp7JrsProbabilityClassificationModelType */
typedef Dc1Ptr< IMp7JrsProbabilityClassificationModelType > Mp7JrsProbabilityClassificationModelPtr;

#define TypeOfProbabilityClassificationModelType_ProbabilityModelClass_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityClassificationModelType_ProbabilityModelClass_CollectionType")))
#define CreateProbabilityClassificationModelType_ProbabilityModelClass_CollectionType (Dc1Factory::CreateObject(TypeOfProbabilityClassificationModelType_ProbabilityModelClass_CollectionType))
class Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionType;
class IMp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionType;
/** Smart pointer for instance of IMp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionType */
typedef Dc1Ptr< IMp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionType > Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr;

#define TypeOfProbabilityDistributionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType")))
#define CreateProbabilityDistributionType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType))
class Mp7JrsProbabilityDistributionType;
class IMp7JrsProbabilityDistributionType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType > Mp7JrsProbabilityDistributionPtr;

#define TypeOfProbabilityDistributionType_Cumulant_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Cumulant_CollectionType")))
#define CreateProbabilityDistributionType_Cumulant_CollectionType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Cumulant_CollectionType))
class Mp7JrsProbabilityDistributionType_Cumulant_CollectionType;
class IMp7JrsProbabilityDistributionType_Cumulant_CollectionType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Cumulant_CollectionType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Cumulant_CollectionType > Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr;

#define TypeOfProbabilityDistributionType_Cumulant_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Cumulant_LocalType")))
#define CreateProbabilityDistributionType_Cumulant_LocalType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Cumulant_LocalType))
class Mp7JrsProbabilityDistributionType_Cumulant_LocalType;
class IMp7JrsProbabilityDistributionType_Cumulant_LocalType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Cumulant_LocalType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Cumulant_LocalType > Mp7JrsProbabilityDistributionType_Cumulant_LocalPtr;

#define TypeOfProbabilityDistributionType_Cumulant_Value_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Cumulant_Value_LocalType")))
#define CreateProbabilityDistributionType_Cumulant_Value_LocalType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Cumulant_Value_LocalType))
class Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType;
class IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType > Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalPtr;

#define TypeOfProbabilityDistributionType_Moment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Moment_CollectionType")))
#define CreateProbabilityDistributionType_Moment_CollectionType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Moment_CollectionType))
class Mp7JrsProbabilityDistributionType_Moment_CollectionType;
class IMp7JrsProbabilityDistributionType_Moment_CollectionType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Moment_CollectionType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Moment_CollectionType > Mp7JrsProbabilityDistributionType_Moment_CollectionPtr;

#define TypeOfProbabilityDistributionType_Moment_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Moment_LocalType")))
#define CreateProbabilityDistributionType_Moment_LocalType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Moment_LocalType))
class Mp7JrsProbabilityDistributionType_Moment_LocalType;
class IMp7JrsProbabilityDistributionType_Moment_LocalType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Moment_LocalType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Moment_LocalType > Mp7JrsProbabilityDistributionType_Moment_LocalPtr;

#define TypeOfProbabilityDistributionType_Moment_Value_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Moment_Value_LocalType")))
#define CreateProbabilityDistributionType_Moment_Value_LocalType (Dc1Factory::CreateObject(TypeOfProbabilityDistributionType_Moment_Value_LocalType))
class Mp7JrsProbabilityDistributionType_Moment_Value_LocalType;
class IMp7JrsProbabilityDistributionType_Moment_Value_LocalType;
/** Smart pointer for instance of IMp7JrsProbabilityDistributionType_Moment_Value_LocalType */
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Moment_Value_LocalType > Mp7JrsProbabilityDistributionType_Moment_Value_LocalPtr;

#define TypeOfProbabilityMatrixType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityMatrixType")))
#define CreateProbabilityMatrixType (Dc1Factory::CreateObject(TypeOfProbabilityMatrixType))
class Mp7JrsProbabilityMatrixType;
class IMp7JrsProbabilityMatrixType;
/** Smart pointer for instance of IMp7JrsProbabilityMatrixType */
typedef Dc1Ptr< IMp7JrsProbabilityMatrixType > Mp7JrsProbabilityMatrixPtr;

#define TypeOfProbabilityModelClassType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityModelClassType")))
#define CreateProbabilityModelClassType (Dc1Factory::CreateObject(TypeOfProbabilityModelClassType))
class Mp7JrsProbabilityModelClassType;
class IMp7JrsProbabilityModelClassType;
/** Smart pointer for instance of IMp7JrsProbabilityModelClassType */
typedef Dc1Ptr< IMp7JrsProbabilityModelClassType > Mp7JrsProbabilityModelClassPtr;

#define TypeOfProbabilityModelClassType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityModelClassType_CollectionType")))
#define CreateProbabilityModelClassType_CollectionType (Dc1Factory::CreateObject(TypeOfProbabilityModelClassType_CollectionType))
class Mp7JrsProbabilityModelClassType_CollectionType;
class IMp7JrsProbabilityModelClassType_CollectionType;
/** Smart pointer for instance of IMp7JrsProbabilityModelClassType_CollectionType */
typedef Dc1Ptr< IMp7JrsProbabilityModelClassType_CollectionType > Mp7JrsProbabilityModelClassType_CollectionPtr;

#define TypeOfProbabilityModelClassType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityModelClassType_LocalType")))
#define CreateProbabilityModelClassType_LocalType (Dc1Factory::CreateObject(TypeOfProbabilityModelClassType_LocalType))
class Mp7JrsProbabilityModelClassType_LocalType;
class IMp7JrsProbabilityModelClassType_LocalType;
/** Smart pointer for instance of IMp7JrsProbabilityModelClassType_LocalType */
typedef Dc1Ptr< IMp7JrsProbabilityModelClassType_LocalType > Mp7JrsProbabilityModelClassType_LocalPtr;

#define TypeOfProbabilityModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityModelType")))
#define CreateProbabilityModelType (Dc1Factory::CreateObject(TypeOfProbabilityModelType))
class Mp7JrsProbabilityModelType;
class IMp7JrsProbabilityModelType;
/** Smart pointer for instance of IMp7JrsProbabilityModelType */
typedef Dc1Ptr< IMp7JrsProbabilityModelType > Mp7JrsProbabilityModelPtr;

#define TypeOfprobabilityVector (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:probabilityVector")))
#define CreateprobabilityVector (Dc1Factory::CreateObject(TypeOfprobabilityVector))
class Mp7JrsprobabilityVector;
class IMp7JrsprobabilityVector;
/** Smart pointer for instance of IMp7JrsprobabilityVector */
typedef Dc1Ptr< IMp7JrsprobabilityVector > Mp7JrsprobabilityVectorPtr;

#define TypeOfprobabilityVector_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:probabilityVector_LocalType")))
#define CreateprobabilityVector_LocalType (Dc1Factory::CreateObject(TypeOfprobabilityVector_LocalType))
class Mp7JrsprobabilityVector_LocalType;
class IMp7JrsprobabilityVector_LocalType;
/** Smart pointer for instance of IMp7JrsprobabilityVector_LocalType */
typedef Dc1Ptr< IMp7JrsprobabilityVector_LocalType > Mp7JrsprobabilityVector_LocalPtr;

#define TypeOfProfileWeightsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProfileWeightsType")))
#define CreateProfileWeightsType (Dc1Factory::CreateObject(TypeOfProfileWeightsType))
class Mp7JrsProfileWeightsType;
class IMp7JrsProfileWeightsType;
/** Smart pointer for instance of IMp7JrsProfileWeightsType */
typedef Dc1Ptr< IMp7JrsProfileWeightsType > Mp7JrsProfileWeightsPtr;

#define TypeOfPullbackDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PullbackDefinitionType")))
#define CreatePullbackDefinitionType (Dc1Factory::CreateObject(TypeOfPullbackDefinitionType))
class Mp7JrsPullbackDefinitionType;
class IMp7JrsPullbackDefinitionType;
/** Smart pointer for instance of IMp7JrsPullbackDefinitionType */
typedef Dc1Ptr< IMp7JrsPullbackDefinitionType > Mp7JrsPullbackDefinitionPtr;

#define TypeOfPushoutDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PushoutDefinitionType")))
#define CreatePushoutDefinitionType (Dc1Factory::CreateObject(TypeOfPushoutDefinitionType))
class Mp7JrsPushoutDefinitionType;
class IMp7JrsPushoutDefinitionType;
/** Smart pointer for instance of IMp7JrsPushoutDefinitionType */
typedef Dc1Ptr< IMp7JrsPushoutDefinitionType > Mp7JrsPushoutDefinitionPtr;

#define TypeOfQCItemResultType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType")))
#define CreateQCItemResultType (Dc1Factory::CreateObject(TypeOfQCItemResultType))
class Mp7JrsQCItemResultType;
class IMp7JrsQCItemResultType;
/** Smart pointer for instance of IMp7JrsQCItemResultType */
typedef Dc1Ptr< IMp7JrsQCItemResultType > Mp7JrsQCItemResultPtr;

#define TypeOfQCItemResultType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType_CollectionType")))
#define CreateQCItemResultType_CollectionType (Dc1Factory::CreateObject(TypeOfQCItemResultType_CollectionType))
class Mp7JrsQCItemResultType_CollectionType;
class IMp7JrsQCItemResultType_CollectionType;
/** Smart pointer for instance of IMp7JrsQCItemResultType_CollectionType */
typedef Dc1Ptr< IMp7JrsQCItemResultType_CollectionType > Mp7JrsQCItemResultType_CollectionPtr;

#define TypeOfQCItemResultType_DetectionMethod_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType_DetectionMethod_LocalType")))
#define CreateQCItemResultType_DetectionMethod_LocalType (Dc1Factory::CreateObject(TypeOfQCItemResultType_DetectionMethod_LocalType))
class Mp7JrsQCItemResultType_DetectionMethod_LocalType;
class IMp7JrsQCItemResultType_DetectionMethod_LocalType;
// No smart pointer instance for IMp7JrsQCItemResultType_DetectionMethod_LocalType

#define TypeOfQCItemResultType_ExecutionStatus_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType_ExecutionStatus_LocalType")))
#define CreateQCItemResultType_ExecutionStatus_LocalType (Dc1Factory::CreateObject(TypeOfQCItemResultType_ExecutionStatus_LocalType))
class Mp7JrsQCItemResultType_ExecutionStatus_LocalType;
class IMp7JrsQCItemResultType_ExecutionStatus_LocalType;
// No smart pointer instance for IMp7JrsQCItemResultType_ExecutionStatus_LocalType

#define TypeOfQCItemResultType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType_LocalType")))
#define CreateQCItemResultType_LocalType (Dc1Factory::CreateObject(TypeOfQCItemResultType_LocalType))
class Mp7JrsQCItemResultType_LocalType;
class IMp7JrsQCItemResultType_LocalType;
/** Smart pointer for instance of IMp7JrsQCItemResultType_LocalType */
typedef Dc1Ptr< IMp7JrsQCItemResultType_LocalType > Mp7JrsQCItemResultType_LocalPtr;

#define TypeOfQCItemResultType_VerificationMedia_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType_VerificationMedia_CollectionType")))
#define CreateQCItemResultType_VerificationMedia_CollectionType (Dc1Factory::CreateObject(TypeOfQCItemResultType_VerificationMedia_CollectionType))
class Mp7JrsQCItemResultType_VerificationMedia_CollectionType;
class IMp7JrsQCItemResultType_VerificationMedia_CollectionType;
/** Smart pointer for instance of IMp7JrsQCItemResultType_VerificationMedia_CollectionType */
typedef Dc1Ptr< IMp7JrsQCItemResultType_VerificationMedia_CollectionType > Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr;

#define TypeOfQCProfileType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType")))
#define CreateQCProfileType (Dc1Factory::CreateObject(TypeOfQCProfileType))
class Mp7JrsQCProfileType;
class IMp7JrsQCProfileType;
/** Smart pointer for instance of IMp7JrsQCProfileType */
typedef Dc1Ptr< IMp7JrsQCProfileType > Mp7JrsQCProfilePtr;

#define TypeOfQCProfileType_CheckResultRule_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_CheckResultRule_LocalType")))
#define CreateQCProfileType_CheckResultRule_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_CheckResultRule_LocalType))
class Mp7JrsQCProfileType_CheckResultRule_LocalType;
class IMp7JrsQCProfileType_CheckResultRule_LocalType;
// No smart pointer instance for IMp7JrsQCProfileType_CheckResultRule_LocalType

#define TypeOfQCProfileType_QCItem_Category_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Category_LocalType")))
#define CreateQCProfileType_QCItem_Category_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_Category_LocalType))
class Mp7JrsQCProfileType_QCItem_Category_LocalType;
class IMp7JrsQCProfileType_QCItem_Category_LocalType;
// No smart pointer instance for IMp7JrsQCProfileType_QCItem_Category_LocalType

#define TypeOfQCProfileType_QCItem_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_CollectionType")))
#define CreateQCProfileType_QCItem_CollectionType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_CollectionType))
class Mp7JrsQCProfileType_QCItem_CollectionType;
class IMp7JrsQCProfileType_QCItem_CollectionType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_CollectionType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_CollectionType > Mp7JrsQCProfileType_QCItem_CollectionPtr;

#define TypeOfQCProfileType_QCItem_EssenceType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_EssenceType_CollectionType")))
#define CreateQCProfileType_QCItem_EssenceType_CollectionType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_EssenceType_CollectionType))
class Mp7JrsQCProfileType_QCItem_EssenceType_CollectionType;
class IMp7JrsQCProfileType_QCItem_EssenceType_CollectionType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_EssenceType_CollectionType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_EssenceType_CollectionType > Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr;

#define TypeOfQCProfileType_QCItem_EssenceType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_EssenceType_LocalType")))
#define CreateQCProfileType_QCItem_EssenceType_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_EssenceType_LocalType))
class Mp7JrsQCProfileType_QCItem_EssenceType_LocalType;
class IMp7JrsQCProfileType_QCItem_EssenceType_LocalType;
// No smart pointer instance for IMp7JrsQCProfileType_QCItem_EssenceType_LocalType

#define TypeOfQCProfileType_QCItem_InputParameter_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_InputParameter_CollectionType")))
#define CreateQCProfileType_QCItem_InputParameter_CollectionType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_InputParameter_CollectionType))
class Mp7JrsQCProfileType_QCItem_InputParameter_CollectionType;
class IMp7JrsQCProfileType_QCItem_InputParameter_CollectionType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_InputParameter_CollectionType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_InputParameter_CollectionType > Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr;

#define TypeOfQCProfileType_QCItem_ItemScope_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_ItemScope_CollectionType")))
#define CreateQCProfileType_QCItem_ItemScope_CollectionType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_ItemScope_CollectionType))
class Mp7JrsQCProfileType_QCItem_ItemScope_CollectionType;
class IMp7JrsQCProfileType_QCItem_ItemScope_CollectionType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_ItemScope_CollectionType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_ItemScope_CollectionType > Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr;

#define TypeOfQCProfileType_QCItem_Layer_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Layer_LocalType")))
#define CreateQCProfileType_QCItem_Layer_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_Layer_LocalType))
class Mp7JrsQCProfileType_QCItem_Layer_LocalType;
class IMp7JrsQCProfileType_QCItem_Layer_LocalType;
// No smart pointer instance for IMp7JrsQCProfileType_QCItem_Layer_LocalType

#define TypeOfQCProfileType_QCItem_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_LocalType")))
#define CreateQCProfileType_QCItem_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_LocalType))
class Mp7JrsQCProfileType_QCItem_LocalType;
class IMp7JrsQCProfileType_QCItem_LocalType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_LocalType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_LocalType > Mp7JrsQCProfileType_QCItem_LocalPtr;

#define TypeOfQCProfileType_QCItem_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Name_CollectionType")))
#define CreateQCProfileType_QCItem_Name_CollectionType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_Name_CollectionType))
class Mp7JrsQCProfileType_QCItem_Name_CollectionType;
class IMp7JrsQCProfileType_QCItem_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_Name_CollectionType > Mp7JrsQCProfileType_QCItem_Name_CollectionPtr;

#define TypeOfQCProfileType_QCItem_Relevance_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Relevance_LocalType")))
#define CreateQCProfileType_QCItem_Relevance_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_QCItem_Relevance_LocalType))
class Mp7JrsQCProfileType_QCItem_Relevance_LocalType;
class IMp7JrsQCProfileType_QCItem_Relevance_LocalType;
/** Smart pointer for instance of IMp7JrsQCProfileType_QCItem_Relevance_LocalType */
typedef Dc1Ptr< IMp7JrsQCProfileType_QCItem_Relevance_LocalType > Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr;

#define TypeOfQCProfileType_RelevanceLevel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_RelevanceLevel_LocalType")))
#define CreateQCProfileType_RelevanceLevel_LocalType (Dc1Factory::CreateObject(TypeOfQCProfileType_RelevanceLevel_LocalType))
class Mp7JrsQCProfileType_RelevanceLevel_LocalType;
class IMp7JrsQCProfileType_RelevanceLevel_LocalType;
/** Smart pointer for instance of IMp7JrsQCProfileType_RelevanceLevel_LocalType */
typedef Dc1Ptr< IMp7JrsQCProfileType_RelevanceLevel_LocalType > Mp7JrsQCProfileType_RelevanceLevel_LocalPtr;

#define TypeOfQCValueType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCValueType")))
#define CreateQCValueType (Dc1Factory::CreateObject(TypeOfQCValueType))
class Mp7JrsQCValueType;
class IMp7JrsQCValueType;
/** Smart pointer for instance of IMp7JrsQCValueType */
typedef Dc1Ptr< IMp7JrsQCValueType > Mp7JrsQCValuePtr;

#define TypeOfQCValueType_track_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCValueType_track_LocalType")))
#define CreateQCValueType_track_LocalType (Dc1Factory::CreateObject(TypeOfQCValueType_track_LocalType))
class Mp7JrsQCValueType_track_LocalType;
class IMp7JrsQCValueType_track_LocalType;
/** Smart pointer for instance of IMp7JrsQCValueType_track_LocalType */
typedef Dc1Ptr< IMp7JrsQCValueType_track_LocalType > Mp7JrsQCValueType_track_LocalPtr;

#define TypeOfRatingType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType")))
#define CreateRatingType (Dc1Factory::CreateObject(TypeOfRatingType))
class Mp7JrsRatingType;
class IMp7JrsRatingType;
/** Smart pointer for instance of IMp7JrsRatingType */
typedef Dc1Ptr< IMp7JrsRatingType > Mp7JrsRatingPtr;

#define TypeOfRatingType_RatingScheme_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType_RatingScheme_LocalType")))
#define CreateRatingType_RatingScheme_LocalType (Dc1Factory::CreateObject(TypeOfRatingType_RatingScheme_LocalType))
class Mp7JrsRatingType_RatingScheme_LocalType;
class IMp7JrsRatingType_RatingScheme_LocalType;
/** Smart pointer for instance of IMp7JrsRatingType_RatingScheme_LocalType */
typedef Dc1Ptr< IMp7JrsRatingType_RatingScheme_LocalType > Mp7JrsRatingType_RatingScheme_LocalPtr;

#define TypeOfRatingType_RatingScheme_style_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType_RatingScheme_style_LocalType")))
#define CreateRatingType_RatingScheme_style_LocalType (Dc1Factory::CreateObject(TypeOfRatingType_RatingScheme_style_LocalType))
class Mp7JrsRatingType_RatingScheme_style_LocalType;
class IMp7JrsRatingType_RatingScheme_style_LocalType;
// No smart pointer instance for IMp7JrsRatingType_RatingScheme_style_LocalType

#define TypeOfRecordingPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingPreferencesType")))
#define CreateRecordingPreferencesType (Dc1Factory::CreateObject(TypeOfRecordingPreferencesType))
class Mp7JrsRecordingPreferencesType;
class IMp7JrsRecordingPreferencesType;
/** Smart pointer for instance of IMp7JrsRecordingPreferencesType */
typedef Dc1Ptr< IMp7JrsRecordingPreferencesType > Mp7JrsRecordingPreferencesPtr;

#define TypeOfRecordingPreferencesType_RecordingRequest_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingPreferencesType_RecordingRequest_CollectionType")))
#define CreateRecordingPreferencesType_RecordingRequest_CollectionType (Dc1Factory::CreateObject(TypeOfRecordingPreferencesType_RecordingRequest_CollectionType))
class Mp7JrsRecordingPreferencesType_RecordingRequest_CollectionType;
class IMp7JrsRecordingPreferencesType_RecordingRequest_CollectionType;
/** Smart pointer for instance of IMp7JrsRecordingPreferencesType_RecordingRequest_CollectionType */
typedef Dc1Ptr< IMp7JrsRecordingPreferencesType_RecordingRequest_CollectionType > Mp7JrsRecordingPreferencesType_RecordingRequest_CollectionPtr;

#define TypeOfRecordingRequestType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType")))
#define CreateRecordingRequestType (Dc1Factory::CreateObject(TypeOfRecordingRequestType))
class Mp7JrsRecordingRequestType;
class IMp7JrsRecordingRequestType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType > Mp7JrsRecordingRequestPtr;

#define TypeOfRecordingRequestType_RecordAssociatedData_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_LocalType")))
#define CreateRecordingRequestType_RecordAssociatedData_LocalType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordAssociatedData_LocalType))
class Mp7JrsRecordingRequestType_RecordAssociatedData_LocalType;
class IMp7JrsRecordingRequestType_RecordAssociatedData_LocalType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordAssociatedData_LocalType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordAssociatedData_LocalType > Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr;

#define TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_type_LocalType")))
#define CreateRecordingRequestType_RecordAssociatedData_type_LocalType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType))
class Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType;
class IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType;
// No smart pointer instance for IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType

#define TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_type_LocalType0")))
#define CreateRecordingRequestType_RecordAssociatedData_type_LocalType0 (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType0))
class Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType0;
class IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType0;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType0 */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType0 > Mp7JrsRecordingRequestType_RecordAssociatedData_type_Local0Ptr;

#define TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_type_LocalType1")))
#define CreateRecordingRequestType_RecordAssociatedData_type_LocalType1 (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType1))
class Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType1;
class IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType1;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType1 */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType1 > Mp7JrsRecordingRequestType_RecordAssociatedData_type_Local1Ptr;

#define TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_type_LocalType2")))
#define CreateRecordingRequestType_RecordAssociatedData_type_LocalType2 (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordAssociatedData_type_LocalType2))
class Mp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType2;
class IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType2;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType2 */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordAssociatedData_type_LocalType2 > Mp7JrsRecordingRequestType_RecordAssociatedData_type_Local2Ptr;

#define TypeOfRecordingRequestType_RecordingLocationPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingLocationPreferences_CollectionType")))
#define CreateRecordingRequestType_RecordingLocationPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordingLocationPreferences_CollectionType))
class Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType;
class IMp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType > Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr;

#define TypeOfRecordingRequestType_RecordingPeriod_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingPeriod_LocalType")))
#define CreateRecordingRequestType_RecordingPeriod_LocalType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordingPeriod_LocalType))
class Mp7JrsRecordingRequestType_RecordingPeriod_LocalType;
class IMp7JrsRecordingRequestType_RecordingPeriod_LocalType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordingPeriod_LocalType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingPeriod_LocalType > Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr;

#define TypeOfRecordingRequestType_RecordingSourceTypePreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingSourceTypePreferences_CollectionType")))
#define CreateRecordingRequestType_RecordingSourceTypePreferences_CollectionType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordingSourceTypePreferences_CollectionType))
class Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType;
class IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType > Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr;

#define TypeOfRecordingRequestType_RecordingSourceTypePreferences_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingSourceTypePreferences_LocalType")))
#define CreateRecordingRequestType_RecordingSourceTypePreferences_LocalType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordingSourceTypePreferences_LocalType))
class Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType;
class IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType > Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalPtr;

#define TypeOfRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType")))
#define CreateRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType (Dc1Factory::CreateObject(TypeOfRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType))
class Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType;
class IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType;
/** Smart pointer for instance of IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType */
typedef Dc1Ptr< IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType > Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr;

#define TypeOfReferencePitchType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType")))
#define CreateReferencePitchType (Dc1Factory::CreateObject(TypeOfReferencePitchType))
class Mp7JrsReferencePitchType;
class IMp7JrsReferencePitchType;
/** Smart pointer for instance of IMp7JrsReferencePitchType */
typedef Dc1Ptr< IMp7JrsReferencePitchType > Mp7JrsReferencePitchPtr;

#define TypeOfReferencePitchType_BaseFrequency_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BaseFrequency_LocalType")))
#define CreateReferencePitchType_BaseFrequency_LocalType (Dc1Factory::CreateObject(TypeOfReferencePitchType_BaseFrequency_LocalType))
class Mp7JrsReferencePitchType_BaseFrequency_LocalType;
class IMp7JrsReferencePitchType_BaseFrequency_LocalType;
/** Smart pointer for instance of IMp7JrsReferencePitchType_BaseFrequency_LocalType */
typedef Dc1Ptr< IMp7JrsReferencePitchType_BaseFrequency_LocalType > Mp7JrsReferencePitchType_BaseFrequency_LocalPtr;

#define TypeOfReferencePitchType_BasePitchNumber_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BasePitchNumber_LocalType")))
#define CreateReferencePitchType_BasePitchNumber_LocalType (Dc1Factory::CreateObject(TypeOfReferencePitchType_BasePitchNumber_LocalType))
class Mp7JrsReferencePitchType_BasePitchNumber_LocalType;
class IMp7JrsReferencePitchType_BasePitchNumber_LocalType;
/** Smart pointer for instance of IMp7JrsReferencePitchType_BasePitchNumber_LocalType */
typedef Dc1Ptr< IMp7JrsReferencePitchType_BasePitchNumber_LocalType > Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr;

#define TypeOfReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))
#define CreateReferenceType (Dc1Factory::CreateObject(TypeOfReferenceType))
class Mp7JrsReferenceType;
class IMp7JrsReferenceType;
/** Smart pointer for instance of IMp7JrsReferenceType */
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;

#define TypeOfregionCode (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:regionCode")))
#define CreateregionCode (Dc1Factory::CreateObject(TypeOfregionCode))
class Mp7JrsregionCode;
class IMp7JrsregionCode;
/** Smart pointer for instance of IMp7JrsregionCode */
typedef Dc1Ptr< IMp7JrsregionCode > Mp7JrsregionCodePtr;

#define TypeOfRegionLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType")))
#define CreateRegionLocatorType (Dc1Factory::CreateObject(TypeOfRegionLocatorType))
class Mp7JrsRegionLocatorType;
class IMp7JrsRegionLocatorType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType > Mp7JrsRegionLocatorPtr;

#define TypeOfRegionLocatorType_Box_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Box_CollectionType")))
#define CreateRegionLocatorType_Box_CollectionType (Dc1Factory::CreateObject(TypeOfRegionLocatorType_Box_CollectionType))
class Mp7JrsRegionLocatorType_Box_CollectionType;
class IMp7JrsRegionLocatorType_Box_CollectionType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType_Box_CollectionType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType_Box_CollectionType > Mp7JrsRegionLocatorType_Box_CollectionPtr;

#define TypeOfRegionLocatorType_Box_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Box_LocalType")))
#define CreateRegionLocatorType_Box_LocalType (Dc1Factory::CreateObject(TypeOfRegionLocatorType_Box_LocalType))
class Mp7JrsRegionLocatorType_Box_LocalType;
class IMp7JrsRegionLocatorType_Box_LocalType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType_Box_LocalType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType_Box_LocalType > Mp7JrsRegionLocatorType_Box_LocalPtr;

#define TypeOfRegionLocatorType_CoordRef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_CoordRef_LocalType")))
#define CreateRegionLocatorType_CoordRef_LocalType (Dc1Factory::CreateObject(TypeOfRegionLocatorType_CoordRef_LocalType))
class Mp7JrsRegionLocatorType_CoordRef_LocalType;
class IMp7JrsRegionLocatorType_CoordRef_LocalType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType_CoordRef_LocalType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType_CoordRef_LocalType > Mp7JrsRegionLocatorType_CoordRef_LocalPtr;

#define TypeOfRegionLocatorType_Polygon_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Polygon_CollectionType")))
#define CreateRegionLocatorType_Polygon_CollectionType (Dc1Factory::CreateObject(TypeOfRegionLocatorType_Polygon_CollectionType))
class Mp7JrsRegionLocatorType_Polygon_CollectionType;
class IMp7JrsRegionLocatorType_Polygon_CollectionType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType_Polygon_CollectionType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType_Polygon_CollectionType > Mp7JrsRegionLocatorType_Polygon_CollectionPtr;

#define TypeOfRegionLocatorType_Polygon_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Polygon_LocalType")))
#define CreateRegionLocatorType_Polygon_LocalType (Dc1Factory::CreateObject(TypeOfRegionLocatorType_Polygon_LocalType))
class Mp7JrsRegionLocatorType_Polygon_LocalType;
class IMp7JrsRegionLocatorType_Polygon_LocalType;
/** Smart pointer for instance of IMp7JrsRegionLocatorType_Polygon_LocalType */
typedef Dc1Ptr< IMp7JrsRegionLocatorType_Polygon_LocalType > Mp7JrsRegionLocatorType_Polygon_LocalPtr;

#define TypeOfRegionShapeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionShapeType")))
#define CreateRegionShapeType (Dc1Factory::CreateObject(TypeOfRegionShapeType))
class Mp7JrsRegionShapeType;
class IMp7JrsRegionShapeType;
/** Smart pointer for instance of IMp7JrsRegionShapeType */
typedef Dc1Ptr< IMp7JrsRegionShapeType > Mp7JrsRegionShapePtr;

#define TypeOfRegionShapeType_MagnitudeOfART_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionShapeType_MagnitudeOfART_CollectionType")))
#define CreateRegionShapeType_MagnitudeOfART_CollectionType (Dc1Factory::CreateObject(TypeOfRegionShapeType_MagnitudeOfART_CollectionType))
class Mp7JrsRegionShapeType_MagnitudeOfART_CollectionType;
class IMp7JrsRegionShapeType_MagnitudeOfART_CollectionType;
/** Smart pointer for instance of IMp7JrsRegionShapeType_MagnitudeOfART_CollectionType */
typedef Dc1Ptr< IMp7JrsRegionShapeType_MagnitudeOfART_CollectionType > Mp7JrsRegionShapeType_MagnitudeOfART_CollectionPtr;

#define TypeOfRegionShapeType_MagnitudeOfART_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionShapeType_MagnitudeOfART_LocalType")))
#define CreateRegionShapeType_MagnitudeOfART_LocalType (Dc1Factory::CreateObject(TypeOfRegionShapeType_MagnitudeOfART_LocalType))
class Mp7JrsRegionShapeType_MagnitudeOfART_LocalType;
class IMp7JrsRegionShapeType_MagnitudeOfART_LocalType;
/** Smart pointer for instance of IMp7JrsRegionShapeType_MagnitudeOfART_LocalType */
typedef Dc1Ptr< IMp7JrsRegionShapeType_MagnitudeOfART_LocalType > Mp7JrsRegionShapeType_MagnitudeOfART_LocalPtr;

#define TypeOfRegularVisualTimeSeriesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegularVisualTimeSeriesType")))
#define CreateRegularVisualTimeSeriesType (Dc1Factory::CreateObject(TypeOfRegularVisualTimeSeriesType))
class Mp7JrsRegularVisualTimeSeriesType;
class IMp7JrsRegularVisualTimeSeriesType;
/** Smart pointer for instance of IMp7JrsRegularVisualTimeSeriesType */
typedef Dc1Ptr< IMp7JrsRegularVisualTimeSeriesType > Mp7JrsRegularVisualTimeSeriesPtr;

#define TypeOfRegularVisualTimeSeriesType_Descriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegularVisualTimeSeriesType_Descriptor_CollectionType")))
#define CreateRegularVisualTimeSeriesType_Descriptor_CollectionType (Dc1Factory::CreateObject(TypeOfRegularVisualTimeSeriesType_Descriptor_CollectionType))
class Mp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionType;
class IMp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionType > Mp7JrsRegularVisualTimeSeriesType_Descriptor_CollectionPtr;

#define TypeOfRelatedMaterialType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelatedMaterialType")))
#define CreateRelatedMaterialType (Dc1Factory::CreateObject(TypeOfRelatedMaterialType))
class Mp7JrsRelatedMaterialType;
class IMp7JrsRelatedMaterialType;
/** Smart pointer for instance of IMp7JrsRelatedMaterialType */
typedef Dc1Ptr< IMp7JrsRelatedMaterialType > Mp7JrsRelatedMaterialPtr;

#define TypeOfRelationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelationType")))
#define CreateRelationType (Dc1Factory::CreateObject(TypeOfRelationType))
class Mp7JrsRelationType;
class IMp7JrsRelationType;
/** Smart pointer for instance of IMp7JrsRelationType */
typedef Dc1Ptr< IMp7JrsRelationType > Mp7JrsRelationPtr;

#define TypeOfRelativeDelayType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelativeDelayType")))
#define CreateRelativeDelayType (Dc1Factory::CreateObject(TypeOfRelativeDelayType))
class Mp7JrsRelativeDelayType;
class IMp7JrsRelativeDelayType;
/** Smart pointer for instance of IMp7JrsRelativeDelayType */
typedef Dc1Ptr< IMp7JrsRelativeDelayType > Mp7JrsRelativeDelayPtr;

#define TypeOfRelIncrTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelIncrTimePointType")))
#define CreateRelIncrTimePointType (Dc1Factory::CreateObject(TypeOfRelIncrTimePointType))
class Mp7JrsRelIncrTimePointType;
class IMp7JrsRelIncrTimePointType;
/** Smart pointer for instance of IMp7JrsRelIncrTimePointType */
typedef Dc1Ptr< IMp7JrsRelIncrTimePointType > Mp7JrsRelIncrTimePointPtr;

#define TypeOfRelTimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelTimePointType")))
#define CreateRelTimePointType (Dc1Factory::CreateObject(TypeOfRelTimePointType))
class Mp7JrsRelTimePointType;
class IMp7JrsRelTimePointType;
/** Smart pointer for instance of IMp7JrsRelTimePointType */
typedef Dc1Ptr< IMp7JrsRelTimePointType > Mp7JrsRelTimePointPtr;

#define TypeOfResolutionViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ResolutionViewType")))
#define CreateResolutionViewType (Dc1Factory::CreateObject(TypeOfResolutionViewType))
class Mp7JrsResolutionViewType;
class IMp7JrsResolutionViewType;
/** Smart pointer for instance of IMp7JrsResolutionViewType */
typedef Dc1Ptr< IMp7JrsResolutionViewType > Mp7JrsResolutionViewPtr;

#define TypeOfRhythmicBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RhythmicBaseType")))
#define CreateRhythmicBaseType (Dc1Factory::CreateObject(TypeOfRhythmicBaseType))
class Mp7JrsRhythmicBaseType;
class IMp7JrsRhythmicBaseType;
/** Smart pointer for instance of IMp7JrsRhythmicBaseType */
typedef Dc1Ptr< IMp7JrsRhythmicBaseType > Mp7JrsRhythmicBasePtr;

#define TypeOfRightsType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType")))
#define CreateRightsType (Dc1Factory::CreateObject(TypeOfRightsType))
class Mp7JrsRightsType;
class IMp7JrsRightsType;
/** Smart pointer for instance of IMp7JrsRightsType */
typedef Dc1Ptr< IMp7JrsRightsType > Mp7JrsRightsPtr;

#define TypeOfRightsType_RightsID_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType_RightsID_CollectionType")))
#define CreateRightsType_RightsID_CollectionType (Dc1Factory::CreateObject(TypeOfRightsType_RightsID_CollectionType))
class Mp7JrsRightsType_RightsID_CollectionType;
class IMp7JrsRightsType_RightsID_CollectionType;
/** Smart pointer for instance of IMp7JrsRightsType_RightsID_CollectionType */
typedef Dc1Ptr< IMp7JrsRightsType_RightsID_CollectionType > Mp7JrsRightsType_RightsID_CollectionPtr;

#define TypeOfScalableColorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType")))
#define CreateScalableColorType (Dc1Factory::CreateObject(TypeOfScalableColorType))
class Mp7JrsScalableColorType;
class IMp7JrsScalableColorType;
/** Smart pointer for instance of IMp7JrsScalableColorType */
typedef Dc1Ptr< IMp7JrsScalableColorType > Mp7JrsScalableColorPtr;

#define TypeOfScalableColorType_numOfBitplanesDiscarded_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType_numOfBitplanesDiscarded_LocalType")))
#define CreateScalableColorType_numOfBitplanesDiscarded_LocalType (Dc1Factory::CreateObject(TypeOfScalableColorType_numOfBitplanesDiscarded_LocalType))
class Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType;
class IMp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType;
// No smart pointer instance for IMp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType

#define TypeOfScalableColorType_numOfCoeff_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType_numOfCoeff_LocalType")))
#define CreateScalableColorType_numOfCoeff_LocalType (Dc1Factory::CreateObject(TypeOfScalableColorType_numOfCoeff_LocalType))
class Mp7JrsScalableColorType_numOfCoeff_LocalType;
class IMp7JrsScalableColorType_numOfCoeff_LocalType;
// No smart pointer instance for IMp7JrsScalableColorType_numOfCoeff_LocalType

#define TypeOfScalableSeriesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableSeriesType")))
#define CreateScalableSeriesType (Dc1Factory::CreateObject(TypeOfScalableSeriesType))
class Mp7JrsScalableSeriesType;
class IMp7JrsScalableSeriesType;
/** Smart pointer for instance of IMp7JrsScalableSeriesType */
typedef Dc1Ptr< IMp7JrsScalableSeriesType > Mp7JrsScalableSeriesPtr;

#define TypeOfScalableSeriesType_Scaling_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableSeriesType_Scaling_CollectionType")))
#define CreateScalableSeriesType_Scaling_CollectionType (Dc1Factory::CreateObject(TypeOfScalableSeriesType_Scaling_CollectionType))
class Mp7JrsScalableSeriesType_Scaling_CollectionType;
class IMp7JrsScalableSeriesType_Scaling_CollectionType;
/** Smart pointer for instance of IMp7JrsScalableSeriesType_Scaling_CollectionType */
typedef Dc1Ptr< IMp7JrsScalableSeriesType_Scaling_CollectionType > Mp7JrsScalableSeriesType_Scaling_CollectionPtr;

#define TypeOfScalableSeriesType_Scaling_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableSeriesType_Scaling_LocalType")))
#define CreateScalableSeriesType_Scaling_LocalType (Dc1Factory::CreateObject(TypeOfScalableSeriesType_Scaling_LocalType))
class Mp7JrsScalableSeriesType_Scaling_LocalType;
class IMp7JrsScalableSeriesType_Scaling_LocalType;
/** Smart pointer for instance of IMp7JrsScalableSeriesType_Scaling_LocalType */
typedef Dc1Ptr< IMp7JrsScalableSeriesType_Scaling_LocalType > Mp7JrsScalableSeriesType_Scaling_LocalPtr;

#define TypeOfscaleType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:scaleType")))
#define CreatescaleType (Dc1Factory::CreateObject(TypeOfscaleType))
class Mp7JrsscaleType;
class IMp7JrsscaleType;
/** Smart pointer for instance of IMp7JrsscaleType */
typedef Dc1Ptr< IMp7JrsscaleType > Mp7JrsscalePtr;

#define TypeOfSceneGraphMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SceneGraphMaskType")))
#define CreateSceneGraphMaskType (Dc1Factory::CreateObject(TypeOfSceneGraphMaskType))
class Mp7JrsSceneGraphMaskType;
class IMp7JrsSceneGraphMaskType;
/** Smart pointer for instance of IMp7JrsSceneGraphMaskType */
typedef Dc1Ptr< IMp7JrsSceneGraphMaskType > Mp7JrsSceneGraphMaskPtr;

#define TypeOfSceneGraphMaskType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SceneGraphMaskType_CollectionType")))
#define CreateSceneGraphMaskType_CollectionType (Dc1Factory::CreateObject(TypeOfSceneGraphMaskType_CollectionType))
class Mp7JrsSceneGraphMaskType_CollectionType;
class IMp7JrsSceneGraphMaskType_CollectionType;
/** Smart pointer for instance of IMp7JrsSceneGraphMaskType_CollectionType */
typedef Dc1Ptr< IMp7JrsSceneGraphMaskType_CollectionType > Mp7JrsSceneGraphMaskType_CollectionPtr;

#define TypeOfSceneGraphMaskType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SceneGraphMaskType_LocalType")))
#define CreateSceneGraphMaskType_LocalType (Dc1Factory::CreateObject(TypeOfSceneGraphMaskType_LocalType))
class Mp7JrsSceneGraphMaskType_LocalType;
class IMp7JrsSceneGraphMaskType_LocalType;
/** Smart pointer for instance of IMp7JrsSceneGraphMaskType_LocalType */
typedef Dc1Ptr< IMp7JrsSceneGraphMaskType_LocalType > Mp7JrsSceneGraphMaskType_LocalPtr;

#define TypeOfSchemaReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SchemaReferenceType")))
#define CreateSchemaReferenceType (Dc1Factory::CreateObject(TypeOfSchemaReferenceType))
class Mp7JrsSchemaReferenceType;
class IMp7JrsSchemaReferenceType;
/** Smart pointer for instance of IMp7JrsSchemaReferenceType */
typedef Dc1Ptr< IMp7JrsSchemaReferenceType > Mp7JrsSchemaReferencePtr;

#define TypeOfSegmentCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType")))
#define CreateSegmentCollectionType (Dc1Factory::CreateObject(TypeOfSegmentCollectionType))
class Mp7JrsSegmentCollectionType;
class IMp7JrsSegmentCollectionType;
/** Smart pointer for instance of IMp7JrsSegmentCollectionType */
typedef Dc1Ptr< IMp7JrsSegmentCollectionType > Mp7JrsSegmentCollectionPtr;

#define TypeOfSegmentCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType_CollectionType")))
#define CreateSegmentCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentCollectionType_CollectionType))
class Mp7JrsSegmentCollectionType_CollectionType;
class IMp7JrsSegmentCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentCollectionType_CollectionType > Mp7JrsSegmentCollectionType_CollectionPtr;

#define TypeOfSegmentCollectionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType_CollectionType0")))
#define CreateSegmentCollectionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfSegmentCollectionType_CollectionType0))
class Mp7JrsSegmentCollectionType_CollectionType0;
class IMp7JrsSegmentCollectionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsSegmentCollectionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsSegmentCollectionType_CollectionType0 > Mp7JrsSegmentCollectionType_Collection0Ptr;

#define TypeOfSegmentCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType_LocalType")))
#define CreateSegmentCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfSegmentCollectionType_LocalType))
class Mp7JrsSegmentCollectionType_LocalType;
class IMp7JrsSegmentCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsSegmentCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsSegmentCollectionType_LocalType > Mp7JrsSegmentCollectionType_LocalPtr;

#define TypeOfSegmentCollectionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType_LocalType0")))
#define CreateSegmentCollectionType_LocalType0 (Dc1Factory::CreateObject(TypeOfSegmentCollectionType_LocalType0))
class Mp7JrsSegmentCollectionType_LocalType0;
class IMp7JrsSegmentCollectionType_LocalType0;
/** Smart pointer for instance of IMp7JrsSegmentCollectionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsSegmentCollectionType_LocalType0 > Mp7JrsSegmentCollectionType_Local0Ptr;

#define TypeOfSegmentDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentDecompositionType")))
#define CreateSegmentDecompositionType (Dc1Factory::CreateObject(TypeOfSegmentDecompositionType))
class Mp7JrsSegmentDecompositionType;
class IMp7JrsSegmentDecompositionType;
/** Smart pointer for instance of IMp7JrsSegmentDecompositionType */
typedef Dc1Ptr< IMp7JrsSegmentDecompositionType > Mp7JrsSegmentDecompositionPtr;

#define TypeOfSegmentOutputQCValueType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentOutputQCValueType")))
#define CreateSegmentOutputQCValueType (Dc1Factory::CreateObject(TypeOfSegmentOutputQCValueType))
class Mp7JrsSegmentOutputQCValueType;
class IMp7JrsSegmentOutputQCValueType;
/** Smart pointer for instance of IMp7JrsSegmentOutputQCValueType */
typedef Dc1Ptr< IMp7JrsSegmentOutputQCValueType > Mp7JrsSegmentOutputQCValuePtr;

#define TypeOfSegmentOutputQCValueType_Output_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentOutputQCValueType_Output_CollectionType")))
#define CreateSegmentOutputQCValueType_Output_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentOutputQCValueType_Output_CollectionType))
class Mp7JrsSegmentOutputQCValueType_Output_CollectionType;
class IMp7JrsSegmentOutputQCValueType_Output_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentOutputQCValueType_Output_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentOutputQCValueType_Output_CollectionType > Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr;

#define TypeOfSegmentQCValueType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentQCValueType")))
#define CreateSegmentQCValueType (Dc1Factory::CreateObject(TypeOfSegmentQCValueType))
class Mp7JrsSegmentQCValueType;
class IMp7JrsSegmentQCValueType;
/** Smart pointer for instance of IMp7JrsSegmentQCValueType */
typedef Dc1Ptr< IMp7JrsSegmentQCValueType > Mp7JrsSegmentQCValuePtr;

#define TypeOfSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType")))
#define CreateSegmentType (Dc1Factory::CreateObject(TypeOfSegmentType))
class Mp7JrsSegmentType;
class IMp7JrsSegmentType;
/** Smart pointer for instance of IMp7JrsSegmentType */
typedef Dc1Ptr< IMp7JrsSegmentType > Mp7JrsSegmentPtr;

#define TypeOfSegmentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_CollectionType")))
#define CreateSegmentType_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentType_CollectionType))
class Mp7JrsSegmentType_CollectionType;
class IMp7JrsSegmentType_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentType_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentType_CollectionType > Mp7JrsSegmentType_CollectionPtr;

#define TypeOfSegmentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_LocalType")))
#define CreateSegmentType_LocalType (Dc1Factory::CreateObject(TypeOfSegmentType_LocalType))
class Mp7JrsSegmentType_LocalType;
class IMp7JrsSegmentType_LocalType;
/** Smart pointer for instance of IMp7JrsSegmentType_LocalType */
typedef Dc1Ptr< IMp7JrsSegmentType_LocalType > Mp7JrsSegmentType_LocalPtr;

#define TypeOfSegmentType_MatchingHint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_MatchingHint_CollectionType")))
#define CreateSegmentType_MatchingHint_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentType_MatchingHint_CollectionType))
class Mp7JrsSegmentType_MatchingHint_CollectionType;
class IMp7JrsSegmentType_MatchingHint_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentType_MatchingHint_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentType_MatchingHint_CollectionType > Mp7JrsSegmentType_MatchingHint_CollectionPtr;

#define TypeOfSegmentType_PointOfView_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_PointOfView_CollectionType")))
#define CreateSegmentType_PointOfView_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentType_PointOfView_CollectionType))
class Mp7JrsSegmentType_PointOfView_CollectionType;
class IMp7JrsSegmentType_PointOfView_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentType_PointOfView_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentType_PointOfView_CollectionType > Mp7JrsSegmentType_PointOfView_CollectionPtr;

#define TypeOfSegmentType_Relation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_Relation_CollectionType")))
#define CreateSegmentType_Relation_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentType_Relation_CollectionType))
class Mp7JrsSegmentType_Relation_CollectionType;
class IMp7JrsSegmentType_Relation_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentType_Relation_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentType_Relation_CollectionType > Mp7JrsSegmentType_Relation_CollectionPtr;

#define TypeOfSegmentType_TextAnnotation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_TextAnnotation_CollectionType")))
#define CreateSegmentType_TextAnnotation_CollectionType (Dc1Factory::CreateObject(TypeOfSegmentType_TextAnnotation_CollectionType))
class Mp7JrsSegmentType_TextAnnotation_CollectionType;
class IMp7JrsSegmentType_TextAnnotation_CollectionType;
/** Smart pointer for instance of IMp7JrsSegmentType_TextAnnotation_CollectionType */
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_CollectionType > Mp7JrsSegmentType_TextAnnotation_CollectionPtr;

#define TypeOfSegmentType_TextAnnotation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_TextAnnotation_LocalType")))
#define CreateSegmentType_TextAnnotation_LocalType (Dc1Factory::CreateObject(TypeOfSegmentType_TextAnnotation_LocalType))
class Mp7JrsSegmentType_TextAnnotation_LocalType;
class IMp7JrsSegmentType_TextAnnotation_LocalType;
/** Smart pointer for instance of IMp7JrsSegmentType_TextAnnotation_LocalType */
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_LocalType > Mp7JrsSegmentType_TextAnnotation_LocalPtr;

#define TypeOfSegmentType_TextAnnotation_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_TextAnnotation_type_LocalType")))
#define CreateSegmentType_TextAnnotation_type_LocalType (Dc1Factory::CreateObject(TypeOfSegmentType_TextAnnotation_type_LocalType))
class Mp7JrsSegmentType_TextAnnotation_type_LocalType;
class IMp7JrsSegmentType_TextAnnotation_type_LocalType;
/** Smart pointer for instance of IMp7JrsSegmentType_TextAnnotation_type_LocalType */
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_type_LocalType > Mp7JrsSegmentType_TextAnnotation_type_LocalPtr;

#define TypeOfSemanticBagType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBagType")))
#define CreateSemanticBagType (Dc1Factory::CreateObject(TypeOfSemanticBagType))
class Mp7JrsSemanticBagType;
class IMp7JrsSemanticBagType;
/** Smart pointer for instance of IMp7JrsSemanticBagType */
typedef Dc1Ptr< IMp7JrsSemanticBagType > Mp7JrsSemanticBagPtr;

#define TypeOfSemanticBagType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBagType_CollectionType")))
#define CreateSemanticBagType_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBagType_CollectionType))
class Mp7JrsSemanticBagType_CollectionType;
class IMp7JrsSemanticBagType_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBagType_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBagType_CollectionType > Mp7JrsSemanticBagType_CollectionPtr;

#define TypeOfSemanticBagType_Graph_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBagType_Graph_CollectionType")))
#define CreateSemanticBagType_Graph_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBagType_Graph_CollectionType))
class Mp7JrsSemanticBagType_Graph_CollectionType;
class IMp7JrsSemanticBagType_Graph_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBagType_Graph_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBagType_Graph_CollectionType > Mp7JrsSemanticBagType_Graph_CollectionPtr;

#define TypeOfSemanticBagType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBagType_LocalType")))
#define CreateSemanticBagType_LocalType (Dc1Factory::CreateObject(TypeOfSemanticBagType_LocalType))
class Mp7JrsSemanticBagType_LocalType;
class IMp7JrsSemanticBagType_LocalType;
/** Smart pointer for instance of IMp7JrsSemanticBagType_LocalType */
typedef Dc1Ptr< IMp7JrsSemanticBagType_LocalType > Mp7JrsSemanticBagType_LocalPtr;

#define TypeOfSemanticBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType")))
#define CreateSemanticBaseType (Dc1Factory::CreateObject(TypeOfSemanticBaseType))
class Mp7JrsSemanticBaseType;
class IMp7JrsSemanticBaseType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType > Mp7JrsSemanticBasePtr;

#define TypeOfSemanticBaseType_Label_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_Label_CollectionType")))
#define CreateSemanticBaseType_Label_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_Label_CollectionType))
class Mp7JrsSemanticBaseType_Label_CollectionType;
class IMp7JrsSemanticBaseType_Label_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_Label_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_Label_CollectionType > Mp7JrsSemanticBaseType_Label_CollectionPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType")))
#define CreateSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType))
class Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType;
class IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType")))
#define CreateSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType))
class Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType;
class IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_CollectionType")))
#define CreateSemanticBaseType_MediaOccurrence_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_CollectionType))
class Mp7JrsSemanticBaseType_MediaOccurrence_CollectionType;
class IMp7JrsSemanticBaseType_MediaOccurrence_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_LocalType")))
#define CreateSemanticBaseType_MediaOccurrence_LocalType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_LocalType))
class Mp7JrsSemanticBaseType_MediaOccurrence_LocalType;
class IMp7JrsSemanticBaseType_MediaOccurrence_LocalType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_LocalType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_LocalType > Mp7JrsSemanticBaseType_MediaOccurrence_LocalPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_type_LocalType")))
#define CreateSemanticBaseType_MediaOccurrence_type_LocalType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_type_LocalType))
class Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType;
class IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType;
// No smart pointer instance for IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType

#define TypeOfSemanticBaseType_MediaOccurrence_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_type_LocalType0")))
#define CreateSemanticBaseType_MediaOccurrence_type_LocalType0 (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_type_LocalType0))
class Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType0;
class IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType0;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType0 */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType0 > Mp7JrsSemanticBaseType_MediaOccurrence_type_Local0Ptr;

#define TypeOfSemanticBaseType_MediaOccurrence_type_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_type_LocalType1")))
#define CreateSemanticBaseType_MediaOccurrence_type_LocalType1 (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_type_LocalType1))
class Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType1;
class IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType1;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType1 */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType1 > Mp7JrsSemanticBaseType_MediaOccurrence_type_Local1Ptr;

#define TypeOfSemanticBaseType_MediaOccurrence_type_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_type_LocalType2")))
#define CreateSemanticBaseType_MediaOccurrence_type_LocalType2 (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_type_LocalType2))
class Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2;
class IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2 */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2 > Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr;

#define TypeOfSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType")))
#define CreateSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType))
class Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType;
class IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr;

#define TypeOfSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType")))
#define CreateSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType))
class Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType;
class IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType > Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr;

#define TypeOfSemanticBaseType_Property_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_Property_CollectionType")))
#define CreateSemanticBaseType_Property_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_Property_CollectionType))
class Mp7JrsSemanticBaseType_Property_CollectionType;
class IMp7JrsSemanticBaseType_Property_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_Property_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_Property_CollectionType > Mp7JrsSemanticBaseType_Property_CollectionPtr;

#define TypeOfSemanticBaseType_Relation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_Relation_CollectionType")))
#define CreateSemanticBaseType_Relation_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticBaseType_Relation_CollectionType))
class Mp7JrsSemanticBaseType_Relation_CollectionType;
class IMp7JrsSemanticBaseType_Relation_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticBaseType_Relation_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticBaseType_Relation_CollectionType > Mp7JrsSemanticBaseType_Relation_CollectionPtr;

#define TypeOfSemanticDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticDescriptionType")))
#define CreateSemanticDescriptionType (Dc1Factory::CreateObject(TypeOfSemanticDescriptionType))
class Mp7JrsSemanticDescriptionType;
class IMp7JrsSemanticDescriptionType;
/** Smart pointer for instance of IMp7JrsSemanticDescriptionType */
typedef Dc1Ptr< IMp7JrsSemanticDescriptionType > Mp7JrsSemanticDescriptionPtr;

#define TypeOfSemanticDescriptionType_ConceptCollection_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticDescriptionType_ConceptCollection_CollectionType")))
#define CreateSemanticDescriptionType_ConceptCollection_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticDescriptionType_ConceptCollection_CollectionType))
class Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionType;
class IMp7JrsSemanticDescriptionType_ConceptCollection_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticDescriptionType_ConceptCollection_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticDescriptionType_ConceptCollection_CollectionType > Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr;

#define TypeOfSemanticDescriptionType_Semantics_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticDescriptionType_Semantics_CollectionType")))
#define CreateSemanticDescriptionType_Semantics_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticDescriptionType_Semantics_CollectionType))
class Mp7JrsSemanticDescriptionType_Semantics_CollectionType;
class IMp7JrsSemanticDescriptionType_Semantics_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticDescriptionType_Semantics_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticDescriptionType_Semantics_CollectionType > Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr;

#define TypeOfSemanticPlaceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticPlaceType")))
#define CreateSemanticPlaceType (Dc1Factory::CreateObject(TypeOfSemanticPlaceType))
class Mp7JrsSemanticPlaceType;
class IMp7JrsSemanticPlaceType;
/** Smart pointer for instance of IMp7JrsSemanticPlaceType */
typedef Dc1Ptr< IMp7JrsSemanticPlaceType > Mp7JrsSemanticPlacePtr;

#define TypeOfSemanticPlaceType_SemanticPlaceInterval_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticPlaceType_SemanticPlaceInterval_CollectionType")))
#define CreateSemanticPlaceType_SemanticPlaceInterval_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticPlaceType_SemanticPlaceInterval_CollectionType))
class Mp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionType;
class IMp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionType > Mp7JrsSemanticPlaceType_SemanticPlaceInterval_CollectionPtr;

#define TypeOfSemanticPlaceType_SemanticPlaceInterval_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticPlaceType_SemanticPlaceInterval_LocalType")))
#define CreateSemanticPlaceType_SemanticPlaceInterval_LocalType (Dc1Factory::CreateObject(TypeOfSemanticPlaceType_SemanticPlaceInterval_LocalType))
class Mp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalType;
class IMp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalType;
/** Smart pointer for instance of IMp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalType */
typedef Dc1Ptr< IMp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalType > Mp7JrsSemanticPlaceType_SemanticPlaceInterval_LocalPtr;

#define TypeOfSemanticStateType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticStateType")))
#define CreateSemanticStateType (Dc1Factory::CreateObject(TypeOfSemanticStateType))
class Mp7JrsSemanticStateType;
class IMp7JrsSemanticStateType;
/** Smart pointer for instance of IMp7JrsSemanticStateType */
typedef Dc1Ptr< IMp7JrsSemanticStateType > Mp7JrsSemanticStatePtr;

#define TypeOfSemanticStateType_AttributeValuePair_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticStateType_AttributeValuePair_CollectionType")))
#define CreateSemanticStateType_AttributeValuePair_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticStateType_AttributeValuePair_CollectionType))
class Mp7JrsSemanticStateType_AttributeValuePair_CollectionType;
class IMp7JrsSemanticStateType_AttributeValuePair_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticStateType_AttributeValuePair_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticStateType_AttributeValuePair_CollectionType > Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr;

#define TypeOfSemanticStateType_AttributeValuePair_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticStateType_AttributeValuePair_CollectionType0")))
#define CreateSemanticStateType_AttributeValuePair_CollectionType0 (Dc1Factory::CreateObject(TypeOfSemanticStateType_AttributeValuePair_CollectionType0))
class Mp7JrsSemanticStateType_AttributeValuePair_CollectionType0;
class IMp7JrsSemanticStateType_AttributeValuePair_CollectionType0;
/** Smart pointer for instance of IMp7JrsSemanticStateType_AttributeValuePair_CollectionType0 */
typedef Dc1Ptr< IMp7JrsSemanticStateType_AttributeValuePair_CollectionType0 > Mp7JrsSemanticStateType_AttributeValuePair_Collection0Ptr;

#define TypeOfSemanticStateType_AttributeValuePair_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticStateType_AttributeValuePair_LocalType")))
#define CreateSemanticStateType_AttributeValuePair_LocalType (Dc1Factory::CreateObject(TypeOfSemanticStateType_AttributeValuePair_LocalType))
class Mp7JrsSemanticStateType_AttributeValuePair_LocalType;
class IMp7JrsSemanticStateType_AttributeValuePair_LocalType;
/** Smart pointer for instance of IMp7JrsSemanticStateType_AttributeValuePair_LocalType */
typedef Dc1Ptr< IMp7JrsSemanticStateType_AttributeValuePair_LocalType > Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr;

#define TypeOfSemanticStateType_AttributeValuePair_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticStateType_AttributeValuePair_LocalType0")))
#define CreateSemanticStateType_AttributeValuePair_LocalType0 (Dc1Factory::CreateObject(TypeOfSemanticStateType_AttributeValuePair_LocalType0))
class Mp7JrsSemanticStateType_AttributeValuePair_LocalType0;
class IMp7JrsSemanticStateType_AttributeValuePair_LocalType0;
/** Smart pointer for instance of IMp7JrsSemanticStateType_AttributeValuePair_LocalType0 */
typedef Dc1Ptr< IMp7JrsSemanticStateType_AttributeValuePair_LocalType0 > Mp7JrsSemanticStateType_AttributeValuePair_Local0Ptr;

#define TypeOfSemanticTimeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticTimeType")))
#define CreateSemanticTimeType (Dc1Factory::CreateObject(TypeOfSemanticTimeType))
class Mp7JrsSemanticTimeType;
class IMp7JrsSemanticTimeType;
/** Smart pointer for instance of IMp7JrsSemanticTimeType */
typedef Dc1Ptr< IMp7JrsSemanticTimeType > Mp7JrsSemanticTimePtr;

#define TypeOfSemanticTimeType_SemanticTimeInterval_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticTimeType_SemanticTimeInterval_CollectionType")))
#define CreateSemanticTimeType_SemanticTimeInterval_CollectionType (Dc1Factory::CreateObject(TypeOfSemanticTimeType_SemanticTimeInterval_CollectionType))
class Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionType;
class IMp7JrsSemanticTimeType_SemanticTimeInterval_CollectionType;
/** Smart pointer for instance of IMp7JrsSemanticTimeType_SemanticTimeInterval_CollectionType */
typedef Dc1Ptr< IMp7JrsSemanticTimeType_SemanticTimeInterval_CollectionType > Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr;

#define TypeOfSemanticTimeType_SemanticTimeInterval_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticTimeType_SemanticTimeInterval_LocalType")))
#define CreateSemanticTimeType_SemanticTimeInterval_LocalType (Dc1Factory::CreateObject(TypeOfSemanticTimeType_SemanticTimeInterval_LocalType))
class Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType;
class IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType;
/** Smart pointer for instance of IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType */
typedef Dc1Ptr< IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType > Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalPtr;

#define TypeOfSemanticType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticType")))
#define CreateSemanticType (Dc1Factory::CreateObject(TypeOfSemanticType))
class Mp7JrsSemanticType;
class IMp7JrsSemanticType;
/** Smart pointer for instance of IMp7JrsSemanticType */
typedef Dc1Ptr< IMp7JrsSemanticType > Mp7JrsSemanticPtr;

#define TypeOfSentencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SentencesType")))
#define CreateSentencesType (Dc1Factory::CreateObject(TypeOfSentencesType))
class Mp7JrsSentencesType;
class IMp7JrsSentencesType;
/** Smart pointer for instance of IMp7JrsSentencesType */
typedef Dc1Ptr< IMp7JrsSentencesType > Mp7JrsSentencesPtr;

#define TypeOfSentencesType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SentencesType_CollectionType")))
#define CreateSentencesType_CollectionType (Dc1Factory::CreateObject(TypeOfSentencesType_CollectionType))
class Mp7JrsSentencesType_CollectionType;
class IMp7JrsSentencesType_CollectionType;
/** Smart pointer for instance of IMp7JrsSentencesType_CollectionType */
typedef Dc1Ptr< IMp7JrsSentencesType_CollectionType > Mp7JrsSentencesType_CollectionPtr;

#define TypeOfSentencesType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SentencesType_LocalType")))
#define CreateSentencesType_LocalType (Dc1Factory::CreateObject(TypeOfSentencesType_LocalType))
class Mp7JrsSentencesType_LocalType;
class IMp7JrsSentencesType_LocalType;
/** Smart pointer for instance of IMp7JrsSentencesType_LocalType */
typedef Dc1Ptr< IMp7JrsSentencesType_LocalType > Mp7JrsSentencesType_LocalPtr;

#define TypeOfSequentialSummaryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType")))
#define CreateSequentialSummaryType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType))
class Mp7JrsSequentialSummaryType;
class IMp7JrsSequentialSummaryType;
/** Smart pointer for instance of IMp7JrsSequentialSummaryType */
typedef Dc1Ptr< IMp7JrsSequentialSummaryType > Mp7JrsSequentialSummaryPtr;

#define TypeOfSequentialSummaryType_AudioSummaryComponent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_AudioSummaryComponent_CollectionType")))
#define CreateSequentialSummaryType_AudioSummaryComponent_CollectionType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType_AudioSummaryComponent_CollectionType))
class Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionType;
class IMp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionType;
/** Smart pointer for instance of IMp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionType */
typedef Dc1Ptr< IMp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionType > Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr;

#define TypeOfSequentialSummaryType_components_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_components_CollectionType")))
#define CreateSequentialSummaryType_components_CollectionType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType_components_CollectionType))
class Mp7JrsSequentialSummaryType_components_CollectionType;
class IMp7JrsSequentialSummaryType_components_CollectionType;
/** Smart pointer for instance of IMp7JrsSequentialSummaryType_components_CollectionType */
typedef Dc1Ptr< IMp7JrsSequentialSummaryType_components_CollectionType > Mp7JrsSequentialSummaryType_components_CollectionPtr;

#define TypeOfSequentialSummaryType_components_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_components_LocalType")))
#define CreateSequentialSummaryType_components_LocalType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType_components_LocalType))
class Mp7JrsSequentialSummaryType_components_LocalType;
class IMp7JrsSequentialSummaryType_components_LocalType;
// No smart pointer instance for IMp7JrsSequentialSummaryType_components_LocalType

#define TypeOfSequentialSummaryType_TextualSummaryComponent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_TextualSummaryComponent_CollectionType")))
#define CreateSequentialSummaryType_TextualSummaryComponent_CollectionType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType_TextualSummaryComponent_CollectionType))
class Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionType;
class IMp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionType;
/** Smart pointer for instance of IMp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionType */
typedef Dc1Ptr< IMp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionType > Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr;

#define TypeOfSequentialSummaryType_VisualSummaryComponent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_VisualSummaryComponent_CollectionType")))
#define CreateSequentialSummaryType_VisualSummaryComponent_CollectionType (Dc1Factory::CreateObject(TypeOfSequentialSummaryType_VisualSummaryComponent_CollectionType))
class Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionType;
class IMp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionType;
/** Smart pointer for instance of IMp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionType */
typedef Dc1Ptr< IMp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionType > Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr;

#define TypeOfSeriesOfScalarBinaryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SeriesOfScalarBinaryType")))
#define CreateSeriesOfScalarBinaryType (Dc1Factory::CreateObject(TypeOfSeriesOfScalarBinaryType))
class Mp7JrsSeriesOfScalarBinaryType;
class IMp7JrsSeriesOfScalarBinaryType;
/** Smart pointer for instance of IMp7JrsSeriesOfScalarBinaryType */
typedef Dc1Ptr< IMp7JrsSeriesOfScalarBinaryType > Mp7JrsSeriesOfScalarBinaryPtr;

#define TypeOfSeriesOfScalarType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SeriesOfScalarType")))
#define CreateSeriesOfScalarType (Dc1Factory::CreateObject(TypeOfSeriesOfScalarType))
class Mp7JrsSeriesOfScalarType;
class IMp7JrsSeriesOfScalarType;
/** Smart pointer for instance of IMp7JrsSeriesOfScalarType */
typedef Dc1Ptr< IMp7JrsSeriesOfScalarType > Mp7JrsSeriesOfScalarPtr;

#define TypeOfSeriesOfVectorBinaryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SeriesOfVectorBinaryType")))
#define CreateSeriesOfVectorBinaryType (Dc1Factory::CreateObject(TypeOfSeriesOfVectorBinaryType))
class Mp7JrsSeriesOfVectorBinaryType;
class IMp7JrsSeriesOfVectorBinaryType;
/** Smart pointer for instance of IMp7JrsSeriesOfVectorBinaryType */
typedef Dc1Ptr< IMp7JrsSeriesOfVectorBinaryType > Mp7JrsSeriesOfVectorBinaryPtr;

#define TypeOfSeriesOfVectorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SeriesOfVectorType")))
#define CreateSeriesOfVectorType (Dc1Factory::CreateObject(TypeOfSeriesOfVectorType))
class Mp7JrsSeriesOfVectorType;
class IMp7JrsSeriesOfVectorType;
/** Smart pointer for instance of IMp7JrsSeriesOfVectorType */
typedef Dc1Ptr< IMp7JrsSeriesOfVectorType > Mp7JrsSeriesOfVectorPtr;

#define TypeOfShape3DType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Shape3DType")))
#define CreateShape3DType (Dc1Factory::CreateObject(TypeOfShape3DType))
class Mp7JrsShape3DType;
class IMp7JrsShape3DType;
/** Smart pointer for instance of IMp7JrsShape3DType */
typedef Dc1Ptr< IMp7JrsShape3DType > Mp7JrsShape3DPtr;

#define TypeOfShape3DType_Spectrum_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Shape3DType_Spectrum_CollectionType")))
#define CreateShape3DType_Spectrum_CollectionType (Dc1Factory::CreateObject(TypeOfShape3DType_Spectrum_CollectionType))
class Mp7JrsShape3DType_Spectrum_CollectionType;
class IMp7JrsShape3DType_Spectrum_CollectionType;
/** Smart pointer for instance of IMp7JrsShape3DType_Spectrum_CollectionType */
typedef Dc1Ptr< IMp7JrsShape3DType_Spectrum_CollectionType > Mp7JrsShape3DType_Spectrum_CollectionPtr;

#define TypeOfShape3DType_Spectrum_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Shape3DType_Spectrum_LocalType")))
#define CreateShape3DType_Spectrum_LocalType (Dc1Factory::CreateObject(TypeOfShape3DType_Spectrum_LocalType))
class Mp7JrsShape3DType_Spectrum_LocalType;
class IMp7JrsShape3DType_Spectrum_LocalType;
/** Smart pointer for instance of IMp7JrsShape3DType_Spectrum_LocalType */
typedef Dc1Ptr< IMp7JrsShape3DType_Spectrum_LocalType > Mp7JrsShape3DType_Spectrum_LocalPtr;

#define TypeOfShapeVariationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType")))
#define CreateShapeVariationType (Dc1Factory::CreateObject(TypeOfShapeVariationType))
class Mp7JrsShapeVariationType;
class IMp7JrsShapeVariationType;
/** Smart pointer for instance of IMp7JrsShapeVariationType */
typedef Dc1Ptr< IMp7JrsShapeVariationType > Mp7JrsShapeVariationPtr;

#define TypeOfShapeVariationType_DynamicShapeVariation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_DynamicShapeVariation_CollectionType")))
#define CreateShapeVariationType_DynamicShapeVariation_CollectionType (Dc1Factory::CreateObject(TypeOfShapeVariationType_DynamicShapeVariation_CollectionType))
class Mp7JrsShapeVariationType_DynamicShapeVariation_CollectionType;
class IMp7JrsShapeVariationType_DynamicShapeVariation_CollectionType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_DynamicShapeVariation_CollectionType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_DynamicShapeVariation_CollectionType > Mp7JrsShapeVariationType_DynamicShapeVariation_CollectionPtr;

#define TypeOfShapeVariationType_DynamicShapeVariation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_DynamicShapeVariation_LocalType")))
#define CreateShapeVariationType_DynamicShapeVariation_LocalType (Dc1Factory::CreateObject(TypeOfShapeVariationType_DynamicShapeVariation_LocalType))
class Mp7JrsShapeVariationType_DynamicShapeVariation_LocalType;
class IMp7JrsShapeVariationType_DynamicShapeVariation_LocalType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_DynamicShapeVariation_LocalType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_DynamicShapeVariation_LocalType > Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr;

#define TypeOfShapeVariationType_StaticShapeVariation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StaticShapeVariation_CollectionType")))
#define CreateShapeVariationType_StaticShapeVariation_CollectionType (Dc1Factory::CreateObject(TypeOfShapeVariationType_StaticShapeVariation_CollectionType))
class Mp7JrsShapeVariationType_StaticShapeVariation_CollectionType;
class IMp7JrsShapeVariationType_StaticShapeVariation_CollectionType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_StaticShapeVariation_CollectionType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_StaticShapeVariation_CollectionType > Mp7JrsShapeVariationType_StaticShapeVariation_CollectionPtr;

#define TypeOfShapeVariationType_StaticShapeVariation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StaticShapeVariation_LocalType")))
#define CreateShapeVariationType_StaticShapeVariation_LocalType (Dc1Factory::CreateObject(TypeOfShapeVariationType_StaticShapeVariation_LocalType))
class Mp7JrsShapeVariationType_StaticShapeVariation_LocalType;
class IMp7JrsShapeVariationType_StaticShapeVariation_LocalType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_StaticShapeVariation_LocalType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_StaticShapeVariation_LocalType > Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr;

#define TypeOfShapeVariationType_StatisticalVariation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StatisticalVariation_CollectionType")))
#define CreateShapeVariationType_StatisticalVariation_CollectionType (Dc1Factory::CreateObject(TypeOfShapeVariationType_StatisticalVariation_CollectionType))
class Mp7JrsShapeVariationType_StatisticalVariation_CollectionType;
class IMp7JrsShapeVariationType_StatisticalVariation_CollectionType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_StatisticalVariation_CollectionType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_StatisticalVariation_CollectionType > Mp7JrsShapeVariationType_StatisticalVariation_CollectionPtr;

#define TypeOfShapeVariationType_StatisticalVariation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StatisticalVariation_LocalType")))
#define CreateShapeVariationType_StatisticalVariation_LocalType (Dc1Factory::CreateObject(TypeOfShapeVariationType_StatisticalVariation_LocalType))
class Mp7JrsShapeVariationType_StatisticalVariation_LocalType;
class IMp7JrsShapeVariationType_StatisticalVariation_LocalType;
/** Smart pointer for instance of IMp7JrsShapeVariationType_StatisticalVariation_LocalType */
typedef Dc1Ptr< IMp7JrsShapeVariationType_StatisticalVariation_LocalType > Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr;

#define TypeOfShotEditingTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotEditingTemporalDecompositionType")))
#define CreateShotEditingTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfShotEditingTemporalDecompositionType))
class Mp7JrsShotEditingTemporalDecompositionType;
class IMp7JrsShotEditingTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsShotEditingTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsShotEditingTemporalDecompositionType > Mp7JrsShotEditingTemporalDecompositionPtr;

#define TypeOfShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotEditingTemporalDecompositionType_CollectionType")))
#define CreateShotEditingTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfShotEditingTemporalDecompositionType_CollectionType))
class Mp7JrsShotEditingTemporalDecompositionType_CollectionType;
class IMp7JrsShotEditingTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsShotEditingTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsShotEditingTemporalDecompositionType_CollectionType > Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr;

#define TypeOfShotEditingTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotEditingTemporalDecompositionType_LocalType")))
#define CreateShotEditingTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfShotEditingTemporalDecompositionType_LocalType))
class Mp7JrsShotEditingTemporalDecompositionType_LocalType;
class IMp7JrsShotEditingTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsShotEditingTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsShotEditingTemporalDecompositionType_LocalType > Mp7JrsShotEditingTemporalDecompositionType_LocalPtr;

#define TypeOfShotType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotType")))
#define CreateShotType (Dc1Factory::CreateObject(TypeOfShotType))
class Mp7JrsShotType;
class IMp7JrsShotType;
/** Smart pointer for instance of IMp7JrsShotType */
typedef Dc1Ptr< IMp7JrsShotType > Mp7JrsShotPtr;

#define TypeOfShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotType_AnalyticEditingTemporalDecomposition_CollectionType")))
#define CreateShotType_AnalyticEditingTemporalDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfShotType_AnalyticEditingTemporalDecomposition_CollectionType))
class Mp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionType;
class IMp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionType > Mp7JrsShotType_AnalyticEditingTemporalDecomposition_CollectionPtr;

#define TypeOfSignalPlaneFractionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneFractionType")))
#define CreateSignalPlaneFractionType (Dc1Factory::CreateObject(TypeOfSignalPlaneFractionType))
class Mp7JrsSignalPlaneFractionType;
class IMp7JrsSignalPlaneFractionType;
/** Smart pointer for instance of IMp7JrsSignalPlaneFractionType */
typedef Dc1Ptr< IMp7JrsSignalPlaneFractionType > Mp7JrsSignalPlaneFractionPtr;

#define TypeOfSignalPlaneOriginType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneOriginType")))
#define CreateSignalPlaneOriginType (Dc1Factory::CreateObject(TypeOfSignalPlaneOriginType))
class Mp7JrsSignalPlaneOriginType;
class IMp7JrsSignalPlaneOriginType;
/** Smart pointer for instance of IMp7JrsSignalPlaneOriginType */
typedef Dc1Ptr< IMp7JrsSignalPlaneOriginType > Mp7JrsSignalPlaneOriginPtr;

#define TypeOfSignalPlaneOriginType_timeOrigin_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneOriginType_timeOrigin_LocalType")))
#define CreateSignalPlaneOriginType_timeOrigin_LocalType (Dc1Factory::CreateObject(TypeOfSignalPlaneOriginType_timeOrigin_LocalType))
class Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType;
class IMp7JrsSignalPlaneOriginType_timeOrigin_LocalType;
// No smart pointer instance for IMp7JrsSignalPlaneOriginType_timeOrigin_LocalType

#define TypeOfSignalPlaneOriginType_xOrigin_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneOriginType_xOrigin_LocalType")))
#define CreateSignalPlaneOriginType_xOrigin_LocalType (Dc1Factory::CreateObject(TypeOfSignalPlaneOriginType_xOrigin_LocalType))
class Mp7JrsSignalPlaneOriginType_xOrigin_LocalType;
class IMp7JrsSignalPlaneOriginType_xOrigin_LocalType;
// No smart pointer instance for IMp7JrsSignalPlaneOriginType_xOrigin_LocalType

#define TypeOfSignalPlaneOriginType_yOrigin_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneOriginType_yOrigin_LocalType")))
#define CreateSignalPlaneOriginType_yOrigin_LocalType (Dc1Factory::CreateObject(TypeOfSignalPlaneOriginType_yOrigin_LocalType))
class Mp7JrsSignalPlaneOriginType_yOrigin_LocalType;
class IMp7JrsSignalPlaneOriginType_yOrigin_LocalType;
// No smart pointer instance for IMp7JrsSignalPlaneOriginType_yOrigin_LocalType

#define TypeOfSignalPlaneOriginType_zOrigin_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneOriginType_zOrigin_LocalType")))
#define CreateSignalPlaneOriginType_zOrigin_LocalType (Dc1Factory::CreateObject(TypeOfSignalPlaneOriginType_zOrigin_LocalType))
class Mp7JrsSignalPlaneOriginType_zOrigin_LocalType;
class IMp7JrsSignalPlaneOriginType_zOrigin_LocalType;
// No smart pointer instance for IMp7JrsSignalPlaneOriginType_zOrigin_LocalType

#define TypeOfSignalPlaneSampleType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))
#define CreateSignalPlaneSampleType (Dc1Factory::CreateObject(TypeOfSignalPlaneSampleType))
class Mp7JrsSignalPlaneSampleType;
class IMp7JrsSignalPlaneSampleType;
/** Smart pointer for instance of IMp7JrsSignalPlaneSampleType */
typedef Dc1Ptr< IMp7JrsSignalPlaneSampleType > Mp7JrsSignalPlaneSamplePtr;

#define TypeOfSignalPlaneType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneType")))
#define CreateSignalPlaneType (Dc1Factory::CreateObject(TypeOfSignalPlaneType))
class Mp7JrsSignalPlaneType;
class IMp7JrsSignalPlaneType;
/** Smart pointer for instance of IMp7JrsSignalPlaneType */
typedef Dc1Ptr< IMp7JrsSignalPlaneType > Mp7JrsSignalPlanePtr;

#define TypeOfSignalPlaneType_dim_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneType_dim_LocalType")))
#define CreateSignalPlaneType_dim_LocalType (Dc1Factory::CreateObject(TypeOfSignalPlaneType_dim_LocalType))
class Mp7JrsSignalPlaneType_dim_LocalType;
class IMp7JrsSignalPlaneType_dim_LocalType;
/** Smart pointer for instance of IMp7JrsSignalPlaneType_dim_LocalType */
typedef Dc1Ptr< IMp7JrsSignalPlaneType_dim_LocalType > Mp7JrsSignalPlaneType_dim_LocalPtr;

#define TypeOfSignalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalType")))
#define CreateSignalType (Dc1Factory::CreateObject(TypeOfSignalType))
class Mp7JrsSignalType;
class IMp7JrsSignalType;
/** Smart pointer for instance of IMp7JrsSignalType */
typedef Dc1Ptr< IMp7JrsSignalType > Mp7JrsSignalPtr;

#define TypeOfSilenceHeaderType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SilenceHeaderType")))
#define CreateSilenceHeaderType (Dc1Factory::CreateObject(TypeOfSilenceHeaderType))
class Mp7JrsSilenceHeaderType;
class IMp7JrsSilenceHeaderType;
/** Smart pointer for instance of IMp7JrsSilenceHeaderType */
typedef Dc1Ptr< IMp7JrsSilenceHeaderType > Mp7JrsSilenceHeaderPtr;

#define TypeOfSilenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SilenceType")))
#define CreateSilenceType (Dc1Factory::CreateObject(TypeOfSilenceType))
class Mp7JrsSilenceType;
class IMp7JrsSilenceType;
/** Smart pointer for instance of IMp7JrsSilenceType */
typedef Dc1Ptr< IMp7JrsSilenceType > Mp7JrsSilencePtr;

#define TypeOfSoundClassificationModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundClassificationModelType")))
#define CreateSoundClassificationModelType (Dc1Factory::CreateObject(TypeOfSoundClassificationModelType))
class Mp7JrsSoundClassificationModelType;
class IMp7JrsSoundClassificationModelType;
/** Smart pointer for instance of IMp7JrsSoundClassificationModelType */
typedef Dc1Ptr< IMp7JrsSoundClassificationModelType > Mp7JrsSoundClassificationModelPtr;

#define TypeOfSoundClassificationModelType_SoundModel_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundClassificationModelType_SoundModel_CollectionType")))
#define CreateSoundClassificationModelType_SoundModel_CollectionType (Dc1Factory::CreateObject(TypeOfSoundClassificationModelType_SoundModel_CollectionType))
class Mp7JrsSoundClassificationModelType_SoundModel_CollectionType;
class IMp7JrsSoundClassificationModelType_SoundModel_CollectionType;
/** Smart pointer for instance of IMp7JrsSoundClassificationModelType_SoundModel_CollectionType */
typedef Dc1Ptr< IMp7JrsSoundClassificationModelType_SoundModel_CollectionType > Mp7JrsSoundClassificationModelType_SoundModel_CollectionPtr;

#define TypeOfSoundModelStateHistogramType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundModelStateHistogramType")))
#define CreateSoundModelStateHistogramType (Dc1Factory::CreateObject(TypeOfSoundModelStateHistogramType))
class Mp7JrsSoundModelStateHistogramType;
class IMp7JrsSoundModelStateHistogramType;
/** Smart pointer for instance of IMp7JrsSoundModelStateHistogramType */
typedef Dc1Ptr< IMp7JrsSoundModelStateHistogramType > Mp7JrsSoundModelStateHistogramPtr;

#define TypeOfSoundModelStateHistogramType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundModelStateHistogramType_CollectionType")))
#define CreateSoundModelStateHistogramType_CollectionType (Dc1Factory::CreateObject(TypeOfSoundModelStateHistogramType_CollectionType))
class Mp7JrsSoundModelStateHistogramType_CollectionType;
class IMp7JrsSoundModelStateHistogramType_CollectionType;
/** Smart pointer for instance of IMp7JrsSoundModelStateHistogramType_CollectionType */
typedef Dc1Ptr< IMp7JrsSoundModelStateHistogramType_CollectionType > Mp7JrsSoundModelStateHistogramType_CollectionPtr;

#define TypeOfSoundModelStateHistogramType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundModelStateHistogramType_LocalType")))
#define CreateSoundModelStateHistogramType_LocalType (Dc1Factory::CreateObject(TypeOfSoundModelStateHistogramType_LocalType))
class Mp7JrsSoundModelStateHistogramType_LocalType;
class IMp7JrsSoundModelStateHistogramType_LocalType;
/** Smart pointer for instance of IMp7JrsSoundModelStateHistogramType_LocalType */
typedef Dc1Ptr< IMp7JrsSoundModelStateHistogramType_LocalType > Mp7JrsSoundModelStateHistogramType_LocalPtr;

#define TypeOfSoundModelStatePathType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundModelStatePathType")))
#define CreateSoundModelStatePathType (Dc1Factory::CreateObject(TypeOfSoundModelStatePathType))
class Mp7JrsSoundModelStatePathType;
class IMp7JrsSoundModelStatePathType;
/** Smart pointer for instance of IMp7JrsSoundModelStatePathType */
typedef Dc1Ptr< IMp7JrsSoundModelStatePathType > Mp7JrsSoundModelStatePathPtr;

#define TypeOfSoundModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SoundModelType")))
#define CreateSoundModelType (Dc1Factory::CreateObject(TypeOfSoundModelType))
class Mp7JrsSoundModelType;
class IMp7JrsSoundModelType;
/** Smart pointer for instance of IMp7JrsSoundModelType */
typedef Dc1Ptr< IMp7JrsSoundModelType > Mp7JrsSoundModelPtr;

#define TypeOfSourcePreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType")))
#define CreateSourcePreferencesType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType))
class Mp7JrsSourcePreferencesType;
class IMp7JrsSourcePreferencesType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType > Mp7JrsSourcePreferencesPtr;

#define TypeOfSourcePreferencesType_DisseminationDate_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationDate_CollectionType")))
#define CreateSourcePreferencesType_DisseminationDate_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationDate_CollectionType))
class Mp7JrsSourcePreferencesType_DisseminationDate_CollectionType;
class IMp7JrsSourcePreferencesType_DisseminationDate_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationDate_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationDate_CollectionType > Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr;

#define TypeOfSourcePreferencesType_DisseminationDate_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationDate_LocalType")))
#define CreateSourcePreferencesType_DisseminationDate_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationDate_LocalType))
class Mp7JrsSourcePreferencesType_DisseminationDate_LocalType;
class IMp7JrsSourcePreferencesType_DisseminationDate_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationDate_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationDate_LocalType > Mp7JrsSourcePreferencesType_DisseminationDate_LocalPtr;

#define TypeOfSourcePreferencesType_DisseminationFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationFormat_CollectionType")))
#define CreateSourcePreferencesType_DisseminationFormat_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationFormat_CollectionType))
class Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionType;
class IMp7JrsSourcePreferencesType_DisseminationFormat_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationFormat_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationFormat_CollectionType > Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr;

#define TypeOfSourcePreferencesType_DisseminationFormat_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationFormat_LocalType")))
#define CreateSourcePreferencesType_DisseminationFormat_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationFormat_LocalType))
class Mp7JrsSourcePreferencesType_DisseminationFormat_LocalType;
class IMp7JrsSourcePreferencesType_DisseminationFormat_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationFormat_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationFormat_LocalType > Mp7JrsSourcePreferencesType_DisseminationFormat_LocalPtr;

#define TypeOfSourcePreferencesType_DisseminationLocation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationLocation_CollectionType")))
#define CreateSourcePreferencesType_DisseminationLocation_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationLocation_CollectionType))
class Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionType;
class IMp7JrsSourcePreferencesType_DisseminationLocation_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationLocation_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationLocation_CollectionType > Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr;

#define TypeOfSourcePreferencesType_DisseminationLocation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationLocation_LocalType")))
#define CreateSourcePreferencesType_DisseminationLocation_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationLocation_LocalType))
class Mp7JrsSourcePreferencesType_DisseminationLocation_LocalType;
class IMp7JrsSourcePreferencesType_DisseminationLocation_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationLocation_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationLocation_LocalType > Mp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr;

#define TypeOfSourcePreferencesType_DisseminationSource_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationSource_CollectionType")))
#define CreateSourcePreferencesType_DisseminationSource_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationSource_CollectionType))
class Mp7JrsSourcePreferencesType_DisseminationSource_CollectionType;
class IMp7JrsSourcePreferencesType_DisseminationSource_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationSource_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationSource_CollectionType > Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr;

#define TypeOfSourcePreferencesType_DisseminationSource_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationSource_LocalType")))
#define CreateSourcePreferencesType_DisseminationSource_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_DisseminationSource_LocalType))
class Mp7JrsSourcePreferencesType_DisseminationSource_LocalType;
class IMp7JrsSourcePreferencesType_DisseminationSource_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_DisseminationSource_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_DisseminationSource_LocalType > Mp7JrsSourcePreferencesType_DisseminationSource_LocalPtr;

#define TypeOfSourcePreferencesType_Disseminator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_Disseminator_CollectionType")))
#define CreateSourcePreferencesType_Disseminator_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_Disseminator_CollectionType))
class Mp7JrsSourcePreferencesType_Disseminator_CollectionType;
class IMp7JrsSourcePreferencesType_Disseminator_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_Disseminator_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_Disseminator_CollectionType > Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr;

#define TypeOfSourcePreferencesType_Disseminator_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_Disseminator_LocalType")))
#define CreateSourcePreferencesType_Disseminator_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_Disseminator_LocalType))
class Mp7JrsSourcePreferencesType_Disseminator_LocalType;
class IMp7JrsSourcePreferencesType_Disseminator_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_Disseminator_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_Disseminator_LocalType > Mp7JrsSourcePreferencesType_Disseminator_LocalPtr;

#define TypeOfSourcePreferencesType_MediaFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_MediaFormat_CollectionType")))
#define CreateSourcePreferencesType_MediaFormat_CollectionType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_MediaFormat_CollectionType))
class Mp7JrsSourcePreferencesType_MediaFormat_CollectionType;
class IMp7JrsSourcePreferencesType_MediaFormat_CollectionType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_MediaFormat_CollectionType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_MediaFormat_CollectionType > Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr;

#define TypeOfSourcePreferencesType_MediaFormat_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_MediaFormat_LocalType")))
#define CreateSourcePreferencesType_MediaFormat_LocalType (Dc1Factory::CreateObject(TypeOfSourcePreferencesType_MediaFormat_LocalType))
class Mp7JrsSourcePreferencesType_MediaFormat_LocalType;
class IMp7JrsSourcePreferencesType_MediaFormat_LocalType;
/** Smart pointer for instance of IMp7JrsSourcePreferencesType_MediaFormat_LocalType */
typedef Dc1Ptr< IMp7JrsSourcePreferencesType_MediaFormat_LocalType > Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr;

#define TypeOfSpaceFrequencyGraphType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType")))
#define CreateSpaceFrequencyGraphType (Dc1Factory::CreateObject(TypeOfSpaceFrequencyGraphType))
class Mp7JrsSpaceFrequencyGraphType;
class IMp7JrsSpaceFrequencyGraphType;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyGraphType */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyGraphType > Mp7JrsSpaceFrequencyGraphPtr;

#define TypeOfSpaceFrequencyGraphType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType_CollectionType")))
#define CreateSpaceFrequencyGraphType_CollectionType (Dc1Factory::CreateObject(TypeOfSpaceFrequencyGraphType_CollectionType))
class Mp7JrsSpaceFrequencyGraphType_CollectionType;
class IMp7JrsSpaceFrequencyGraphType_CollectionType;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyGraphType_CollectionType */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyGraphType_CollectionType > Mp7JrsSpaceFrequencyGraphType_CollectionPtr;

#define TypeOfSpaceFrequencyGraphType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType_CollectionType0")))
#define CreateSpaceFrequencyGraphType_CollectionType0 (Dc1Factory::CreateObject(TypeOfSpaceFrequencyGraphType_CollectionType0))
class Mp7JrsSpaceFrequencyGraphType_CollectionType0;
class IMp7JrsSpaceFrequencyGraphType_CollectionType0;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyGraphType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyGraphType_CollectionType0 > Mp7JrsSpaceFrequencyGraphType_Collection0Ptr;

#define TypeOfSpaceFrequencyGraphType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType_LocalType")))
#define CreateSpaceFrequencyGraphType_LocalType (Dc1Factory::CreateObject(TypeOfSpaceFrequencyGraphType_LocalType))
class Mp7JrsSpaceFrequencyGraphType_LocalType;
class IMp7JrsSpaceFrequencyGraphType_LocalType;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyGraphType_LocalType */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyGraphType_LocalType > Mp7JrsSpaceFrequencyGraphType_LocalPtr;

#define TypeOfSpaceFrequencyGraphType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType_LocalType0")))
#define CreateSpaceFrequencyGraphType_LocalType0 (Dc1Factory::CreateObject(TypeOfSpaceFrequencyGraphType_LocalType0))
class Mp7JrsSpaceFrequencyGraphType_LocalType0;
class IMp7JrsSpaceFrequencyGraphType_LocalType0;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyGraphType_LocalType0 */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyGraphType_LocalType0 > Mp7JrsSpaceFrequencyGraphType_Local0Ptr;

#define TypeOfSpaceFrequencyViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyViewType")))
#define CreateSpaceFrequencyViewType (Dc1Factory::CreateObject(TypeOfSpaceFrequencyViewType))
class Mp7JrsSpaceFrequencyViewType;
class IMp7JrsSpaceFrequencyViewType;
/** Smart pointer for instance of IMp7JrsSpaceFrequencyViewType */
typedef Dc1Ptr< IMp7JrsSpaceFrequencyViewType > Mp7JrsSpaceFrequencyViewPtr;

#define TypeOfSpaceFrequencyViewType_order_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyViewType_order_LocalType")))
#define CreateSpaceFrequencyViewType_order_LocalType (Dc1Factory::CreateObject(TypeOfSpaceFrequencyViewType_order_LocalType))
class Mp7JrsSpaceFrequencyViewType_order_LocalType;
class IMp7JrsSpaceFrequencyViewType_order_LocalType;
// No smart pointer instance for IMp7JrsSpaceFrequencyViewType_order_LocalType

#define TypeOfSpaceResolutionViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceResolutionViewType")))
#define CreateSpaceResolutionViewType (Dc1Factory::CreateObject(TypeOfSpaceResolutionViewType))
class Mp7JrsSpaceResolutionViewType;
class IMp7JrsSpaceResolutionViewType;
/** Smart pointer for instance of IMp7JrsSpaceResolutionViewType */
typedef Dc1Ptr< IMp7JrsSpaceResolutionViewType > Mp7JrsSpaceResolutionViewPtr;

#define TypeOfSpaceResolutionViewType_order_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceResolutionViewType_order_LocalType")))
#define CreateSpaceResolutionViewType_order_LocalType (Dc1Factory::CreateObject(TypeOfSpaceResolutionViewType_order_LocalType))
class Mp7JrsSpaceResolutionViewType_order_LocalType;
class IMp7JrsSpaceResolutionViewType_order_LocalType;
// No smart pointer instance for IMp7JrsSpaceResolutionViewType_order_LocalType

#define TypeOfSpaceTreeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceTreeType")))
#define CreateSpaceTreeType (Dc1Factory::CreateObject(TypeOfSpaceTreeType))
class Mp7JrsSpaceTreeType;
class IMp7JrsSpaceTreeType;
/** Smart pointer for instance of IMp7JrsSpaceTreeType */
typedef Dc1Ptr< IMp7JrsSpaceTreeType > Mp7JrsSpaceTreePtr;

#define TypeOfSpaceTreeType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceTreeType_CollectionType")))
#define CreateSpaceTreeType_CollectionType (Dc1Factory::CreateObject(TypeOfSpaceTreeType_CollectionType))
class Mp7JrsSpaceTreeType_CollectionType;
class IMp7JrsSpaceTreeType_CollectionType;
/** Smart pointer for instance of IMp7JrsSpaceTreeType_CollectionType */
typedef Dc1Ptr< IMp7JrsSpaceTreeType_CollectionType > Mp7JrsSpaceTreeType_CollectionPtr;

#define TypeOfSpaceTreeType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceTreeType_LocalType")))
#define CreateSpaceTreeType_LocalType (Dc1Factory::CreateObject(TypeOfSpaceTreeType_LocalType))
class Mp7JrsSpaceTreeType_LocalType;
class IMp7JrsSpaceTreeType_LocalType;
/** Smart pointer for instance of IMp7JrsSpaceTreeType_LocalType */
typedef Dc1Ptr< IMp7JrsSpaceTreeType_LocalType > Mp7JrsSpaceTreeType_LocalPtr;

#define TypeOfSpaceViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceViewType")))
#define CreateSpaceViewType (Dc1Factory::CreateObject(TypeOfSpaceViewType))
class Mp7JrsSpaceViewType;
class IMp7JrsSpaceViewType;
/** Smart pointer for instance of IMp7JrsSpaceViewType */
typedef Dc1Ptr< IMp7JrsSpaceViewType > Mp7JrsSpaceViewPtr;

#define TypeOfSpatial2DCoordinateSystemType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType")))
#define CreateSpatial2DCoordinateSystemType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType))
class Mp7JrsSpatial2DCoordinateSystemType;
class IMp7JrsSpatial2DCoordinateSystemType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType > Mp7JrsSpatial2DCoordinateSystemPtr;

#define TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType")))
#define CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType))
class Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType;
class IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionType > Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_CollectionPtr;

#define TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType")))
#define CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType > Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0")))
#define CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0 (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0))
class Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0;
class IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0 */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType0 > Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_Local0Ptr;

#define TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType")))
#define CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType;
// No smart pointer instance for IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_modelType_LocalType

#define TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType")))
#define CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType))
class Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType;
class IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType > Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0 (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0 */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0 > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1 (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1 */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1 > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local1Ptr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType")))
#define CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType;
/** Smart pointer for instance of IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType */
typedef Dc1Ptr< IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType > Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr;

#define TypeOfSpatial2DCoordinateSystemType_Unit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_Unit_LocalType")))
#define CreateSpatial2DCoordinateSystemType_Unit_LocalType (Dc1Factory::CreateObject(TypeOfSpatial2DCoordinateSystemType_Unit_LocalType))
class Mp7JrsSpatial2DCoordinateSystemType_Unit_LocalType;
class IMp7JrsSpatial2DCoordinateSystemType_Unit_LocalType;
// No smart pointer instance for IMp7JrsSpatial2DCoordinateSystemType_Unit_LocalType

#define TypeOfSpatialMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatialMaskType")))
#define CreateSpatialMaskType (Dc1Factory::CreateObject(TypeOfSpatialMaskType))
class Mp7JrsSpatialMaskType;
class IMp7JrsSpatialMaskType;
/** Smart pointer for instance of IMp7JrsSpatialMaskType */
typedef Dc1Ptr< IMp7JrsSpatialMaskType > Mp7JrsSpatialMaskPtr;

#define TypeOfSpatialMaskType_SubRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatialMaskType_SubRegion_CollectionType")))
#define CreateSpatialMaskType_SubRegion_CollectionType (Dc1Factory::CreateObject(TypeOfSpatialMaskType_SubRegion_CollectionType))
class Mp7JrsSpatialMaskType_SubRegion_CollectionType;
class IMp7JrsSpatialMaskType_SubRegion_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatialMaskType_SubRegion_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatialMaskType_SubRegion_CollectionType > Mp7JrsSpatialMaskType_SubRegion_CollectionPtr;

#define TypeOfSpatialSegmentDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatialSegmentDecompositionType")))
#define CreateSpatialSegmentDecompositionType (Dc1Factory::CreateObject(TypeOfSpatialSegmentDecompositionType))
class Mp7JrsSpatialSegmentDecompositionType;
class IMp7JrsSpatialSegmentDecompositionType;
/** Smart pointer for instance of IMp7JrsSpatialSegmentDecompositionType */
typedef Dc1Ptr< IMp7JrsSpatialSegmentDecompositionType > Mp7JrsSpatialSegmentDecompositionPtr;

#define TypeOfSpatioTemporalLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType")))
#define CreateSpatioTemporalLocatorType (Dc1Factory::CreateObject(TypeOfSpatioTemporalLocatorType))
class Mp7JrsSpatioTemporalLocatorType;
class IMp7JrsSpatioTemporalLocatorType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalLocatorType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType > Mp7JrsSpatioTemporalLocatorPtr;

#define TypeOfSpatioTemporalLocatorType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType_CollectionType")))
#define CreateSpatioTemporalLocatorType_CollectionType (Dc1Factory::CreateObject(TypeOfSpatioTemporalLocatorType_CollectionType))
class Mp7JrsSpatioTemporalLocatorType_CollectionType;
class IMp7JrsSpatioTemporalLocatorType_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalLocatorType_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType_CollectionType > Mp7JrsSpatioTemporalLocatorType_CollectionPtr;

#define TypeOfSpatioTemporalLocatorType_CoordRef_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType_CoordRef_LocalType")))
#define CreateSpatioTemporalLocatorType_CoordRef_LocalType (Dc1Factory::CreateObject(TypeOfSpatioTemporalLocatorType_CoordRef_LocalType))
class Mp7JrsSpatioTemporalLocatorType_CoordRef_LocalType;
class IMp7JrsSpatioTemporalLocatorType_CoordRef_LocalType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalLocatorType_CoordRef_LocalType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType_CoordRef_LocalType > Mp7JrsSpatioTemporalLocatorType_CoordRef_LocalPtr;

#define TypeOfSpatioTemporalLocatorType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType_LocalType")))
#define CreateSpatioTemporalLocatorType_LocalType (Dc1Factory::CreateObject(TypeOfSpatioTemporalLocatorType_LocalType))
class Mp7JrsSpatioTemporalLocatorType_LocalType;
class IMp7JrsSpatioTemporalLocatorType_LocalType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalLocatorType_LocalType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType_LocalType > Mp7JrsSpatioTemporalLocatorType_LocalPtr;

#define TypeOfSpatioTemporalMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalMaskType")))
#define CreateSpatioTemporalMaskType (Dc1Factory::CreateObject(TypeOfSpatioTemporalMaskType))
class Mp7JrsSpatioTemporalMaskType;
class IMp7JrsSpatioTemporalMaskType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalMaskType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalMaskType > Mp7JrsSpatioTemporalMaskPtr;

#define TypeOfSpatioTemporalMaskType_SubRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalMaskType_SubRegion_CollectionType")))
#define CreateSpatioTemporalMaskType_SubRegion_CollectionType (Dc1Factory::CreateObject(TypeOfSpatioTemporalMaskType_SubRegion_CollectionType))
class Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionType;
class IMp7JrsSpatioTemporalMaskType_SubRegion_CollectionType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalMaskType_SubRegion_CollectionType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalMaskType_SubRegion_CollectionType > Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr;

#define TypeOfSpatioTemporalSegmentDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalSegmentDecompositionType")))
#define CreateSpatioTemporalSegmentDecompositionType (Dc1Factory::CreateObject(TypeOfSpatioTemporalSegmentDecompositionType))
class Mp7JrsSpatioTemporalSegmentDecompositionType;
class IMp7JrsSpatioTemporalSegmentDecompositionType;
/** Smart pointer for instance of IMp7JrsSpatioTemporalSegmentDecompositionType */
typedef Dc1Ptr< IMp7JrsSpatioTemporalSegmentDecompositionType > Mp7JrsSpatioTemporalSegmentDecompositionPtr;

#define TypeOfSpeakerInfoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType")))
#define CreateSpeakerInfoType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType))
class Mp7JrsSpeakerInfoType;
class IMp7JrsSpeakerInfoType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType > Mp7JrsSpeakerInfoPtr;

#define TypeOfSpeakerInfoType_PhoneIndex_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_LocalType")))
#define CreateSpeakerInfoType_PhoneIndex_LocalType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_PhoneIndex_LocalType))
class Mp7JrsSpeakerInfoType_PhoneIndex_LocalType;
class IMp7JrsSpeakerInfoType_PhoneIndex_LocalType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_PhoneIndex_LocalType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_LocalType > Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr;

#define TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType")))
#define CreateSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType))
class Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType;
class IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType > Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionPtr;

#define TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType")))
#define CreateSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType))
class Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType;
class IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionType > Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_IndexEntry_CollectionPtr;

#define TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType")))
#define CreateSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType))
class Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType;
class IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionType > Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_key_CollectionPtr;

#define TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType")))
#define CreateSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType))
class Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType;
class IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType > Mp7JrsSpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalPtr;

#define TypeOfSpeakerInfoType_provenance_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_provenance_LocalType")))
#define CreateSpeakerInfoType_provenance_LocalType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_provenance_LocalType))
class Mp7JrsSpeakerInfoType_provenance_LocalType;
class IMp7JrsSpeakerInfoType_provenance_LocalType;
// No smart pointer instance for IMp7JrsSpeakerInfoType_provenance_LocalType

#define TypeOfSpeakerInfoType_WordIndex_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_LocalType")))
#define CreateSpeakerInfoType_WordIndex_LocalType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_WordIndex_LocalType))
class Mp7JrsSpeakerInfoType_WordIndex_LocalType;
class IMp7JrsSpeakerInfoType_WordIndex_LocalType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_WordIndex_LocalType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_LocalType > Mp7JrsSpeakerInfoType_WordIndex_LocalPtr;

#define TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_WordIndexEntry_CollectionType")))
#define CreateSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType))
class Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType;
class IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionType > Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_CollectionPtr;

#define TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType")))
#define CreateSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType))
class Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType;
class IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionType > Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_IndexEntry_CollectionPtr;

#define TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType")))
#define CreateSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType))
class Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType;
class IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionType > Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_key_CollectionPtr;

#define TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_WordIndexEntry_LocalType")))
#define CreateSpeakerInfoType_WordIndex_WordIndexEntry_LocalType (Dc1Factory::CreateObject(TypeOfSpeakerInfoType_WordIndex_WordIndexEntry_LocalType))
class Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalType;
class IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalType;
/** Smart pointer for instance of IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalType */
typedef Dc1Ptr< IMp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalType > Mp7JrsSpeakerInfoType_WordIndex_WordIndexEntry_LocalPtr;

#define TypeOfSpectralCentroidType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpectralCentroidType")))
#define CreateSpectralCentroidType (Dc1Factory::CreateObject(TypeOfSpectralCentroidType))
class Mp7JrsSpectralCentroidType;
class IMp7JrsSpectralCentroidType;
/** Smart pointer for instance of IMp7JrsSpectralCentroidType */
typedef Dc1Ptr< IMp7JrsSpectralCentroidType > Mp7JrsSpectralCentroidPtr;

#define TypeOfSpokenContentHeaderType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentHeaderType")))
#define CreateSpokenContentHeaderType (Dc1Factory::CreateObject(TypeOfSpokenContentHeaderType))
class Mp7JrsSpokenContentHeaderType;
class IMp7JrsSpokenContentHeaderType;
/** Smart pointer for instance of IMp7JrsSpokenContentHeaderType */
typedef Dc1Ptr< IMp7JrsSpokenContentHeaderType > Mp7JrsSpokenContentHeaderPtr;

#define TypeOfSpokenContentHeaderType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentHeaderType_CollectionType")))
#define CreateSpokenContentHeaderType_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentHeaderType_CollectionType))
class Mp7JrsSpokenContentHeaderType_CollectionType;
class IMp7JrsSpokenContentHeaderType_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentHeaderType_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentHeaderType_CollectionType > Mp7JrsSpokenContentHeaderType_CollectionPtr;

#define TypeOfSpokenContentHeaderType_ConfusionInfo_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentHeaderType_ConfusionInfo_CollectionType")))
#define CreateSpokenContentHeaderType_ConfusionInfo_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentHeaderType_ConfusionInfo_CollectionType))
class Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionType;
class IMp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionType > Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr;

#define TypeOfSpokenContentHeaderType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentHeaderType_LocalType")))
#define CreateSpokenContentHeaderType_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentHeaderType_LocalType))
class Mp7JrsSpokenContentHeaderType_LocalType;
class IMp7JrsSpokenContentHeaderType_LocalType;
/** Smart pointer for instance of IMp7JrsSpokenContentHeaderType_LocalType */
typedef Dc1Ptr< IMp7JrsSpokenContentHeaderType_LocalType > Mp7JrsSpokenContentHeaderType_LocalPtr;

#define TypeOfSpokenContentHeaderType_SpeakerInfo_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentHeaderType_SpeakerInfo_CollectionType")))
#define CreateSpokenContentHeaderType_SpeakerInfo_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentHeaderType_SpeakerInfo_CollectionType))
class Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionType;
class IMp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionType > Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr;

#define TypeOfSpokenContentIndexEntryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentIndexEntryType")))
#define CreateSpokenContentIndexEntryType (Dc1Factory::CreateObject(TypeOfSpokenContentIndexEntryType))
class Mp7JrsSpokenContentIndexEntryType;
class IMp7JrsSpokenContentIndexEntryType;
/** Smart pointer for instance of IMp7JrsSpokenContentIndexEntryType */
typedef Dc1Ptr< IMp7JrsSpokenContentIndexEntryType > Mp7JrsSpokenContentIndexEntryPtr;

#define TypeOfSpokenContentLatticeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType")))
#define CreateSpokenContentLatticeType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType))
class Mp7JrsSpokenContentLatticeType;
class IMp7JrsSpokenContentLatticeType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType > Mp7JrsSpokenContentLatticePtr;

#define TypeOfSpokenContentLatticeType_Block_audio_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_audio_LocalType")))
#define CreateSpokenContentLatticeType_Block_audio_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_audio_LocalType))
class Mp7JrsSpokenContentLatticeType_Block_audio_LocalType;
class IMp7JrsSpokenContentLatticeType_Block_audio_LocalType;
// No smart pointer instance for IMp7JrsSpokenContentLatticeType_Block_audio_LocalType

#define TypeOfSpokenContentLatticeType_Block_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_CollectionType")))
#define CreateSpokenContentLatticeType_Block_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_CollectionType))
class Mp7JrsSpokenContentLatticeType_Block_CollectionType;
class IMp7JrsSpokenContentLatticeType_Block_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_CollectionType > Mp7JrsSpokenContentLatticeType_Block_CollectionPtr;

#define TypeOfSpokenContentLatticeType_Block_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_LocalType")))
#define CreateSpokenContentLatticeType_Block_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_LocalType))
class Mp7JrsSpokenContentLatticeType_Block_LocalType;
class IMp7JrsSpokenContentLatticeType_Block_LocalType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_LocalType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_LocalType > Mp7JrsSpokenContentLatticeType_Block_LocalPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_CollectionType")))
#define CreateSpokenContentLatticeType_Block_Node_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_CollectionType))
class Mp7JrsSpokenContentLatticeType_Block_Node_CollectionType;
class IMp7JrsSpokenContentLatticeType_Block_Node_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_CollectionType > Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_LocalType")))
#define CreateSpokenContentLatticeType_Block_Node_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_LocalType))
class Mp7JrsSpokenContentLatticeType_Block_Node_LocalType;
class IMp7JrsSpokenContentLatticeType_Block_Node_LocalType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_LocalType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_LocalType > Mp7JrsSpokenContentLatticeType_Block_Node_LocalPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_PhoneLink_CollectionType")))
#define CreateSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType))
class Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType;
class IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType > Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_PhoneLink_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_PhoneLink_LocalType")))
#define CreateSpokenContentLatticeType_Block_Node_PhoneLink_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_PhoneLink_LocalType))
class Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalType;
class IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalType > Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_WordLink_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_WordLink_CollectionType")))
#define CreateSpokenContentLatticeType_Block_Node_WordLink_CollectionType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_WordLink_CollectionType))
class Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionType;
class IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionType > Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr;

#define TypeOfSpokenContentLatticeType_Block_Node_WordLink_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_WordLink_LocalType")))
#define CreateSpokenContentLatticeType_Block_Node_WordLink_LocalType (Dc1Factory::CreateObject(TypeOfSpokenContentLatticeType_Block_Node_WordLink_LocalType))
class Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalType;
class IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalType;
/** Smart pointer for instance of IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalType */
typedef Dc1Ptr< IMp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalType > Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalPtr;

#define TypeOfSpokenContentLinkType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLinkType")))
#define CreateSpokenContentLinkType (Dc1Factory::CreateObject(TypeOfSpokenContentLinkType))
class Mp7JrsSpokenContentLinkType;
class IMp7JrsSpokenContentLinkType;
/** Smart pointer for instance of IMp7JrsSpokenContentLinkType */
typedef Dc1Ptr< IMp7JrsSpokenContentLinkType > Mp7JrsSpokenContentLinkPtr;

#define TypeOfStateTransitionModelType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StateTransitionModelType")))
#define CreateStateTransitionModelType (Dc1Factory::CreateObject(TypeOfStateTransitionModelType))
class Mp7JrsStateTransitionModelType;
class IMp7JrsStateTransitionModelType;
/** Smart pointer for instance of IMp7JrsStateTransitionModelType */
typedef Dc1Ptr< IMp7JrsStateTransitionModelType > Mp7JrsStateTransitionModelPtr;

#define TypeOfStateTransitionModelType_State_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StateTransitionModelType_State_CollectionType")))
#define CreateStateTransitionModelType_State_CollectionType (Dc1Factory::CreateObject(TypeOfStateTransitionModelType_State_CollectionType))
class Mp7JrsStateTransitionModelType_State_CollectionType;
class IMp7JrsStateTransitionModelType_State_CollectionType;
/** Smart pointer for instance of IMp7JrsStateTransitionModelType_State_CollectionType */
typedef Dc1Ptr< IMp7JrsStateTransitionModelType_State_CollectionType > Mp7JrsStateTransitionModelType_State_CollectionPtr;

#define TypeOfStillRegion3DSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DSpatialDecompositionType")))
#define CreateStillRegion3DSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfStillRegion3DSpatialDecompositionType))
class Mp7JrsStillRegion3DSpatialDecompositionType;
class IMp7JrsStillRegion3DSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsStillRegion3DSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsStillRegion3DSpatialDecompositionType > Mp7JrsStillRegion3DSpatialDecompositionPtr;

#define TypeOfStillRegion3DSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DSpatialDecompositionType_CollectionType")))
#define CreateStillRegion3DSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegion3DSpatialDecompositionType_CollectionType))
class Mp7JrsStillRegion3DSpatialDecompositionType_CollectionType;
class IMp7JrsStillRegion3DSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegion3DSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegion3DSpatialDecompositionType_CollectionType > Mp7JrsStillRegion3DSpatialDecompositionType_CollectionPtr;

#define TypeOfStillRegion3DSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DSpatialDecompositionType_LocalType")))
#define CreateStillRegion3DSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfStillRegion3DSpatialDecompositionType_LocalType))
class Mp7JrsStillRegion3DSpatialDecompositionType_LocalType;
class IMp7JrsStillRegion3DSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsStillRegion3DSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsStillRegion3DSpatialDecompositionType_LocalType > Mp7JrsStillRegion3DSpatialDecompositionType_LocalPtr;

#define TypeOfStillRegion3DType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DType")))
#define CreateStillRegion3DType (Dc1Factory::CreateObject(TypeOfStillRegion3DType))
class Mp7JrsStillRegion3DType;
class IMp7JrsStillRegion3DType;
/** Smart pointer for instance of IMp7JrsStillRegion3DType */
typedef Dc1Ptr< IMp7JrsStillRegion3DType > Mp7JrsStillRegion3DPtr;

#define TypeOfStillRegion3DType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DType_CollectionType")))
#define CreateStillRegion3DType_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegion3DType_CollectionType))
class Mp7JrsStillRegion3DType_CollectionType;
class IMp7JrsStillRegion3DType_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegion3DType_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegion3DType_CollectionType > Mp7JrsStillRegion3DType_CollectionPtr;

#define TypeOfStillRegion3DType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DType_LocalType")))
#define CreateStillRegion3DType_LocalType (Dc1Factory::CreateObject(TypeOfStillRegion3DType_LocalType))
class Mp7JrsStillRegion3DType_LocalType;
class IMp7JrsStillRegion3DType_LocalType;
/** Smart pointer for instance of IMp7JrsStillRegion3DType_LocalType */
typedef Dc1Ptr< IMp7JrsStillRegion3DType_LocalType > Mp7JrsStillRegion3DType_LocalPtr;

#define TypeOfStillRegion3DType_SpatialDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DType_SpatialDecomposition_CollectionType")))
#define CreateStillRegion3DType_SpatialDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegion3DType_SpatialDecomposition_CollectionType))
class Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionType;
class IMp7JrsStillRegion3DType_SpatialDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegion3DType_SpatialDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegion3DType_SpatialDecomposition_CollectionType > Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr;

#define TypeOfStillRegionFeatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionFeatureType")))
#define CreateStillRegionFeatureType (Dc1Factory::CreateObject(TypeOfStillRegionFeatureType))
class Mp7JrsStillRegionFeatureType;
class IMp7JrsStillRegionFeatureType;
/** Smart pointer for instance of IMp7JrsStillRegionFeatureType */
typedef Dc1Ptr< IMp7JrsStillRegionFeatureType > Mp7JrsStillRegionFeaturePtr;

#define TypeOfStillRegionSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionSpatialDecompositionType")))
#define CreateStillRegionSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfStillRegionSpatialDecompositionType))
class Mp7JrsStillRegionSpatialDecompositionType;
class IMp7JrsStillRegionSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsStillRegionSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsStillRegionSpatialDecompositionType > Mp7JrsStillRegionSpatialDecompositionPtr;

#define TypeOfStillRegionSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionSpatialDecompositionType_CollectionType")))
#define CreateStillRegionSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegionSpatialDecompositionType_CollectionType))
class Mp7JrsStillRegionSpatialDecompositionType_CollectionType;
class IMp7JrsStillRegionSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegionSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegionSpatialDecompositionType_CollectionType > Mp7JrsStillRegionSpatialDecompositionType_CollectionPtr;

#define TypeOfStillRegionSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionSpatialDecompositionType_LocalType")))
#define CreateStillRegionSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfStillRegionSpatialDecompositionType_LocalType))
class Mp7JrsStillRegionSpatialDecompositionType_LocalType;
class IMp7JrsStillRegionSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsStillRegionSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsStillRegionSpatialDecompositionType_LocalType > Mp7JrsStillRegionSpatialDecompositionType_LocalPtr;

#define TypeOfStillRegionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType")))
#define CreateStillRegionType (Dc1Factory::CreateObject(TypeOfStillRegionType))
class Mp7JrsStillRegionType;
class IMp7JrsStillRegionType;
/** Smart pointer for instance of IMp7JrsStillRegionType */
typedef Dc1Ptr< IMp7JrsStillRegionType > Mp7JrsStillRegionPtr;

#define TypeOfStillRegionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType_CollectionType")))
#define CreateStillRegionType_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegionType_CollectionType))
class Mp7JrsStillRegionType_CollectionType;
class IMp7JrsStillRegionType_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegionType_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegionType_CollectionType > Mp7JrsStillRegionType_CollectionPtr;

#define TypeOfStillRegionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType_LocalType")))
#define CreateStillRegionType_LocalType (Dc1Factory::CreateObject(TypeOfStillRegionType_LocalType))
class Mp7JrsStillRegionType_LocalType;
class IMp7JrsStillRegionType_LocalType;
/** Smart pointer for instance of IMp7JrsStillRegionType_LocalType */
typedef Dc1Ptr< IMp7JrsStillRegionType_LocalType > Mp7JrsStillRegionType_LocalPtr;

#define TypeOfStillRegionType_SpatialDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType_SpatialDecomposition_CollectionType")))
#define CreateStillRegionType_SpatialDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfStillRegionType_SpatialDecomposition_CollectionType))
class Mp7JrsStillRegionType_SpatialDecomposition_CollectionType;
class IMp7JrsStillRegionType_SpatialDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsStillRegionType_SpatialDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsStillRegionType_SpatialDecomposition_CollectionType > Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr;

#define TypeOfStreamLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamLocatorType")))
#define CreateStreamLocatorType (Dc1Factory::CreateObject(TypeOfStreamLocatorType))
class Mp7JrsStreamLocatorType;
class IMp7JrsStreamLocatorType;
/** Smart pointer for instance of IMp7JrsStreamLocatorType */
typedef Dc1Ptr< IMp7JrsStreamLocatorType > Mp7JrsStreamLocatorPtr;

#define TypeOfStreamMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamMaskType")))
#define CreateStreamMaskType (Dc1Factory::CreateObject(TypeOfStreamMaskType))
class Mp7JrsStreamMaskType;
class IMp7JrsStreamMaskType;
/** Smart pointer for instance of IMp7JrsStreamMaskType */
typedef Dc1Ptr< IMp7JrsStreamMaskType > Mp7JrsStreamMaskPtr;

#define TypeOfStreamMaskType_StreamSection_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamMaskType_StreamSection_CollectionType")))
#define CreateStreamMaskType_StreamSection_CollectionType (Dc1Factory::CreateObject(TypeOfStreamMaskType_StreamSection_CollectionType))
class Mp7JrsStreamMaskType_StreamSection_CollectionType;
class IMp7JrsStreamMaskType_StreamSection_CollectionType;
/** Smart pointer for instance of IMp7JrsStreamMaskType_StreamSection_CollectionType */
typedef Dc1Ptr< IMp7JrsStreamMaskType_StreamSection_CollectionType > Mp7JrsStreamMaskType_StreamSection_CollectionPtr;

#define TypeOfStreamSectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamSectionType")))
#define CreateStreamSectionType (Dc1Factory::CreateObject(TypeOfStreamSectionType))
class Mp7JrsStreamSectionType;
class IMp7JrsStreamSectionType;
/** Smart pointer for instance of IMp7JrsStreamSectionType */
typedef Dc1Ptr< IMp7JrsStreamSectionType > Mp7JrsStreamSectionPtr;

#define TypeOfStructuredAnnotationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType")))
#define CreateStructuredAnnotationType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType))
class Mp7JrsStructuredAnnotationType;
class IMp7JrsStructuredAnnotationType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType > Mp7JrsStructuredAnnotationPtr;

#define TypeOfStructuredAnnotationType_How_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_How_CollectionType")))
#define CreateStructuredAnnotationType_How_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_How_CollectionType))
class Mp7JrsStructuredAnnotationType_How_CollectionType;
class IMp7JrsStructuredAnnotationType_How_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_How_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_How_CollectionType > Mp7JrsStructuredAnnotationType_How_CollectionPtr;

#define TypeOfStructuredAnnotationType_WhatAction_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_WhatAction_CollectionType")))
#define CreateStructuredAnnotationType_WhatAction_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_WhatAction_CollectionType))
class Mp7JrsStructuredAnnotationType_WhatAction_CollectionType;
class IMp7JrsStructuredAnnotationType_WhatAction_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_WhatAction_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_WhatAction_CollectionType > Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr;

#define TypeOfStructuredAnnotationType_WhatObject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_WhatObject_CollectionType")))
#define CreateStructuredAnnotationType_WhatObject_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_WhatObject_CollectionType))
class Mp7JrsStructuredAnnotationType_WhatObject_CollectionType;
class IMp7JrsStructuredAnnotationType_WhatObject_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_WhatObject_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_WhatObject_CollectionType > Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr;

#define TypeOfStructuredAnnotationType_When_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_When_CollectionType")))
#define CreateStructuredAnnotationType_When_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_When_CollectionType))
class Mp7JrsStructuredAnnotationType_When_CollectionType;
class IMp7JrsStructuredAnnotationType_When_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_When_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_When_CollectionType > Mp7JrsStructuredAnnotationType_When_CollectionPtr;

#define TypeOfStructuredAnnotationType_Where_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_Where_CollectionType")))
#define CreateStructuredAnnotationType_Where_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_Where_CollectionType))
class Mp7JrsStructuredAnnotationType_Where_CollectionType;
class IMp7JrsStructuredAnnotationType_Where_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_Where_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_Where_CollectionType > Mp7JrsStructuredAnnotationType_Where_CollectionPtr;

#define TypeOfStructuredAnnotationType_Who_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_Who_CollectionType")))
#define CreateStructuredAnnotationType_Who_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_Who_CollectionType))
class Mp7JrsStructuredAnnotationType_Who_CollectionType;
class IMp7JrsStructuredAnnotationType_Who_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_Who_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_Who_CollectionType > Mp7JrsStructuredAnnotationType_Who_CollectionPtr;

#define TypeOfStructuredAnnotationType_Why_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType_Why_CollectionType")))
#define CreateStructuredAnnotationType_Why_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredAnnotationType_Why_CollectionType))
class Mp7JrsStructuredAnnotationType_Why_CollectionType;
class IMp7JrsStructuredAnnotationType_Why_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredAnnotationType_Why_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredAnnotationType_Why_CollectionType > Mp7JrsStructuredAnnotationType_Why_CollectionPtr;

#define TypeOfStructuredCollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType")))
#define CreateStructuredCollectionType (Dc1Factory::CreateObject(TypeOfStructuredCollectionType))
class Mp7JrsStructuredCollectionType;
class IMp7JrsStructuredCollectionType;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType > Mp7JrsStructuredCollectionPtr;

#define TypeOfStructuredCollectionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_CollectionType")))
#define CreateStructuredCollectionType_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_CollectionType))
class Mp7JrsStructuredCollectionType_CollectionType;
class IMp7JrsStructuredCollectionType_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_CollectionType > Mp7JrsStructuredCollectionType_CollectionPtr;

#define TypeOfStructuredCollectionType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_CollectionType0")))
#define CreateStructuredCollectionType_CollectionType0 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_CollectionType0))
class Mp7JrsStructuredCollectionType_CollectionType0;
class IMp7JrsStructuredCollectionType_CollectionType0;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_CollectionType0 > Mp7JrsStructuredCollectionType_Collection0Ptr;

#define TypeOfStructuredCollectionType_CollectionType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_CollectionType1")))
#define CreateStructuredCollectionType_CollectionType1 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_CollectionType1))
class Mp7JrsStructuredCollectionType_CollectionType1;
class IMp7JrsStructuredCollectionType_CollectionType1;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_CollectionType1 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_CollectionType1 > Mp7JrsStructuredCollectionType_Collection1Ptr;

#define TypeOfStructuredCollectionType_CollectionType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_CollectionType2")))
#define CreateStructuredCollectionType_CollectionType2 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_CollectionType2))
class Mp7JrsStructuredCollectionType_CollectionType2;
class IMp7JrsStructuredCollectionType_CollectionType2;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_CollectionType2 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_CollectionType2 > Mp7JrsStructuredCollectionType_Collection2Ptr;

#define TypeOfStructuredCollectionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_LocalType")))
#define CreateStructuredCollectionType_LocalType (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_LocalType))
class Mp7JrsStructuredCollectionType_LocalType;
class IMp7JrsStructuredCollectionType_LocalType;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_LocalType */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_LocalType > Mp7JrsStructuredCollectionType_LocalPtr;

#define TypeOfStructuredCollectionType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_LocalType0")))
#define CreateStructuredCollectionType_LocalType0 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_LocalType0))
class Mp7JrsStructuredCollectionType_LocalType0;
class IMp7JrsStructuredCollectionType_LocalType0;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_LocalType0 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_LocalType0 > Mp7JrsStructuredCollectionType_Local0Ptr;

#define TypeOfStructuredCollectionType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_LocalType1")))
#define CreateStructuredCollectionType_LocalType1 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_LocalType1))
class Mp7JrsStructuredCollectionType_LocalType1;
class IMp7JrsStructuredCollectionType_LocalType1;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_LocalType1 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_LocalType1 > Mp7JrsStructuredCollectionType_Local1Ptr;

#define TypeOfStructuredCollectionType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_LocalType2")))
#define CreateStructuredCollectionType_LocalType2 (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_LocalType2))
class Mp7JrsStructuredCollectionType_LocalType2;
class IMp7JrsStructuredCollectionType_LocalType2;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_LocalType2 */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_LocalType2 > Mp7JrsStructuredCollectionType_Local2Ptr;

#define TypeOfStructuredCollectionType_Relationships_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType_Relationships_CollectionType")))
#define CreateStructuredCollectionType_Relationships_CollectionType (Dc1Factory::CreateObject(TypeOfStructuredCollectionType_Relationships_CollectionType))
class Mp7JrsStructuredCollectionType_Relationships_CollectionType;
class IMp7JrsStructuredCollectionType_Relationships_CollectionType;
/** Smart pointer for instance of IMp7JrsStructuredCollectionType_Relationships_CollectionType */
typedef Dc1Ptr< IMp7JrsStructuredCollectionType_Relationships_CollectionType > Mp7JrsStructuredCollectionType_Relationships_CollectionPtr;

#define TypeOfSubjectClassificationSchemeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectClassificationSchemeType")))
#define CreateSubjectClassificationSchemeType (Dc1Factory::CreateObject(TypeOfSubjectClassificationSchemeType))
class Mp7JrsSubjectClassificationSchemeType;
class IMp7JrsSubjectClassificationSchemeType;
/** Smart pointer for instance of IMp7JrsSubjectClassificationSchemeType */
typedef Dc1Ptr< IMp7JrsSubjectClassificationSchemeType > Mp7JrsSubjectClassificationSchemePtr;

#define TypeOfSubjectClassificationSchemeType_Term_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectClassificationSchemeType_Term_CollectionType")))
#define CreateSubjectClassificationSchemeType_Term_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectClassificationSchemeType_Term_CollectionType))
class Mp7JrsSubjectClassificationSchemeType_Term_CollectionType;
class IMp7JrsSubjectClassificationSchemeType_Term_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectClassificationSchemeType_Term_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectClassificationSchemeType_Term_CollectionType > Mp7JrsSubjectClassificationSchemeType_Term_CollectionPtr;

#define TypeOfSubjectTermDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType")))
#define CreateSubjectTermDefinitionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType))
class Mp7JrsSubjectTermDefinitionType;
class IMp7JrsSubjectTermDefinitionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType > Mp7JrsSubjectTermDefinitionPtr;

#define TypeOfSubjectTermDefinitionType_Classification_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Classification_CollectionType")))
#define CreateSubjectTermDefinitionType_Classification_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Classification_CollectionType))
class Mp7JrsSubjectTermDefinitionType_Classification_CollectionType;
class IMp7JrsSubjectTermDefinitionType_Classification_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Classification_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Classification_CollectionType > Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr;

#define TypeOfSubjectTermDefinitionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_CollectionType")))
#define CreateSubjectTermDefinitionType_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_CollectionType))
class Mp7JrsSubjectTermDefinitionType_CollectionType;
class IMp7JrsSubjectTermDefinitionType_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_CollectionType > Mp7JrsSubjectTermDefinitionType_CollectionPtr;

#define TypeOfSubjectTermDefinitionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_LocalType")))
#define CreateSubjectTermDefinitionType_LocalType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_LocalType))
class Mp7JrsSubjectTermDefinitionType_LocalType;
class IMp7JrsSubjectTermDefinitionType_LocalType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_LocalType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_LocalType > Mp7JrsSubjectTermDefinitionType_LocalPtr;

#define TypeOfSubjectTermDefinitionType_Note_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Note_CollectionType")))
#define CreateSubjectTermDefinitionType_Note_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Note_CollectionType))
class Mp7JrsSubjectTermDefinitionType_Note_CollectionType;
class IMp7JrsSubjectTermDefinitionType_Note_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Note_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Note_CollectionType > Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr;

#define TypeOfSubjectTermDefinitionType_Note_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Note_LocalType")))
#define CreateSubjectTermDefinitionType_Note_LocalType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Note_LocalType))
class Mp7JrsSubjectTermDefinitionType_Note_LocalType;
class IMp7JrsSubjectTermDefinitionType_Note_LocalType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Note_LocalType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Note_LocalType > Mp7JrsSubjectTermDefinitionType_Note_LocalPtr;

#define TypeOfSubjectTermDefinitionType_Subdivision_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Subdivision_CollectionType")))
#define CreateSubjectTermDefinitionType_Subdivision_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Subdivision_CollectionType))
class Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionType;
class IMp7JrsSubjectTermDefinitionType_Subdivision_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Subdivision_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Subdivision_CollectionType > Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr;

#define TypeOfSubjectTermDefinitionType_Term_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Term_CollectionType")))
#define CreateSubjectTermDefinitionType_Term_CollectionType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Term_CollectionType))
class Mp7JrsSubjectTermDefinitionType_Term_CollectionType;
class IMp7JrsSubjectTermDefinitionType_Term_CollectionType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Term_CollectionType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Term_CollectionType > Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr;

#define TypeOfSubjectTermDefinitionType_Term_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SubjectTermDefinitionType_Term_LocalType")))
#define CreateSubjectTermDefinitionType_Term_LocalType (Dc1Factory::CreateObject(TypeOfSubjectTermDefinitionType_Term_LocalType))
class Mp7JrsSubjectTermDefinitionType_Term_LocalType;
class IMp7JrsSubjectTermDefinitionType_Term_LocalType;
/** Smart pointer for instance of IMp7JrsSubjectTermDefinitionType_Term_LocalType */
typedef Dc1Ptr< IMp7JrsSubjectTermDefinitionType_Term_LocalType > Mp7JrsSubjectTermDefinitionType_Term_LocalPtr;

#define TypeOfSummarizationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarizationType")))
#define CreateSummarizationType (Dc1Factory::CreateObject(TypeOfSummarizationType))
class Mp7JrsSummarizationType;
class IMp7JrsSummarizationType;
/** Smart pointer for instance of IMp7JrsSummarizationType */
typedef Dc1Ptr< IMp7JrsSummarizationType > Mp7JrsSummarizationPtr;

#define TypeOfSummarizationType_Summary_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarizationType_Summary_CollectionType")))
#define CreateSummarizationType_Summary_CollectionType (Dc1Factory::CreateObject(TypeOfSummarizationType_Summary_CollectionType))
class Mp7JrsSummarizationType_Summary_CollectionType;
class IMp7JrsSummarizationType_Summary_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarizationType_Summary_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarizationType_Summary_CollectionType > Mp7JrsSummarizationType_Summary_CollectionPtr;

#define TypeOfsummaryComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:summaryComponentType")))
#define CreatesummaryComponentType (Dc1Factory::CreateObject(TypeOfsummaryComponentType))
class Mp7JrssummaryComponentType;
class IMp7JrssummaryComponentType;
/** Smart pointer for instance of IMp7JrssummaryComponentType */
typedef Dc1Ptr< IMp7JrssummaryComponentType > Mp7JrssummaryComponentPtr;

#define TypeOfsummaryComponentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:summaryComponentType_LocalType")))
#define CreatesummaryComponentType_LocalType (Dc1Factory::CreateObject(TypeOfsummaryComponentType_LocalType))
class Mp7JrssummaryComponentType_LocalType;
class IMp7JrssummaryComponentType_LocalType;
// No smart pointer instance for IMp7JrssummaryComponentType_LocalType

#define TypeOfsummaryComponentType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:summaryComponentType_LocalType0")))
#define CreatesummaryComponentType_LocalType0 (Dc1Factory::CreateObject(TypeOfsummaryComponentType_LocalType0))
class Mp7JrssummaryComponentType_LocalType0;
class IMp7JrssummaryComponentType_LocalType0;
/** Smart pointer for instance of IMp7JrssummaryComponentType_LocalType0 */
typedef Dc1Ptr< IMp7JrssummaryComponentType_LocalType0 > Mp7JrssummaryComponentType_Local0Ptr;

#define TypeOfsummaryComponentType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:summaryComponentType_LocalType1")))
#define CreatesummaryComponentType_LocalType1 (Dc1Factory::CreateObject(TypeOfsummaryComponentType_LocalType1))
class Mp7JrssummaryComponentType_LocalType1;
class IMp7JrssummaryComponentType_LocalType1;
/** Smart pointer for instance of IMp7JrssummaryComponentType_LocalType1 */
typedef Dc1Ptr< IMp7JrssummaryComponentType_LocalType1 > Mp7JrssummaryComponentType_Local1Ptr;

#define TypeOfSummaryDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryDescriptionType")))
#define CreateSummaryDescriptionType (Dc1Factory::CreateObject(TypeOfSummaryDescriptionType))
class Mp7JrsSummaryDescriptionType;
class IMp7JrsSummaryDescriptionType;
/** Smart pointer for instance of IMp7JrsSummaryDescriptionType */
typedef Dc1Ptr< IMp7JrsSummaryDescriptionType > Mp7JrsSummaryDescriptionPtr;

#define TypeOfSummaryDescriptionType_Summarization_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryDescriptionType_Summarization_CollectionType")))
#define CreateSummaryDescriptionType_Summarization_CollectionType (Dc1Factory::CreateObject(TypeOfSummaryDescriptionType_Summarization_CollectionType))
class Mp7JrsSummaryDescriptionType_Summarization_CollectionType;
class IMp7JrsSummaryDescriptionType_Summarization_CollectionType;
/** Smart pointer for instance of IMp7JrsSummaryDescriptionType_Summarization_CollectionType */
typedef Dc1Ptr< IMp7JrsSummaryDescriptionType_Summarization_CollectionType > Mp7JrsSummaryDescriptionType_Summarization_CollectionPtr;

#define TypeOfSummaryPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType")))
#define CreateSummaryPreferencesType (Dc1Factory::CreateObject(TypeOfSummaryPreferencesType))
class Mp7JrsSummaryPreferencesType;
class IMp7JrsSummaryPreferencesType;
/** Smart pointer for instance of IMp7JrsSummaryPreferencesType */
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType > Mp7JrsSummaryPreferencesPtr;

#define TypeOfSummaryPreferencesType_SummaryTheme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryTheme_CollectionType")))
#define CreateSummaryPreferencesType_SummaryTheme_CollectionType (Dc1Factory::CreateObject(TypeOfSummaryPreferencesType_SummaryTheme_CollectionType))
class Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionType;
class IMp7JrsSummaryPreferencesType_SummaryTheme_CollectionType;
/** Smart pointer for instance of IMp7JrsSummaryPreferencesType_SummaryTheme_CollectionType */
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryTheme_CollectionType > Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr;

#define TypeOfSummaryPreferencesType_SummaryTheme_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryTheme_LocalType")))
#define CreateSummaryPreferencesType_SummaryTheme_LocalType (Dc1Factory::CreateObject(TypeOfSummaryPreferencesType_SummaryTheme_LocalType))
class Mp7JrsSummaryPreferencesType_SummaryTheme_LocalType;
class IMp7JrsSummaryPreferencesType_SummaryTheme_LocalType;
/** Smart pointer for instance of IMp7JrsSummaryPreferencesType_SummaryTheme_LocalType */
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryTheme_LocalType > Mp7JrsSummaryPreferencesType_SummaryTheme_LocalPtr;

#define TypeOfSummaryPreferencesType_SummaryType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryType_CollectionType")))
#define CreateSummaryPreferencesType_SummaryType_CollectionType (Dc1Factory::CreateObject(TypeOfSummaryPreferencesType_SummaryType_CollectionType))
class Mp7JrsSummaryPreferencesType_SummaryType_CollectionType;
class IMp7JrsSummaryPreferencesType_SummaryType_CollectionType;
/** Smart pointer for instance of IMp7JrsSummaryPreferencesType_SummaryType_CollectionType */
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryType_CollectionType > Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr;

#define TypeOfSummaryPreferencesType_SummaryType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryType_LocalType")))
#define CreateSummaryPreferencesType_SummaryType_LocalType (Dc1Factory::CreateObject(TypeOfSummaryPreferencesType_SummaryType_LocalType))
class Mp7JrsSummaryPreferencesType_SummaryType_LocalType;
class IMp7JrsSummaryPreferencesType_SummaryType_LocalType;
/** Smart pointer for instance of IMp7JrsSummaryPreferencesType_SummaryType_LocalType */
typedef Dc1Ptr< IMp7JrsSummaryPreferencesType_SummaryType_LocalType > Mp7JrsSummaryPreferencesType_SummaryType_LocalPtr;

#define TypeOfSummarySegmentGroupType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType")))
#define CreateSummarySegmentGroupType (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType))
class Mp7JrsSummarySegmentGroupType;
class IMp7JrsSummarySegmentGroupType;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType > Mp7JrsSummarySegmentGroupPtr;

#define TypeOfSummarySegmentGroupType_Caption_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_Caption_CollectionType")))
#define CreateSummarySegmentGroupType_Caption_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_Caption_CollectionType))
class Mp7JrsSummarySegmentGroupType_Caption_CollectionType;
class IMp7JrsSummarySegmentGroupType_Caption_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_Caption_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_Caption_CollectionType > Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr;

#define TypeOfSummarySegmentGroupType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_CollectionType")))
#define CreateSummarySegmentGroupType_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_CollectionType))
class Mp7JrsSummarySegmentGroupType_CollectionType;
class IMp7JrsSummarySegmentGroupType_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_CollectionType > Mp7JrsSummarySegmentGroupType_CollectionPtr;

#define TypeOfSummarySegmentGroupType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_CollectionType0")))
#define CreateSummarySegmentGroupType_CollectionType0 (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_CollectionType0))
class Mp7JrsSummarySegmentGroupType_CollectionType0;
class IMp7JrsSummarySegmentGroupType_CollectionType0;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_CollectionType0 > Mp7JrsSummarySegmentGroupType_Collection0Ptr;

#define TypeOfSummarySegmentGroupType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_LocalType")))
#define CreateSummarySegmentGroupType_LocalType (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_LocalType))
class Mp7JrsSummarySegmentGroupType_LocalType;
class IMp7JrsSummarySegmentGroupType_LocalType;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_LocalType */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_LocalType > Mp7JrsSummarySegmentGroupType_LocalPtr;

#define TypeOfSummarySegmentGroupType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_LocalType0")))
#define CreateSummarySegmentGroupType_LocalType0 (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_LocalType0))
class Mp7JrsSummarySegmentGroupType_LocalType0;
class IMp7JrsSummarySegmentGroupType_LocalType0;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_LocalType0 */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_LocalType0 > Mp7JrsSummarySegmentGroupType_Local0Ptr;

#define TypeOfSummarySegmentGroupType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType_Name_CollectionType")))
#define CreateSummarySegmentGroupType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentGroupType_Name_CollectionType))
class Mp7JrsSummarySegmentGroupType_Name_CollectionType;
class IMp7JrsSummarySegmentGroupType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentGroupType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_Name_CollectionType > Mp7JrsSummarySegmentGroupType_Name_CollectionPtr;

#define TypeOfSummarySegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentType")))
#define CreateSummarySegmentType (Dc1Factory::CreateObject(TypeOfSummarySegmentType))
class Mp7JrsSummarySegmentType;
class IMp7JrsSummarySegmentType;
/** Smart pointer for instance of IMp7JrsSummarySegmentType */
typedef Dc1Ptr< IMp7JrsSummarySegmentType > Mp7JrsSummarySegmentPtr;

#define TypeOfSummarySegmentType_KeyFrame_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentType_KeyFrame_CollectionType")))
#define CreateSummarySegmentType_KeyFrame_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentType_KeyFrame_CollectionType))
class Mp7JrsSummarySegmentType_KeyFrame_CollectionType;
class IMp7JrsSummarySegmentType_KeyFrame_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentType_KeyFrame_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentType_KeyFrame_CollectionType > Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr;

#define TypeOfSummarySegmentType_KeySound_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentType_KeySound_CollectionType")))
#define CreateSummarySegmentType_KeySound_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentType_KeySound_CollectionType))
class Mp7JrsSummarySegmentType_KeySound_CollectionType;
class IMp7JrsSummarySegmentType_KeySound_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentType_KeySound_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentType_KeySound_CollectionType > Mp7JrsSummarySegmentType_KeySound_CollectionPtr;

#define TypeOfSummarySegmentType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentType_Name_CollectionType")))
#define CreateSummarySegmentType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfSummarySegmentType_Name_CollectionType))
class Mp7JrsSummarySegmentType_Name_CollectionType;
class IMp7JrsSummarySegmentType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsSummarySegmentType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsSummarySegmentType_Name_CollectionType > Mp7JrsSummarySegmentType_Name_CollectionPtr;

#define TypeOfSummaryThemeListType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryThemeListType")))
#define CreateSummaryThemeListType (Dc1Factory::CreateObject(TypeOfSummaryThemeListType))
class Mp7JrsSummaryThemeListType;
class IMp7JrsSummaryThemeListType;
/** Smart pointer for instance of IMp7JrsSummaryThemeListType */
typedef Dc1Ptr< IMp7JrsSummaryThemeListType > Mp7JrsSummaryThemeListPtr;

#define TypeOfSummaryThemeListType_SummaryTheme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryThemeListType_SummaryTheme_CollectionType")))
#define CreateSummaryThemeListType_SummaryTheme_CollectionType (Dc1Factory::CreateObject(TypeOfSummaryThemeListType_SummaryTheme_CollectionType))
class Mp7JrsSummaryThemeListType_SummaryTheme_CollectionType;
class IMp7JrsSummaryThemeListType_SummaryTheme_CollectionType;
/** Smart pointer for instance of IMp7JrsSummaryThemeListType_SummaryTheme_CollectionType */
typedef Dc1Ptr< IMp7JrsSummaryThemeListType_SummaryTheme_CollectionType > Mp7JrsSummaryThemeListType_SummaryTheme_CollectionPtr;

#define TypeOfSummaryThemeListType_SummaryTheme_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryThemeListType_SummaryTheme_LocalType")))
#define CreateSummaryThemeListType_SummaryTheme_LocalType (Dc1Factory::CreateObject(TypeOfSummaryThemeListType_SummaryTheme_LocalType))
class Mp7JrsSummaryThemeListType_SummaryTheme_LocalType;
class IMp7JrsSummaryThemeListType_SummaryTheme_LocalType;
/** Smart pointer for instance of IMp7JrsSummaryThemeListType_SummaryTheme_LocalType */
typedef Dc1Ptr< IMp7JrsSummaryThemeListType_SummaryTheme_LocalType > Mp7JrsSummaryThemeListType_SummaryTheme_LocalPtr;

#define TypeOfSummaryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryType")))
#define CreateSummaryType (Dc1Factory::CreateObject(TypeOfSummaryType))
class Mp7JrsSummaryType;
class IMp7JrsSummaryType;
/** Smart pointer for instance of IMp7JrsSummaryType */
typedef Dc1Ptr< IMp7JrsSummaryType > Mp7JrsSummaryPtr;

#define TypeOfSummaryType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryType_Name_CollectionType")))
#define CreateSummaryType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfSummaryType_Name_CollectionType))
class Mp7JrsSummaryType_Name_CollectionType;
class IMp7JrsSummaryType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsSummaryType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsSummaryType_Name_CollectionType > Mp7JrsSummaryType_Name_CollectionPtr;

#define TypeOfSyntacticConstituentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType")))
#define CreateSyntacticConstituentType (Dc1Factory::CreateObject(TypeOfSyntacticConstituentType))
class Mp7JrsSyntacticConstituentType;
class IMp7JrsSyntacticConstituentType;
/** Smart pointer for instance of IMp7JrsSyntacticConstituentType */
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType > Mp7JrsSyntacticConstituentPtr;

#define TypeOfSyntacticConstituentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType_CollectionType")))
#define CreateSyntacticConstituentType_CollectionType (Dc1Factory::CreateObject(TypeOfSyntacticConstituentType_CollectionType))
class Mp7JrsSyntacticConstituentType_CollectionType;
class IMp7JrsSyntacticConstituentType_CollectionType;
/** Smart pointer for instance of IMp7JrsSyntacticConstituentType_CollectionType */
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType_CollectionType > Mp7JrsSyntacticConstituentType_CollectionPtr;

#define TypeOfSyntacticConstituentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType_LocalType")))
#define CreateSyntacticConstituentType_LocalType (Dc1Factory::CreateObject(TypeOfSyntacticConstituentType_LocalType))
class Mp7JrsSyntacticConstituentType_LocalType;
class IMp7JrsSyntacticConstituentType_LocalType;
/** Smart pointer for instance of IMp7JrsSyntacticConstituentType_LocalType */
typedef Dc1Ptr< IMp7JrsSyntacticConstituentType_LocalType > Mp7JrsSyntacticConstituentType_LocalPtr;

#define TypeOfsynthesisType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:synthesisType")))
#define CreatesynthesisType (Dc1Factory::CreateObject(TypeOfsynthesisType))
class Mp7JrssynthesisType;
class IMp7JrssynthesisType;
// No smart pointer instance for IMp7JrssynthesisType

#define TypeOfTemporalCentroidType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalCentroidType")))
#define CreateTemporalCentroidType (Dc1Factory::CreateObject(TypeOfTemporalCentroidType))
class Mp7JrsTemporalCentroidType;
class IMp7JrsTemporalCentroidType;
/** Smart pointer for instance of IMp7JrsTemporalCentroidType */
typedef Dc1Ptr< IMp7JrsTemporalCentroidType > Mp7JrsTemporalCentroidPtr;

#define TypeOfTemporalInterpolationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType")))
#define CreateTemporalInterpolationType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType))
class Mp7JrsTemporalInterpolationType;
class IMp7JrsTemporalInterpolationType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType > Mp7JrsTemporalInterpolationPtr;

#define TypeOfTemporalInterpolationType_InterpolationFunctions_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_CollectionType")))
#define CreateTemporalInterpolationType_InterpolationFunctions_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_InterpolationFunctions_CollectionType))
class Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionType;
class IMp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionType > Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr;

#define TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType")))
#define CreateTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType))
class Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType;
class IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType > Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionPtr;

#define TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType")))
#define CreateTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType))
class Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType;
class IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType > Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalPtr;

#define TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType")))
#define CreateTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType))
class Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType;
class IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType;
// No smart pointer instance for IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType

#define TypeOfTemporalInterpolationType_InterpolationFunctions_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_LocalType")))
#define CreateTemporalInterpolationType_InterpolationFunctions_LocalType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_InterpolationFunctions_LocalType))
class Mp7JrsTemporalInterpolationType_InterpolationFunctions_LocalType;
class IMp7JrsTemporalInterpolationType_InterpolationFunctions_LocalType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_InterpolationFunctions_LocalType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_InterpolationFunctions_LocalType > Mp7JrsTemporalInterpolationType_InterpolationFunctions_LocalPtr;

#define TypeOfTemporalInterpolationType_KeyTimePoint_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_LocalType")))
#define CreateTemporalInterpolationType_KeyTimePoint_LocalType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_KeyTimePoint_LocalType))
class Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType;
class IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType > Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr;

#define TypeOfTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType")))
#define CreateTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType))
class Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType;
class IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType > Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr;

#define TypeOfTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType")))
#define CreateTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType))
class Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType;
class IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType > Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr;

#define TypeOfTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType")))
#define CreateTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType))
class Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType;
class IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType > Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr;

#define TypeOfTemporalInterpolationType_WholeInterval_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_WholeInterval_LocalType")))
#define CreateTemporalInterpolationType_WholeInterval_LocalType (Dc1Factory::CreateObject(TypeOfTemporalInterpolationType_WholeInterval_LocalType))
class Mp7JrsTemporalInterpolationType_WholeInterval_LocalType;
class IMp7JrsTemporalInterpolationType_WholeInterval_LocalType;
/** Smart pointer for instance of IMp7JrsTemporalInterpolationType_WholeInterval_LocalType */
typedef Dc1Ptr< IMp7JrsTemporalInterpolationType_WholeInterval_LocalType > Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr;

#define TypeOfTemporalMaskType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalMaskType")))
#define CreateTemporalMaskType (Dc1Factory::CreateObject(TypeOfTemporalMaskType))
class Mp7JrsTemporalMaskType;
class IMp7JrsTemporalMaskType;
/** Smart pointer for instance of IMp7JrsTemporalMaskType */
typedef Dc1Ptr< IMp7JrsTemporalMaskType > Mp7JrsTemporalMaskPtr;

#define TypeOfTemporalMaskType_SubInterval_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalMaskType_SubInterval_CollectionType")))
#define CreateTemporalMaskType_SubInterval_CollectionType (Dc1Factory::CreateObject(TypeOfTemporalMaskType_SubInterval_CollectionType))
class Mp7JrsTemporalMaskType_SubInterval_CollectionType;
class IMp7JrsTemporalMaskType_SubInterval_CollectionType;
/** Smart pointer for instance of IMp7JrsTemporalMaskType_SubInterval_CollectionType */
typedef Dc1Ptr< IMp7JrsTemporalMaskType_SubInterval_CollectionType > Mp7JrsTemporalMaskType_SubInterval_CollectionPtr;

#define TypeOfTemporalSegmentDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentDecompositionType")))
#define CreateTemporalSegmentDecompositionType (Dc1Factory::CreateObject(TypeOfTemporalSegmentDecompositionType))
class Mp7JrsTemporalSegmentDecompositionType;
class IMp7JrsTemporalSegmentDecompositionType;
/** Smart pointer for instance of IMp7JrsTemporalSegmentDecompositionType */
typedef Dc1Ptr< IMp7JrsTemporalSegmentDecompositionType > Mp7JrsTemporalSegmentDecompositionPtr;

#define TypeOfTemporalSegmentLocatorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))
#define CreateTemporalSegmentLocatorType (Dc1Factory::CreateObject(TypeOfTemporalSegmentLocatorType))
class Mp7JrsTemporalSegmentLocatorType;
class IMp7JrsTemporalSegmentLocatorType;
/** Smart pointer for instance of IMp7JrsTemporalSegmentLocatorType */
typedef Dc1Ptr< IMp7JrsTemporalSegmentLocatorType > Mp7JrsTemporalSegmentLocatorPtr;

#define TypeOfTemporalSegmentLocatorType_BytePosition_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType_BytePosition_LocalType")))
#define CreateTemporalSegmentLocatorType_BytePosition_LocalType (Dc1Factory::CreateObject(TypeOfTemporalSegmentLocatorType_BytePosition_LocalType))
class Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalType;
class IMp7JrsTemporalSegmentLocatorType_BytePosition_LocalType;
/** Smart pointer for instance of IMp7JrsTemporalSegmentLocatorType_BytePosition_LocalType */
typedef Dc1Ptr< IMp7JrsTemporalSegmentLocatorType_BytePosition_LocalType > Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr;

#define TypeOftermAliasReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termAliasReferenceType")))
#define CreatetermAliasReferenceType (Dc1Factory::CreateObject(TypeOftermAliasReferenceType))
class Mp7JrstermAliasReferenceType;
class IMp7JrstermAliasReferenceType;
/** Smart pointer for instance of IMp7JrstermAliasReferenceType */
typedef Dc1Ptr< IMp7JrstermAliasReferenceType > Mp7JrstermAliasReferencePtr;

#define TypeOfTermDefinitionBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionBaseType")))
#define CreateTermDefinitionBaseType (Dc1Factory::CreateObject(TypeOfTermDefinitionBaseType))
class Mp7JrsTermDefinitionBaseType;
class IMp7JrsTermDefinitionBaseType;
/** Smart pointer for instance of IMp7JrsTermDefinitionBaseType */
typedef Dc1Ptr< IMp7JrsTermDefinitionBaseType > Mp7JrsTermDefinitionBasePtr;

#define TypeOfTermDefinitionBaseType_Definition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionBaseType_Definition_CollectionType")))
#define CreateTermDefinitionBaseType_Definition_CollectionType (Dc1Factory::CreateObject(TypeOfTermDefinitionBaseType_Definition_CollectionType))
class Mp7JrsTermDefinitionBaseType_Definition_CollectionType;
class IMp7JrsTermDefinitionBaseType_Definition_CollectionType;
/** Smart pointer for instance of IMp7JrsTermDefinitionBaseType_Definition_CollectionType */
typedef Dc1Ptr< IMp7JrsTermDefinitionBaseType_Definition_CollectionType > Mp7JrsTermDefinitionBaseType_Definition_CollectionPtr;

#define TypeOfTermDefinitionBaseType_Name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionBaseType_Name_CollectionType")))
#define CreateTermDefinitionBaseType_Name_CollectionType (Dc1Factory::CreateObject(TypeOfTermDefinitionBaseType_Name_CollectionType))
class Mp7JrsTermDefinitionBaseType_Name_CollectionType;
class IMp7JrsTermDefinitionBaseType_Name_CollectionType;
/** Smart pointer for instance of IMp7JrsTermDefinitionBaseType_Name_CollectionType */
typedef Dc1Ptr< IMp7JrsTermDefinitionBaseType_Name_CollectionType > Mp7JrsTermDefinitionBaseType_Name_CollectionPtr;

#define TypeOfTermDefinitionBaseType_Name_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionBaseType_Name_LocalType")))
#define CreateTermDefinitionBaseType_Name_LocalType (Dc1Factory::CreateObject(TypeOfTermDefinitionBaseType_Name_LocalType))
class Mp7JrsTermDefinitionBaseType_Name_LocalType;
class IMp7JrsTermDefinitionBaseType_Name_LocalType;
/** Smart pointer for instance of IMp7JrsTermDefinitionBaseType_Name_LocalType */
typedef Dc1Ptr< IMp7JrsTermDefinitionBaseType_Name_LocalType > Mp7JrsTermDefinitionBaseType_Name_LocalPtr;

#define TypeOfTermDefinitionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionType")))
#define CreateTermDefinitionType (Dc1Factory::CreateObject(TypeOfTermDefinitionType))
class Mp7JrsTermDefinitionType;
class IMp7JrsTermDefinitionType;
/** Smart pointer for instance of IMp7JrsTermDefinitionType */
typedef Dc1Ptr< IMp7JrsTermDefinitionType > Mp7JrsTermDefinitionPtr;

#define TypeOfTermDefinitionType_Term_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionType_Term_CollectionType")))
#define CreateTermDefinitionType_Term_CollectionType (Dc1Factory::CreateObject(TypeOfTermDefinitionType_Term_CollectionType))
class Mp7JrsTermDefinitionType_Term_CollectionType;
class IMp7JrsTermDefinitionType_Term_CollectionType;
/** Smart pointer for instance of IMp7JrsTermDefinitionType_Term_CollectionType */
typedef Dc1Ptr< IMp7JrsTermDefinitionType_Term_CollectionType > Mp7JrsTermDefinitionType_Term_CollectionPtr;

#define TypeOfTermDefinitionType_Term_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermDefinitionType_Term_LocalType")))
#define CreateTermDefinitionType_Term_LocalType (Dc1Factory::CreateObject(TypeOfTermDefinitionType_Term_LocalType))
class Mp7JrsTermDefinitionType_Term_LocalType;
class IMp7JrsTermDefinitionType_Term_LocalType;
/** Smart pointer for instance of IMp7JrsTermDefinitionType_Term_LocalType */
typedef Dc1Ptr< IMp7JrsTermDefinitionType_Term_LocalType > Mp7JrsTermDefinitionType_Term_LocalPtr;

#define TypeOftermNoteQualifierType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termNoteQualifierType")))
#define CreatetermNoteQualifierType (Dc1Factory::CreateObject(TypeOftermNoteQualifierType))
class Mp7JrstermNoteQualifierType;
class IMp7JrstermNoteQualifierType;
/** Smart pointer for instance of IMp7JrstermNoteQualifierType */
typedef Dc1Ptr< IMp7JrstermNoteQualifierType > Mp7JrstermNoteQualifierPtr;

#define TypeOftermNoteQualifierType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termNoteQualifierType_LocalType")))
#define CreatetermNoteQualifierType_LocalType (Dc1Factory::CreateObject(TypeOftermNoteQualifierType_LocalType))
class Mp7JrstermNoteQualifierType_LocalType;
class IMp7JrstermNoteQualifierType_LocalType;
// No smart pointer instance for IMp7JrstermNoteQualifierType_LocalType

#define TypeOftermNoteQualifierType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termNoteQualifierType_LocalType0")))
#define CreatetermNoteQualifierType_LocalType0 (Dc1Factory::CreateObject(TypeOftermNoteQualifierType_LocalType0))
class Mp7JrstermNoteQualifierType_LocalType0;
class IMp7JrstermNoteQualifierType_LocalType0;
/** Smart pointer for instance of IMp7JrstermNoteQualifierType_LocalType0 */
typedef Dc1Ptr< IMp7JrstermNoteQualifierType_LocalType0 > Mp7JrstermNoteQualifierType_Local0Ptr;

#define TypeOftermNoteQualifierType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termNoteQualifierType_LocalType1")))
#define CreatetermNoteQualifierType_LocalType1 (Dc1Factory::CreateObject(TypeOftermNoteQualifierType_LocalType1))
class Mp7JrstermNoteQualifierType_LocalType1;
class IMp7JrstermNoteQualifierType_LocalType1;
/** Smart pointer for instance of IMp7JrstermNoteQualifierType_LocalType1 */
typedef Dc1Ptr< IMp7JrstermNoteQualifierType_LocalType1 > Mp7JrstermNoteQualifierType_Local1Ptr;

#define TypeOftermReferenceListType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termReferenceListType")))
#define CreatetermReferenceListType (Dc1Factory::CreateObject(TypeOftermReferenceListType))
class Mp7JrstermReferenceListType;
class IMp7JrstermReferenceListType;
/** Smart pointer for instance of IMp7JrstermReferenceListType */
typedef Dc1Ptr< IMp7JrstermReferenceListType > Mp7JrstermReferenceListPtr;

#define TypeOftermReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termReferenceType")))
#define CreatetermReferenceType (Dc1Factory::CreateObject(TypeOftermReferenceType))
class Mp7JrstermReferenceType;
class IMp7JrstermReferenceType;
/** Smart pointer for instance of IMp7JrstermReferenceType */
typedef Dc1Ptr< IMp7JrstermReferenceType > Mp7JrstermReferencePtr;

#define TypeOftermRelationQualifierType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termRelationQualifierType")))
#define CreatetermRelationQualifierType (Dc1Factory::CreateObject(TypeOftermRelationQualifierType))
class Mp7JrstermRelationQualifierType;
class IMp7JrstermRelationQualifierType;
/** Smart pointer for instance of IMp7JrstermRelationQualifierType */
typedef Dc1Ptr< IMp7JrstermRelationQualifierType > Mp7JrstermRelationQualifierPtr;

#define TypeOftermRelationQualifierType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termRelationQualifierType_LocalType")))
#define CreatetermRelationQualifierType_LocalType (Dc1Factory::CreateObject(TypeOftermRelationQualifierType_LocalType))
class Mp7JrstermRelationQualifierType_LocalType;
class IMp7JrstermRelationQualifierType_LocalType;
// No smart pointer instance for IMp7JrstermRelationQualifierType_LocalType

#define TypeOftermRelationQualifierType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termRelationQualifierType_LocalType0")))
#define CreatetermRelationQualifierType_LocalType0 (Dc1Factory::CreateObject(TypeOftermRelationQualifierType_LocalType0))
class Mp7JrstermRelationQualifierType_LocalType0;
class IMp7JrstermRelationQualifierType_LocalType0;
/** Smart pointer for instance of IMp7JrstermRelationQualifierType_LocalType0 */
typedef Dc1Ptr< IMp7JrstermRelationQualifierType_LocalType0 > Mp7JrstermRelationQualifierType_Local0Ptr;

#define TypeOftermRelationQualifierType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termRelationQualifierType_LocalType1")))
#define CreatetermRelationQualifierType_LocalType1 (Dc1Factory::CreateObject(TypeOftermRelationQualifierType_LocalType1))
class Mp7JrstermRelationQualifierType_LocalType1;
class IMp7JrstermRelationQualifierType_LocalType1;
/** Smart pointer for instance of IMp7JrstermRelationQualifierType_LocalType1 */
typedef Dc1Ptr< IMp7JrstermRelationQualifierType_LocalType1 > Mp7JrstermRelationQualifierType_Local1Ptr;

#define TypeOftermURIReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termURIReferenceType")))
#define CreatetermURIReferenceType (Dc1Factory::CreateObject(TypeOftermURIReferenceType))
class Mp7JrstermURIReferenceType;
class IMp7JrstermURIReferenceType;
/** Smart pointer for instance of IMp7JrstermURIReferenceType */
typedef Dc1Ptr< IMp7JrstermURIReferenceType > Mp7JrstermURIReferencePtr;

#define TypeOfTermUseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))
#define CreateTermUseType (Dc1Factory::CreateObject(TypeOfTermUseType))
class Mp7JrsTermUseType;
class IMp7JrsTermUseType;
/** Smart pointer for instance of IMp7JrsTermUseType */
typedef Dc1Ptr< IMp7JrsTermUseType > Mp7JrsTermUsePtr;

#define TypeOfTextAnnotationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))
#define CreateTextAnnotationType (Dc1Factory::CreateObject(TypeOfTextAnnotationType))
class Mp7JrsTextAnnotationType;
class IMp7JrsTextAnnotationType;
/** Smart pointer for instance of IMp7JrsTextAnnotationType */
typedef Dc1Ptr< IMp7JrsTextAnnotationType > Mp7JrsTextAnnotationPtr;

#define TypeOfTextAnnotationType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType_CollectionType")))
#define CreateTextAnnotationType_CollectionType (Dc1Factory::CreateObject(TypeOfTextAnnotationType_CollectionType))
class Mp7JrsTextAnnotationType_CollectionType;
class IMp7JrsTextAnnotationType_CollectionType;
/** Smart pointer for instance of IMp7JrsTextAnnotationType_CollectionType */
typedef Dc1Ptr< IMp7JrsTextAnnotationType_CollectionType > Mp7JrsTextAnnotationType_CollectionPtr;

#define TypeOfTextAnnotationType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType_LocalType")))
#define CreateTextAnnotationType_LocalType (Dc1Factory::CreateObject(TypeOfTextAnnotationType_LocalType))
class Mp7JrsTextAnnotationType_LocalType;
class IMp7JrsTextAnnotationType_LocalType;
/** Smart pointer for instance of IMp7JrsTextAnnotationType_LocalType */
typedef Dc1Ptr< IMp7JrsTextAnnotationType_LocalType > Mp7JrsTextAnnotationType_LocalPtr;

#define TypeOfTextDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextDecompositionType")))
#define CreateTextDecompositionType (Dc1Factory::CreateObject(TypeOfTextDecompositionType))
class Mp7JrsTextDecompositionType;
class IMp7JrsTextDecompositionType;
/** Smart pointer for instance of IMp7JrsTextDecompositionType */
typedef Dc1Ptr< IMp7JrsTextDecompositionType > Mp7JrsTextDecompositionPtr;

#define TypeOfTextDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextDecompositionType_CollectionType")))
#define CreateTextDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfTextDecompositionType_CollectionType))
class Mp7JrsTextDecompositionType_CollectionType;
class IMp7JrsTextDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsTextDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsTextDecompositionType_CollectionType > Mp7JrsTextDecompositionType_CollectionPtr;

#define TypeOfTextDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextDecompositionType_LocalType")))
#define CreateTextDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfTextDecompositionType_LocalType))
class Mp7JrsTextDecompositionType_LocalType;
class IMp7JrsTextDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsTextDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsTextDecompositionType_LocalType > Mp7JrsTextDecompositionType_LocalPtr;

#define TypeOfTextSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextSegmentType")))
#define CreateTextSegmentType (Dc1Factory::CreateObject(TypeOfTextSegmentType))
class Mp7JrsTextSegmentType;
class IMp7JrsTextSegmentType;
/** Smart pointer for instance of IMp7JrsTextSegmentType */
typedef Dc1Ptr< IMp7JrsTextSegmentType > Mp7JrsTextSegmentPtr;

#define TypeOfTextSegmentType_TextDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextSegmentType_TextDecomposition_CollectionType")))
#define CreateTextSegmentType_TextDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfTextSegmentType_TextDecomposition_CollectionType))
class Mp7JrsTextSegmentType_TextDecomposition_CollectionType;
class IMp7JrsTextSegmentType_TextDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsTextSegmentType_TextDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsTextSegmentType_TextDecomposition_CollectionType > Mp7JrsTextSegmentType_TextDecomposition_CollectionPtr;

#define TypeOfTextType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextType")))
#define CreateTextType (Dc1Factory::CreateObject(TypeOfTextType))
class Mp7JrsTextType;
class IMp7JrsTextType;
/** Smart pointer for instance of IMp7JrsTextType */
typedef Dc1Ptr< IMp7JrsTextType > Mp7JrsTextPtr;

#define TypeOfTextualBaseType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualBaseType")))
#define CreateTextualBaseType (Dc1Factory::CreateObject(TypeOfTextualBaseType))
class Mp7JrsTextualBaseType;
class IMp7JrsTextualBaseType;
/** Smart pointer for instance of IMp7JrsTextualBaseType */
typedef Dc1Ptr< IMp7JrsTextualBaseType > Mp7JrsTextualBasePtr;

#define TypeOfTextualBaseType_phoneticTranscription_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualBaseType_phoneticTranscription_CollectionType")))
#define CreateTextualBaseType_phoneticTranscription_CollectionType (Dc1Factory::CreateObject(TypeOfTextualBaseType_phoneticTranscription_CollectionType))
class Mp7JrsTextualBaseType_phoneticTranscription_CollectionType;
class IMp7JrsTextualBaseType_phoneticTranscription_CollectionType;
/** Smart pointer for instance of IMp7JrsTextualBaseType_phoneticTranscription_CollectionType */
typedef Dc1Ptr< IMp7JrsTextualBaseType_phoneticTranscription_CollectionType > Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr;

#define TypeOfTextualSummaryComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualSummaryComponentType")))
#define CreateTextualSummaryComponentType (Dc1Factory::CreateObject(TypeOfTextualSummaryComponentType))
class Mp7JrsTextualSummaryComponentType;
class IMp7JrsTextualSummaryComponentType;
/** Smart pointer for instance of IMp7JrsTextualSummaryComponentType */
typedef Dc1Ptr< IMp7JrsTextualSummaryComponentType > Mp7JrsTextualSummaryComponentPtr;

#define TypeOfTextualSummaryComponentType_FreeText_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualSummaryComponentType_FreeText_CollectionType")))
#define CreateTextualSummaryComponentType_FreeText_CollectionType (Dc1Factory::CreateObject(TypeOfTextualSummaryComponentType_FreeText_CollectionType))
class Mp7JrsTextualSummaryComponentType_FreeText_CollectionType;
class IMp7JrsTextualSummaryComponentType_FreeText_CollectionType;
/** Smart pointer for instance of IMp7JrsTextualSummaryComponentType_FreeText_CollectionType */
typedef Dc1Ptr< IMp7JrsTextualSummaryComponentType_FreeText_CollectionType > Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr;

#define TypeOfTextualType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))
#define CreateTextualType (Dc1Factory::CreateObject(TypeOfTextualType))
class Mp7JrsTextualType;
class IMp7JrsTextualType;
/** Smart pointer for instance of IMp7JrsTextualType */
typedef Dc1Ptr< IMp7JrsTextualType > Mp7JrsTextualPtr;

#define TypeOfTextureBrowsingType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType")))
#define CreateTextureBrowsingType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType))
class Mp7JrsTextureBrowsingType;
class IMp7JrsTextureBrowsingType;
/** Smart pointer for instance of IMp7JrsTextureBrowsingType */
typedef Dc1Ptr< IMp7JrsTextureBrowsingType > Mp7JrsTextureBrowsingPtr;

#define TypeOfTextureBrowsingType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType_CollectionType")))
#define CreateTextureBrowsingType_CollectionType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType_CollectionType))
class Mp7JrsTextureBrowsingType_CollectionType;
class IMp7JrsTextureBrowsingType_CollectionType;
/** Smart pointer for instance of IMp7JrsTextureBrowsingType_CollectionType */
typedef Dc1Ptr< IMp7JrsTextureBrowsingType_CollectionType > Mp7JrsTextureBrowsingType_CollectionPtr;

#define TypeOfTextureBrowsingType_Direction_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType_Direction_LocalType")))
#define CreateTextureBrowsingType_Direction_LocalType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType_Direction_LocalType))
class Mp7JrsTextureBrowsingType_Direction_LocalType;
class IMp7JrsTextureBrowsingType_Direction_LocalType;
// No smart pointer instance for IMp7JrsTextureBrowsingType_Direction_LocalType

#define TypeOfTextureBrowsingType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType_LocalType")))
#define CreateTextureBrowsingType_LocalType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType_LocalType))
class Mp7JrsTextureBrowsingType_LocalType;
class IMp7JrsTextureBrowsingType_LocalType;
/** Smart pointer for instance of IMp7JrsTextureBrowsingType_LocalType */
typedef Dc1Ptr< IMp7JrsTextureBrowsingType_LocalType > Mp7JrsTextureBrowsingType_LocalPtr;

#define TypeOfTextureBrowsingType_Regularity_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType_Regularity_LocalType")))
#define CreateTextureBrowsingType_Regularity_LocalType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType_Regularity_LocalType))
class Mp7JrsTextureBrowsingType_Regularity_LocalType;
class IMp7JrsTextureBrowsingType_Regularity_LocalType;
// No smart pointer instance for IMp7JrsTextureBrowsingType_Regularity_LocalType

#define TypeOfTextureBrowsingType_Scale_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType_Scale_LocalType")))
#define CreateTextureBrowsingType_Scale_LocalType (Dc1Factory::CreateObject(TypeOfTextureBrowsingType_Scale_LocalType))
class Mp7JrsTextureBrowsingType_Scale_LocalType;
class IMp7JrsTextureBrowsingType_Scale_LocalType;
// No smart pointer instance for IMp7JrsTextureBrowsingType_Scale_LocalType

#define TypeOftextureListType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:textureListType")))
#define CreatetextureListType (Dc1Factory::CreateObject(TypeOftextureListType))
class Mp7JrstextureListType;
class IMp7JrstextureListType;
/** Smart pointer for instance of IMp7JrstextureListType */
typedef Dc1Ptr< IMp7JrstextureListType > Mp7JrstextureListPtr;

#define TypeOftextureListType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:textureListType_CollectionType")))
#define CreatetextureListType_CollectionType (Dc1Factory::CreateObject(TypeOftextureListType_CollectionType))
class Mp7JrstextureListType_CollectionType;
class IMp7JrstextureListType_CollectionType;
/** Smart pointer for instance of IMp7JrstextureListType_CollectionType */
typedef Dc1Ptr< IMp7JrstextureListType_CollectionType > Mp7JrstextureListType_CollectionPtr;

#define TypeOftimeOffsetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timeOffsetType")))
#define CreatetimeOffsetType (Dc1Factory::CreateObject(TypeOftimeOffsetType))
class Mp7JrstimeOffsetType;
class IMp7JrstimeOffsetType;
/** Smart pointer for instance of IMp7JrstimeOffsetType */
typedef Dc1Ptr< IMp7JrstimeOffsetType > Mp7JrstimeOffsetPtr;

#define TypeOftimePointType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))
#define CreatetimePointType (Dc1Factory::CreateObject(TypeOftimePointType))
class Mp7JrstimePointType;
class IMp7JrstimePointType;
/** Smart pointer for instance of IMp7JrstimePointType */
typedef Dc1Ptr< IMp7JrstimePointType > Mp7JrstimePointPtr;

#define TypeOfTimeType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))
#define CreateTimeType (Dc1Factory::CreateObject(TypeOfTimeType))
class Mp7JrsTimeType;
class IMp7JrsTimeType;
/** Smart pointer for instance of IMp7JrsTimeType */
typedef Dc1Ptr< IMp7JrsTimeType > Mp7JrsTimePtr;

#define TypeOfTimeZoneType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeZoneType")))
#define CreateTimeZoneType (Dc1Factory::CreateObject(TypeOfTimeZoneType))
class Mp7JrsTimeZoneType;
class IMp7JrsTimeZoneType;
/** Smart pointer for instance of IMp7JrsTimeZoneType */
typedef Dc1Ptr< IMp7JrsTimeZoneType > Mp7JrsTimeZonePtr;

#define TypeOfTimeZoneType_value_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeZoneType_value_LocalType")))
#define CreateTimeZoneType_value_LocalType (Dc1Factory::CreateObject(TypeOfTimeZoneType_value_LocalType))
class Mp7JrsTimeZoneType_value_LocalType;
class IMp7JrsTimeZoneType_value_LocalType;
/** Smart pointer for instance of IMp7JrsTimeZoneType_value_LocalType */
typedef Dc1Ptr< IMp7JrsTimeZoneType_value_LocalType > Mp7JrsTimeZoneType_value_LocalPtr;

#define TypeOfTitleMediaType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleMediaType")))
#define CreateTitleMediaType (Dc1Factory::CreateObject(TypeOfTitleMediaType))
class Mp7JrsTitleMediaType;
class IMp7JrsTitleMediaType;
/** Smart pointer for instance of IMp7JrsTitleMediaType */
typedef Dc1Ptr< IMp7JrsTitleMediaType > Mp7JrsTitleMediaPtr;

#define TypeOfTitleType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType")))
#define CreateTitleType (Dc1Factory::CreateObject(TypeOfTitleType))
class Mp7JrsTitleType;
class IMp7JrsTitleType;
/** Smart pointer for instance of IMp7JrsTitleType */
typedef Dc1Ptr< IMp7JrsTitleType > Mp7JrsTitlePtr;

#define TypeOfTitleType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType_type_LocalType")))
#define CreateTitleType_type_LocalType (Dc1Factory::CreateObject(TypeOfTitleType_type_LocalType))
class Mp7JrsTitleType_type_LocalType;
class IMp7JrsTitleType_type_LocalType;
// No smart pointer instance for IMp7JrsTitleType_type_LocalType

#define TypeOfTitleType_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType_type_LocalType0")))
#define CreateTitleType_type_LocalType0 (Dc1Factory::CreateObject(TypeOfTitleType_type_LocalType0))
class Mp7JrsTitleType_type_LocalType0;
class IMp7JrsTitleType_type_LocalType0;
/** Smart pointer for instance of IMp7JrsTitleType_type_LocalType0 */
typedef Dc1Ptr< IMp7JrsTitleType_type_LocalType0 > Mp7JrsTitleType_type_Local0Ptr;

#define TypeOfTitleType_type_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType_type_LocalType1")))
#define CreateTitleType_type_LocalType1 (Dc1Factory::CreateObject(TypeOfTitleType_type_LocalType1))
class Mp7JrsTitleType_type_LocalType1;
class IMp7JrsTitleType_type_LocalType1;
/** Smart pointer for instance of IMp7JrsTitleType_type_LocalType1 */
typedef Dc1Ptr< IMp7JrsTitleType_type_LocalType1 > Mp7JrsTitleType_type_Local1Ptr;

#define TypeOfTitleType_type_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType_type_LocalType2")))
#define CreateTitleType_type_LocalType2 (Dc1Factory::CreateObject(TypeOfTitleType_type_LocalType2))
class Mp7JrsTitleType_type_LocalType2;
class IMp7JrsTitleType_type_LocalType2;
/** Smart pointer for instance of IMp7JrsTitleType_type_LocalType2 */
typedef Dc1Ptr< IMp7JrsTitleType_type_LocalType2 > Mp7JrsTitleType_type_Local2Ptr;

#define TypeOfTransmissionTechnologyType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TransmissionTechnologyType")))
#define CreateTransmissionTechnologyType (Dc1Factory::CreateObject(TypeOfTransmissionTechnologyType))
class Mp7JrsTransmissionTechnologyType;
class IMp7JrsTransmissionTechnologyType;
/** Smart pointer for instance of IMp7JrsTransmissionTechnologyType */
typedef Dc1Ptr< IMp7JrsTransmissionTechnologyType > Mp7JrsTransmissionTechnologyPtr;

#define TypeOfUniqueIDType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))
#define CreateUniqueIDType (Dc1Factory::CreateObject(TypeOfUniqueIDType))
class Mp7JrsUniqueIDType;
class IMp7JrsUniqueIDType;
/** Smart pointer for instance of IMp7JrsUniqueIDType */
typedef Dc1Ptr< IMp7JrsUniqueIDType > Mp7JrsUniqueIDPtr;

#define TypeOfUniqueIDType_encoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType_encoding_LocalType")))
#define CreateUniqueIDType_encoding_LocalType (Dc1Factory::CreateObject(TypeOfUniqueIDType_encoding_LocalType))
class Mp7JrsUniqueIDType_encoding_LocalType;
class IMp7JrsUniqueIDType_encoding_LocalType;
// No smart pointer instance for IMp7JrsUniqueIDType_encoding_LocalType

#define TypeOfunitType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unitType")))
#define CreateunitType (Dc1Factory::CreateObject(TypeOfunitType))
class Mp7JrsunitType;
class IMp7JrsunitType;
// No smart pointer instance for IMp7JrsunitType

#define TypeOfunsigned1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned1")))
#define Createunsigned1 (Dc1Factory::CreateObject(TypeOfunsigned1))
class Mp7Jrsunsigned1;
class IMp7Jrsunsigned1;
/** Smart pointer for instance of IMp7Jrsunsigned1 */
typedef Dc1Ptr< IMp7Jrsunsigned1 > Mp7Jrsunsigned1Ptr;

#define TypeOfunsigned10 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned10")))
#define Createunsigned10 (Dc1Factory::CreateObject(TypeOfunsigned10))
class Mp7Jrsunsigned10;
class IMp7Jrsunsigned10;
/** Smart pointer for instance of IMp7Jrsunsigned10 */
typedef Dc1Ptr< IMp7Jrsunsigned10 > Mp7Jrsunsigned10Ptr;

#define TypeOfunsigned11 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))
#define Createunsigned11 (Dc1Factory::CreateObject(TypeOfunsigned11))
class Mp7Jrsunsigned11;
class IMp7Jrsunsigned11;
/** Smart pointer for instance of IMp7Jrsunsigned11 */
typedef Dc1Ptr< IMp7Jrsunsigned11 > Mp7Jrsunsigned11Ptr;

#define TypeOfunsigned12 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12")))
#define Createunsigned12 (Dc1Factory::CreateObject(TypeOfunsigned12))
class Mp7Jrsunsigned12;
class IMp7Jrsunsigned12;
/** Smart pointer for instance of IMp7Jrsunsigned12 */
typedef Dc1Ptr< IMp7Jrsunsigned12 > Mp7Jrsunsigned12Ptr;

#define TypeOfunsigned13 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned13")))
#define Createunsigned13 (Dc1Factory::CreateObject(TypeOfunsigned13))
class Mp7Jrsunsigned13;
class IMp7Jrsunsigned13;
/** Smart pointer for instance of IMp7Jrsunsigned13 */
typedef Dc1Ptr< IMp7Jrsunsigned13 > Mp7Jrsunsigned13Ptr;

#define TypeOfunsigned14 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned14")))
#define Createunsigned14 (Dc1Factory::CreateObject(TypeOfunsigned14))
class Mp7Jrsunsigned14;
class IMp7Jrsunsigned14;
/** Smart pointer for instance of IMp7Jrsunsigned14 */
typedef Dc1Ptr< IMp7Jrsunsigned14 > Mp7Jrsunsigned14Ptr;

#define TypeOfunsigned15 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))
#define Createunsigned15 (Dc1Factory::CreateObject(TypeOfunsigned15))
class Mp7Jrsunsigned15;
class IMp7Jrsunsigned15;
/** Smart pointer for instance of IMp7Jrsunsigned15 */
typedef Dc1Ptr< IMp7Jrsunsigned15 > Mp7Jrsunsigned15Ptr;

#define TypeOfunsigned16 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned16")))
#define Createunsigned16 (Dc1Factory::CreateObject(TypeOfunsigned16))
class Mp7Jrsunsigned16;
class IMp7Jrsunsigned16;
/** Smart pointer for instance of IMp7Jrsunsigned16 */
typedef Dc1Ptr< IMp7Jrsunsigned16 > Mp7Jrsunsigned16Ptr;

#define TypeOfunsigned17 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned17")))
#define Createunsigned17 (Dc1Factory::CreateObject(TypeOfunsigned17))
class Mp7Jrsunsigned17;
class IMp7Jrsunsigned17;
/** Smart pointer for instance of IMp7Jrsunsigned17 */
typedef Dc1Ptr< IMp7Jrsunsigned17 > Mp7Jrsunsigned17Ptr;

#define TypeOfunsigned18 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned18")))
#define Createunsigned18 (Dc1Factory::CreateObject(TypeOfunsigned18))
class Mp7Jrsunsigned18;
class IMp7Jrsunsigned18;
/** Smart pointer for instance of IMp7Jrsunsigned18 */
typedef Dc1Ptr< IMp7Jrsunsigned18 > Mp7Jrsunsigned18Ptr;

#define TypeOfunsigned19 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned19")))
#define Createunsigned19 (Dc1Factory::CreateObject(TypeOfunsigned19))
class Mp7Jrsunsigned19;
class IMp7Jrsunsigned19;
/** Smart pointer for instance of IMp7Jrsunsigned19 */
typedef Dc1Ptr< IMp7Jrsunsigned19 > Mp7Jrsunsigned19Ptr;

#define TypeOfunsigned2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned2")))
#define Createunsigned2 (Dc1Factory::CreateObject(TypeOfunsigned2))
class Mp7Jrsunsigned2;
class IMp7Jrsunsigned2;
/** Smart pointer for instance of IMp7Jrsunsigned2 */
typedef Dc1Ptr< IMp7Jrsunsigned2 > Mp7Jrsunsigned2Ptr;

#define TypeOfunsigned20 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned20")))
#define Createunsigned20 (Dc1Factory::CreateObject(TypeOfunsigned20))
class Mp7Jrsunsigned20;
class IMp7Jrsunsigned20;
/** Smart pointer for instance of IMp7Jrsunsigned20 */
typedef Dc1Ptr< IMp7Jrsunsigned20 > Mp7Jrsunsigned20Ptr;

#define TypeOfunsigned21 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned21")))
#define Createunsigned21 (Dc1Factory::CreateObject(TypeOfunsigned21))
class Mp7Jrsunsigned21;
class IMp7Jrsunsigned21;
/** Smart pointer for instance of IMp7Jrsunsigned21 */
typedef Dc1Ptr< IMp7Jrsunsigned21 > Mp7Jrsunsigned21Ptr;

#define TypeOfunsigned22 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned22")))
#define Createunsigned22 (Dc1Factory::CreateObject(TypeOfunsigned22))
class Mp7Jrsunsigned22;
class IMp7Jrsunsigned22;
/** Smart pointer for instance of IMp7Jrsunsigned22 */
typedef Dc1Ptr< IMp7Jrsunsigned22 > Mp7Jrsunsigned22Ptr;

#define TypeOfunsigned23 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned23")))
#define Createunsigned23 (Dc1Factory::CreateObject(TypeOfunsigned23))
class Mp7Jrsunsigned23;
class IMp7Jrsunsigned23;
/** Smart pointer for instance of IMp7Jrsunsigned23 */
typedef Dc1Ptr< IMp7Jrsunsigned23 > Mp7Jrsunsigned23Ptr;

#define TypeOfunsigned24 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned24")))
#define Createunsigned24 (Dc1Factory::CreateObject(TypeOfunsigned24))
class Mp7Jrsunsigned24;
class IMp7Jrsunsigned24;
/** Smart pointer for instance of IMp7Jrsunsigned24 */
typedef Dc1Ptr< IMp7Jrsunsigned24 > Mp7Jrsunsigned24Ptr;

#define TypeOfunsigned25 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned25")))
#define Createunsigned25 (Dc1Factory::CreateObject(TypeOfunsigned25))
class Mp7Jrsunsigned25;
class IMp7Jrsunsigned25;
/** Smart pointer for instance of IMp7Jrsunsigned25 */
typedef Dc1Ptr< IMp7Jrsunsigned25 > Mp7Jrsunsigned25Ptr;

#define TypeOfunsigned26 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned26")))
#define Createunsigned26 (Dc1Factory::CreateObject(TypeOfunsigned26))
class Mp7Jrsunsigned26;
class IMp7Jrsunsigned26;
/** Smart pointer for instance of IMp7Jrsunsigned26 */
typedef Dc1Ptr< IMp7Jrsunsigned26 > Mp7Jrsunsigned26Ptr;

#define TypeOfunsigned27 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned27")))
#define Createunsigned27 (Dc1Factory::CreateObject(TypeOfunsigned27))
class Mp7Jrsunsigned27;
class IMp7Jrsunsigned27;
/** Smart pointer for instance of IMp7Jrsunsigned27 */
typedef Dc1Ptr< IMp7Jrsunsigned27 > Mp7Jrsunsigned27Ptr;

#define TypeOfunsigned28 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned28")))
#define Createunsigned28 (Dc1Factory::CreateObject(TypeOfunsigned28))
class Mp7Jrsunsigned28;
class IMp7Jrsunsigned28;
/** Smart pointer for instance of IMp7Jrsunsigned28 */
typedef Dc1Ptr< IMp7Jrsunsigned28 > Mp7Jrsunsigned28Ptr;

#define TypeOfunsigned29 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned29")))
#define Createunsigned29 (Dc1Factory::CreateObject(TypeOfunsigned29))
class Mp7Jrsunsigned29;
class IMp7Jrsunsigned29;
/** Smart pointer for instance of IMp7Jrsunsigned29 */
typedef Dc1Ptr< IMp7Jrsunsigned29 > Mp7Jrsunsigned29Ptr;

#define TypeOfunsigned3 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned3")))
#define Createunsigned3 (Dc1Factory::CreateObject(TypeOfunsigned3))
class Mp7Jrsunsigned3;
class IMp7Jrsunsigned3;
/** Smart pointer for instance of IMp7Jrsunsigned3 */
typedef Dc1Ptr< IMp7Jrsunsigned3 > Mp7Jrsunsigned3Ptr;

#define TypeOfunsigned30 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned30")))
#define Createunsigned30 (Dc1Factory::CreateObject(TypeOfunsigned30))
class Mp7Jrsunsigned30;
class IMp7Jrsunsigned30;
/** Smart pointer for instance of IMp7Jrsunsigned30 */
typedef Dc1Ptr< IMp7Jrsunsigned30 > Mp7Jrsunsigned30Ptr;

#define TypeOfunsigned31 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned31")))
#define Createunsigned31 (Dc1Factory::CreateObject(TypeOfunsigned31))
class Mp7Jrsunsigned31;
class IMp7Jrsunsigned31;
/** Smart pointer for instance of IMp7Jrsunsigned31 */
typedef Dc1Ptr< IMp7Jrsunsigned31 > Mp7Jrsunsigned31Ptr;

#define TypeOfunsigned32 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))
#define Createunsigned32 (Dc1Factory::CreateObject(TypeOfunsigned32))
class Mp7Jrsunsigned32;
class IMp7Jrsunsigned32;
/** Smart pointer for instance of IMp7Jrsunsigned32 */
typedef Dc1Ptr< IMp7Jrsunsigned32 > Mp7Jrsunsigned32Ptr;

#define TypeOfunsigned4 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned4")))
#define Createunsigned4 (Dc1Factory::CreateObject(TypeOfunsigned4))
class Mp7Jrsunsigned4;
class IMp7Jrsunsigned4;
/** Smart pointer for instance of IMp7Jrsunsigned4 */
typedef Dc1Ptr< IMp7Jrsunsigned4 > Mp7Jrsunsigned4Ptr;

#define TypeOfunsigned5 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned5")))
#define Createunsigned5 (Dc1Factory::CreateObject(TypeOfunsigned5))
class Mp7Jrsunsigned5;
class IMp7Jrsunsigned5;
/** Smart pointer for instance of IMp7Jrsunsigned5 */
typedef Dc1Ptr< IMp7Jrsunsigned5 > Mp7Jrsunsigned5Ptr;

#define TypeOfunsigned6 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6")))
#define Createunsigned6 (Dc1Factory::CreateObject(TypeOfunsigned6))
class Mp7Jrsunsigned6;
class IMp7Jrsunsigned6;
/** Smart pointer for instance of IMp7Jrsunsigned6 */
typedef Dc1Ptr< IMp7Jrsunsigned6 > Mp7Jrsunsigned6Ptr;

#define TypeOfunsigned7 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))
#define Createunsigned7 (Dc1Factory::CreateObject(TypeOfunsigned7))
class Mp7Jrsunsigned7;
class IMp7Jrsunsigned7;
/** Smart pointer for instance of IMp7Jrsunsigned7 */
typedef Dc1Ptr< IMp7Jrsunsigned7 > Mp7Jrsunsigned7Ptr;

#define TypeOfunsigned8 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8")))
#define Createunsigned8 (Dc1Factory::CreateObject(TypeOfunsigned8))
class Mp7Jrsunsigned8;
class IMp7Jrsunsigned8;
/** Smart pointer for instance of IMp7Jrsunsigned8 */
typedef Dc1Ptr< IMp7Jrsunsigned8 > Mp7Jrsunsigned8Ptr;

#define TypeOfunsigned9 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned9")))
#define Createunsigned9 (Dc1Factory::CreateObject(TypeOfunsigned9))
class Mp7Jrsunsigned9;
class IMp7Jrsunsigned9;
/** Smart pointer for instance of IMp7Jrsunsigned9 */
typedef Dc1Ptr< IMp7Jrsunsigned9 > Mp7Jrsunsigned9Ptr;

#define TypeOfURIFragmentReferenceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:URIFragmentReferenceType")))
#define CreateURIFragmentReferenceType (Dc1Factory::CreateObject(TypeOfURIFragmentReferenceType))
class Mp7JrsURIFragmentReferenceType;
class IMp7JrsURIFragmentReferenceType;
/** Smart pointer for instance of IMp7JrsURIFragmentReferenceType */
typedef Dc1Ptr< IMp7JrsURIFragmentReferenceType > Mp7JrsURIFragmentReferencePtr;

#define TypeOfUsageDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageDescriptionType")))
#define CreateUsageDescriptionType (Dc1Factory::CreateObject(TypeOfUsageDescriptionType))
class Mp7JrsUsageDescriptionType;
class IMp7JrsUsageDescriptionType;
/** Smart pointer for instance of IMp7JrsUsageDescriptionType */
typedef Dc1Ptr< IMp7JrsUsageDescriptionType > Mp7JrsUsageDescriptionPtr;

#define TypeOfUsageDescriptionType_UsageInformation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageDescriptionType_UsageInformation_CollectionType")))
#define CreateUsageDescriptionType_UsageInformation_CollectionType (Dc1Factory::CreateObject(TypeOfUsageDescriptionType_UsageInformation_CollectionType))
class Mp7JrsUsageDescriptionType_UsageInformation_CollectionType;
class IMp7JrsUsageDescriptionType_UsageInformation_CollectionType;
/** Smart pointer for instance of IMp7JrsUsageDescriptionType_UsageInformation_CollectionType */
typedef Dc1Ptr< IMp7JrsUsageDescriptionType_UsageInformation_CollectionType > Mp7JrsUsageDescriptionType_UsageInformation_CollectionPtr;

#define TypeOfUsageHistoryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageHistoryType")))
#define CreateUsageHistoryType (Dc1Factory::CreateObject(TypeOfUsageHistoryType))
class Mp7JrsUsageHistoryType;
class IMp7JrsUsageHistoryType;
/** Smart pointer for instance of IMp7JrsUsageHistoryType */
typedef Dc1Ptr< IMp7JrsUsageHistoryType > Mp7JrsUsageHistoryPtr;

#define TypeOfUsageHistoryType_UserActionHistory_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageHistoryType_UserActionHistory_CollectionType")))
#define CreateUsageHistoryType_UserActionHistory_CollectionType (Dc1Factory::CreateObject(TypeOfUsageHistoryType_UserActionHistory_CollectionType))
class Mp7JrsUsageHistoryType_UserActionHistory_CollectionType;
class IMp7JrsUsageHistoryType_UserActionHistory_CollectionType;
/** Smart pointer for instance of IMp7JrsUsageHistoryType_UserActionHistory_CollectionType */
typedef Dc1Ptr< IMp7JrsUsageHistoryType_UserActionHistory_CollectionType > Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr;

#define TypeOfUsageInformationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageInformationType")))
#define CreateUsageInformationType (Dc1Factory::CreateObject(TypeOfUsageInformationType))
class Mp7JrsUsageInformationType;
class IMp7JrsUsageInformationType;
/** Smart pointer for instance of IMp7JrsUsageInformationType */
typedef Dc1Ptr< IMp7JrsUsageInformationType > Mp7JrsUsageInformationPtr;

#define TypeOfUsageInformationType_Availability_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageInformationType_Availability_CollectionType")))
#define CreateUsageInformationType_Availability_CollectionType (Dc1Factory::CreateObject(TypeOfUsageInformationType_Availability_CollectionType))
class Mp7JrsUsageInformationType_Availability_CollectionType;
class IMp7JrsUsageInformationType_Availability_CollectionType;
/** Smart pointer for instance of IMp7JrsUsageInformationType_Availability_CollectionType */
typedef Dc1Ptr< IMp7JrsUsageInformationType_Availability_CollectionType > Mp7JrsUsageInformationType_Availability_CollectionPtr;

#define TypeOfUsageInformationType_UsageRecord_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageInformationType_UsageRecord_CollectionType")))
#define CreateUsageInformationType_UsageRecord_CollectionType (Dc1Factory::CreateObject(TypeOfUsageInformationType_UsageRecord_CollectionType))
class Mp7JrsUsageInformationType_UsageRecord_CollectionType;
class IMp7JrsUsageInformationType_UsageRecord_CollectionType;
/** Smart pointer for instance of IMp7JrsUsageInformationType_UsageRecord_CollectionType */
typedef Dc1Ptr< IMp7JrsUsageInformationType_UsageRecord_CollectionType > Mp7JrsUsageInformationType_UsageRecord_CollectionPtr;

#define TypeOfUsageRecordType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageRecordType")))
#define CreateUsageRecordType (Dc1Factory::CreateObject(TypeOfUsageRecordType))
class Mp7JrsUsageRecordType;
class IMp7JrsUsageRecordType;
/** Smart pointer for instance of IMp7JrsUsageRecordType */
typedef Dc1Ptr< IMp7JrsUsageRecordType > Mp7JrsUsageRecordPtr;

#define TypeOfUserActionHistoryType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionHistoryType")))
#define CreateUserActionHistoryType (Dc1Factory::CreateObject(TypeOfUserActionHistoryType))
class Mp7JrsUserActionHistoryType;
class IMp7JrsUserActionHistoryType;
/** Smart pointer for instance of IMp7JrsUserActionHistoryType */
typedef Dc1Ptr< IMp7JrsUserActionHistoryType > Mp7JrsUserActionHistoryPtr;

#define TypeOfUserActionHistoryType_ObservationPeriod_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionHistoryType_ObservationPeriod_CollectionType")))
#define CreateUserActionHistoryType_ObservationPeriod_CollectionType (Dc1Factory::CreateObject(TypeOfUserActionHistoryType_ObservationPeriod_CollectionType))
class Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionType;
class IMp7JrsUserActionHistoryType_ObservationPeriod_CollectionType;
/** Smart pointer for instance of IMp7JrsUserActionHistoryType_ObservationPeriod_CollectionType */
typedef Dc1Ptr< IMp7JrsUserActionHistoryType_ObservationPeriod_CollectionType > Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr;

#define TypeOfUserActionHistoryType_UserActionList_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionHistoryType_UserActionList_CollectionType")))
#define CreateUserActionHistoryType_UserActionList_CollectionType (Dc1Factory::CreateObject(TypeOfUserActionHistoryType_UserActionList_CollectionType))
class Mp7JrsUserActionHistoryType_UserActionList_CollectionType;
class IMp7JrsUserActionHistoryType_UserActionList_CollectionType;
/** Smart pointer for instance of IMp7JrsUserActionHistoryType_UserActionList_CollectionType */
typedef Dc1Ptr< IMp7JrsUserActionHistoryType_UserActionList_CollectionType > Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr;

#define TypeOfUserActionListType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionListType")))
#define CreateUserActionListType (Dc1Factory::CreateObject(TypeOfUserActionListType))
class Mp7JrsUserActionListType;
class IMp7JrsUserActionListType;
/** Smart pointer for instance of IMp7JrsUserActionListType */
typedef Dc1Ptr< IMp7JrsUserActionListType > Mp7JrsUserActionListPtr;

#define TypeOfUserActionListType_UserAction_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionListType_UserAction_CollectionType")))
#define CreateUserActionListType_UserAction_CollectionType (Dc1Factory::CreateObject(TypeOfUserActionListType_UserAction_CollectionType))
class Mp7JrsUserActionListType_UserAction_CollectionType;
class IMp7JrsUserActionListType_UserAction_CollectionType;
/** Smart pointer for instance of IMp7JrsUserActionListType_UserAction_CollectionType */
typedef Dc1Ptr< IMp7JrsUserActionListType_UserAction_CollectionType > Mp7JrsUserActionListType_UserAction_CollectionPtr;

#define TypeOfUserActionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionType")))
#define CreateUserActionType (Dc1Factory::CreateObject(TypeOfUserActionType))
class Mp7JrsUserActionType;
class IMp7JrsUserActionType;
/** Smart pointer for instance of IMp7JrsUserActionType */
typedef Dc1Ptr< IMp7JrsUserActionType > Mp7JrsUserActionPtr;

#define TypeOfUserActionType_ActionDataItem_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionType_ActionDataItem_CollectionType")))
#define CreateUserActionType_ActionDataItem_CollectionType (Dc1Factory::CreateObject(TypeOfUserActionType_ActionDataItem_CollectionType))
class Mp7JrsUserActionType_ActionDataItem_CollectionType;
class IMp7JrsUserActionType_ActionDataItem_CollectionType;
/** Smart pointer for instance of IMp7JrsUserActionType_ActionDataItem_CollectionType */
typedef Dc1Ptr< IMp7JrsUserActionType_ActionDataItem_CollectionType > Mp7JrsUserActionType_ActionDataItem_CollectionPtr;

#define TypeOfUserActionType_ActionTime_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionType_ActionTime_LocalType")))
#define CreateUserActionType_ActionTime_LocalType (Dc1Factory::CreateObject(TypeOfUserActionType_ActionTime_LocalType))
class Mp7JrsUserActionType_ActionTime_LocalType;
class IMp7JrsUserActionType_ActionTime_LocalType;
/** Smart pointer for instance of IMp7JrsUserActionType_ActionTime_LocalType */
typedef Dc1Ptr< IMp7JrsUserActionType_ActionTime_LocalType > Mp7JrsUserActionType_ActionTime_LocalPtr;

#define TypeOfuserChoiceType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:userChoiceType")))
#define CreateuserChoiceType (Dc1Factory::CreateObject(TypeOfuserChoiceType))
class Mp7JrsuserChoiceType;
class IMp7JrsuserChoiceType;
// No smart pointer instance for IMp7JrsuserChoiceType

#define TypeOfUserDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserDescriptionType")))
#define CreateUserDescriptionType (Dc1Factory::CreateObject(TypeOfUserDescriptionType))
class Mp7JrsUserDescriptionType;
class IMp7JrsUserDescriptionType;
/** Smart pointer for instance of IMp7JrsUserDescriptionType */
typedef Dc1Ptr< IMp7JrsUserDescriptionType > Mp7JrsUserDescriptionPtr;

#define TypeOfUserDescriptionType_UsageHistory_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserDescriptionType_UsageHistory_CollectionType")))
#define CreateUserDescriptionType_UsageHistory_CollectionType (Dc1Factory::CreateObject(TypeOfUserDescriptionType_UsageHistory_CollectionType))
class Mp7JrsUserDescriptionType_UsageHistory_CollectionType;
class IMp7JrsUserDescriptionType_UsageHistory_CollectionType;
/** Smart pointer for instance of IMp7JrsUserDescriptionType_UsageHistory_CollectionType */
typedef Dc1Ptr< IMp7JrsUserDescriptionType_UsageHistory_CollectionType > Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr;

#define TypeOfUserDescriptionType_UserPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserDescriptionType_UserPreferences_CollectionType")))
#define CreateUserDescriptionType_UserPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfUserDescriptionType_UserPreferences_CollectionType))
class Mp7JrsUserDescriptionType_UserPreferences_CollectionType;
class IMp7JrsUserDescriptionType_UserPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsUserDescriptionType_UserPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsUserDescriptionType_UserPreferences_CollectionType > Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr;

#define TypeOfUserIdentifierType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserIdentifierType")))
#define CreateUserIdentifierType (Dc1Factory::CreateObject(TypeOfUserIdentifierType))
class Mp7JrsUserIdentifierType;
class IMp7JrsUserIdentifierType;
/** Smart pointer for instance of IMp7JrsUserIdentifierType */
typedef Dc1Ptr< IMp7JrsUserIdentifierType > Mp7JrsUserIdentifierPtr;

#define TypeOfUserPreferencesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserPreferencesType")))
#define CreateUserPreferencesType (Dc1Factory::CreateObject(TypeOfUserPreferencesType))
class Mp7JrsUserPreferencesType;
class IMp7JrsUserPreferencesType;
/** Smart pointer for instance of IMp7JrsUserPreferencesType */
typedef Dc1Ptr< IMp7JrsUserPreferencesType > Mp7JrsUserPreferencesPtr;

#define TypeOfUserPreferencesType_BrowsingPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserPreferencesType_BrowsingPreferences_CollectionType")))
#define CreateUserPreferencesType_BrowsingPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfUserPreferencesType_BrowsingPreferences_CollectionType))
class Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionType;
class IMp7JrsUserPreferencesType_BrowsingPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsUserPreferencesType_BrowsingPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsUserPreferencesType_BrowsingPreferences_CollectionType > Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr;

#define TypeOfUserPreferencesType_FilteringAndSearchPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserPreferencesType_FilteringAndSearchPreferences_CollectionType")))
#define CreateUserPreferencesType_FilteringAndSearchPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfUserPreferencesType_FilteringAndSearchPreferences_CollectionType))
class Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionType;
class IMp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionType > Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr;

#define TypeOfUserPreferencesType_RecordingPreferences_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserPreferencesType_RecordingPreferences_CollectionType")))
#define CreateUserPreferencesType_RecordingPreferences_CollectionType (Dc1Factory::CreateObject(TypeOfUserPreferencesType_RecordingPreferences_CollectionType))
class Mp7JrsUserPreferencesType_RecordingPreferences_CollectionType;
class IMp7JrsUserPreferencesType_RecordingPreferences_CollectionType;
/** Smart pointer for instance of IMp7JrsUserPreferencesType_RecordingPreferences_CollectionType */
typedef Dc1Ptr< IMp7JrsUserPreferencesType_RecordingPreferences_CollectionType > Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr;

#define TypeOfUserProfileType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserProfileType")))
#define CreateUserProfileType (Dc1Factory::CreateObject(TypeOfUserProfileType))
class Mp7JrsUserProfileType;
class IMp7JrsUserProfileType;
/** Smart pointer for instance of IMp7JrsUserProfileType */
typedef Dc1Ptr< IMp7JrsUserProfileType > Mp7JrsUserProfilePtr;

#define TypeOfUserProfileType_PublicIdentifier_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserProfileType_PublicIdentifier_CollectionType")))
#define CreateUserProfileType_PublicIdentifier_CollectionType (Dc1Factory::CreateObject(TypeOfUserProfileType_PublicIdentifier_CollectionType))
class Mp7JrsUserProfileType_PublicIdentifier_CollectionType;
class IMp7JrsUserProfileType_PublicIdentifier_CollectionType;
/** Smart pointer for instance of IMp7JrsUserProfileType_PublicIdentifier_CollectionType */
typedef Dc1Ptr< IMp7JrsUserProfileType_PublicIdentifier_CollectionType > Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr;

#define TypeOfUserProfileType_sex_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserProfileType_sex_LocalType")))
#define CreateUserProfileType_sex_LocalType (Dc1Factory::CreateObject(TypeOfUserProfileType_sex_LocalType))
class Mp7JrsUserProfileType_sex_LocalType;
class IMp7JrsUserProfileType_sex_LocalType;
// No smart pointer instance for IMp7JrsUserProfileType_sex_LocalType

#define TypeOfVariationDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationDescriptionType")))
#define CreateVariationDescriptionType (Dc1Factory::CreateObject(TypeOfVariationDescriptionType))
class Mp7JrsVariationDescriptionType;
class IMp7JrsVariationDescriptionType;
/** Smart pointer for instance of IMp7JrsVariationDescriptionType */
typedef Dc1Ptr< IMp7JrsVariationDescriptionType > Mp7JrsVariationDescriptionPtr;

#define TypeOfVariationDescriptionType_Variation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationDescriptionType_Variation_CollectionType")))
#define CreateVariationDescriptionType_Variation_CollectionType (Dc1Factory::CreateObject(TypeOfVariationDescriptionType_Variation_CollectionType))
class Mp7JrsVariationDescriptionType_Variation_CollectionType;
class IMp7JrsVariationDescriptionType_Variation_CollectionType;
/** Smart pointer for instance of IMp7JrsVariationDescriptionType_Variation_CollectionType */
typedef Dc1Ptr< IMp7JrsVariationDescriptionType_Variation_CollectionType > Mp7JrsVariationDescriptionType_Variation_CollectionPtr;

#define TypeOfVariationDescriptionType_VariationSet_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationDescriptionType_VariationSet_CollectionType")))
#define CreateVariationDescriptionType_VariationSet_CollectionType (Dc1Factory::CreateObject(TypeOfVariationDescriptionType_VariationSet_CollectionType))
class Mp7JrsVariationDescriptionType_VariationSet_CollectionType;
class IMp7JrsVariationDescriptionType_VariationSet_CollectionType;
/** Smart pointer for instance of IMp7JrsVariationDescriptionType_VariationSet_CollectionType */
typedef Dc1Ptr< IMp7JrsVariationDescriptionType_VariationSet_CollectionType > Mp7JrsVariationDescriptionType_VariationSet_CollectionPtr;

#define TypeOfVariationSetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationSetType")))
#define CreateVariationSetType (Dc1Factory::CreateObject(TypeOfVariationSetType))
class Mp7JrsVariationSetType;
class IMp7JrsVariationSetType;
/** Smart pointer for instance of IMp7JrsVariationSetType */
typedef Dc1Ptr< IMp7JrsVariationSetType > Mp7JrsVariationSetPtr;

#define TypeOfVariationSetType_Variation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationSetType_Variation_CollectionType")))
#define CreateVariationSetType_Variation_CollectionType (Dc1Factory::CreateObject(TypeOfVariationSetType_Variation_CollectionType))
class Mp7JrsVariationSetType_Variation_CollectionType;
class IMp7JrsVariationSetType_Variation_CollectionType;
/** Smart pointer for instance of IMp7JrsVariationSetType_Variation_CollectionType */
typedef Dc1Ptr< IMp7JrsVariationSetType_Variation_CollectionType > Mp7JrsVariationSetType_Variation_CollectionPtr;

#define TypeOfVariationType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType")))
#define CreateVariationType (Dc1Factory::CreateObject(TypeOfVariationType))
class Mp7JrsVariationType;
class IMp7JrsVariationType;
/** Smart pointer for instance of IMp7JrsVariationType */
typedef Dc1Ptr< IMp7JrsVariationType > Mp7JrsVariationPtr;

#define TypeOfVariationType_VariationRelationship_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_CollectionType")))
#define CreateVariationType_VariationRelationship_CollectionType (Dc1Factory::CreateObject(TypeOfVariationType_VariationRelationship_CollectionType))
class Mp7JrsVariationType_VariationRelationship_CollectionType;
class IMp7JrsVariationType_VariationRelationship_CollectionType;
/** Smart pointer for instance of IMp7JrsVariationType_VariationRelationship_CollectionType */
typedef Dc1Ptr< IMp7JrsVariationType_VariationRelationship_CollectionType > Mp7JrsVariationType_VariationRelationship_CollectionPtr;

#define TypeOfVariationType_VariationRelationship_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_LocalType")))
#define CreateVariationType_VariationRelationship_LocalType (Dc1Factory::CreateObject(TypeOfVariationType_VariationRelationship_LocalType))
class Mp7JrsVariationType_VariationRelationship_LocalType;
class IMp7JrsVariationType_VariationRelationship_LocalType;
// No smart pointer instance for IMp7JrsVariationType_VariationRelationship_LocalType

#define TypeOfVariationType_VariationRelationship_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_LocalType0")))
#define CreateVariationType_VariationRelationship_LocalType0 (Dc1Factory::CreateObject(TypeOfVariationType_VariationRelationship_LocalType0))
class Mp7JrsVariationType_VariationRelationship_LocalType0;
class IMp7JrsVariationType_VariationRelationship_LocalType0;
/** Smart pointer for instance of IMp7JrsVariationType_VariationRelationship_LocalType0 */
typedef Dc1Ptr< IMp7JrsVariationType_VariationRelationship_LocalType0 > Mp7JrsVariationType_VariationRelationship_Local0Ptr;

#define TypeOfVariationType_VariationRelationship_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_LocalType1")))
#define CreateVariationType_VariationRelationship_LocalType1 (Dc1Factory::CreateObject(TypeOfVariationType_VariationRelationship_LocalType1))
class Mp7JrsVariationType_VariationRelationship_LocalType1;
class IMp7JrsVariationType_VariationRelationship_LocalType1;
/** Smart pointer for instance of IMp7JrsVariationType_VariationRelationship_LocalType1 */
typedef Dc1Ptr< IMp7JrsVariationType_VariationRelationship_LocalType1 > Mp7JrsVariationType_VariationRelationship_Local1Ptr;

#define TypeOfVariationType_VariationRelationship_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_LocalType2")))
#define CreateVariationType_VariationRelationship_LocalType2 (Dc1Factory::CreateObject(TypeOfVariationType_VariationRelationship_LocalType2))
class Mp7JrsVariationType_VariationRelationship_LocalType2;
class IMp7JrsVariationType_VariationRelationship_LocalType2;
/** Smart pointer for instance of IMp7JrsVariationType_VariationRelationship_LocalType2 */
typedef Dc1Ptr< IMp7JrsVariationType_VariationRelationship_LocalType2 > Mp7JrsVariationType_VariationRelationship_Local2Ptr;

#define TypeOfVideoSegmentFeatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentFeatureType")))
#define CreateVideoSegmentFeatureType (Dc1Factory::CreateObject(TypeOfVideoSegmentFeatureType))
class Mp7JrsVideoSegmentFeatureType;
class IMp7JrsVideoSegmentFeatureType;
/** Smart pointer for instance of IMp7JrsVideoSegmentFeatureType */
typedef Dc1Ptr< IMp7JrsVideoSegmentFeatureType > Mp7JrsVideoSegmentFeaturePtr;

#define TypeOfVideoSegmentFeatureType_RepresentativeFeature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentFeatureType_RepresentativeFeature_CollectionType")))
#define CreateVideoSegmentFeatureType_RepresentativeFeature_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentFeatureType_RepresentativeFeature_CollectionType))
class Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType;
class IMp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType > Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr;

#define TypeOfVideoSegmentFeatureType_TemporalTransition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentFeatureType_TemporalTransition_CollectionType")))
#define CreateVideoSegmentFeatureType_TemporalTransition_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentFeatureType_TemporalTransition_CollectionType))
class Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType;
class IMp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType > Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr;

#define TypeOfVideoSegmentMediaSourceDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentMediaSourceDecompositionType")))
#define CreateVideoSegmentMediaSourceDecompositionType (Dc1Factory::CreateObject(TypeOfVideoSegmentMediaSourceDecompositionType))
class Mp7JrsVideoSegmentMediaSourceDecompositionType;
class IMp7JrsVideoSegmentMediaSourceDecompositionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentMediaSourceDecompositionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentMediaSourceDecompositionType > Mp7JrsVideoSegmentMediaSourceDecompositionPtr;

#define TypeOfVideoSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentMediaSourceDecompositionType_CollectionType")))
#define CreateVideoSegmentMediaSourceDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentMediaSourceDecompositionType_CollectionType))
class Mp7JrsVideoSegmentMediaSourceDecompositionType_CollectionType;
class IMp7JrsVideoSegmentMediaSourceDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentMediaSourceDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentMediaSourceDecompositionType_CollectionType > Mp7JrsVideoSegmentMediaSourceDecompositionType_CollectionPtr;

#define TypeOfVideoSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentMediaSourceDecompositionType_LocalType")))
#define CreateVideoSegmentMediaSourceDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfVideoSegmentMediaSourceDecompositionType_LocalType))
class Mp7JrsVideoSegmentMediaSourceDecompositionType_LocalType;
class IMp7JrsVideoSegmentMediaSourceDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSegmentMediaSourceDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSegmentMediaSourceDecompositionType_LocalType > Mp7JrsVideoSegmentMediaSourceDecompositionType_LocalPtr;

#define TypeOfVideoSegmentSpatialDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatialDecompositionType")))
#define CreateVideoSegmentSpatialDecompositionType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatialDecompositionType))
class Mp7JrsVideoSegmentSpatialDecompositionType;
class IMp7JrsVideoSegmentSpatialDecompositionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatialDecompositionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatialDecompositionType > Mp7JrsVideoSegmentSpatialDecompositionPtr;

#define TypeOfVideoSegmentSpatialDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatialDecompositionType_CollectionType")))
#define CreateVideoSegmentSpatialDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatialDecompositionType_CollectionType))
class Mp7JrsVideoSegmentSpatialDecompositionType_CollectionType;
class IMp7JrsVideoSegmentSpatialDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatialDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatialDecompositionType_CollectionType > Mp7JrsVideoSegmentSpatialDecompositionType_CollectionPtr;

#define TypeOfVideoSegmentSpatialDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatialDecompositionType_LocalType")))
#define CreateVideoSegmentSpatialDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatialDecompositionType_LocalType))
class Mp7JrsVideoSegmentSpatialDecompositionType_LocalType;
class IMp7JrsVideoSegmentSpatialDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatialDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatialDecompositionType_LocalType > Mp7JrsVideoSegmentSpatialDecompositionType_LocalPtr;

#define TypeOfVideoSegmentSpatioTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatioTemporalDecompositionType")))
#define CreateVideoSegmentSpatioTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatioTemporalDecompositionType))
class Mp7JrsVideoSegmentSpatioTemporalDecompositionType;
class IMp7JrsVideoSegmentSpatioTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatioTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatioTemporalDecompositionType > Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr;

#define TypeOfVideoSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatioTemporalDecompositionType_CollectionType")))
#define CreateVideoSegmentSpatioTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatioTemporalDecompositionType_CollectionType))
class Mp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionType;
class IMp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionType > Mp7JrsVideoSegmentSpatioTemporalDecompositionType_CollectionPtr;

#define TypeOfVideoSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatioTemporalDecompositionType_LocalType")))
#define CreateVideoSegmentSpatioTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfVideoSegmentSpatioTemporalDecompositionType_LocalType))
class Mp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalType;
class IMp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalType > Mp7JrsVideoSegmentSpatioTemporalDecompositionType_LocalPtr;

#define TypeOfVideoSegmentTemporalDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentTemporalDecompositionType")))
#define CreateVideoSegmentTemporalDecompositionType (Dc1Factory::CreateObject(TypeOfVideoSegmentTemporalDecompositionType))
class Mp7JrsVideoSegmentTemporalDecompositionType;
class IMp7JrsVideoSegmentTemporalDecompositionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentTemporalDecompositionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentTemporalDecompositionType > Mp7JrsVideoSegmentTemporalDecompositionPtr;

#define TypeOfVideoSegmentTemporalDecompositionType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentTemporalDecompositionType_CollectionType")))
#define CreateVideoSegmentTemporalDecompositionType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentTemporalDecompositionType_CollectionType))
class Mp7JrsVideoSegmentTemporalDecompositionType_CollectionType;
class IMp7JrsVideoSegmentTemporalDecompositionType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentTemporalDecompositionType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentTemporalDecompositionType_CollectionType > Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr;

#define TypeOfVideoSegmentTemporalDecompositionType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentTemporalDecompositionType_LocalType")))
#define CreateVideoSegmentTemporalDecompositionType_LocalType (Dc1Factory::CreateObject(TypeOfVideoSegmentTemporalDecompositionType_LocalType))
class Mp7JrsVideoSegmentTemporalDecompositionType_LocalType;
class IMp7JrsVideoSegmentTemporalDecompositionType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSegmentTemporalDecompositionType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSegmentTemporalDecompositionType_LocalType > Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr;

#define TypeOfVideoSegmentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType")))
#define CreateVideoSegmentType (Dc1Factory::CreateObject(TypeOfVideoSegmentType))
class Mp7JrsVideoSegmentType;
class IMp7JrsVideoSegmentType;
/** Smart pointer for instance of IMp7JrsVideoSegmentType */
typedef Dc1Ptr< IMp7JrsVideoSegmentType > Mp7JrsVideoSegmentPtr;

#define TypeOfVideoSegmentType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType_CollectionType")))
#define CreateVideoSegmentType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentType_CollectionType))
class Mp7JrsVideoSegmentType_CollectionType;
class IMp7JrsVideoSegmentType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentType_CollectionType > Mp7JrsVideoSegmentType_CollectionPtr;

#define TypeOfVideoSegmentType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType_CollectionType0")))
#define CreateVideoSegmentType_CollectionType0 (Dc1Factory::CreateObject(TypeOfVideoSegmentType_CollectionType0))
class Mp7JrsVideoSegmentType_CollectionType0;
class IMp7JrsVideoSegmentType_CollectionType0;
/** Smart pointer for instance of IMp7JrsVideoSegmentType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsVideoSegmentType_CollectionType0 > Mp7JrsVideoSegmentType_Collection0Ptr;

#define TypeOfVideoSegmentType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType_LocalType")))
#define CreateVideoSegmentType_LocalType (Dc1Factory::CreateObject(TypeOfVideoSegmentType_LocalType))
class Mp7JrsVideoSegmentType_LocalType;
class IMp7JrsVideoSegmentType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSegmentType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSegmentType_LocalType > Mp7JrsVideoSegmentType_LocalPtr;

#define TypeOfVideoSegmentType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType_LocalType0")))
#define CreateVideoSegmentType_LocalType0 (Dc1Factory::CreateObject(TypeOfVideoSegmentType_LocalType0))
class Mp7JrsVideoSegmentType_LocalType0;
class IMp7JrsVideoSegmentType_LocalType0;
/** Smart pointer for instance of IMp7JrsVideoSegmentType_LocalType0 */
typedef Dc1Ptr< IMp7JrsVideoSegmentType_LocalType0 > Mp7JrsVideoSegmentType_Local0Ptr;

#define TypeOfVideoSegmentType_Mosaic_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType_Mosaic_CollectionType")))
#define CreateVideoSegmentType_Mosaic_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSegmentType_Mosaic_CollectionType))
class Mp7JrsVideoSegmentType_Mosaic_CollectionType;
class IMp7JrsVideoSegmentType_Mosaic_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSegmentType_Mosaic_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSegmentType_Mosaic_CollectionType > Mp7JrsVideoSegmentType_Mosaic_CollectionPtr;

#define TypeOfVideoSignatureType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType")))
#define CreateVideoSignatureType (Dc1Factory::CreateObject(TypeOfVideoSignatureType))
class Mp7JrsVideoSignatureType;
class IMp7JrsVideoSignatureType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType > Mp7JrsVideoSignaturePtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_Pixel_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0 (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0 */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0 > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalPtr;

#define TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType")))
#define CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType (Dc1Factory::CreateObject(TypeOfVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType))
class Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType;
class IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType;
/** Smart pointer for instance of IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType */
typedef Dc1Ptr< IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType > Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr;

#define TypeOfVideoTextType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoTextType")))
#define CreateVideoTextType (Dc1Factory::CreateObject(TypeOfVideoTextType))
class Mp7JrsVideoTextType;
class IMp7JrsVideoTextType;
/** Smart pointer for instance of IMp7JrsVideoTextType */
typedef Dc1Ptr< IMp7JrsVideoTextType > Mp7JrsVideoTextPtr;

#define TypeOfVideoTextType_textType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoTextType_textType_LocalType")))
#define CreateVideoTextType_textType_LocalType (Dc1Factory::CreateObject(TypeOfVideoTextType_textType_LocalType))
class Mp7JrsVideoTextType_textType_LocalType;
class IMp7JrsVideoTextType_textType_LocalType;
// No smart pointer instance for IMp7JrsVideoTextType_textType_LocalType

#define TypeOfVideoTextType_textType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoTextType_textType_LocalType0")))
#define CreateVideoTextType_textType_LocalType0 (Dc1Factory::CreateObject(TypeOfVideoTextType_textType_LocalType0))
class Mp7JrsVideoTextType_textType_LocalType0;
class IMp7JrsVideoTextType_textType_LocalType0;
/** Smart pointer for instance of IMp7JrsVideoTextType_textType_LocalType0 */
typedef Dc1Ptr< IMp7JrsVideoTextType_textType_LocalType0 > Mp7JrsVideoTextType_textType_Local0Ptr;

#define TypeOfVideoTextType_textType_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoTextType_textType_LocalType1")))
#define CreateVideoTextType_textType_LocalType1 (Dc1Factory::CreateObject(TypeOfVideoTextType_textType_LocalType1))
class Mp7JrsVideoTextType_textType_LocalType1;
class IMp7JrsVideoTextType_textType_LocalType1;
/** Smart pointer for instance of IMp7JrsVideoTextType_textType_LocalType1 */
typedef Dc1Ptr< IMp7JrsVideoTextType_textType_LocalType1 > Mp7JrsVideoTextType_textType_Local1Ptr;

#define TypeOfVideoTextType_textType_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoTextType_textType_LocalType2")))
#define CreateVideoTextType_textType_LocalType2 (Dc1Factory::CreateObject(TypeOfVideoTextType_textType_LocalType2))
class Mp7JrsVideoTextType_textType_LocalType2;
class IMp7JrsVideoTextType_textType_LocalType2;
/** Smart pointer for instance of IMp7JrsVideoTextType_textType_LocalType2 */
typedef Dc1Ptr< IMp7JrsVideoTextType_textType_LocalType2 > Mp7JrsVideoTextType_textType_Local2Ptr;

#define TypeOfVideoType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoType")))
#define CreateVideoType (Dc1Factory::CreateObject(TypeOfVideoType))
class Mp7JrsVideoType;
class IMp7JrsVideoType;
/** Smart pointer for instance of IMp7JrsVideoType */
typedef Dc1Ptr< IMp7JrsVideoType > Mp7JrsVideoPtr;

#define TypeOfVideoViewGraphType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType")))
#define CreateVideoViewGraphType (Dc1Factory::CreateObject(TypeOfVideoViewGraphType))
class Mp7JrsVideoViewGraphType;
class IMp7JrsVideoViewGraphType;
/** Smart pointer for instance of IMp7JrsVideoViewGraphType */
typedef Dc1Ptr< IMp7JrsVideoViewGraphType > Mp7JrsVideoViewGraphPtr;

#define TypeOfVideoViewGraphType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType_CollectionType")))
#define CreateVideoViewGraphType_CollectionType (Dc1Factory::CreateObject(TypeOfVideoViewGraphType_CollectionType))
class Mp7JrsVideoViewGraphType_CollectionType;
class IMp7JrsVideoViewGraphType_CollectionType;
/** Smart pointer for instance of IMp7JrsVideoViewGraphType_CollectionType */
typedef Dc1Ptr< IMp7JrsVideoViewGraphType_CollectionType > Mp7JrsVideoViewGraphType_CollectionPtr;

#define TypeOfVideoViewGraphType_CollectionType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType_CollectionType0")))
#define CreateVideoViewGraphType_CollectionType0 (Dc1Factory::CreateObject(TypeOfVideoViewGraphType_CollectionType0))
class Mp7JrsVideoViewGraphType_CollectionType0;
class IMp7JrsVideoViewGraphType_CollectionType0;
/** Smart pointer for instance of IMp7JrsVideoViewGraphType_CollectionType0 */
typedef Dc1Ptr< IMp7JrsVideoViewGraphType_CollectionType0 > Mp7JrsVideoViewGraphType_Collection0Ptr;

#define TypeOfVideoViewGraphType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType_LocalType")))
#define CreateVideoViewGraphType_LocalType (Dc1Factory::CreateObject(TypeOfVideoViewGraphType_LocalType))
class Mp7JrsVideoViewGraphType_LocalType;
class IMp7JrsVideoViewGraphType_LocalType;
/** Smart pointer for instance of IMp7JrsVideoViewGraphType_LocalType */
typedef Dc1Ptr< IMp7JrsVideoViewGraphType_LocalType > Mp7JrsVideoViewGraphType_LocalPtr;

#define TypeOfVideoViewGraphType_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType_LocalType0")))
#define CreateVideoViewGraphType_LocalType0 (Dc1Factory::CreateObject(TypeOfVideoViewGraphType_LocalType0))
class Mp7JrsVideoViewGraphType_LocalType0;
class IMp7JrsVideoViewGraphType_LocalType0;
/** Smart pointer for instance of IMp7JrsVideoViewGraphType_LocalType0 */
typedef Dc1Ptr< IMp7JrsVideoViewGraphType_LocalType0 > Mp7JrsVideoViewGraphType_Local0Ptr;

#define TypeOfViewDecompositionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewDecompositionType")))
#define CreateViewDecompositionType (Dc1Factory::CreateObject(TypeOfViewDecompositionType))
class Mp7JrsViewDecompositionType;
class IMp7JrsViewDecompositionType;
/** Smart pointer for instance of IMp7JrsViewDecompositionType */
typedef Dc1Ptr< IMp7JrsViewDecompositionType > Mp7JrsViewDecompositionPtr;

#define TypeOfViewDescriptionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewDescriptionType")))
#define CreateViewDescriptionType (Dc1Factory::CreateObject(TypeOfViewDescriptionType))
class Mp7JrsViewDescriptionType;
class IMp7JrsViewDescriptionType;
/** Smart pointer for instance of IMp7JrsViewDescriptionType */
typedef Dc1Ptr< IMp7JrsViewDescriptionType > Mp7JrsViewDescriptionPtr;

#define TypeOfViewDescriptionType_View_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewDescriptionType_View_CollectionType")))
#define CreateViewDescriptionType_View_CollectionType (Dc1Factory::CreateObject(TypeOfViewDescriptionType_View_CollectionType))
class Mp7JrsViewDescriptionType_View_CollectionType;
class IMp7JrsViewDescriptionType_View_CollectionType;
/** Smart pointer for instance of IMp7JrsViewDescriptionType_View_CollectionType */
typedef Dc1Ptr< IMp7JrsViewDescriptionType_View_CollectionType > Mp7JrsViewDescriptionType_View_CollectionPtr;

#define TypeOfViewDescriptionType_ViewDecomposition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewDescriptionType_ViewDecomposition_CollectionType")))
#define CreateViewDescriptionType_ViewDecomposition_CollectionType (Dc1Factory::CreateObject(TypeOfViewDescriptionType_ViewDecomposition_CollectionType))
class Mp7JrsViewDescriptionType_ViewDecomposition_CollectionType;
class IMp7JrsViewDescriptionType_ViewDecomposition_CollectionType;
/** Smart pointer for instance of IMp7JrsViewDescriptionType_ViewDecomposition_CollectionType */
typedef Dc1Ptr< IMp7JrsViewDescriptionType_ViewDecomposition_CollectionType > Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr;

#define TypeOfViewDescriptionType_ViewSet_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewDescriptionType_ViewSet_CollectionType")))
#define CreateViewDescriptionType_ViewSet_CollectionType (Dc1Factory::CreateObject(TypeOfViewDescriptionType_ViewSet_CollectionType))
class Mp7JrsViewDescriptionType_ViewSet_CollectionType;
class IMp7JrsViewDescriptionType_ViewSet_CollectionType;
/** Smart pointer for instance of IMp7JrsViewDescriptionType_ViewSet_CollectionType */
typedef Dc1Ptr< IMp7JrsViewDescriptionType_ViewSet_CollectionType > Mp7JrsViewDescriptionType_ViewSet_CollectionPtr;

#define TypeOfViewSetType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType")))
#define CreateViewSetType (Dc1Factory::CreateObject(TypeOfViewSetType))
class Mp7JrsViewSetType;
class IMp7JrsViewSetType;
/** Smart pointer for instance of IMp7JrsViewSetType */
typedef Dc1Ptr< IMp7JrsViewSetType > Mp7JrsViewSetPtr;

#define TypeOfViewSetType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_CollectionType")))
#define CreateViewSetType_CollectionType (Dc1Factory::CreateObject(TypeOfViewSetType_CollectionType))
class Mp7JrsViewSetType_CollectionType;
class IMp7JrsViewSetType_CollectionType;
/** Smart pointer for instance of IMp7JrsViewSetType_CollectionType */
typedef Dc1Ptr< IMp7JrsViewSetType_CollectionType > Mp7JrsViewSetType_CollectionPtr;

#define TypeOfViewSetType_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_LocalType")))
#define CreateViewSetType_LocalType (Dc1Factory::CreateObject(TypeOfViewSetType_LocalType))
class Mp7JrsViewSetType_LocalType;
class IMp7JrsViewSetType_LocalType;
/** Smart pointer for instance of IMp7JrsViewSetType_LocalType */
typedef Dc1Ptr< IMp7JrsViewSetType_LocalType > Mp7JrsViewSetType_LocalPtr;

#define TypeOfViewSetType_setProperty_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_setProperty_LocalType")))
#define CreateViewSetType_setProperty_LocalType (Dc1Factory::CreateObject(TypeOfViewSetType_setProperty_LocalType))
class Mp7JrsViewSetType_setProperty_LocalType;
class IMp7JrsViewSetType_setProperty_LocalType;
// No smart pointer instance for IMp7JrsViewSetType_setProperty_LocalType

#define TypeOfViewSetType_setProperty_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_setProperty_LocalType0")))
#define CreateViewSetType_setProperty_LocalType0 (Dc1Factory::CreateObject(TypeOfViewSetType_setProperty_LocalType0))
class Mp7JrsViewSetType_setProperty_LocalType0;
class IMp7JrsViewSetType_setProperty_LocalType0;
/** Smart pointer for instance of IMp7JrsViewSetType_setProperty_LocalType0 */
typedef Dc1Ptr< IMp7JrsViewSetType_setProperty_LocalType0 > Mp7JrsViewSetType_setProperty_Local0Ptr;

#define TypeOfViewSetType_setProperty_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_setProperty_LocalType1")))
#define CreateViewSetType_setProperty_LocalType1 (Dc1Factory::CreateObject(TypeOfViewSetType_setProperty_LocalType1))
class Mp7JrsViewSetType_setProperty_LocalType1;
class IMp7JrsViewSetType_setProperty_LocalType1;
/** Smart pointer for instance of IMp7JrsViewSetType_setProperty_LocalType1 */
typedef Dc1Ptr< IMp7JrsViewSetType_setProperty_LocalType1 > Mp7JrsViewSetType_setProperty_Local1Ptr;

#define TypeOfViewSetType_setProperty_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType_setProperty_LocalType2")))
#define CreateViewSetType_setProperty_LocalType2 (Dc1Factory::CreateObject(TypeOfViewSetType_setProperty_LocalType2))
class Mp7JrsViewSetType_setProperty_LocalType2;
class IMp7JrsViewSetType_setProperty_LocalType2;
/** Smart pointer for instance of IMp7JrsViewSetType_setProperty_LocalType2 */
typedef Dc1Ptr< IMp7JrsViewSetType_setProperty_LocalType2 > Mp7JrsViewSetType_setProperty_Local2Ptr;

#define TypeOfViewType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewType")))
#define CreateViewType (Dc1Factory::CreateObject(TypeOfViewType))
class Mp7JrsViewType;
class IMp7JrsViewType;
/** Smart pointer for instance of IMp7JrsViewType */
typedef Dc1Ptr< IMp7JrsViewType > Mp7JrsViewPtr;

#define TypeOfVisualDSType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VisualDSType")))
#define CreateVisualDSType (Dc1Factory::CreateObject(TypeOfVisualDSType))
class Mp7JrsVisualDSType;
class IMp7JrsVisualDSType;
/** Smart pointer for instance of IMp7JrsVisualDSType */
typedef Dc1Ptr< IMp7JrsVisualDSType > Mp7JrsVisualDSPtr;

#define TypeOfVisualDType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VisualDType")))
#define CreateVisualDType (Dc1Factory::CreateObject(TypeOfVisualDType))
class Mp7JrsVisualDType;
class IMp7JrsVisualDType;
/** Smart pointer for instance of IMp7JrsVisualDType */
typedef Dc1Ptr< IMp7JrsVisualDType > Mp7JrsVisualDPtr;

#define TypeOfVisualSummaryComponentType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VisualSummaryComponentType")))
#define CreateVisualSummaryComponentType (Dc1Factory::CreateObject(TypeOfVisualSummaryComponentType))
class Mp7JrsVisualSummaryComponentType;
class IMp7JrsVisualSummaryComponentType;
/** Smart pointer for instance of IMp7JrsVisualSummaryComponentType */
typedef Dc1Ptr< IMp7JrsVisualSummaryComponentType > Mp7JrsVisualSummaryComponentPtr;

#define TypeOfVisualTimeSeriesType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VisualTimeSeriesType")))
#define CreateVisualTimeSeriesType (Dc1Factory::CreateObject(TypeOfVisualTimeSeriesType))
class Mp7JrsVisualTimeSeriesType;
class IMp7JrsVisualTimeSeriesType;
/** Smart pointer for instance of IMp7JrsVisualTimeSeriesType */
typedef Dc1Ptr< IMp7JrsVisualTimeSeriesType > Mp7JrsVisualTimeSeriesPtr;

#define TypeOfWordFormType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType")))
#define CreateWordFormType (Dc1Factory::CreateObject(TypeOfWordFormType))
class Mp7JrsWordFormType;
class IMp7JrsWordFormType;
/** Smart pointer for instance of IMp7JrsWordFormType */
typedef Dc1Ptr< IMp7JrsWordFormType > Mp7JrsWordFormPtr;

#define TypeOfWordFormType_terms_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_terms_CollectionType")))
#define CreateWordFormType_terms_CollectionType (Dc1Factory::CreateObject(TypeOfWordFormType_terms_CollectionType))
class Mp7JrsWordFormType_terms_CollectionType;
class IMp7JrsWordFormType_terms_CollectionType;
/** Smart pointer for instance of IMp7JrsWordFormType_terms_CollectionType */
typedef Dc1Ptr< IMp7JrsWordFormType_terms_CollectionType > Mp7JrsWordFormType_terms_CollectionPtr;

#define TypeOfWordFormType_type_CollectionType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_type_CollectionType")))
#define CreateWordFormType_type_CollectionType (Dc1Factory::CreateObject(TypeOfWordFormType_type_CollectionType))
class Mp7JrsWordFormType_type_CollectionType;
class IMp7JrsWordFormType_type_CollectionType;
/** Smart pointer for instance of IMp7JrsWordFormType_type_CollectionType */
typedef Dc1Ptr< IMp7JrsWordFormType_type_CollectionType > Mp7JrsWordFormType_type_CollectionPtr;

#define TypeOfWordFormType_type_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_type_LocalType")))
#define CreateWordFormType_type_LocalType (Dc1Factory::CreateObject(TypeOfWordFormType_type_LocalType))
class Mp7JrsWordFormType_type_LocalType;
class IMp7JrsWordFormType_type_LocalType;
// No smart pointer instance for IMp7JrsWordFormType_type_LocalType

#define TypeOfWordFormType_type_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_type_LocalType0")))
#define CreateWordFormType_type_LocalType0 (Dc1Factory::CreateObject(TypeOfWordFormType_type_LocalType0))
class Mp7JrsWordFormType_type_LocalType0;
class IMp7JrsWordFormType_type_LocalType0;
/** Smart pointer for instance of IMp7JrsWordFormType_type_LocalType0 */
typedef Dc1Ptr< IMp7JrsWordFormType_type_LocalType0 > Mp7JrsWordFormType_type_Local0Ptr;

#define TypeOfWordFormType_type_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_type_LocalType1")))
#define CreateWordFormType_type_LocalType1 (Dc1Factory::CreateObject(TypeOfWordFormType_type_LocalType1))
class Mp7JrsWordFormType_type_LocalType1;
class IMp7JrsWordFormType_type_LocalType1;
/** Smart pointer for instance of IMp7JrsWordFormType_type_LocalType1 */
typedef Dc1Ptr< IMp7JrsWordFormType_type_LocalType1 > Mp7JrsWordFormType_type_Local1Ptr;

#define TypeOfWordFormType_type_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType_type_LocalType2")))
#define CreateWordFormType_type_LocalType2 (Dc1Factory::CreateObject(TypeOfWordFormType_type_LocalType2))
class Mp7JrsWordFormType_type_LocalType2;
class IMp7JrsWordFormType_type_LocalType2;
/** Smart pointer for instance of IMp7JrsWordFormType_type_LocalType2 */
typedef Dc1Ptr< IMp7JrsWordFormType_type_LocalType2 > Mp7JrsWordFormType_type_Local2Ptr;

#define TypeOfWordLexiconIndexType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconIndexType")))
#define CreateWordLexiconIndexType (Dc1Factory::CreateObject(TypeOfWordLexiconIndexType))
class Mp7JrsWordLexiconIndexType;
class IMp7JrsWordLexiconIndexType;
/** Smart pointer for instance of IMp7JrsWordLexiconIndexType */
typedef Dc1Ptr< IMp7JrsWordLexiconIndexType > Mp7JrsWordLexiconIndexPtr;

#define TypeOfWordLexiconType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType")))
#define CreateWordLexiconType (Dc1Factory::CreateObject(TypeOfWordLexiconType))
class Mp7JrsWordLexiconType;
class IMp7JrsWordLexiconType;
/** Smart pointer for instance of IMp7JrsWordLexiconType */
typedef Dc1Ptr< IMp7JrsWordLexiconType > Mp7JrsWordLexiconPtr;

#define TypeOfWordLexiconType_Token_linguisticUnit_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_linguisticUnit_LocalType")))
#define CreateWordLexiconType_Token_linguisticUnit_LocalType (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_linguisticUnit_LocalType))
class Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType;
class IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType;
// No smart pointer instance for IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType

#define TypeOfWordLexiconType_Token_linguisticUnit_LocalType0 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_linguisticUnit_LocalType0")))
#define CreateWordLexiconType_Token_linguisticUnit_LocalType0 (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_linguisticUnit_LocalType0))
class Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType0;
class IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType0;
/** Smart pointer for instance of IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType0 */
typedef Dc1Ptr< IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType0 > Mp7JrsWordLexiconType_Token_linguisticUnit_Local0Ptr;

#define TypeOfWordLexiconType_Token_linguisticUnit_LocalType1 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_linguisticUnit_LocalType1")))
#define CreateWordLexiconType_Token_linguisticUnit_LocalType1 (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_linguisticUnit_LocalType1))
class Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType1;
class IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType1;
/** Smart pointer for instance of IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType1 */
typedef Dc1Ptr< IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType1 > Mp7JrsWordLexiconType_Token_linguisticUnit_Local1Ptr;

#define TypeOfWordLexiconType_Token_linguisticUnit_LocalType2 (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_linguisticUnit_LocalType2")))
#define CreateWordLexiconType_Token_linguisticUnit_LocalType2 (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_linguisticUnit_LocalType2))
class Mp7JrsWordLexiconType_Token_linguisticUnit_LocalType2;
class IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType2;
/** Smart pointer for instance of IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType2 */
typedef Dc1Ptr< IMp7JrsWordLexiconType_Token_linguisticUnit_LocalType2 > Mp7JrsWordLexiconType_Token_linguisticUnit_Local2Ptr;

#define TypeOfWordLexiconType_Token_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_LocalType")))
#define CreateWordLexiconType_Token_LocalType (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_LocalType))
class Mp7JrsWordLexiconType_Token_LocalType;
class IMp7JrsWordLexiconType_Token_LocalType;
/** Smart pointer for instance of IMp7JrsWordLexiconType_Token_LocalType */
typedef Dc1Ptr< IMp7JrsWordLexiconType_Token_LocalType > Mp7JrsWordLexiconType_Token_LocalPtr;

#define TypeOfWordLexiconType_Token_representation_LocalType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_representation_LocalType")))
#define CreateWordLexiconType_Token_representation_LocalType (Dc1Factory::CreateObject(TypeOfWordLexiconType_Token_representation_LocalType))
class Mp7JrsWordLexiconType_Token_representation_LocalType;
class IMp7JrsWordLexiconType_Token_representation_LocalType;
// No smart pointer instance for IMp7JrsWordLexiconType_Token_representation_LocalType

#define TypeOfWordType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordType")))
#define CreateWordType (Dc1Factory::CreateObject(TypeOfWordType))
class Mp7JrsWordType;
class IMp7JrsWordType;
/** Smart pointer for instance of IMp7JrsWordType */
typedef Dc1Ptr< IMp7JrsWordType > Mp7JrsWordPtr;

#define TypeOfxPathAbsoluteSelectorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathAbsoluteSelectorType")))
#define CreatexPathAbsoluteSelectorType (Dc1Factory::CreateObject(TypeOfxPathAbsoluteSelectorType))
class Mp7JrsxPathAbsoluteSelectorType;
class IMp7JrsxPathAbsoluteSelectorType;
/** Smart pointer for instance of IMp7JrsxPathAbsoluteSelectorType */
typedef Dc1Ptr< IMp7JrsxPathAbsoluteSelectorType > Mp7JrsxPathAbsoluteSelectorPtr;

#define TypeOfxPathFieldType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathFieldType")))
#define CreatexPathFieldType (Dc1Factory::CreateObject(TypeOfxPathFieldType))
class Mp7JrsxPathFieldType;
class IMp7JrsxPathFieldType;
/** Smart pointer for instance of IMp7JrsxPathFieldType */
typedef Dc1Ptr< IMp7JrsxPathFieldType > Mp7JrsxPathFieldPtr;

#define TypeOfxPathRefType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathRefType")))
#define CreatexPathRefType (Dc1Factory::CreateObject(TypeOfxPathRefType))
class Mp7JrsxPathRefType;
class IMp7JrsxPathRefType;
/** Smart pointer for instance of IMp7JrsxPathRefType */
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;

#define TypeOfxPathSelectorType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathSelectorType")))
#define CreatexPathSelectorType (Dc1Factory::CreateObject(TypeOfxPathSelectorType))
class Mp7JrsxPathSelectorType;
class IMp7JrsxPathSelectorType;
/** Smart pointer for instance of IMp7JrsxPathSelectorType */
typedef Dc1Ptr< IMp7JrsxPathSelectorType > Mp7JrsxPathSelectorPtr;

#define TypeOfxPathType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathType")))
#define CreatexPathType (Dc1Factory::CreateObject(TypeOfxPathType))
class Mp7JrsxPathType;
class IMp7JrsxPathType;
/** Smart pointer for instance of IMp7JrsxPathType */
typedef Dc1Ptr< IMp7JrsxPathType > Mp7JrsxPathPtr;

#define TypeOfzeroToOneType (Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType")))
#define CreatezeroToOneType (Dc1Factory::CreateObject(TypeOfzeroToOneType))
class Mp7JrszeroToOneType;
class IMp7JrszeroToOneType;
/** Smart pointer for instance of IMp7JrszeroToOneType */
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;

#include "Dc1PtrTypes.h"

#endif // MP7JRSFACTORYDEFINES_H
