/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef MP7JRSFACTORY_H
#define MP7JRSFACTORY_H

#include "Dc1Defines.h"

#include "Mp7JrsDefines.h" // FTT for MP7JRS_EXPORT
#include "Dc1Factory.h"
// This is for including all necessary types for initialising...
#include "Mp7JrsFactoryForward.h"


/** Extension factory class. To use extension classes covered by this factory, see the following code snippet.
 * \code
 * #include <xercesc/util/PlatformUtils.hpp>
 * #include "Your-Core-Factory.h" // Core lib
 * #include "Mp7JrsFactory.h" // Extension lib
 * void main()
 * {
 *	XMLPlatformUtils::Initialize();
 *	Your-Core-Factory::Initialize(); // Register all core classes
 * #  include "Mp7JrsFactoryInitialize.h" // Registers your extension classes
 *	{
 *	   // Do something useful here
 *	}
 *	Your-Core-Factory::Terminate(); // Cleanup
 *	XMLPlatformUtils::Terminate();
 * }
 * \endcode
 */
class MP7JRS_EXPORT Mp7JrsFactory
{

public:
	

	/** Returns the version string of the library. This normally changes, if the XML schema has changed. */
	static const char * GetVersion();
	
	
	//@}

	
};

#endif // MP7JRSFACTORY_H
