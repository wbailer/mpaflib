/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3589MP7JRSSUMMARYSEGMENTGROUPTYPE_H
#define _3589MP7JRSSUMMARYSEGMENTGROUPTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "Mp7JrsSummarySegmentGroupTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// begin extension included
// file Mp7JrsSummarySegmentGroupType_ExtInclude.h
class Mp7JrsSummarySegmentType;
class Mp7JrsTextualType;
// end extension included


class IMp7JrsDSType;
typedef Dc1Ptr< IMp7JrsDSType > Mp7JrsDSPtr;
class IMp7JrsSummarySegmentGroupType;
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType> Mp7JrsSummarySegmentGroupPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
class IW3CIDREFS;
typedef Dc1Ptr< IW3CIDREFS > W3CIDREFSPtr;
class IMp7JrsSummarySegmentGroupType_Name_CollectionType;
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_Name_CollectionType > Mp7JrsSummarySegmentGroupType_Name_CollectionPtr;
class IMp7JrsSummarySegmentGroupType_Caption_CollectionType;
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_Caption_CollectionType > Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr;
class IMp7JrsSummarySegmentGroupType_CollectionType;
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_CollectionType > Mp7JrsSummarySegmentGroupType_CollectionPtr;
class IMp7JrsSummarySegmentGroupType_CollectionType0;
typedef Dc1Ptr< IMp7JrsSummarySegmentGroupType_CollectionType0 > Mp7JrsSummarySegmentGroupType_Collection0Ptr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsSummarySegmentGroupType for class Mp7JrsSummarySegmentGroupType<br>
 * Located at: mpeg7-v4.xsd, line 8456<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsSummarySegmentGroupType 
 :
		public IMp7JrsDSType
{
public:
	// TODO: make these protected?
	IMp7JrsSummarySegmentGroupType();
	virtual ~IMp7JrsSummarySegmentGroupType();
	/**
	 * Get level attribute.
	 * @return int
	 */
	virtual int Getlevel() const = 0;

	/**
	 * Get level attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlevel() const = 0;

	/**
	 * Set level attribute.
	 * @param item int
	 */
	virtual void Setlevel(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate level attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelevel() = 0;

	/**
	 * Get duration attribute.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr Getduration() const = 0;

	/**
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const = 0;

	/**
	 * Set duration attribute.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void Setduration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration() = 0;

	/**
	 * Get numOfKeyFrames attribute.
	 * @return unsigned
	 */
	virtual unsigned GetnumOfKeyFrames() const = 0;

	/**
	 * Get numOfKeyFrames attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnumOfKeyFrames() const = 0;

	/**
	 * Set numOfKeyFrames attribute.
	 * @param item unsigned
	 */
	virtual void SetnumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate numOfKeyFrames attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenumOfKeyFrames() = 0;

	/**
	 * Get fidelity attribute.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getfidelity() const = 0;

	/**
	 * Get fidelity attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfidelity() const = 0;

	/**
	 * Set fidelity attribute.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setfidelity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate fidelity attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefidelity() = 0;

	/**
	 * Get themeIDs attribute.
	 * @return W3CIDREFSPtr
	 */
	virtual W3CIDREFSPtr GetthemeIDs() const = 0;

	/**
	 * Get themeIDs attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistthemeIDs() const = 0;

	/**
	 * Set themeIDs attribute.
	 * @param item const W3CIDREFSPtr &
	 */
	virtual void SetthemeIDs(const W3CIDREFSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate themeIDs attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatethemeIDs() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Name element.
	 * @return Mp7JrsSummarySegmentGroupType_Name_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Name_CollectionPtr GetName() const = 0;

	/**
	 * Set Name element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetName(const Mp7JrsSummarySegmentGroupType_Name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Caption element.
	 * @return Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr GetCaption() const = 0;

	/**
	 * Set Caption element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCaption(const Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SummarySegmentGroupType_LocalType element.
	 * @return Mp7JrsSummarySegmentGroupType_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_CollectionPtr GetSummarySegmentGroupType_LocalType() const = 0;

	/**
	 * Set SummarySegmentGroupType_LocalType element.
	 * @param item const Mp7JrsSummarySegmentGroupType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummarySegmentGroupType_LocalType(const Mp7JrsSummarySegmentGroupType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SummarySegmentGroupType_LocalType0 element.
	 * @return Mp7JrsSummarySegmentGroupType_Collection0Ptr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Collection0Ptr GetSummarySegmentGroupType_LocalType0() const = 0;

	/**
	 * Set SummarySegmentGroupType_LocalType0 element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetSummarySegmentGroupType_LocalType0(const Mp7JrsSummarySegmentGroupType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of SummarySegmentGroupType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Name</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Caption</li>
	 * <li>SummarySegment</li>
	 * <li>SummarySegmentRef</li>
	 * <li>SummarySegmentGroup</li>
	 * <li>SummarySegmentGroupRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// begin extension included
// file Mp7JrsSummarySegmentGroupType_ExtMethodDef.h


public:

	/** Add a summary segment */
	virtual void AddSegment(Mp7JrsSummarySegmentPtr segment, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get number of summary segments */
	virtual int GetNrSegments();


	/** Add a summary segment group */
	virtual void AddSegmentGroup(Mp7JrsSummarySegmentGroupPtr segment, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Get number of summary segment groups */
	virtual int GetNrSegmentGroups();


	// name and caption

	/** Add a name */
	virtual void AddName(XMLCh* name, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/** Add a caption */
	virtual void AddCaption(XMLCh* caption, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	// fidelity

	/** Set the fidelity. */
	virtual void Setfidelity(float fid, Dc1ClientID client);

// end extension included


// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSummarySegmentGroupType<br>
 * Located at: mpeg7-v4.xsd, line 8456<br>
 * Classified: Class<br>
 * Derived from: DSType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 8456.<br>
 */
class DC1_EXPORT Mp7JrsSummarySegmentGroupType :
		public IMp7JrsSummarySegmentGroupType
{
	friend class Mp7JrsSummarySegmentGroupTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsSummarySegmentGroupType
	 
	 * @return Dc1Ptr< Mp7JrsDSType >
	 */
	virtual Dc1Ptr< Mp7JrsDSType > GetBase() const;
	/*
	 * Get level attribute.
	 * @return int
	 */
	virtual int Getlevel() const;

	/*
	 * Get level attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlevel() const;

	/*
	 * Set level attribute.
	 * @param item int
	 */
	virtual void Setlevel(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate level attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelevel();

	/*
	 * Get duration attribute.
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr Getduration() const;

	/*
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const;

	/*
	 * Set duration attribute.
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void Setduration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration();

	/*
	 * Get numOfKeyFrames attribute.
	 * @return unsigned
	 */
	virtual unsigned GetnumOfKeyFrames() const;

	/*
	 * Get numOfKeyFrames attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistnumOfKeyFrames() const;

	/*
	 * Set numOfKeyFrames attribute.
	 * @param item unsigned
	 */
	virtual void SetnumOfKeyFrames(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate numOfKeyFrames attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenumOfKeyFrames();

	/*
	 * Get fidelity attribute.
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getfidelity() const;

	/*
	 * Get fidelity attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfidelity() const;

	/*
	 * Set fidelity attribute.
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setfidelity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate fidelity attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefidelity();

	/*
	 * Get themeIDs attribute.
	 * @return W3CIDREFSPtr
	 */
	virtual W3CIDREFSPtr GetthemeIDs() const;

	/*
	 * Get themeIDs attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistthemeIDs() const;

	/*
	 * Set themeIDs attribute.
	 * @param item const W3CIDREFSPtr &
	 */
	virtual void SetthemeIDs(const W3CIDREFSPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate themeIDs attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatethemeIDs();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Name element.
	 * @return Mp7JrsSummarySegmentGroupType_Name_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Name_CollectionPtr GetName() const;

	/*
	 * Set Name element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetName(const Mp7JrsSummarySegmentGroupType_Name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Caption element.
	 * @return Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr GetCaption() const;

	/*
	 * Set Caption element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCaption(const Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SummarySegmentGroupType_LocalType element.
	 * @return Mp7JrsSummarySegmentGroupType_CollectionPtr
	 */
	virtual Mp7JrsSummarySegmentGroupType_CollectionPtr GetSummarySegmentGroupType_LocalType() const;

	/*
	 * Set SummarySegmentGroupType_LocalType element.
	 * @param item const Mp7JrsSummarySegmentGroupType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSummarySegmentGroupType_LocalType(const Mp7JrsSummarySegmentGroupType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SummarySegmentGroupType_LocalType0 element.
	 * @return Mp7JrsSummarySegmentGroupType_Collection0Ptr
	 */
	virtual Mp7JrsSummarySegmentGroupType_Collection0Ptr GetSummarySegmentGroupType_LocalType0() const;

	/*
	 * Set SummarySegmentGroupType_LocalType0 element.
	 * @param item const Mp7JrsSummarySegmentGroupType_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetSummarySegmentGroupType_LocalType0(const Mp7JrsSummarySegmentGroupType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of SummarySegmentGroupType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Name</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Caption</li>
	 * <li>SummarySegment</li>
	 * <li>SummarySegmentRef</li>
	 * <li>SummarySegmentGroup</li>
	 * <li>SummarySegmentGroupRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSummarySegmentGroupType();

protected:
	Mp7JrsSummarySegmentGroupType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_level;
	bool m_level_Exist;
	Mp7JrsmediaDurationPtr m_duration;
	bool m_duration_Exist;
	unsigned m_numOfKeyFrames;
	bool m_numOfKeyFrames_Exist;
	Mp7JrszeroToOnePtr m_fidelity;
	bool m_fidelity_Exist;
	W3CIDREFSPtr m_themeIDs;
	bool m_themeIDs_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsDSType > m_Base;

	Mp7JrsSummarySegmentGroupType_Name_CollectionPtr m_Name;
	Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr m_Caption;
	Mp7JrsSummarySegmentGroupType_CollectionPtr m_SummarySegmentGroupType_LocalType;
	Mp7JrsSummarySegmentGroupType_Collection0Ptr m_SummarySegmentGroupType_LocalType0;

// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3589MP7JRSSUMMARYSEGMENTGROUPTYPE_H

