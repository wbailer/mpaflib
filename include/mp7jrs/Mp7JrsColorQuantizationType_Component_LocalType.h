/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _1517MP7JRSCOLORQUANTIZATIONTYPE_COMPONENT_LOCALTYPE_H
#define _1517MP7JRSCOLORQUANTIZATIONTYPE_COMPONENT_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_Component_LocalType_ExtInclude.h




/**
 * Generated enumeration Mp7JrsColorQuantizationType_Component_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 423<br>
 * Enumerations: <br>
 * R (mapped to XML R) <br>
 * G (mapped to XML G) <br>
 * B (mapped to XML B) <br>
 * Y (mapped to XML Y) <br>
 * Cb (mapped to XML Cb) <br>
 * Cr (mapped to XML Cr) <br>
 * H (mapped to XML H) <br>
 * S (mapped to XML S) <br>
 * V (mapped to XML V) <br>
 * Max (mapped to XML Max) <br>
 * Min (mapped to XML Min) <br>
 * Diff (mapped to XML Diff) <br>
 * Sum (mapped to XML Sum) <br>
 */
class DC1_EXPORT Mp7JrsColorQuantizationType_Component_LocalType
{
public:
	/** Mp7JrsColorQuantizationType_Component_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		R, // Mapped to R
		G, // Mapped to G
		B, // Mapped to B
		Y, // Mapped to Y
		Cb, // Mapped to Cb
		Cr, // Mapped to Cr
		H, // Mapped to H
		S, // Mapped to S
		V, // Mapped to V
		Max, // Mapped to Max
		Min, // Mapped to Min
		Diff, // Mapped to Diff
		Sum, // Mapped to Sum
	};
	

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_Component_LocalType_ExtMethodDef.h


	static XMLCh * ToText(Mp7JrsColorQuantizationType_Component_LocalType::Enumeration item);
	static Mp7JrsColorQuantizationType_Component_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_Component_LocalType_ExtPropDef.h



};
#endif // _1517MP7JRSCOLORQUANTIZATIONTYPE_COMPONENT_LOCALTYPE_H
