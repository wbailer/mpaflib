/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _3191MP7JRSSEMANTICSTATETYPE_ATTRIBUTEVALUEPAIR_LOCALTYPE_H
#define _3191MP7JRSSEMANTICSTATETYPE_ATTRIBUTEVALUEPAIR_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsSemanticStateType_AttributeValuePair_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtInclude.h


class IMp7JrsSemanticStateType_AttributeValuePair_LocalType;
typedef Dc1Ptr< IMp7JrsSemanticStateType_AttributeValuePair_LocalType> Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr;
class IMp7JrsIntegerMatrixType;
typedef Dc1Ptr< IMp7JrsIntegerMatrixType > Mp7JrsIntegerMatrixPtr;
class IMp7JrsFloatMatrixType;
typedef Dc1Ptr< IMp7JrsFloatMatrixType > Mp7JrsFloatMatrixPtr;
class IMp7JrsTextualType;
typedef Dc1Ptr< IMp7JrsTextualType > Mp7JrsTextualPtr;
class IMp7JrsTextAnnotationType;
typedef Dc1Ptr< IMp7JrsTextAnnotationType > Mp7JrsTextAnnotationPtr;
class IMp7JrsControlledTermUseType;
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;
class IMp7JrsDType;
typedef Dc1Ptr< IMp7JrsDType > Mp7JrsDPtr;

/** 
 * Generated interface IMp7JrsSemanticStateType_AttributeValuePair_LocalType for class Mp7JrsSemanticStateType_AttributeValuePair_LocalType<br>
 */
class MP7JRS_EXPORT IMp7JrsSemanticStateType_AttributeValuePair_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsSemanticStateType_AttributeValuePair_LocalType();
	virtual ~IMp7JrsSemanticStateType_AttributeValuePair_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get BooleanValue element.
	 * @return bool
	 */
	virtual bool GetBooleanValue() const = 0;

	/**
	 * Set BooleanValue element.
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetBooleanValue(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get IntegerValue element.
	 * @return int
	 */
	virtual int GetIntegerValue() const = 0;

	/**
	 * Set IntegerValue element.
	 * @param item int
	 */
	// Mandatory			
	virtual void SetIntegerValue(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get FloatValue element.
	 * @return float
	 */
	virtual float GetFloatValue() const = 0;

	/**
	 * Set FloatValue element.
	 * @param item float
	 */
	// Mandatory			
	virtual void SetFloatValue(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get IntegerMatrixValue element.
	 * @return Mp7JrsIntegerMatrixPtr
	 */
	virtual Mp7JrsIntegerMatrixPtr GetIntegerMatrixValue() const = 0;

	/**
	 * Set IntegerMatrixValue element.
	 * @param item const Mp7JrsIntegerMatrixPtr &
	 */
	// Mandatory			
	virtual void SetIntegerMatrixValue(const Mp7JrsIntegerMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get FloatMatrixValue element.
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetFloatMatrixValue() const = 0;

	/**
	 * Set FloatMatrixValue element.
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	// Mandatory			
	virtual void SetFloatMatrixValue(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get TextValue element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetTextValue() const = 0;

	/**
	 * Set TextValue element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	// Mandatory			
	virtual void SetTextValue(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get TextAnnotationValue element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetTextAnnotationValue() const = 0;

	/**
	 * Set TextAnnotationValue element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	// Mandatory			
	virtual void SetTextAnnotationValue(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ControlledTermUseValue element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetControlledTermUseValue() const = 0;

	/**
	 * Set ControlledTermUseValue element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	// Mandatory			
	virtual void SetControlledTermUseValue(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get DescriptorValue element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptorValue() const = 0;

	/**
	 * Set DescriptorValue element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVisualSummaryComponentPtr
	virtual void SetDescriptorValue(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsSemanticStateType_AttributeValuePair_LocalType<br>
 * Defined in mpeg7-v4.xsd, line 8271.<br>
 */
class DC1_EXPORT Mp7JrsSemanticStateType_AttributeValuePair_LocalType :
		public IMp7JrsSemanticStateType_AttributeValuePair_LocalType
{
	friend class Mp7JrsSemanticStateType_AttributeValuePair_LocalTypeFactory; // constructs objects

public:
		/* @name Choice

		 */
		//@{
	/*
	 * Get BooleanValue element.
	 * @return bool
	 */
	virtual bool GetBooleanValue() const;

	/*
	 * Set BooleanValue element.
	 * @param item bool
	 */
	// Mandatory			
	virtual void SetBooleanValue(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get IntegerValue element.
	 * @return int
	 */
	virtual int GetIntegerValue() const;

	/*
	 * Set IntegerValue element.
	 * @param item int
	 */
	// Mandatory			
	virtual void SetIntegerValue(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get FloatValue element.
	 * @return float
	 */
	virtual float GetFloatValue() const;

	/*
	 * Set FloatValue element.
	 * @param item float
	 */
	// Mandatory			
	virtual void SetFloatValue(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get IntegerMatrixValue element.
	 * @return Mp7JrsIntegerMatrixPtr
	 */
	virtual Mp7JrsIntegerMatrixPtr GetIntegerMatrixValue() const;

	/*
	 * Set IntegerMatrixValue element.
	 * @param item const Mp7JrsIntegerMatrixPtr &
	 */
	// Mandatory			
	virtual void SetIntegerMatrixValue(const Mp7JrsIntegerMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get FloatMatrixValue element.
	 * @return Mp7JrsFloatMatrixPtr
	 */
	virtual Mp7JrsFloatMatrixPtr GetFloatMatrixValue() const;

	/*
	 * Set FloatMatrixValue element.
	 * @param item const Mp7JrsFloatMatrixPtr &
	 */
	// Mandatory			
	virtual void SetFloatMatrixValue(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TextValue element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetTextValue() const;

	/*
	 * Set TextValue element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	// Mandatory			
	virtual void SetTextValue(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get TextAnnotationValue element.
	 * @return Mp7JrsTextAnnotationPtr
	 */
	virtual Mp7JrsTextAnnotationPtr GetTextAnnotationValue() const;

	/*
	 * Set TextAnnotationValue element.
	 * @param item const Mp7JrsTextAnnotationPtr &
	 */
	// Mandatory			
	virtual void SetTextAnnotationValue(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ControlledTermUseValue element.
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetControlledTermUseValue() const;

	/*
	 * Set ControlledTermUseValue element.
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	// Mandatory			
	virtual void SetControlledTermUseValue(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get DescriptorValue element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptorValue() const;

	/*
	 * Set DescriptorValue element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVisualSummaryComponentPtr
	virtual void SetDescriptorValue(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsSemanticStateType_AttributeValuePair_LocalType();

protected:
	Mp7JrsSemanticStateType_AttributeValuePair_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


// DefinionChoice
	bool m_BooleanValue;
	bool m_BooleanValue_Valid; // For value types in unions
	int m_IntegerValue;
	bool m_IntegerValue_Valid; // For value types in unions
	float m_FloatValue;
	bool m_FloatValue_Valid; // For value types in unions
	Mp7JrsIntegerMatrixPtr m_IntegerMatrixValue;
	Mp7JrsFloatMatrixPtr m_FloatMatrixValue;
	Mp7JrsTextualPtr m_TextValue;
	Mp7JrsTextAnnotationPtr m_TextAnnotationValue;
	Mp7JrsControlledTermUsePtr m_ControlledTermUseValue;
	Dc1Ptr< Dc1Node > m_DescriptorValue;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _3191MP7JRSSEMANTICSTATETYPE_ATTRIBUTEVALUEPAIR_LOCALTYPE_H

