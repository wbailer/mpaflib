/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef _3150MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_AUDIODESCRIPTIONSCHEME_COLLECTIONTYPEFACTORY_H
#define _3150MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_AUDIODESCRIPTIONSCHEME_COLLECTIONTYPEFACTORY_H

#include "Dc1Defines.h"
#include "Dc1Node.h"
#include "Dc1Factory.h"
#include "Dc1Ptr.h"
#include "Dc1PtrTypes.h"

/*
 * Generated factory Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionTypeFactory<br>
 * for class Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType:<br>
 * Located at: mpeg7-v4.xsd, line 8147<br>
 * Abstract item type: AudioDSType<br>
 * Possible classes:<br>
 * \li AudioChordPatternDS (Class)<br>
 * \li AudioRhythmicPatternDS (Class)<br>
 * \li AudioSignalQualityType (Class)<br>
 * \li AudioSignatureType (Class)<br>
 * \li AudioTempoType (Class)<br>
 * \li EnhancedAudioSignatureType (Class)<br>
 * \li ErrorEventType (Class)<br>
 * \li HarmonicInstrumentTimbreType (Class)<br>
 * \li InstrumentTimbreType (Class)<br>
 * \li KeyType (Class)<br>
 * \li MelodyContourType (Class)<br>
 * \li MelodySequenceType (Class)<br>
 * \li MelodySequenceType_NoteArray_LocalType (Class)<br>
 * \li MelodyType (Class)<br>
 * \li PerceptualAttributeDSType (Class)<br>
 * \li PercussiveInstrumentTimbreType (Class)<br>
 * \li PitchProfileDSType (Class)<br>
 * \li SoundModelStatePathType (Class)<br>
 * \li SpokenContentLatticeType (Class)<br>
 */

class DC1_EXPORT Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionTypeFactory :
	public Dc1Factory
{
public:
	Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionTypeFactory();
	virtual ~Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionTypeFactory();

	virtual const char * GetTypeName() const;
	virtual Dc1NodePtr CreateObject();
};

#endif // _3150MP7JRSSEMANTICBASETYPE_MEDIAOCCURRENCE_AUDIODESCRIPTIONSCHEME_COLLECTIONTYPEFACTORY_H
