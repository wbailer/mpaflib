/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2587MP7JRSMPEG7_LOCALTYPE_H
#define _2587MP7JRSMPEG7_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsMpeg7_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtInclude.h


class IMp7JrsMpeg7Type;
typedef Dc1Ptr< IMp7JrsMpeg7Type > Mp7JrsMpeg7Ptr;
class IMp7JrsMpeg7_LocalType;
typedef Dc1Ptr< IMp7JrsMpeg7_LocalType> Mp7JrsMpeg7_LocalPtr;
class IMp7JrsMpeg7BaseType;
typedef Dc1Ptr< IMp7JrsMpeg7BaseType > Mp7JrsMpeg7BasePtr;
class IMp7JrsMpeg7_Description_CollectionType;
typedef Dc1Ptr< IMp7JrsMpeg7_Description_CollectionType > Mp7JrsMpeg7_Description_CollectionPtr;
#include "Mp7JrsMpeg7Type.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDescriptionProfileType;
typedef Dc1Ptr< IMp7JrsDescriptionProfileType > Mp7JrsDescriptionProfilePtr;
class IMp7JrsDescriptionMetadataType;
typedef Dc1Ptr< IMp7JrsDescriptionMetadataType > Mp7JrsDescriptionMetadataPtr;

/** 
 * Generated interface IMp7JrsMpeg7_LocalType for class Mp7JrsMpeg7_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 3444<br>
 * Classified: Class<br>
 * Derived from: Mpeg7Type (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsMpeg7_LocalType 
 :
		public IMp7JrsMpeg7Type
{
public:
	// TODO: make these protected?
	IMp7JrsMpeg7_LocalType();
	virtual ~IMp7JrsMpeg7_LocalType();
		/** @name Choice

		 */
		//@{
	/**
	 * Get DescriptionUnit element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptionUnit() const = 0;

	/**
	 * Set DescriptionUnit element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAcquaintancePtr
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAffectivePtr
	// Mp7JrsAffiliationPtr
	// Mp7JrsAgentObjectPtr
	// Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsAnalyticEditedVideoPtr
	// Mp7JrsAnalyticModelType_Semantics_LocalPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioChordPatternDSPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioRhythmicPatternDSPtr
	// Mp7JrsAudioSegmentMediaSourceDecompositionPtr
	// Mp7JrsAudioSegmentTemporalDecompositionPtr
	// Mp7JrsAudioSegmentPtr
	// Mp7JrsAudioSignalQualityPtr
	// Mp7JrsAudioSignaturePtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioTempoPtr
	// Mp7JrsAudioPtr
	// Mp7JrsAudioVisualRegionMediaSourceDecompositionPtr
	// Mp7JrsAudioVisualRegionSpatialDecompositionPtr
	// Mp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr
	// Mp7JrsAudioVisualRegionTemporalDecompositionPtr
	// Mp7JrsAudioVisualRegionPtr
	// Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr
	// Mp7JrsAudioVisualSegmentSpatialDecompositionPtr
	// Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsAudioVisualSegmentTemporalDecompositionPtr
	// Mp7JrsAudioVisualSegmentPtr
	// Mp7JrsAudioVisualPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsAvailabilityPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBinomialDistributionPtr
	// Mp7JrsBrowsingPreferencesPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsClassificationPreferencesPtr
	// Mp7JrsClassificationSchemeAliasPtr
	// Mp7JrsClassificationSchemePtr
	// Mp7JrsClassificationPtr
	// Mp7JrsClusterClassificationModelPtr
	// Mp7JrsClusterModelPtr
	// Mp7JrsCollectionModelPtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsCompositionShotEditingTemporalDecompositionPtr
	// Mp7JrsCompositionShotPtr
	// Mp7JrsCompositionTransitionPtr
	// Mp7JrsConceptCollectionPtr
	// Mp7JrsConceptPtr
	// Mp7JrsConfusionCountPtr
	// Mp7JrsContentCollectionPtr
	// Mp7JrsContinuousHiddenMarkovModelPtr
	// Mp7JrsContinuousUniformDistributionPtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCreationInformationPtr
	// Mp7JrsCreationPreferencesPtr
	// Mp7JrsCreationPreferencesType_Location_LocalPtr
	// Mp7JrsCreationPtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDescriptionMetadataPtr
	// Mp7JrsDescriptorCollectionPtr
	// Mp7JrsDescriptorModelPtr
	// Mp7JrsDiscreteHiddenMarkovModelPtr
	// Mp7JrsDiscreteUniformDistributionPtr
	// Mp7JrsDisseminationPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsDoublePullbackDefinitionPtr
	// Mp7JrsDoublePushoutDefinitionPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsEditedMovingRegionPtr
	// Mp7JrsEditedVideoEditingTemporalDecompositionPtr
	// Mp7JrsEditedVideoPtr
	// Mp7JrsEnhancedAudioSignaturePtr
	// Mp7JrsErrorEventPtr
	// Mp7JrsEventPtr
	// Mp7JrsExponentialDistributionPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsFilteringAndSearchPreferencesPtr
	// Mp7JrsFrequencyTreePtr
	// Mp7JrsFrequencyViewPtr
	// Mp7JrsGammaDistributionPtr
	// Mp7JrsGaussianDistributionPtr
	// Mp7JrsGaussianMixtureModelPtr
	// Mp7JrsGeneralizedGaussianDistributionPtr
	// Mp7JrsGeometricDistributionPtr
	// Mp7JrsGlobalTransitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsGraphicalClassificationSchemePtr
	// Mp7JrsGraphicalTermDefinitionPtr
	// Mp7JrsGraphPtr
	// Mp7JrsHandWritingRecogInformationPtr
	// Mp7JrsHandWritingRecogResultPtr
	// Mp7JrsHarmonicInstrumentTimbrePtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHierarchicalSummaryPtr
	// Mp7JrsHistogramProbabilityPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsHyperGeometricDistributionPtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsImageTextPtr
	// Mp7JrsImagePtr
	// Mp7JrsInkContentPtr
	// Mp7JrsInkMediaInformationPtr
	// Mp7JrsInkSegmentSpatialDecompositionPtr
	// Mp7JrsInkSegmentTemporalDecompositionPtr
	// Mp7JrsInkSegmentPtr
	// Mp7JrsInstrumentTimbrePtr
	// Mp7JrsInternalTransitionPtr
	// Mp7JrsIntraCompositionShotEditingTemporalDecompositionPtr
	// Mp7JrsIntraCompositionShotPtr
	// Mp7JrsKeyPtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLinguisticDocumentPtr
	// Mp7JrsLinguisticPtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsLognormalDistributionPtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaInformationPtr
	// Mp7JrsMediaInstancePtr
	// Mp7JrsMediaProfilePtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMelodyContourPtr
	// Mp7JrsMelodySequencePtr
	// Mp7JrsMelodySequenceType_NoteArray_LocalPtr
	// Mp7JrsMelodyPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMixedCollectionPtr
	// Mp7JrsModelStatePtr
	// Mp7JrsMorphismGraphPtr
	// Mp7JrsMosaicPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsMovingRegionFeaturePtr
	// Mp7JrsMovingRegionMediaSourceDecompositionPtr
	// Mp7JrsMovingRegionSpatialDecompositionPtr
	// Mp7JrsMovingRegionSpatioTemporalDecompositionPtr
	// Mp7JrsMovingRegionTemporalDecompositionPtr
	// Mp7JrsMovingRegionPtr
	// Mp7JrsMultimediaCollectionPtr
	// Mp7JrsMultimediaSegmentMediaSourceDecompositionPtr
	// Mp7JrsMultimediaSegmentPtr
	// Mp7JrsMultimediaPtr
	// Mp7JrsMultiResolutionPyramidPtr
	// Mp7JrsObjectPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsOrderingKeyPtr
	// Mp7JrsOrganizationPtr
	// Mp7JrsPackagePtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualAttributeDSPtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPercussiveInstrumentTimbrePtr
	// Mp7JrsPersonGroupPtr
	// Mp7JrsPersonPtr
	// Mp7JrsPhoneLexiconPtr
	// Mp7JrsPhoneticTranscriptionLexiconPtr
	// Mp7JrsPitchProfileDSPtr
	// Mp7JrsPlacePtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPoissonDistributionPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsProbabilityClassificationModelPtr
	// Mp7JrsProbabilityDistributionPtr
	// Mp7JrsProbabilityModelClassPtr
	// Mp7JrsPullbackDefinitionPtr
	// Mp7JrsPushoutDefinitionPtr
	// Mp7JrsQCItemResultPtr
	// Mp7JrsQCProfilePtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelatedMaterialPtr
	// Mp7JrsRelationPtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsResolutionViewPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsSegmentCollectionPtr
	// Mp7JrsSemanticPlacePtr
	// Mp7JrsSemanticStatePtr
	// Mp7JrsSemanticTimePtr
	// Mp7JrsSemanticPtr
	// Mp7JrsSentencesPtr
	// Mp7JrsSequentialSummaryPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsShotEditingTemporalDecompositionPtr
	// Mp7JrsShotPtr
	// Mp7JrsSignalPtr
	// Mp7JrsSilenceHeaderPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundClassificationModelPtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSoundModelStatePathPtr
	// Mp7JrsSoundModelPtr
	// Mp7JrsSourcePreferencesPtr
	// Mp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpaceFrequencyGraphPtr
	// Mp7JrsSpaceFrequencyViewPtr
	// Mp7JrsSpaceResolutionViewPtr
	// Mp7JrsSpaceTreePtr
	// Mp7JrsSpaceViewPtr
	// Mp7JrsSpatial2DCoordinateSystemPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpeakerInfoPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsSpokenContentHeaderPtr
	// Mp7JrsSpokenContentLatticePtr
	// Mp7JrsStateTransitionModelPtr
	// Mp7JrsStillRegion3DSpatialDecompositionPtr
	// Mp7JrsStillRegion3DPtr
	// Mp7JrsStillRegionFeaturePtr
	// Mp7JrsStillRegionSpatialDecompositionPtr
	// Mp7JrsStillRegionPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsStructuredCollectionPtr
	// Mp7JrsSubjectClassificationSchemePtr
	// Mp7JrsSubjectTermDefinitionPtr
	// Mp7JrsSubjectTermDefinitionType_Term_LocalPtr
	// Mp7JrsSummarizationPtr
	// Mp7JrsSummaryPreferencesPtr
	// Mp7JrsSummarySegmentGroupPtr
	// Mp7JrsSummarySegmentPtr
	// Mp7JrsSummaryThemeListPtr
	// Mp7JrsSyntacticConstituentPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTermDefinitionPtr
	// Mp7JrsTermDefinitionType_Term_LocalPtr
	// Mp7JrsTextDecompositionPtr
	// Mp7JrsTextSegmentPtr
	// Mp7JrsTextPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsUsageHistoryPtr
	// Mp7JrsUsageInformationPtr
	// Mp7JrsUsageRecordPtr
	// Mp7JrsUserActionHistoryPtr
	// Mp7JrsUserActionListPtr
	// Mp7JrsUserActionPtr
	// Mp7JrsUserPreferencesPtr
	// Mp7JrsUserProfilePtr
	// Mp7JrsVariationSetPtr
	// Mp7JrsVariationPtr
	// Mp7JrsVideoSegmentFeaturePtr
	// Mp7JrsVideoSegmentMediaSourceDecompositionPtr
	// Mp7JrsVideoSegmentSpatialDecompositionPtr
	// Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsVideoSegmentTemporalDecompositionPtr
	// Mp7JrsVideoSegmentPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVideoTextPtr
	// Mp7JrsVideoPtr
	// Mp7JrsVideoViewGraphPtr
	// Mp7JrsViewSetPtr
	// Mp7JrsVisualSummaryComponentPtr
	// Mp7JrsWordLexiconPtr
	virtual void SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Description element.
	 * @return Mp7JrsMpeg7_Description_CollectionPtr
	 */
	virtual Mp7JrsMpeg7_Description_CollectionPtr GetDescription() const = 0;

	/**
	 * Set Description element.
	 * @param item const Mp7JrsMpeg7_Description_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescription(const Mp7JrsMpeg7_Description_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get DescriptionProfile element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsDescriptionProfilePtr
	 */
	virtual Mp7JrsDescriptionProfilePtr GetDescriptionProfile() const = 0;

	virtual bool IsValidDescriptionProfile() const = 0;

	/**
	 * Set DescriptionProfile element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsDescriptionProfilePtr &
	 */
	virtual void SetDescriptionProfile(const Mp7JrsDescriptionProfilePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DescriptionProfile element.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescriptionProfile() = 0;

	/**
	 * Get DescriptionMetadata element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsDescriptionMetadataPtr
	 */
	virtual Mp7JrsDescriptionMetadataPtr GetDescriptionMetadata() const = 0;

	virtual bool IsValidDescriptionMetadata() const = 0;

	/**
	 * Set DescriptionMetadata element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsDescriptionMetadataPtr &
	 */
	virtual void SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate DescriptionMetadata element.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescriptionMetadata() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of Mpeg7_LocalType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>DescriptionProfile</li>
	 * <li>DescriptionMetadata</li>
	 * <li>DescriptionUnit</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Description</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsMpeg7_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 3444<br>
 * Classified: Class<br>
 * Derived from: Mpeg7Type (Class)<br>
 * Defined in mpeg7-v4.xsd, line 3444.<br>
 */
class DC1_EXPORT Mp7JrsMpeg7_LocalType :
		public IMp7JrsMpeg7_LocalType
{
	friend class Mp7JrsMpeg7_LocalTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsMpeg7_LocalType
	 
	 * @return Dc1Ptr< Mp7JrsMpeg7Type >
	 */
	virtual Dc1Ptr< Mp7JrsMpeg7Type > GetBase() const;
		/* @name Choice

		 */
		//@{
	/*
	 * Get DescriptionUnit element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptionUnit() const;

	/*
	 * Set DescriptionUnit element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAcquaintancePtr</li>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAffectivePtr</li>
	 * <li>IMp7JrsAffiliationPtr</li>
	 * <li>IMp7JrsAgentObjectPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAnalyticEditedVideoPtr</li>
	 * <li>IMp7JrsAnalyticModelType_Semantics_LocalPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioChordPatternDSPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioRhythmicPatternDSPtr</li>
	 * <li>IMp7JrsAudioSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioSegmentPtr</li>
	 * <li>IMp7JrsAudioSignalQualityPtr</li>
	 * <li>IMp7JrsAudioSignaturePtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioTempoPtr</li>
	 * <li>IMp7JrsAudioPtr</li>
	 * <li>IMp7JrsAudioVisualRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualRegionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsAudioVisualSegmentPtr</li>
	 * <li>IMp7JrsAudioVisualPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsAvailabilityPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBinomialDistributionPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsClassificationPreferencesPtr</li>
	 * <li>IMp7JrsClassificationSchemeAliasPtr</li>
	 * <li>IMp7JrsClassificationSchemePtr</li>
	 * <li>IMp7JrsClassificationPtr</li>
	 * <li>IMp7JrsClusterClassificationModelPtr</li>
	 * <li>IMp7JrsClusterModelPtr</li>
	 * <li>IMp7JrsCollectionModelPtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsCompositionShotPtr</li>
	 * <li>IMp7JrsCompositionTransitionPtr</li>
	 * <li>IMp7JrsConceptCollectionPtr</li>
	 * <li>IMp7JrsConceptPtr</li>
	 * <li>IMp7JrsConfusionCountPtr</li>
	 * <li>IMp7JrsContentCollectionPtr</li>
	 * <li>IMp7JrsContinuousHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsContinuousUniformDistributionPtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCreationInformationPtr</li>
	 * <li>IMp7JrsCreationPreferencesPtr</li>
	 * <li>IMp7JrsCreationPreferencesType_Location_LocalPtr</li>
	 * <li>IMp7JrsCreationPtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDescriptionMetadataPtr</li>
	 * <li>IMp7JrsDescriptorCollectionPtr</li>
	 * <li>IMp7JrsDescriptorModelPtr</li>
	 * <li>IMp7JrsDiscreteHiddenMarkovModelPtr</li>
	 * <li>IMp7JrsDiscreteUniformDistributionPtr</li>
	 * <li>IMp7JrsDisseminationPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsDoublePullbackDefinitionPtr</li>
	 * <li>IMp7JrsDoublePushoutDefinitionPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsEditedMovingRegionPtr</li>
	 * <li>IMp7JrsEditedVideoEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsEditedVideoPtr</li>
	 * <li>IMp7JrsEnhancedAudioSignaturePtr</li>
	 * <li>IMp7JrsErrorEventPtr</li>
	 * <li>IMp7JrsEventPtr</li>
	 * <li>IMp7JrsExponentialDistributionPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsFilteringAndSearchPreferencesPtr</li>
	 * <li>IMp7JrsFrequencyTreePtr</li>
	 * <li>IMp7JrsFrequencyViewPtr</li>
	 * <li>IMp7JrsGammaDistributionPtr</li>
	 * <li>IMp7JrsGaussianDistributionPtr</li>
	 * <li>IMp7JrsGaussianMixtureModelPtr</li>
	 * <li>IMp7JrsGeneralizedGaussianDistributionPtr</li>
	 * <li>IMp7JrsGeometricDistributionPtr</li>
	 * <li>IMp7JrsGlobalTransitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsGraphicalClassificationSchemePtr</li>
	 * <li>IMp7JrsGraphicalTermDefinitionPtr</li>
	 * <li>IMp7JrsGraphPtr</li>
	 * <li>IMp7JrsHandWritingRecogInformationPtr</li>
	 * <li>IMp7JrsHandWritingRecogResultPtr</li>
	 * <li>IMp7JrsHarmonicInstrumentTimbrePtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHierarchicalSummaryPtr</li>
	 * <li>IMp7JrsHistogramProbabilityPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsHyperGeometricDistributionPtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsImageTextPtr</li>
	 * <li>IMp7JrsImagePtr</li>
	 * <li>IMp7JrsInkContentPtr</li>
	 * <li>IMp7JrsInkMediaInformationPtr</li>
	 * <li>IMp7JrsInkSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsInkSegmentPtr</li>
	 * <li>IMp7JrsInstrumentTimbrePtr</li>
	 * <li>IMp7JrsInternalTransitionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsIntraCompositionShotPtr</li>
	 * <li>IMp7JrsKeyPtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLinguisticDocumentPtr</li>
	 * <li>IMp7JrsLinguisticPtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsLognormalDistributionPtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaInformationPtr</li>
	 * <li>IMp7JrsMediaInstancePtr</li>
	 * <li>IMp7JrsMediaProfilePtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMelodyContourPtr</li>
	 * <li>IMp7JrsMelodySequencePtr</li>
	 * <li>IMp7JrsMelodySequenceType_NoteArray_LocalPtr</li>
	 * <li>IMp7JrsMelodyPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMixedCollectionPtr</li>
	 * <li>IMp7JrsModelStatePtr</li>
	 * <li>IMp7JrsMorphismGraphPtr</li>
	 * <li>IMp7JrsMosaicPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsMovingRegionFeaturePtr</li>
	 * <li>IMp7JrsMovingRegionMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionTemporalDecompositionPtr</li>
	 * <li>IMp7JrsMovingRegionPtr</li>
	 * <li>IMp7JrsMultimediaCollectionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsMultimediaSegmentPtr</li>
	 * <li>IMp7JrsMultimediaPtr</li>
	 * <li>IMp7JrsMultiResolutionPyramidPtr</li>
	 * <li>IMp7JrsObjectPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsOrderingKeyPtr</li>
	 * <li>IMp7JrsOrganizationPtr</li>
	 * <li>IMp7JrsPackagePtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualAttributeDSPtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPercussiveInstrumentTimbrePtr</li>
	 * <li>IMp7JrsPersonGroupPtr</li>
	 * <li>IMp7JrsPersonPtr</li>
	 * <li>IMp7JrsPhoneLexiconPtr</li>
	 * <li>IMp7JrsPhoneticTranscriptionLexiconPtr</li>
	 * <li>IMp7JrsPitchProfileDSPtr</li>
	 * <li>IMp7JrsPlacePtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPoissonDistributionPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsProbabilityClassificationModelPtr</li>
	 * <li>IMp7JrsProbabilityDistributionPtr</li>
	 * <li>IMp7JrsProbabilityModelClassPtr</li>
	 * <li>IMp7JrsPullbackDefinitionPtr</li>
	 * <li>IMp7JrsPushoutDefinitionPtr</li>
	 * <li>IMp7JrsQCItemResultPtr</li>
	 * <li>IMp7JrsQCProfilePtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelatedMaterialPtr</li>
	 * <li>IMp7JrsRelationPtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsResolutionViewPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsSegmentCollectionPtr</li>
	 * <li>IMp7JrsSemanticPlacePtr</li>
	 * <li>IMp7JrsSemanticStatePtr</li>
	 * <li>IMp7JrsSemanticTimePtr</li>
	 * <li>IMp7JrsSemanticPtr</li>
	 * <li>IMp7JrsSentencesPtr</li>
	 * <li>IMp7JrsSequentialSummaryPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsShotEditingTemporalDecompositionPtr</li>
	 * <li>IMp7JrsShotPtr</li>
	 * <li>IMp7JrsSignalPtr</li>
	 * <li>IMp7JrsSilenceHeaderPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundClassificationModelPtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSoundModelStatePathPtr</li>
	 * <li>IMp7JrsSoundModelPtr</li>
	 * <li>IMp7JrsSourcePreferencesPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpaceFrequencyGraphPtr</li>
	 * <li>IMp7JrsSpaceFrequencyViewPtr</li>
	 * <li>IMp7JrsSpaceResolutionViewPtr</li>
	 * <li>IMp7JrsSpaceTreePtr</li>
	 * <li>IMp7JrsSpaceViewPtr</li>
	 * <li>IMp7JrsSpatial2DCoordinateSystemPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpeakerInfoPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsSpokenContentHeaderPtr</li>
	 * <li>IMp7JrsSpokenContentLatticePtr</li>
	 * <li>IMp7JrsStateTransitionModelPtr</li>
	 * <li>IMp7JrsStillRegion3DSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegion3DPtr</li>
	 * <li>IMp7JrsStillRegionFeaturePtr</li>
	 * <li>IMp7JrsStillRegionSpatialDecompositionPtr</li>
	 * <li>IMp7JrsStillRegionPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsStructuredCollectionPtr</li>
	 * <li>IMp7JrsSubjectClassificationSchemePtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionPtr</li>
	 * <li>IMp7JrsSubjectTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsSummarizationPtr</li>
	 * <li>IMp7JrsSummaryPreferencesPtr</li>
	 * <li>IMp7JrsSummarySegmentGroupPtr</li>
	 * <li>IMp7JrsSummarySegmentPtr</li>
	 * <li>IMp7JrsSummaryThemeListPtr</li>
	 * <li>IMp7JrsSyntacticConstituentPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTermDefinitionPtr</li>
	 * <li>IMp7JrsTermDefinitionType_Term_LocalPtr</li>
	 * <li>IMp7JrsTextDecompositionPtr</li>
	 * <li>IMp7JrsTextSegmentPtr</li>
	 * <li>IMp7JrsTextPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsUsageHistoryPtr</li>
	 * <li>IMp7JrsUsageInformationPtr</li>
	 * <li>IMp7JrsUsageRecordPtr</li>
	 * <li>IMp7JrsUserActionHistoryPtr</li>
	 * <li>IMp7JrsUserActionListPtr</li>
	 * <li>IMp7JrsUserActionPtr</li>
	 * <li>IMp7JrsUserPreferencesPtr</li>
	 * <li>IMp7JrsUserProfilePtr</li>
	 * <li>IMp7JrsVariationSetPtr</li>
	 * <li>IMp7JrsVariationPtr</li>
	 * <li>IMp7JrsVideoSegmentFeaturePtr</li>
	 * <li>IMp7JrsVideoSegmentMediaSourceDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatialDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentSpatioTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentTemporalDecompositionPtr</li>
	 * <li>IMp7JrsVideoSegmentPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVideoTextPtr</li>
	 * <li>IMp7JrsVideoPtr</li>
	 * <li>IMp7JrsVideoViewGraphPtr</li>
	 * <li>IMp7JrsViewSetPtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * <li>IMp7JrsWordLexiconPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAcquaintancePtr
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAffectivePtr
	// Mp7JrsAffiliationPtr
	// Mp7JrsAgentObjectPtr
	// Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsAnalyticEditedVideoPtr
	// Mp7JrsAnalyticModelType_Semantics_LocalPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioChordPatternDSPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioRhythmicPatternDSPtr
	// Mp7JrsAudioSegmentMediaSourceDecompositionPtr
	// Mp7JrsAudioSegmentTemporalDecompositionPtr
	// Mp7JrsAudioSegmentPtr
	// Mp7JrsAudioSignalQualityPtr
	// Mp7JrsAudioSignaturePtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioTempoPtr
	// Mp7JrsAudioPtr
	// Mp7JrsAudioVisualRegionMediaSourceDecompositionPtr
	// Mp7JrsAudioVisualRegionSpatialDecompositionPtr
	// Mp7JrsAudioVisualRegionSpatioTemporalDecompositionPtr
	// Mp7JrsAudioVisualRegionTemporalDecompositionPtr
	// Mp7JrsAudioVisualRegionPtr
	// Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr
	// Mp7JrsAudioVisualSegmentSpatialDecompositionPtr
	// Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsAudioVisualSegmentTemporalDecompositionPtr
	// Mp7JrsAudioVisualSegmentPtr
	// Mp7JrsAudioVisualPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsAvailabilityPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBinomialDistributionPtr
	// Mp7JrsBrowsingPreferencesPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsClassificationPreferencesPtr
	// Mp7JrsClassificationSchemeAliasPtr
	// Mp7JrsClassificationSchemePtr
	// Mp7JrsClassificationPtr
	// Mp7JrsClusterClassificationModelPtr
	// Mp7JrsClusterModelPtr
	// Mp7JrsCollectionModelPtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsCompositionShotEditingTemporalDecompositionPtr
	// Mp7JrsCompositionShotPtr
	// Mp7JrsCompositionTransitionPtr
	// Mp7JrsConceptCollectionPtr
	// Mp7JrsConceptPtr
	// Mp7JrsConfusionCountPtr
	// Mp7JrsContentCollectionPtr
	// Mp7JrsContinuousHiddenMarkovModelPtr
	// Mp7JrsContinuousUniformDistributionPtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCreationInformationPtr
	// Mp7JrsCreationPreferencesPtr
	// Mp7JrsCreationPreferencesType_Location_LocalPtr
	// Mp7JrsCreationPtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDescriptionMetadataPtr
	// Mp7JrsDescriptorCollectionPtr
	// Mp7JrsDescriptorModelPtr
	// Mp7JrsDiscreteHiddenMarkovModelPtr
	// Mp7JrsDiscreteUniformDistributionPtr
	// Mp7JrsDisseminationPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsDoublePullbackDefinitionPtr
	// Mp7JrsDoublePushoutDefinitionPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsEditedMovingRegionPtr
	// Mp7JrsEditedVideoEditingTemporalDecompositionPtr
	// Mp7JrsEditedVideoPtr
	// Mp7JrsEnhancedAudioSignaturePtr
	// Mp7JrsErrorEventPtr
	// Mp7JrsEventPtr
	// Mp7JrsExponentialDistributionPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsFilteringAndSearchPreferencesPtr
	// Mp7JrsFrequencyTreePtr
	// Mp7JrsFrequencyViewPtr
	// Mp7JrsGammaDistributionPtr
	// Mp7JrsGaussianDistributionPtr
	// Mp7JrsGaussianMixtureModelPtr
	// Mp7JrsGeneralizedGaussianDistributionPtr
	// Mp7JrsGeometricDistributionPtr
	// Mp7JrsGlobalTransitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsGraphicalClassificationSchemePtr
	// Mp7JrsGraphicalTermDefinitionPtr
	// Mp7JrsGraphPtr
	// Mp7JrsHandWritingRecogInformationPtr
	// Mp7JrsHandWritingRecogResultPtr
	// Mp7JrsHarmonicInstrumentTimbrePtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHierarchicalSummaryPtr
	// Mp7JrsHistogramProbabilityPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsHyperGeometricDistributionPtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsImageTextPtr
	// Mp7JrsImagePtr
	// Mp7JrsInkContentPtr
	// Mp7JrsInkMediaInformationPtr
	// Mp7JrsInkSegmentSpatialDecompositionPtr
	// Mp7JrsInkSegmentTemporalDecompositionPtr
	// Mp7JrsInkSegmentPtr
	// Mp7JrsInstrumentTimbrePtr
	// Mp7JrsInternalTransitionPtr
	// Mp7JrsIntraCompositionShotEditingTemporalDecompositionPtr
	// Mp7JrsIntraCompositionShotPtr
	// Mp7JrsKeyPtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLinguisticDocumentPtr
	// Mp7JrsLinguisticPtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsLognormalDistributionPtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaInformationPtr
	// Mp7JrsMediaInstancePtr
	// Mp7JrsMediaProfilePtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMelodyContourPtr
	// Mp7JrsMelodySequencePtr
	// Mp7JrsMelodySequenceType_NoteArray_LocalPtr
	// Mp7JrsMelodyPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMixedCollectionPtr
	// Mp7JrsModelStatePtr
	// Mp7JrsMorphismGraphPtr
	// Mp7JrsMosaicPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsMovingRegionFeaturePtr
	// Mp7JrsMovingRegionMediaSourceDecompositionPtr
	// Mp7JrsMovingRegionSpatialDecompositionPtr
	// Mp7JrsMovingRegionSpatioTemporalDecompositionPtr
	// Mp7JrsMovingRegionTemporalDecompositionPtr
	// Mp7JrsMovingRegionPtr
	// Mp7JrsMultimediaCollectionPtr
	// Mp7JrsMultimediaSegmentMediaSourceDecompositionPtr
	// Mp7JrsMultimediaSegmentPtr
	// Mp7JrsMultimediaPtr
	// Mp7JrsMultiResolutionPyramidPtr
	// Mp7JrsObjectPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsOrderingKeyPtr
	// Mp7JrsOrganizationPtr
	// Mp7JrsPackagePtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualAttributeDSPtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPercussiveInstrumentTimbrePtr
	// Mp7JrsPersonGroupPtr
	// Mp7JrsPersonPtr
	// Mp7JrsPhoneLexiconPtr
	// Mp7JrsPhoneticTranscriptionLexiconPtr
	// Mp7JrsPitchProfileDSPtr
	// Mp7JrsPlacePtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPoissonDistributionPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsProbabilityClassificationModelPtr
	// Mp7JrsProbabilityDistributionPtr
	// Mp7JrsProbabilityModelClassPtr
	// Mp7JrsPullbackDefinitionPtr
	// Mp7JrsPushoutDefinitionPtr
	// Mp7JrsQCItemResultPtr
	// Mp7JrsQCProfilePtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelatedMaterialPtr
	// Mp7JrsRelationPtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsResolutionViewPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsSegmentCollectionPtr
	// Mp7JrsSemanticPlacePtr
	// Mp7JrsSemanticStatePtr
	// Mp7JrsSemanticTimePtr
	// Mp7JrsSemanticPtr
	// Mp7JrsSentencesPtr
	// Mp7JrsSequentialSummaryPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsShotEditingTemporalDecompositionPtr
	// Mp7JrsShotPtr
	// Mp7JrsSignalPtr
	// Mp7JrsSilenceHeaderPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundClassificationModelPtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSoundModelStatePathPtr
	// Mp7JrsSoundModelPtr
	// Mp7JrsSourcePreferencesPtr
	// Mp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpaceFrequencyGraphPtr
	// Mp7JrsSpaceFrequencyViewPtr
	// Mp7JrsSpaceResolutionViewPtr
	// Mp7JrsSpaceTreePtr
	// Mp7JrsSpaceViewPtr
	// Mp7JrsSpatial2DCoordinateSystemPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpeakerInfoPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsSpokenContentHeaderPtr
	// Mp7JrsSpokenContentLatticePtr
	// Mp7JrsStateTransitionModelPtr
	// Mp7JrsStillRegion3DSpatialDecompositionPtr
	// Mp7JrsStillRegion3DPtr
	// Mp7JrsStillRegionFeaturePtr
	// Mp7JrsStillRegionSpatialDecompositionPtr
	// Mp7JrsStillRegionPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsStructuredCollectionPtr
	// Mp7JrsSubjectClassificationSchemePtr
	// Mp7JrsSubjectTermDefinitionPtr
	// Mp7JrsSubjectTermDefinitionType_Term_LocalPtr
	// Mp7JrsSummarizationPtr
	// Mp7JrsSummaryPreferencesPtr
	// Mp7JrsSummarySegmentGroupPtr
	// Mp7JrsSummarySegmentPtr
	// Mp7JrsSummaryThemeListPtr
	// Mp7JrsSyntacticConstituentPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTermDefinitionPtr
	// Mp7JrsTermDefinitionType_Term_LocalPtr
	// Mp7JrsTextDecompositionPtr
	// Mp7JrsTextSegmentPtr
	// Mp7JrsTextPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsUsageHistoryPtr
	// Mp7JrsUsageInformationPtr
	// Mp7JrsUsageRecordPtr
	// Mp7JrsUserActionHistoryPtr
	// Mp7JrsUserActionListPtr
	// Mp7JrsUserActionPtr
	// Mp7JrsUserPreferencesPtr
	// Mp7JrsUserProfilePtr
	// Mp7JrsVariationSetPtr
	// Mp7JrsVariationPtr
	// Mp7JrsVideoSegmentFeaturePtr
	// Mp7JrsVideoSegmentMediaSourceDecompositionPtr
	// Mp7JrsVideoSegmentSpatialDecompositionPtr
	// Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr
	// Mp7JrsVideoSegmentTemporalDecompositionPtr
	// Mp7JrsVideoSegmentPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVideoTextPtr
	// Mp7JrsVideoPtr
	// Mp7JrsVideoViewGraphPtr
	// Mp7JrsViewSetPtr
	// Mp7JrsVisualSummaryComponentPtr
	// Mp7JrsWordLexiconPtr
	virtual void SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Description element.
	 * @return Mp7JrsMpeg7_Description_CollectionPtr
	 */
	virtual Mp7JrsMpeg7_Description_CollectionPtr GetDescription() const;

	/*
	 * Set Description element.
	 * @param item const Mp7JrsMpeg7_Description_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetDescription(const Mp7JrsMpeg7_Description_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get DescriptionProfile element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsDescriptionProfilePtr
	 */
	virtual Mp7JrsDescriptionProfilePtr GetDescriptionProfile() const;

	virtual bool IsValidDescriptionProfile() const;

	/*
	 * Set DescriptionProfile element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsDescriptionProfilePtr &
	 */
	virtual void SetDescriptionProfile(const Mp7JrsDescriptionProfilePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DescriptionProfile element.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescriptionProfile();

	/*
	 * Get DescriptionMetadata element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @return Mp7JrsDescriptionMetadataPtr
	 */
	virtual Mp7JrsDescriptionMetadataPtr GetDescriptionMetadata() const;

	virtual bool IsValidDescriptionMetadata() const;

	/*
	 * Set DescriptionMetadata element.
<br>
	 * (Inherited from Mp7JrsMpeg7Type)
	 * @param item const Mp7JrsDescriptionMetadataPtr &
	 */
	virtual void SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate DescriptionMetadata element.
	 * (Inherited from Mp7JrsMpeg7Type)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateDescriptionMetadata();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of Mpeg7_LocalType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>DescriptionProfile</li>
	 * <li>DescriptionMetadata</li>
	 * <li>DescriptionUnit</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Description</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsMpeg7_LocalType();

protected:
	Mp7JrsMpeg7_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsMpeg7Type > m_Base;

// DefinionChoice
	Dc1Ptr< Dc1Node > m_DescriptionUnit;
	Mp7JrsMpeg7_Description_CollectionPtr m_Description;
// End DefinionChoice

// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2587MP7JRSMPEG7_LOCALTYPE_H

