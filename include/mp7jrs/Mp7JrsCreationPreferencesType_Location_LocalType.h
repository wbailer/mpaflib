/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1677MP7JRSCREATIONPREFERENCESTYPE_LOCATION_LOCALTYPE_H
#define _1677MP7JRSCREATIONPREFERENCESTYPE_LOCATION_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsCreationPreferencesType_Location_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtInclude.h


class IMp7JrsPlaceType;
typedef Dc1Ptr< IMp7JrsPlaceType > Mp7JrsPlacePtr;
class IMp7JrsCreationPreferencesType_Location_LocalType;
typedef Dc1Ptr< IMp7JrsCreationPreferencesType_Location_LocalType> Mp7JrsCreationPreferencesType_Location_LocalPtr;
class IMp7JrspreferenceValueType;
typedef Dc1Ptr< IMp7JrspreferenceValueType > Mp7JrspreferenceValuePtr;
#include "Mp7JrsPlaceType.h"
class IMp7JrsPlaceType_Name_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_Name_CollectionType > Mp7JrsPlaceType_Name_CollectionPtr;
class IMp7JrsPlaceType_NameTerm_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_NameTerm_CollectionType > Mp7JrsPlaceType_NameTerm_CollectionPtr;
class IMp7JrsPlaceType_PlaceDescription_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_PlaceDescription_CollectionType > Mp7JrsPlaceType_PlaceDescription_CollectionPtr;
class IMp7JrsTermUseType;
typedef Dc1Ptr< IMp7JrsTermUseType > Mp7JrsTermUsePtr;
class IMp7JrsPlaceType_GeographicPosition_LocalType;
typedef Dc1Ptr< IMp7JrsPlaceType_GeographicPosition_LocalType > Mp7JrsPlaceType_GeographicPosition_LocalPtr;
class IMp7JrsPlaceType_AstronomicalBody_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_AstronomicalBody_CollectionType > Mp7JrsPlaceType_AstronomicalBody_CollectionPtr;
class IMp7JrsPlaceType_Region_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_Region_CollectionType > Mp7JrsPlaceType_Region_CollectionPtr;
class IMp7JrsPlaceType_AdministrativeUnit_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_AdministrativeUnit_CollectionType > Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr;
class IMp7JrsPlaceType_PostalAddress_LocalType;
typedef Dc1Ptr< IMp7JrsPlaceType_PostalAddress_LocalType > Mp7JrsPlaceType_PostalAddress_LocalPtr;
class IMp7JrsPlaceType_StructuredPostalAddress_LocalType;
typedef Dc1Ptr< IMp7JrsPlaceType_StructuredPostalAddress_LocalType > Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr;
class IMp7JrsPlaceType_StructuredInternalCoordinates_LocalType;
typedef Dc1Ptr< IMp7JrsPlaceType_StructuredInternalCoordinates_LocalType > Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr;
class IMp7JrsPlaceType_ElectronicAddress_CollectionType;
typedef Dc1Ptr< IMp7JrsPlaceType_ElectronicAddress_CollectionType > Mp7JrsPlaceType_ElectronicAddress_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsCreationPreferencesType_Location_LocalType for class Mp7JrsCreationPreferencesType_Location_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 9839<br>
 * Classified: Class<br>
 * Derived from: PlaceType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsCreationPreferencesType_Location_LocalType 
 :
		public IMp7JrsPlaceType
{
public:
	// TODO: make these protected?
	IMp7JrsCreationPreferencesType_Location_LocalType();
	virtual ~IMp7JrsCreationPreferencesType_Location_LocalType();
	/**
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const = 0;

	/**
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const = 0;

	/**
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue() = 0;

	/**
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsPlaceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Name element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_Name_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_Name_CollectionPtr GetName() const = 0;

	/**
	 * Set Name element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_Name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetName(const Mp7JrsPlaceType_Name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get NameTerm element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_NameTerm_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_NameTerm_CollectionPtr GetNameTerm() const = 0;

	/**
	 * Set NameTerm element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_NameTerm_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetNameTerm(const Mp7JrsPlaceType_NameTerm_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PlaceDescription element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_PlaceDescription_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_PlaceDescription_CollectionPtr GetPlaceDescription() const = 0;

	/**
	 * Set PlaceDescription element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_PlaceDescription_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPlaceDescription(const Mp7JrsPlaceType_PlaceDescription_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Role element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetRole() const = 0;

	virtual bool IsValidRole() const = 0;

	/**
	 * Set Role element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetRole(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Role element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRole() = 0;

	/**
	 * Get GeographicPosition element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_GeographicPosition_LocalPtr
	 */
	virtual Mp7JrsPlaceType_GeographicPosition_LocalPtr GetGeographicPosition() const = 0;

	virtual bool IsValidGeographicPosition() const = 0;

	/**
	 * Set GeographicPosition element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_GeographicPosition_LocalPtr &
	 */
	virtual void SetGeographicPosition(const Mp7JrsPlaceType_GeographicPosition_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate GeographicPosition element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateGeographicPosition() = 0;

	/**
	 * Get AstronomicalBody element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_AstronomicalBody_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_AstronomicalBody_CollectionPtr GetAstronomicalBody() const = 0;

	/**
	 * Set AstronomicalBody element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAstronomicalBody(const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Region element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_Region_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_Region_CollectionPtr GetRegion() const = 0;

	/**
	 * Set Region element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_Region_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRegion(const Mp7JrsPlaceType_Region_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get AdministrativeUnit element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr GetAdministrativeUnit() const = 0;

	/**
	 * Set AdministrativeUnit element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAdministrativeUnit(const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get PostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_PostalAddress_LocalPtr
	 */
	virtual Mp7JrsPlaceType_PostalAddress_LocalPtr GetPostalAddress() const = 0;

	virtual bool IsValidPostalAddress() const = 0;

	/**
	 * Set PostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_PostalAddress_LocalPtr &
	 */
	virtual void SetPostalAddress(const Mp7JrsPlaceType_PostalAddress_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate PostalAddress element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePostalAddress() = 0;

	/**
	 * Get StructuredPostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr
	 */
	virtual Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr GetStructuredPostalAddress() const = 0;

	virtual bool IsValidStructuredPostalAddress() const = 0;

	/**
	 * Set StructuredPostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr &
	 */
	virtual void SetStructuredPostalAddress(const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate StructuredPostalAddress element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuredPostalAddress() = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get InternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetInternalCoordinates() const = 0;

	virtual bool IsValidInternalCoordinates() const = 0;

	/**
	 * Set InternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item XMLCh *
	 */
	virtual void SetInternalCoordinates(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate InternalCoordinates element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateInternalCoordinates() = 0;

	/**
	 * Get StructuredInternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr
	 */
	virtual Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr GetStructuredInternalCoordinates() const = 0;

	virtual bool IsValidStructuredInternalCoordinates() const = 0;

	/**
	 * Set StructuredInternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr &
	 */
	virtual void SetStructuredInternalCoordinates(const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate StructuredInternalCoordinates element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuredInternalCoordinates() = 0;

		//@}
	/**
	 * Get ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_ElectronicAddress_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_ElectronicAddress_CollectionPtr GetElectronicAddress() const = 0;

	/**
	 * Set ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetElectronicAddress(const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of CreationPreferencesType_Location_LocalType.
	 * Currently this type contains 13 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Name</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:NameTerm</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PlaceDescription</li>
	 * <li>Role</li>
	 * <li>GeographicPosition</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AstronomicalBody</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Region</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AdministrativeUnit</li>
	 * <li>PostalAddress</li>
	 * <li>StructuredPostalAddress</li>
	 * <li>StructuredInternalCoordinates</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:ElectronicAddress</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsCreationPreferencesType_Location_LocalType<br>
 * Located at: mpeg7-v4.xsd, line 9839<br>
 * Classified: Class<br>
 * Derived from: PlaceType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 9839.<br>
 */
class DC1_EXPORT Mp7JrsCreationPreferencesType_Location_LocalType :
		public IMp7JrsCreationPreferencesType_Location_LocalType
{
	friend class Mp7JrsCreationPreferencesType_Location_LocalTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsCreationPreferencesType_Location_LocalType
	 
	 * @return Dc1Ptr< Mp7JrsPlaceType >
	 */
	virtual Dc1Ptr< Mp7JrsPlaceType > GetBase() const;
	/*
	 * Get preferenceValue attribute.
	 * @return Mp7JrspreferenceValuePtr
	 */
	virtual Mp7JrspreferenceValuePtr GetpreferenceValue() const;

	/*
	 * Get preferenceValue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpreferenceValue() const;

	/*
	 * Set preferenceValue attribute.
	 * @param item const Mp7JrspreferenceValuePtr &
	 */
	virtual void SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate preferenceValue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepreferenceValue();

	/*
	 * Get lang attribute.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * (Inherited from Mp7JrsPlaceType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Name element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_Name_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_Name_CollectionPtr GetName() const;

	/*
	 * Set Name element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_Name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetName(const Mp7JrsPlaceType_Name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get NameTerm element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_NameTerm_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_NameTerm_CollectionPtr GetNameTerm() const;

	/*
	 * Set NameTerm element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_NameTerm_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetNameTerm(const Mp7JrsPlaceType_NameTerm_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PlaceDescription element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_PlaceDescription_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_PlaceDescription_CollectionPtr GetPlaceDescription() const;

	/*
	 * Set PlaceDescription element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_PlaceDescription_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPlaceDescription(const Mp7JrsPlaceType_PlaceDescription_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Role element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsTermUsePtr
	 */
	virtual Mp7JrsTermUsePtr GetRole() const;

	virtual bool IsValidRole() const;

	/*
	 * Set Role element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsTermUsePtr &
	 */
	virtual void SetRole(const Mp7JrsTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Role element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateRole();

	/*
	 * Get GeographicPosition element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_GeographicPosition_LocalPtr
	 */
	virtual Mp7JrsPlaceType_GeographicPosition_LocalPtr GetGeographicPosition() const;

	virtual bool IsValidGeographicPosition() const;

	/*
	 * Set GeographicPosition element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_GeographicPosition_LocalPtr &
	 */
	virtual void SetGeographicPosition(const Mp7JrsPlaceType_GeographicPosition_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate GeographicPosition element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateGeographicPosition();

	/*
	 * Get AstronomicalBody element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_AstronomicalBody_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_AstronomicalBody_CollectionPtr GetAstronomicalBody() const;

	/*
	 * Set AstronomicalBody element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAstronomicalBody(const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Region element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_Region_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_Region_CollectionPtr GetRegion() const;

	/*
	 * Set Region element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_Region_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRegion(const Mp7JrsPlaceType_Region_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get AdministrativeUnit element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr GetAdministrativeUnit() const;

	/*
	 * Set AdministrativeUnit element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetAdministrativeUnit(const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Choice

		 */
		//@{
	/*
	 * Get PostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_PostalAddress_LocalPtr
	 */
	virtual Mp7JrsPlaceType_PostalAddress_LocalPtr GetPostalAddress() const;

	virtual bool IsValidPostalAddress() const;

	/*
	 * Set PostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_PostalAddress_LocalPtr &
	 */
	virtual void SetPostalAddress(const Mp7JrsPlaceType_PostalAddress_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate PostalAddress element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatePostalAddress();

	/*
	 * Get StructuredPostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr
	 */
	virtual Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr GetStructuredPostalAddress() const;

	virtual bool IsValidStructuredPostalAddress() const;

	/*
	 * Set StructuredPostalAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr &
	 */
	virtual void SetStructuredPostalAddress(const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StructuredPostalAddress element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuredPostalAddress();

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get InternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetInternalCoordinates() const;

	virtual bool IsValidInternalCoordinates() const;

	/*
	 * Set InternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item XMLCh *
	 */
	virtual void SetInternalCoordinates(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate InternalCoordinates element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateInternalCoordinates();

	/*
	 * Get StructuredInternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr
	 */
	virtual Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr GetStructuredInternalCoordinates() const;

	virtual bool IsValidStructuredInternalCoordinates() const;

	/*
	 * Set StructuredInternalCoordinates element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr &
	 */
	virtual void SetStructuredInternalCoordinates(const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StructuredInternalCoordinates element.
	 * (Inherited from Mp7JrsPlaceType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuredInternalCoordinates();

		//@}
	/*
	 * Get ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @return Mp7JrsPlaceType_ElectronicAddress_CollectionPtr
	 */
	virtual Mp7JrsPlaceType_ElectronicAddress_CollectionPtr GetElectronicAddress() const;

	/*
	 * Set ElectronicAddress element.
<br>
	 * (Inherited from Mp7JrsPlaceType)
	 * @param item const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetElectronicAddress(const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of CreationPreferencesType_Location_LocalType.
	 * Currently this type contains 13 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Name</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:NameTerm</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PlaceDescription</li>
	 * <li>Role</li>
	 * <li>GeographicPosition</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AstronomicalBody</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Region</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:AdministrativeUnit</li>
	 * <li>PostalAddress</li>
	 * <li>StructuredPostalAddress</li>
	 * <li>StructuredInternalCoordinates</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:ElectronicAddress</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsCreationPreferencesType_Location_LocalType();

protected:
	Mp7JrsCreationPreferencesType_Location_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrspreferenceValuePtr m_preferenceValue;
	bool m_preferenceValue_Exist;
	Mp7JrspreferenceValuePtr m_preferenceValue_Default;

	// Base Class
	Dc1Ptr< Mp7JrsPlaceType > m_Base;


// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1677MP7JRSCREATIONPREFERENCESTYPE_LOCATION_LOCALTYPE_H

