/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _4001MP7JRSVIDEOTEXTTYPE_H
#define _4001MP7JRSVIDEOTEXTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsVideoTextTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsVideoTextType_ExtInclude.h


class IMp7JrsMovingRegionType;
typedef Dc1Ptr< IMp7JrsMovingRegionType > Mp7JrsMovingRegionPtr;
class IMp7JrsVideoTextType;
typedef Dc1Ptr< IMp7JrsVideoTextType> Mp7JrsVideoTextPtr;
class IMp7JrsVideoTextType_textType_LocalType2;
typedef Dc1Ptr< IMp7JrsVideoTextType_textType_LocalType2 > Mp7JrsVideoTextType_textType_Local2Ptr;
class IMp7JrsTextualType;
typedef Dc1Ptr< IMp7JrsTextualType > Mp7JrsTextualPtr;
#include "Mp7JrsMovingRegionType.h"
class IMp7JrsSpatioTemporalLocatorType;
typedef Dc1Ptr< IMp7JrsSpatioTemporalLocatorType > Mp7JrsSpatioTemporalLocatorPtr;
class IMp7JrsSpatioTemporalMaskType;
typedef Dc1Ptr< IMp7JrsSpatioTemporalMaskType > Mp7JrsSpatioTemporalMaskPtr;
class IMp7JrsMovingRegionType_CollectionType;
typedef Dc1Ptr< IMp7JrsMovingRegionType_CollectionType > Mp7JrsMovingRegionType_CollectionPtr;
class IMp7JrsMultipleViewType;
typedef Dc1Ptr< IMp7JrsMultipleViewType > Mp7JrsMultipleViewPtr;
class IMp7JrsMovingRegionType_CollectionType0;
typedef Dc1Ptr< IMp7JrsMovingRegionType_CollectionType0 > Mp7JrsMovingRegionType_Collection0Ptr;
#include "Mp7JrsSegmentType.h"
class IMp7JrsMediaInformationType;
typedef Dc1Ptr< IMp7JrsMediaInformationType > Mp7JrsMediaInformationPtr;
class IMp7JrsReferenceType;
typedef Dc1Ptr< IMp7JrsReferenceType > Mp7JrsReferencePtr;
class IMp7JrsMediaLocatorType;
typedef Dc1Ptr< IMp7JrsMediaLocatorType > Mp7JrsMediaLocatorPtr;
class IMp7JrsControlledTermUseType;
typedef Dc1Ptr< IMp7JrsControlledTermUseType > Mp7JrsControlledTermUsePtr;
class IMp7JrsCreationInformationType;
typedef Dc1Ptr< IMp7JrsCreationInformationType > Mp7JrsCreationInformationPtr;
class IMp7JrsUsageInformationType;
typedef Dc1Ptr< IMp7JrsUsageInformationType > Mp7JrsUsageInformationPtr;
class IMp7JrsSegmentType_TextAnnotation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_TextAnnotation_CollectionType > Mp7JrsSegmentType_TextAnnotation_CollectionPtr;
class IMp7JrsSegmentType_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_CollectionType > Mp7JrsSegmentType_CollectionPtr;
class IMp7JrsSegmentType_MatchingHint_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_MatchingHint_CollectionType > Mp7JrsSegmentType_MatchingHint_CollectionPtr;
class IMp7JrsSegmentType_PointOfView_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_PointOfView_CollectionType > Mp7JrsSegmentType_PointOfView_CollectionPtr;
class IMp7JrsSegmentType_Relation_CollectionType;
typedef Dc1Ptr< IMp7JrsSegmentType_Relation_CollectionType > Mp7JrsSegmentType_Relation_CollectionPtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsVideoTextType for class Mp7JrsVideoTextType<br>
 * Located at: mpeg7-v4.xsd, line 7341<br>
 * Classified: Class<br>
 * Derived from: MovingRegionType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsVideoTextType 
 :
		public IMp7JrsMovingRegionType
{
public:
	// TODO: make these protected?
	IMp7JrsVideoTextType();
	virtual ~IMp7JrsVideoTextType();
	/**
	 * Get textType attribute.
	 * @return Mp7JrsVideoTextType_textType_Local2Ptr
	 */
	virtual Mp7JrsVideoTextType_textType_Local2Ptr GettextType() const = 0;

	/**
	 * Get textType attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttextType() const = 0;

	/**
	 * Set textType attribute.
	 * @param item const Mp7JrsVideoTextType_textType_Local2Ptr &
	 */
	virtual void SettextType(const Mp7JrsVideoTextType_textType_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate textType attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetextType() = 0;

	/**
	 * Get fontSize attribute.
	 * @return unsigned
	 */
	virtual unsigned GetfontSize() const = 0;

	/**
	 * Get fontSize attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfontSize() const = 0;

	/**
	 * Set fontSize attribute.
	 * @param item unsigned
	 */
	virtual void SetfontSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate fontSize attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefontSize() = 0;

	/**
	 * Get fontType attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfontType() const = 0;

	/**
	 * Get fontType attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfontType() const = 0;

	/**
	 * Set fontType attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfontType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate fontType attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefontType() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Text element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetText() const = 0;

	virtual bool IsValidText() const = 0;

	/**
	 * Set Text element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetText(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Text element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateText() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get SpatioTemporalLocator element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsSpatioTemporalLocatorPtr
	 */
	virtual Mp7JrsSpatioTemporalLocatorPtr GetSpatioTemporalLocator() const = 0;

	/**
	 * Set SpatioTemporalLocator element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsSpatioTemporalLocatorPtr &
	 */
	virtual void SetSpatioTemporalLocator(const Mp7JrsSpatioTemporalLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SpatioTemporalMask element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsSpatioTemporalMaskPtr
	 */
	virtual Mp7JrsSpatioTemporalMaskPtr GetSpatioTemporalMask() const = 0;

	/**
	 * Set SpatioTemporalMask element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsSpatioTemporalMaskPtr &
	 */
	virtual void SetSpatioTemporalMask(const Mp7JrsSpatioTemporalMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get MovingRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMovingRegionType_CollectionPtr
	 */
	virtual Mp7JrsMovingRegionType_CollectionPtr GetMovingRegionType_LocalType() const = 0;

	/**
	 * Set MovingRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMovingRegionType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMovingRegionType_LocalType(const Mp7JrsMovingRegionType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMultipleViewPtr
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const = 0;

	virtual bool IsValidMultipleView() const = 0;

	/**
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsMovingRegionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMultipleView() = 0;

	/**
	 * Get MovingRegionType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMovingRegionType_Collection0Ptr
	 */
	virtual Mp7JrsMovingRegionType_Collection0Ptr GetMovingRegionType_LocalType0() const = 0;

	/**
	 * Set MovingRegionType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMovingRegionType_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetMovingRegionType_LocalType0(const Mp7JrsMovingRegionType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const = 0;

	/**
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const = 0;

	/**
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const = 0;

	/**
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const = 0;

	virtual bool IsValidStructuralUnit() const = 0;

	/**
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuralUnit() = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const = 0;

	/**
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const = 0;

	/**
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		/** @name Choice

		 */
		//@{
	/**
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const = 0;

	/**
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const = 0;

	/**
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const = 0;

	/**
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const = 0;

	/**
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const = 0;

	/**
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const = 0;

	/**
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const = 0;

	/**
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of VideoTextType.
	 * Currently this type contains 27 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>SpatioTemporalLocator</li>
	 * <li>SpatioTemporalMask</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>VisualTimeSeriesDescriptor</li>
	 * <li>GofGopFeature</li>
	 * <li>MultipleView</li>
	 * <li>SpatialDecomposition</li>
	 * <li>TemporalDecomposition</li>
	 * <li>SpatioTemporalDecomposition</li>
	 * <li>MediaSourceDecomposition</li>
	 * <li>Text</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsVideoTextType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsVideoTextType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsVideoTextType<br>
 * Located at: mpeg7-v4.xsd, line 7341<br>
 * Classified: Class<br>
 * Derived from: MovingRegionType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 7341.<br>
 */
class DC1_EXPORT Mp7JrsVideoTextType :
		public IMp7JrsVideoTextType
{
	friend class Mp7JrsVideoTextTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsVideoTextType
	 
	 * @return Dc1Ptr< Mp7JrsMovingRegionType >
	 */
	virtual Dc1Ptr< Mp7JrsMovingRegionType > GetBase() const;
	/*
	 * Get textType attribute.
	 * @return Mp7JrsVideoTextType_textType_Local2Ptr
	 */
	virtual Mp7JrsVideoTextType_textType_Local2Ptr GettextType() const;

	/*
	 * Get textType attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttextType() const;

	/*
	 * Set textType attribute.
	 * @param item const Mp7JrsVideoTextType_textType_Local2Ptr &
	 */
	virtual void SettextType(const Mp7JrsVideoTextType_textType_Local2Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate textType attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetextType();

	/*
	 * Get fontSize attribute.
	 * @return unsigned
	 */
	virtual unsigned GetfontSize() const;

	/*
	 * Get fontSize attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfontSize() const;

	/*
	 * Set fontSize attribute.
	 * @param item unsigned
	 */
	virtual void SetfontSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate fontSize attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefontSize();

	/*
	 * Get fontType attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetfontType() const;

	/*
	 * Get fontType attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfontType() const;

	/*
	 * Set fontType attribute.
	 * @param item XMLCh *
	 */
	virtual void SetfontType(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate fontType attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefontType();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Text element.
	 * @return Mp7JrsTextualPtr
	 */
	virtual Mp7JrsTextualPtr GetText() const;

	virtual bool IsValidText() const;

	/*
	 * Set Text element.
	 * @param item const Mp7JrsTextualPtr &
	 */
	virtual void SetText(const Mp7JrsTextualPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Text element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateText();

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get SpatioTemporalLocator element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsSpatioTemporalLocatorPtr
	 */
	virtual Mp7JrsSpatioTemporalLocatorPtr GetSpatioTemporalLocator() const;

	/*
	 * Set SpatioTemporalLocator element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsSpatioTemporalLocatorPtr &
	 */
	virtual void SetSpatioTemporalLocator(const Mp7JrsSpatioTemporalLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SpatioTemporalMask element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsSpatioTemporalMaskPtr
	 */
	virtual Mp7JrsSpatioTemporalMaskPtr GetSpatioTemporalMask() const;

	/*
	 * Set SpatioTemporalMask element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsSpatioTemporalMaskPtr &
	 */
	virtual void SetSpatioTemporalMask(const Mp7JrsSpatioTemporalMaskPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get MovingRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMovingRegionType_CollectionPtr
	 */
	virtual Mp7JrsMovingRegionType_CollectionPtr GetMovingRegionType_LocalType() const;

	/*
	 * Set MovingRegionType_LocalType element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMovingRegionType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMovingRegionType_LocalType(const Mp7JrsMovingRegionType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MultipleView element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMultipleViewPtr
	 */
	virtual Mp7JrsMultipleViewPtr GetMultipleView() const;

	virtual bool IsValidMultipleView() const;

	/*
	 * Set MultipleView element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMultipleViewPtr &
	 */
	virtual void SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate MultipleView element.
	 * (Inherited from Mp7JrsMovingRegionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMultipleView();

	/*
	 * Get MovingRegionType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @return Mp7JrsMovingRegionType_Collection0Ptr
	 */
	virtual Mp7JrsMovingRegionType_Collection0Ptr GetMovingRegionType_LocalType0() const;

	/*
	 * Set MovingRegionType_LocalType0 element.
<br>
	 * (Inherited from Mp7JrsMovingRegionType)
	 * @param item const Mp7JrsMovingRegionType_Collection0Ptr &
	 */
	// Mandatory collection
	virtual void SetMovingRegionType_LocalType0(const Mp7JrsMovingRegionType_Collection0Ptr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaInformationPtr
	 */
	virtual Mp7JrsMediaInformationPtr GetMediaInformation() const;

	/*
	 * Set MediaInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaInformationPtr &
	 */
	virtual void SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetMediaInformationRef() const;

	/*
	 * Set MediaInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsMediaLocatorPtr
	 */
	virtual Mp7JrsMediaLocatorPtr GetMediaLocator() const;

	/*
	 * Set MediaLocator element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsMediaLocatorPtr &
	 */
	virtual void SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsControlledTermUsePtr
	 */
	virtual Mp7JrsControlledTermUsePtr GetStructuralUnit() const;

	virtual bool IsValidStructuralUnit() const;

	/*
	 * Set StructuralUnit element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsControlledTermUsePtr &
	 */
	virtual void SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate StructuralUnit element.
	 * (Inherited from Mp7JrsSegmentType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateStructuralUnit();

		/* @name Choice

		 */
		//@{
	/*
	 * Get CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsCreationInformationPtr
	 */
	virtual Mp7JrsCreationInformationPtr GetCreationInformation() const;

	/*
	 * Set CreationInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsCreationInformationPtr &
	 */
	virtual void SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetCreationInformationRef() const;

	/*
	 * Set CreationInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		/* @name Choice

		 */
		//@{
	/*
	 * Get UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsUsageInformationPtr
	 */
	virtual Mp7JrsUsageInformationPtr GetUsageInformation() const;

	/*
	 * Set UsageInformation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsUsageInformationPtr &
	 */
	virtual void SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsReferencePtr
	 */
	virtual Mp7JrsReferencePtr GetUsageInformationRef() const;

	/*
	 * Set UsageInformationRef element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsReferencePtr &
	 */
	virtual void SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_TextAnnotation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_TextAnnotation_CollectionPtr GetTextAnnotation() const;

	/*
	 * Set TextAnnotation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_TextAnnotation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_CollectionPtr GetSegmentType_LocalType() const;

	/*
	 * Set SegmentType_LocalType element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_MatchingHint_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_MatchingHint_CollectionPtr GetMatchingHint() const;

	/*
	 * Set MatchingHint element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_MatchingHint_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_PointOfView_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_PointOfView_CollectionPtr GetPointOfView() const;

	/*
	 * Set PointOfView element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_PointOfView_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @return Mp7JrsSegmentType_Relation_CollectionPtr
	 */
	virtual Mp7JrsSegmentType_Relation_CollectionPtr GetRelation() const;

	/*
	 * Set Relation element.
<br>
	 * (Inherited from Mp7JrsSegmentType)
	 * @param item const Mp7JrsSegmentType_Relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of VideoTextType.
	 * Currently this type contains 27 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>MediaInformation</li>
	 * <li>MediaInformationRef</li>
	 * <li>MediaLocator</li>
	 * <li>StructuralUnit</li>
	 * <li>CreationInformation</li>
	 * <li>CreationInformationRef</li>
	 * <li>UsageInformation</li>
	 * <li>UsageInformationRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:TextAnnotation</li>
	 * <li>Semantic</li>
	 * <li>SemanticRef</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:MatchingHint</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:PointOfView</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Relation</li>
	 * <li>SpatioTemporalLocator</li>
	 * <li>SpatioTemporalMask</li>
	 * <li>VisualDescriptor</li>
	 * <li>VisualDescriptionScheme</li>
	 * <li>VisualTimeSeriesDescriptor</li>
	 * <li>GofGopFeature</li>
	 * <li>MultipleView</li>
	 * <li>SpatialDecomposition</li>
	 * <li>TemporalDecomposition</li>
	 * <li>SpatioTemporalDecomposition</li>
	 * <li>MediaSourceDecomposition</li>
	 * <li>Text</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsVideoTextType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsVideoTextType();

protected:
	Mp7JrsVideoTextType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	Mp7JrsVideoTextType_textType_Local2Ptr m_textType;
	bool m_textType_Exist;
	unsigned m_fontSize;
	bool m_fontSize_Exist;
	XMLCh * m_fontType;
	bool m_fontType_Exist;

	// Base Class
	Dc1Ptr< Mp7JrsMovingRegionType > m_Base;

	Mp7JrsTextualPtr m_Text;
	bool m_Text_Exist; // For optional elements 

// no includefile for extension defined 
// file Mp7JrsVideoTextType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _4001MP7JRSVIDEOTEXTTYPE_H

