/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1773MP7JRSDESCRIPTORMODELTYPE_H
#define _1773MP7JRSDESCRIPTORMODELTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsDescriptorModelTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsDescriptorModelType_ExtInclude.h


class IMp7JrsModelType;
typedef Dc1Ptr< IMp7JrsModelType > Mp7JrsModelPtr;
class IMp7JrsDescriptorModelType;
typedef Dc1Ptr< IMp7JrsDescriptorModelType> Mp7JrsDescriptorModelPtr;
class IMp7JrsDType;
typedef Dc1Ptr< IMp7JrsDType > Mp7JrsDPtr;
class IMp7JrsDescriptorModelType_Field_CollectionType;
typedef Dc1Ptr< IMp7JrsDescriptorModelType_Field_CollectionType > Mp7JrsDescriptorModelType_Field_CollectionPtr;
#include "Mp7JrsModelType.h"
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsDescriptorModelType for class Mp7JrsDescriptorModelType<br>
 * Located at: mpeg7-v4.xsd, line 9612<br>
 * Classified: Class<br>
 * Derived from: ModelType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsDescriptorModelType 
 :
		public IMp7JrsModelType
{
public:
	// TODO: make these protected?
	IMp7JrsDescriptorModelType();
	virtual ~IMp7JrsDescriptorModelType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Descriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptor() const = 0;

	/**
	 * Set Descriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVisualSummaryComponentPtr
	virtual void SetDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Field element.
	 * @return Mp7JrsDescriptorModelType_Field_CollectionPtr
	 */
	virtual Mp7JrsDescriptorModelType_Field_CollectionPtr GetField() const = 0;

	/**
	 * Set Field element.
	 * @param item const Mp7JrsDescriptorModelType_Field_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetField(const Mp7JrsDescriptorModelType_Field_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const = 0;

	/**
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existconfidence() const = 0;

	/**
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateconfidence() = 0;

	/**
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const = 0;

	/**
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existreliability() const = 0;

	/**
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatereliability() = 0;

	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of DescriptorModelType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Descriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Field</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsDescriptorModelType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsDescriptorModelType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsDescriptorModelType<br>
 * Located at: mpeg7-v4.xsd, line 9612<br>
 * Classified: Class<br>
 * Derived from: ModelType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 9612.<br>
 */
class DC1_EXPORT Mp7JrsDescriptorModelType :
		public IMp7JrsDescriptorModelType
{
	friend class Mp7JrsDescriptorModelTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsDescriptorModelType
	 
	 * @return Dc1Ptr< Mp7JrsModelType >
	 */
	virtual Dc1Ptr< Mp7JrsModelType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Descriptor element.
	 * @return Dc1Ptr< Dc1Node >
	 * Possible types (within this library):
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	virtual Dc1Ptr< Dc1Node > GetDescriptor() const;

	/*
	 * Set Descriptor element.
	 * @param item const Dc1Ptr< Dc1Node > &
	 * Valid types for item:
	 * <ul>
	 * <li>IMp7JrsAdvancedFaceRecognitionPtr</li>
	 * <li>IMp7JrsAudioBPMPtr</li>
	 * <li>IMp7JrsAudioFundamentalFrequencyPtr</li>
	 * <li>IMp7JrsAudioHarmonicityPtr</li>
	 * <li>IMp7JrsAudioPowerPtr</li>
	 * <li>IMp7JrsAudioSpectrumBasisPtr</li>
	 * <li>IMp7JrsAudioSpectrumCentroidPtr</li>
	 * <li>IMp7JrsAudioSpectrumEnvelopePtr</li>
	 * <li>IMp7JrsAudioSpectrumFlatnessPtr</li>
	 * <li>IMp7JrsAudioSpectrumProjectionPtr</li>
	 * <li>IMp7JrsAudioSpectrumSpreadPtr</li>
	 * <li>IMp7JrsAudioSummaryComponentPtr</li>
	 * <li>IMp7JrsAudioWaveformPtr</li>
	 * <li>IMp7JrsBackgroundNoiseLevelPtr</li>
	 * <li>IMp7JrsBalancePtr</li>
	 * <li>IMp7JrsBandwidthPtr</li>
	 * <li>IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr</li>
	 * <li>IMp7JrsCameraMotionPtr</li>
	 * <li>IMp7JrsChordBasePtr</li>
	 * <li>IMp7JrsClassificationPerceptualAttributePtr</li>
	 * <li>IMp7JrsColorLayoutPtr</li>
	 * <li>IMp7JrsColorSamplingPtr</li>
	 * <li>IMp7JrsColorStructurePtr</li>
	 * <li>IMp7JrsColorTemperaturePtr</li>
	 * <li>IMp7JrsContourShapePtr</li>
	 * <li>IMp7JrsCrossChannelCorrelationPtr</li>
	 * <li>IMp7JrsDcOffsetPtr</li>
	 * <li>IMp7JrsDistributionPerceptualAttributePtr</li>
	 * <li>IMp7JrsDominantColorPtr</li>
	 * <li>IMp7JrsEdgeHistogramPtr</li>
	 * <li>IMp7JrsExtendedMediaQualityPtr</li>
	 * <li>IMp7JrsFaceRecognitionPtr</li>
	 * <li>IMp7JrsGoFGoPColorPtr</li>
	 * <li>IMp7JrsHarmonicSpectralCentroidPtr</li>
	 * <li>IMp7JrsHarmonicSpectralDeviationPtr</li>
	 * <li>IMp7JrsHarmonicSpectralSpreadPtr</li>
	 * <li>IMp7JrsHarmonicSpectralVariationPtr</li>
	 * <li>IMp7JrsHomogeneousTexturePtr</li>
	 * <li>IMp7JrsImageSignaturePtr</li>
	 * <li>IMp7JrsLinearPerceptualAttributePtr</li>
	 * <li>IMp7JrsLogAttackTimePtr</li>
	 * <li>IMp7JrsMatchingHintPtr</li>
	 * <li>IMp7JrsMediaFormatPtr</li>
	 * <li>IMp7JrsMediaIdentificationPtr</li>
	 * <li>IMp7JrsMediaQualityPtr</li>
	 * <li>IMp7JrsMediaSpaceMaskPtr</li>
	 * <li>IMp7JrsMediaTranscodingHintsPtr</li>
	 * <li>IMp7JrsMeterPtr</li>
	 * <li>IMp7JrsMotionActivityPtr</li>
	 * <li>IMp7JrsMotionTrajectoryPtr</li>
	 * <li>IMp7JrsOrderedGroupDataSetMaskPtr</li>
	 * <li>IMp7JrsParametricMotionPtr</li>
	 * <li>IMp7JrsPerceptual3DShapePtr</li>
	 * <li>IMp7JrsPerceptualBeatPtr</li>
	 * <li>IMp7JrsPerceptualEnergyPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualGenreDistributionPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualInstrumentationDistributionPtr</li>
	 * <li>IMp7JrsPerceptualLanguagePtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualLyricsDistributionPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionElementPtr</li>
	 * <li>IMp7JrsPerceptualMoodDistributionPtr</li>
	 * <li>IMp7JrsPerceptualRecordingPtr</li>
	 * <li>IMp7JrsPerceptualSoundPtr</li>
	 * <li>IMp7JrsPerceptualTempoPtr</li>
	 * <li>IMp7JrsPerceptualValencePtr</li>
	 * <li>IMp7JrsPerceptualVocalsPtr</li>
	 * <li>IMp7JrsPointOfViewPtr</li>
	 * <li>IMp7JrsPreferenceConditionPtr</li>
	 * <li>IMp7JrsReferencePitchPtr</li>
	 * <li>IMp7JrsRegionShapePtr</li>
	 * <li>IMp7JrsRelativeDelayPtr</li>
	 * <li>IMp7JrsRhythmicBasePtr</li>
	 * <li>IMp7JrsScalableColorPtr</li>
	 * <li>IMp7JrsSceneGraphMaskPtr</li>
	 * <li>IMp7JrsShape3DPtr</li>
	 * <li>IMp7JrsShapeVariationPtr</li>
	 * <li>IMp7JrsSilencePtr</li>
	 * <li>IMp7JrsSoundModelStateHistogramPtr</li>
	 * <li>IMp7JrsSourcePreferencesType_MediaFormat_LocalPtr</li>
	 * <li>IMp7JrsSpatialMaskPtr</li>
	 * <li>IMp7JrsSpatioTemporalMaskPtr</li>
	 * <li>IMp7JrsSpectralCentroidPtr</li>
	 * <li>IMp7JrsStreamMaskPtr</li>
	 * <li>IMp7JrsTemporalCentroidPtr</li>
	 * <li>IMp7JrsTemporalMaskPtr</li>
	 * <li>IMp7JrsTextualSummaryComponentPtr</li>
	 * <li>IMp7JrsTextureBrowsingPtr</li>
	 * <li>IMp7JrsVideoSignaturePtr</li>
	 * <li>IMp7JrsVisualSummaryComponentPtr</li>
	 * </ul>
	 */
	// Mandatory abstract element, possible types are
	// Mp7JrsAdvancedFaceRecognitionPtr
	// Mp7JrsAudioBPMPtr
	// Mp7JrsAudioFundamentalFrequencyPtr
	// Mp7JrsAudioHarmonicityPtr
	// Mp7JrsAudioPowerPtr
	// Mp7JrsAudioSpectrumBasisPtr
	// Mp7JrsAudioSpectrumCentroidPtr
	// Mp7JrsAudioSpectrumEnvelopePtr
	// Mp7JrsAudioSpectrumFlatnessPtr
	// Mp7JrsAudioSpectrumProjectionPtr
	// Mp7JrsAudioSpectrumSpreadPtr
	// Mp7JrsAudioSummaryComponentPtr
	// Mp7JrsAudioWaveformPtr
	// Mp7JrsBackgroundNoiseLevelPtr
	// Mp7JrsBalancePtr
	// Mp7JrsBandwidthPtr
	// Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr
	// Mp7JrsCameraMotionPtr
	// Mp7JrsChordBasePtr
	// Mp7JrsClassificationPerceptualAttributePtr
	// Mp7JrsColorLayoutPtr
	// Mp7JrsColorSamplingPtr
	// Mp7JrsColorStructurePtr
	// Mp7JrsColorTemperaturePtr
	// Mp7JrsContourShapePtr
	// Mp7JrsCrossChannelCorrelationPtr
	// Mp7JrsDcOffsetPtr
	// Mp7JrsDistributionPerceptualAttributePtr
	// Mp7JrsDominantColorPtr
	// Mp7JrsEdgeHistogramPtr
	// Mp7JrsExtendedMediaQualityPtr
	// Mp7JrsFaceRecognitionPtr
	// Mp7JrsGoFGoPColorPtr
	// Mp7JrsHarmonicSpectralCentroidPtr
	// Mp7JrsHarmonicSpectralDeviationPtr
	// Mp7JrsHarmonicSpectralSpreadPtr
	// Mp7JrsHarmonicSpectralVariationPtr
	// Mp7JrsHomogeneousTexturePtr
	// Mp7JrsImageSignaturePtr
	// Mp7JrsLinearPerceptualAttributePtr
	// Mp7JrsLogAttackTimePtr
	// Mp7JrsMatchingHintPtr
	// Mp7JrsMediaFormatPtr
	// Mp7JrsMediaIdentificationPtr
	// Mp7JrsMediaQualityPtr
	// Mp7JrsMediaSpaceMaskPtr
	// Mp7JrsMediaTranscodingHintsPtr
	// Mp7JrsMeterPtr
	// Mp7JrsMotionActivityPtr
	// Mp7JrsMotionTrajectoryPtr
	// Mp7JrsOrderedGroupDataSetMaskPtr
	// Mp7JrsParametricMotionPtr
	// Mp7JrsPerceptual3DShapePtr
	// Mp7JrsPerceptualBeatPtr
	// Mp7JrsPerceptualEnergyPtr
	// Mp7JrsPerceptualGenreDistributionElementPtr
	// Mp7JrsPerceptualGenreDistributionPtr
	// Mp7JrsPerceptualInstrumentationDistributionElementPtr
	// Mp7JrsPerceptualInstrumentationDistributionPtr
	// Mp7JrsPerceptualLanguagePtr
	// Mp7JrsPerceptualLyricsDistributionElementPtr
	// Mp7JrsPerceptualLyricsDistributionPtr
	// Mp7JrsPerceptualMoodDistributionElementPtr
	// Mp7JrsPerceptualMoodDistributionPtr
	// Mp7JrsPerceptualRecordingPtr
	// Mp7JrsPerceptualSoundPtr
	// Mp7JrsPerceptualTempoPtr
	// Mp7JrsPerceptualValencePtr
	// Mp7JrsPerceptualVocalsPtr
	// Mp7JrsPointOfViewPtr
	// Mp7JrsPreferenceConditionPtr
	// Mp7JrsReferencePitchPtr
	// Mp7JrsRegionShapePtr
	// Mp7JrsRelativeDelayPtr
	// Mp7JrsRhythmicBasePtr
	// Mp7JrsScalableColorPtr
	// Mp7JrsSceneGraphMaskPtr
	// Mp7JrsShape3DPtr
	// Mp7JrsShapeVariationPtr
	// Mp7JrsSilencePtr
	// Mp7JrsSoundModelStateHistogramPtr
	// Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr
	// Mp7JrsSpatialMaskPtr
	// Mp7JrsSpatioTemporalMaskPtr
	// Mp7JrsSpectralCentroidPtr
	// Mp7JrsStreamMaskPtr
	// Mp7JrsTemporalCentroidPtr
	// Mp7JrsTemporalMaskPtr
	// Mp7JrsTextualSummaryComponentPtr
	// Mp7JrsTextureBrowsingPtr
	// Mp7JrsVideoSignaturePtr
	// Mp7JrsVisualSummaryComponentPtr
	virtual void SetDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Field element.
	 * @return Mp7JrsDescriptorModelType_Field_CollectionPtr
	 */
	virtual Mp7JrsDescriptorModelType_Field_CollectionPtr GetField() const;

	/*
	 * Set Field element.
	 * @param item const Mp7JrsDescriptorModelType_Field_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetField(const Mp7JrsDescriptorModelType_Field_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const;

	/*
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existconfidence() const;

	/*
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateconfidence();

	/*
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const;

	/*
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existreliability() const;

	/*
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatereliability();

	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of DescriptorModelType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Descriptor</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Field</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsDescriptorModelType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsDescriptorModelType();

protected:
	Mp7JrsDescriptorModelType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsModelType > m_Base;

	Dc1Ptr< Dc1Node > m_Descriptor;
	Mp7JrsDescriptorModelType_Field_CollectionPtr m_Field;

// no includefile for extension defined 
// file Mp7JrsDescriptorModelType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1773MP7JRSDESCRIPTORMODELTYPE_H

