/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _2993MP7JRSQCVALUETYPE_H
#define _2993MP7JRSQCVALUETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsQCValueTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsQCValueType_ExtInclude.h


class IMp7JrsQCValueType;
typedef Dc1Ptr< IMp7JrsQCValueType> Mp7JrsQCValuePtr;
class IMp7JrsQCValueType_track_LocalType;
typedef Dc1Ptr< IMp7JrsQCValueType_track_LocalType > Mp7JrsQCValueType_track_LocalPtr;

/** 
 * Generated interface IMp7JrsQCValueType for class Mp7JrsQCValueType<br>
 * Located at: mpeg7-v4.xsd, line 6282<br>
 * Classified: Class<br>
 * Derived from: string (String)<br>
 */
class MP7JRS_EXPORT IMp7JrsQCValueType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IMp7JrsQCValueType();
	virtual ~IMp7JrsQCValueType();
	/**
	 * Sets the content of Mp7JrsQCValueType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of Mp7JrsQCValueType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get name attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getname() const = 0;

	/**
	 * Set name attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Setname(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get unit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getunit() const = 0;

	/**
	 * Get unit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existunit() const = 0;

	/**
	 * Set unit attribute.
	 * @param item XMLCh *
	 */
	virtual void Setunit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate unit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateunit() = 0;

	/**
	 * Get track attribute.
	 * @return Mp7JrsQCValueType_track_LocalPtr
	 */
	virtual Mp7JrsQCValueType_track_LocalPtr Gettrack() const = 0;

	/**
	 * Get track attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtrack() const = 0;

	/**
	 * Set track attribute.
	 * @param item const Mp7JrsQCValueType_track_LocalPtr &
	 */
	virtual void Settrack(const Mp7JrsQCValueType_track_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate track attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetrack() = 0;

	/**
	 * Get valueRange attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvalueRange() const = 0;

	/**
	 * Get valueRange attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvalueRange() const = 0;

	/**
	 * Set valueRange attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvalueRange(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate valueRange attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevalueRange() = 0;

	/**
	 * Get type attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const = 0;

	/**
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const = 0;

	/**
	 * Set type attribute.
	 * @param item XMLCh *
	 */
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype() = 0;

	/**
	 * Get representation attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getrepresentation() const = 0;

	/**
	 * Get representation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrepresentation() const = 0;

	/**
	 * Set representation attribute.
	 * @param item XMLCh *
	 */
	virtual void Setrepresentation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate representation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterepresentation() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of QCValueType.
	 * Currently just supports rudimentary XPath expressions as QCValueType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsQCValueType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsQCValueType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsQCValueType<br>
 * Located at: mpeg7-v4.xsd, line 6282<br>
 * Classified: Class<br>
 * Derived from: string (String)<br>
 * Defined in mpeg7-v4.xsd, line 6282.<br>
 */
class DC1_EXPORT Mp7JrsQCValueType :
		public IMp7JrsQCValueType
{
	friend class Mp7JrsQCValueTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of Mp7JrsQCValueType	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of Mp7JrsQCValueType 
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get name attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getname() const;

	/*
	 * Set name attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void Setname(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get unit attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getunit() const;

	/*
	 * Get unit attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existunit() const;

	/*
	 * Set unit attribute.
	 * @param item XMLCh *
	 */
	virtual void Setunit(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate unit attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateunit();

	/*
	 * Get track attribute.
	 * @return Mp7JrsQCValueType_track_LocalPtr
	 */
	virtual Mp7JrsQCValueType_track_LocalPtr Gettrack() const;

	/*
	 * Get track attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtrack() const;

	/*
	 * Set track attribute.
	 * @param item const Mp7JrsQCValueType_track_LocalPtr &
	 */
	virtual void Settrack(const Mp7JrsQCValueType_track_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate track attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetrack();

	/*
	 * Get valueRange attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvalueRange() const;

	/*
	 * Get valueRange attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvalueRange() const;

	/*
	 * Set valueRange attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvalueRange(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate valueRange attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevalueRange();

	/*
	 * Get type attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettype() const;

	/*
	 * Get type attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtype() const;

	/*
	 * Set type attribute.
	 * @param item XMLCh *
	 */
	virtual void Settype(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate type attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetype();

	/*
	 * Get representation attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getrepresentation() const;

	/*
	 * Get representation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrepresentation() const;

	/*
	 * Set representation attribute.
	 * @param item XMLCh *
	 */
	virtual void Setrepresentation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate representation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidaterepresentation();

	/*
	 * Gets or creates Dc1NodePtr child elements of QCValueType.
	 * Currently just supports rudimentary XPath expressions as QCValueType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsQCValueType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsQCValueType();

protected:
	Mp7JrsQCValueType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_name;
	XMLCh * m_unit;
	bool m_unit_Exist;
	Mp7JrsQCValueType_track_LocalPtr m_track;
	bool m_track_Exist;
	XMLCh * m_valueRange;
	bool m_valueRange_Exist;
	XMLCh * m_type;
	bool m_type_Exist;
	XMLCh * m_representation;
	bool m_representation_Exist;

	// Base String
	XMLCh * m_Base;


// no includefile for extension defined 
// file Mp7JrsQCValueType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _2993MP7JRSQCVALUETYPE_H

