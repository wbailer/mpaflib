/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _1883MP7JRSEXPONENTIALDISTRIBUTIONTYPE_H
#define _1883MP7JRSEXPONENTIALDISTRIBUTIONTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Mp7JrsDefines.h"
#include "Dc1FactoryDefines.h"
#include "Mp7JrsFactoryDefines.h"

#include "Mp7JrsExponentialDistributionTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file Mp7JrsExponentialDistributionType_ExtInclude.h


class IMp7JrsContinuousDistributionType;
typedef Dc1Ptr< IMp7JrsContinuousDistributionType > Mp7JrsContinuousDistributionPtr;
class IMp7JrsExponentialDistributionType;
typedef Dc1Ptr< IMp7JrsExponentialDistributionType> Mp7JrsExponentialDistributionPtr;
class IMp7JrsDoubleMatrixType;
typedef Dc1Ptr< IMp7JrsDoubleMatrixType > Mp7JrsDoubleMatrixPtr;
#include "Mp7JrsContinuousDistributionType.h"
#include "Mp7JrsProbabilityDistributionType.h"
class IMp7JrsProbabilityDistributionType_Moment_CollectionType;
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Moment_CollectionType > Mp7JrsProbabilityDistributionType_Moment_CollectionPtr;
class IMp7JrsProbabilityDistributionType_Cumulant_CollectionType;
typedef Dc1Ptr< IMp7JrsProbabilityDistributionType_Cumulant_CollectionType > Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr;
#include "Mp7JrsProbabilityModelType.h"
#include "Mp7JrsModelType.h"
class IMp7JrszeroToOneType;
typedef Dc1Ptr< IMp7JrszeroToOneType > Mp7JrszeroToOnePtr;
#include "Mp7JrsDSType.h"
class IMp7JrsxPathRefType;
typedef Dc1Ptr< IMp7JrsxPathRefType > Mp7JrsxPathRefPtr;
class IMp7JrsdurationType;
typedef Dc1Ptr< IMp7JrsdurationType > Mp7JrsdurationPtr;
class IMp7JrsmediaDurationType;
typedef Dc1Ptr< IMp7JrsmediaDurationType > Mp7JrsmediaDurationPtr;
class IMp7JrsDSType_Header_CollectionType;
typedef Dc1Ptr< IMp7JrsDSType_Header_CollectionType > Mp7JrsDSType_Header_CollectionPtr;
#include "Mp7JrsMpeg7BaseType.h"

/** 
 * Generated interface IMp7JrsExponentialDistributionType for class Mp7JrsExponentialDistributionType<br>
 * Located at: mpeg7-v4.xsd, line 9447<br>
 * Classified: Class<br>
 * Derived from: ContinuousDistributionType (Class)<br>
 */
class MP7JRS_EXPORT IMp7JrsExponentialDistributionType 
 :
		public IMp7JrsContinuousDistributionType
{
public:
	// TODO: make these protected?
	IMp7JrsExponentialDistributionType();
	virtual ~IMp7JrsExponentialDistributionType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get Theta element.
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetTheta() const = 0;

	/**
	 * Set Theta element.
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	// Mandatory			
	virtual void SetTheta(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return unsigned
	 */
	virtual unsigned Getdim() const = 0;

	/**
	 * Get dim attribute validity information.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdim() const = 0;

	/**
	 * Set dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item unsigned
	 */
	virtual void Setdim(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dim attribute.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedim() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMean() const = 0;

	virtual bool IsValidMean() const = 0;

	/**
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMean() = 0;

	/**
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetVariance() const = 0;

	virtual bool IsValidVariance() const = 0;

	/**
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVariance() = 0;

	/**
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMin() const = 0;

	virtual bool IsValidMin() const = 0;

	/**
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMin() = 0;

	/**
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMax() const = 0;

	virtual bool IsValidMax() const = 0;

	/**
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMax() = 0;

	/**
	 * Get Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMode() const = 0;

	virtual bool IsValidMode() const = 0;

	/**
	 * Set Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Mode element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMode() = 0;

	/**
	 * Get Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMedian() const = 0;

	virtual bool IsValidMedian() const = 0;

	/**
	 * Set Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate Median element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMedian() = 0;

	/**
	 * Get Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Moment_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Moment_CollectionPtr GetMoment() const = 0;

	/**
	 * Set Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr GetCumulant() const = 0;

	/**
	 * Set Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const = 0;

	/**
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existconfidence() const = 0;

	/**
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateconfidence() = 0;

	/**
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const = 0;

	/**
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existreliability() const = 0;

	/**
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatereliability() = 0;

	/**
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const = 0;

	/**
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const = 0;

	/**
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid() = 0;

	/**
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const = 0;

	/**
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const = 0;

	/**
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase() = 0;

	/**
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const = 0;

	/**
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const = 0;

	/**
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit() = 0;

	/**
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const = 0;

	/**
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const = 0;

	/**
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase() = 0;

	/**
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const = 0;

	/**
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const = 0;

	/**
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const = 0;

	/**
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of ExponentialDistributionType.
	 * Currently this type contains 10 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Mean</li>
	 * <li>Variance</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mode</li>
	 * <li>Median</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Moment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Cumulant</li>
	 * <li>Theta</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file Mp7JrsExponentialDistributionType_ExtMethodDef.h


// no includefile for extension defined 
// file Mp7JrsExponentialDistributionType_ExtPropDef.h

};




/*
 * Generated class Mp7JrsExponentialDistributionType<br>
 * Located at: mpeg7-v4.xsd, line 9447<br>
 * Classified: Class<br>
 * Derived from: ContinuousDistributionType (Class)<br>
 * Defined in mpeg7-v4.xsd, line 9447.<br>
 */
class DC1_EXPORT Mp7JrsExponentialDistributionType :
		public IMp7JrsExponentialDistributionType
{
	friend class Mp7JrsExponentialDistributionTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of Mp7JrsExponentialDistributionType
	 
	 * @return Dc1Ptr< Mp7JrsContinuousDistributionType >
	 */
	virtual Dc1Ptr< Mp7JrsContinuousDistributionType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get Theta element.
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetTheta() const;

	/*
	 * Set Theta element.
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	// Mandatory			
	virtual void SetTheta(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return unsigned
	 */
	virtual unsigned Getdim() const;

	/*
	 * Get dim attribute validity information.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdim() const;

	/*
	 * Set dim attribute.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item unsigned
	 */
	virtual void Setdim(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dim attribute.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedim();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMean() const;

	virtual bool IsValidMean() const;

	/*
	 * Set Mean element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mean element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMean();

	/*
	 * Get Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetVariance() const;

	virtual bool IsValidVariance() const;

	/*
	 * Set Variance element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Variance element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateVariance();

	/*
	 * Get Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMin() const;

	virtual bool IsValidMin() const;

	/*
	 * Set Min element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Min element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMin();

	/*
	 * Get Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMax() const;

	virtual bool IsValidMax() const;

	/*
	 * Set Max element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Max element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMax();

	/*
	 * Get Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMode() const;

	virtual bool IsValidMode() const;

	/*
	 * Set Mode element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Mode element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMode();

	/*
	 * Get Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsDoubleMatrixPtr
	 */
	virtual Mp7JrsDoubleMatrixPtr GetMedian() const;

	virtual bool IsValidMedian() const;

	/*
	 * Set Median element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsDoubleMatrixPtr &
	 */
	virtual void SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate Median element.
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateMedian();

	/*
	 * Get Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Moment_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Moment_CollectionPtr GetMoment() const;

	/*
	 * Set Moment element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @return Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr
	 */
	virtual Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr GetCumulant() const;

	/*
	 * Set Cumulant element.
<br>
	 * (Inherited from Mp7JrsProbabilityDistributionType)
	 * @param item const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Get confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getconfidence() const;

	/*
	 * Get confidence attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existconfidence() const;

	/*
	 * Set confidence attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate confidence attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateconfidence();

	/*
	 * Get reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @return Mp7JrszeroToOnePtr
	 */
	virtual Mp7JrszeroToOnePtr Getreliability() const;

	/*
	 * Get reliability attribute validity information.
	 * (Inherited from Mp7JrsModelType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existreliability() const;

	/*
	 * Set reliability attribute.
<br>
	 * (Inherited from Mp7JrsModelType)
	 * @param item const Mp7JrszeroToOnePtr &
	 */
	virtual void Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate reliability attribute.
	 * (Inherited from Mp7JrsModelType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatereliability();

	/*
	 * Get id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getid() const;

	/*
	 * Get id attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existid() const;

	/*
	 * Set id attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item XMLCh *
	 */
	virtual void Setid(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate id attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateid();

	/*
	 * Get timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GettimeBase() const;

	/*
	 * Get timeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeBase() const;

	/*
	 * Set timeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeBase();

	/*
	 * Get timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsdurationPtr
	 */
	virtual Mp7JrsdurationPtr GettimeUnit() const;

	/*
	 * Get timeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeUnit() const;

	/*
	 * Set timeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsdurationPtr &
	 */
	virtual void SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeUnit();

	/*
	 * Get mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsxPathRefPtr
	 */
	virtual Mp7JrsxPathRefPtr GetmediaTimeBase() const;

	/*
	 * Get mediaTimeBase attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeBase() const;

	/*
	 * Set mediaTimeBase attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsxPathRefPtr &
	 */
	virtual void SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeBase attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeBase();

	/*
	 * Get mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsmediaDurationPtr
	 */
	virtual Mp7JrsmediaDurationPtr GetmediaTimeUnit() const;

	/*
	 * Get mediaTimeUnit attribute validity information.
	 * (Inherited from Mp7JrsDSType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistmediaTimeUnit() const;

	/*
	 * Set mediaTimeUnit attribute.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsmediaDurationPtr &
	 */
	virtual void SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate mediaTimeUnit attribute.
	 * (Inherited from Mp7JrsDSType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemediaTimeUnit();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @return Mp7JrsDSType_Header_CollectionPtr
	 */
	virtual Mp7JrsDSType_Header_CollectionPtr GetHeader() const;

	/*
	 * Set Header element.
<br>
	 * (Inherited from Mp7JrsDSType)
	 * @param item const Mp7JrsDSType_Header_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of ExponentialDistributionType.
	 * Currently this type contains 10 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:mpeg:mpeg7:schema:2004:Header</li>
	 * <li>Mean</li>
	 * <li>Variance</li>
	 * <li>Min</li>
	 * <li>Max</li>
	 * <li>Mode</li>
	 * <li>Median</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Moment</li>
	 * <li>urn:mpeg:mpeg7:schema:2004:Cumulant</li>
	 * <li>Theta</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file Mp7JrsExponentialDistributionType_ExtMyMethodDef.h


public:

	virtual ~Mp7JrsExponentialDistributionType();

protected:
	Mp7JrsExponentialDistributionType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< Mp7JrsContinuousDistributionType > m_Base;

	Mp7JrsDoubleMatrixPtr m_Theta;

// no includefile for extension defined 
// file Mp7JrsExponentialDistributionType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _1883MP7JRSEXPONENTIALDISTRIBUTIONTYPE_H

