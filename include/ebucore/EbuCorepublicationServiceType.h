/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _493EBUCOREPUBLICATIONSERVICETYPE_H
#define _493EBUCOREPUBLICATIONSERVICETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCorepublicationServiceTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtInclude.h


class IEbuCorepublicationServiceType;
typedef Dc1Ptr< IEbuCorepublicationServiceType> EbuCorepublicationServicePtr;
class IEbuCoreorganisationDetailsType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType > EbuCoreorganisationDetailsPtr;

/** 
 * Generated interface IEbuCorepublicationServiceType for class EbuCorepublicationServiceType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1715<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorepublicationServiceType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorepublicationServiceType();
	virtual ~IEbuCorepublicationServiceType();
	/**
	 * Get serviceId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetserviceId() const = 0;

	/**
	 * Get serviceId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistserviceId() const = 0;

	/**
	 * Set serviceId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetserviceId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate serviceId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateserviceId() = 0;

	/**
	 * Get linkToLogo attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetlinkToLogo() const = 0;

	/**
	 * Get linkToLogo attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlinkToLogo() const = 0;

	/**
	 * Set linkToLogo attribute.
	 * @param item XMLCh *
	 */
	virtual void SetlinkToLogo(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate linkToLogo attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelinkToLogo() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get publicationServiceName element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationServiceName() const = 0;

	virtual bool IsValidpublicationServiceName() const = 0;

	/**
	 * Set publicationServiceName element.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationServiceName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationServiceName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationServiceName() = 0;

	/**
	 * Get publicationSource element.
	 * @return EbuCoreorganisationDetailsPtr
	 */
	virtual EbuCoreorganisationDetailsPtr GetpublicationSource() const = 0;

	virtual bool IsValidpublicationSource() const = 0;

	/**
	 * Set publicationSource element.
	 * @param item const EbuCoreorganisationDetailsPtr &
	 */
	virtual void SetpublicationSource(const EbuCoreorganisationDetailsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationSource element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationSource() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of publicationServiceType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>publicationSource</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtPropDef.h

};




/*
 * Generated class EbuCorepublicationServiceType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1715<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 1715.<br>
 */
class DC1_EXPORT EbuCorepublicationServiceType :
		public IEbuCorepublicationServiceType
{
	friend class EbuCorepublicationServiceTypeFactory; // constructs objects

public:
	/*
	 * Get serviceId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetserviceId() const;

	/*
	 * Get serviceId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistserviceId() const;

	/*
	 * Set serviceId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetserviceId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate serviceId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateserviceId();

	/*
	 * Get linkToLogo attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetlinkToLogo() const;

	/*
	 * Get linkToLogo attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlinkToLogo() const;

	/*
	 * Set linkToLogo attribute.
	 * @param item XMLCh *
	 */
	virtual void SetlinkToLogo(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate linkToLogo attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelinkToLogo();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get publicationServiceName element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationServiceName() const;

	virtual bool IsValidpublicationServiceName() const;

	/*
	 * Set publicationServiceName element.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationServiceName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationServiceName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationServiceName();

	/*
	 * Get publicationSource element.
	 * @return EbuCoreorganisationDetailsPtr
	 */
	virtual EbuCoreorganisationDetailsPtr GetpublicationSource() const;

	virtual bool IsValidpublicationSource() const;

	/*
	 * Set publicationSource element.
	 * @param item const EbuCoreorganisationDetailsPtr &
	 */
	virtual void SetpublicationSource(const EbuCoreorganisationDetailsPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationSource element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationSource();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of publicationServiceType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>publicationSource</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMyMethodDef.h


public:

	virtual ~EbuCorepublicationServiceType();

protected:
	EbuCorepublicationServiceType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_serviceId;
	bool m_serviceId_Exist;
	XMLCh * m_linkToLogo;
	bool m_linkToLogo_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;


	XMLCh * m_publicationServiceName;
	bool m_publicationServiceName_Exist; // For optional elements 
	EbuCoreorganisationDetailsPtr m_publicationSource;
	bool m_publicationSource_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCorepublicationServiceType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _493EBUCOREPUBLICATIONSERVICETYPE_H

