/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _471EBUCOREPARTTYPE_H
#define _471EBUCOREPARTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCorepartTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorepartType_ExtInclude.h


class IEbuCorecoreMetadataType;
typedef Dc1Ptr< IEbuCorecoreMetadataType > EbuCorecoreMetadataPtr;
class IEbuCorepartType;
typedef Dc1Ptr< IEbuCorepartType> EbuCorepartPtr;
class IEbuCoretimeType;
typedef Dc1Ptr< IEbuCoretimeType > EbuCoretimePtr;
class IEbuCoredurationType;
typedef Dc1Ptr< IEbuCoredurationType > EbuCoredurationPtr;
#include "EbuCorecoreMetadataType.h"
class IEbuCorecoreMetadataType_title_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_title_CollectionType > EbuCorecoreMetadataType_title_CollectionPtr;
class IEbuCorecoreMetadataType_alternativeTitle_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_alternativeTitle_CollectionType > EbuCorecoreMetadataType_alternativeTitle_CollectionPtr;
class IEbuCorecoreMetadataType_creator_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_creator_CollectionType > EbuCorecoreMetadataType_creator_CollectionPtr;
class IEbuCorecoreMetadataType_subject_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_subject_CollectionType > EbuCorecoreMetadataType_subject_CollectionPtr;
class IEbuCorecoreMetadataType_description_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_description_CollectionType > EbuCorecoreMetadataType_description_CollectionPtr;
class IEbuCorecoreMetadataType_publisher_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_publisher_CollectionType > EbuCorecoreMetadataType_publisher_CollectionPtr;
class IEbuCorecoreMetadataType_contributor_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_contributor_CollectionType > EbuCorecoreMetadataType_contributor_CollectionPtr;
class IEbuCorecoreMetadataType_date_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_date_CollectionType > EbuCorecoreMetadataType_date_CollectionPtr;
class IEbuCorecoreMetadataType_type_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_type_CollectionType > EbuCorecoreMetadataType_type_CollectionPtr;
class IEbuCorecoreMetadataType_format_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_format_CollectionType > EbuCorecoreMetadataType_format_CollectionPtr;
class IEbuCorecoreMetadataType_identifier_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_identifier_CollectionType > EbuCorecoreMetadataType_identifier_CollectionPtr;
class IEbuCorecoreMetadataType_source_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_source_CollectionType > EbuCorecoreMetadataType_source_CollectionPtr;
class IEbuCorecoreMetadataType_language_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_language_CollectionType > EbuCorecoreMetadataType_language_CollectionPtr;
class IEbuCorecoreMetadataType_relation_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_relation_CollectionType > EbuCorecoreMetadataType_relation_CollectionPtr;
class IEbuCorecoreMetadataType_isRelatedTo_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isRelatedTo_CollectionType > EbuCorecoreMetadataType_isRelatedTo_CollectionPtr;
class IEbuCorecoreMetadataType_isNextInSequence_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isNextInSequence_CollectionType > EbuCorecoreMetadataType_isNextInSequence_CollectionPtr;
class IEbuCorecoreMetadataType_isVersionOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isVersionOf_CollectionType > EbuCorecoreMetadataType_isVersionOf_CollectionPtr;
class IEbuCorecoreMetadataType_hasVersion_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasVersion_CollectionType > EbuCorecoreMetadataType_hasVersion_CollectionPtr;
class IEbuCorecoreMetadataType_isReplacedBy_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isReplacedBy_CollectionType > EbuCorecoreMetadataType_isReplacedBy_CollectionPtr;
class IEbuCorecoreMetadataType_replaces_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_replaces_CollectionType > EbuCorecoreMetadataType_replaces_CollectionPtr;
class IEbuCorecoreMetadataType_isRequiredBy_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isRequiredBy_CollectionType > EbuCorecoreMetadataType_isRequiredBy_CollectionPtr;
class IEbuCorecoreMetadataType_requires_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_requires_CollectionType > EbuCorecoreMetadataType_requires_CollectionPtr;
class IEbuCorecoreMetadataType_isPartOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isPartOf_CollectionType > EbuCorecoreMetadataType_isPartOf_CollectionPtr;
class IEbuCorecoreMetadataType_hasPart_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasPart_CollectionType > EbuCorecoreMetadataType_hasPart_CollectionPtr;
class IEbuCorecoreMetadataType_hasTrackPart_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasTrackPart_CollectionType > EbuCorecoreMetadataType_hasTrackPart_CollectionPtr;
class IEbuCorecoreMetadataType_isReferencedBy_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isReferencedBy_CollectionType > EbuCorecoreMetadataType_isReferencedBy_CollectionPtr;
class IEbuCorecoreMetadataType_references_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_references_CollectionType > EbuCorecoreMetadataType_references_CollectionPtr;
class IEbuCorecoreMetadataType_isFormatOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isFormatOf_CollectionType > EbuCorecoreMetadataType_isFormatOf_CollectionPtr;
class IEbuCorecoreMetadataType_hasFormat_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasFormat_CollectionType > EbuCorecoreMetadataType_hasFormat_CollectionPtr;
class IEbuCorecoreMetadataType_isEpisodeOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isEpisodeOf_CollectionType > EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr;
class IEbuCorecoreMetadataType_isSeasonOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isSeasonOf_CollectionType > EbuCorecoreMetadataType_isSeasonOf_CollectionPtr;
class IEbuCorecoreMetadataType_hasEpisode_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasEpisode_CollectionType > EbuCorecoreMetadataType_hasEpisode_CollectionPtr;
class IEbuCorecoreMetadataType_hasSeason_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasSeason_CollectionType > EbuCorecoreMetadataType_hasSeason_CollectionPtr;
class IEbuCorecoreMetadataType_isMemberOf_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_isMemberOf_CollectionType > EbuCorecoreMetadataType_isMemberOf_CollectionPtr;
class IEbuCorecoreMetadataType_hasMember_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasMember_CollectionType > EbuCorecoreMetadataType_hasMember_CollectionPtr;
class IEbuCorecoreMetadataType_sameAs_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_sameAs_CollectionType > EbuCorecoreMetadataType_sameAs_CollectionPtr;
class IEbuCorecoreMetadataType_coverage_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_coverage_CollectionType > EbuCorecoreMetadataType_coverage_CollectionPtr;
class IEbuCorecoreMetadataType_rights_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_rights_CollectionType > EbuCorecoreMetadataType_rights_CollectionPtr;
class IEbuCorecoreMetadataType_version_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_version_CollectionType > EbuCorecoreMetadataType_version_CollectionPtr;
class IEbuCorecoreMetadataType_publicationHistory_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_publicationHistory_CollectionType > EbuCorecoreMetadataType_publicationHistory_CollectionPtr;
class IEbuCorecoreMetadataType_planning_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_planning_CollectionType > EbuCorecoreMetadataType_planning_CollectionPtr;
class IEbuCorecoreMetadataType_rating_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_rating_CollectionType > EbuCorecoreMetadataType_rating_CollectionPtr;
class IEbuCorecoreMetadataType_part_CollectionType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_part_CollectionType > EbuCorecoreMetadataType_part_CollectionPtr;

/** 
 * Generated interface IEbuCorepartType for class EbuCorepartType<br>
 * Located at: EBU_CORE_20140318.xsd, line 596<br>
 * Classified: Class<br>
 * Derived from: coreMetadataType (Class)<br>
 */
class EBUCORE_EXPORT IEbuCorepartType 
 :
		public IEbuCorecoreMetadataType
{
public:
	// TODO: make these protected?
	IEbuCorepartType();
	virtual ~IEbuCorepartType();
	/**
	 * Get partId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartId() const = 0;

	/**
	 * Get partId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartId() const = 0;

	/**
	 * Set partId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartId() = 0;

	/**
	 * Get partName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartName() const = 0;

	/**
	 * Get partName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartName() const = 0;

	/**
	 * Set partName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartName() = 0;

	/**
	 * Get partDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartDefinition() const = 0;

	/**
	 * Get partDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartDefinition() const = 0;

	/**
	 * Set partDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartDefinition() = 0;

	/**
	 * Get partNumber attribute.
	 * @return int
	 */
	virtual int GetpartNumber() const = 0;

	/**
	 * Get partNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartNumber() const = 0;

	/**
	 * Set partNumber attribute.
	 * @param item int
	 */
	virtual void SetpartNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartNumber() = 0;

	/**
	 * Get partTotalNumber attribute.
	 * @return int
	 */
	virtual int GetpartTotalNumber() const = 0;

	/**
	 * Get partTotalNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartTotalNumber() const = 0;

	/**
	 * Set partTotalNumber attribute.
	 * @param item int
	 */
	virtual void SetpartTotalNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partTotalNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartTotalNumber() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get partStartTime element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetpartStartTime() const = 0;

	virtual bool IsValidpartStartTime() const = 0;

	/**
	 * Set partStartTime element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetpartStartTime(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partStartTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartStartTime() = 0;

	/**
	 * Get partDuration element.
	 * @return EbuCoredurationPtr
	 */
	virtual EbuCoredurationPtr GetpartDuration() const = 0;

	virtual bool IsValidpartDuration() const = 0;

	/**
	 * Set partDuration element.
	 * @param item const EbuCoredurationPtr &
	 */
	virtual void SetpartDuration(const EbuCoredurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate partDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartDuration() = 0;

		//@}
		/** @name Sequence

		 */
		//@{
	/**
	 * Get title element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_title_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_title_CollectionPtr Gettitle() const = 0;

	/**
	 * Set title element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_title_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settitle(const EbuCorecoreMetadataType_title_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get alternativeTitle element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_alternativeTitle_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_alternativeTitle_CollectionPtr GetalternativeTitle() const = 0;

	/**
	 * Set alternativeTitle element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetalternativeTitle(const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get creator element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_creator_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_creator_CollectionPtr Getcreator() const = 0;

	/**
	 * Set creator element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_creator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcreator(const EbuCorecoreMetadataType_creator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get subject element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_subject_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_subject_CollectionPtr Getsubject() const = 0;

	/**
	 * Set subject element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_subject_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setsubject(const EbuCorecoreMetadataType_subject_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get description element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_description_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_description_CollectionPtr Getdescription() const = 0;

	/**
	 * Set description element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_description_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdescription(const EbuCorecoreMetadataType_description_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get publisher element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_publisher_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_publisher_CollectionPtr Getpublisher() const = 0;

	/**
	 * Set publisher element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_publisher_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setpublisher(const EbuCorecoreMetadataType_publisher_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get contributor element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_contributor_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_contributor_CollectionPtr Getcontributor() const = 0;

	/**
	 * Set contributor element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_contributor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcontributor(const EbuCorecoreMetadataType_contributor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get date element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_date_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_date_CollectionPtr Getdate() const = 0;

	/**
	 * Set date element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_date_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdate(const EbuCorecoreMetadataType_date_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get type element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_type_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_type_CollectionPtr Gettype() const = 0;

	/**
	 * Set type element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_type_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settype(const EbuCorecoreMetadataType_type_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get format element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_format_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_format_CollectionPtr Getformat() const = 0;

	/**
	 * Set format element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_format_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setformat(const EbuCorecoreMetadataType_format_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get identifier element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_identifier_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_identifier_CollectionPtr Getidentifier() const = 0;

	/**
	 * Set identifier element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_identifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setidentifier(const EbuCorecoreMetadataType_identifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get source element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_source_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_source_CollectionPtr Getsource() const = 0;

	/**
	 * Set source element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_source_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setsource(const EbuCorecoreMetadataType_source_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get language element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_language_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_language_CollectionPtr Getlanguage() const = 0;

	/**
	 * Set language element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_language_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setlanguage(const EbuCorecoreMetadataType_language_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relation element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_relation_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_relation_CollectionPtr Getrelation() const = 0;

	/**
	 * Set relation element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrelation(const EbuCorecoreMetadataType_relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isRelatedTo element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isRelatedTo_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isRelatedTo_CollectionPtr GetisRelatedTo() const = 0;

	/**
	 * Set isRelatedTo element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisRelatedTo(const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isNextInSequence element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isNextInSequence_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isNextInSequence_CollectionPtr GetisNextInSequence() const = 0;

	/**
	 * Set isNextInSequence element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisNextInSequence(const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isVersionOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isVersionOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isVersionOf_CollectionPtr GetisVersionOf() const = 0;

	/**
	 * Set isVersionOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isVersionOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisVersionOf(const EbuCorecoreMetadataType_isVersionOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasVersion element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasVersion_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasVersion_CollectionPtr GethasVersion() const = 0;

	/**
	 * Set hasVersion element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasVersion_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasVersion(const EbuCorecoreMetadataType_hasVersion_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isReplacedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isReplacedBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isReplacedBy_CollectionPtr GetisReplacedBy() const = 0;

	/**
	 * Set isReplacedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisReplacedBy(const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get replaces element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_replaces_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_replaces_CollectionPtr Getreplaces() const = 0;

	/**
	 * Set replaces element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_replaces_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setreplaces(const EbuCorecoreMetadataType_replaces_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isRequiredBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isRequiredBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isRequiredBy_CollectionPtr GetisRequiredBy() const = 0;

	/**
	 * Set isRequiredBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisRequiredBy(const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get requires element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_requires_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_requires_CollectionPtr Getrequires() const = 0;

	/**
	 * Set requires element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_requires_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrequires(const EbuCorecoreMetadataType_requires_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isPartOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isPartOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isPartOf_CollectionPtr GetisPartOf() const = 0;

	/**
	 * Set isPartOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isPartOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisPartOf(const EbuCorecoreMetadataType_isPartOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasPart_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasPart_CollectionPtr GethasPart() const = 0;

	/**
	 * Set hasPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasPart_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasPart(const EbuCorecoreMetadataType_hasPart_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasTrackPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasTrackPart_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasTrackPart_CollectionPtr GethasTrackPart() const = 0;

	/**
	 * Set hasTrackPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasTrackPart(const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isReferencedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isReferencedBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isReferencedBy_CollectionPtr GetisReferencedBy() const = 0;

	/**
	 * Set isReferencedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisReferencedBy(const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get references element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_references_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_references_CollectionPtr Getreferences() const = 0;

	/**
	 * Set references element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_references_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setreferences(const EbuCorecoreMetadataType_references_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isFormatOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isFormatOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isFormatOf_CollectionPtr GetisFormatOf() const = 0;

	/**
	 * Set isFormatOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isFormatOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisFormatOf(const EbuCorecoreMetadataType_isFormatOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasFormat element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasFormat_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasFormat_CollectionPtr GethasFormat() const = 0;

	/**
	 * Set hasFormat element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasFormat(const EbuCorecoreMetadataType_hasFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isEpisodeOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr GetisEpisodeOf() const = 0;

	/**
	 * Set isEpisodeOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisEpisodeOf(const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isSeasonOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isSeasonOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isSeasonOf_CollectionPtr GetisSeasonOf() const = 0;

	/**
	 * Set isSeasonOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisSeasonOf(const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasEpisode element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasEpisode_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasEpisode_CollectionPtr GethasEpisode() const = 0;

	/**
	 * Set hasEpisode element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasEpisode_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasEpisode(const EbuCorecoreMetadataType_hasEpisode_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasSeason element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasSeason_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasSeason_CollectionPtr GethasSeason() const = 0;

	/**
	 * Set hasSeason element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasSeason_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasSeason(const EbuCorecoreMetadataType_hasSeason_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get isMemberOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isMemberOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isMemberOf_CollectionPtr GetisMemberOf() const = 0;

	/**
	 * Set isMemberOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isMemberOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisMemberOf(const EbuCorecoreMetadataType_isMemberOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get hasMember element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasMember_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasMember_CollectionPtr GethasMember() const = 0;

	/**
	 * Set hasMember element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasMember_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasMember(const EbuCorecoreMetadataType_hasMember_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get sameAs element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_sameAs_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_sameAs_CollectionPtr GetsameAs() const = 0;

	/**
	 * Set sameAs element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_sameAs_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetsameAs(const EbuCorecoreMetadataType_sameAs_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get coverage element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_coverage_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_coverage_CollectionPtr Getcoverage() const = 0;

	/**
	 * Set coverage element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_coverage_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcoverage(const EbuCorecoreMetadataType_coverage_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get rights element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_rights_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_rights_CollectionPtr Getrights() const = 0;

	/**
	 * Set rights element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_rights_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrights(const EbuCorecoreMetadataType_rights_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get version element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_version_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_version_CollectionPtr Getversion() const = 0;

	/**
	 * Set version element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_version_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setversion(const EbuCorecoreMetadataType_version_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get publicationHistory element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_publicationHistory_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_publicationHistory_CollectionPtr GetpublicationHistory() const = 0;

	/**
	 * Set publicationHistory element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_publicationHistory_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetpublicationHistory(const EbuCorecoreMetadataType_publicationHistory_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get planning element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_planning_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_planning_CollectionPtr Getplanning() const = 0;

	/**
	 * Set planning element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_planning_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setplanning(const EbuCorecoreMetadataType_planning_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get rating element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_rating_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_rating_CollectionPtr Getrating() const = 0;

	/**
	 * Set rating element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_rating_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrating(const EbuCorecoreMetadataType_rating_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get part element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_part_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_part_CollectionPtr Getpart() const = 0;

	/**
	 * Set part element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_part_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setpart(const EbuCorecoreMetadataType_part_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of partType.
	 * Currently this type contains 45 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:title</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:alternativeTitle</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:creator</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:subject</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:description</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publisher</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:contributor</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:date</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:type</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:format</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:identifier</li>
	 * <li>http://purl.org/dc/elements/1.1/:source</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:language</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relation</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isRelatedTo</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isNextInSequence</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isVersionOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasVersion</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isReplacedBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:replaces</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isRequiredBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:requires</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isPartOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasPart</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasTrackPart</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isReferencedBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:references</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isFormatOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isEpisodeOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isSeasonOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasEpisode</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasSeason</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isMemberOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasMember</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:sameAs</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:coverage</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:rights</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:version</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publicationHistory</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:planning</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:rating</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:part</li>
	 * <li>partStartTime</li>
	 * <li>partDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorepartType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorepartType_ExtPropDef.h

};




/*
 * Generated class EbuCorepartType<br>
 * Located at: EBU_CORE_20140318.xsd, line 596<br>
 * Classified: Class<br>
 * Derived from: coreMetadataType (Class)<br>
 * Defined in EBU_CORE_20140318.xsd, line 596.<br>
 */
class DC1_EXPORT EbuCorepartType :
		public IEbuCorepartType
{
	friend class EbuCorepartTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of EbuCorepartType
	 
	 * @return Dc1Ptr< EbuCorecoreMetadataType >
	 */
	virtual Dc1Ptr< EbuCorecoreMetadataType > GetBase() const;
	/*
	 * Get partId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartId() const;

	/*
	 * Get partId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartId() const;

	/*
	 * Set partId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartId();

	/*
	 * Get partName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartName() const;

	/*
	 * Get partName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartName() const;

	/*
	 * Set partName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartName();

	/*
	 * Get partDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpartDefinition() const;

	/*
	 * Get partDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartDefinition() const;

	/*
	 * Set partDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpartDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartDefinition();

	/*
	 * Get partNumber attribute.
	 * @return int
	 */
	virtual int GetpartNumber() const;

	/*
	 * Get partNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartNumber() const;

	/*
	 * Set partNumber attribute.
	 * @param item int
	 */
	virtual void SetpartNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartNumber();

	/*
	 * Get partTotalNumber attribute.
	 * @return int
	 */
	virtual int GetpartTotalNumber() const;

	/*
	 * Get partTotalNumber attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpartTotalNumber() const;

	/*
	 * Set partTotalNumber attribute.
	 * @param item int
	 */
	virtual void SetpartTotalNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partTotalNumber attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartTotalNumber();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get partStartTime element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetpartStartTime() const;

	virtual bool IsValidpartStartTime() const;

	/*
	 * Set partStartTime element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetpartStartTime(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partStartTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartStartTime();

	/*
	 * Get partDuration element.
	 * @return EbuCoredurationPtr
	 */
	virtual EbuCoredurationPtr GetpartDuration() const;

	virtual bool IsValidpartDuration() const;

	/*
	 * Set partDuration element.
	 * @param item const EbuCoredurationPtr &
	 */
	virtual void SetpartDuration(const EbuCoredurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate partDuration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepartDuration();

		//@}
		/* @name Sequence

		 */
		//@{
	/*
	 * Get title element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_title_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_title_CollectionPtr Gettitle() const;

	/*
	 * Set title element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_title_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settitle(const EbuCorecoreMetadataType_title_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get alternativeTitle element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_alternativeTitle_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_alternativeTitle_CollectionPtr GetalternativeTitle() const;

	/*
	 * Set alternativeTitle element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetalternativeTitle(const EbuCorecoreMetadataType_alternativeTitle_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get creator element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_creator_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_creator_CollectionPtr Getcreator() const;

	/*
	 * Set creator element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_creator_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcreator(const EbuCorecoreMetadataType_creator_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get subject element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_subject_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_subject_CollectionPtr Getsubject() const;

	/*
	 * Set subject element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_subject_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setsubject(const EbuCorecoreMetadataType_subject_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get description element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_description_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_description_CollectionPtr Getdescription() const;

	/*
	 * Set description element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_description_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdescription(const EbuCorecoreMetadataType_description_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get publisher element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_publisher_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_publisher_CollectionPtr Getpublisher() const;

	/*
	 * Set publisher element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_publisher_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setpublisher(const EbuCorecoreMetadataType_publisher_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get contributor element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_contributor_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_contributor_CollectionPtr Getcontributor() const;

	/*
	 * Set contributor element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_contributor_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcontributor(const EbuCorecoreMetadataType_contributor_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get date element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_date_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_date_CollectionPtr Getdate() const;

	/*
	 * Set date element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_date_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdate(const EbuCorecoreMetadataType_date_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get type element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_type_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_type_CollectionPtr Gettype() const;

	/*
	 * Set type element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_type_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settype(const EbuCorecoreMetadataType_type_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get format element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_format_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_format_CollectionPtr Getformat() const;

	/*
	 * Set format element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_format_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setformat(const EbuCorecoreMetadataType_format_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get identifier element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_identifier_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_identifier_CollectionPtr Getidentifier() const;

	/*
	 * Set identifier element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_identifier_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setidentifier(const EbuCorecoreMetadataType_identifier_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get source element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_source_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_source_CollectionPtr Getsource() const;

	/*
	 * Set source element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_source_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setsource(const EbuCorecoreMetadataType_source_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get language element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_language_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_language_CollectionPtr Getlanguage() const;

	/*
	 * Set language element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_language_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setlanguage(const EbuCorecoreMetadataType_language_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relation element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_relation_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_relation_CollectionPtr Getrelation() const;

	/*
	 * Set relation element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_relation_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrelation(const EbuCorecoreMetadataType_relation_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isRelatedTo element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isRelatedTo_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isRelatedTo_CollectionPtr GetisRelatedTo() const;

	/*
	 * Set isRelatedTo element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisRelatedTo(const EbuCorecoreMetadataType_isRelatedTo_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isNextInSequence element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isNextInSequence_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isNextInSequence_CollectionPtr GetisNextInSequence() const;

	/*
	 * Set isNextInSequence element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisNextInSequence(const EbuCorecoreMetadataType_isNextInSequence_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isVersionOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isVersionOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isVersionOf_CollectionPtr GetisVersionOf() const;

	/*
	 * Set isVersionOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isVersionOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisVersionOf(const EbuCorecoreMetadataType_isVersionOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasVersion element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasVersion_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasVersion_CollectionPtr GethasVersion() const;

	/*
	 * Set hasVersion element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasVersion_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasVersion(const EbuCorecoreMetadataType_hasVersion_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isReplacedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isReplacedBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isReplacedBy_CollectionPtr GetisReplacedBy() const;

	/*
	 * Set isReplacedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisReplacedBy(const EbuCorecoreMetadataType_isReplacedBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get replaces element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_replaces_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_replaces_CollectionPtr Getreplaces() const;

	/*
	 * Set replaces element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_replaces_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setreplaces(const EbuCorecoreMetadataType_replaces_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isRequiredBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isRequiredBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isRequiredBy_CollectionPtr GetisRequiredBy() const;

	/*
	 * Set isRequiredBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisRequiredBy(const EbuCorecoreMetadataType_isRequiredBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get requires element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_requires_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_requires_CollectionPtr Getrequires() const;

	/*
	 * Set requires element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_requires_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrequires(const EbuCorecoreMetadataType_requires_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isPartOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isPartOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isPartOf_CollectionPtr GetisPartOf() const;

	/*
	 * Set isPartOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isPartOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisPartOf(const EbuCorecoreMetadataType_isPartOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasPart_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasPart_CollectionPtr GethasPart() const;

	/*
	 * Set hasPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasPart_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasPart(const EbuCorecoreMetadataType_hasPart_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasTrackPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasTrackPart_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasTrackPart_CollectionPtr GethasTrackPart() const;

	/*
	 * Set hasTrackPart element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasTrackPart(const EbuCorecoreMetadataType_hasTrackPart_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isReferencedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isReferencedBy_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isReferencedBy_CollectionPtr GetisReferencedBy() const;

	/*
	 * Set isReferencedBy element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisReferencedBy(const EbuCorecoreMetadataType_isReferencedBy_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get references element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_references_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_references_CollectionPtr Getreferences() const;

	/*
	 * Set references element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_references_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setreferences(const EbuCorecoreMetadataType_references_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isFormatOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isFormatOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isFormatOf_CollectionPtr GetisFormatOf() const;

	/*
	 * Set isFormatOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isFormatOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisFormatOf(const EbuCorecoreMetadataType_isFormatOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasFormat element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasFormat_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasFormat_CollectionPtr GethasFormat() const;

	/*
	 * Set hasFormat element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasFormat(const EbuCorecoreMetadataType_hasFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isEpisodeOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr GetisEpisodeOf() const;

	/*
	 * Set isEpisodeOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisEpisodeOf(const EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isSeasonOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isSeasonOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isSeasonOf_CollectionPtr GetisSeasonOf() const;

	/*
	 * Set isSeasonOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisSeasonOf(const EbuCorecoreMetadataType_isSeasonOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasEpisode element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasEpisode_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasEpisode_CollectionPtr GethasEpisode() const;

	/*
	 * Set hasEpisode element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasEpisode_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasEpisode(const EbuCorecoreMetadataType_hasEpisode_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasSeason element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasSeason_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasSeason_CollectionPtr GethasSeason() const;

	/*
	 * Set hasSeason element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasSeason_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasSeason(const EbuCorecoreMetadataType_hasSeason_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get isMemberOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_isMemberOf_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_isMemberOf_CollectionPtr GetisMemberOf() const;

	/*
	 * Set isMemberOf element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_isMemberOf_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetisMemberOf(const EbuCorecoreMetadataType_isMemberOf_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get hasMember element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_hasMember_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_hasMember_CollectionPtr GethasMember() const;

	/*
	 * Set hasMember element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_hasMember_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SethasMember(const EbuCorecoreMetadataType_hasMember_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get sameAs element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_sameAs_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_sameAs_CollectionPtr GetsameAs() const;

	/*
	 * Set sameAs element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_sameAs_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetsameAs(const EbuCorecoreMetadataType_sameAs_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get coverage element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_coverage_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_coverage_CollectionPtr Getcoverage() const;

	/*
	 * Set coverage element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_coverage_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcoverage(const EbuCorecoreMetadataType_coverage_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get rights element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_rights_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_rights_CollectionPtr Getrights() const;

	/*
	 * Set rights element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_rights_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrights(const EbuCorecoreMetadataType_rights_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get version element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_version_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_version_CollectionPtr Getversion() const;

	/*
	 * Set version element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_version_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setversion(const EbuCorecoreMetadataType_version_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get publicationHistory element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_publicationHistory_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_publicationHistory_CollectionPtr GetpublicationHistory() const;

	/*
	 * Set publicationHistory element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_publicationHistory_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetpublicationHistory(const EbuCorecoreMetadataType_publicationHistory_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get planning element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_planning_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_planning_CollectionPtr Getplanning() const;

	/*
	 * Set planning element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_planning_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setplanning(const EbuCorecoreMetadataType_planning_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get rating element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_rating_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_rating_CollectionPtr Getrating() const;

	/*
	 * Set rating element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_rating_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setrating(const EbuCorecoreMetadataType_rating_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get part element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @return EbuCorecoreMetadataType_part_CollectionPtr
	 */
	virtual EbuCorecoreMetadataType_part_CollectionPtr Getpart() const;

	/*
	 * Set part element.
<br>
	 * (Inherited from EbuCorecoreMetadataType)
	 * @param item const EbuCorecoreMetadataType_part_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setpart(const EbuCorecoreMetadataType_part_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of partType.
	 * Currently this type contains 45 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:title</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:alternativeTitle</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:creator</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:subject</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:description</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publisher</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:contributor</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:date</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:type</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:format</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:identifier</li>
	 * <li>http://purl.org/dc/elements/1.1/:source</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:language</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relation</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isRelatedTo</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isNextInSequence</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isVersionOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasVersion</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isReplacedBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:replaces</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isRequiredBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:requires</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isPartOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasPart</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasTrackPart</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isReferencedBy</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:references</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isFormatOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isEpisodeOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isSeasonOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasEpisode</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasSeason</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:isMemberOf</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:hasMember</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:sameAs</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:coverage</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:rights</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:version</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publicationHistory</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:planning</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:rating</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:part</li>
	 * <li>partStartTime</li>
	 * <li>partDuration</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorepartType_ExtMyMethodDef.h


public:

	virtual ~EbuCorepartType();

protected:
	EbuCorepartType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_partId;
	bool m_partId_Exist;
	XMLCh * m_partName;
	bool m_partName_Exist;
	XMLCh * m_partDefinition;
	bool m_partDefinition_Exist;
	int m_partNumber;
	bool m_partNumber_Exist;
	int m_partTotalNumber;
	bool m_partTotalNumber_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;

	// Base Class
	Dc1Ptr< EbuCorecoreMetadataType > m_Base;

	EbuCoretimePtr m_partStartTime;
	bool m_partStartTime_Exist; // For optional elements 
	EbuCoredurationPtr m_partDuration;
	bool m_partDuration_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCorepartType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _471EBUCOREPARTTYPE_H

