/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _121EBUCOREAUDIOFORMATEXTENDEDTYPE_H
#define _121EBUCOREAUDIOFORMATEXTENDEDTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioFormatExtendedTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtInclude.h


class IEbuCoreaudioFormatExtendedType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType> EbuCoreaudioFormatExtendedPtr;
class IEbuCoreaudioFormatExtendedType_audioProgramme_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioProgramme_CollectionType > EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioContent_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioContent_CollectionType > EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioObject_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioObject_CollectionType > EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr;
class IEbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType > EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr;

/** 
 * Generated interface IEbuCoreaudioFormatExtendedType for class EbuCoreaudioFormatExtendedType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3807<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioFormatExtendedType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioFormatExtendedType();
	virtual ~IEbuCoreaudioFormatExtendedType();
	/**
	 * Get audioFormatExtendedID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedID() const = 0;

	/**
	 * Get audioFormatExtendedID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedID() const = 0;

	/**
	 * Set audioFormatExtendedID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatExtendedID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedID() = 0;

	/**
	 * Get audioFormatExtendedName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedName() const = 0;

	/**
	 * Get audioFormatExtendedName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedName() const = 0;

	/**
	 * Set audioFormatExtendedName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatExtendedName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedName() = 0;

	/**
	 * Get audioFormatExtendedDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedDefinition() const = 0;

	/**
	 * Get audioFormatExtendedDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedDefinition() const = 0;

	/**
	 * Set audioFormatExtendedDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatExtendedDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedDefinition() = 0;

	/**
	 * Get audioFormatExtendedVersion attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedVersion() const = 0;

	/**
	 * Get audioFormatExtendedVersion attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedVersion() const = 0;

	/**
	 * Set audioFormatExtendedVersion attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedVersion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatExtendedVersion attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedVersion() = 0;

	/**
	 * Get audioFormatExtendedPresenceFlag attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedPresenceFlag() const = 0;

	/**
	 * Get audioFormatExtendedPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedPresenceFlag() const = 0;

	/**
	 * Set audioFormatExtendedPresenceFlag attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedPresenceFlag(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatExtendedPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedPresenceFlag() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get audioProgramme element.
	 * @return EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr GetaudioProgramme() const = 0;

	/**
	 * Set audioProgramme element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioProgramme(const EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioContent element.
	 * @return EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr GetaudioContent() const = 0;

	/**
	 * Set audioContent element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioContent(const EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioObject element.
	 * @return EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr GetaudioObject() const = 0;

	/**
	 * Set audioObject element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioObject(const EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioPackFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr GetaudioPackFormat() const = 0;

	/**
	 * Set audioPackFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormat(const EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioChannelFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr GetaudioChannelFormat() const = 0;

	/**
	 * Set audioChannelFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioChannelFormat(const EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioBlockFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr GetaudioBlockFormat() const = 0;

	/**
	 * Set audioBlockFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioBlockFormat(const EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioStreamFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr GetaudioStreamFormat() const = 0;

	/**
	 * Set audioStreamFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioStreamFormat(const EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioTrackFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr GetaudioTrackFormat() const = 0;

	/**
	 * Set audioTrackFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackFormat(const EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioTrackUID element.
	 * @return EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr GetaudioTrackUID() const = 0;

	/**
	 * Set audioTrackUID element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackUID(const EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioFormatExtendedType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioProgramme</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioContent</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioObject</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackUID</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioFormatExtendedType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3807<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 3807.<br>
 */
class DC1_EXPORT EbuCoreaudioFormatExtendedType :
		public IEbuCoreaudioFormatExtendedType
{
	friend class EbuCoreaudioFormatExtendedTypeFactory; // constructs objects

public:
	/*
	 * Get audioFormatExtendedID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedID() const;

	/*
	 * Get audioFormatExtendedID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedID() const;

	/*
	 * Set audioFormatExtendedID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatExtendedID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedID();

	/*
	 * Get audioFormatExtendedName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedName() const;

	/*
	 * Get audioFormatExtendedName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedName() const;

	/*
	 * Set audioFormatExtendedName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatExtendedName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedName();

	/*
	 * Get audioFormatExtendedDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedDefinition() const;

	/*
	 * Get audioFormatExtendedDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedDefinition() const;

	/*
	 * Set audioFormatExtendedDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatExtendedDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedDefinition();

	/*
	 * Get audioFormatExtendedVersion attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedVersion() const;

	/*
	 * Get audioFormatExtendedVersion attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedVersion() const;

	/*
	 * Set audioFormatExtendedVersion attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedVersion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatExtendedVersion attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedVersion();

	/*
	 * Get audioFormatExtendedPresenceFlag attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatExtendedPresenceFlag() const;

	/*
	 * Get audioFormatExtendedPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatExtendedPresenceFlag() const;

	/*
	 * Set audioFormatExtendedPresenceFlag attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatExtendedPresenceFlag(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatExtendedPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatExtendedPresenceFlag();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get audioProgramme element.
	 * @return EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr GetaudioProgramme() const;

	/*
	 * Set audioProgramme element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioProgramme(const EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioContent element.
	 * @return EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr GetaudioContent() const;

	/*
	 * Set audioContent element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioContent(const EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioObject element.
	 * @return EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr GetaudioObject() const;

	/*
	 * Set audioObject element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioObject(const EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioPackFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr GetaudioPackFormat() const;

	/*
	 * Set audioPackFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormat(const EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioChannelFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr GetaudioChannelFormat() const;

	/*
	 * Set audioChannelFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioChannelFormat(const EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioBlockFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr GetaudioBlockFormat() const;

	/*
	 * Set audioBlockFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioBlockFormat(const EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioStreamFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr GetaudioStreamFormat() const;

	/*
	 * Set audioStreamFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioStreamFormat(const EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioTrackFormat element.
	 * @return EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr GetaudioTrackFormat() const;

	/*
	 * Set audioTrackFormat element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackFormat(const EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioTrackUID element.
	 * @return EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr
	 */
	virtual EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr GetaudioTrackUID() const;

	/*
	 * Set audioTrackUID element.
	 * @param item const EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackUID(const EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioFormatExtendedType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioProgramme</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioContent</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioObject</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackUID</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioFormatExtendedType();

protected:
	EbuCoreaudioFormatExtendedType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_audioFormatExtendedID;
	bool m_audioFormatExtendedID_Exist;
	XMLCh * m_audioFormatExtendedName;
	bool m_audioFormatExtendedName_Exist;
	XMLCh * m_audioFormatExtendedDefinition;
	bool m_audioFormatExtendedDefinition_Exist;
	XMLCh * m_audioFormatExtendedVersion;
	bool m_audioFormatExtendedVersion_Exist;
	XMLCh * m_audioFormatExtendedPresenceFlag;
	bool m_audioFormatExtendedPresenceFlag_Exist;


	EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr m_audioProgramme;
	EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr m_audioContent;
	EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr m_audioObject;
	EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr m_audioPackFormat;
	EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr m_audioChannelFormat;
	EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr m_audioBlockFormat;
	EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr m_audioStreamFormat;
	EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr m_audioTrackFormat;
	EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr m_audioTrackUID;

// no includefile for extension defined 
// file EbuCoreaudioFormatExtendedType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _121EBUCOREAUDIOFORMATEXTENDEDTYPE_H

