/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _509EBUCORERATIONALTYPE_H
#define _509EBUCORERATIONALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"

#include "EbuCorerationalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorerationalType_ExtInclude.h


class IEbuCorerationalType;
typedef Dc1Ptr< IEbuCorerationalType> EbuCorerationalPtr;

/** 
 * Generated interface IEbuCorerationalType for class EbuCorerationalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4876<br>
 * Classified: Class<br>
 * Derived from: long (Value)<br>
 */
class EBUCORE_EXPORT IEbuCorerationalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorerationalType();
	virtual ~IEbuCorerationalType();
	/**
	 * Sets the content of EbuCorerationalType	 * @param item int
	 */
	virtual void SetContent(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of EbuCorerationalType 
	 * @return int
	 */
	virtual int GetContent() const = 0;

	/**
	 * Get factorNumerator attribute.
	 * @return int
	 */
	virtual int GetfactorNumerator() const = 0;

	/**
	 * Get factorNumerator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfactorNumerator() const = 0;

	/**
	 * Set factorNumerator attribute.
	 * @param item int
	 */
	virtual void SetfactorNumerator(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate factorNumerator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefactorNumerator() = 0;

	/**
	 * Get factorDenominator attribute.
	 * @return int
	 */
	virtual int GetfactorDenominator() const = 0;

	/**
	 * Get factorDenominator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfactorDenominator() const = 0;

	/**
	 * Set factorDenominator attribute.
	 * @param item int
	 */
	virtual void SetfactorDenominator(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate factorDenominator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefactorDenominator() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of rationalType.
	 * Currently just supports rudimentary XPath expressions as rationalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorerationalType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorerationalType_ExtPropDef.h

};




/*
 * Generated class EbuCorerationalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4876<br>
 * Classified: Class<br>
 * Derived from: long (Value)<br>
 * Defined in EBU_CORE_20140318.xsd, line 4876.<br>
 */
class DC1_EXPORT EbuCorerationalType :
		public IEbuCorerationalType
{
	friend class EbuCorerationalTypeFactory; // constructs objects

public:
	/*
	 * Sets the content of EbuCorerationalType	 * @param item int
	 */
	virtual void SetContent(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of EbuCorerationalType 
	 * @return int
	 */
	virtual int GetContent() const;

	/*
	 * Get factorNumerator attribute.
	 * @return int
	 */
	virtual int GetfactorNumerator() const;

	/*
	 * Get factorNumerator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfactorNumerator() const;

	/*
	 * Set factorNumerator attribute.
	 * @param item int
	 */
	virtual void SetfactorNumerator(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate factorNumerator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefactorNumerator();

	/*
	 * Get factorDenominator attribute.
	 * @return int
	 */
	virtual int GetfactorDenominator() const;

	/*
	 * Get factorDenominator attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfactorDenominator() const;

	/*
	 * Set factorDenominator attribute.
	 * @param item int
	 */
	virtual void SetfactorDenominator(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate factorDenominator attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefactorDenominator();

	/*
	 * Gets or creates Dc1NodePtr child elements of rationalType.
	 * Currently just supports rudimentary XPath expressions as rationalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorerationalType_ExtMyMethodDef.h


public:

	virtual ~EbuCorerationalType();

protected:
	EbuCorerationalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_factorNumerator;
	bool m_factorNumerator_Exist;
	int m_factorNumerator_Default;
	int m_factorDenominator;
	bool m_factorDenominator_Exist;
	int m_factorDenominator_Default;

	// Base Value
	int m_Base;


// no includefile for extension defined 
// file EbuCorerationalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _509EBUCORERATIONALTYPE_H

