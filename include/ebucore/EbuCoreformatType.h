/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _393EBUCOREFORMATTYPE_H
#define _393EBUCOREFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreformatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreformatType_ExtInclude.h


class IEbuCoreformatType;
typedef Dc1Ptr< IEbuCoreformatType> EbuCoreformatPtr;
class IDc1elementType;
typedef Dc1Ptr< IDc1elementType > Dc1elementPtr;
class IEbuCoreformatType_medium_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_medium_CollectionType > EbuCoreformatType_medium_CollectionPtr;
class IEbuCoreformatType_imageFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_imageFormat_CollectionType > EbuCoreformatType_imageFormat_CollectionPtr;
class IEbuCoreformatType_videoFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_videoFormat_CollectionType > EbuCoreformatType_videoFormat_CollectionPtr;
class IEbuCoreformatType_audioFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_audioFormat_CollectionType > EbuCoreformatType_audioFormat_CollectionPtr;
class IEbuCoreformatType_audioFormatExtended_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_audioFormatExtended_CollectionType > EbuCoreformatType_audioFormatExtended_CollectionPtr;
class IEbuCoreformatType_containerFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_containerFormat_CollectionType > EbuCoreformatType_containerFormat_CollectionPtr;
class IEbuCoresigningFormatType;
typedef Dc1Ptr< IEbuCoresigningFormatType > EbuCoresigningFormatPtr;
class IEbuCoreformatType_dataFormat_CollectionType;
typedef Dc1Ptr< IEbuCoreformatType_dataFormat_CollectionType > EbuCoreformatType_dataFormat_CollectionPtr;
class IEbuCoretimeType;
typedef Dc1Ptr< IEbuCoretimeType > EbuCoretimePtr;
class IEbuCoredurationType;
typedef Dc1Ptr< IEbuCoredurationType > EbuCoredurationPtr;
class IEbuCorefileInfo;
typedef Dc1Ptr< IEbuCorefileInfo > EbuCorefileInfoPtr;
class IEbuCoredocumentFormatType;
typedef Dc1Ptr< IEbuCoredocumentFormatType > EbuCoredocumentFormatPtr;
class IEbuCoretechnicalAttributes;
typedef Dc1Ptr< IEbuCoretechnicalAttributes > EbuCoretechnicalAttributesPtr;
class IEbuCoreformatType_dateCreated_LocalType;
typedef Dc1Ptr< IEbuCoreformatType_dateCreated_LocalType > EbuCoreformatType_dateCreated_LocalPtr;
class IEbuCoreformatType_dateModified_LocalType;
typedef Dc1Ptr< IEbuCoreformatType_dateModified_LocalType > EbuCoreformatType_dateModified_LocalPtr;

/** 
 * Generated interface IEbuCoreformatType for class EbuCoreformatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1799<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreformatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreformatType();
	virtual ~IEbuCoreformatType();
	/**
	 * Get formatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatId() const = 0;

	/**
	 * Get formatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatId() const = 0;

	/**
	 * Set formatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatId() = 0;

	/**
	 * Get formatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatVersionId() const = 0;

	/**
	 * Get formatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatVersionId() const = 0;

	/**
	 * Set formatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatVersionId() = 0;

	/**
	 * Get formatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatName() const = 0;

	/**
	 * Get formatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatName() const = 0;

	/**
	 * Set formatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatName() = 0;

	/**
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const = 0;

	/**
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const = 0;

	/**
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get format element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getformat() const = 0;

	virtual bool IsValidformat() const = 0;

	/**
	 * Set format element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setformat(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate format element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateformat() = 0;

	/**
	 * Get medium element.
	 * @return EbuCoreformatType_medium_CollectionPtr
	 */
	virtual EbuCoreformatType_medium_CollectionPtr Getmedium() const = 0;

	/**
	 * Set medium element.
	 * @param item const EbuCoreformatType_medium_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setmedium(const EbuCoreformatType_medium_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get imageFormat element.
	 * @return EbuCoreformatType_imageFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_imageFormat_CollectionPtr GetimageFormat() const = 0;

	/**
	 * Set imageFormat element.
	 * @param item const EbuCoreformatType_imageFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetimageFormat(const EbuCoreformatType_imageFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get videoFormat element.
	 * @return EbuCoreformatType_videoFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_videoFormat_CollectionPtr GetvideoFormat() const = 0;

	/**
	 * Set videoFormat element.
	 * @param item const EbuCoreformatType_videoFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetvideoFormat(const EbuCoreformatType_videoFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioFormat element.
	 * @return EbuCoreformatType_audioFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_audioFormat_CollectionPtr GetaudioFormat() const = 0;

	/**
	 * Set audioFormat element.
	 * @param item const EbuCoreformatType_audioFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioFormat(const EbuCoreformatType_audioFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioFormatExtended element.
	 * @return EbuCoreformatType_audioFormatExtended_CollectionPtr
	 */
	virtual EbuCoreformatType_audioFormatExtended_CollectionPtr GetaudioFormatExtended() const = 0;

	/**
	 * Set audioFormatExtended element.
	 * @param item const EbuCoreformatType_audioFormatExtended_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioFormatExtended(const EbuCoreformatType_audioFormatExtended_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get containerFormat element.
	 * @return EbuCoreformatType_containerFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_containerFormat_CollectionPtr GetcontainerFormat() const = 0;

	/**
	 * Set containerFormat element.
	 * @param item const EbuCoreformatType_containerFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetcontainerFormat(const EbuCoreformatType_containerFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get signingFormat element.
	 * @return EbuCoresigningFormatPtr
	 */
	virtual EbuCoresigningFormatPtr GetsigningFormat() const = 0;

	virtual bool IsValidsigningFormat() const = 0;

	/**
	 * Set signingFormat element.
	 * @param item const EbuCoresigningFormatPtr &
	 */
	virtual void SetsigningFormat(const EbuCoresigningFormatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate signingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesigningFormat() = 0;

	/**
	 * Get dataFormat element.
	 * @return EbuCoreformatType_dataFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_dataFormat_CollectionPtr GetdataFormat() const = 0;

	/**
	 * Set dataFormat element.
	 * @param item const EbuCoreformatType_dataFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetdataFormat(const EbuCoreformatType_dataFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get start element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr Getstart() const = 0;

	virtual bool IsValidstart() const = 0;

	/**
	 * Set start element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void Setstart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get end element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr Getend() const = 0;

	virtual bool IsValidend() const = 0;

	/**
	 * Set end element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void Setend(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate end element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateend() = 0;

	/**
	 * Get duration element.
	 * @return EbuCoredurationPtr
	 */
	virtual EbuCoredurationPtr Getduration() const = 0;

	virtual bool IsValidduration() const = 0;

	/**
	 * Set duration element.
	 * @param item const EbuCoredurationPtr &
	 */
	virtual void Setduration(const EbuCoredurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate duration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration() = 0;

	/**
	 * Get fileInfo element.
	 * @return EbuCorefileInfoPtr
	 */
	virtual EbuCorefileInfoPtr GetfileInfo() const = 0;

	/**
	 * Set fileInfo element.
	 * @param item const EbuCorefileInfoPtr &
	 */
	// Mandatory			
	virtual void SetfileInfo(const EbuCorefileInfoPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get documentFormat element.
	 * @return EbuCoredocumentFormatPtr
	 */
	virtual EbuCoredocumentFormatPtr GetdocumentFormat() const = 0;

	virtual bool IsValiddocumentFormat() const = 0;

	/**
	 * Set documentFormat element.
	 * @param item const EbuCoredocumentFormatPtr &
	 */
	virtual void SetdocumentFormat(const EbuCoredocumentFormatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate documentFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentFormat() = 0;

	/**
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const = 0;

	/**
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get dateCreated element.
	 * @return EbuCoreformatType_dateCreated_LocalPtr
	 */
	virtual EbuCoreformatType_dateCreated_LocalPtr GetdateCreated() const = 0;

	virtual bool IsValiddateCreated() const = 0;

	/**
	 * Set dateCreated element.
	 * @param item const EbuCoreformatType_dateCreated_LocalPtr &
	 */
	virtual void SetdateCreated(const EbuCoreformatType_dateCreated_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dateCreated element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateCreated() = 0;

	/**
	 * Get dateModified element.
	 * @return EbuCoreformatType_dateModified_LocalPtr
	 */
	virtual EbuCoreformatType_dateModified_LocalPtr GetdateModified() const = 0;

	virtual bool IsValiddateModified() const = 0;

	/**
	 * Set dateModified element.
	 * @param item const EbuCoreformatType_dateModified_LocalPtr &
	 */
	virtual void SetdateModified(const EbuCoreformatType_dateModified_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dateModified element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateModified() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of formatType.
	 * Currently this type contains 17 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>format</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:medium</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:imageFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:videoFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtended</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:containerFormat</li>
	 * <li>signingFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:dataFormat</li>
	 * <li>start</li>
	 * <li>end</li>
	 * <li>duration</li>
	 * <li>fileInfo</li>
	 * <li>documentFormat</li>
	 * <li>technicalAttributes</li>
	 * <li>dateCreated</li>
	 * <li>dateModified</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreformatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreformatType_ExtPropDef.h

};




/*
 * Generated class EbuCoreformatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1799<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 1799.<br>
 */
class DC1_EXPORT EbuCoreformatType :
		public IEbuCoreformatType
{
	friend class EbuCoreformatTypeFactory; // constructs objects

public:
	/*
	 * Get formatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatId() const;

	/*
	 * Get formatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatId() const;

	/*
	 * Set formatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatId();

	/*
	 * Get formatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatVersionId() const;

	/*
	 * Get formatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatVersionId() const;

	/*
	 * Set formatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatVersionId();

	/*
	 * Get formatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatName() const;

	/*
	 * Get formatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatName() const;

	/*
	 * Set formatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatName();

	/*
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const;

	/*
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const;

	/*
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get format element.
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getformat() const;

	virtual bool IsValidformat() const;

	/*
	 * Set format element.
	 * @param item const Dc1elementPtr &
	 */
	virtual void Setformat(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate format element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateformat();

	/*
	 * Get medium element.
	 * @return EbuCoreformatType_medium_CollectionPtr
	 */
	virtual EbuCoreformatType_medium_CollectionPtr Getmedium() const;

	/*
	 * Set medium element.
	 * @param item const EbuCoreformatType_medium_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setmedium(const EbuCoreformatType_medium_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get imageFormat element.
	 * @return EbuCoreformatType_imageFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_imageFormat_CollectionPtr GetimageFormat() const;

	/*
	 * Set imageFormat element.
	 * @param item const EbuCoreformatType_imageFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetimageFormat(const EbuCoreformatType_imageFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get videoFormat element.
	 * @return EbuCoreformatType_videoFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_videoFormat_CollectionPtr GetvideoFormat() const;

	/*
	 * Set videoFormat element.
	 * @param item const EbuCoreformatType_videoFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetvideoFormat(const EbuCoreformatType_videoFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioFormat element.
	 * @return EbuCoreformatType_audioFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_audioFormat_CollectionPtr GetaudioFormat() const;

	/*
	 * Set audioFormat element.
	 * @param item const EbuCoreformatType_audioFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioFormat(const EbuCoreformatType_audioFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioFormatExtended element.
	 * @return EbuCoreformatType_audioFormatExtended_CollectionPtr
	 */
	virtual EbuCoreformatType_audioFormatExtended_CollectionPtr GetaudioFormatExtended() const;

	/*
	 * Set audioFormatExtended element.
	 * @param item const EbuCoreformatType_audioFormatExtended_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioFormatExtended(const EbuCoreformatType_audioFormatExtended_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get containerFormat element.
	 * @return EbuCoreformatType_containerFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_containerFormat_CollectionPtr GetcontainerFormat() const;

	/*
	 * Set containerFormat element.
	 * @param item const EbuCoreformatType_containerFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetcontainerFormat(const EbuCoreformatType_containerFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get signingFormat element.
	 * @return EbuCoresigningFormatPtr
	 */
	virtual EbuCoresigningFormatPtr GetsigningFormat() const;

	virtual bool IsValidsigningFormat() const;

	/*
	 * Set signingFormat element.
	 * @param item const EbuCoresigningFormatPtr &
	 */
	virtual void SetsigningFormat(const EbuCoresigningFormatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate signingFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesigningFormat();

	/*
	 * Get dataFormat element.
	 * @return EbuCoreformatType_dataFormat_CollectionPtr
	 */
	virtual EbuCoreformatType_dataFormat_CollectionPtr GetdataFormat() const;

	/*
	 * Set dataFormat element.
	 * @param item const EbuCoreformatType_dataFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetdataFormat(const EbuCoreformatType_dataFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get start element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr Getstart() const;

	virtual bool IsValidstart() const;

	/*
	 * Set start element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void Setstart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get end element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr Getend() const;

	virtual bool IsValidend() const;

	/*
	 * Set end element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void Setend(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate end element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateend();

	/*
	 * Get duration element.
	 * @return EbuCoredurationPtr
	 */
	virtual EbuCoredurationPtr Getduration() const;

	virtual bool IsValidduration() const;

	/*
	 * Set duration element.
	 * @param item const EbuCoredurationPtr &
	 */
	virtual void Setduration(const EbuCoredurationPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate duration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration();

	/*
	 * Get fileInfo element.
	 * @return EbuCorefileInfoPtr
	 */
	virtual EbuCorefileInfoPtr GetfileInfo() const;

	/*
	 * Set fileInfo element.
	 * @param item const EbuCorefileInfoPtr &
	 */
	// Mandatory			
	virtual void SetfileInfo(const EbuCorefileInfoPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get documentFormat element.
	 * @return EbuCoredocumentFormatPtr
	 */
	virtual EbuCoredocumentFormatPtr GetdocumentFormat() const;

	virtual bool IsValiddocumentFormat() const;

	/*
	 * Set documentFormat element.
	 * @param item const EbuCoredocumentFormatPtr &
	 */
	virtual void SetdocumentFormat(const EbuCoredocumentFormatPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate documentFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentFormat();

	/*
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const;

	/*
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get dateCreated element.
	 * @return EbuCoreformatType_dateCreated_LocalPtr
	 */
	virtual EbuCoreformatType_dateCreated_LocalPtr GetdateCreated() const;

	virtual bool IsValiddateCreated() const;

	/*
	 * Set dateCreated element.
	 * @param item const EbuCoreformatType_dateCreated_LocalPtr &
	 */
	virtual void SetdateCreated(const EbuCoreformatType_dateCreated_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dateCreated element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateCreated();

	/*
	 * Get dateModified element.
	 * @return EbuCoreformatType_dateModified_LocalPtr
	 */
	virtual EbuCoreformatType_dateModified_LocalPtr GetdateModified() const;

	virtual bool IsValiddateModified() const;

	/*
	 * Set dateModified element.
	 * @param item const EbuCoreformatType_dateModified_LocalPtr &
	 */
	virtual void SetdateModified(const EbuCoreformatType_dateModified_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dateModified element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateModified();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of formatType.
	 * Currently this type contains 17 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>format</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:medium</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:imageFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:videoFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtended</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:containerFormat</li>
	 * <li>signingFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:dataFormat</li>
	 * <li>start</li>
	 * <li>end</li>
	 * <li>duration</li>
	 * <li>fileInfo</li>
	 * <li>documentFormat</li>
	 * <li>technicalAttributes</li>
	 * <li>dateCreated</li>
	 * <li>dateModified</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreformatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreformatType();

protected:
	EbuCoreformatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_formatId;
	bool m_formatId_Exist;
	XMLCh * m_formatVersionId;
	bool m_formatVersionId_Exist;
	XMLCh * m_formatName;
	bool m_formatName_Exist;
	XMLCh * m_formatDefinition;
	bool m_formatDefinition_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;


	Dc1elementPtr m_format;
	bool m_format_Exist; // For optional elements 
	EbuCoreformatType_medium_CollectionPtr m_medium;
	EbuCoreformatType_imageFormat_CollectionPtr m_imageFormat;
	EbuCoreformatType_videoFormat_CollectionPtr m_videoFormat;
	EbuCoreformatType_audioFormat_CollectionPtr m_audioFormat;
	EbuCoreformatType_audioFormatExtended_CollectionPtr m_audioFormatExtended;
	EbuCoreformatType_containerFormat_CollectionPtr m_containerFormat;
	EbuCoresigningFormatPtr m_signingFormat;
	bool m_signingFormat_Exist; // For optional elements 
	EbuCoreformatType_dataFormat_CollectionPtr m_dataFormat;
	EbuCoretimePtr m_start;
	bool m_start_Exist; // For optional elements 
	EbuCoretimePtr m_end;
	bool m_end_Exist; // For optional elements 
	EbuCoredurationPtr m_duration;
	bool m_duration_Exist; // For optional elements 
	EbuCorefileInfoPtr m_fileInfo;
	EbuCoredocumentFormatPtr m_documentFormat;
	bool m_documentFormat_Exist; // For optional elements 
	EbuCoretechnicalAttributesPtr m_technicalAttributes;
	EbuCoreformatType_dateCreated_LocalPtr m_dateCreated;
	bool m_dateCreated_Exist; // For optional elements 
	EbuCoreformatType_dateModified_LocalPtr m_dateModified;
	bool m_dateModified_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreformatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _393EBUCOREFORMATTYPE_H

