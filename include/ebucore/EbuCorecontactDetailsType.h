/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _201EBUCORECONTACTDETAILSTYPE_H
#define _201EBUCORECONTACTDETAILSTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCorecontactDetailsTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtInclude.h


class IEbuCorecontactDetailsType;
typedef Dc1Ptr< IEbuCorecontactDetailsType> EbuCorecontactDetailsPtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IEbuCorecontactDetailsType_name_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_name_CollectionType > EbuCorecontactDetailsType_name_CollectionPtr;
class IEbuCoreelementType;
typedef Dc1Ptr< IEbuCoreelementType > EbuCoreelementPtr;
class IEbuCorecontactDetailsType_otherGivenName_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_otherGivenName_CollectionType > EbuCorecontactDetailsType_otherGivenName_CollectionPtr;
class IEbuCorecontactDetailsType_username_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_username_CollectionType > EbuCorecontactDetailsType_username_CollectionPtr;
class IEbuCorecontactDetailsType_nickname_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_nickname_CollectionType > EbuCorecontactDetailsType_nickname_CollectionPtr;
class IEbuCorecontactDetailsType_details_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_details_CollectionType > EbuCorecontactDetailsType_details_CollectionPtr;
class IEbuCorecontactDetailsType_stageName_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_stageName_CollectionType > EbuCorecontactDetailsType_stageName_CollectionPtr;
class IEbuCorecontactDetailsType_relatedInformationLink_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_relatedInformationLink_CollectionType > EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr;
class IEbuCorecontactDetailsType_relatedContacts_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_relatedContacts_CollectionType > EbuCorecontactDetailsType_relatedContacts_CollectionPtr;
class IEbuCorecontactDetailsType_skill_CollectionType;
typedef Dc1Ptr< IEbuCorecontactDetailsType_skill_CollectionType > EbuCorecontactDetailsType_skill_CollectionPtr;

/** 
 * Generated interface IEbuCorecontactDetailsType for class EbuCorecontactDetailsType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2133<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorecontactDetailsType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorecontactDetailsType();
	virtual ~IEbuCorecontactDetailsType();
	/**
	 * Get contactId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetcontactId() const = 0;

	/**
	 * Get contactId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontactId() const = 0;

	/**
	 * Set contactId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetcontactId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate contactId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontactId() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get lastUpdate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetlastUpdate() const = 0;

	/**
	 * Get lastUpdate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastUpdate() const = 0;

	/**
	 * Set lastUpdate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lastUpdate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastUpdate() = 0;

		/** @name Sequence

		 */
		//@{
		/** @name Choice

		 */
		//@{
	/**
	 * Get name element.
	 * @return EbuCorecontactDetailsType_name_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_name_CollectionPtr Getname() const = 0;

	/**
	 * Set name element.
	 * @param item const EbuCorecontactDetailsType_name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setname(const EbuCorecontactDetailsType_name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get givenName element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetgivenName() const = 0;

	virtual bool IsValidgivenName() const = 0;

	/**
	 * Set givenName element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetgivenName(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate givenName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategivenName() = 0;

	/**
	 * Get familyName element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetfamilyName() const = 0;

	virtual bool IsValidfamilyName() const = 0;

	/**
	 * Set familyName element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetfamilyName(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate familyName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefamilyName() = 0;

	/**
	 * Get otherGivenName element.
	 * @return EbuCorecontactDetailsType_otherGivenName_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_otherGivenName_CollectionPtr GetotherGivenName() const = 0;

	/**
	 * Set otherGivenName element.
	 * @param item const EbuCorecontactDetailsType_otherGivenName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetotherGivenName(const EbuCorecontactDetailsType_otherGivenName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get suffix element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getsuffix() const = 0;

	virtual bool IsValidsuffix() const = 0;

	/**
	 * Set suffix element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setsuffix(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate suffix element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesuffix() = 0;

	/**
	 * Get salutation element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getsalutation() const = 0;

	virtual bool IsValidsalutation() const = 0;

	/**
	 * Set salutation element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setsalutation(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate salutation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesalutation() = 0;

	/**
	 * Get birthDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetbirthDate() const = 0;

	/**
	 * Set birthDate element.
	 * @param item const W3CdatePtr &
	 */
	// Mandatory			
	virtual void SetbirthDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get deathDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetdeathDate() const = 0;

	/**
	 * Set deathDate element.
	 * @param item const W3CdatePtr &
	 */
	// Mandatory			
	virtual void SetdeathDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
		//@}
	/**
	 * Get username element.
	 * @return EbuCorecontactDetailsType_username_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_username_CollectionPtr Getusername() const = 0;

	/**
	 * Set username element.
	 * @param item const EbuCorecontactDetailsType_username_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setusername(const EbuCorecontactDetailsType_username_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get nickname element.
	 * @return EbuCorecontactDetailsType_nickname_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_nickname_CollectionPtr Getnickname() const = 0;

	/**
	 * Set nickname element.
	 * @param item const EbuCorecontactDetailsType_nickname_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setnickname(const EbuCorecontactDetailsType_nickname_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get occupation element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getoccupation() const = 0;

	virtual bool IsValidoccupation() const = 0;

	/**
	 * Set occupation element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setoccupation(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate occupation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoccupation() = 0;

	/**
	 * Get details element.
	 * @return EbuCorecontactDetailsType_details_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_details_CollectionPtr Getdetails() const = 0;

	/**
	 * Set details element.
	 * @param item const EbuCorecontactDetailsType_details_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdetails(const EbuCorecontactDetailsType_details_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get stageName element.
	 * @return EbuCorecontactDetailsType_stageName_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_stageName_CollectionPtr GetstageName() const = 0;

	/**
	 * Set stageName element.
	 * @param item const EbuCorecontactDetailsType_stageName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetstageName(const EbuCorecontactDetailsType_stageName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get guest element.
	 * @return bool
	 */
	virtual bool Getguest() const = 0;

	virtual bool IsValidguest() const = 0;

	/**
	 * Set guest element.
	 * @param item bool
	 */
	virtual void Setguest(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate guest element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateguest() = 0;

	/**
	 * Get gender element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getgender() const = 0;

	virtual bool IsValidgender() const = 0;

	/**
	 * Set gender element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setgender(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate gender element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidategender() = 0;

	/**
	 * Get relatedInformationLink element.
	 * @return EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr GetrelatedInformationLink() const = 0;

	/**
	 * Set relatedInformationLink element.
	 * @param item const EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedInformationLink(const EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relatedContacts element.
	 * @return EbuCorecontactDetailsType_relatedContacts_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_relatedContacts_CollectionPtr GetrelatedContacts() const = 0;

	/**
	 * Set relatedContacts element.
	 * @param item const EbuCorecontactDetailsType_relatedContacts_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedContacts(const EbuCorecontactDetailsType_relatedContacts_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get skill element.
	 * @return EbuCorecontactDetailsType_skill_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_skill_CollectionPtr Getskill() const = 0;

	/**
	 * Set skill element.
	 * @param item const EbuCorecontactDetailsType_skill_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setskill(const EbuCorecontactDetailsType_skill_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of contactDetailsType.
	 * Currently this type contains 17 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:name</li>
	 * <li>givenName</li>
	 * <li>familyName</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:otherGivenName</li>
	 * <li>suffix</li>
	 * <li>salutation</li>
	 * <li>birthDate</li>
	 * <li>deathDate</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:username</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:nickname</li>
	 * <li>occupation</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:details</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:stageName</li>
	 * <li>gender</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedContacts</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:skill</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtPropDef.h

};




/*
 * Generated class EbuCorecontactDetailsType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2133<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 2133.<br>
 */
class DC1_EXPORT EbuCorecontactDetailsType :
		public IEbuCorecontactDetailsType
{
	friend class EbuCorecontactDetailsTypeFactory; // constructs objects

public:
	/*
	 * Get contactId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetcontactId() const;

	/*
	 * Get contactId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistcontactId() const;

	/*
	 * Set contactId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetcontactId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate contactId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatecontactId();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get lastUpdate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetlastUpdate() const;

	/*
	 * Get lastUpdate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastUpdate() const;

	/*
	 * Set lastUpdate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lastUpdate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastUpdate();

		/* @name Sequence

		 */
		//@{
		/* @name Choice

		 */
		//@{
	/*
	 * Get name element.
	 * @return EbuCorecontactDetailsType_name_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_name_CollectionPtr Getname() const;

	/*
	 * Set name element.
	 * @param item const EbuCorecontactDetailsType_name_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setname(const EbuCorecontactDetailsType_name_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		/* @name Sequence

		 */
		//@{
	/*
	 * Get givenName element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetgivenName() const;

	virtual bool IsValidgivenName() const;

	/*
	 * Set givenName element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetgivenName(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate givenName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategivenName();

	/*
	 * Get familyName element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetfamilyName() const;

	virtual bool IsValidfamilyName() const;

	/*
	 * Set familyName element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetfamilyName(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate familyName element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefamilyName();

	/*
	 * Get otherGivenName element.
	 * @return EbuCorecontactDetailsType_otherGivenName_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_otherGivenName_CollectionPtr GetotherGivenName() const;

	/*
	 * Set otherGivenName element.
	 * @param item const EbuCorecontactDetailsType_otherGivenName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetotherGivenName(const EbuCorecontactDetailsType_otherGivenName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get suffix element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getsuffix() const;

	virtual bool IsValidsuffix() const;

	/*
	 * Set suffix element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setsuffix(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate suffix element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesuffix();

	/*
	 * Get salutation element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getsalutation() const;

	virtual bool IsValidsalutation() const;

	/*
	 * Set salutation element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setsalutation(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate salutation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatesalutation();

	/*
	 * Get birthDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetbirthDate() const;

	/*
	 * Set birthDate element.
	 * @param item const W3CdatePtr &
	 */
	// Mandatory			
	virtual void SetbirthDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get deathDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetdeathDate() const;

	/*
	 * Set deathDate element.
	 * @param item const W3CdatePtr &
	 */
	// Mandatory			
	virtual void SetdeathDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
		//@}
	/*
	 * Get username element.
	 * @return EbuCorecontactDetailsType_username_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_username_CollectionPtr Getusername() const;

	/*
	 * Set username element.
	 * @param item const EbuCorecontactDetailsType_username_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setusername(const EbuCorecontactDetailsType_username_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get nickname element.
	 * @return EbuCorecontactDetailsType_nickname_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_nickname_CollectionPtr Getnickname() const;

	/*
	 * Set nickname element.
	 * @param item const EbuCorecontactDetailsType_nickname_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setnickname(const EbuCorecontactDetailsType_nickname_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get occupation element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getoccupation() const;

	virtual bool IsValidoccupation() const;

	/*
	 * Set occupation element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setoccupation(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate occupation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateoccupation();

	/*
	 * Get details element.
	 * @return EbuCorecontactDetailsType_details_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_details_CollectionPtr Getdetails() const;

	/*
	 * Set details element.
	 * @param item const EbuCorecontactDetailsType_details_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdetails(const EbuCorecontactDetailsType_details_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get stageName element.
	 * @return EbuCorecontactDetailsType_stageName_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_stageName_CollectionPtr GetstageName() const;

	/*
	 * Set stageName element.
	 * @param item const EbuCorecontactDetailsType_stageName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetstageName(const EbuCorecontactDetailsType_stageName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get guest element.
	 * @return bool
	 */
	virtual bool Getguest() const;

	virtual bool IsValidguest() const;

	/*
	 * Set guest element.
	 * @param item bool
	 */
	virtual void Setguest(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate guest element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateguest();

	/*
	 * Get gender element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr Getgender() const;

	virtual bool IsValidgender() const;

	/*
	 * Set gender element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void Setgender(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate gender element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidategender();

	/*
	 * Get relatedInformationLink element.
	 * @return EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr GetrelatedInformationLink() const;

	/*
	 * Set relatedInformationLink element.
	 * @param item const EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedInformationLink(const EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relatedContacts element.
	 * @return EbuCorecontactDetailsType_relatedContacts_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_relatedContacts_CollectionPtr GetrelatedContacts() const;

	/*
	 * Set relatedContacts element.
	 * @param item const EbuCorecontactDetailsType_relatedContacts_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedContacts(const EbuCorecontactDetailsType_relatedContacts_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get skill element.
	 * @return EbuCorecontactDetailsType_skill_CollectionPtr
	 */
	virtual EbuCorecontactDetailsType_skill_CollectionPtr Getskill() const;

	/*
	 * Set skill element.
	 * @param item const EbuCorecontactDetailsType_skill_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setskill(const EbuCorecontactDetailsType_skill_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of contactDetailsType.
	 * Currently this type contains 17 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:name</li>
	 * <li>givenName</li>
	 * <li>familyName</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:otherGivenName</li>
	 * <li>suffix</li>
	 * <li>salutation</li>
	 * <li>birthDate</li>
	 * <li>deathDate</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:username</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:nickname</li>
	 * <li>occupation</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:details</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:stageName</li>
	 * <li>gender</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedContacts</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:skill</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMyMethodDef.h


public:

	virtual ~EbuCorecontactDetailsType();

protected:
	EbuCorecontactDetailsType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_contactId;
	bool m_contactId_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;
	W3CdatePtr m_lastUpdate;
	bool m_lastUpdate_Exist;


// DefinionChoice
	EbuCorecontactDetailsType_name_CollectionPtr m_name;
	EbuCoreelementPtr m_givenName;
	bool m_givenName_Exist; // For optional elements 
	EbuCoreelementPtr m_familyName;
	bool m_familyName_Exist; // For optional elements 
	EbuCorecontactDetailsType_otherGivenName_CollectionPtr m_otherGivenName;
	EbuCoreelementPtr m_suffix;
	bool m_suffix_Exist; // For optional elements 
	EbuCoreelementPtr m_salutation;
	bool m_salutation_Exist; // For optional elements 
	W3CdatePtr m_birthDate;
	W3CdatePtr m_deathDate;
// End DefinionChoice
	EbuCorecontactDetailsType_username_CollectionPtr m_username;
	EbuCorecontactDetailsType_nickname_CollectionPtr m_nickname;
	EbuCoreelementPtr m_occupation;
	bool m_occupation_Exist; // For optional elements 
	EbuCorecontactDetailsType_details_CollectionPtr m_details;
	EbuCorecontactDetailsType_stageName_CollectionPtr m_stageName;
	bool m_guest;
	bool m_guest_Exist; // For optional elements 
	EbuCoreelementPtr m_gender;
	bool m_gender_Exist; // For optional elements 
	EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr m_relatedInformationLink;
	EbuCorecontactDetailsType_relatedContacts_CollectionPtr m_relatedContacts;
	EbuCorecontactDetailsType_skill_CollectionPtr m_skill;

// no includefile for extension defined 
// file EbuCorecontactDetailsType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _201EBUCORECONTACTDETAILSTYPE_H

