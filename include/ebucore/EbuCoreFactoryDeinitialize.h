/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// DeInitialize section of Dc1Factory.h
// To remove a bunch of registered factories from the list.
{
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:addressType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:addressType_addressLine_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:addressType_country_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:alternativeDateType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType_title_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormatType_lineNumber_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:aspectRatioType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_position_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_speakerLabel_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_speakerLabel_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType_audioBlockFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType_frequency_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioContentType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioContentType_audioObjectIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioBlockFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioChannelFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioContent_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioObject_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioPackFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioProgramme_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioStreamFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioTrackFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioTrackUID_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioEncoding_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrack_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrack_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrackConfiguration_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioMXFLookUpType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioObjectIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioPackFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioTrackUIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType_audioChannelFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType_audioPackFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioProgrammeType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioProgrammeType_audioContentIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioChannelFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioPackFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioTrackFormatIDRef_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:audioTrackUIDType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Boolean");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:captioningFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:codecType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coefficientType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:compoundNameType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_details_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_name_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_nickname_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_otherGivenName_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedContacts_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedInformationLink_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedInformationLink_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_skill_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_stageName_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_username_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:containerFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:containerFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_alternativeTitle_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_contributor_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_coverage_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_creator_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_date_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_description_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_format_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasEpisode_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasMember_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasPart_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasSeason_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasTrackPart_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasTrackPart_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasVersion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_identifier_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isEpisodeOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isFormatOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isMemberOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isNextInSequence_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isPartOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isReferencedBy_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isRelatedTo_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isReplacedBy_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isRequiredBy_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isSeasonOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isVersionOf_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_language_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_part_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_planning_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_publicationHistory_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_publisher_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_rating_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_references_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_relation_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_replaces_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_requires_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_rights_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_sameAs_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_source_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_subject_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_title_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_type_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_version_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:coverageType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_ancillaryDataFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_captioningFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_subtitlingFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_alternative_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_copyrighted_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_created_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_date_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_digitised_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_issued_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_modified_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dateType_released_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:descriptionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:descriptionType_description_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:detailsType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:detailsType_emailAddress_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:dimensionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:durationType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:durationType_duration_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ebuCoreMainType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:editUnitNumberType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:elementType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:entityType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:entityType_contactDetails_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:entityType_organisationDetails_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:entityType_role_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:entityType_role_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:fileInfo");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_locator_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_locator_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_mimeType_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_mimeType_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Float");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_audioFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_audioFormatExtended_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_containerFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_dataFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateCreated_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateModified_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_imageFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_medium_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_medium_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:formatType_videoFormat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:frequency_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:hashType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:hashType_hashFunction_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:identifierType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_imageEncoding_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Int16");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Int32");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Int64");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:Int8");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:languageType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:lengthType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:locationType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:locationType_coordinates_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:matrixType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:matrixType_coefficient_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDepartmentType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_contacts_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_details_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_organisationCode_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_organisationName_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_relatedInformationLink_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_relatedInformationLink_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:PackageIDType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:partType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:periodOfTimeType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:planningType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:planningType_publicationEvent_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:position_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationChannelType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationEventType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationEventType_publicationRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationHistoryType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationHistoryType_publicationEvent_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationMediumType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:publicationServiceType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingExclusionRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingLink_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingScaleMaxValue_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingScaleMinValue_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingValue_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rationalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:regionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:regionType_country_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:regionType_countryRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:regionType_countryRegion_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:relationType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_contactDetails_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_copyrightStatement_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_disclaimer_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_exploitationIssues_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rights_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rightsAttributedId_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rightsHolder_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:signingFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:spatialType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:spatialType_location_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:String");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:subjectType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:subjectType_subject_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:subjectType_subjectDefinition_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType_targetExclusionRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType_targetRegion_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRationalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeBoolean_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeByte_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeFloat_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeInteger_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeLong_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeRational_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeShort_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeString_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedByte_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedInteger_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedLong_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedShort_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUri_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUriType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:temporalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:temporalType_PeriodOfTime_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:timecodeType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:timeType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:timeType_time_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:titleType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:titleType_title_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_genre_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_genre_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_objectType_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_objectType_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_targetAudience_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:typeType_type_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:UInt16");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:UInt32");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:UInt64");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:UInt8");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:versionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_aspectRatio_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_comment_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_height_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_height_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoEncoding_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoTrack_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoTrack_LocalType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_width_CollectionType");
  Dc1Factory::UnregisterFactory("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_width_LocalType");
}
