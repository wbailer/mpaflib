/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef EBUCOREFACTORYDEFINES_H
#define EBUCOREFACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for extension classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfaddressType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType")))
#define CreateaddressType (Dc1Factory::CreateObject(TypeOfaddressType))
class EbuCoreaddressType;
class IEbuCoreaddressType;
/** Smart pointer for instance of IEbuCoreaddressType */
typedef Dc1Ptr< IEbuCoreaddressType > EbuCoreaddressPtr;

#define TypeOfaddressType_addressLine_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType_addressLine_CollectionType")))
#define CreateaddressType_addressLine_CollectionType (Dc1Factory::CreateObject(TypeOfaddressType_addressLine_CollectionType))
class EbuCoreaddressType_addressLine_CollectionType;
class IEbuCoreaddressType_addressLine_CollectionType;
/** Smart pointer for instance of IEbuCoreaddressType_addressLine_CollectionType */
typedef Dc1Ptr< IEbuCoreaddressType_addressLine_CollectionType > EbuCoreaddressType_addressLine_CollectionPtr;

#define TypeOfaddressType_country_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType_country_LocalType")))
#define CreateaddressType_country_LocalType (Dc1Factory::CreateObject(TypeOfaddressType_country_LocalType))
class EbuCoreaddressType_country_LocalType;
class IEbuCoreaddressType_country_LocalType;
/** Smart pointer for instance of IEbuCoreaddressType_country_LocalType */
typedef Dc1Ptr< IEbuCoreaddressType_country_LocalType > EbuCoreaddressType_country_LocalPtr;

#define TypeOfalternativeDateType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeDateType")))
#define CreatealternativeDateType (Dc1Factory::CreateObject(TypeOfalternativeDateType))
class EbuCorealternativeDateType;
class IEbuCorealternativeDateType;
/** Smart pointer for instance of IEbuCorealternativeDateType */
typedef Dc1Ptr< IEbuCorealternativeDateType > EbuCorealternativeDatePtr;

#define TypeOfalternativeTitleType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType")))
#define CreatealternativeTitleType (Dc1Factory::CreateObject(TypeOfalternativeTitleType))
class EbuCorealternativeTitleType;
class IEbuCorealternativeTitleType;
/** Smart pointer for instance of IEbuCorealternativeTitleType */
typedef Dc1Ptr< IEbuCorealternativeTitleType > EbuCorealternativeTitlePtr;

#define TypeOfalternativeTitleType_title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:alternativeTitleType_title_CollectionType")))
#define CreatealternativeTitleType_title_CollectionType (Dc1Factory::CreateObject(TypeOfalternativeTitleType_title_CollectionType))
class EbuCorealternativeTitleType_title_CollectionType;
class IEbuCorealternativeTitleType_title_CollectionType;
/** Smart pointer for instance of IEbuCorealternativeTitleType_title_CollectionType */
typedef Dc1Ptr< IEbuCorealternativeTitleType_title_CollectionType > EbuCorealternativeTitleType_title_CollectionPtr;

#define TypeOfancillaryDataFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormatType")))
#define CreateancillaryDataFormatType (Dc1Factory::CreateObject(TypeOfancillaryDataFormatType))
class EbuCoreancillaryDataFormatType;
class IEbuCoreancillaryDataFormatType;
/** Smart pointer for instance of IEbuCoreancillaryDataFormatType */
typedef Dc1Ptr< IEbuCoreancillaryDataFormatType > EbuCoreancillaryDataFormatPtr;

#define TypeOfancillaryDataFormatType_lineNumber_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormatType_lineNumber_CollectionType")))
#define CreateancillaryDataFormatType_lineNumber_CollectionType (Dc1Factory::CreateObject(TypeOfancillaryDataFormatType_lineNumber_CollectionType))
class EbuCoreancillaryDataFormatType_lineNumber_CollectionType;
class IEbuCoreancillaryDataFormatType_lineNumber_CollectionType;
/** Smart pointer for instance of IEbuCoreancillaryDataFormatType_lineNumber_CollectionType */
typedef Dc1Ptr< IEbuCoreancillaryDataFormatType_lineNumber_CollectionType > EbuCoreancillaryDataFormatType_lineNumber_CollectionPtr;

#define TypeOfaspectRatioType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:aspectRatioType")))
#define CreateaspectRatioType (Dc1Factory::CreateObject(TypeOfaspectRatioType))
class EbuCoreaspectRatioType;
class IEbuCoreaspectRatioType;
/** Smart pointer for instance of IEbuCoreaspectRatioType */
typedef Dc1Ptr< IEbuCoreaspectRatioType > EbuCoreaspectRatioPtr;

#define TypeOfaudioBlockFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType")))
#define CreateaudioBlockFormatType (Dc1Factory::CreateObject(TypeOfaudioBlockFormatType))
class EbuCoreaudioBlockFormatType;
class IEbuCoreaudioBlockFormatType;
/** Smart pointer for instance of IEbuCoreaudioBlockFormatType */
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType > EbuCoreaudioBlockFormatPtr;

#define TypeOfaudioBlockFormatType_position_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_position_CollectionType")))
#define CreateaudioBlockFormatType_position_CollectionType (Dc1Factory::CreateObject(TypeOfaudioBlockFormatType_position_CollectionType))
class EbuCoreaudioBlockFormatType_position_CollectionType;
class IEbuCoreaudioBlockFormatType_position_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioBlockFormatType_position_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType_position_CollectionType > EbuCoreaudioBlockFormatType_position_CollectionPtr;

#define TypeOfaudioBlockFormatType_speakerLabel_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_speakerLabel_CollectionType")))
#define CreateaudioBlockFormatType_speakerLabel_CollectionType (Dc1Factory::CreateObject(TypeOfaudioBlockFormatType_speakerLabel_CollectionType))
class EbuCoreaudioBlockFormatType_speakerLabel_CollectionType;
class IEbuCoreaudioBlockFormatType_speakerLabel_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioBlockFormatType_speakerLabel_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType_speakerLabel_CollectionType > EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr;

#define TypeOfaudioBlockFormatType_speakerLabel_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType_speakerLabel_LocalType")))
#define CreateaudioBlockFormatType_speakerLabel_LocalType (Dc1Factory::CreateObject(TypeOfaudioBlockFormatType_speakerLabel_LocalType))
class EbuCoreaudioBlockFormatType_speakerLabel_LocalType;
class IEbuCoreaudioBlockFormatType_speakerLabel_LocalType;
/** Smart pointer for instance of IEbuCoreaudioBlockFormatType_speakerLabel_LocalType */
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType_speakerLabel_LocalType > EbuCoreaudioBlockFormatType_speakerLabel_LocalPtr;

#define TypeOfaudioChannelFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType")))
#define CreateaudioChannelFormatType (Dc1Factory::CreateObject(TypeOfaudioChannelFormatType))
class EbuCoreaudioChannelFormatType;
class IEbuCoreaudioChannelFormatType;
/** Smart pointer for instance of IEbuCoreaudioChannelFormatType */
typedef Dc1Ptr< IEbuCoreaudioChannelFormatType > EbuCoreaudioChannelFormatPtr;

#define TypeOfaudioChannelFormatType_audioBlockFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType_audioBlockFormat_CollectionType")))
#define CreateaudioChannelFormatType_audioBlockFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioChannelFormatType_audioBlockFormat_CollectionType))
class EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType;
class IEbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType > EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr;

#define TypeOfaudioChannelFormatType_frequency_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatType_frequency_CollectionType")))
#define CreateaudioChannelFormatType_frequency_CollectionType (Dc1Factory::CreateObject(TypeOfaudioChannelFormatType_frequency_CollectionType))
class EbuCoreaudioChannelFormatType_frequency_CollectionType;
class IEbuCoreaudioChannelFormatType_frequency_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioChannelFormatType_frequency_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioChannelFormatType_frequency_CollectionType > EbuCoreaudioChannelFormatType_frequency_CollectionPtr;

#define TypeOfaudioContentType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioContentType")))
#define CreateaudioContentType (Dc1Factory::CreateObject(TypeOfaudioContentType))
class EbuCoreaudioContentType;
class IEbuCoreaudioContentType;
/** Smart pointer for instance of IEbuCoreaudioContentType */
typedef Dc1Ptr< IEbuCoreaudioContentType > EbuCoreaudioContentPtr;

#define TypeOfaudioContentType_audioObjectIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioContentType_audioObjectIDRef_CollectionType")))
#define CreateaudioContentType_audioObjectIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioContentType_audioObjectIDRef_CollectionType))
class EbuCoreaudioContentType_audioObjectIDRef_CollectionType;
class IEbuCoreaudioContentType_audioObjectIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioContentType_audioObjectIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioContentType_audioObjectIDRef_CollectionType > EbuCoreaudioContentType_audioObjectIDRef_CollectionPtr;

#define TypeOfaudioFormatExtendedType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType")))
#define CreateaudioFormatExtendedType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType))
class EbuCoreaudioFormatExtendedType;
class IEbuCoreaudioFormatExtendedType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType > EbuCoreaudioFormatExtendedPtr;

#define TypeOfaudioFormatExtendedType_audioBlockFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioBlockFormat_CollectionType")))
#define CreateaudioFormatExtendedType_audioBlockFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioBlockFormat_CollectionType))
class EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioChannelFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioChannelFormat_CollectionType")))
#define CreateaudioFormatExtendedType_audioChannelFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioChannelFormat_CollectionType))
class EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioContent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioContent_CollectionType")))
#define CreateaudioFormatExtendedType_audioContent_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioContent_CollectionType))
class EbuCoreaudioFormatExtendedType_audioContent_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioContent_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioContent_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioContent_CollectionType > EbuCoreaudioFormatExtendedType_audioContent_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioObject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioObject_CollectionType")))
#define CreateaudioFormatExtendedType_audioObject_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioObject_CollectionType))
class EbuCoreaudioFormatExtendedType_audioObject_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioObject_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioObject_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioObject_CollectionType > EbuCoreaudioFormatExtendedType_audioObject_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioPackFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioPackFormat_CollectionType")))
#define CreateaudioFormatExtendedType_audioPackFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioPackFormat_CollectionType))
class EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioProgramme_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioProgramme_CollectionType")))
#define CreateaudioFormatExtendedType_audioProgramme_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioProgramme_CollectionType))
class EbuCoreaudioFormatExtendedType_audioProgramme_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioProgramme_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioProgramme_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioProgramme_CollectionType > EbuCoreaudioFormatExtendedType_audioProgramme_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioStreamFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioStreamFormat_CollectionType")))
#define CreateaudioFormatExtendedType_audioStreamFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioStreamFormat_CollectionType))
class EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioTrackFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioTrackFormat_CollectionType")))
#define CreateaudioFormatExtendedType_audioTrackFormat_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioTrackFormat_CollectionType))
class EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType > EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionPtr;

#define TypeOfaudioFormatExtendedType_audioTrackUID_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatExtendedType_audioTrackUID_CollectionType")))
#define CreateaudioFormatExtendedType_audioTrackUID_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatExtendedType_audioTrackUID_CollectionType))
class EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType;
class IEbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType > EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionPtr;

#define TypeOfaudioFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType")))
#define CreateaudioFormatType (Dc1Factory::CreateObject(TypeOfaudioFormatType))
class EbuCoreaudioFormatType;
class IEbuCoreaudioFormatType;
/** Smart pointer for instance of IEbuCoreaudioFormatType */
typedef Dc1Ptr< IEbuCoreaudioFormatType > EbuCoreaudioFormatPtr;

#define TypeOfaudioFormatType_audioEncoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioEncoding_LocalType")))
#define CreateaudioFormatType_audioEncoding_LocalType (Dc1Factory::CreateObject(TypeOfaudioFormatType_audioEncoding_LocalType))
class EbuCoreaudioFormatType_audioEncoding_LocalType;
class IEbuCoreaudioFormatType_audioEncoding_LocalType;
/** Smart pointer for instance of IEbuCoreaudioFormatType_audioEncoding_LocalType */
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioEncoding_LocalType > EbuCoreaudioFormatType_audioEncoding_LocalPtr;

#define TypeOfaudioFormatType_audioTrack_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrack_CollectionType")))
#define CreateaudioFormatType_audioTrack_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatType_audioTrack_CollectionType))
class EbuCoreaudioFormatType_audioTrack_CollectionType;
class IEbuCoreaudioFormatType_audioTrack_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatType_audioTrack_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioTrack_CollectionType > EbuCoreaudioFormatType_audioTrack_CollectionPtr;

#define TypeOfaudioFormatType_audioTrack_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrack_LocalType")))
#define CreateaudioFormatType_audioTrack_LocalType (Dc1Factory::CreateObject(TypeOfaudioFormatType_audioTrack_LocalType))
class EbuCoreaudioFormatType_audioTrack_LocalType;
class IEbuCoreaudioFormatType_audioTrack_LocalType;
/** Smart pointer for instance of IEbuCoreaudioFormatType_audioTrack_LocalType */
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioTrack_LocalType > EbuCoreaudioFormatType_audioTrack_LocalPtr;

#define TypeOfaudioFormatType_audioTrackConfiguration_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_audioTrackConfiguration_LocalType")))
#define CreateaudioFormatType_audioTrackConfiguration_LocalType (Dc1Factory::CreateObject(TypeOfaudioFormatType_audioTrackConfiguration_LocalType))
class EbuCoreaudioFormatType_audioTrackConfiguration_LocalType;
class IEbuCoreaudioFormatType_audioTrackConfiguration_LocalType;
/** Smart pointer for instance of IEbuCoreaudioFormatType_audioTrackConfiguration_LocalType */
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioTrackConfiguration_LocalType > EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr;

#define TypeOfaudioFormatType_bitRateMode_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_bitRateMode_LocalType")))
#define CreateaudioFormatType_bitRateMode_LocalType (Dc1Factory::CreateObject(TypeOfaudioFormatType_bitRateMode_LocalType))
class EbuCoreaudioFormatType_bitRateMode_LocalType;
class IEbuCoreaudioFormatType_bitRateMode_LocalType;
// No smart pointer instance for IEbuCoreaudioFormatType_bitRateMode_LocalType

#define TypeOfaudioFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_comment_CollectionType")))
#define CreateaudioFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfaudioFormatType_comment_CollectionType))
class EbuCoreaudioFormatType_comment_CollectionType;
class IEbuCoreaudioFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioFormatType_comment_CollectionType > EbuCoreaudioFormatType_comment_CollectionPtr;

#define TypeOfaudioFormatType_sampleType_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioFormatType_sampleType_LocalType")))
#define CreateaudioFormatType_sampleType_LocalType (Dc1Factory::CreateObject(TypeOfaudioFormatType_sampleType_LocalType))
class EbuCoreaudioFormatType_sampleType_LocalType;
class IEbuCoreaudioFormatType_sampleType_LocalType;
// No smart pointer instance for IEbuCoreaudioFormatType_sampleType_LocalType

#define TypeOfaudioMXFLookUpType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioMXFLookUpType")))
#define CreateaudioMXFLookUpType (Dc1Factory::CreateObject(TypeOfaudioMXFLookUpType))
class EbuCoreaudioMXFLookUpType;
class IEbuCoreaudioMXFLookUpType;
/** Smart pointer for instance of IEbuCoreaudioMXFLookUpType */
typedef Dc1Ptr< IEbuCoreaudioMXFLookUpType > EbuCoreaudioMXFLookUpPtr;

#define TypeOfaudioObjectType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType")))
#define CreateaudioObjectType (Dc1Factory::CreateObject(TypeOfaudioObjectType))
class EbuCoreaudioObjectType;
class IEbuCoreaudioObjectType;
/** Smart pointer for instance of IEbuCoreaudioObjectType */
typedef Dc1Ptr< IEbuCoreaudioObjectType > EbuCoreaudioObjectPtr;

#define TypeOfaudioObjectType_audioObjectIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioObjectIDRef_CollectionType")))
#define CreateaudioObjectType_audioObjectIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioObjectType_audioObjectIDRef_CollectionType))
class EbuCoreaudioObjectType_audioObjectIDRef_CollectionType;
class IEbuCoreaudioObjectType_audioObjectIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioObjectType_audioObjectIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioObjectIDRef_CollectionType > EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr;

#define TypeOfaudioObjectType_audioPackFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioPackFormatIDRef_CollectionType")))
#define CreateaudioObjectType_audioPackFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioObjectType_audioPackFormatIDRef_CollectionType))
class EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType;
class IEbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType > EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr;

#define TypeOfaudioObjectType_audioTrackUIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioObjectType_audioTrackUIDRef_CollectionType")))
#define CreateaudioObjectType_audioTrackUIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioObjectType_audioTrackUIDRef_CollectionType))
class EbuCoreaudioObjectType_audioTrackUIDRef_CollectionType;
class IEbuCoreaudioObjectType_audioTrackUIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioObjectType_audioTrackUIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioTrackUIDRef_CollectionType > EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr;

#define TypeOfaudioPackFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType")))
#define CreateaudioPackFormatType (Dc1Factory::CreateObject(TypeOfaudioPackFormatType))
class EbuCoreaudioPackFormatType;
class IEbuCoreaudioPackFormatType;
/** Smart pointer for instance of IEbuCoreaudioPackFormatType */
typedef Dc1Ptr< IEbuCoreaudioPackFormatType > EbuCoreaudioPackFormatPtr;

#define TypeOfaudioPackFormatType_audioChannelFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType_audioChannelFormatIDRef_CollectionType")))
#define CreateaudioPackFormatType_audioChannelFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioPackFormatType_audioChannelFormatIDRef_CollectionType))
class EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType;
class IEbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType > EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionPtr;

#define TypeOfaudioPackFormatType_audioPackFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatType_audioPackFormatIDRef_CollectionType")))
#define CreateaudioPackFormatType_audioPackFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioPackFormatType_audioPackFormatIDRef_CollectionType))
class EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType;
class IEbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType > EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionPtr;

#define TypeOfaudioProgrammeType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioProgrammeType")))
#define CreateaudioProgrammeType (Dc1Factory::CreateObject(TypeOfaudioProgrammeType))
class EbuCoreaudioProgrammeType;
class IEbuCoreaudioProgrammeType;
/** Smart pointer for instance of IEbuCoreaudioProgrammeType */
typedef Dc1Ptr< IEbuCoreaudioProgrammeType > EbuCoreaudioProgrammePtr;

#define TypeOfaudioProgrammeType_audioContentIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioProgrammeType_audioContentIDRef_CollectionType")))
#define CreateaudioProgrammeType_audioContentIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioProgrammeType_audioContentIDRef_CollectionType))
class EbuCoreaudioProgrammeType_audioContentIDRef_CollectionType;
class IEbuCoreaudioProgrammeType_audioContentIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioProgrammeType_audioContentIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioProgrammeType_audioContentIDRef_CollectionType > EbuCoreaudioProgrammeType_audioContentIDRef_CollectionPtr;

#define TypeOfaudioStreamFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType")))
#define CreateaudioStreamFormatType (Dc1Factory::CreateObject(TypeOfaudioStreamFormatType))
class EbuCoreaudioStreamFormatType;
class IEbuCoreaudioStreamFormatType;
/** Smart pointer for instance of IEbuCoreaudioStreamFormatType */
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType > EbuCoreaudioStreamFormatPtr;

#define TypeOfaudioStreamFormatType_audioChannelFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioChannelFormatIDRef_CollectionType")))
#define CreateaudioStreamFormatType_audioChannelFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioStreamFormatType_audioChannelFormatIDRef_CollectionType))
class EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType;
class IEbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr;

#define TypeOfaudioStreamFormatType_audioPackFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioPackFormatIDRef_CollectionType")))
#define CreateaudioStreamFormatType_audioPackFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioStreamFormatType_audioPackFormatIDRef_CollectionType))
class EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType;
class IEbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr;

#define TypeOfaudioStreamFormatType_audioTrackFormatIDRef_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioStreamFormatType_audioTrackFormatIDRef_CollectionType")))
#define CreateaudioStreamFormatType_audioTrackFormatIDRef_CollectionType (Dc1Factory::CreateObject(TypeOfaudioStreamFormatType_audioTrackFormatIDRef_CollectionType))
class EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType;
class IEbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType;
/** Smart pointer for instance of IEbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType */
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr;

#define TypeOfaudioTrackFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormatType")))
#define CreateaudioTrackFormatType (Dc1Factory::CreateObject(TypeOfaudioTrackFormatType))
class EbuCoreaudioTrackFormatType;
class IEbuCoreaudioTrackFormatType;
/** Smart pointer for instance of IEbuCoreaudioTrackFormatType */
typedef Dc1Ptr< IEbuCoreaudioTrackFormatType > EbuCoreaudioTrackFormatPtr;

#define TypeOfaudioTrackUIDType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioTrackUIDType")))
#define CreateaudioTrackUIDType (Dc1Factory::CreateObject(TypeOfaudioTrackUIDType))
class EbuCoreaudioTrackUIDType;
class IEbuCoreaudioTrackUIDType;
/** Smart pointer for instance of IEbuCoreaudioTrackUIDType */
typedef Dc1Ptr< IEbuCoreaudioTrackUIDType > EbuCoreaudioTrackUIDPtr;

#define TypeOfBoolean (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Boolean")))
#define CreateBoolean (Dc1Factory::CreateObject(TypeOfBoolean))
class EbuCoreBoolean;
class IEbuCoreBoolean;
/** Smart pointer for instance of IEbuCoreBoolean */
typedef Dc1Ptr< IEbuCoreBoolean > EbuCoreBooleanPtr;

#define TypeOfcaptioningFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:captioningFormatType")))
#define CreatecaptioningFormatType (Dc1Factory::CreateObject(TypeOfcaptioningFormatType))
class EbuCorecaptioningFormatType;
class IEbuCorecaptioningFormatType;
/** Smart pointer for instance of IEbuCorecaptioningFormatType */
typedef Dc1Ptr< IEbuCorecaptioningFormatType > EbuCorecaptioningFormatPtr;

#define TypeOfcodecType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:codecType")))
#define CreatecodecType (Dc1Factory::CreateObject(TypeOfcodecType))
class EbuCorecodecType;
class IEbuCorecodecType;
/** Smart pointer for instance of IEbuCorecodecType */
typedef Dc1Ptr< IEbuCorecodecType > EbuCorecodecPtr;

#define TypeOfcoefficientType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coefficientType")))
#define CreatecoefficientType (Dc1Factory::CreateObject(TypeOfcoefficientType))
class EbuCorecoefficientType;
class IEbuCorecoefficientType;
/** Smart pointer for instance of IEbuCorecoefficientType */
typedef Dc1Ptr< IEbuCorecoefficientType > EbuCorecoefficientPtr;

#define TypeOfcomment_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:comment_LocalType")))
#define Createcomment_LocalType (Dc1Factory::CreateObject(TypeOfcomment_LocalType))
class EbuCorecomment_LocalType;
class IEbuCorecomment_LocalType;
/** Smart pointer for instance of IEbuCorecomment_LocalType */
typedef Dc1Ptr< IEbuCorecomment_LocalType > EbuCorecomment_LocalPtr;

#define TypeOfcompoundNameType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:compoundNameType")))
#define CreatecompoundNameType (Dc1Factory::CreateObject(TypeOfcompoundNameType))
class EbuCorecompoundNameType;
class IEbuCorecompoundNameType;
/** Smart pointer for instance of IEbuCorecompoundNameType */
typedef Dc1Ptr< IEbuCorecompoundNameType > EbuCorecompoundNamePtr;

#define TypeOfcontactDetailsType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType")))
#define CreatecontactDetailsType (Dc1Factory::CreateObject(TypeOfcontactDetailsType))
class EbuCorecontactDetailsType;
class IEbuCorecontactDetailsType;
/** Smart pointer for instance of IEbuCorecontactDetailsType */
typedef Dc1Ptr< IEbuCorecontactDetailsType > EbuCorecontactDetailsPtr;

#define TypeOfcontactDetailsType_details_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_details_CollectionType")))
#define CreatecontactDetailsType_details_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_details_CollectionType))
class EbuCorecontactDetailsType_details_CollectionType;
class IEbuCorecontactDetailsType_details_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_details_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_details_CollectionType > EbuCorecontactDetailsType_details_CollectionPtr;

#define TypeOfcontactDetailsType_name_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_name_CollectionType")))
#define CreatecontactDetailsType_name_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_name_CollectionType))
class EbuCorecontactDetailsType_name_CollectionType;
class IEbuCorecontactDetailsType_name_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_name_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_name_CollectionType > EbuCorecontactDetailsType_name_CollectionPtr;

#define TypeOfcontactDetailsType_nickname_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_nickname_CollectionType")))
#define CreatecontactDetailsType_nickname_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_nickname_CollectionType))
class EbuCorecontactDetailsType_nickname_CollectionType;
class IEbuCorecontactDetailsType_nickname_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_nickname_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_nickname_CollectionType > EbuCorecontactDetailsType_nickname_CollectionPtr;

#define TypeOfcontactDetailsType_otherGivenName_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_otherGivenName_CollectionType")))
#define CreatecontactDetailsType_otherGivenName_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_otherGivenName_CollectionType))
class EbuCorecontactDetailsType_otherGivenName_CollectionType;
class IEbuCorecontactDetailsType_otherGivenName_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_otherGivenName_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_otherGivenName_CollectionType > EbuCorecontactDetailsType_otherGivenName_CollectionPtr;

#define TypeOfcontactDetailsType_relatedContacts_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedContacts_CollectionType")))
#define CreatecontactDetailsType_relatedContacts_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_relatedContacts_CollectionType))
class EbuCorecontactDetailsType_relatedContacts_CollectionType;
class IEbuCorecontactDetailsType_relatedContacts_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_relatedContacts_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_relatedContacts_CollectionType > EbuCorecontactDetailsType_relatedContacts_CollectionPtr;

#define TypeOfcontactDetailsType_relatedInformationLink_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedInformationLink_CollectionType")))
#define CreatecontactDetailsType_relatedInformationLink_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_relatedInformationLink_CollectionType))
class EbuCorecontactDetailsType_relatedInformationLink_CollectionType;
class IEbuCorecontactDetailsType_relatedInformationLink_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_relatedInformationLink_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_relatedInformationLink_CollectionType > EbuCorecontactDetailsType_relatedInformationLink_CollectionPtr;

#define TypeOfcontactDetailsType_relatedInformationLink_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_relatedInformationLink_LocalType")))
#define CreatecontactDetailsType_relatedInformationLink_LocalType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_relatedInformationLink_LocalType))
class EbuCorecontactDetailsType_relatedInformationLink_LocalType;
class IEbuCorecontactDetailsType_relatedInformationLink_LocalType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_relatedInformationLink_LocalType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_relatedInformationLink_LocalType > EbuCorecontactDetailsType_relatedInformationLink_LocalPtr;

#define TypeOfcontactDetailsType_skill_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_skill_CollectionType")))
#define CreatecontactDetailsType_skill_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_skill_CollectionType))
class EbuCorecontactDetailsType_skill_CollectionType;
class IEbuCorecontactDetailsType_skill_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_skill_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_skill_CollectionType > EbuCorecontactDetailsType_skill_CollectionPtr;

#define TypeOfcontactDetailsType_stageName_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_stageName_CollectionType")))
#define CreatecontactDetailsType_stageName_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_stageName_CollectionType))
class EbuCorecontactDetailsType_stageName_CollectionType;
class IEbuCorecontactDetailsType_stageName_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_stageName_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_stageName_CollectionType > EbuCorecontactDetailsType_stageName_CollectionPtr;

#define TypeOfcontactDetailsType_username_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:contactDetailsType_username_CollectionType")))
#define CreatecontactDetailsType_username_CollectionType (Dc1Factory::CreateObject(TypeOfcontactDetailsType_username_CollectionType))
class EbuCorecontactDetailsType_username_CollectionType;
class IEbuCorecontactDetailsType_username_CollectionType;
/** Smart pointer for instance of IEbuCorecontactDetailsType_username_CollectionType */
typedef Dc1Ptr< IEbuCorecontactDetailsType_username_CollectionType > EbuCorecontactDetailsType_username_CollectionPtr;

#define TypeOfcontainerFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:containerFormatType")))
#define CreatecontainerFormatType (Dc1Factory::CreateObject(TypeOfcontainerFormatType))
class EbuCorecontainerFormatType;
class IEbuCorecontainerFormatType;
/** Smart pointer for instance of IEbuCorecontainerFormatType */
typedef Dc1Ptr< IEbuCorecontainerFormatType > EbuCorecontainerFormatPtr;

#define TypeOfcontainerFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:containerFormatType_comment_CollectionType")))
#define CreatecontainerFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfcontainerFormatType_comment_CollectionType))
class EbuCorecontainerFormatType_comment_CollectionType;
class IEbuCorecontainerFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCorecontainerFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCorecontainerFormatType_comment_CollectionType > EbuCorecontainerFormatType_comment_CollectionPtr;

#define TypeOfcoreMetadataType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType")))
#define CreatecoreMetadataType (Dc1Factory::CreateObject(TypeOfcoreMetadataType))
class EbuCorecoreMetadataType;
class IEbuCorecoreMetadataType;
/** Smart pointer for instance of IEbuCorecoreMetadataType */
typedef Dc1Ptr< IEbuCorecoreMetadataType > EbuCorecoreMetadataPtr;

#define TypeOfcoreMetadataType_alternativeTitle_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_alternativeTitle_CollectionType")))
#define CreatecoreMetadataType_alternativeTitle_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_alternativeTitle_CollectionType))
class EbuCorecoreMetadataType_alternativeTitle_CollectionType;
class IEbuCorecoreMetadataType_alternativeTitle_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_alternativeTitle_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_alternativeTitle_CollectionType > EbuCorecoreMetadataType_alternativeTitle_CollectionPtr;

#define TypeOfcoreMetadataType_contributor_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_contributor_CollectionType")))
#define CreatecoreMetadataType_contributor_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_contributor_CollectionType))
class EbuCorecoreMetadataType_contributor_CollectionType;
class IEbuCorecoreMetadataType_contributor_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_contributor_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_contributor_CollectionType > EbuCorecoreMetadataType_contributor_CollectionPtr;

#define TypeOfcoreMetadataType_coverage_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_coverage_CollectionType")))
#define CreatecoreMetadataType_coverage_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_coverage_CollectionType))
class EbuCorecoreMetadataType_coverage_CollectionType;
class IEbuCorecoreMetadataType_coverage_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_coverage_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_coverage_CollectionType > EbuCorecoreMetadataType_coverage_CollectionPtr;

#define TypeOfcoreMetadataType_creator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_creator_CollectionType")))
#define CreatecoreMetadataType_creator_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_creator_CollectionType))
class EbuCorecoreMetadataType_creator_CollectionType;
class IEbuCorecoreMetadataType_creator_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_creator_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_creator_CollectionType > EbuCorecoreMetadataType_creator_CollectionPtr;

#define TypeOfcoreMetadataType_date_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_date_CollectionType")))
#define CreatecoreMetadataType_date_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_date_CollectionType))
class EbuCorecoreMetadataType_date_CollectionType;
class IEbuCorecoreMetadataType_date_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_date_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_date_CollectionType > EbuCorecoreMetadataType_date_CollectionPtr;

#define TypeOfcoreMetadataType_description_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_description_CollectionType")))
#define CreatecoreMetadataType_description_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_description_CollectionType))
class EbuCorecoreMetadataType_description_CollectionType;
class IEbuCorecoreMetadataType_description_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_description_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_description_CollectionType > EbuCorecoreMetadataType_description_CollectionPtr;

#define TypeOfcoreMetadataType_format_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_format_CollectionType")))
#define CreatecoreMetadataType_format_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_format_CollectionType))
class EbuCorecoreMetadataType_format_CollectionType;
class IEbuCorecoreMetadataType_format_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_format_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_format_CollectionType > EbuCorecoreMetadataType_format_CollectionPtr;

#define TypeOfcoreMetadataType_hasEpisode_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasEpisode_CollectionType")))
#define CreatecoreMetadataType_hasEpisode_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasEpisode_CollectionType))
class EbuCorecoreMetadataType_hasEpisode_CollectionType;
class IEbuCorecoreMetadataType_hasEpisode_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasEpisode_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasEpisode_CollectionType > EbuCorecoreMetadataType_hasEpisode_CollectionPtr;

#define TypeOfcoreMetadataType_hasFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasFormat_CollectionType")))
#define CreatecoreMetadataType_hasFormat_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasFormat_CollectionType))
class EbuCorecoreMetadataType_hasFormat_CollectionType;
class IEbuCorecoreMetadataType_hasFormat_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasFormat_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasFormat_CollectionType > EbuCorecoreMetadataType_hasFormat_CollectionPtr;

#define TypeOfcoreMetadataType_hasMember_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasMember_CollectionType")))
#define CreatecoreMetadataType_hasMember_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasMember_CollectionType))
class EbuCorecoreMetadataType_hasMember_CollectionType;
class IEbuCorecoreMetadataType_hasMember_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasMember_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasMember_CollectionType > EbuCorecoreMetadataType_hasMember_CollectionPtr;

#define TypeOfcoreMetadataType_hasPart_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasPart_CollectionType")))
#define CreatecoreMetadataType_hasPart_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasPart_CollectionType))
class EbuCorecoreMetadataType_hasPart_CollectionType;
class IEbuCorecoreMetadataType_hasPart_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasPart_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasPart_CollectionType > EbuCorecoreMetadataType_hasPart_CollectionPtr;

#define TypeOfcoreMetadataType_hasSeason_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasSeason_CollectionType")))
#define CreatecoreMetadataType_hasSeason_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasSeason_CollectionType))
class EbuCorecoreMetadataType_hasSeason_CollectionType;
class IEbuCorecoreMetadataType_hasSeason_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasSeason_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasSeason_CollectionType > EbuCorecoreMetadataType_hasSeason_CollectionPtr;

#define TypeOfcoreMetadataType_hasTrackPart_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasTrackPart_CollectionType")))
#define CreatecoreMetadataType_hasTrackPart_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasTrackPart_CollectionType))
class EbuCorecoreMetadataType_hasTrackPart_CollectionType;
class IEbuCorecoreMetadataType_hasTrackPart_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasTrackPart_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasTrackPart_CollectionType > EbuCorecoreMetadataType_hasTrackPart_CollectionPtr;

#define TypeOfcoreMetadataType_hasTrackPart_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasTrackPart_LocalType")))
#define CreatecoreMetadataType_hasTrackPart_LocalType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasTrackPart_LocalType))
class EbuCorecoreMetadataType_hasTrackPart_LocalType;
class IEbuCorecoreMetadataType_hasTrackPart_LocalType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasTrackPart_LocalType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasTrackPart_LocalType > EbuCorecoreMetadataType_hasTrackPart_LocalPtr;

#define TypeOfcoreMetadataType_hasVersion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_hasVersion_CollectionType")))
#define CreatecoreMetadataType_hasVersion_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_hasVersion_CollectionType))
class EbuCorecoreMetadataType_hasVersion_CollectionType;
class IEbuCorecoreMetadataType_hasVersion_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_hasVersion_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasVersion_CollectionType > EbuCorecoreMetadataType_hasVersion_CollectionPtr;

#define TypeOfcoreMetadataType_identifier_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_identifier_CollectionType")))
#define CreatecoreMetadataType_identifier_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_identifier_CollectionType))
class EbuCorecoreMetadataType_identifier_CollectionType;
class IEbuCorecoreMetadataType_identifier_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_identifier_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_identifier_CollectionType > EbuCorecoreMetadataType_identifier_CollectionPtr;

#define TypeOfcoreMetadataType_isEpisodeOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isEpisodeOf_CollectionType")))
#define CreatecoreMetadataType_isEpisodeOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isEpisodeOf_CollectionType))
class EbuCorecoreMetadataType_isEpisodeOf_CollectionType;
class IEbuCorecoreMetadataType_isEpisodeOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isEpisodeOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isEpisodeOf_CollectionType > EbuCorecoreMetadataType_isEpisodeOf_CollectionPtr;

#define TypeOfcoreMetadataType_isFormatOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isFormatOf_CollectionType")))
#define CreatecoreMetadataType_isFormatOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isFormatOf_CollectionType))
class EbuCorecoreMetadataType_isFormatOf_CollectionType;
class IEbuCorecoreMetadataType_isFormatOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isFormatOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isFormatOf_CollectionType > EbuCorecoreMetadataType_isFormatOf_CollectionPtr;

#define TypeOfcoreMetadataType_isMemberOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isMemberOf_CollectionType")))
#define CreatecoreMetadataType_isMemberOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isMemberOf_CollectionType))
class EbuCorecoreMetadataType_isMemberOf_CollectionType;
class IEbuCorecoreMetadataType_isMemberOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isMemberOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isMemberOf_CollectionType > EbuCorecoreMetadataType_isMemberOf_CollectionPtr;

#define TypeOfcoreMetadataType_isNextInSequence_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isNextInSequence_CollectionType")))
#define CreatecoreMetadataType_isNextInSequence_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isNextInSequence_CollectionType))
class EbuCorecoreMetadataType_isNextInSequence_CollectionType;
class IEbuCorecoreMetadataType_isNextInSequence_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isNextInSequence_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isNextInSequence_CollectionType > EbuCorecoreMetadataType_isNextInSequence_CollectionPtr;

#define TypeOfcoreMetadataType_isPartOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isPartOf_CollectionType")))
#define CreatecoreMetadataType_isPartOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isPartOf_CollectionType))
class EbuCorecoreMetadataType_isPartOf_CollectionType;
class IEbuCorecoreMetadataType_isPartOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isPartOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isPartOf_CollectionType > EbuCorecoreMetadataType_isPartOf_CollectionPtr;

#define TypeOfcoreMetadataType_isReferencedBy_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isReferencedBy_CollectionType")))
#define CreatecoreMetadataType_isReferencedBy_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isReferencedBy_CollectionType))
class EbuCorecoreMetadataType_isReferencedBy_CollectionType;
class IEbuCorecoreMetadataType_isReferencedBy_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isReferencedBy_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isReferencedBy_CollectionType > EbuCorecoreMetadataType_isReferencedBy_CollectionPtr;

#define TypeOfcoreMetadataType_isRelatedTo_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isRelatedTo_CollectionType")))
#define CreatecoreMetadataType_isRelatedTo_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isRelatedTo_CollectionType))
class EbuCorecoreMetadataType_isRelatedTo_CollectionType;
class IEbuCorecoreMetadataType_isRelatedTo_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isRelatedTo_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isRelatedTo_CollectionType > EbuCorecoreMetadataType_isRelatedTo_CollectionPtr;

#define TypeOfcoreMetadataType_isReplacedBy_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isReplacedBy_CollectionType")))
#define CreatecoreMetadataType_isReplacedBy_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isReplacedBy_CollectionType))
class EbuCorecoreMetadataType_isReplacedBy_CollectionType;
class IEbuCorecoreMetadataType_isReplacedBy_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isReplacedBy_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isReplacedBy_CollectionType > EbuCorecoreMetadataType_isReplacedBy_CollectionPtr;

#define TypeOfcoreMetadataType_isRequiredBy_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isRequiredBy_CollectionType")))
#define CreatecoreMetadataType_isRequiredBy_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isRequiredBy_CollectionType))
class EbuCorecoreMetadataType_isRequiredBy_CollectionType;
class IEbuCorecoreMetadataType_isRequiredBy_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isRequiredBy_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isRequiredBy_CollectionType > EbuCorecoreMetadataType_isRequiredBy_CollectionPtr;

#define TypeOfcoreMetadataType_isSeasonOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isSeasonOf_CollectionType")))
#define CreatecoreMetadataType_isSeasonOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isSeasonOf_CollectionType))
class EbuCorecoreMetadataType_isSeasonOf_CollectionType;
class IEbuCorecoreMetadataType_isSeasonOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isSeasonOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isSeasonOf_CollectionType > EbuCorecoreMetadataType_isSeasonOf_CollectionPtr;

#define TypeOfcoreMetadataType_isVersionOf_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_isVersionOf_CollectionType")))
#define CreatecoreMetadataType_isVersionOf_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_isVersionOf_CollectionType))
class EbuCorecoreMetadataType_isVersionOf_CollectionType;
class IEbuCorecoreMetadataType_isVersionOf_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_isVersionOf_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_isVersionOf_CollectionType > EbuCorecoreMetadataType_isVersionOf_CollectionPtr;

#define TypeOfcoreMetadataType_language_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_language_CollectionType")))
#define CreatecoreMetadataType_language_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_language_CollectionType))
class EbuCorecoreMetadataType_language_CollectionType;
class IEbuCorecoreMetadataType_language_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_language_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_language_CollectionType > EbuCorecoreMetadataType_language_CollectionPtr;

#define TypeOfcoreMetadataType_part_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_part_CollectionType")))
#define CreatecoreMetadataType_part_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_part_CollectionType))
class EbuCorecoreMetadataType_part_CollectionType;
class IEbuCorecoreMetadataType_part_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_part_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_part_CollectionType > EbuCorecoreMetadataType_part_CollectionPtr;

#define TypeOfcoreMetadataType_planning_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_planning_CollectionType")))
#define CreatecoreMetadataType_planning_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_planning_CollectionType))
class EbuCorecoreMetadataType_planning_CollectionType;
class IEbuCorecoreMetadataType_planning_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_planning_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_planning_CollectionType > EbuCorecoreMetadataType_planning_CollectionPtr;

#define TypeOfcoreMetadataType_publicationHistory_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_publicationHistory_CollectionType")))
#define CreatecoreMetadataType_publicationHistory_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_publicationHistory_CollectionType))
class EbuCorecoreMetadataType_publicationHistory_CollectionType;
class IEbuCorecoreMetadataType_publicationHistory_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_publicationHistory_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_publicationHistory_CollectionType > EbuCorecoreMetadataType_publicationHistory_CollectionPtr;

#define TypeOfcoreMetadataType_publisher_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_publisher_CollectionType")))
#define CreatecoreMetadataType_publisher_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_publisher_CollectionType))
class EbuCorecoreMetadataType_publisher_CollectionType;
class IEbuCorecoreMetadataType_publisher_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_publisher_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_publisher_CollectionType > EbuCorecoreMetadataType_publisher_CollectionPtr;

#define TypeOfcoreMetadataType_rating_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_rating_CollectionType")))
#define CreatecoreMetadataType_rating_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_rating_CollectionType))
class EbuCorecoreMetadataType_rating_CollectionType;
class IEbuCorecoreMetadataType_rating_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_rating_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_rating_CollectionType > EbuCorecoreMetadataType_rating_CollectionPtr;

#define TypeOfcoreMetadataType_references_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_references_CollectionType")))
#define CreatecoreMetadataType_references_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_references_CollectionType))
class EbuCorecoreMetadataType_references_CollectionType;
class IEbuCorecoreMetadataType_references_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_references_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_references_CollectionType > EbuCorecoreMetadataType_references_CollectionPtr;

#define TypeOfcoreMetadataType_relation_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_relation_CollectionType")))
#define CreatecoreMetadataType_relation_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_relation_CollectionType))
class EbuCorecoreMetadataType_relation_CollectionType;
class IEbuCorecoreMetadataType_relation_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_relation_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_relation_CollectionType > EbuCorecoreMetadataType_relation_CollectionPtr;

#define TypeOfcoreMetadataType_replaces_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_replaces_CollectionType")))
#define CreatecoreMetadataType_replaces_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_replaces_CollectionType))
class EbuCorecoreMetadataType_replaces_CollectionType;
class IEbuCorecoreMetadataType_replaces_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_replaces_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_replaces_CollectionType > EbuCorecoreMetadataType_replaces_CollectionPtr;

#define TypeOfcoreMetadataType_requires_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_requires_CollectionType")))
#define CreatecoreMetadataType_requires_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_requires_CollectionType))
class EbuCorecoreMetadataType_requires_CollectionType;
class IEbuCorecoreMetadataType_requires_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_requires_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_requires_CollectionType > EbuCorecoreMetadataType_requires_CollectionPtr;

#define TypeOfcoreMetadataType_rights_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_rights_CollectionType")))
#define CreatecoreMetadataType_rights_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_rights_CollectionType))
class EbuCorecoreMetadataType_rights_CollectionType;
class IEbuCorecoreMetadataType_rights_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_rights_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_rights_CollectionType > EbuCorecoreMetadataType_rights_CollectionPtr;

#define TypeOfcoreMetadataType_sameAs_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_sameAs_CollectionType")))
#define CreatecoreMetadataType_sameAs_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_sameAs_CollectionType))
class EbuCorecoreMetadataType_sameAs_CollectionType;
class IEbuCorecoreMetadataType_sameAs_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_sameAs_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_sameAs_CollectionType > EbuCorecoreMetadataType_sameAs_CollectionPtr;

#define TypeOfcoreMetadataType_source_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_source_CollectionType")))
#define CreatecoreMetadataType_source_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_source_CollectionType))
class EbuCorecoreMetadataType_source_CollectionType;
class IEbuCorecoreMetadataType_source_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_source_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_source_CollectionType > EbuCorecoreMetadataType_source_CollectionPtr;

#define TypeOfcoreMetadataType_subject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_subject_CollectionType")))
#define CreatecoreMetadataType_subject_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_subject_CollectionType))
class EbuCorecoreMetadataType_subject_CollectionType;
class IEbuCorecoreMetadataType_subject_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_subject_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_subject_CollectionType > EbuCorecoreMetadataType_subject_CollectionPtr;

#define TypeOfcoreMetadataType_title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_title_CollectionType")))
#define CreatecoreMetadataType_title_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_title_CollectionType))
class EbuCorecoreMetadataType_title_CollectionType;
class IEbuCorecoreMetadataType_title_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_title_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_title_CollectionType > EbuCorecoreMetadataType_title_CollectionPtr;

#define TypeOfcoreMetadataType_type_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_type_CollectionType")))
#define CreatecoreMetadataType_type_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_type_CollectionType))
class EbuCorecoreMetadataType_type_CollectionType;
class IEbuCorecoreMetadataType_type_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_type_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_type_CollectionType > EbuCorecoreMetadataType_type_CollectionPtr;

#define TypeOfcoreMetadataType_version_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coreMetadataType_version_CollectionType")))
#define CreatecoreMetadataType_version_CollectionType (Dc1Factory::CreateObject(TypeOfcoreMetadataType_version_CollectionType))
class EbuCorecoreMetadataType_version_CollectionType;
class IEbuCorecoreMetadataType_version_CollectionType;
/** Smart pointer for instance of IEbuCorecoreMetadataType_version_CollectionType */
typedef Dc1Ptr< IEbuCorecoreMetadataType_version_CollectionType > EbuCorecoreMetadataType_version_CollectionPtr;

#define TypeOfcoverageType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:coverageType")))
#define CreatecoverageType (Dc1Factory::CreateObject(TypeOfcoverageType))
class EbuCorecoverageType;
class IEbuCorecoverageType;
/** Smart pointer for instance of IEbuCorecoverageType */
typedef Dc1Ptr< IEbuCorecoverageType > EbuCorecoveragePtr;

#define TypeOfdataFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType")))
#define CreatedataFormatType (Dc1Factory::CreateObject(TypeOfdataFormatType))
class EbuCoredataFormatType;
class IEbuCoredataFormatType;
/** Smart pointer for instance of IEbuCoredataFormatType */
typedef Dc1Ptr< IEbuCoredataFormatType > EbuCoredataFormatPtr;

#define TypeOfdataFormatType_ancillaryDataFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_ancillaryDataFormat_CollectionType")))
#define CreatedataFormatType_ancillaryDataFormat_CollectionType (Dc1Factory::CreateObject(TypeOfdataFormatType_ancillaryDataFormat_CollectionType))
class EbuCoredataFormatType_ancillaryDataFormat_CollectionType;
class IEbuCoredataFormatType_ancillaryDataFormat_CollectionType;
/** Smart pointer for instance of IEbuCoredataFormatType_ancillaryDataFormat_CollectionType */
typedef Dc1Ptr< IEbuCoredataFormatType_ancillaryDataFormat_CollectionType > EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr;

#define TypeOfdataFormatType_captioningFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_captioningFormat_CollectionType")))
#define CreatedataFormatType_captioningFormat_CollectionType (Dc1Factory::CreateObject(TypeOfdataFormatType_captioningFormat_CollectionType))
class EbuCoredataFormatType_captioningFormat_CollectionType;
class IEbuCoredataFormatType_captioningFormat_CollectionType;
/** Smart pointer for instance of IEbuCoredataFormatType_captioningFormat_CollectionType */
typedef Dc1Ptr< IEbuCoredataFormatType_captioningFormat_CollectionType > EbuCoredataFormatType_captioningFormat_CollectionPtr;

#define TypeOfdataFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_comment_CollectionType")))
#define CreatedataFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfdataFormatType_comment_CollectionType))
class EbuCoredataFormatType_comment_CollectionType;
class IEbuCoredataFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCoredataFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCoredataFormatType_comment_CollectionType > EbuCoredataFormatType_comment_CollectionPtr;

#define TypeOfdataFormatType_subtitlingFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dataFormatType_subtitlingFormat_CollectionType")))
#define CreatedataFormatType_subtitlingFormat_CollectionType (Dc1Factory::CreateObject(TypeOfdataFormatType_subtitlingFormat_CollectionType))
class EbuCoredataFormatType_subtitlingFormat_CollectionType;
class IEbuCoredataFormatType_subtitlingFormat_CollectionType;
/** Smart pointer for instance of IEbuCoredataFormatType_subtitlingFormat_CollectionType */
typedef Dc1Ptr< IEbuCoredataFormatType_subtitlingFormat_CollectionType > EbuCoredataFormatType_subtitlingFormat_CollectionPtr;

#define TypeOfdateType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType")))
#define CreatedateType (Dc1Factory::CreateObject(TypeOfdateType))
class EbuCoredateType;
class IEbuCoredateType;
/** Smart pointer for instance of IEbuCoredateType */
typedef Dc1Ptr< IEbuCoredateType > EbuCoredatePtr;

#define TypeOfdateType_alternative_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_alternative_CollectionType")))
#define CreatedateType_alternative_CollectionType (Dc1Factory::CreateObject(TypeOfdateType_alternative_CollectionType))
class EbuCoredateType_alternative_CollectionType;
class IEbuCoredateType_alternative_CollectionType;
/** Smart pointer for instance of IEbuCoredateType_alternative_CollectionType */
typedef Dc1Ptr< IEbuCoredateType_alternative_CollectionType > EbuCoredateType_alternative_CollectionPtr;

#define TypeOfdateType_copyrighted_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_copyrighted_LocalType")))
#define CreatedateType_copyrighted_LocalType (Dc1Factory::CreateObject(TypeOfdateType_copyrighted_LocalType))
class EbuCoredateType_copyrighted_LocalType;
class IEbuCoredateType_copyrighted_LocalType;
/** Smart pointer for instance of IEbuCoredateType_copyrighted_LocalType */
typedef Dc1Ptr< IEbuCoredateType_copyrighted_LocalType > EbuCoredateType_copyrighted_LocalPtr;

#define TypeOfdateType_created_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_created_LocalType")))
#define CreatedateType_created_LocalType (Dc1Factory::CreateObject(TypeOfdateType_created_LocalType))
class EbuCoredateType_created_LocalType;
class IEbuCoredateType_created_LocalType;
/** Smart pointer for instance of IEbuCoredateType_created_LocalType */
typedef Dc1Ptr< IEbuCoredateType_created_LocalType > EbuCoredateType_created_LocalPtr;

#define TypeOfdateType_date_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_date_CollectionType")))
#define CreatedateType_date_CollectionType (Dc1Factory::CreateObject(TypeOfdateType_date_CollectionType))
class EbuCoredateType_date_CollectionType;
class IEbuCoredateType_date_CollectionType;
/** Smart pointer for instance of IEbuCoredateType_date_CollectionType */
typedef Dc1Ptr< IEbuCoredateType_date_CollectionType > EbuCoredateType_date_CollectionPtr;

#define TypeOfdateType_digitised_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_digitised_LocalType")))
#define CreatedateType_digitised_LocalType (Dc1Factory::CreateObject(TypeOfdateType_digitised_LocalType))
class EbuCoredateType_digitised_LocalType;
class IEbuCoredateType_digitised_LocalType;
/** Smart pointer for instance of IEbuCoredateType_digitised_LocalType */
typedef Dc1Ptr< IEbuCoredateType_digitised_LocalType > EbuCoredateType_digitised_LocalPtr;

#define TypeOfdateType_issued_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_issued_LocalType")))
#define CreatedateType_issued_LocalType (Dc1Factory::CreateObject(TypeOfdateType_issued_LocalType))
class EbuCoredateType_issued_LocalType;
class IEbuCoredateType_issued_LocalType;
/** Smart pointer for instance of IEbuCoredateType_issued_LocalType */
typedef Dc1Ptr< IEbuCoredateType_issued_LocalType > EbuCoredateType_issued_LocalPtr;

#define TypeOfdateType_modified_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_modified_LocalType")))
#define CreatedateType_modified_LocalType (Dc1Factory::CreateObject(TypeOfdateType_modified_LocalType))
class EbuCoredateType_modified_LocalType;
class IEbuCoredateType_modified_LocalType;
/** Smart pointer for instance of IEbuCoredateType_modified_LocalType */
typedef Dc1Ptr< IEbuCoredateType_modified_LocalType > EbuCoredateType_modified_LocalPtr;

#define TypeOfdateType_released_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dateType_released_LocalType")))
#define CreatedateType_released_LocalType (Dc1Factory::CreateObject(TypeOfdateType_released_LocalType))
class EbuCoredateType_released_LocalType;
class IEbuCoredateType_released_LocalType;
/** Smart pointer for instance of IEbuCoredateType_released_LocalType */
typedef Dc1Ptr< IEbuCoredateType_released_LocalType > EbuCoredateType_released_LocalPtr;

#define TypeOfdescriptionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:descriptionType")))
#define CreatedescriptionType (Dc1Factory::CreateObject(TypeOfdescriptionType))
class EbuCoredescriptionType;
class IEbuCoredescriptionType;
/** Smart pointer for instance of IEbuCoredescriptionType */
typedef Dc1Ptr< IEbuCoredescriptionType > EbuCoredescriptionPtr;

#define TypeOfdescriptionType_description_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:descriptionType_description_CollectionType")))
#define CreatedescriptionType_description_CollectionType (Dc1Factory::CreateObject(TypeOfdescriptionType_description_CollectionType))
class EbuCoredescriptionType_description_CollectionType;
class IEbuCoredescriptionType_description_CollectionType;
/** Smart pointer for instance of IEbuCoredescriptionType_description_CollectionType */
typedef Dc1Ptr< IEbuCoredescriptionType_description_CollectionType > EbuCoredescriptionType_description_CollectionPtr;

#define TypeOfdetailsType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:detailsType")))
#define CreatedetailsType (Dc1Factory::CreateObject(TypeOfdetailsType))
class EbuCoredetailsType;
class IEbuCoredetailsType;
/** Smart pointer for instance of IEbuCoredetailsType */
typedef Dc1Ptr< IEbuCoredetailsType > EbuCoredetailsPtr;

#define TypeOfdetailsType_emailAddress_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:detailsType_emailAddress_CollectionType")))
#define CreatedetailsType_emailAddress_CollectionType (Dc1Factory::CreateObject(TypeOfdetailsType_emailAddress_CollectionType))
class EbuCoredetailsType_emailAddress_CollectionType;
class IEbuCoredetailsType_emailAddress_CollectionType;
/** Smart pointer for instance of IEbuCoredetailsType_emailAddress_CollectionType */
typedef Dc1Ptr< IEbuCoredetailsType_emailAddress_CollectionType > EbuCoredetailsType_emailAddress_CollectionPtr;

#define TypeOfdimensionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:dimensionType")))
#define CreatedimensionType (Dc1Factory::CreateObject(TypeOfdimensionType))
class EbuCoredimensionType;
class IEbuCoredimensionType;
/** Smart pointer for instance of IEbuCoredimensionType */
typedef Dc1Ptr< IEbuCoredimensionType > EbuCoredimensionPtr;

#define TypeOfdocumentFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType")))
#define CreatedocumentFormatType (Dc1Factory::CreateObject(TypeOfdocumentFormatType))
class EbuCoredocumentFormatType;
class IEbuCoredocumentFormatType;
/** Smart pointer for instance of IEbuCoredocumentFormatType */
typedef Dc1Ptr< IEbuCoredocumentFormatType > EbuCoredocumentFormatPtr;

#define TypeOfdocumentFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:documentFormatType_comment_CollectionType")))
#define CreatedocumentFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfdocumentFormatType_comment_CollectionType))
class EbuCoredocumentFormatType_comment_CollectionType;
class IEbuCoredocumentFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCoredocumentFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCoredocumentFormatType_comment_CollectionType > EbuCoredocumentFormatType_comment_CollectionPtr;

#define TypeOfdurationType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType")))
#define CreatedurationType (Dc1Factory::CreateObject(TypeOfdurationType))
class EbuCoredurationType;
class IEbuCoredurationType;
/** Smart pointer for instance of IEbuCoredurationType */
typedef Dc1Ptr< IEbuCoredurationType > EbuCoredurationPtr;

#define TypeOfdurationType_duration_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:durationType_duration_LocalType")))
#define CreatedurationType_duration_LocalType (Dc1Factory::CreateObject(TypeOfdurationType_duration_LocalType))
class EbuCoredurationType_duration_LocalType;
class IEbuCoredurationType_duration_LocalType;
/** Smart pointer for instance of IEbuCoredurationType_duration_LocalType */
typedef Dc1Ptr< IEbuCoredurationType_duration_LocalType > EbuCoredurationType_duration_LocalPtr;

#define TypeOfebuCoreMainType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ebuCoreMainType")))
#define CreateebuCoreMainType (Dc1Factory::CreateObject(TypeOfebuCoreMainType))
class EbuCoreebuCoreMainType;
class IEbuCoreebuCoreMainType;
/** Smart pointer for instance of IEbuCoreebuCoreMainType */
typedef Dc1Ptr< IEbuCoreebuCoreMainType > EbuCoreebuCoreMainPtr;

#define TypeOfeditUnitNumberType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:editUnitNumberType")))
#define CreateeditUnitNumberType (Dc1Factory::CreateObject(TypeOfeditUnitNumberType))
class EbuCoreeditUnitNumberType;
class IEbuCoreeditUnitNumberType;
/** Smart pointer for instance of IEbuCoreeditUnitNumberType */
typedef Dc1Ptr< IEbuCoreeditUnitNumberType > EbuCoreeditUnitNumberPtr;

// This is special, since it derives from dublin core complextype with the same name
#define TypeOfEbuCoreelementType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:elementType")))
#define CreateEbuCoreelementType (Dc1Factory::CreateObject(TypeOfEbuCoreelementType))
class EbuCoreelementType;
class IEbuCoreelementType;
/** Smart pointer to instance of IEbuCoreelementType */
typedef Dc1Ptr< IEbuCoreelementType > EbuCoreelementPtr;

#define TypeOfentityType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))
#define CreateentityType (Dc1Factory::CreateObject(TypeOfentityType))
class EbuCoreentityType;
class IEbuCoreentityType;
/** Smart pointer for instance of IEbuCoreentityType */
typedef Dc1Ptr< IEbuCoreentityType > EbuCoreentityPtr;

#define TypeOfentityType_contactDetails_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType_contactDetails_CollectionType")))
#define CreateentityType_contactDetails_CollectionType (Dc1Factory::CreateObject(TypeOfentityType_contactDetails_CollectionType))
class EbuCoreentityType_contactDetails_CollectionType;
class IEbuCoreentityType_contactDetails_CollectionType;
/** Smart pointer for instance of IEbuCoreentityType_contactDetails_CollectionType */
typedef Dc1Ptr< IEbuCoreentityType_contactDetails_CollectionType > EbuCoreentityType_contactDetails_CollectionPtr;

#define TypeOfentityType_organisationDetails_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType_organisationDetails_CollectionType")))
#define CreateentityType_organisationDetails_CollectionType (Dc1Factory::CreateObject(TypeOfentityType_organisationDetails_CollectionType))
class EbuCoreentityType_organisationDetails_CollectionType;
class IEbuCoreentityType_organisationDetails_CollectionType;
/** Smart pointer for instance of IEbuCoreentityType_organisationDetails_CollectionType */
typedef Dc1Ptr< IEbuCoreentityType_organisationDetails_CollectionType > EbuCoreentityType_organisationDetails_CollectionPtr;

#define TypeOfentityType_role_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType_role_CollectionType")))
#define CreateentityType_role_CollectionType (Dc1Factory::CreateObject(TypeOfentityType_role_CollectionType))
class EbuCoreentityType_role_CollectionType;
class IEbuCoreentityType_role_CollectionType;
/** Smart pointer for instance of IEbuCoreentityType_role_CollectionType */
typedef Dc1Ptr< IEbuCoreentityType_role_CollectionType > EbuCoreentityType_role_CollectionPtr;

#define TypeOfentityType_role_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType_role_LocalType")))
#define CreateentityType_role_LocalType (Dc1Factory::CreateObject(TypeOfentityType_role_LocalType))
class EbuCoreentityType_role_LocalType;
class IEbuCoreentityType_role_LocalType;
/** Smart pointer for instance of IEbuCoreentityType_role_LocalType */
typedef Dc1Ptr< IEbuCoreentityType_role_LocalType > EbuCoreentityType_role_LocalPtr;

#define TypeOffileInfo (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo")))
#define CreatefileInfo (Dc1Factory::CreateObject(TypeOffileInfo))
class EbuCorefileInfo;
class IEbuCorefileInfo;
/** Smart pointer for instance of IEbuCorefileInfo */
typedef Dc1Ptr< IEbuCorefileInfo > EbuCorefileInfoPtr;

#define TypeOffileInfo_locator_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_locator_CollectionType")))
#define CreatefileInfo_locator_CollectionType (Dc1Factory::CreateObject(TypeOffileInfo_locator_CollectionType))
class EbuCorefileInfo_locator_CollectionType;
class IEbuCorefileInfo_locator_CollectionType;
/** Smart pointer for instance of IEbuCorefileInfo_locator_CollectionType */
typedef Dc1Ptr< IEbuCorefileInfo_locator_CollectionType > EbuCorefileInfo_locator_CollectionPtr;

#define TypeOffileInfo_locator_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_locator_LocalType")))
#define CreatefileInfo_locator_LocalType (Dc1Factory::CreateObject(TypeOffileInfo_locator_LocalType))
class EbuCorefileInfo_locator_LocalType;
class IEbuCorefileInfo_locator_LocalType;
/** Smart pointer for instance of IEbuCorefileInfo_locator_LocalType */
typedef Dc1Ptr< IEbuCorefileInfo_locator_LocalType > EbuCorefileInfo_locator_LocalPtr;

#define TypeOffileInfo_mimeType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_mimeType_CollectionType")))
#define CreatefileInfo_mimeType_CollectionType (Dc1Factory::CreateObject(TypeOffileInfo_mimeType_CollectionType))
class EbuCorefileInfo_mimeType_CollectionType;
class IEbuCorefileInfo_mimeType_CollectionType;
/** Smart pointer for instance of IEbuCorefileInfo_mimeType_CollectionType */
typedef Dc1Ptr< IEbuCorefileInfo_mimeType_CollectionType > EbuCorefileInfo_mimeType_CollectionPtr;

#define TypeOffileInfo_mimeType_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:fileInfo_mimeType_LocalType")))
#define CreatefileInfo_mimeType_LocalType (Dc1Factory::CreateObject(TypeOffileInfo_mimeType_LocalType))
class EbuCorefileInfo_mimeType_LocalType;
class IEbuCorefileInfo_mimeType_LocalType;
/** Smart pointer for instance of IEbuCorefileInfo_mimeType_LocalType */
typedef Dc1Ptr< IEbuCorefileInfo_mimeType_LocalType > EbuCorefileInfo_mimeType_LocalPtr;

#define TypeOfFloat (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Float")))
#define CreateFloat (Dc1Factory::CreateObject(TypeOfFloat))
class EbuCoreFloat;
class IEbuCoreFloat;
/** Smart pointer for instance of IEbuCoreFloat */
typedef Dc1Ptr< IEbuCoreFloat > EbuCoreFloatPtr;

#define TypeOfformatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType")))
#define CreateformatType (Dc1Factory::CreateObject(TypeOfformatType))
class EbuCoreformatType;
class IEbuCoreformatType;
/** Smart pointer for instance of IEbuCoreformatType */
typedef Dc1Ptr< IEbuCoreformatType > EbuCoreformatPtr;

#define TypeOfformatType_audioFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_audioFormat_CollectionType")))
#define CreateformatType_audioFormat_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_audioFormat_CollectionType))
class EbuCoreformatType_audioFormat_CollectionType;
class IEbuCoreformatType_audioFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_audioFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_audioFormat_CollectionType > EbuCoreformatType_audioFormat_CollectionPtr;

#define TypeOfformatType_audioFormatExtended_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_audioFormatExtended_CollectionType")))
#define CreateformatType_audioFormatExtended_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_audioFormatExtended_CollectionType))
class EbuCoreformatType_audioFormatExtended_CollectionType;
class IEbuCoreformatType_audioFormatExtended_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_audioFormatExtended_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_audioFormatExtended_CollectionType > EbuCoreformatType_audioFormatExtended_CollectionPtr;

#define TypeOfformatType_containerFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_containerFormat_CollectionType")))
#define CreateformatType_containerFormat_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_containerFormat_CollectionType))
class EbuCoreformatType_containerFormat_CollectionType;
class IEbuCoreformatType_containerFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_containerFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_containerFormat_CollectionType > EbuCoreformatType_containerFormat_CollectionPtr;

#define TypeOfformatType_dataFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dataFormat_CollectionType")))
#define CreateformatType_dataFormat_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_dataFormat_CollectionType))
class EbuCoreformatType_dataFormat_CollectionType;
class IEbuCoreformatType_dataFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_dataFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_dataFormat_CollectionType > EbuCoreformatType_dataFormat_CollectionPtr;

#define TypeOfformatType_dateCreated_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateCreated_LocalType")))
#define CreateformatType_dateCreated_LocalType (Dc1Factory::CreateObject(TypeOfformatType_dateCreated_LocalType))
class EbuCoreformatType_dateCreated_LocalType;
class IEbuCoreformatType_dateCreated_LocalType;
/** Smart pointer for instance of IEbuCoreformatType_dateCreated_LocalType */
typedef Dc1Ptr< IEbuCoreformatType_dateCreated_LocalType > EbuCoreformatType_dateCreated_LocalPtr;

#define TypeOfformatType_dateModified_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_dateModified_LocalType")))
#define CreateformatType_dateModified_LocalType (Dc1Factory::CreateObject(TypeOfformatType_dateModified_LocalType))
class EbuCoreformatType_dateModified_LocalType;
class IEbuCoreformatType_dateModified_LocalType;
/** Smart pointer for instance of IEbuCoreformatType_dateModified_LocalType */
typedef Dc1Ptr< IEbuCoreformatType_dateModified_LocalType > EbuCoreformatType_dateModified_LocalPtr;

#define TypeOfformatType_imageFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_imageFormat_CollectionType")))
#define CreateformatType_imageFormat_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_imageFormat_CollectionType))
class EbuCoreformatType_imageFormat_CollectionType;
class IEbuCoreformatType_imageFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_imageFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_imageFormat_CollectionType > EbuCoreformatType_imageFormat_CollectionPtr;

#define TypeOfformatType_medium_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_medium_CollectionType")))
#define CreateformatType_medium_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_medium_CollectionType))
class EbuCoreformatType_medium_CollectionType;
class IEbuCoreformatType_medium_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_medium_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_medium_CollectionType > EbuCoreformatType_medium_CollectionPtr;

#define TypeOfformatType_medium_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_medium_LocalType")))
#define CreateformatType_medium_LocalType (Dc1Factory::CreateObject(TypeOfformatType_medium_LocalType))
class EbuCoreformatType_medium_LocalType;
class IEbuCoreformatType_medium_LocalType;
/** Smart pointer for instance of IEbuCoreformatType_medium_LocalType */
typedef Dc1Ptr< IEbuCoreformatType_medium_LocalType > EbuCoreformatType_medium_LocalPtr;

#define TypeOfformatType_videoFormat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:formatType_videoFormat_CollectionType")))
#define CreateformatType_videoFormat_CollectionType (Dc1Factory::CreateObject(TypeOfformatType_videoFormat_CollectionType))
class EbuCoreformatType_videoFormat_CollectionType;
class IEbuCoreformatType_videoFormat_CollectionType;
/** Smart pointer for instance of IEbuCoreformatType_videoFormat_CollectionType */
typedef Dc1Ptr< IEbuCoreformatType_videoFormat_CollectionType > EbuCoreformatType_videoFormat_CollectionPtr;

#define TypeOffrequency_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:frequency_LocalType")))
#define Createfrequency_LocalType (Dc1Factory::CreateObject(TypeOffrequency_LocalType))
class EbuCorefrequency_LocalType;
class IEbuCorefrequency_LocalType;
/** Smart pointer for instance of IEbuCorefrequency_LocalType */
typedef Dc1Ptr< IEbuCorefrequency_LocalType > EbuCorefrequency_LocalPtr;

#define TypeOfhashType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:hashType")))
#define CreatehashType (Dc1Factory::CreateObject(TypeOfhashType))
class EbuCorehashType;
class IEbuCorehashType;
/** Smart pointer for instance of IEbuCorehashType */
typedef Dc1Ptr< IEbuCorehashType > EbuCorehashPtr;

#define TypeOfhashType_hashFunction_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:hashType_hashFunction_LocalType")))
#define CreatehashType_hashFunction_LocalType (Dc1Factory::CreateObject(TypeOfhashType_hashFunction_LocalType))
class EbuCorehashType_hashFunction_LocalType;
class IEbuCorehashType_hashFunction_LocalType;
/** Smart pointer for instance of IEbuCorehashType_hashFunction_LocalType */
typedef Dc1Ptr< IEbuCorehashType_hashFunction_LocalType > EbuCorehashType_hashFunction_LocalPtr;

#define TypeOfidentifierType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))
#define CreateidentifierType (Dc1Factory::CreateObject(TypeOfidentifierType))
class EbuCoreidentifierType;
class IEbuCoreidentifierType;
/** Smart pointer for instance of IEbuCoreidentifierType */
typedef Dc1Ptr< IEbuCoreidentifierType > EbuCoreidentifierPtr;

#define TypeOfimageFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType")))
#define CreateimageFormatType (Dc1Factory::CreateObject(TypeOfimageFormatType))
class EbuCoreimageFormatType;
class IEbuCoreimageFormatType;
/** Smart pointer for instance of IEbuCoreimageFormatType */
typedef Dc1Ptr< IEbuCoreimageFormatType > EbuCoreimageFormatPtr;

#define TypeOfimageFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_comment_CollectionType")))
#define CreateimageFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfimageFormatType_comment_CollectionType))
class EbuCoreimageFormatType_comment_CollectionType;
class IEbuCoreimageFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCoreimageFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCoreimageFormatType_comment_CollectionType > EbuCoreimageFormatType_comment_CollectionPtr;

#define TypeOfimageFormatType_imageEncoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_imageEncoding_LocalType")))
#define CreateimageFormatType_imageEncoding_LocalType (Dc1Factory::CreateObject(TypeOfimageFormatType_imageEncoding_LocalType))
class EbuCoreimageFormatType_imageEncoding_LocalType;
class IEbuCoreimageFormatType_imageEncoding_LocalType;
/** Smart pointer for instance of IEbuCoreimageFormatType_imageEncoding_LocalType */
typedef Dc1Ptr< IEbuCoreimageFormatType_imageEncoding_LocalType > EbuCoreimageFormatType_imageEncoding_LocalPtr;

#define TypeOfimageFormatType_orientation_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:imageFormatType_orientation_LocalType")))
#define CreateimageFormatType_orientation_LocalType (Dc1Factory::CreateObject(TypeOfimageFormatType_orientation_LocalType))
class EbuCoreimageFormatType_orientation_LocalType;
class IEbuCoreimageFormatType_orientation_LocalType;
// No smart pointer instance for IEbuCoreimageFormatType_orientation_LocalType

#define TypeOfInt16 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int16")))
#define CreateInt16 (Dc1Factory::CreateObject(TypeOfInt16))
class EbuCoreInt16;
class IEbuCoreInt16;
/** Smart pointer for instance of IEbuCoreInt16 */
typedef Dc1Ptr< IEbuCoreInt16 > EbuCoreInt16Ptr;

#define TypeOfInt32 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int32")))
#define CreateInt32 (Dc1Factory::CreateObject(TypeOfInt32))
class EbuCoreInt32;
class IEbuCoreInt32;
/** Smart pointer for instance of IEbuCoreInt32 */
typedef Dc1Ptr< IEbuCoreInt32 > EbuCoreInt32Ptr;

#define TypeOfInt64 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int64")))
#define CreateInt64 (Dc1Factory::CreateObject(TypeOfInt64))
class EbuCoreInt64;
class IEbuCoreInt64;
/** Smart pointer for instance of IEbuCoreInt64 */
typedef Dc1Ptr< IEbuCoreInt64 > EbuCoreInt64Ptr;

#define TypeOfInt8 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:Int8")))
#define CreateInt8 (Dc1Factory::CreateObject(TypeOfInt8))
class EbuCoreInt8;
class IEbuCoreInt8;
/** Smart pointer for instance of IEbuCoreInt8 */
typedef Dc1Ptr< IEbuCoreInt8 > EbuCoreInt8Ptr;

#define TypeOflanguageType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:languageType")))
#define CreatelanguageType (Dc1Factory::CreateObject(TypeOflanguageType))
class EbuCorelanguageType;
class IEbuCorelanguageType;
/** Smart pointer for instance of IEbuCorelanguageType */
typedef Dc1Ptr< IEbuCorelanguageType > EbuCorelanguagePtr;

#define TypeOflengthType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:lengthType")))
#define CreatelengthType (Dc1Factory::CreateObject(TypeOflengthType))
class EbuCorelengthType;
class IEbuCorelengthType;
/** Smart pointer for instance of IEbuCorelengthType */
typedef Dc1Ptr< IEbuCorelengthType > EbuCorelengthPtr;

#define TypeOflocationType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:locationType")))
#define CreatelocationType (Dc1Factory::CreateObject(TypeOflocationType))
class EbuCorelocationType;
class IEbuCorelocationType;
/** Smart pointer for instance of IEbuCorelocationType */
typedef Dc1Ptr< IEbuCorelocationType > EbuCorelocationPtr;

#define TypeOflocationType_coordinates_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:locationType_coordinates_LocalType")))
#define CreatelocationType_coordinates_LocalType (Dc1Factory::CreateObject(TypeOflocationType_coordinates_LocalType))
class EbuCorelocationType_coordinates_LocalType;
class IEbuCorelocationType_coordinates_LocalType;
/** Smart pointer for instance of IEbuCorelocationType_coordinates_LocalType */
typedef Dc1Ptr< IEbuCorelocationType_coordinates_LocalType > EbuCorelocationType_coordinates_LocalPtr;

#define TypeOfloudnessMetadataType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:loudnessMetadataType")))
#define CreateloudnessMetadataType (Dc1Factory::CreateObject(TypeOfloudnessMetadataType))
class EbuCoreloudnessMetadataType;
class IEbuCoreloudnessMetadataType;
/** Smart pointer for instance of IEbuCoreloudnessMetadataType */
typedef Dc1Ptr< IEbuCoreloudnessMetadataType > EbuCoreloudnessMetadataPtr;

#define TypeOfmatrixType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:matrixType")))
#define CreatematrixType (Dc1Factory::CreateObject(TypeOfmatrixType))
class EbuCorematrixType;
class IEbuCorematrixType;
/** Smart pointer for instance of IEbuCorematrixType */
typedef Dc1Ptr< IEbuCorematrixType > EbuCorematrixPtr;

#define TypeOfmatrixType_coefficient_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:matrixType_coefficient_CollectionType")))
#define CreatematrixType_coefficient_CollectionType (Dc1Factory::CreateObject(TypeOfmatrixType_coefficient_CollectionType))
class EbuCorematrixType_coefficient_CollectionType;
class IEbuCorematrixType_coefficient_CollectionType;
/** Smart pointer for instance of IEbuCorematrixType_coefficient_CollectionType */
typedef Dc1Ptr< IEbuCorematrixType_coefficient_CollectionType > EbuCorematrixType_coefficient_CollectionPtr;

#define TypeOforganisationDepartmentType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDepartmentType")))
#define CreateorganisationDepartmentType (Dc1Factory::CreateObject(TypeOforganisationDepartmentType))
class EbuCoreorganisationDepartmentType;
class IEbuCoreorganisationDepartmentType;
/** Smart pointer for instance of IEbuCoreorganisationDepartmentType */
typedef Dc1Ptr< IEbuCoreorganisationDepartmentType > EbuCoreorganisationDepartmentPtr;

#define TypeOforganisationDetailsType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType")))
#define CreateorganisationDetailsType (Dc1Factory::CreateObject(TypeOforganisationDetailsType))
class EbuCoreorganisationDetailsType;
class IEbuCoreorganisationDetailsType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType > EbuCoreorganisationDetailsPtr;

#define TypeOforganisationDetailsType_contacts_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_contacts_CollectionType")))
#define CreateorganisationDetailsType_contacts_CollectionType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_contacts_CollectionType))
class EbuCoreorganisationDetailsType_contacts_CollectionType;
class IEbuCoreorganisationDetailsType_contacts_CollectionType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_contacts_CollectionType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_contacts_CollectionType > EbuCoreorganisationDetailsType_contacts_CollectionPtr;

#define TypeOforganisationDetailsType_details_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_details_CollectionType")))
#define CreateorganisationDetailsType_details_CollectionType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_details_CollectionType))
class EbuCoreorganisationDetailsType_details_CollectionType;
class IEbuCoreorganisationDetailsType_details_CollectionType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_details_CollectionType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_details_CollectionType > EbuCoreorganisationDetailsType_details_CollectionPtr;

#define TypeOforganisationDetailsType_organisationCode_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_organisationCode_CollectionType")))
#define CreateorganisationDetailsType_organisationCode_CollectionType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_organisationCode_CollectionType))
class EbuCoreorganisationDetailsType_organisationCode_CollectionType;
class IEbuCoreorganisationDetailsType_organisationCode_CollectionType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_organisationCode_CollectionType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_organisationCode_CollectionType > EbuCoreorganisationDetailsType_organisationCode_CollectionPtr;

#define TypeOforganisationDetailsType_organisationName_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_organisationName_CollectionType")))
#define CreateorganisationDetailsType_organisationName_CollectionType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_organisationName_CollectionType))
class EbuCoreorganisationDetailsType_organisationName_CollectionType;
class IEbuCoreorganisationDetailsType_organisationName_CollectionType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_organisationName_CollectionType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_organisationName_CollectionType > EbuCoreorganisationDetailsType_organisationName_CollectionPtr;

#define TypeOforganisationDetailsType_relatedInformationLink_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_relatedInformationLink_CollectionType")))
#define CreateorganisationDetailsType_relatedInformationLink_CollectionType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_relatedInformationLink_CollectionType))
class EbuCoreorganisationDetailsType_relatedInformationLink_CollectionType;
class IEbuCoreorganisationDetailsType_relatedInformationLink_CollectionType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_relatedInformationLink_CollectionType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_relatedInformationLink_CollectionType > EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr;

#define TypeOforganisationDetailsType_relatedInformationLink_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_relatedInformationLink_LocalType")))
#define CreateorganisationDetailsType_relatedInformationLink_LocalType (Dc1Factory::CreateObject(TypeOforganisationDetailsType_relatedInformationLink_LocalType))
class EbuCoreorganisationDetailsType_relatedInformationLink_LocalType;
class IEbuCoreorganisationDetailsType_relatedInformationLink_LocalType;
/** Smart pointer for instance of IEbuCoreorganisationDetailsType_relatedInformationLink_LocalType */
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_relatedInformationLink_LocalType > EbuCoreorganisationDetailsType_relatedInformationLink_LocalPtr;

#define TypeOfPackageIDType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:PackageIDType")))
#define CreatePackageIDType (Dc1Factory::CreateObject(TypeOfPackageIDType))
class EbuCorePackageIDType;
class IEbuCorePackageIDType;
/** Smart pointer for instance of IEbuCorePackageIDType */
typedef Dc1Ptr< IEbuCorePackageIDType > EbuCorePackageIDPtr;

#define TypeOfpartType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:partType")))
#define CreatepartType (Dc1Factory::CreateObject(TypeOfpartType))
class EbuCorepartType;
class IEbuCorepartType;
/** Smart pointer for instance of IEbuCorepartType */
typedef Dc1Ptr< IEbuCorepartType > EbuCorepartPtr;

#define TypeOfperiodOfTimeType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:periodOfTimeType")))
#define CreateperiodOfTimeType (Dc1Factory::CreateObject(TypeOfperiodOfTimeType))
class EbuCoreperiodOfTimeType;
class IEbuCoreperiodOfTimeType;
/** Smart pointer for instance of IEbuCoreperiodOfTimeType */
typedef Dc1Ptr< IEbuCoreperiodOfTimeType > EbuCoreperiodOfTimePtr;

#define TypeOfplanningType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:planningType")))
#define CreateplanningType (Dc1Factory::CreateObject(TypeOfplanningType))
class EbuCoreplanningType;
class IEbuCoreplanningType;
/** Smart pointer for instance of IEbuCoreplanningType */
typedef Dc1Ptr< IEbuCoreplanningType > EbuCoreplanningPtr;

#define TypeOfplanningType_publicationEvent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:planningType_publicationEvent_CollectionType")))
#define CreateplanningType_publicationEvent_CollectionType (Dc1Factory::CreateObject(TypeOfplanningType_publicationEvent_CollectionType))
class EbuCoreplanningType_publicationEvent_CollectionType;
class IEbuCoreplanningType_publicationEvent_CollectionType;
/** Smart pointer for instance of IEbuCoreplanningType_publicationEvent_CollectionType */
typedef Dc1Ptr< IEbuCoreplanningType_publicationEvent_CollectionType > EbuCoreplanningType_publicationEvent_CollectionPtr;

#define TypeOfposition_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:position_LocalType")))
#define Createposition_LocalType (Dc1Factory::CreateObject(TypeOfposition_LocalType))
class EbuCoreposition_LocalType;
class IEbuCoreposition_LocalType;
/** Smart pointer for instance of IEbuCoreposition_LocalType */
typedef Dc1Ptr< IEbuCoreposition_LocalType > EbuCoreposition_LocalPtr;

#define TypeOfpublicationChannelType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationChannelType")))
#define CreatepublicationChannelType (Dc1Factory::CreateObject(TypeOfpublicationChannelType))
class EbuCorepublicationChannelType;
class IEbuCorepublicationChannelType;
/** Smart pointer for instance of IEbuCorepublicationChannelType */
typedef Dc1Ptr< IEbuCorepublicationChannelType > EbuCorepublicationChannelPtr;

#define TypeOfpublicationEventType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationEventType")))
#define CreatepublicationEventType (Dc1Factory::CreateObject(TypeOfpublicationEventType))
class EbuCorepublicationEventType;
class IEbuCorepublicationEventType;
/** Smart pointer for instance of IEbuCorepublicationEventType */
typedef Dc1Ptr< IEbuCorepublicationEventType > EbuCorepublicationEventPtr;

#define TypeOfpublicationEventType_publicationRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationEventType_publicationRegion_CollectionType")))
#define CreatepublicationEventType_publicationRegion_CollectionType (Dc1Factory::CreateObject(TypeOfpublicationEventType_publicationRegion_CollectionType))
class EbuCorepublicationEventType_publicationRegion_CollectionType;
class IEbuCorepublicationEventType_publicationRegion_CollectionType;
/** Smart pointer for instance of IEbuCorepublicationEventType_publicationRegion_CollectionType */
typedef Dc1Ptr< IEbuCorepublicationEventType_publicationRegion_CollectionType > EbuCorepublicationEventType_publicationRegion_CollectionPtr;

#define TypeOfpublicationHistoryType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationHistoryType")))
#define CreatepublicationHistoryType (Dc1Factory::CreateObject(TypeOfpublicationHistoryType))
class EbuCorepublicationHistoryType;
class IEbuCorepublicationHistoryType;
/** Smart pointer for instance of IEbuCorepublicationHistoryType */
typedef Dc1Ptr< IEbuCorepublicationHistoryType > EbuCorepublicationHistoryPtr;

#define TypeOfpublicationHistoryType_publicationEvent_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationHistoryType_publicationEvent_CollectionType")))
#define CreatepublicationHistoryType_publicationEvent_CollectionType (Dc1Factory::CreateObject(TypeOfpublicationHistoryType_publicationEvent_CollectionType))
class EbuCorepublicationHistoryType_publicationEvent_CollectionType;
class IEbuCorepublicationHistoryType_publicationEvent_CollectionType;
/** Smart pointer for instance of IEbuCorepublicationHistoryType_publicationEvent_CollectionType */
typedef Dc1Ptr< IEbuCorepublicationHistoryType_publicationEvent_CollectionType > EbuCorepublicationHistoryType_publicationEvent_CollectionPtr;

#define TypeOfpublicationMediumType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationMediumType")))
#define CreatepublicationMediumType (Dc1Factory::CreateObject(TypeOfpublicationMediumType))
class EbuCorepublicationMediumType;
class IEbuCorepublicationMediumType;
/** Smart pointer for instance of IEbuCorepublicationMediumType */
typedef Dc1Ptr< IEbuCorepublicationMediumType > EbuCorepublicationMediumPtr;

#define TypeOfpublicationServiceType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:publicationServiceType")))
#define CreatepublicationServiceType (Dc1Factory::CreateObject(TypeOfpublicationServiceType))
class EbuCorepublicationServiceType;
class IEbuCorepublicationServiceType;
/** Smart pointer for instance of IEbuCorepublicationServiceType */
typedef Dc1Ptr< IEbuCorepublicationServiceType > EbuCorepublicationServicePtr;

#define TypeOfratingType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType")))
#define CreateratingType (Dc1Factory::CreateObject(TypeOfratingType))
class EbuCoreratingType;
class IEbuCoreratingType;
/** Smart pointer for instance of IEbuCoreratingType */
typedef Dc1Ptr< IEbuCoreratingType > EbuCoreratingPtr;

#define TypeOfratingType_ratingExclusionRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingExclusionRegion_CollectionType")))
#define CreateratingType_ratingExclusionRegion_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingExclusionRegion_CollectionType))
class EbuCoreratingType_ratingExclusionRegion_CollectionType;
class IEbuCoreratingType_ratingExclusionRegion_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingExclusionRegion_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingExclusionRegion_CollectionType > EbuCoreratingType_ratingExclusionRegion_CollectionPtr;

#define TypeOfratingType_ratingLink_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingLink_CollectionType")))
#define CreateratingType_ratingLink_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingLink_CollectionType))
class EbuCoreratingType_ratingLink_CollectionType;
class IEbuCoreratingType_ratingLink_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingLink_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingLink_CollectionType > EbuCoreratingType_ratingLink_CollectionPtr;

#define TypeOfratingType_ratingRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingRegion_CollectionType")))
#define CreateratingType_ratingRegion_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingRegion_CollectionType))
class EbuCoreratingType_ratingRegion_CollectionType;
class IEbuCoreratingType_ratingRegion_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingRegion_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingRegion_CollectionType > EbuCoreratingType_ratingRegion_CollectionPtr;

#define TypeOfratingType_ratingScaleMaxValue_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingScaleMaxValue_CollectionType")))
#define CreateratingType_ratingScaleMaxValue_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingScaleMaxValue_CollectionType))
class EbuCoreratingType_ratingScaleMaxValue_CollectionType;
class IEbuCoreratingType_ratingScaleMaxValue_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingScaleMaxValue_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingScaleMaxValue_CollectionType > EbuCoreratingType_ratingScaleMaxValue_CollectionPtr;

#define TypeOfratingType_ratingScaleMinValue_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingScaleMinValue_CollectionType")))
#define CreateratingType_ratingScaleMinValue_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingScaleMinValue_CollectionType))
class EbuCoreratingType_ratingScaleMinValue_CollectionType;
class IEbuCoreratingType_ratingScaleMinValue_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingScaleMinValue_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingScaleMinValue_CollectionType > EbuCoreratingType_ratingScaleMinValue_CollectionPtr;

#define TypeOfratingType_ratingValue_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:ratingType_ratingValue_CollectionType")))
#define CreateratingType_ratingValue_CollectionType (Dc1Factory::CreateObject(TypeOfratingType_ratingValue_CollectionType))
class EbuCoreratingType_ratingValue_CollectionType;
class IEbuCoreratingType_ratingValue_CollectionType;
/** Smart pointer for instance of IEbuCoreratingType_ratingValue_CollectionType */
typedef Dc1Ptr< IEbuCoreratingType_ratingValue_CollectionType > EbuCoreratingType_ratingValue_CollectionPtr;

#define TypeOfrationalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rationalType")))
#define CreaterationalType (Dc1Factory::CreateObject(TypeOfrationalType))
class EbuCorerationalType;
class IEbuCorerationalType;
/** Smart pointer for instance of IEbuCorerationalType */
typedef Dc1Ptr< IEbuCorerationalType > EbuCorerationalPtr;

#define TypeOfregionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))
#define CreateregionType (Dc1Factory::CreateObject(TypeOfregionType))
class EbuCoreregionType;
class IEbuCoreregionType;
/** Smart pointer for instance of IEbuCoreregionType */
typedef Dc1Ptr< IEbuCoreregionType > EbuCoreregionPtr;

#define TypeOfregionType_country_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType_country_LocalType")))
#define CreateregionType_country_LocalType (Dc1Factory::CreateObject(TypeOfregionType_country_LocalType))
class EbuCoreregionType_country_LocalType;
class IEbuCoreregionType_country_LocalType;
/** Smart pointer for instance of IEbuCoreregionType_country_LocalType */
typedef Dc1Ptr< IEbuCoreregionType_country_LocalType > EbuCoreregionType_country_LocalPtr;

#define TypeOfregionType_countryRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType_countryRegion_CollectionType")))
#define CreateregionType_countryRegion_CollectionType (Dc1Factory::CreateObject(TypeOfregionType_countryRegion_CollectionType))
class EbuCoreregionType_countryRegion_CollectionType;
class IEbuCoreregionType_countryRegion_CollectionType;
/** Smart pointer for instance of IEbuCoreregionType_countryRegion_CollectionType */
typedef Dc1Ptr< IEbuCoreregionType_countryRegion_CollectionType > EbuCoreregionType_countryRegion_CollectionPtr;

#define TypeOfregionType_countryRegion_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType_countryRegion_LocalType")))
#define CreateregionType_countryRegion_LocalType (Dc1Factory::CreateObject(TypeOfregionType_countryRegion_LocalType))
class EbuCoreregionType_countryRegion_LocalType;
class IEbuCoreregionType_countryRegion_LocalType;
/** Smart pointer for instance of IEbuCoreregionType_countryRegion_LocalType */
typedef Dc1Ptr< IEbuCoreregionType_countryRegion_LocalType > EbuCoreregionType_countryRegion_LocalPtr;

#define TypeOfrelationType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:relationType")))
#define CreaterelationType (Dc1Factory::CreateObject(TypeOfrelationType))
class EbuCorerelationType;
class IEbuCorerelationType;
/** Smart pointer for instance of IEbuCorerelationType */
typedef Dc1Ptr< IEbuCorerelationType > EbuCorerelationPtr;

#define TypeOfrightsType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType")))
#define CreaterightsType (Dc1Factory::CreateObject(TypeOfrightsType))
class EbuCorerightsType;
class IEbuCorerightsType;
/** Smart pointer for instance of IEbuCorerightsType */
typedef Dc1Ptr< IEbuCorerightsType > EbuCorerightsPtr;

#define TypeOfrightsType_contactDetails_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_contactDetails_CollectionType")))
#define CreaterightsType_contactDetails_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_contactDetails_CollectionType))
class EbuCorerightsType_contactDetails_CollectionType;
class IEbuCorerightsType_contactDetails_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_contactDetails_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_contactDetails_CollectionType > EbuCorerightsType_contactDetails_CollectionPtr;

#define TypeOfrightsType_copyrightStatement_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_copyrightStatement_CollectionType")))
#define CreaterightsType_copyrightStatement_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_copyrightStatement_CollectionType))
class EbuCorerightsType_copyrightStatement_CollectionType;
class IEbuCorerightsType_copyrightStatement_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_copyrightStatement_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_copyrightStatement_CollectionType > EbuCorerightsType_copyrightStatement_CollectionPtr;

#define TypeOfrightsType_disclaimer_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_disclaimer_CollectionType")))
#define CreaterightsType_disclaimer_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_disclaimer_CollectionType))
class EbuCorerightsType_disclaimer_CollectionType;
class IEbuCorerightsType_disclaimer_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_disclaimer_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_disclaimer_CollectionType > EbuCorerightsType_disclaimer_CollectionPtr;

#define TypeOfrightsType_exploitationIssues_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_exploitationIssues_CollectionType")))
#define CreaterightsType_exploitationIssues_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_exploitationIssues_CollectionType))
class EbuCorerightsType_exploitationIssues_CollectionType;
class IEbuCorerightsType_exploitationIssues_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_exploitationIssues_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_exploitationIssues_CollectionType > EbuCorerightsType_exploitationIssues_CollectionPtr;

#define TypeOfrightsType_rights_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rights_CollectionType")))
#define CreaterightsType_rights_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_rights_CollectionType))
class EbuCorerightsType_rights_CollectionType;
class IEbuCorerightsType_rights_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_rights_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_rights_CollectionType > EbuCorerightsType_rights_CollectionPtr;

#define TypeOfrightsType_rightsAttributedId_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rightsAttributedId_CollectionType")))
#define CreaterightsType_rightsAttributedId_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_rightsAttributedId_CollectionType))
class EbuCorerightsType_rightsAttributedId_CollectionType;
class IEbuCorerightsType_rightsAttributedId_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_rightsAttributedId_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_rightsAttributedId_CollectionType > EbuCorerightsType_rightsAttributedId_CollectionPtr;

#define TypeOfrightsType_rightsHolder_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:rightsType_rightsHolder_CollectionType")))
#define CreaterightsType_rightsHolder_CollectionType (Dc1Factory::CreateObject(TypeOfrightsType_rightsHolder_CollectionType))
class EbuCorerightsType_rightsHolder_CollectionType;
class IEbuCorerightsType_rightsHolder_CollectionType;
/** Smart pointer for instance of IEbuCorerightsType_rightsHolder_CollectionType */
typedef Dc1Ptr< IEbuCorerightsType_rightsHolder_CollectionType > EbuCorerightsType_rightsHolder_CollectionPtr;

#define TypeOfsigningFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:signingFormatType")))
#define CreatesigningFormatType (Dc1Factory::CreateObject(TypeOfsigningFormatType))
class EbuCoresigningFormatType;
class IEbuCoresigningFormatType;
/** Smart pointer for instance of IEbuCoresigningFormatType */
typedef Dc1Ptr< IEbuCoresigningFormatType > EbuCoresigningFormatPtr;

#define TypeOfspatialType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:spatialType")))
#define CreatespatialType (Dc1Factory::CreateObject(TypeOfspatialType))
class EbuCorespatialType;
class IEbuCorespatialType;
/** Smart pointer for instance of IEbuCorespatialType */
typedef Dc1Ptr< IEbuCorespatialType > EbuCorespatialPtr;

#define TypeOfspatialType_location_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:spatialType_location_CollectionType")))
#define CreatespatialType_location_CollectionType (Dc1Factory::CreateObject(TypeOfspatialType_location_CollectionType))
class EbuCorespatialType_location_CollectionType;
class IEbuCorespatialType_location_CollectionType;
/** Smart pointer for instance of IEbuCorespatialType_location_CollectionType */
typedef Dc1Ptr< IEbuCorespatialType_location_CollectionType > EbuCorespatialType_location_CollectionPtr;

#define TypeOfString (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:String")))
#define CreateString (Dc1Factory::CreateObject(TypeOfString))
class EbuCoreString;
class IEbuCoreString;
/** Smart pointer for instance of IEbuCoreString */
typedef Dc1Ptr< IEbuCoreString > EbuCoreStringPtr;

#define TypeOfsubjectType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subjectType")))
#define CreatesubjectType (Dc1Factory::CreateObject(TypeOfsubjectType))
class EbuCoresubjectType;
class IEbuCoresubjectType;
/** Smart pointer for instance of IEbuCoresubjectType */
typedef Dc1Ptr< IEbuCoresubjectType > EbuCoresubjectPtr;

#define TypeOfsubjectType_subject_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subjectType_subject_CollectionType")))
#define CreatesubjectType_subject_CollectionType (Dc1Factory::CreateObject(TypeOfsubjectType_subject_CollectionType))
class EbuCoresubjectType_subject_CollectionType;
class IEbuCoresubjectType_subject_CollectionType;
/** Smart pointer for instance of IEbuCoresubjectType_subject_CollectionType */
typedef Dc1Ptr< IEbuCoresubjectType_subject_CollectionType > EbuCoresubjectType_subject_CollectionPtr;

#define TypeOfsubjectType_subjectDefinition_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subjectType_subjectDefinition_CollectionType")))
#define CreatesubjectType_subjectDefinition_CollectionType (Dc1Factory::CreateObject(TypeOfsubjectType_subjectDefinition_CollectionType))
class EbuCoresubjectType_subjectDefinition_CollectionType;
class IEbuCoresubjectType_subjectDefinition_CollectionType;
/** Smart pointer for instance of IEbuCoresubjectType_subjectDefinition_CollectionType */
typedef Dc1Ptr< IEbuCoresubjectType_subjectDefinition_CollectionType > EbuCoresubjectType_subjectDefinition_CollectionPtr;

#define TypeOfsubtitlingFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormatType")))
#define CreatesubtitlingFormatType (Dc1Factory::CreateObject(TypeOfsubtitlingFormatType))
class EbuCoresubtitlingFormatType;
class IEbuCoresubtitlingFormatType;
/** Smart pointer for instance of IEbuCoresubtitlingFormatType */
typedef Dc1Ptr< IEbuCoresubtitlingFormatType > EbuCoresubtitlingFormatPtr;

#define TypeOftargetAudienceType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType")))
#define CreatetargetAudienceType (Dc1Factory::CreateObject(TypeOftargetAudienceType))
class EbuCoretargetAudienceType;
class IEbuCoretargetAudienceType;
/** Smart pointer for instance of IEbuCoretargetAudienceType */
typedef Dc1Ptr< IEbuCoretargetAudienceType > EbuCoretargetAudiencePtr;

#define TypeOftargetAudienceType_targetExclusionRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType_targetExclusionRegion_CollectionType")))
#define CreatetargetAudienceType_targetExclusionRegion_CollectionType (Dc1Factory::CreateObject(TypeOftargetAudienceType_targetExclusionRegion_CollectionType))
class EbuCoretargetAudienceType_targetExclusionRegion_CollectionType;
class IEbuCoretargetAudienceType_targetExclusionRegion_CollectionType;
/** Smart pointer for instance of IEbuCoretargetAudienceType_targetExclusionRegion_CollectionType */
typedef Dc1Ptr< IEbuCoretargetAudienceType_targetExclusionRegion_CollectionType > EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr;

#define TypeOftargetAudienceType_targetRegion_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:targetAudienceType_targetRegion_CollectionType")))
#define CreatetargetAudienceType_targetRegion_CollectionType (Dc1Factory::CreateObject(TypeOftargetAudienceType_targetRegion_CollectionType))
class EbuCoretargetAudienceType_targetRegion_CollectionType;
class IEbuCoretargetAudienceType_targetRegion_CollectionType;
/** Smart pointer for instance of IEbuCoretargetAudienceType_targetRegion_CollectionType */
typedef Dc1Ptr< IEbuCoretargetAudienceType_targetRegion_CollectionType > EbuCoretargetAudienceType_targetRegion_CollectionPtr;

#define TypeOftechnicalAttributeRationalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRationalType")))
#define CreatetechnicalAttributeRationalType (Dc1Factory::CreateObject(TypeOftechnicalAttributeRationalType))
class EbuCoretechnicalAttributeRationalType;
class IEbuCoretechnicalAttributeRationalType;
/** Smart pointer for instance of IEbuCoretechnicalAttributeRationalType */
typedef Dc1Ptr< IEbuCoretechnicalAttributeRationalType > EbuCoretechnicalAttributeRationalPtr;

#define TypeOftechnicalAttributes (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes")))
#define CreatetechnicalAttributes (Dc1Factory::CreateObject(TypeOftechnicalAttributes))
class EbuCoretechnicalAttributes;
class IEbuCoretechnicalAttributes;
/** Smart pointer for instance of IEbuCoretechnicalAttributes */
typedef Dc1Ptr< IEbuCoretechnicalAttributes > EbuCoretechnicalAttributesPtr;

#define TypeOftechnicalAttributes_technicalAttributeBoolean_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeBoolean_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeBoolean_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeBoolean_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType > EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeByte_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeByte_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeByte_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeByte_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeByte_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeByte_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeByte_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeByte_CollectionType > EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeFloat_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeFloat_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeFloat_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeFloat_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType > EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeInteger_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeInteger_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeInteger_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeInteger_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType > EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeLong_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeLong_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeLong_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeLong_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeLong_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeLong_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeLong_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeLong_CollectionType > EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeRational_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeRational_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeRational_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeRational_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeRational_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeRational_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeRational_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeRational_CollectionType > EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeShort_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeShort_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeShort_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeShort_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeShort_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeShort_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeShort_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeShort_CollectionType > EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeString_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeString_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeString_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeString_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeString_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeString_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeString_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeString_CollectionType > EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeUnsignedByte_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedByte_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeUnsignedByte_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeUnsignedByte_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedInteger_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeUnsignedLong_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedLong_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeUnsignedLong_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeUnsignedLong_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeUnsignedShort_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUnsignedShort_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeUnsignedShort_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeUnsignedShort_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr;

#define TypeOftechnicalAttributes_technicalAttributeUri_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributes_technicalAttributeUri_CollectionType")))
#define CreatetechnicalAttributes_technicalAttributeUri_CollectionType (Dc1Factory::CreateObject(TypeOftechnicalAttributes_technicalAttributeUri_CollectionType))
class EbuCoretechnicalAttributes_technicalAttributeUri_CollectionType;
class IEbuCoretechnicalAttributes_technicalAttributeUri_CollectionType;
/** Smart pointer for instance of IEbuCoretechnicalAttributes_technicalAttributeUri_CollectionType */
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUri_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr;

#define TypeOftechnicalAttributeUriType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUriType")))
#define CreatetechnicalAttributeUriType (Dc1Factory::CreateObject(TypeOftechnicalAttributeUriType))
class EbuCoretechnicalAttributeUriType;
class IEbuCoretechnicalAttributeUriType;
/** Smart pointer for instance of IEbuCoretechnicalAttributeUriType */
typedef Dc1Ptr< IEbuCoretechnicalAttributeUriType > EbuCoretechnicalAttributeUriPtr;

#define TypeOftemporalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:temporalType")))
#define CreatetemporalType (Dc1Factory::CreateObject(TypeOftemporalType))
class EbuCoretemporalType;
class IEbuCoretemporalType;
/** Smart pointer for instance of IEbuCoretemporalType */
typedef Dc1Ptr< IEbuCoretemporalType > EbuCoretemporalPtr;

#define TypeOftemporalType_PeriodOfTime_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:temporalType_PeriodOfTime_CollectionType")))
#define CreatetemporalType_PeriodOfTime_CollectionType (Dc1Factory::CreateObject(TypeOftemporalType_PeriodOfTime_CollectionType))
class EbuCoretemporalType_PeriodOfTime_CollectionType;
class IEbuCoretemporalType_PeriodOfTime_CollectionType;
/** Smart pointer for instance of IEbuCoretemporalType_PeriodOfTime_CollectionType */
typedef Dc1Ptr< IEbuCoretemporalType_PeriodOfTime_CollectionType > EbuCoretemporalType_PeriodOfTime_CollectionPtr;

#define TypeOftimecodeType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timecodeType")))
#define CreatetimecodeType (Dc1Factory::CreateObject(TypeOftimecodeType))
class EbuCoretimecodeType;
class IEbuCoretimecodeType;
/** Smart pointer for instance of IEbuCoretimecodeType */
typedef Dc1Ptr< IEbuCoretimecodeType > EbuCoretimecodePtr;

#define TypeOftimeType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType")))
#define CreatetimeType (Dc1Factory::CreateObject(TypeOftimeType))
class EbuCoretimeType;
class IEbuCoretimeType;
/** Smart pointer for instance of IEbuCoretimeType */
typedef Dc1Ptr< IEbuCoretimeType > EbuCoretimePtr;

#define TypeOftimeType_time_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:timeType_time_LocalType")))
#define CreatetimeType_time_LocalType (Dc1Factory::CreateObject(TypeOftimeType_time_LocalType))
class EbuCoretimeType_time_LocalType;
class IEbuCoretimeType_time_LocalType;
/** Smart pointer for instance of IEbuCoretimeType_time_LocalType */
typedef Dc1Ptr< IEbuCoretimeType_time_LocalType > EbuCoretimeType_time_LocalPtr;

#define TypeOftitleType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:titleType")))
#define CreatetitleType (Dc1Factory::CreateObject(TypeOftitleType))
class EbuCoretitleType;
class IEbuCoretitleType;
/** Smart pointer for instance of IEbuCoretitleType */
typedef Dc1Ptr< IEbuCoretitleType > EbuCoretitlePtr;

#define TypeOftitleType_title_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:titleType_title_CollectionType")))
#define CreatetitleType_title_CollectionType (Dc1Factory::CreateObject(TypeOftitleType_title_CollectionType))
class EbuCoretitleType_title_CollectionType;
class IEbuCoretitleType_title_CollectionType;
/** Smart pointer for instance of IEbuCoretitleType_title_CollectionType */
typedef Dc1Ptr< IEbuCoretitleType_title_CollectionType > EbuCoretitleType_title_CollectionPtr;

#define TypeOftypeType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType")))
#define CreatetypeType (Dc1Factory::CreateObject(TypeOftypeType))
class EbuCoretypeType;
class IEbuCoretypeType;
/** Smart pointer for instance of IEbuCoretypeType */
typedef Dc1Ptr< IEbuCoretypeType > EbuCoretypePtr;

#define TypeOftypeType_genre_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_genre_CollectionType")))
#define CreatetypeType_genre_CollectionType (Dc1Factory::CreateObject(TypeOftypeType_genre_CollectionType))
class EbuCoretypeType_genre_CollectionType;
class IEbuCoretypeType_genre_CollectionType;
/** Smart pointer for instance of IEbuCoretypeType_genre_CollectionType */
typedef Dc1Ptr< IEbuCoretypeType_genre_CollectionType > EbuCoretypeType_genre_CollectionPtr;

#define TypeOftypeType_genre_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_genre_LocalType")))
#define CreatetypeType_genre_LocalType (Dc1Factory::CreateObject(TypeOftypeType_genre_LocalType))
class EbuCoretypeType_genre_LocalType;
class IEbuCoretypeType_genre_LocalType;
/** Smart pointer for instance of IEbuCoretypeType_genre_LocalType */
typedef Dc1Ptr< IEbuCoretypeType_genre_LocalType > EbuCoretypeType_genre_LocalPtr;

#define TypeOftypeType_objectType_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_objectType_CollectionType")))
#define CreatetypeType_objectType_CollectionType (Dc1Factory::CreateObject(TypeOftypeType_objectType_CollectionType))
class EbuCoretypeType_objectType_CollectionType;
class IEbuCoretypeType_objectType_CollectionType;
/** Smart pointer for instance of IEbuCoretypeType_objectType_CollectionType */
typedef Dc1Ptr< IEbuCoretypeType_objectType_CollectionType > EbuCoretypeType_objectType_CollectionPtr;

#define TypeOftypeType_objectType_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_objectType_LocalType")))
#define CreatetypeType_objectType_LocalType (Dc1Factory::CreateObject(TypeOftypeType_objectType_LocalType))
class EbuCoretypeType_objectType_LocalType;
class IEbuCoretypeType_objectType_LocalType;
/** Smart pointer for instance of IEbuCoretypeType_objectType_LocalType */
typedef Dc1Ptr< IEbuCoretypeType_objectType_LocalType > EbuCoretypeType_objectType_LocalPtr;

#define TypeOftypeType_targetAudience_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_targetAudience_CollectionType")))
#define CreatetypeType_targetAudience_CollectionType (Dc1Factory::CreateObject(TypeOftypeType_targetAudience_CollectionType))
class EbuCoretypeType_targetAudience_CollectionType;
class IEbuCoretypeType_targetAudience_CollectionType;
/** Smart pointer for instance of IEbuCoretypeType_targetAudience_CollectionType */
typedef Dc1Ptr< IEbuCoretypeType_targetAudience_CollectionType > EbuCoretypeType_targetAudience_CollectionPtr;

#define TypeOftypeType_type_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:typeType_type_CollectionType")))
#define CreatetypeType_type_CollectionType (Dc1Factory::CreateObject(TypeOftypeType_type_CollectionType))
class EbuCoretypeType_type_CollectionType;
class IEbuCoretypeType_type_CollectionType;
/** Smart pointer for instance of IEbuCoretypeType_type_CollectionType */
typedef Dc1Ptr< IEbuCoretypeType_type_CollectionType > EbuCoretypeType_type_CollectionPtr;

#define TypeOfUInt16 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt16")))
#define CreateUInt16 (Dc1Factory::CreateObject(TypeOfUInt16))
class EbuCoreUInt16;
class IEbuCoreUInt16;
/** Smart pointer for instance of IEbuCoreUInt16 */
typedef Dc1Ptr< IEbuCoreUInt16 > EbuCoreUInt16Ptr;

#define TypeOfUInt32 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt32")))
#define CreateUInt32 (Dc1Factory::CreateObject(TypeOfUInt32))
class EbuCoreUInt32;
class IEbuCoreUInt32;
/** Smart pointer for instance of IEbuCoreUInt32 */
typedef Dc1Ptr< IEbuCoreUInt32 > EbuCoreUInt32Ptr;

#define TypeOfUInt64 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt64")))
#define CreateUInt64 (Dc1Factory::CreateObject(TypeOfUInt64))
class EbuCoreUInt64;
class IEbuCoreUInt64;
/** Smart pointer for instance of IEbuCoreUInt64 */
typedef Dc1Ptr< IEbuCoreUInt64 > EbuCoreUInt64Ptr;

#define TypeOfUInt8 (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:UInt8")))
#define CreateUInt8 (Dc1Factory::CreateObject(TypeOfUInt8))
class EbuCoreUInt8;
class IEbuCoreUInt8;
/** Smart pointer for instance of IEbuCoreUInt8 */
typedef Dc1Ptr< IEbuCoreUInt8 > EbuCoreUInt8Ptr;

#define TypeOfversionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:versionType")))
#define CreateversionType (Dc1Factory::CreateObject(TypeOfversionType))
class EbuCoreversionType;
class IEbuCoreversionType;
/** Smart pointer for instance of IEbuCoreversionType */
typedef Dc1Ptr< IEbuCoreversionType > EbuCoreversionPtr;

#define TypeOfvideoFormatType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType")))
#define CreatevideoFormatType (Dc1Factory::CreateObject(TypeOfvideoFormatType))
class EbuCorevideoFormatType;
class IEbuCorevideoFormatType;
/** Smart pointer for instance of IEbuCorevideoFormatType */
typedef Dc1Ptr< IEbuCorevideoFormatType > EbuCorevideoFormatPtr;

#define TypeOfvideoFormatType_aspectRatio_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_aspectRatio_CollectionType")))
#define CreatevideoFormatType_aspectRatio_CollectionType (Dc1Factory::CreateObject(TypeOfvideoFormatType_aspectRatio_CollectionType))
class EbuCorevideoFormatType_aspectRatio_CollectionType;
class IEbuCorevideoFormatType_aspectRatio_CollectionType;
/** Smart pointer for instance of IEbuCorevideoFormatType_aspectRatio_CollectionType */
typedef Dc1Ptr< IEbuCorevideoFormatType_aspectRatio_CollectionType > EbuCorevideoFormatType_aspectRatio_CollectionPtr;

#define TypeOfvideoFormatType_bitRateMode_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_bitRateMode_LocalType")))
#define CreatevideoFormatType_bitRateMode_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_bitRateMode_LocalType))
class EbuCorevideoFormatType_bitRateMode_LocalType;
class IEbuCorevideoFormatType_bitRateMode_LocalType;
// No smart pointer instance for IEbuCorevideoFormatType_bitRateMode_LocalType

#define TypeOfvideoFormatType_comment_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_comment_CollectionType")))
#define CreatevideoFormatType_comment_CollectionType (Dc1Factory::CreateObject(TypeOfvideoFormatType_comment_CollectionType))
class EbuCorevideoFormatType_comment_CollectionType;
class IEbuCorevideoFormatType_comment_CollectionType;
/** Smart pointer for instance of IEbuCorevideoFormatType_comment_CollectionType */
typedef Dc1Ptr< IEbuCorevideoFormatType_comment_CollectionType > EbuCorevideoFormatType_comment_CollectionPtr;

#define TypeOfvideoFormatType_height_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_height_CollectionType")))
#define CreatevideoFormatType_height_CollectionType (Dc1Factory::CreateObject(TypeOfvideoFormatType_height_CollectionType))
class EbuCorevideoFormatType_height_CollectionType;
class IEbuCorevideoFormatType_height_CollectionType;
/** Smart pointer for instance of IEbuCorevideoFormatType_height_CollectionType */
typedef Dc1Ptr< IEbuCorevideoFormatType_height_CollectionType > EbuCorevideoFormatType_height_CollectionPtr;

#define TypeOfvideoFormatType_height_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_height_LocalType")))
#define CreatevideoFormatType_height_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_height_LocalType))
class EbuCorevideoFormatType_height_LocalType;
class IEbuCorevideoFormatType_height_LocalType;
/** Smart pointer for instance of IEbuCorevideoFormatType_height_LocalType */
typedef Dc1Ptr< IEbuCorevideoFormatType_height_LocalType > EbuCorevideoFormatType_height_LocalPtr;

#define TypeOfvideoFormatType_scanningFormat_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_scanningFormat_LocalType")))
#define CreatevideoFormatType_scanningFormat_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_scanningFormat_LocalType))
class EbuCorevideoFormatType_scanningFormat_LocalType;
class IEbuCorevideoFormatType_scanningFormat_LocalType;
// No smart pointer instance for IEbuCorevideoFormatType_scanningFormat_LocalType

#define TypeOfvideoFormatType_scanningOrder_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_scanningOrder_LocalType")))
#define CreatevideoFormatType_scanningOrder_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_scanningOrder_LocalType))
class EbuCorevideoFormatType_scanningOrder_LocalType;
class IEbuCorevideoFormatType_scanningOrder_LocalType;
// No smart pointer instance for IEbuCorevideoFormatType_scanningOrder_LocalType

#define TypeOfvideoFormatType_videoEncoding_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoEncoding_LocalType")))
#define CreatevideoFormatType_videoEncoding_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_videoEncoding_LocalType))
class EbuCorevideoFormatType_videoEncoding_LocalType;
class IEbuCorevideoFormatType_videoEncoding_LocalType;
/** Smart pointer for instance of IEbuCorevideoFormatType_videoEncoding_LocalType */
typedef Dc1Ptr< IEbuCorevideoFormatType_videoEncoding_LocalType > EbuCorevideoFormatType_videoEncoding_LocalPtr;

#define TypeOfvideoFormatType_videoTrack_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoTrack_CollectionType")))
#define CreatevideoFormatType_videoTrack_CollectionType (Dc1Factory::CreateObject(TypeOfvideoFormatType_videoTrack_CollectionType))
class EbuCorevideoFormatType_videoTrack_CollectionType;
class IEbuCorevideoFormatType_videoTrack_CollectionType;
/** Smart pointer for instance of IEbuCorevideoFormatType_videoTrack_CollectionType */
typedef Dc1Ptr< IEbuCorevideoFormatType_videoTrack_CollectionType > EbuCorevideoFormatType_videoTrack_CollectionPtr;

#define TypeOfvideoFormatType_videoTrack_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_videoTrack_LocalType")))
#define CreatevideoFormatType_videoTrack_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_videoTrack_LocalType))
class EbuCorevideoFormatType_videoTrack_LocalType;
class IEbuCorevideoFormatType_videoTrack_LocalType;
/** Smart pointer for instance of IEbuCorevideoFormatType_videoTrack_LocalType */
typedef Dc1Ptr< IEbuCorevideoFormatType_videoTrack_LocalType > EbuCorevideoFormatType_videoTrack_LocalPtr;

#define TypeOfvideoFormatType_width_CollectionType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_width_CollectionType")))
#define CreatevideoFormatType_width_CollectionType (Dc1Factory::CreateObject(TypeOfvideoFormatType_width_CollectionType))
class EbuCorevideoFormatType_width_CollectionType;
class IEbuCorevideoFormatType_width_CollectionType;
/** Smart pointer for instance of IEbuCorevideoFormatType_width_CollectionType */
typedef Dc1Ptr< IEbuCorevideoFormatType_width_CollectionType > EbuCorevideoFormatType_width_CollectionPtr;

#define TypeOfvideoFormatType_width_LocalType (Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:videoFormatType_width_LocalType")))
#define CreatevideoFormatType_width_LocalType (Dc1Factory::CreateObject(TypeOfvideoFormatType_width_LocalType))
class EbuCorevideoFormatType_width_LocalType;
class IEbuCorevideoFormatType_width_LocalType;
/** Smart pointer for instance of IEbuCorevideoFormatType_width_LocalType */
typedef Dc1Ptr< IEbuCorevideoFormatType_width_LocalType > EbuCorevideoFormatType_width_LocalPtr;

#include "Dc1PtrTypes.h"

#endif // EBUCOREFACTORYDEFINES_H
