/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _629EBUCOREVIDEOFORMATTYPE_H
#define _629EBUCOREVIDEOFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCorevideoFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtInclude.h


class IEbuCorevideoFormatType;
typedef Dc1Ptr< IEbuCorevideoFormatType> EbuCorevideoFormatPtr;
class IEbuCorevideoFormatType_width_CollectionType;
typedef Dc1Ptr< IEbuCorevideoFormatType_width_CollectionType > EbuCorevideoFormatType_width_CollectionPtr;
class IEbuCorevideoFormatType_height_CollectionType;
typedef Dc1Ptr< IEbuCorevideoFormatType_height_CollectionType > EbuCorevideoFormatType_height_CollectionPtr;
class IEbuCorerationalType;
typedef Dc1Ptr< IEbuCorerationalType > EbuCorerationalPtr;
class IEbuCorevideoFormatType_aspectRatio_CollectionType;
typedef Dc1Ptr< IEbuCorevideoFormatType_aspectRatio_CollectionType > EbuCorevideoFormatType_aspectRatio_CollectionPtr;
class IEbuCorevideoFormatType_videoEncoding_LocalType;
typedef Dc1Ptr< IEbuCorevideoFormatType_videoEncoding_LocalType > EbuCorevideoFormatType_videoEncoding_LocalPtr;
class IEbuCorecodecType;
typedef Dc1Ptr< IEbuCorecodecType > EbuCorecodecPtr;
#include "EbuCorevideoFormatType_bitRateMode_LocalType.h"
#include "EbuCorevideoFormatType_scanningFormat_LocalType.h"
#include "EbuCorevideoFormatType_scanningOrder_LocalType.h"
class IEbuCorevideoFormatType_videoTrack_CollectionType;
typedef Dc1Ptr< IEbuCorevideoFormatType_videoTrack_CollectionType > EbuCorevideoFormatType_videoTrack_CollectionPtr;
class IEbuCoretechnicalAttributes;
typedef Dc1Ptr< IEbuCoretechnicalAttributes > EbuCoretechnicalAttributesPtr;
class IEbuCorevideoFormatType_comment_CollectionType;
typedef Dc1Ptr< IEbuCorevideoFormatType_comment_CollectionType > EbuCorevideoFormatType_comment_CollectionPtr;

/** 
 * Generated interface IEbuCorevideoFormatType for class EbuCorevideoFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2870<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorevideoFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorevideoFormatType();
	virtual ~IEbuCorevideoFormatType();
	/**
	 * Get videoFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatId() const = 0;

	/**
	 * Get videoFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatId() const = 0;

	/**
	 * Set videoFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatId() = 0;

	/**
	 * Get videoFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatVersionId() const = 0;

	/**
	 * Get videoFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatVersionId() const = 0;

	/**
	 * Set videoFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatVersionId() = 0;

	/**
	 * Get videoFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatName() const = 0;

	/**
	 * Get videoFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatName() const = 0;

	/**
	 * Set videoFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatName() = 0;

	/**
	 * Get videoFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatDefinition() const = 0;

	/**
	 * Get videoFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatDefinition() const = 0;

	/**
	 * Set videoFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatDefinition() = 0;

	/**
	 * Get videoPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetvideoPresenceFlag() const = 0;

	/**
	 * Get videoPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoPresenceFlag() const = 0;

	/**
	 * Set videoPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetvideoPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoPresenceFlag() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get regionDelimX element.
	 * @return unsigned
	 */
	virtual unsigned GetregionDelimX() const = 0;

	virtual bool IsValidregionDelimX() const = 0;

	/**
	 * Set regionDelimX element.
	 * @param item unsigned
	 */
	virtual void SetregionDelimX(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate regionDelimX element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateregionDelimX() = 0;

	/**
	 * Get regionDelimY element.
	 * @return unsigned
	 */
	virtual unsigned GetregionDelimY() const = 0;

	virtual bool IsValidregionDelimY() const = 0;

	/**
	 * Set regionDelimY element.
	 * @param item unsigned
	 */
	virtual void SetregionDelimY(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate regionDelimY element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateregionDelimY() = 0;

	/**
	 * Get width element.
	 * @return EbuCorevideoFormatType_width_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_width_CollectionPtr Getwidth() const = 0;

	/**
	 * Set width element.
	 * @param item const EbuCorevideoFormatType_width_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setwidth(const EbuCorevideoFormatType_width_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get height element.
	 * @return EbuCorevideoFormatType_height_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_height_CollectionPtr Getheight() const = 0;

	/**
	 * Set height element.
	 * @param item const EbuCorevideoFormatType_height_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setheight(const EbuCorevideoFormatType_height_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get lines element.
	 * @return unsigned
	 */
	virtual unsigned Getlines() const = 0;

	virtual bool IsValidlines() const = 0;

	/**
	 * Set lines element.
	 * @param item unsigned
	 */
	virtual void Setlines(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lines element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelines() = 0;

	/**
	 * Get frameRate element.
	 * @return EbuCorerationalPtr
	 */
	virtual EbuCorerationalPtr GetframeRate() const = 0;

	virtual bool IsValidframeRate() const = 0;

	/**
	 * Set frameRate element.
	 * @param item const EbuCorerationalPtr &
	 */
	virtual void SetframeRate(const EbuCorerationalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate frameRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateframeRate() = 0;

	/**
	 * Get aspectRatio element.
	 * @return EbuCorevideoFormatType_aspectRatio_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_aspectRatio_CollectionPtr GetaspectRatio() const = 0;

	/**
	 * Set aspectRatio element.
	 * @param item const EbuCorevideoFormatType_aspectRatio_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaspectRatio(const EbuCorevideoFormatType_aspectRatio_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get videoEncoding element.
	 * @return EbuCorevideoFormatType_videoEncoding_LocalPtr
	 */
	virtual EbuCorevideoFormatType_videoEncoding_LocalPtr GetvideoEncoding() const = 0;

	virtual bool IsValidvideoEncoding() const = 0;

	/**
	 * Set videoEncoding element.
	 * @param item const EbuCorevideoFormatType_videoEncoding_LocalPtr &
	 */
	virtual void SetvideoEncoding(const EbuCorevideoFormatType_videoEncoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate videoEncoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoEncoding() = 0;

	/**
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const = 0;

	virtual bool IsValidcodec() const = 0;

	/**
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec() = 0;

	/**
	 * Get bitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRate() const = 0;

	virtual bool IsValidbitRate() const = 0;

	/**
	 * Set bitRate element.
	 * @param item unsigned
	 */
	virtual void SetbitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRate() = 0;

	/**
	 * Get bitRateMax element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRateMax() const = 0;

	virtual bool IsValidbitRateMax() const = 0;

	/**
	 * Set bitRateMax element.
	 * @param item unsigned
	 */
	virtual void SetbitRateMax(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRateMax element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMax() = 0;

	/**
	 * Get bitRateMode element.
	 * @return EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration GetbitRateMode() const = 0;

	virtual bool IsValidbitRateMode() const = 0;

	/**
	 * Set bitRateMode element.
	 * @param item EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual void SetbitRateMode(EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRateMode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMode() = 0;

	/**
	 * Get scanningFormat element.
	 * @return EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration GetscanningFormat() const = 0;

	virtual bool IsValidscanningFormat() const = 0;

	/**
	 * Set scanningFormat element.
	 * @param item EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration
	 */
	virtual void SetscanningFormat(EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate scanningFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescanningFormat() = 0;

	/**
	 * Get scanningOrder element.
	 * @return EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration GetscanningOrder() const = 0;

	virtual bool IsValidscanningOrder() const = 0;

	/**
	 * Set scanningOrder element.
	 * @param item EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration
	 */
	virtual void SetscanningOrder(EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate scanningOrder element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescanningOrder() = 0;

	/**
	 * Get noiseFilter element.
	 * @return bool
	 */
	virtual bool GetnoiseFilter() const = 0;

	virtual bool IsValidnoiseFilter() const = 0;

	/**
	 * Set noiseFilter element.
	 * @param item bool
	 */
	virtual void SetnoiseFilter(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate noiseFilter element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoiseFilter() = 0;

	/**
	 * Get videoTrack element.
	 * @return EbuCorevideoFormatType_videoTrack_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_videoTrack_CollectionPtr GetvideoTrack() const = 0;

	/**
	 * Set videoTrack element.
	 * @param item const EbuCorevideoFormatType_videoTrack_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetvideoTrack(const EbuCorevideoFormatType_videoTrack_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get flag_3D element.
	 * @return bool
	 */
	virtual bool Getflag_3D() const = 0;

	virtual bool IsValidflag_3D() const = 0;

	/**
	 * Set flag_3D element.
	 * @param item bool
	 */
	virtual void Setflag_3D(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate flag_3D element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateflag_3D() = 0;

	/**
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const = 0;

	/**
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get comment element.
	 * @return EbuCorevideoFormatType_comment_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_comment_CollectionPtr Getcomment() const = 0;

	/**
	 * Set comment element.
	 * @param item const EbuCorevideoFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCorevideoFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of videoFormatType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:width</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:height</li>
	 * <li>frameRate</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:aspectRatio</li>
	 * <li>videoEncoding</li>
	 * <li>codec</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:videoTrack</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCorevideoFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2870<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 2870.<br>
 */
class DC1_EXPORT EbuCorevideoFormatType :
		public IEbuCorevideoFormatType
{
	friend class EbuCorevideoFormatTypeFactory; // constructs objects

public:
	/*
	 * Get videoFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatId() const;

	/*
	 * Get videoFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatId() const;

	/*
	 * Set videoFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatId();

	/*
	 * Get videoFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatVersionId() const;

	/*
	 * Get videoFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatVersionId() const;

	/*
	 * Set videoFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatVersionId();

	/*
	 * Get videoFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatName() const;

	/*
	 * Get videoFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatName() const;

	/*
	 * Set videoFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatName();

	/*
	 * Get videoFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetvideoFormatDefinition() const;

	/*
	 * Get videoFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoFormatDefinition() const;

	/*
	 * Set videoFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetvideoFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoFormatDefinition();

	/*
	 * Get videoPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetvideoPresenceFlag() const;

	/*
	 * Get videoPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistvideoPresenceFlag() const;

	/*
	 * Set videoPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetvideoPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoPresenceFlag();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get regionDelimX element.
	 * @return unsigned
	 */
	virtual unsigned GetregionDelimX() const;

	virtual bool IsValidregionDelimX() const;

	/*
	 * Set regionDelimX element.
	 * @param item unsigned
	 */
	virtual void SetregionDelimX(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate regionDelimX element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateregionDelimX();

	/*
	 * Get regionDelimY element.
	 * @return unsigned
	 */
	virtual unsigned GetregionDelimY() const;

	virtual bool IsValidregionDelimY() const;

	/*
	 * Set regionDelimY element.
	 * @param item unsigned
	 */
	virtual void SetregionDelimY(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate regionDelimY element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateregionDelimY();

	/*
	 * Get width element.
	 * @return EbuCorevideoFormatType_width_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_width_CollectionPtr Getwidth() const;

	/*
	 * Set width element.
	 * @param item const EbuCorevideoFormatType_width_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setwidth(const EbuCorevideoFormatType_width_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get height element.
	 * @return EbuCorevideoFormatType_height_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_height_CollectionPtr Getheight() const;

	/*
	 * Set height element.
	 * @param item const EbuCorevideoFormatType_height_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setheight(const EbuCorevideoFormatType_height_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get lines element.
	 * @return unsigned
	 */
	virtual unsigned Getlines() const;

	virtual bool IsValidlines() const;

	/*
	 * Set lines element.
	 * @param item unsigned
	 */
	virtual void Setlines(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lines element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelines();

	/*
	 * Get frameRate element.
	 * @return EbuCorerationalPtr
	 */
	virtual EbuCorerationalPtr GetframeRate() const;

	virtual bool IsValidframeRate() const;

	/*
	 * Set frameRate element.
	 * @param item const EbuCorerationalPtr &
	 */
	virtual void SetframeRate(const EbuCorerationalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate frameRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateframeRate();

	/*
	 * Get aspectRatio element.
	 * @return EbuCorevideoFormatType_aspectRatio_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_aspectRatio_CollectionPtr GetaspectRatio() const;

	/*
	 * Set aspectRatio element.
	 * @param item const EbuCorevideoFormatType_aspectRatio_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaspectRatio(const EbuCorevideoFormatType_aspectRatio_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get videoEncoding element.
	 * @return EbuCorevideoFormatType_videoEncoding_LocalPtr
	 */
	virtual EbuCorevideoFormatType_videoEncoding_LocalPtr GetvideoEncoding() const;

	virtual bool IsValidvideoEncoding() const;

	/*
	 * Set videoEncoding element.
	 * @param item const EbuCorevideoFormatType_videoEncoding_LocalPtr &
	 */
	virtual void SetvideoEncoding(const EbuCorevideoFormatType_videoEncoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate videoEncoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatevideoEncoding();

	/*
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const;

	virtual bool IsValidcodec() const;

	/*
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec();

	/*
	 * Get bitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRate() const;

	virtual bool IsValidbitRate() const;

	/*
	 * Set bitRate element.
	 * @param item unsigned
	 */
	virtual void SetbitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRate();

	/*
	 * Get bitRateMax element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRateMax() const;

	virtual bool IsValidbitRateMax() const;

	/*
	 * Set bitRateMax element.
	 * @param item unsigned
	 */
	virtual void SetbitRateMax(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRateMax element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMax();

	/*
	 * Get bitRateMode element.
	 * @return EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration GetbitRateMode() const;

	virtual bool IsValidbitRateMode() const;

	/*
	 * Set bitRateMode element.
	 * @param item EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual void SetbitRateMode(EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRateMode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMode();

	/*
	 * Get scanningFormat element.
	 * @return EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration GetscanningFormat() const;

	virtual bool IsValidscanningFormat() const;

	/*
	 * Set scanningFormat element.
	 * @param item EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration
	 */
	virtual void SetscanningFormat(EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate scanningFormat element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescanningFormat();

	/*
	 * Get scanningOrder element.
	 * @return EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration
	 */
	virtual EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration GetscanningOrder() const;

	virtual bool IsValidscanningOrder() const;

	/*
	 * Set scanningOrder element.
	 * @param item EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration
	 */
	virtual void SetscanningOrder(EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate scanningOrder element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescanningOrder();

	/*
	 * Get noiseFilter element.
	 * @return bool
	 */
	virtual bool GetnoiseFilter() const;

	virtual bool IsValidnoiseFilter() const;

	/*
	 * Set noiseFilter element.
	 * @param item bool
	 */
	virtual void SetnoiseFilter(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate noiseFilter element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatenoiseFilter();

	/*
	 * Get videoTrack element.
	 * @return EbuCorevideoFormatType_videoTrack_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_videoTrack_CollectionPtr GetvideoTrack() const;

	/*
	 * Set videoTrack element.
	 * @param item const EbuCorevideoFormatType_videoTrack_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetvideoTrack(const EbuCorevideoFormatType_videoTrack_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get flag_3D element.
	 * @return bool
	 */
	virtual bool Getflag_3D() const;

	virtual bool IsValidflag_3D() const;

	/*
	 * Set flag_3D element.
	 * @param item bool
	 */
	virtual void Setflag_3D(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate flag_3D element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateflag_3D();

	/*
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const;

	/*
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get comment element.
	 * @return EbuCorevideoFormatType_comment_CollectionPtr
	 */
	virtual EbuCorevideoFormatType_comment_CollectionPtr Getcomment() const;

	/*
	 * Set comment element.
	 * @param item const EbuCorevideoFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCorevideoFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of videoFormatType.
	 * Currently this type contains 9 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:width</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:height</li>
	 * <li>frameRate</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:aspectRatio</li>
	 * <li>videoEncoding</li>
	 * <li>codec</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:videoTrack</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCorevideoFormatType();

protected:
	EbuCorevideoFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_videoFormatId;
	bool m_videoFormatId_Exist;
	XMLCh * m_videoFormatVersionId;
	bool m_videoFormatVersionId_Exist;
	XMLCh * m_videoFormatName;
	bool m_videoFormatName_Exist;
	XMLCh * m_videoFormatDefinition;
	bool m_videoFormatDefinition_Exist;
	bool m_videoPresenceFlag;
	bool m_videoPresenceFlag_Exist;


	unsigned m_regionDelimX;
	bool m_regionDelimX_Exist; // For optional elements 
	unsigned m_regionDelimY;
	bool m_regionDelimY_Exist; // For optional elements 
	EbuCorevideoFormatType_width_CollectionPtr m_width;
	EbuCorevideoFormatType_height_CollectionPtr m_height;
	unsigned m_lines;
	bool m_lines_Exist; // For optional elements 
	EbuCorerationalPtr m_frameRate;
	bool m_frameRate_Exist; // For optional elements 
	EbuCorevideoFormatType_aspectRatio_CollectionPtr m_aspectRatio;
	EbuCorevideoFormatType_videoEncoding_LocalPtr m_videoEncoding;
	bool m_videoEncoding_Exist; // For optional elements 
	EbuCorecodecPtr m_codec;
	bool m_codec_Exist; // For optional elements 
	unsigned m_bitRate;
	bool m_bitRate_Exist; // For optional elements 
	unsigned m_bitRateMax;
	bool m_bitRateMax_Exist; // For optional elements 
	EbuCorevideoFormatType_bitRateMode_LocalType::Enumeration m_bitRateMode;
	bool m_bitRateMode_Exist; // For optional elements 
	EbuCorevideoFormatType_scanningFormat_LocalType::Enumeration m_scanningFormat;
	bool m_scanningFormat_Exist; // For optional elements 
	EbuCorevideoFormatType_scanningOrder_LocalType::Enumeration m_scanningOrder;
	bool m_scanningOrder_Exist; // For optional elements 
	bool m_noiseFilter;
	bool m_noiseFilter_Exist; // For optional elements 
	EbuCorevideoFormatType_videoTrack_CollectionPtr m_videoTrack;
	bool m_flag_3D;
	bool m_flag_3D_Exist; // For optional elements 
	EbuCoretechnicalAttributesPtr m_technicalAttributes;
	EbuCorevideoFormatType_comment_CollectionPtr m_comment;

// no includefile for extension defined 
// file EbuCorevideoFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _629EBUCOREVIDEOFORMATTYPE_H

