/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _141EBUCOREAUDIOFORMATTYPE_H
#define _141EBUCOREAUDIOFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtInclude.h


class IEbuCoreaudioFormatType;
typedef Dc1Ptr< IEbuCoreaudioFormatType> EbuCoreaudioFormatPtr;
class IEbuCoreaudioFormatType_audioEncoding_LocalType;
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioEncoding_LocalType > EbuCoreaudioFormatType_audioEncoding_LocalPtr;
class IEbuCorecodecType;
typedef Dc1Ptr< IEbuCorecodecType > EbuCorecodecPtr;
class IEbuCoreaudioFormatType_audioTrackConfiguration_LocalType;
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioTrackConfiguration_LocalType > EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr;
#include "EbuCoreaudioFormatType_sampleType_LocalType.h"
#include "EbuCoreaudioFormatType_bitRateMode_LocalType.h"
class IEbuCoreaudioFormatType_audioTrack_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatType_audioTrack_CollectionType > EbuCoreaudioFormatType_audioTrack_CollectionPtr;
class IEbuCoretechnicalAttributes;
typedef Dc1Ptr< IEbuCoretechnicalAttributes > EbuCoretechnicalAttributesPtr;
class IEbuCoreaudioFormatType_comment_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioFormatType_comment_CollectionType > EbuCoreaudioFormatType_comment_CollectionPtr;

/** 
 * Generated interface IEbuCoreaudioFormatType for class EbuCoreaudioFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3096<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioFormatType();
	virtual ~IEbuCoreaudioFormatType();
	/**
	 * Get audioFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatId() const = 0;

	/**
	 * Get audioFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatId() const = 0;

	/**
	 * Set audioFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatId() = 0;

	/**
	 * Get audioFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatVersionId() const = 0;

	/**
	 * Get audioFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatVersionId() const = 0;

	/**
	 * Set audioFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatVersionId() = 0;

	/**
	 * Get audioFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatName() const = 0;

	/**
	 * Get audioFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatName() const = 0;

	/**
	 * Set audioFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatName() = 0;

	/**
	 * Get audioFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatDefinition() const = 0;

	/**
	 * Get audioFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatDefinition() const = 0;

	/**
	 * Set audioFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatDefinition() = 0;

	/**
	 * Get audioPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetaudioPresenceFlag() const = 0;

	/**
	 * Get audioPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioPresenceFlag() const = 0;

	/**
	 * Set audioPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetaudioPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioPresenceFlag() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get audioEncoding element.
	 * @return EbuCoreaudioFormatType_audioEncoding_LocalPtr
	 */
	virtual EbuCoreaudioFormatType_audioEncoding_LocalPtr GetaudioEncoding() const = 0;

	virtual bool IsValidaudioEncoding() const = 0;

	/**
	 * Set audioEncoding element.
	 * @param item const EbuCoreaudioFormatType_audioEncoding_LocalPtr &
	 */
	virtual void SetaudioEncoding(const EbuCoreaudioFormatType_audioEncoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioEncoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioEncoding() = 0;

	/**
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const = 0;

	virtual bool IsValidcodec() const = 0;

	/**
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec() = 0;

	/**
	 * Get audioTrackConfiguration element.
	 * @return EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr
	 */
	virtual EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr GetaudioTrackConfiguration() const = 0;

	virtual bool IsValidaudioTrackConfiguration() const = 0;

	/**
	 * Set audioTrackConfiguration element.
	 * @param item const EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr &
	 */
	virtual void SetaudioTrackConfiguration(const EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioTrackConfiguration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioTrackConfiguration() = 0;

	/**
	 * Get samplingRate element.
	 * @return int
	 */
	virtual int GetsamplingRate() const = 0;

	virtual bool IsValidsamplingRate() const = 0;

	/**
	 * Set samplingRate element.
	 * @param item int
	 */
	virtual void SetsamplingRate(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate samplingRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesamplingRate() = 0;

	/**
	 * Get sampleSize element.
	 * @return unsigned
	 */
	virtual unsigned GetsampleSize() const = 0;

	virtual bool IsValidsampleSize() const = 0;

	/**
	 * Set sampleSize element.
	 * @param item unsigned
	 */
	virtual void SetsampleSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sampleSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleSize() = 0;

	/**
	 * Get sampleType element.
	 * @return EbuCoreaudioFormatType_sampleType_LocalType::Enumeration
	 */
	virtual EbuCoreaudioFormatType_sampleType_LocalType::Enumeration GetsampleType() const = 0;

	virtual bool IsValidsampleType() const = 0;

	/**
	 * Set sampleType element.
	 * @param item EbuCoreaudioFormatType_sampleType_LocalType::Enumeration
	 */
	virtual void SetsampleType(EbuCoreaudioFormatType_sampleType_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sampleType element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleType() = 0;

	/**
	 * Get bitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRate() const = 0;

	virtual bool IsValidbitRate() const = 0;

	/**
	 * Set bitRate element.
	 * @param item unsigned
	 */
	virtual void SetbitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRate() = 0;

	/**
	 * Get bitRateMax element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRateMax() const = 0;

	virtual bool IsValidbitRateMax() const = 0;

	/**
	 * Set bitRateMax element.
	 * @param item unsigned
	 */
	virtual void SetbitRateMax(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRateMax element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMax() = 0;

	/**
	 * Get bitRateMode element.
	 * @return EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration GetbitRateMode() const = 0;

	virtual bool IsValidbitRateMode() const = 0;

	/**
	 * Set bitRateMode element.
	 * @param item EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual void SetbitRateMode(EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitRateMode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMode() = 0;

	/**
	 * Get audioTrack element.
	 * @return EbuCoreaudioFormatType_audioTrack_CollectionPtr
	 */
	virtual EbuCoreaudioFormatType_audioTrack_CollectionPtr GetaudioTrack() const = 0;

	/**
	 * Set audioTrack element.
	 * @param item const EbuCoreaudioFormatType_audioTrack_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrack(const EbuCoreaudioFormatType_audioTrack_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get channels element.
	 * @return unsigned
	 */
	virtual unsigned Getchannels() const = 0;

	virtual bool IsValidchannels() const = 0;

	/**
	 * Set channels element.
	 * @param item unsigned
	 */
	virtual void Setchannels(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate channels element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels() = 0;

	/**
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const = 0;

	/**
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get comment element.
	 * @return EbuCoreaudioFormatType_comment_CollectionPtr
	 */
	virtual EbuCoreaudioFormatType_comment_CollectionPtr Getcomment() const = 0;

	/**
	 * Set comment element.
	 * @param item const EbuCoreaudioFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCoreaudioFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioFormatType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>audioEncoding</li>
	 * <li>codec</li>
	 * <li>audioTrackConfiguration</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrack</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3096<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 3096.<br>
 */
class DC1_EXPORT EbuCoreaudioFormatType :
		public IEbuCoreaudioFormatType
{
	friend class EbuCoreaudioFormatTypeFactory; // constructs objects

public:
	/*
	 * Get audioFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatId() const;

	/*
	 * Get audioFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatId() const;

	/*
	 * Set audioFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatId();

	/*
	 * Get audioFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatVersionId() const;

	/*
	 * Get audioFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatVersionId() const;

	/*
	 * Set audioFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatVersionId();

	/*
	 * Get audioFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatName() const;

	/*
	 * Get audioFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatName() const;

	/*
	 * Set audioFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatName();

	/*
	 * Get audioFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioFormatDefinition() const;

	/*
	 * Get audioFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioFormatDefinition() const;

	/*
	 * Set audioFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioFormatDefinition();

	/*
	 * Get audioPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetaudioPresenceFlag() const;

	/*
	 * Get audioPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioPresenceFlag() const;

	/*
	 * Set audioPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetaudioPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioPresenceFlag();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get audioEncoding element.
	 * @return EbuCoreaudioFormatType_audioEncoding_LocalPtr
	 */
	virtual EbuCoreaudioFormatType_audioEncoding_LocalPtr GetaudioEncoding() const;

	virtual bool IsValidaudioEncoding() const;

	/*
	 * Set audioEncoding element.
	 * @param item const EbuCoreaudioFormatType_audioEncoding_LocalPtr &
	 */
	virtual void SetaudioEncoding(const EbuCoreaudioFormatType_audioEncoding_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioEncoding element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioEncoding();

	/*
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const;

	virtual bool IsValidcodec() const;

	/*
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec();

	/*
	 * Get audioTrackConfiguration element.
	 * @return EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr
	 */
	virtual EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr GetaudioTrackConfiguration() const;

	virtual bool IsValidaudioTrackConfiguration() const;

	/*
	 * Set audioTrackConfiguration element.
	 * @param item const EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr &
	 */
	virtual void SetaudioTrackConfiguration(const EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioTrackConfiguration element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioTrackConfiguration();

	/*
	 * Get samplingRate element.
	 * @return int
	 */
	virtual int GetsamplingRate() const;

	virtual bool IsValidsamplingRate() const;

	/*
	 * Set samplingRate element.
	 * @param item int
	 */
	virtual void SetsamplingRate(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate samplingRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesamplingRate();

	/*
	 * Get sampleSize element.
	 * @return unsigned
	 */
	virtual unsigned GetsampleSize() const;

	virtual bool IsValidsampleSize() const;

	/*
	 * Set sampleSize element.
	 * @param item unsigned
	 */
	virtual void SetsampleSize(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sampleSize element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleSize();

	/*
	 * Get sampleType element.
	 * @return EbuCoreaudioFormatType_sampleType_LocalType::Enumeration
	 */
	virtual EbuCoreaudioFormatType_sampleType_LocalType::Enumeration GetsampleType() const;

	virtual bool IsValidsampleType() const;

	/*
	 * Set sampleType element.
	 * @param item EbuCoreaudioFormatType_sampleType_LocalType::Enumeration
	 */
	virtual void SetsampleType(EbuCoreaudioFormatType_sampleType_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sampleType element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleType();

	/*
	 * Get bitRate element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRate() const;

	virtual bool IsValidbitRate() const;

	/*
	 * Set bitRate element.
	 * @param item unsigned
	 */
	virtual void SetbitRate(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRate();

	/*
	 * Get bitRateMax element.
	 * @return unsigned
	 */
	virtual unsigned GetbitRateMax() const;

	virtual bool IsValidbitRateMax() const;

	/*
	 * Set bitRateMax element.
	 * @param item unsigned
	 */
	virtual void SetbitRateMax(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRateMax element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMax();

	/*
	 * Get bitRateMode element.
	 * @return EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration GetbitRateMode() const;

	virtual bool IsValidbitRateMode() const;

	/*
	 * Set bitRateMode element.
	 * @param item EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration
	 */
	virtual void SetbitRateMode(EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitRateMode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitRateMode();

	/*
	 * Get audioTrack element.
	 * @return EbuCoreaudioFormatType_audioTrack_CollectionPtr
	 */
	virtual EbuCoreaudioFormatType_audioTrack_CollectionPtr GetaudioTrack() const;

	/*
	 * Set audioTrack element.
	 * @param item const EbuCoreaudioFormatType_audioTrack_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrack(const EbuCoreaudioFormatType_audioTrack_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get channels element.
	 * @return unsigned
	 */
	virtual unsigned Getchannels() const;

	virtual bool IsValidchannels() const;

	/*
	 * Set channels element.
	 * @param item unsigned
	 */
	virtual void Setchannels(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate channels element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatechannels();

	/*
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const;

	/*
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get comment element.
	 * @return EbuCoreaudioFormatType_comment_CollectionPtr
	 */
	virtual EbuCoreaudioFormatType_comment_CollectionPtr Getcomment() const;

	/*
	 * Set comment element.
	 * @param item const EbuCoreaudioFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCoreaudioFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioFormatType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>audioEncoding</li>
	 * <li>codec</li>
	 * <li>audioTrackConfiguration</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrack</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioFormatType();

protected:
	EbuCoreaudioFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_audioFormatId;
	bool m_audioFormatId_Exist;
	XMLCh * m_audioFormatVersionId;
	bool m_audioFormatVersionId_Exist;
	XMLCh * m_audioFormatName;
	bool m_audioFormatName_Exist;
	XMLCh * m_audioFormatDefinition;
	bool m_audioFormatDefinition_Exist;
	bool m_audioPresenceFlag;
	bool m_audioPresenceFlag_Exist;


	EbuCoreaudioFormatType_audioEncoding_LocalPtr m_audioEncoding;
	bool m_audioEncoding_Exist; // For optional elements 
	EbuCorecodecPtr m_codec;
	bool m_codec_Exist; // For optional elements 
	EbuCoreaudioFormatType_audioTrackConfiguration_LocalPtr m_audioTrackConfiguration;
	bool m_audioTrackConfiguration_Exist; // For optional elements 
	int m_samplingRate;
	bool m_samplingRate_Exist; // For optional elements 
	unsigned m_sampleSize;
	bool m_sampleSize_Exist; // For optional elements 
	EbuCoreaudioFormatType_sampleType_LocalType::Enumeration m_sampleType;
	bool m_sampleType_Exist; // For optional elements 
	unsigned m_bitRate;
	bool m_bitRate_Exist; // For optional elements 
	unsigned m_bitRateMax;
	bool m_bitRateMax_Exist; // For optional elements 
	EbuCoreaudioFormatType_bitRateMode_LocalType::Enumeration m_bitRateMode;
	bool m_bitRateMode_Exist; // For optional elements 
	EbuCoreaudioFormatType_audioTrack_CollectionPtr m_audioTrack;
	unsigned m_channels;
	bool m_channels_Exist; // For optional elements 
	EbuCoretechnicalAttributesPtr m_technicalAttributes;
	EbuCoreaudioFormatType_comment_CollectionPtr m_comment;

// no includefile for extension defined 
// file EbuCoreaudioFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _141EBUCOREAUDIOFORMATTYPE_H

