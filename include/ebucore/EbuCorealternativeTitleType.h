/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _93EBUCOREALTERNATIVETITLETYPE_H
#define _93EBUCOREALTERNATIVETITLETYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCorealternativeTitleTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtInclude.h


class IEbuCorealternativeTitleType;
typedef Dc1Ptr< IEbuCorealternativeTitleType> EbuCorealternativeTitlePtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IW3Ctime;
typedef Dc1Ptr< IW3Ctime > W3CtimePtr;
class IEbuCorealternativeTitleType_title_CollectionType;
typedef Dc1Ptr< IEbuCorealternativeTitleType_title_CollectionType > EbuCorealternativeTitleType_title_CollectionPtr;

/** 
 * Generated interface IEbuCorealternativeTitleType for class EbuCorealternativeTitleType<br>
 * Located at: EBU_CORE_20140318.xsd, line 720<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorealternativeTitleType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorealternativeTitleType();
	virtual ~IEbuCorealternativeTitleType();
	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get statusLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLabel() const = 0;

	/**
	 * Get statusLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLabel() const = 0;

	/**
	 * Set statusLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate statusLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLabel() = 0;

	/**
	 * Get statusDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusDefinition() const = 0;

	/**
	 * Get statusDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusDefinition() const = 0;

	/**
	 * Set statusDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate statusDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusDefinition() = 0;

	/**
	 * Get statusLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLink() const = 0;

	/**
	 * Get statusLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLink() const = 0;

	/**
	 * Set statusLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate statusLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLink() = 0;

	/**
	 * Get statusSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusSource() const = 0;

	/**
	 * Get statusSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusSource() const = 0;

	/**
	 * Set statusSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate statusSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusSource() = 0;

	/**
	 * Get statusLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLanguage() const = 0;

	/**
	 * Get statusLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLanguage() const = 0;

	/**
	 * Set statusLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate statusLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLanguage() = 0;

	/**
	 * Get startYear attribute.
	 * @return int
	 */
	virtual int GetstartYear() const = 0;

	/**
	 * Get startYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartYear() const = 0;

	/**
	 * Set startYear attribute.
	 * @param item int
	 */
	virtual void SetstartYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartYear() = 0;

	/**
	 * Get startDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetstartDate() const = 0;

	/**
	 * Get startDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartDate() const = 0;

	/**
	 * Set startDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetstartDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartDate() = 0;

	/**
	 * Get startTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetstartTime() const = 0;

	/**
	 * Get startTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartTime() const = 0;

	/**
	 * Set startTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetstartTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartTime() = 0;

	/**
	 * Get endYear attribute.
	 * @return int
	 */
	virtual int GetendYear() const = 0;

	/**
	 * Get endYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendYear() const = 0;

	/**
	 * Set endYear attribute.
	 * @param item int
	 */
	virtual void SetendYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendYear() = 0;

	/**
	 * Get endDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetendDate() const = 0;

	/**
	 * Get endDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendDate() const = 0;

	/**
	 * Set endDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetendDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendDate() = 0;

	/**
	 * Get endTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetendTime() const = 0;

	/**
	 * Get endTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendTime() const = 0;

	/**
	 * Set endTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetendTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendTime() = 0;

	/**
	 * Get period attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getperiod() const = 0;

	/**
	 * Get period attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existperiod() const = 0;

	/**
	 * Set period attribute.
	 * @param item XMLCh *
	 */
	virtual void Setperiod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate period attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateperiod() = 0;

	/**
	 * Get length attribute.
	 * @return unsigned
	 */
	virtual unsigned Getlength() const = 0;

	/**
	 * Get length attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const = 0;

	/**
	 * Set length attribute.
	 * @param item unsigned
	 */
	virtual void Setlength(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate length attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength() = 0;

	/**
	 * Get geographicalScope attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetgeographicalScope() const = 0;

	/**
	 * Get geographicalScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistgeographicalScope() const = 0;

	/**
	 * Set geographicalScope attribute.
	 * @param item XMLCh *
	 */
	virtual void SetgeographicalScope(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate geographicalScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategeographicalScope() = 0;

	/**
	 * Get geographicalExclusionScope attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetgeographicalExclusionScope() const = 0;

	/**
	 * Get geographicalExclusionScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistgeographicalExclusionScope() const = 0;

	/**
	 * Set geographicalExclusionScope attribute.
	 * @param item XMLCh *
	 */
	virtual void SetgeographicalExclusionScope(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate geographicalExclusionScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategeographicalExclusionScope() = 0;

	/**
	 * Get note attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const = 0;

	/**
	 * Get note attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const = 0;

	/**
	 * Set note attribute.
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate note attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get title element.
	 * @return EbuCorealternativeTitleType_title_CollectionPtr
	 */
	virtual EbuCorealternativeTitleType_title_CollectionPtr Gettitle() const = 0;

	/**
	 * Set title element.
	 * @param item const EbuCorealternativeTitleType_title_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settitle(const EbuCorealternativeTitleType_title_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of alternativeTitleType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>http://purl.org/dc/elements/1.1/:title</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtPropDef.h

};




/*
 * Generated class EbuCorealternativeTitleType<br>
 * Located at: EBU_CORE_20140318.xsd, line 720<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 720.<br>
 */
class DC1_EXPORT EbuCorealternativeTitleType :
		public IEbuCorealternativeTitleType
{
	friend class EbuCorealternativeTitleTypeFactory; // constructs objects

public:
	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get statusLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLabel() const;

	/*
	 * Get statusLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLabel() const;

	/*
	 * Set statusLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate statusLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLabel();

	/*
	 * Get statusDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusDefinition() const;

	/*
	 * Get statusDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusDefinition() const;

	/*
	 * Set statusDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate statusDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusDefinition();

	/*
	 * Get statusLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLink() const;

	/*
	 * Get statusLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLink() const;

	/*
	 * Set statusLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate statusLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLink();

	/*
	 * Get statusSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusSource() const;

	/*
	 * Get statusSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusSource() const;

	/*
	 * Set statusSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate statusSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusSource();

	/*
	 * Get statusLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetstatusLanguage() const;

	/*
	 * Get statusLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststatusLanguage() const;

	/*
	 * Set statusLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetstatusLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate statusLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestatusLanguage();

	/*
	 * Get startYear attribute.
	 * @return int
	 */
	virtual int GetstartYear() const;

	/*
	 * Get startYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartYear() const;

	/*
	 * Set startYear attribute.
	 * @param item int
	 */
	virtual void SetstartYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartYear();

	/*
	 * Get startDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetstartDate() const;

	/*
	 * Get startDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartDate() const;

	/*
	 * Set startDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetstartDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartDate();

	/*
	 * Get startTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetstartTime() const;

	/*
	 * Get startTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartTime() const;

	/*
	 * Set startTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetstartTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartTime();

	/*
	 * Get endYear attribute.
	 * @return int
	 */
	virtual int GetendYear() const;

	/*
	 * Get endYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendYear() const;

	/*
	 * Set endYear attribute.
	 * @param item int
	 */
	virtual void SetendYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendYear();

	/*
	 * Get endDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetendDate() const;

	/*
	 * Get endDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendDate() const;

	/*
	 * Set endDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetendDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendDate();

	/*
	 * Get endTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetendTime() const;

	/*
	 * Get endTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendTime() const;

	/*
	 * Set endTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetendTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendTime();

	/*
	 * Get period attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getperiod() const;

	/*
	 * Get period attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existperiod() const;

	/*
	 * Set period attribute.
	 * @param item XMLCh *
	 */
	virtual void Setperiod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate period attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateperiod();

	/*
	 * Get length attribute.
	 * @return unsigned
	 */
	virtual unsigned Getlength() const;

	/*
	 * Get length attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlength() const;

	/*
	 * Set length attribute.
	 * @param item unsigned
	 */
	virtual void Setlength(unsigned item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate length attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelength();

	/*
	 * Get geographicalScope attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetgeographicalScope() const;

	/*
	 * Get geographicalScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistgeographicalScope() const;

	/*
	 * Set geographicalScope attribute.
	 * @param item XMLCh *
	 */
	virtual void SetgeographicalScope(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate geographicalScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategeographicalScope();

	/*
	 * Get geographicalExclusionScope attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetgeographicalExclusionScope() const;

	/*
	 * Get geographicalExclusionScope attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistgeographicalExclusionScope() const;

	/*
	 * Set geographicalExclusionScope attribute.
	 * @param item XMLCh *
	 */
	virtual void SetgeographicalExclusionScope(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate geographicalExclusionScope attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidategeographicalExclusionScope();

	/*
	 * Get note attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const;

	/*
	 * Get note attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const;

	/*
	 * Set note attribute.
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate note attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get title element.
	 * @return EbuCorealternativeTitleType_title_CollectionPtr
	 */
	virtual EbuCorealternativeTitleType_title_CollectionPtr Gettitle() const;

	/*
	 * Set title element.
	 * @param item const EbuCorealternativeTitleType_title_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Settitle(const EbuCorealternativeTitleType_title_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of alternativeTitleType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>http://purl.org/dc/elements/1.1/:title</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMyMethodDef.h


public:

	virtual ~EbuCorealternativeTitleType();

protected:
	EbuCorealternativeTitleType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;
	XMLCh * m_statusLabel;
	bool m_statusLabel_Exist;
	XMLCh * m_statusDefinition;
	bool m_statusDefinition_Exist;
	XMLCh * m_statusLink;
	bool m_statusLink_Exist;
	XMLCh * m_statusSource;
	bool m_statusSource_Exist;
	XMLCh * m_statusLanguage;
	bool m_statusLanguage_Exist;
	int m_startYear;
	bool m_startYear_Exist;
	W3CdatePtr m_startDate;
	bool m_startDate_Exist;
	W3CtimePtr m_startTime;
	bool m_startTime_Exist;
	int m_endYear;
	bool m_endYear_Exist;
	W3CdatePtr m_endDate;
	bool m_endDate_Exist;
	W3CtimePtr m_endTime;
	bool m_endTime_Exist;
	XMLCh * m_period;
	bool m_period_Exist;
	unsigned m_length;
	bool m_length_Exist;
	XMLCh * m_geographicalScope;
	bool m_geographicalScope_Exist;
	XMLCh * m_geographicalExclusionScope;
	bool m_geographicalExclusionScope_Exist;
	XMLCh * m_note;
	bool m_note_Exist;


	EbuCorealternativeTitleType_title_CollectionPtr m_title;

// no includefile for extension defined 
// file EbuCorealternativeTitleType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _93EBUCOREALTERNATIVETITLETYPE_H

