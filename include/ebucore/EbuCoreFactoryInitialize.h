/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Initialize section of Dc1Factory.h
// included in Dc1Factory::Initialize()

{
	// Register extension namespace uri and its prefix(es), so that we can resolve it without Xerces XML DOM
	Dc1Factory::SetNamespaceUri(X("ebucore"), X("urn:ebu:metadata-schema:ebuCore_2014"));

	Dc1Factory * factory;
	
	factory = new EbuCoreaddressTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaddressType_addressLine_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaddressType_country_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorealternativeDateTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorealternativeTitleTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorealternativeTitleType_title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreancillaryDataFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreancillaryDataFormatType_lineNumber_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaspectRatioTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioBlockFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioBlockFormatType_position_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioBlockFormatType_speakerLabel_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioBlockFormatType_speakerLabel_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioChannelFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioChannelFormatType_frequency_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioContentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioContentType_audioObjectIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioContent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioObject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioProgramme_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatType_audioEncoding_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatType_audioTrack_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatType_audioTrack_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatType_audioTrackConfiguration_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioMXFLookUpTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioObjectTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioObjectType_audioObjectIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioObjectType_audioTrackUIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioPackFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioProgrammeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioProgrammeType_audioContentIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioStreamFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioTrackFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreaudioTrackUIDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreBooleanFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecaptioningFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecodecTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoefficientTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecomment_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecompoundNameTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_details_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_name_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_nickname_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_otherGivenName_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_relatedContacts_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_relatedInformationLink_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_relatedInformationLink_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_skill_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_stageName_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontactDetailsType_username_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontainerFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecontainerFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_alternativeTitle_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_contributor_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_coverage_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_creator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_date_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_description_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_format_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasEpisode_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasMember_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasPart_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasSeason_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasTrackPart_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasTrackPart_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_hasVersion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_identifier_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isEpisodeOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isFormatOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isMemberOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isNextInSequence_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isPartOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isReferencedBy_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isRelatedTo_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isReplacedBy_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isRequiredBy_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isSeasonOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_isVersionOf_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_language_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_part_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_planning_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_publicationHistory_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_publisher_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_rating_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_references_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_relation_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_replaces_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_requires_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_rights_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_sameAs_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_source_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_subject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_type_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoreMetadataType_version_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorecoverageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredataFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredataFormatType_ancillaryDataFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredataFormatType_captioningFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredataFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredataFormatType_subtitlingFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_alternative_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_copyrighted_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_created_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_date_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_digitised_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_issued_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_modified_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredateType_released_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredescriptionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredescriptionType_description_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredetailsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredetailsType_emailAddress_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredimensionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredocumentFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredocumentFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredurationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoredurationType_duration_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreebuCoreMainTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreeditUnitNumberTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreelementTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreentityTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreentityType_contactDetails_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreentityType_organisationDetails_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreentityType_role_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreentityType_role_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefileInfoFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefileInfo_locator_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefileInfo_locator_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefileInfo_mimeType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefileInfo_mimeType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreFloatFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_audioFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_audioFormatExtended_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_containerFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_dataFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_dateCreated_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_dateModified_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_imageFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_medium_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_medium_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreformatType_videoFormat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorefrequency_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorehashTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorehashType_hashFunction_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreidentifierTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreimageFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreimageFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreimageFormatType_imageEncoding_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreInt16Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreInt32Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreInt64Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreInt8Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorelanguageTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorelengthTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorelocationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorelocationType_coordinates_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreloudnessMetadataTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorematrixTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorematrixType_coefficient_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDepartmentTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_contacts_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_details_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_organisationCode_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_organisationName_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_relatedInformationLink_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreorganisationDetailsType_relatedInformationLink_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorePackageIDTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepartTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreperiodOfTimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreplanningTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreplanningType_publicationEvent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreposition_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationChannelTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationEventTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationEventType_publicationRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationHistoryTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationHistoryType_publicationEvent_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationMediumTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorepublicationServiceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingExclusionRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingLink_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingScaleMaxValue_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingScaleMinValue_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreratingType_ratingValue_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerationalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreregionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreregionType_country_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreregionType_countryRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreregionType_countryRegion_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerelationTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_contactDetails_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_copyrightStatement_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_disclaimer_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_exploitationIssues_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_rights_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_rightsAttributedId_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorerightsType_rightsHolder_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoresigningFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorespatialTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorespatialType_location_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreStringFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoresubjectTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoresubjectType_subject_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoresubjectType_subjectDefinition_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoresubtitlingFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretargetAudienceTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretargetAudienceType_targetExclusionRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretargetAudienceType_targetRegion_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributeRationalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributesFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeByte_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeLong_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeRational_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeShort_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeString_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributes_technicalAttributeUri_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretechnicalAttributeUriTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretemporalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretemporalType_PeriodOfTime_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretimecodeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretimeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretimeType_time_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretitleTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretitleType_title_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_genre_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_genre_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_objectType_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_objectType_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_targetAudience_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoretypeType_type_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreUInt16Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreUInt32Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreUInt64Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreUInt8Factory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCoreversionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_aspectRatio_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_comment_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_height_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_height_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_videoEncoding_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_videoTrack_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_videoTrack_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_width_CollectionTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

	factory = new EbuCorevideoFormatType_width_LocalTypeFactory; // Create and install factory
	factory->SetToDelete(true); // We are owner - responsible for destruction

}
