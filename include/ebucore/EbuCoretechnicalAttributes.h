/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _561EBUCORETECHNICALATTRIBUTES_H
#define _561EBUCORETECHNICALATTRIBUTES_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoretechnicalAttributesFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtInclude.h


class IEbuCoretechnicalAttributes;
typedef Dc1Ptr< IEbuCoretechnicalAttributes> EbuCoretechnicalAttributesPtr;
class IEbuCoretechnicalAttributes_technicalAttributeString_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeString_CollectionType > EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeByte_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeByte_CollectionType > EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeShort_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeShort_CollectionType > EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType > EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeLong_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeLong_CollectionType > EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType > EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType > EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeRational_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeRational_CollectionType > EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr;
class IEbuCoretechnicalAttributes_technicalAttributeUri_CollectionType;
typedef Dc1Ptr< IEbuCoretechnicalAttributes_technicalAttributeUri_CollectionType > EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr;

/** 
 * Generated interface IEbuCoretechnicalAttributes for class EbuCoretechnicalAttributes<br>
 * Located at: EBU_CORE_20140318.xsd, line 4570<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoretechnicalAttributes 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoretechnicalAttributes();
	virtual ~IEbuCoretechnicalAttributes();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get technicalAttributeString element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr GettechnicalAttributeString() const = 0;

	/**
	 * Set technicalAttributeString element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeString(const EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeByte element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr GettechnicalAttributeByte() const = 0;

	/**
	 * Set technicalAttributeByte element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeByte(const EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeShort element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr GettechnicalAttributeShort() const = 0;

	/**
	 * Set technicalAttributeShort element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeShort(const EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeInteger element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr GettechnicalAttributeInteger() const = 0;

	/**
	 * Set technicalAttributeInteger element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeInteger(const EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeLong element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr GettechnicalAttributeLong() const = 0;

	/**
	 * Set technicalAttributeLong element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeLong(const EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeUnsignedByte element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr GettechnicalAttributeUnsignedByte() const = 0;

	/**
	 * Set technicalAttributeUnsignedByte element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedByte(const EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeUnsignedShort element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr GettechnicalAttributeUnsignedShort() const = 0;

	/**
	 * Set technicalAttributeUnsignedShort element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedShort(const EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeUnsignedInteger element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr GettechnicalAttributeUnsignedInteger() const = 0;

	/**
	 * Set technicalAttributeUnsignedInteger element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedInteger(const EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeUnsignedLong element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr GettechnicalAttributeUnsignedLong() const = 0;

	/**
	 * Set technicalAttributeUnsignedLong element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedLong(const EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeBoolean element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr GettechnicalAttributeBoolean() const = 0;

	/**
	 * Set technicalAttributeBoolean element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeBoolean(const EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeFloat element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr GettechnicalAttributeFloat() const = 0;

	/**
	 * Set technicalAttributeFloat element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeFloat(const EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeRational element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr GettechnicalAttributeRational() const = 0;

	/**
	 * Set technicalAttributeRational element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeRational(const EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get technicalAttributeUri element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr GettechnicalAttributeUri() const = 0;

	/**
	 * Set technicalAttributeUri element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUri(const EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of technicalAttributes.
	 * Currently this type contains 13 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeString</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeByte</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeShort</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeInteger</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeLong</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedByte</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedShort</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedInteger</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedLong</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeBoolean</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeFloat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRational</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUri</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtPropDef.h

};




/*
 * Generated class EbuCoretechnicalAttributes<br>
 * Located at: EBU_CORE_20140318.xsd, line 4570<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4570.<br>
 */
class DC1_EXPORT EbuCoretechnicalAttributes :
		public IEbuCoretechnicalAttributes
{
	friend class EbuCoretechnicalAttributesFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get technicalAttributeString element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr GettechnicalAttributeString() const;

	/*
	 * Set technicalAttributeString element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeString(const EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeByte element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr GettechnicalAttributeByte() const;

	/*
	 * Set technicalAttributeByte element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeByte(const EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeShort element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr GettechnicalAttributeShort() const;

	/*
	 * Set technicalAttributeShort element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeShort(const EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeInteger element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr GettechnicalAttributeInteger() const;

	/*
	 * Set technicalAttributeInteger element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeInteger(const EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeLong element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr GettechnicalAttributeLong() const;

	/*
	 * Set technicalAttributeLong element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeLong(const EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeUnsignedByte element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr GettechnicalAttributeUnsignedByte() const;

	/*
	 * Set technicalAttributeUnsignedByte element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedByte(const EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeUnsignedShort element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr GettechnicalAttributeUnsignedShort() const;

	/*
	 * Set technicalAttributeUnsignedShort element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedShort(const EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeUnsignedInteger element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr GettechnicalAttributeUnsignedInteger() const;

	/*
	 * Set technicalAttributeUnsignedInteger element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedInteger(const EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeUnsignedLong element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr GettechnicalAttributeUnsignedLong() const;

	/*
	 * Set technicalAttributeUnsignedLong element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUnsignedLong(const EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeBoolean element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr GettechnicalAttributeBoolean() const;

	/*
	 * Set technicalAttributeBoolean element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeBoolean(const EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeFloat element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr GettechnicalAttributeFloat() const;

	/*
	 * Set technicalAttributeFloat element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeFloat(const EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeRational element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr GettechnicalAttributeRational() const;

	/*
	 * Set technicalAttributeRational element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeRational(const EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get technicalAttributeUri element.
	 * @return EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr
	 */
	virtual EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr GettechnicalAttributeUri() const;

	/*
	 * Set technicalAttributeUri element.
	 * @param item const EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SettechnicalAttributeUri(const EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of technicalAttributes.
	 * Currently this type contains 13 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeString</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeByte</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeShort</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeInteger</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeLong</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedByte</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedShort</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedInteger</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUnsignedLong</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeBoolean</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeFloat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeRational</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:technicalAttributeUri</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMyMethodDef.h


public:

	virtual ~EbuCoretechnicalAttributes();

protected:
	EbuCoretechnicalAttributes();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	EbuCoretechnicalAttributes_technicalAttributeString_CollectionPtr m_technicalAttributeString;
	EbuCoretechnicalAttributes_technicalAttributeByte_CollectionPtr m_technicalAttributeByte;
	EbuCoretechnicalAttributes_technicalAttributeShort_CollectionPtr m_technicalAttributeShort;
	EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionPtr m_technicalAttributeInteger;
	EbuCoretechnicalAttributes_technicalAttributeLong_CollectionPtr m_technicalAttributeLong;
	EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionPtr m_technicalAttributeUnsignedByte;
	EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionPtr m_technicalAttributeUnsignedShort;
	EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionPtr m_technicalAttributeUnsignedInteger;
	EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionPtr m_technicalAttributeUnsignedLong;
	EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionPtr m_technicalAttributeBoolean;
	EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionPtr m_technicalAttributeFloat;
	EbuCoretechnicalAttributes_technicalAttributeRational_CollectionPtr m_technicalAttributeRational;
	EbuCoretechnicalAttributes_technicalAttributeUri_CollectionPtr m_technicalAttributeUri;

// no includefile for extension defined 
// file EbuCoretechnicalAttributes_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _561EBUCORETECHNICALATTRIBUTES_H

