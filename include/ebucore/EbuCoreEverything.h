/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef EBUCOREEVERYTHING_INCLUDED_H
#define EBUCOREEVERYTHING_INCLUDED_H

#include "Dc1Defines.h"

#include "EbuCoreaddressType.h"
#include "EbuCoreaddressType_addressLine_CollectionType.h"
#include "EbuCoreaddressType_country_LocalType.h"
#include "EbuCorealternativeDateType.h"
#include "EbuCorealternativeTitleType.h"
#include "EbuCorealternativeTitleType_title_CollectionType.h"
#include "EbuCoreancillaryDataFormatType.h"
#include "EbuCoreancillaryDataFormatType_lineNumber_CollectionType.h"
#include "EbuCoreaspectRatioType.h"
#include "EbuCoreaudioBlockFormatType.h"
#include "EbuCoreaudioBlockFormatType_position_CollectionType.h"
#include "EbuCoreaudioBlockFormatType_speakerLabel_CollectionType.h"
#include "EbuCoreaudioBlockFormatType_speakerLabel_LocalType.h"
#include "EbuCoreaudioChannelFormatType.h"
#include "EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType.h"
#include "EbuCoreaudioChannelFormatType_frequency_CollectionType.h"
#include "EbuCoreaudioContentType.h"
#include "EbuCoreaudioContentType_audioObjectIDRef_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType.h"
#include "EbuCoreaudioFormatExtendedType_audioBlockFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioChannelFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioContent_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioObject_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioPackFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioProgramme_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioStreamFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioTrackFormat_CollectionType.h"
#include "EbuCoreaudioFormatExtendedType_audioTrackUID_CollectionType.h"
#include "EbuCoreaudioFormatType.h"
#include "EbuCoreaudioFormatType_audioEncoding_LocalType.h"
#include "EbuCoreaudioFormatType_audioTrack_CollectionType.h"
#include "EbuCoreaudioFormatType_audioTrack_LocalType.h"
#include "EbuCoreaudioFormatType_audioTrackConfiguration_LocalType.h"
#include "EbuCoreaudioFormatType_bitRateMode_LocalType.h"
#include "EbuCoreaudioFormatType_comment_CollectionType.h"
#include "EbuCoreaudioFormatType_sampleType_LocalType.h"
#include "EbuCoreaudioMXFLookUpType.h"
#include "EbuCoreaudioObjectType.h"
#include "EbuCoreaudioObjectType_audioObjectIDRef_CollectionType.h"
#include "EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioObjectType_audioTrackUIDRef_CollectionType.h"
#include "EbuCoreaudioPackFormatType.h"
#include "EbuCoreaudioPackFormatType_audioChannelFormatIDRef_CollectionType.h"
#include "EbuCoreaudioPackFormatType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioProgrammeType.h"
#include "EbuCoreaudioProgrammeType_audioContentIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType.h"
#include "EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType.h"
#include "EbuCoreaudioTrackFormatType.h"
#include "EbuCoreaudioTrackUIDType.h"
#include "EbuCoreBoolean.h"
#include "EbuCorecaptioningFormatType.h"
#include "EbuCorecodecType.h"
#include "EbuCorecoefficientType.h"
#include "EbuCorecomment_LocalType.h"
#include "EbuCorecompoundNameType.h"
#include "EbuCorecontactDetailsType.h"
#include "EbuCorecontactDetailsType_details_CollectionType.h"
#include "EbuCorecontactDetailsType_name_CollectionType.h"
#include "EbuCorecontactDetailsType_nickname_CollectionType.h"
#include "EbuCorecontactDetailsType_otherGivenName_CollectionType.h"
#include "EbuCorecontactDetailsType_relatedContacts_CollectionType.h"
#include "EbuCorecontactDetailsType_relatedInformationLink_CollectionType.h"
#include "EbuCorecontactDetailsType_relatedInformationLink_LocalType.h"
#include "EbuCorecontactDetailsType_skill_CollectionType.h"
#include "EbuCorecontactDetailsType_stageName_CollectionType.h"
#include "EbuCorecontactDetailsType_username_CollectionType.h"
#include "EbuCorecontainerFormatType.h"
#include "EbuCorecontainerFormatType_comment_CollectionType.h"
#include "EbuCorecoreMetadataType.h"
#include "EbuCorecoreMetadataType_alternativeTitle_CollectionType.h"
#include "EbuCorecoreMetadataType_contributor_CollectionType.h"
#include "EbuCorecoreMetadataType_coverage_CollectionType.h"
#include "EbuCorecoreMetadataType_creator_CollectionType.h"
#include "EbuCorecoreMetadataType_date_CollectionType.h"
#include "EbuCorecoreMetadataType_description_CollectionType.h"
#include "EbuCorecoreMetadataType_format_CollectionType.h"
#include "EbuCorecoreMetadataType_hasEpisode_CollectionType.h"
#include "EbuCorecoreMetadataType_hasFormat_CollectionType.h"
#include "EbuCorecoreMetadataType_hasMember_CollectionType.h"
#include "EbuCorecoreMetadataType_hasPart_CollectionType.h"
#include "EbuCorecoreMetadataType_hasSeason_CollectionType.h"
#include "EbuCorecoreMetadataType_hasTrackPart_CollectionType.h"
#include "EbuCorecoreMetadataType_hasTrackPart_LocalType.h"
#include "EbuCorecoreMetadataType_hasVersion_CollectionType.h"
#include "EbuCorecoreMetadataType_identifier_CollectionType.h"
#include "EbuCorecoreMetadataType_isEpisodeOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isFormatOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isMemberOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isNextInSequence_CollectionType.h"
#include "EbuCorecoreMetadataType_isPartOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isReferencedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_isRelatedTo_CollectionType.h"
#include "EbuCorecoreMetadataType_isReplacedBy_CollectionType.h"
#include "EbuCorecoreMetadataType_isRequiredBy_CollectionType.h"
#include "EbuCorecoreMetadataType_isSeasonOf_CollectionType.h"
#include "EbuCorecoreMetadataType_isVersionOf_CollectionType.h"
#include "EbuCorecoreMetadataType_language_CollectionType.h"
#include "EbuCorecoreMetadataType_part_CollectionType.h"
#include "EbuCorecoreMetadataType_planning_CollectionType.h"
#include "EbuCorecoreMetadataType_publicationHistory_CollectionType.h"
#include "EbuCorecoreMetadataType_publisher_CollectionType.h"
#include "EbuCorecoreMetadataType_rating_CollectionType.h"
#include "EbuCorecoreMetadataType_references_CollectionType.h"
#include "EbuCorecoreMetadataType_relation_CollectionType.h"
#include "EbuCorecoreMetadataType_replaces_CollectionType.h"
#include "EbuCorecoreMetadataType_requires_CollectionType.h"
#include "EbuCorecoreMetadataType_rights_CollectionType.h"
#include "EbuCorecoreMetadataType_sameAs_CollectionType.h"
#include "EbuCorecoreMetadataType_source_CollectionType.h"
#include "EbuCorecoreMetadataType_subject_CollectionType.h"
#include "EbuCorecoreMetadataType_title_CollectionType.h"
#include "EbuCorecoreMetadataType_type_CollectionType.h"
#include "EbuCorecoreMetadataType_version_CollectionType.h"
#include "EbuCorecoverageType.h"
#include "EbuCoredataFormatType.h"
#include "EbuCoredataFormatType_ancillaryDataFormat_CollectionType.h"
#include "EbuCoredataFormatType_captioningFormat_CollectionType.h"
#include "EbuCoredataFormatType_comment_CollectionType.h"
#include "EbuCoredataFormatType_subtitlingFormat_CollectionType.h"
#include "EbuCoredateType.h"
#include "EbuCoredateType_alternative_CollectionType.h"
#include "EbuCoredateType_copyrighted_LocalType.h"
#include "EbuCoredateType_created_LocalType.h"
#include "EbuCoredateType_date_CollectionType.h"
#include "EbuCoredateType_digitised_LocalType.h"
#include "EbuCoredateType_issued_LocalType.h"
#include "EbuCoredateType_modified_LocalType.h"
#include "EbuCoredateType_released_LocalType.h"
#include "EbuCoredescriptionType.h"
#include "EbuCoredescriptionType_description_CollectionType.h"
#include "EbuCoredetailsType.h"
#include "EbuCoredetailsType_emailAddress_CollectionType.h"
#include "EbuCoredimensionType.h"
#include "EbuCoredocumentFormatType.h"
#include "EbuCoredocumentFormatType_comment_CollectionType.h"
#include "EbuCoredurationType.h"
#include "EbuCoredurationType_duration_LocalType.h"
#include "EbuCoreebuCoreMainType.h"
#include "EbuCoreeditUnitNumberType.h"
#include "EbuCoreelementType.h"
#include "EbuCoreentityType.h"
#include "EbuCoreentityType_contactDetails_CollectionType.h"
#include "EbuCoreentityType_organisationDetails_CollectionType.h"
#include "EbuCoreentityType_role_CollectionType.h"
#include "EbuCoreentityType_role_LocalType.h"
#include "EbuCorefileInfo.h"
#include "EbuCorefileInfo_locator_CollectionType.h"
#include "EbuCorefileInfo_locator_LocalType.h"
#include "EbuCorefileInfo_mimeType_CollectionType.h"
#include "EbuCorefileInfo_mimeType_LocalType.h"
#include "EbuCoreFloat.h"
#include "EbuCoreformatType.h"
#include "EbuCoreformatType_audioFormat_CollectionType.h"
#include "EbuCoreformatType_audioFormatExtended_CollectionType.h"
#include "EbuCoreformatType_containerFormat_CollectionType.h"
#include "EbuCoreformatType_dataFormat_CollectionType.h"
#include "EbuCoreformatType_dateCreated_LocalType.h"
#include "EbuCoreformatType_dateModified_LocalType.h"
#include "EbuCoreformatType_imageFormat_CollectionType.h"
#include "EbuCoreformatType_medium_CollectionType.h"
#include "EbuCoreformatType_medium_LocalType.h"
#include "EbuCoreformatType_videoFormat_CollectionType.h"
#include "EbuCorefrequency_LocalType.h"
#include "EbuCorehashType.h"
#include "EbuCorehashType_hashFunction_LocalType.h"
#include "EbuCoreidentifierType.h"
#include "EbuCoreimageFormatType.h"
#include "EbuCoreimageFormatType_comment_CollectionType.h"
#include "EbuCoreimageFormatType_imageEncoding_LocalType.h"
#include "EbuCoreimageFormatType_orientation_LocalType.h"
#include "EbuCoreInt16.h"
#include "EbuCoreInt32.h"
#include "EbuCoreInt64.h"
#include "EbuCoreInt8.h"
#include "EbuCorelanguageType.h"
#include "EbuCorelengthType.h"
#include "EbuCorelocationType.h"
#include "EbuCorelocationType_coordinates_LocalType.h"
#include "EbuCoreloudnessMetadataType.h"
#include "EbuCorematrixType.h"
#include "EbuCorematrixType_coefficient_CollectionType.h"
#include "EbuCoreorganisationDepartmentType.h"
#include "EbuCoreorganisationDetailsType.h"
#include "EbuCoreorganisationDetailsType_contacts_CollectionType.h"
#include "EbuCoreorganisationDetailsType_details_CollectionType.h"
#include "EbuCoreorganisationDetailsType_organisationCode_CollectionType.h"
#include "EbuCoreorganisationDetailsType_organisationName_CollectionType.h"
#include "EbuCoreorganisationDetailsType_relatedInformationLink_CollectionType.h"
#include "EbuCoreorganisationDetailsType_relatedInformationLink_LocalType.h"
#include "EbuCorePackageIDType.h"
#include "EbuCorepartType.h"
#include "EbuCoreperiodOfTimeType.h"
#include "EbuCoreplanningType.h"
#include "EbuCoreplanningType_publicationEvent_CollectionType.h"
#include "EbuCoreposition_LocalType.h"
#include "EbuCorepublicationChannelType.h"
#include "EbuCorepublicationEventType.h"
#include "EbuCorepublicationEventType_publicationRegion_CollectionType.h"
#include "EbuCorepublicationHistoryType.h"
#include "EbuCorepublicationHistoryType_publicationEvent_CollectionType.h"
#include "EbuCorepublicationMediumType.h"
#include "EbuCorepublicationServiceType.h"
#include "EbuCoreratingType.h"
#include "EbuCoreratingType_ratingExclusionRegion_CollectionType.h"
#include "EbuCoreratingType_ratingLink_CollectionType.h"
#include "EbuCoreratingType_ratingRegion_CollectionType.h"
#include "EbuCoreratingType_ratingScaleMaxValue_CollectionType.h"
#include "EbuCoreratingType_ratingScaleMinValue_CollectionType.h"
#include "EbuCoreratingType_ratingValue_CollectionType.h"
#include "EbuCorerationalType.h"
#include "EbuCoreregionType.h"
#include "EbuCoreregionType_country_LocalType.h"
#include "EbuCoreregionType_countryRegion_CollectionType.h"
#include "EbuCoreregionType_countryRegion_LocalType.h"
#include "EbuCorerelationType.h"
#include "EbuCorerightsType.h"
#include "EbuCorerightsType_contactDetails_CollectionType.h"
#include "EbuCorerightsType_copyrightStatement_CollectionType.h"
#include "EbuCorerightsType_disclaimer_CollectionType.h"
#include "EbuCorerightsType_exploitationIssues_CollectionType.h"
#include "EbuCorerightsType_rights_CollectionType.h"
#include "EbuCorerightsType_rightsAttributedId_CollectionType.h"
#include "EbuCorerightsType_rightsHolder_CollectionType.h"
#include "EbuCoresigningFormatType.h"
#include "EbuCorespatialType.h"
#include "EbuCorespatialType_location_CollectionType.h"
#include "EbuCoreString.h"
#include "EbuCoresubjectType.h"
#include "EbuCoresubjectType_subject_CollectionType.h"
#include "EbuCoresubjectType_subjectDefinition_CollectionType.h"
#include "EbuCoresubtitlingFormatType.h"
#include "EbuCoretargetAudienceType.h"
#include "EbuCoretargetAudienceType_targetExclusionRegion_CollectionType.h"
#include "EbuCoretargetAudienceType_targetRegion_CollectionType.h"
#include "EbuCoretechnicalAttributeRationalType.h"
#include "EbuCoretechnicalAttributes.h"
#include "EbuCoretechnicalAttributes_technicalAttributeBoolean_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeByte_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeFloat_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeInteger_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeLong_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeRational_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeShort_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeString_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedByte_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedInteger_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedLong_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUnsignedShort_CollectionType.h"
#include "EbuCoretechnicalAttributes_technicalAttributeUri_CollectionType.h"
#include "EbuCoretechnicalAttributeUriType.h"
#include "EbuCoretemporalType.h"
#include "EbuCoretemporalType_PeriodOfTime_CollectionType.h"
#include "EbuCoretimecodeType.h"
#include "EbuCoretimeType.h"
#include "EbuCoretimeType_time_LocalType.h"
#include "EbuCoretitleType.h"
#include "EbuCoretitleType_title_CollectionType.h"
#include "EbuCoretypeType.h"
#include "EbuCoretypeType_genre_CollectionType.h"
#include "EbuCoretypeType_genre_LocalType.h"
#include "EbuCoretypeType_objectType_CollectionType.h"
#include "EbuCoretypeType_objectType_LocalType.h"
#include "EbuCoretypeType_targetAudience_CollectionType.h"
#include "EbuCoretypeType_type_CollectionType.h"
#include "EbuCoreUInt16.h"
#include "EbuCoreUInt32.h"
#include "EbuCoreUInt64.h"
#include "EbuCoreUInt8.h"
#include "EbuCoreversionType.h"
#include "EbuCorevideoFormatType.h"
#include "EbuCorevideoFormatType_aspectRatio_CollectionType.h"
#include "EbuCorevideoFormatType_bitRateMode_LocalType.h"
#include "EbuCorevideoFormatType_comment_CollectionType.h"
#include "EbuCorevideoFormatType_height_CollectionType.h"
#include "EbuCorevideoFormatType_height_LocalType.h"
#include "EbuCorevideoFormatType_scanningFormat_LocalType.h"
#include "EbuCorevideoFormatType_scanningOrder_LocalType.h"
#include "EbuCorevideoFormatType_videoEncoding_LocalType.h"
#include "EbuCorevideoFormatType_videoTrack_CollectionType.h"
#include "EbuCorevideoFormatType_videoTrack_LocalType.h"
#include "EbuCorevideoFormatType_width_CollectionType.h"
#include "EbuCorevideoFormatType_width_LocalType.h"

#endif // EBUCOREEVERYTHING_INCLUDED_H
