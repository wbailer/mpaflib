/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _483EBUCOREPUBLICATIONEVENTTYPE_H
#define _483EBUCOREPUBLICATIONEVENTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCorepublicationEventTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtInclude.h


class IEbuCorepublicationEventType;
typedef Dc1Ptr< IEbuCorepublicationEventType> EbuCorepublicationEventPtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IW3Ctime;
typedef Dc1Ptr< IW3Ctime > W3CtimePtr;
class IEbuCorepublicationServiceType;
typedef Dc1Ptr< IEbuCorepublicationServiceType > EbuCorepublicationServicePtr;
class IEbuCorepublicationMediumType;
typedef Dc1Ptr< IEbuCorepublicationMediumType > EbuCorepublicationMediumPtr;
class IEbuCorepublicationChannelType;
typedef Dc1Ptr< IEbuCorepublicationChannelType > EbuCorepublicationChannelPtr;
class IEbuCorepublicationEventType_publicationRegion_CollectionType;
typedef Dc1Ptr< IEbuCorepublicationEventType_publicationRegion_CollectionType > EbuCorepublicationEventType_publicationRegion_CollectionPtr;

/** 
 * Generated interface IEbuCorepublicationEventType for class EbuCorepublicationEventType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1613<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorepublicationEventType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorepublicationEventType();
	virtual ~IEbuCorepublicationEventType();
	/**
	 * Get publicationEventId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationEventId() const = 0;

	/**
	 * Get publicationEventId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpublicationEventId() const = 0;

	/**
	 * Set publicationEventId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationEventId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationEventId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationEventId() = 0;

	/**
	 * Get publicationEventName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationEventName() const = 0;

	/**
	 * Get publicationEventName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpublicationEventName() const = 0;

	/**
	 * Set publicationEventName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationEventName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationEventName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationEventName() = 0;

	/**
	 * Get firstShowing attribute.
	 * @return bool
	 */
	virtual bool GetfirstShowing() const = 0;

	/**
	 * Get firstShowing attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfirstShowing() const = 0;

	/**
	 * Set firstShowing attribute.
	 * @param item bool
	 */
	virtual void SetfirstShowing(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate firstShowing attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefirstShowing() = 0;

	/**
	 * Get lastShowing attribute.
	 * @return bool
	 */
	virtual bool GetlastShowing() const = 0;

	/**
	 * Get lastShowing attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastShowing() const = 0;

	/**
	 * Set lastShowing attribute.
	 * @param item bool
	 */
	virtual void SetlastShowing(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lastShowing attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastShowing() = 0;

	/**
	 * Get live attribute.
	 * @return bool
	 */
	virtual bool Getlive() const = 0;

	/**
	 * Get live attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlive() const = 0;

	/**
	 * Set live attribute.
	 * @param item bool
	 */
	virtual void Setlive(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate live attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelive() = 0;

	/**
	 * Get free attribute.
	 * @return bool
	 */
	virtual bool Getfree() const = 0;

	/**
	 * Get free attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfree() const = 0;

	/**
	 * Set free attribute.
	 * @param item bool
	 */
	virtual void Setfree(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate free attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefree() = 0;

	/**
	 * Get note attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const = 0;

	/**
	 * Get note attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const = 0;

	/**
	 * Set note attribute.
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate note attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote() = 0;

	/**
	 * Get formatIdRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatIdRef() const = 0;

	/**
	 * Get formatIdRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatIdRef() const = 0;

	/**
	 * Set formatIdRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatIdRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatIdRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatIdRef() = 0;

	/**
	 * Get rightsIDRefs attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetrightsIDRefs() const = 0;

	/**
	 * Get rightsIDRefs attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistrightsIDRefs() const = 0;

	/**
	 * Set rightsIDRefs attribute.
	 * @param item XMLCh *
	 */
	virtual void SetrightsIDRefs(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate rightsIDRefs attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidaterightsIDRefs() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get publicationDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetpublicationDate() const = 0;

	virtual bool IsValidpublicationDate() const = 0;

	/**
	 * Set publicationDate element.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetpublicationDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationDate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationDate() = 0;

	/**
	 * Get publicationTime element.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetpublicationTime() const = 0;

	virtual bool IsValidpublicationTime() const = 0;

	/**
	 * Set publicationTime element.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetpublicationTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationTime() = 0;

	/**
	 * Get scheduleDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetscheduleDate() const = 0;

	virtual bool IsValidscheduleDate() const = 0;

	/**
	 * Set scheduleDate element.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetscheduleDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate scheduleDate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescheduleDate() = 0;

	/**
	 * Get publicationService element.
	 * @return EbuCorepublicationServicePtr
	 */
	virtual EbuCorepublicationServicePtr GetpublicationService() const = 0;

	virtual bool IsValidpublicationService() const = 0;

	/**
	 * Set publicationService element.
	 * @param item const EbuCorepublicationServicePtr &
	 */
	virtual void SetpublicationService(const EbuCorepublicationServicePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationService element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationService() = 0;

	/**
	 * Get publicationMedium element.
	 * @return EbuCorepublicationMediumPtr
	 */
	virtual EbuCorepublicationMediumPtr GetpublicationMedium() const = 0;

	virtual bool IsValidpublicationMedium() const = 0;

	/**
	 * Set publicationMedium element.
	 * @param item const EbuCorepublicationMediumPtr &
	 */
	virtual void SetpublicationMedium(const EbuCorepublicationMediumPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationMedium element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationMedium() = 0;

	/**
	 * Get publicationChannel element.
	 * @return EbuCorepublicationChannelPtr
	 */
	virtual EbuCorepublicationChannelPtr GetpublicationChannel() const = 0;

	virtual bool IsValidpublicationChannel() const = 0;

	/**
	 * Set publicationChannel element.
	 * @param item const EbuCorepublicationChannelPtr &
	 */
	virtual void SetpublicationChannel(const EbuCorepublicationChannelPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate publicationChannel element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationChannel() = 0;

	/**
	 * Get publicationRegion element.
	 * @return EbuCorepublicationEventType_publicationRegion_CollectionPtr
	 */
	virtual EbuCorepublicationEventType_publicationRegion_CollectionPtr GetpublicationRegion() const = 0;

	/**
	 * Set publicationRegion element.
	 * @param item const EbuCorepublicationEventType_publicationRegion_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetpublicationRegion(const EbuCorepublicationEventType_publicationRegion_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of publicationEventType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>publicationDate</li>
	 * <li>publicationTime</li>
	 * <li>scheduleDate</li>
	 * <li>publicationService</li>
	 * <li>publicationMedium</li>
	 * <li>publicationChannel</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publicationRegion</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtPropDef.h

};




/*
 * Generated class EbuCorepublicationEventType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1613<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 1613.<br>
 */
class DC1_EXPORT EbuCorepublicationEventType :
		public IEbuCorepublicationEventType
{
	friend class EbuCorepublicationEventTypeFactory; // constructs objects

public:
	/*
	 * Get publicationEventId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationEventId() const;

	/*
	 * Get publicationEventId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpublicationEventId() const;

	/*
	 * Set publicationEventId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationEventId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationEventId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationEventId();

	/*
	 * Get publicationEventName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetpublicationEventName() const;

	/*
	 * Get publicationEventName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistpublicationEventName() const;

	/*
	 * Set publicationEventName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetpublicationEventName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationEventName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationEventName();

	/*
	 * Get firstShowing attribute.
	 * @return bool
	 */
	virtual bool GetfirstShowing() const;

	/*
	 * Get firstShowing attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistfirstShowing() const;

	/*
	 * Set firstShowing attribute.
	 * @param item bool
	 */
	virtual void SetfirstShowing(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate firstShowing attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatefirstShowing();

	/*
	 * Get lastShowing attribute.
	 * @return bool
	 */
	virtual bool GetlastShowing() const;

	/*
	 * Get lastShowing attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastShowing() const;

	/*
	 * Set lastShowing attribute.
	 * @param item bool
	 */
	virtual void SetlastShowing(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lastShowing attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastShowing();

	/*
	 * Get live attribute.
	 * @return bool
	 */
	virtual bool Getlive() const;

	/*
	 * Get live attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlive() const;

	/*
	 * Set live attribute.
	 * @param item bool
	 */
	virtual void Setlive(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate live attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelive();

	/*
	 * Get free attribute.
	 * @return bool
	 */
	virtual bool Getfree() const;

	/*
	 * Get free attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existfree() const;

	/*
	 * Set free attribute.
	 * @param item bool
	 */
	virtual void Setfree(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate free attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatefree();

	/*
	 * Get note attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const;

	/*
	 * Get note attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const;

	/*
	 * Set note attribute.
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate note attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote();

	/*
	 * Get formatIdRef attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatIdRef() const;

	/*
	 * Get formatIdRef attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatIdRef() const;

	/*
	 * Set formatIdRef attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatIdRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatIdRef attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatIdRef();

	/*
	 * Get rightsIDRefs attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetrightsIDRefs() const;

	/*
	 * Get rightsIDRefs attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistrightsIDRefs() const;

	/*
	 * Set rightsIDRefs attribute.
	 * @param item XMLCh *
	 */
	virtual void SetrightsIDRefs(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate rightsIDRefs attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidaterightsIDRefs();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get publicationDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetpublicationDate() const;

	virtual bool IsValidpublicationDate() const;

	/*
	 * Set publicationDate element.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetpublicationDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationDate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationDate();

	/*
	 * Get publicationTime element.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetpublicationTime() const;

	virtual bool IsValidpublicationTime() const;

	/*
	 * Set publicationTime element.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetpublicationTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationTime element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationTime();

	/*
	 * Get scheduleDate element.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetscheduleDate() const;

	virtual bool IsValidscheduleDate() const;

	/*
	 * Set scheduleDate element.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetscheduleDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate scheduleDate element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatescheduleDate();

	/*
	 * Get publicationService element.
	 * @return EbuCorepublicationServicePtr
	 */
	virtual EbuCorepublicationServicePtr GetpublicationService() const;

	virtual bool IsValidpublicationService() const;

	/*
	 * Set publicationService element.
	 * @param item const EbuCorepublicationServicePtr &
	 */
	virtual void SetpublicationService(const EbuCorepublicationServicePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationService element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationService();

	/*
	 * Get publicationMedium element.
	 * @return EbuCorepublicationMediumPtr
	 */
	virtual EbuCorepublicationMediumPtr GetpublicationMedium() const;

	virtual bool IsValidpublicationMedium() const;

	/*
	 * Set publicationMedium element.
	 * @param item const EbuCorepublicationMediumPtr &
	 */
	virtual void SetpublicationMedium(const EbuCorepublicationMediumPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationMedium element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationMedium();

	/*
	 * Get publicationChannel element.
	 * @return EbuCorepublicationChannelPtr
	 */
	virtual EbuCorepublicationChannelPtr GetpublicationChannel() const;

	virtual bool IsValidpublicationChannel() const;

	/*
	 * Set publicationChannel element.
	 * @param item const EbuCorepublicationChannelPtr &
	 */
	virtual void SetpublicationChannel(const EbuCorepublicationChannelPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate publicationChannel element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatepublicationChannel();

	/*
	 * Get publicationRegion element.
	 * @return EbuCorepublicationEventType_publicationRegion_CollectionPtr
	 */
	virtual EbuCorepublicationEventType_publicationRegion_CollectionPtr GetpublicationRegion() const;

	/*
	 * Set publicationRegion element.
	 * @param item const EbuCorepublicationEventType_publicationRegion_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetpublicationRegion(const EbuCorepublicationEventType_publicationRegion_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of publicationEventType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>publicationDate</li>
	 * <li>publicationTime</li>
	 * <li>scheduleDate</li>
	 * <li>publicationService</li>
	 * <li>publicationMedium</li>
	 * <li>publicationChannel</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:publicationRegion</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMyMethodDef.h


public:

	virtual ~EbuCorepublicationEventType();

protected:
	EbuCorepublicationEventType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_publicationEventId;
	bool m_publicationEventId_Exist;
	XMLCh * m_publicationEventName;
	bool m_publicationEventName_Exist;
	bool m_firstShowing;
	bool m_firstShowing_Exist;
	bool m_lastShowing;
	bool m_lastShowing_Exist;
	bool m_live;
	bool m_live_Exist;
	bool m_free;
	bool m_free_Exist;
	XMLCh * m_note;
	bool m_note_Exist;
	XMLCh * m_formatIdRef;
	bool m_formatIdRef_Exist;
	XMLCh * m_rightsIDRefs;
	bool m_rightsIDRefs_Exist;


	W3CdatePtr m_publicationDate;
	bool m_publicationDate_Exist; // For optional elements 
	W3CtimePtr m_publicationTime;
	bool m_publicationTime_Exist; // For optional elements 
	W3CdatePtr m_scheduleDate;
	bool m_scheduleDate_Exist; // For optional elements 
	EbuCorepublicationServicePtr m_publicationService;
	bool m_publicationService_Exist; // For optional elements 
	EbuCorepublicationMediumPtr m_publicationMedium;
	bool m_publicationMedium_Exist; // For optional elements 
	EbuCorepublicationChannelPtr m_publicationChannel;
	bool m_publicationChannel_Exist; // For optional elements 
	EbuCorepublicationEventType_publicationRegion_CollectionPtr m_publicationRegion;

// no includefile for extension defined 
// file EbuCorepublicationEventType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _483EBUCOREPUBLICATIONEVENTTYPE_H

