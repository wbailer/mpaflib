/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _159EBUCOREAUDIOOBJECTTYPE_H
#define _159EBUCOREAUDIOOBJECTTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioObjectTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtInclude.h


class IEbuCoreaudioObjectType;
typedef Dc1Ptr< IEbuCoreaudioObjectType> EbuCoreaudioObjectPtr;
class IEbuCoretimecodeType;
typedef Dc1Ptr< IEbuCoretimecodeType > EbuCoretimecodePtr;
class IEbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioPackFormatIDRef_CollectionType > EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr;
class IEbuCoreaudioObjectType_audioObjectIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioObjectIDRef_CollectionType > EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr;
class IEbuCoreaudioObjectType_audioTrackUIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioObjectType_audioTrackUIDRef_CollectionType > EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr;

/** 
 * Generated interface IEbuCoreaudioObjectType for class EbuCoreaudioObjectType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4014<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioObjectType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioObjectType();
	virtual ~IEbuCoreaudioObjectType();
	/**
	 * Get audioObjectID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioObjectID() const = 0;

	/**
	 * Get audioObjectID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioObjectID() const = 0;

	/**
	 * Set audioObjectID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioObjectID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioObjectID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioObjectID() = 0;

	/**
	 * Get audioObjectName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioObjectName() const = 0;

	/**
	 * Get audioObjectName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioObjectName() const = 0;

	/**
	 * Set audioObjectName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioObjectName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioObjectName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioObjectName() = 0;

	/**
	 * Get start attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getstart() const = 0;

	/**
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const = 0;

	/**
	 * Set start attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setstart(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart() = 0;

	/**
	 * Get duration attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getduration() const = 0;

	/**
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const = 0;

	/**
	 * Set duration attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration() = 0;

	/**
	 * Get dialogue attribute.
	 * @return int
	 */
	virtual int Getdialogue() const = 0;

	/**
	 * Get dialogue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdialogue() const = 0;

	/**
	 * Set dialogue attribute.
	 * @param item int
	 */
	virtual void Setdialogue(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dialogue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedialogue() = 0;

	/**
	 * Get importance attribute.
	 * @return int
	 */
	virtual int Getimportance() const = 0;

	/**
	 * Get importance attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existimportance() const = 0;

	/**
	 * Set importance attribute.
	 * @param item int
	 */
	virtual void Setimportance(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate importance attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateimportance() = 0;

	/**
	 * Get interact attribute.
	 * @return bool
	 */
	virtual bool Getinteract() const = 0;

	/**
	 * Get interact attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existinteract() const = 0;

	/**
	 * Set interact attribute.
	 * @param item bool
	 */
	virtual void Setinteract(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate interact attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateinteract() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get audioPackFormatIDRef element.
	 * @return EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr GetaudioPackFormatIDRef() const = 0;

	/**
	 * Set audioPackFormatIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormatIDRef(const EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioObjectIDRef element.
	 * @return EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr GetaudioObjectIDRef() const = 0;

	/**
	 * Set audioObjectIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioObjectIDRef(const EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioTrackUIDRef element.
	 * @return EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr GetaudioTrackUIDRef() const = 0;

	/**
	 * Set audioTrackUIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackUIDRef(const EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioObjectType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioObjectIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackUIDRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioObjectType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4014<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4014.<br>
 */
class DC1_EXPORT EbuCoreaudioObjectType :
		public IEbuCoreaudioObjectType
{
	friend class EbuCoreaudioObjectTypeFactory; // constructs objects

public:
	/*
	 * Get audioObjectID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioObjectID() const;

	/*
	 * Get audioObjectID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioObjectID() const;

	/*
	 * Set audioObjectID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioObjectID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioObjectID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioObjectID();

	/*
	 * Get audioObjectName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioObjectName() const;

	/*
	 * Get audioObjectName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioObjectName() const;

	/*
	 * Set audioObjectName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioObjectName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioObjectName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioObjectName();

	/*
	 * Get start attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getstart() const;

	/*
	 * Get start attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existstart() const;

	/*
	 * Set start attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setstart(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate start attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatestart();

	/*
	 * Get duration attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getduration() const;

	/*
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const;

	/*
	 * Set duration attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration();

	/*
	 * Get dialogue attribute.
	 * @return int
	 */
	virtual int Getdialogue() const;

	/*
	 * Get dialogue attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existdialogue() const;

	/*
	 * Set dialogue attribute.
	 * @param item int
	 */
	virtual void Setdialogue(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dialogue attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedialogue();

	/*
	 * Get importance attribute.
	 * @return int
	 */
	virtual int Getimportance() const;

	/*
	 * Get importance attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existimportance() const;

	/*
	 * Set importance attribute.
	 * @param item int
	 */
	virtual void Setimportance(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate importance attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateimportance();

	/*
	 * Get interact attribute.
	 * @return bool
	 */
	virtual bool Getinteract() const;

	/*
	 * Get interact attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existinteract() const;

	/*
	 * Set interact attribute.
	 * @param item bool
	 */
	virtual void Setinteract(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate interact attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateinteract();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get audioPackFormatIDRef element.
	 * @return EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr GetaudioPackFormatIDRef() const;

	/*
	 * Set audioPackFormatIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormatIDRef(const EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioObjectIDRef element.
	 * @return EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr GetaudioObjectIDRef() const;

	/*
	 * Set audioObjectIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioObjectIDRef(const EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioTrackUIDRef element.
	 * @return EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr GetaudioTrackUIDRef() const;

	/*
	 * Set audioTrackUIDRef element.
	 * @param item const EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackUIDRef(const EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioObjectType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioObjectIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackUIDRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioObjectType();

protected:
	EbuCoreaudioObjectType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_audioObjectID;
	bool m_audioObjectID_Exist;
	XMLCh * m_audioObjectName;
	bool m_audioObjectName_Exist;
	EbuCoretimecodePtr m_start;
	bool m_start_Exist;
	EbuCoretimecodePtr m_duration;
	bool m_duration_Exist;
	int m_dialogue;
	bool m_dialogue_Exist;
	int m_dialogue_Default;
	int m_importance;
	bool m_importance_Exist;
	bool m_interact;
	bool m_interact_Exist;


	EbuCoreaudioObjectType_audioPackFormatIDRef_CollectionPtr m_audioPackFormatIDRef;
	EbuCoreaudioObjectType_audioObjectIDRef_CollectionPtr m_audioObjectIDRef;
	EbuCoreaudioObjectType_audioTrackUIDRef_CollectionPtr m_audioTrackUIDRef;

// no includefile for extension defined 
// file EbuCoreaudioObjectType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _159EBUCOREAUDIOOBJECTTYPE_H

