/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _551EBUCORESUBTITLINGFORMATTYPE_H
#define _551EBUCORESUBTITLINGFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"

#include "EbuCoresubtitlingFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoresubtitlingFormatType_ExtInclude.h


class IEbuCoresubtitlingFormatType;
typedef Dc1Ptr< IEbuCoresubtitlingFormatType> EbuCoresubtitlingFormatPtr;

/** 
 * Generated interface IEbuCoresubtitlingFormatType for class EbuCoresubtitlingFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3381<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoresubtitlingFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoresubtitlingFormatType();
	virtual ~IEbuCoresubtitlingFormatType();
	/**
	 * Get subtitlingFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingFormatId() const = 0;

	/**
	 * Get subtitlingFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingFormatId() const = 0;

	/**
	 * Set subtitlingFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate subtitlingFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingFormatId() = 0;

	/**
	 * Get subtitlingFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingFormatName() const = 0;

	/**
	 * Get subtitlingFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingFormatName() const = 0;

	/**
	 * Set subtitlingFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate subtitlingFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingFormatName() = 0;

	/**
	 * Get trackId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettrackId() const = 0;

	/**
	 * Get trackId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttrackId() const = 0;

	/**
	 * Set trackId attribute.
	 * @param item XMLCh *
	 */
	virtual void SettrackId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate trackId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetrackId() = 0;

	/**
	 * Get trackName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettrackName() const = 0;

	/**
	 * Get trackName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttrackName() const = 0;

	/**
	 * Set trackName attribute.
	 * @param item XMLCh *
	 */
	virtual void SettrackName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate trackName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetrackName() = 0;

	/**
	 * Get subtitlingSourceUri attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingSourceUri() const = 0;

	/**
	 * Get subtitlingSourceUri attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingSourceUri() const = 0;

	/**
	 * Set subtitlingSourceUri attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingSourceUri(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate subtitlingSourceUri attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingSourceUri() = 0;

	/**
	 * Get language attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlanguage() const = 0;

	/**
	 * Get language attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlanguage() const = 0;

	/**
	 * Set language attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate language attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelanguage() = 0;

	/**
	 * Get closed attribute.
	 * @return bool
	 */
	virtual bool Getclosed() const = 0;

	/**
	 * Get closed attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existclosed() const = 0;

	/**
	 * Set closed attribute.
	 * @param item bool
	 */
	virtual void Setclosed(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate closed attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateclosed() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const = 0;

	/**
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const = 0;

	/**
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel() = 0;

	/**
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const = 0;

	/**
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const = 0;

	/**
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition() = 0;

	/**
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const = 0;

	/**
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const = 0;

	/**
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink() = 0;

	/**
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const = 0;

	/**
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const = 0;

	/**
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource() = 0;

	/**
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const = 0;

	/**
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const = 0;

	/**
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage() = 0;

	/**
	 * Get subtitlingPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetsubtitlingPresenceFlag() const = 0;

	/**
	 * Get subtitlingPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingPresenceFlag() const = 0;

	/**
	 * Set subtitlingPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetsubtitlingPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate subtitlingPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingPresenceFlag() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of subtitlingFormatType.
	 * Currently just supports rudimentary XPath expressions as subtitlingFormatType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoresubtitlingFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoresubtitlingFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCoresubtitlingFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3381<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 3381.<br>
 */
class DC1_EXPORT EbuCoresubtitlingFormatType :
		public IEbuCoresubtitlingFormatType
{
	friend class EbuCoresubtitlingFormatTypeFactory; // constructs objects

public:
	/*
	 * Get subtitlingFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingFormatId() const;

	/*
	 * Get subtitlingFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingFormatId() const;

	/*
	 * Set subtitlingFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate subtitlingFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingFormatId();

	/*
	 * Get subtitlingFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingFormatName() const;

	/*
	 * Get subtitlingFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingFormatName() const;

	/*
	 * Set subtitlingFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate subtitlingFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingFormatName();

	/*
	 * Get trackId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettrackId() const;

	/*
	 * Get trackId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttrackId() const;

	/*
	 * Set trackId attribute.
	 * @param item XMLCh *
	 */
	virtual void SettrackId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate trackId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetrackId();

	/*
	 * Get trackName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettrackName() const;

	/*
	 * Get trackName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttrackName() const;

	/*
	 * Set trackName attribute.
	 * @param item XMLCh *
	 */
	virtual void SettrackName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate trackName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetrackName();

	/*
	 * Get subtitlingSourceUri attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsubtitlingSourceUri() const;

	/*
	 * Get subtitlingSourceUri attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingSourceUri() const;

	/*
	 * Set subtitlingSourceUri attribute.
	 * @param item XMLCh *
	 */
	virtual void SetsubtitlingSourceUri(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate subtitlingSourceUri attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingSourceUri();

	/*
	 * Get language attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlanguage() const;

	/*
	 * Get language attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlanguage() const;

	/*
	 * Set language attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate language attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelanguage();

	/*
	 * Get closed attribute.
	 * @return bool
	 */
	virtual bool Getclosed() const;

	/*
	 * Get closed attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existclosed() const;

	/*
	 * Set closed attribute.
	 * @param item bool
	 */
	virtual void Setclosed(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate closed attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateclosed();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const;

	/*
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const;

	/*
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel();

	/*
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const;

	/*
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const;

	/*
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition();

	/*
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const;

	/*
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const;

	/*
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink();

	/*
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const;

	/*
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const;

	/*
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource();

	/*
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const;

	/*
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const;

	/*
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage();

	/*
	 * Get subtitlingPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetsubtitlingPresenceFlag() const;

	/*
	 * Get subtitlingPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsubtitlingPresenceFlag() const;

	/*
	 * Set subtitlingPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetsubtitlingPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate subtitlingPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesubtitlingPresenceFlag();

	/*
	 * Gets or creates Dc1NodePtr child elements of subtitlingFormatType.
	 * Currently just supports rudimentary XPath expressions as subtitlingFormatType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoresubtitlingFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoresubtitlingFormatType();

protected:
	EbuCoresubtitlingFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_subtitlingFormatId;
	bool m_subtitlingFormatId_Exist;
	XMLCh * m_subtitlingFormatName;
	bool m_subtitlingFormatName_Exist;
	XMLCh * m_trackId;
	bool m_trackId_Exist;
	XMLCh * m_trackName;
	bool m_trackName_Exist;
	XMLCh * m_subtitlingSourceUri;
	bool m_subtitlingSourceUri_Exist;
	XMLCh * m_language;
	bool m_language_Exist;
	bool m_closed;
	bool m_closed_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;
	XMLCh * m_formatLabel;
	bool m_formatLabel_Exist;
	XMLCh * m_formatDefinition;
	bool m_formatDefinition_Exist;
	XMLCh * m_formatLink;
	bool m_formatLink_Exist;
	XMLCh * m_formatSource;
	bool m_formatSource_Exist;
	XMLCh * m_formatLanguage;
	bool m_formatLanguage_Exist;
	bool m_subtitlingPresenceFlag;
	bool m_subtitlingPresenceFlag_Exist;



// no includefile for extension defined 
// file EbuCoresubtitlingFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _551EBUCORESUBTITLINGFORMATTYPE_H

