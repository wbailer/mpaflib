/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _365EBUCOREEBUCOREMAINTYPE_H
#define _365EBUCOREEBUCOREMAINTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCoreebuCoreMainTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtInclude.h


class IEbuCoreebuCoreMainType;
typedef Dc1Ptr< IEbuCoreebuCoreMainType> EbuCoreebuCoreMainPtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IW3Ctime;
typedef Dc1Ptr< IW3Ctime > W3CtimePtr;
class IEbuCorecoreMetadataType;
typedef Dc1Ptr< IEbuCorecoreMetadataType > EbuCorecoreMetadataPtr;
class IEbuCoreentityType;
typedef Dc1Ptr< IEbuCoreentityType > EbuCoreentityPtr;

/** 
 * Generated interface IEbuCoreebuCoreMainType for class EbuCoreebuCoreMainType<br>
 * Located at: EBU_CORE_20140318.xsd, line 94<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreebuCoreMainType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreebuCoreMainType();
	virtual ~IEbuCoreebuCoreMainType();
	/**
	 * Get schema attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getschema() const = 0;

	/**
	 * Get schema attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existschema() const = 0;

	/**
	 * Set schema attribute.
	 * @param item XMLCh *
	 */
	virtual void Setschema(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate schema attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateschema() = 0;

	/**
	 * Get version attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getversion() const = 0;

	/**
	 * Get version attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existversion() const = 0;

	/**
	 * Set version attribute.
	 * @param item XMLCh *
	 */
	virtual void Setversion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate version attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateversion() = 0;

	/**
	 * Get dateLastModified attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetdateLastModified() const = 0;

	/**
	 * Get dateLastModified attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdateLastModified() const = 0;

	/**
	 * Set dateLastModified attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetdateLastModified(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dateLastModified attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateLastModified() = 0;

	/**
	 * Get timeLastModified attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GettimeLastModified() const = 0;

	/**
	 * Get timeLastModified attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeLastModified() const = 0;

	/**
	 * Set timeLastModified attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SettimeLastModified(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate timeLastModified attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeLastModified() = 0;

	/**
	 * Get documentId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdocumentId() const = 0;

	/**
	 * Get documentId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdocumentId() const = 0;

	/**
	 * Set documentId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdocumentId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate documentId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentId() = 0;

	/**
	 * Get documentLocation attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdocumentLocation() const = 0;

	/**
	 * Get documentLocation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdocumentLocation() const = 0;

	/**
	 * Set documentLocation attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdocumentLocation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate documentLocation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentLocation() = 0;

	/**
	 * Get lang attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get coreMetadata element.
	 * @return EbuCorecoreMetadataPtr
	 */
	virtual EbuCorecoreMetadataPtr GetcoreMetadata() const = 0;

	/**
	 * Set coreMetadata element.
	 * @param item const EbuCorecoreMetadataPtr &
	 */
	// Mandatory			
	virtual void SetcoreMetadata(const EbuCorecoreMetadataPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get metadataProvider element.
	 * @return EbuCoreentityPtr
	 */
	virtual EbuCoreentityPtr GetmetadataProvider() const = 0;

	virtual bool IsValidmetadataProvider() const = 0;

	/**
	 * Set metadataProvider element.
	 * @param item const EbuCoreentityPtr &
	 */
	virtual void SetmetadataProvider(const EbuCoreentityPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate metadataProvider element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemetadataProvider() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of ebuCoreMainType.
	 * Currently this type contains 2 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>coreMetadata</li>
	 * <li>metadataProvider</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtPropDef.h

};




/*
 * Generated class EbuCoreebuCoreMainType<br>
 * Located at: EBU_CORE_20140318.xsd, line 94<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 94.<br>
 */
class DC1_EXPORT EbuCoreebuCoreMainType :
		public IEbuCoreebuCoreMainType
{
	friend class EbuCoreebuCoreMainTypeFactory; // constructs objects

public:
	/*
	 * Get schema attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getschema() const;

	/*
	 * Get schema attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existschema() const;

	/*
	 * Set schema attribute.
	 * @param item XMLCh *
	 */
	virtual void Setschema(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate schema attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateschema();

	/*
	 * Get version attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getversion() const;

	/*
	 * Get version attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existversion() const;

	/*
	 * Set version attribute.
	 * @param item XMLCh *
	 */
	virtual void Setversion(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate version attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateversion();

	/*
	 * Get dateLastModified attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetdateLastModified() const;

	/*
	 * Get dateLastModified attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdateLastModified() const;

	/*
	 * Set dateLastModified attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetdateLastModified(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dateLastModified attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedateLastModified();

	/*
	 * Get timeLastModified attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GettimeLastModified() const;

	/*
	 * Get timeLastModified attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttimeLastModified() const;

	/*
	 * Set timeLastModified attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SettimeLastModified(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate timeLastModified attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetimeLastModified();

	/*
	 * Get documentId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdocumentId() const;

	/*
	 * Get documentId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdocumentId() const;

	/*
	 * Set documentId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdocumentId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate documentId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentId();

	/*
	 * Get documentLocation attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdocumentLocation() const;

	/*
	 * Get documentLocation attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdocumentLocation() const;

	/*
	 * Set documentLocation attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdocumentLocation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate documentLocation attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedocumentLocation();

	/*
	 * Get lang attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get coreMetadata element.
	 * @return EbuCorecoreMetadataPtr
	 */
	virtual EbuCorecoreMetadataPtr GetcoreMetadata() const;

	/*
	 * Set coreMetadata element.
	 * @param item const EbuCorecoreMetadataPtr &
	 */
	// Mandatory			
	virtual void SetcoreMetadata(const EbuCorecoreMetadataPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get metadataProvider element.
	 * @return EbuCoreentityPtr
	 */
	virtual EbuCoreentityPtr GetmetadataProvider() const;

	virtual bool IsValidmetadataProvider() const;

	/*
	 * Set metadataProvider element.
	 * @param item const EbuCoreentityPtr &
	 */
	virtual void SetmetadataProvider(const EbuCoreentityPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate metadataProvider element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemetadataProvider();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of ebuCoreMainType.
	 * Currently this type contains 2 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>coreMetadata</li>
	 * <li>metadataProvider</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreebuCoreMainType();

protected:
	EbuCoreebuCoreMainType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_schema;
	bool m_schema_Exist;
	XMLCh * m_version;
	bool m_version_Exist;
	XMLCh * m_version_Default;
	W3CdatePtr m_dateLastModified;
	bool m_dateLastModified_Exist;
	W3CtimePtr m_timeLastModified;
	bool m_timeLastModified_Exist;
	XMLCh * m_documentId;
	bool m_documentId_Exist;
	XMLCh * m_documentLocation;
	bool m_documentLocation_Exist;
	XMLCh * m_lang;
	bool m_lang_Exist;


	EbuCorecoreMetadataPtr m_coreMetadata;
	EbuCoreentityPtr m_metadataProvider;
	bool m_metadataProvider_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreebuCoreMainType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _365EBUCOREEBUCOREMAINTYPE_H

