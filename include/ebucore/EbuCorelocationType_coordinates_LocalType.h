/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _445EBUCORELOCATIONTYPE_COORDINATES_LOCALTYPE_H
#define _445EBUCORELOCATIONTYPE_COORDINATES_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"

#include "EbuCorelocationType_coordinates_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtInclude.h


class IEbuCorelocationType_coordinates_LocalType;
typedef Dc1Ptr< IEbuCorelocationType_coordinates_LocalType> EbuCorelocationType_coordinates_LocalPtr;

/** 
 * Generated interface IEbuCorelocationType_coordinates_LocalType for class EbuCorelocationType_coordinates_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1174<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCorelocationType_coordinates_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCorelocationType_coordinates_LocalType();
	virtual ~IEbuCorelocationType_coordinates_LocalType();
	/**
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const = 0;

	/**
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const = 0;

	/**
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel() = 0;

	/**
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const = 0;

	/**
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const = 0;

	/**
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition() = 0;

	/**
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const = 0;

	/**
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const = 0;

	/**
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink() = 0;

	/**
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const = 0;

	/**
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const = 0;

	/**
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource() = 0;

	/**
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const = 0;

	/**
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const = 0;

	/**
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get posy element.
	 * @return float
	 */
	virtual float Getposy() const = 0;

	/**
	 * Set posy element.
	 * @param item float
	 */
	// Mandatory			
	virtual void Setposy(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get posx element.
	 * @return float
	 */
	virtual float Getposx() const = 0;

	/**
	 * Set posx element.
	 * @param item float
	 */
	// Mandatory			
	virtual void Setposx(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of locationType_coordinates_LocalType.
	 * Currently just supports rudimentary XPath expressions as locationType_coordinates_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtPropDef.h

};




/*
 * Generated class EbuCorelocationType_coordinates_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1174<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 1174.<br>
 */
class DC1_EXPORT EbuCorelocationType_coordinates_LocalType :
		public IEbuCorelocationType_coordinates_LocalType
{
	friend class EbuCorelocationType_coordinates_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const;

	/*
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const;

	/*
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel();

	/*
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const;

	/*
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const;

	/*
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition();

	/*
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const;

	/*
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const;

	/*
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink();

	/*
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const;

	/*
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const;

	/*
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource();

	/*
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const;

	/*
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const;

	/*
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get posy element.
	 * @return float
	 */
	virtual float Getposy() const;

	/*
	 * Set posy element.
	 * @param item float
	 */
	// Mandatory			
	virtual void Setposy(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get posx element.
	 * @return float
	 */
	virtual float Getposx() const;

	/*
	 * Set posx element.
	 * @param item float
	 */
	// Mandatory			
	virtual void Setposx(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of locationType_coordinates_LocalType.
	 * Currently just supports rudimentary XPath expressions as locationType_coordinates_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMyMethodDef.h


public:

	virtual ~EbuCorelocationType_coordinates_LocalType();

protected:
	EbuCorelocationType_coordinates_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_formatLabel;
	bool m_formatLabel_Exist;
	XMLCh * m_formatDefinition;
	bool m_formatDefinition_Exist;
	XMLCh * m_formatLink;
	bool m_formatLink_Exist;
	XMLCh * m_formatSource;
	bool m_formatSource_Exist;
	XMLCh * m_formatLanguage;
	bool m_formatLanguage_Exist;


	float m_posy;
	float m_posx;

// no includefile for extension defined 
// file EbuCorelocationType_coordinates_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _445EBUCORELOCATIONTYPE_COORDINATES_LOCALTYPE_H

