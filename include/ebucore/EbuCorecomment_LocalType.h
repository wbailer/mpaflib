/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _197EBUCORECOMMENT_LOCALTYPE_H
#define _197EBUCORECOMMENT_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCorecomment_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorecomment_LocalType_ExtInclude.h


class IEbuCoreelementType;
typedef Dc1Ptr< IEbuCoreelementType > EbuCoreelementPtr;
class IEbuCorecomment_LocalType;
typedef Dc1Ptr< IEbuCorecomment_LocalType> EbuCorecomment_LocalPtr;
#include "EbuCoreelementType.h"
#include "Dc1elementType.h"

/** 
 * Generated interface IEbuCorecomment_LocalType for class EbuCorecomment_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 5007<br>
 * Classified: Class<br>
 * Derived from: elementType (Class)<br>
 */
class EBUCORE_EXPORT IEbuCorecomment_LocalType 
 :
		public IEbuCoreelementType
{
public:
	// TODO: make these protected?
	IEbuCorecomment_LocalType();
	virtual ~IEbuCorecomment_LocalType();
	/**
	 * Sets the content of Dc1elementType<br>
	 * (Inherited from Dc1elementType)
	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Retrieves the content of Dc1elementType 
<br>
	 * (Inherited from Dc1elementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get charset attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getcharset() const = 0;

	/**
	 * Get charset attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcharset() const = 0;

	/**
	 * Set charset attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Setcharset(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate charset attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecharset() = 0;

	/**
	 * Get encoding attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const = 0;

	/**
	 * Get encoding attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const = 0;

	/**
	 * Set encoding attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate encoding attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding() = 0;

	/**
	 * Get transcription attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettranscription() const = 0;

	/**
	 * Get transcription attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtranscription() const = 0;

	/**
	 * Set transcription attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Settranscription(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate transcription attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetranscription() = 0;

	/**
	 * Get lang attribute.
<br>
	 * (Inherited from Dc1elementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const = 0;

	/**
	 * Get lang attribute validity information.
	 * (Inherited from Dc1elementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const = 0;

	/**
	 * Set lang attribute.
<br>
	 * (Inherited from Dc1elementType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lang attribute.
	 * (Inherited from Dc1elementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of comment_LocalType.
	 * Currently just supports rudimentary XPath expressions as comment_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorecomment_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorecomment_LocalType_ExtPropDef.h

};




/*
 * Generated class EbuCorecomment_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 5007<br>
 * Classified: Class<br>
 * Derived from: elementType (Class)<br>
 * Defined in EBU_CORE_20140318.xsd, line 5007.<br>
 */
class DC1_EXPORT EbuCorecomment_LocalType :
		public IEbuCorecomment_LocalType
{
	friend class EbuCorecomment_LocalTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of EbuCorecomment_LocalType
	 
	 * @return Dc1Ptr< EbuCoreelementType >
	 */
	virtual Dc1Ptr< EbuCoreelementType > GetBase() const;
	/*
	 * Sets the content of Dc1elementType<br>
	 * (Inherited from Dc1elementType)
	 * @param item XMLCh *
	 */
	virtual void SetContent(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Retrieves the content of Dc1elementType 
<br>
	 * (Inherited from Dc1elementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetContent() const;

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get charset attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getcharset() const;

	/*
	 * Get charset attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existcharset() const;

	/*
	 * Set charset attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Setcharset(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate charset attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecharset();

	/*
	 * Get encoding attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getencoding() const;

	/*
	 * Get encoding attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existencoding() const;

	/*
	 * Set encoding attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Setencoding(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate encoding attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateencoding();

	/*
	 * Get transcription attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Gettranscription() const;

	/*
	 * Get transcription attribute validity information.
	 * (Inherited from EbuCoreelementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existtranscription() const;

	/*
	 * Set transcription attribute.
<br>
	 * (Inherited from EbuCoreelementType)
	 * @param item XMLCh *
	 */
	virtual void Settranscription(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate transcription attribute.
	 * (Inherited from EbuCoreelementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatetranscription();

	/*
	 * Get lang attribute.
<br>
	 * (Inherited from Dc1elementType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getlang() const;

	/*
	 * Get lang attribute validity information.
	 * (Inherited from Dc1elementType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existlang() const;

	/*
	 * Set lang attribute.
<br>
	 * (Inherited from Dc1elementType)
	 * @param item XMLCh *
	 */
	virtual void Setlang(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lang attribute.
	 * (Inherited from Dc1elementType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatelang();

	/*
	 * Gets or creates Dc1NodePtr child elements of comment_LocalType.
	 * Currently just supports rudimentary XPath expressions as comment_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Parse the content from a string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool ContentFromString(const XMLCh * const txt);

	/* Convert the content to string.  Note, this is purely for ComplexTypes with SimpleContent and SimpleTypes. All other types may return NULL or the class name.
	 * @return the allocated XMLCh * if conversion was successful. The caller must free memory using Dc1Util.deallocate().
	 */
	virtual XMLCh * ContentToString() const;


	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorecomment_LocalType_ExtMyMethodDef.h


public:

	virtual ~EbuCorecomment_LocalType();

protected:
	EbuCorecomment_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;

	// Base Class
	Dc1Ptr< EbuCoreelementType > m_Base;


// no includefile for extension defined 
// file EbuCorecomment_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _197EBUCORECOMMENT_LOCALTYPE_H

