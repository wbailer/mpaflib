/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _187EBUCOREAUDIOTRACKUIDTYPE_H
#define _187EBUCOREAUDIOTRACKUIDTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioTrackUIDTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtInclude.h


class IEbuCoreaudioTrackUIDType;
typedef Dc1Ptr< IEbuCoreaudioTrackUIDType> EbuCoreaudioTrackUIDPtr;
class IEbuCoreaudioMXFLookUpType;
typedef Dc1Ptr< IEbuCoreaudioMXFLookUpType > EbuCoreaudioMXFLookUpPtr;

/** 
 * Generated interface IEbuCoreaudioTrackUIDType for class EbuCoreaudioTrackUIDType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4443<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioTrackUIDType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioTrackUIDType();
	virtual ~IEbuCoreaudioTrackUIDType();
	/**
	 * Get UID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetUID() const = 0;

	/**
	 * Get UID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistUID() const = 0;

	/**
	 * Set UID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetUID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate UID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateUID() = 0;

	/**
	 * Get sampleRate attribute.
	 * @return int
	 */
	virtual int GetsampleRate() const = 0;

	/**
	 * Get sampleRate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsampleRate() const = 0;

	/**
	 * Set sampleRate attribute.
	 * @param item int
	 */
	virtual void SetsampleRate(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sampleRate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleRate() = 0;

	/**
	 * Get bitDepth attribute.
	 * @return int
	 */
	virtual int GetbitDepth() const = 0;

	/**
	 * Get bitDepth attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbitDepth() const = 0;

	/**
	 * Set bitDepth attribute.
	 * @param item int
	 */
	virtual void SetbitDepth(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate bitDepth attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitDepth() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get audioMXFLookUp element.
	 * @return EbuCoreaudioMXFLookUpPtr
	 */
	virtual EbuCoreaudioMXFLookUpPtr GetaudioMXFLookUp() const = 0;

	virtual bool IsValidaudioMXFLookUp() const = 0;

	/**
	 * Set audioMXFLookUp element.
	 * @param item const EbuCoreaudioMXFLookUpPtr &
	 */
	virtual void SetaudioMXFLookUp(const EbuCoreaudioMXFLookUpPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioMXFLookUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioMXFLookUp() = 0;

	/**
	 * Get audioTrackFormatIDRef element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioTrackFormatIDRef() const = 0;

	virtual bool IsValidaudioTrackFormatIDRef() const = 0;

	/**
	 * Set audioTrackFormatIDRef element.
	 * @param item XMLCh *
	 */
	virtual void SetaudioTrackFormatIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioTrackFormatIDRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioTrackFormatIDRef() = 0;

	/**
	 * Get audioPackFormatIDRef element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioPackFormatIDRef() const = 0;

	virtual bool IsValidaudioPackFormatIDRef() const = 0;

	/**
	 * Set audioPackFormatIDRef element.
	 * @param item XMLCh *
	 */
	virtual void SetaudioPackFormatIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioPackFormatIDRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioPackFormatIDRef() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioTrackUIDType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>audioMXFLookUp</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioTrackUIDType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4443<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4443.<br>
 */
class DC1_EXPORT EbuCoreaudioTrackUIDType :
		public IEbuCoreaudioTrackUIDType
{
	friend class EbuCoreaudioTrackUIDTypeFactory; // constructs objects

public:
	/*
	 * Get UID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetUID() const;

	/*
	 * Get UID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistUID() const;

	/*
	 * Set UID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetUID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate UID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateUID();

	/*
	 * Get sampleRate attribute.
	 * @return int
	 */
	virtual int GetsampleRate() const;

	/*
	 * Get sampleRate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistsampleRate() const;

	/*
	 * Set sampleRate attribute.
	 * @param item int
	 */
	virtual void SetsampleRate(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sampleRate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesampleRate();

	/*
	 * Get bitDepth attribute.
	 * @return int
	 */
	virtual int GetbitDepth() const;

	/*
	 * Get bitDepth attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistbitDepth() const;

	/*
	 * Set bitDepth attribute.
	 * @param item int
	 */
	virtual void SetbitDepth(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate bitDepth attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatebitDepth();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get audioMXFLookUp element.
	 * @return EbuCoreaudioMXFLookUpPtr
	 */
	virtual EbuCoreaudioMXFLookUpPtr GetaudioMXFLookUp() const;

	virtual bool IsValidaudioMXFLookUp() const;

	/*
	 * Set audioMXFLookUp element.
	 * @param item const EbuCoreaudioMXFLookUpPtr &
	 */
	virtual void SetaudioMXFLookUp(const EbuCoreaudioMXFLookUpPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioMXFLookUp element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioMXFLookUp();

	/*
	 * Get audioTrackFormatIDRef element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioTrackFormatIDRef() const;

	virtual bool IsValidaudioTrackFormatIDRef() const;

	/*
	 * Set audioTrackFormatIDRef element.
	 * @param item XMLCh *
	 */
	virtual void SetaudioTrackFormatIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioTrackFormatIDRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioTrackFormatIDRef();

	/*
	 * Get audioPackFormatIDRef element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioPackFormatIDRef() const;

	virtual bool IsValidaudioPackFormatIDRef() const;

	/*
	 * Set audioPackFormatIDRef element.
	 * @param item XMLCh *
	 */
	virtual void SetaudioPackFormatIDRef(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioPackFormatIDRef element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioPackFormatIDRef();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioTrackUIDType.
	 * Currently this type contains one child element of type "Dc1NodePtr".

	 * <ul>
	 * <li>audioMXFLookUp</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioTrackUIDType();

protected:
	EbuCoreaudioTrackUIDType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_UID;
	bool m_UID_Exist;
	int m_sampleRate;
	bool m_sampleRate_Exist;
	int m_bitDepth;
	bool m_bitDepth_Exist;


	EbuCoreaudioMXFLookUpPtr m_audioMXFLookUp;
	bool m_audioMXFLookUp_Exist; // For optional elements 
	XMLCh * m_audioTrackFormatIDRef;
	bool m_audioTrackFormatIDRef_Exist; // For optional elements 
	XMLCh * m_audioPackFormatIDRef;
	bool m_audioPackFormatIDRef_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreaudioTrackUIDType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _187EBUCOREAUDIOTRACKUIDTYPE_H

