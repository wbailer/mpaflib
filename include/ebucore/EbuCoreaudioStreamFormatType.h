/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _177EBUCOREAUDIOSTREAMFORMATTYPE_H
#define _177EBUCOREAUDIOSTREAMFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioStreamFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtInclude.h


class IEbuCoreaudioStreamFormatType;
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType> EbuCoreaudioStreamFormatPtr;
class IEbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr;
class IEbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr;
class IEbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionType > EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr;

/** 
 * Generated interface IEbuCoreaudioStreamFormatType for class EbuCoreaudioStreamFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4369<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioStreamFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioStreamFormatType();
	virtual ~IEbuCoreaudioStreamFormatType();
	/**
	 * Get audioStreamFormatID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioStreamFormatID() const = 0;

	/**
	 * Set audioStreamFormatID attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetaudioStreamFormatID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioStreamFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioStreamFormatName() const = 0;

	/**
	 * Get audioStreamFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioStreamFormatName() const = 0;

	/**
	 * Set audioStreamFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioStreamFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioStreamFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioStreamFormatName() = 0;

	/**
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const = 0;

	/**
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const = 0;

	/**
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel() = 0;

	/**
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const = 0;

	/**
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const = 0;

	/**
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition() = 0;

	/**
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const = 0;

	/**
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const = 0;

	/**
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink() = 0;

	/**
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const = 0;

	/**
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const = 0;

	/**
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource() = 0;

	/**
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const = 0;

	/**
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const = 0;

	/**
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get audioChannelFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr GetaudioChannelFormatIDRef() const = 0;

	/**
	 * Set audioChannelFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioChannelFormatIDRef(const EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioPackFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr GetaudioPackFormatIDRef() const = 0;

	/**
	 * Set audioPackFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormatIDRef(const EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get audioTrackFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr GetaudioTrackFormatIDRef() const = 0;

	/**
	 * Set audioTrackFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackFormatIDRef(const EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioStreamFormatType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormatIDRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioStreamFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4369<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4369.<br>
 */
class DC1_EXPORT EbuCoreaudioStreamFormatType :
		public IEbuCoreaudioStreamFormatType
{
	friend class EbuCoreaudioStreamFormatTypeFactory; // constructs objects

public:
	/*
	 * Get audioStreamFormatID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioStreamFormatID() const;

	/*
	 * Set audioStreamFormatID attribute.
	 * @param item XMLCh *
	 */
	// Mandatory
	virtual void SetaudioStreamFormatID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioStreamFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioStreamFormatName() const;

	/*
	 * Get audioStreamFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioStreamFormatName() const;

	/*
	 * Set audioStreamFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioStreamFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioStreamFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioStreamFormatName();

	/*
	 * Get formatLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLabel() const;

	/*
	 * Get formatLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLabel() const;

	/*
	 * Set formatLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLabel();

	/*
	 * Get formatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatDefinition() const;

	/*
	 * Get formatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatDefinition() const;

	/*
	 * Set formatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatDefinition();

	/*
	 * Get formatLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLink() const;

	/*
	 * Get formatLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLink() const;

	/*
	 * Set formatLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLink();

	/*
	 * Get formatSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatSource() const;

	/*
	 * Get formatSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatSource() const;

	/*
	 * Set formatSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatSource();

	/*
	 * Get formatLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetformatLanguage() const;

	/*
	 * Get formatLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistformatLanguage() const;

	/*
	 * Set formatLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetformatLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate formatLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateformatLanguage();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get audioChannelFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr GetaudioChannelFormatIDRef() const;

	/*
	 * Set audioChannelFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioChannelFormatIDRef(const EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioPackFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr GetaudioPackFormatIDRef() const;

	/*
	 * Set audioPackFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioPackFormatIDRef(const EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get audioTrackFormatIDRef element.
	 * @return EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr
	 */
	virtual EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr GetaudioTrackFormatIDRef() const;

	/*
	 * Set audioTrackFormatIDRef element.
	 * @param item const EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaudioTrackFormatIDRef(const EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioStreamFormatType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioChannelFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioPackFormatIDRef</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:audioTrackFormatIDRef</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioStreamFormatType();

protected:
	EbuCoreaudioStreamFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_audioStreamFormatID;
	XMLCh * m_audioStreamFormatName;
	bool m_audioStreamFormatName_Exist;
	XMLCh * m_formatLabel;
	bool m_formatLabel_Exist;
	XMLCh * m_formatDefinition;
	bool m_formatDefinition_Exist;
	XMLCh * m_formatLink;
	bool m_formatLink_Exist;
	XMLCh * m_formatSource;
	bool m_formatSource_Exist;
	XMLCh * m_formatLanguage;
	bool m_formatLanguage_Exist;


	EbuCoreaudioStreamFormatType_audioChannelFormatIDRef_CollectionPtr m_audioChannelFormatIDRef;
	EbuCoreaudioStreamFormatType_audioPackFormatIDRef_CollectionPtr m_audioPackFormatIDRef;
	EbuCoreaudioStreamFormatType_audioTrackFormatIDRef_CollectionPtr m_audioTrackFormatIDRef;

// no includefile for extension defined 
// file EbuCoreaudioStreamFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _177EBUCOREAUDIOSTREAMFORMATTYPE_H

