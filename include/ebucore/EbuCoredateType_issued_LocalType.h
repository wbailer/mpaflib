/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _341EBUCOREDATETYPE_ISSUED_LOCALTYPE_H
#define _341EBUCOREDATETYPE_ISSUED_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCoredateType_issued_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoredateType_issued_LocalType_ExtInclude.h


class IEbuCoredateType_issued_LocalType;
typedef Dc1Ptr< IEbuCoredateType_issued_LocalType> EbuCoredateType_issued_LocalPtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IW3Ctime;
typedef Dc1Ptr< IW3Ctime > W3CtimePtr;

/** 
 * Generated interface IEbuCoredateType_issued_LocalType for class EbuCoredateType_issued_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1482<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoredateType_issued_LocalType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoredateType_issued_LocalType();
	virtual ~IEbuCoredateType_issued_LocalType();
	/**
	 * Get startYear attribute.
	 * @return int
	 */
	virtual int GetstartYear() const = 0;

	/**
	 * Get startYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartYear() const = 0;

	/**
	 * Set startYear attribute.
	 * @param item int
	 */
	virtual void SetstartYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartYear() = 0;

	/**
	 * Get startDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetstartDate() const = 0;

	/**
	 * Get startDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartDate() const = 0;

	/**
	 * Set startDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetstartDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartDate() = 0;

	/**
	 * Get startTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetstartTime() const = 0;

	/**
	 * Get startTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartTime() const = 0;

	/**
	 * Set startTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetstartTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate startTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartTime() = 0;

	/**
	 * Get endYear attribute.
	 * @return int
	 */
	virtual int GetendYear() const = 0;

	/**
	 * Get endYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendYear() const = 0;

	/**
	 * Set endYear attribute.
	 * @param item int
	 */
	virtual void SetendYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendYear() = 0;

	/**
	 * Get endDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetendDate() const = 0;

	/**
	 * Get endDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendDate() const = 0;

	/**
	 * Set endDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetendDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendDate() = 0;

	/**
	 * Get endTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetendTime() const = 0;

	/**
	 * Get endTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendTime() const = 0;

	/**
	 * Set endTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetendTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate endTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendTime() = 0;

	/**
	 * Get period attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getperiod() const = 0;

	/**
	 * Get period attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existperiod() const = 0;

	/**
	 * Set period attribute.
	 * @param item XMLCh *
	 */
	virtual void Setperiod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate period attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateperiod() = 0;

	/**
	 * Gets or creates Dc1NodePtr child elements of dateType_issued_LocalType.
	 * Currently just supports rudimentary XPath expressions as dateType_issued_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoredateType_issued_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoredateType_issued_LocalType_ExtPropDef.h

};




/*
 * Generated class EbuCoredateType_issued_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 1482<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 1482.<br>
 */
class DC1_EXPORT EbuCoredateType_issued_LocalType :
		public IEbuCoredateType_issued_LocalType
{
	friend class EbuCoredateType_issued_LocalTypeFactory; // constructs objects

public:
	/*
	 * Get startYear attribute.
	 * @return int
	 */
	virtual int GetstartYear() const;

	/*
	 * Get startYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartYear() const;

	/*
	 * Set startYear attribute.
	 * @param item int
	 */
	virtual void SetstartYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartYear();

	/*
	 * Get startDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetstartDate() const;

	/*
	 * Get startDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartDate() const;

	/*
	 * Set startDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetstartDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartDate();

	/*
	 * Get startTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetstartTime() const;

	/*
	 * Get startTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExiststartTime() const;

	/*
	 * Set startTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetstartTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate startTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatestartTime();

	/*
	 * Get endYear attribute.
	 * @return int
	 */
	virtual int GetendYear() const;

	/*
	 * Get endYear attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendYear() const;

	/*
	 * Set endYear attribute.
	 * @param item int
	 */
	virtual void SetendYear(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endYear attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendYear();

	/*
	 * Get endDate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetendDate() const;

	/*
	 * Get endDate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendDate() const;

	/*
	 * Set endDate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetendDate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endDate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendDate();

	/*
	 * Get endTime attribute.
	 * @return W3CtimePtr
	 */
	virtual W3CtimePtr GetendTime() const;

	/*
	 * Get endTime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistendTime() const;

	/*
	 * Set endTime attribute.
	 * @param item const W3CtimePtr &
	 */
	virtual void SetendTime(const W3CtimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate endTime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateendTime();

	/*
	 * Get period attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getperiod() const;

	/*
	 * Get period attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existperiod() const;

	/*
	 * Set period attribute.
	 * @param item XMLCh *
	 */
	virtual void Setperiod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate period attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateperiod();

	/*
	 * Gets or creates Dc1NodePtr child elements of dateType_issued_LocalType.
	 * Currently just supports rudimentary XPath expressions as dateType_issued_LocalType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoredateType_issued_LocalType_ExtMyMethodDef.h


public:

	virtual ~EbuCoredateType_issued_LocalType();

protected:
	EbuCoredateType_issued_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_startYear;
	bool m_startYear_Exist;
	W3CdatePtr m_startDate;
	bool m_startDate_Exist;
	W3CtimePtr m_startTime;
	bool m_startTime_Exist;
	int m_endYear;
	bool m_endYear_Exist;
	W3CdatePtr m_endDate;
	bool m_endDate_Exist;
	W3CtimePtr m_endTime;
	bool m_endTime_Exist;
	XMLCh * m_period;
	bool m_period_Exist;



// no includefile for extension defined 
// file EbuCoredateType_issued_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _341EBUCOREDATETYPE_ISSUED_LOCALTYPE_H

