/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#ifndef _155EBUCOREAUDIOFORMATTYPE_SAMPLETYPE_LOCALTYPE_H
#define _155EBUCOREAUDIOFORMATTYPE_SAMPLETYPE_LOCALTYPE_H


#include <xercesc/util/XMLString.hpp>
#include "Dc1Defines.h"

// no includefile for extension defined 
// file EbuCoreaudioFormatType_sampleType_LocalType_ExtInclude.h




/**
 * Generated enumeration EbuCoreaudioFormatType_sampleType_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3145<br>
 * Enumerations: <br>
 * _float (mapped to XML float) <br>
 * integer (mapped to XML integer) <br>
 */
class DC1_EXPORT EbuCoreaudioFormatType_sampleType_LocalType
{
public:
	/** EbuCoreaudioFormatType_sampleType_LocalType enumeration */
	enum Enumeration
	{
		UninitializedEnumeration, 
		_float, // Mapped to float
		integer, // Mapped to integer
	};
	

// no includefile for extension defined 
// file EbuCoreaudioFormatType_sampleType_LocalType_ExtMethodDef.h


	static XMLCh * ToText(EbuCoreaudioFormatType_sampleType_LocalType::Enumeration item);
	static EbuCoreaudioFormatType_sampleType_LocalType::Enumeration Parse(const XMLCh * const txt);

  static const XMLCh * nsURI(); // For namespace handling

// no includefile for extension defined 
// file EbuCoreaudioFormatType_sampleType_LocalType_ExtPropDef.h



};
#endif // _155EBUCOREAUDIOFORMATTYPE_SAMPLETYPE_LOCALTYPE_H
