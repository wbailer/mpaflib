/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _103EBUCOREAUDIOBLOCKFORMATTYPE_H
#define _103EBUCOREAUDIOBLOCKFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaudioBlockFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtInclude.h


class IEbuCoreaudioBlockFormatType;
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType> EbuCoreaudioBlockFormatPtr;
class IEbuCoretimecodeType;
typedef Dc1Ptr< IEbuCoretimecodeType > EbuCoretimecodePtr;
class IEbuCoreaudioBlockFormatType_speakerLabel_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType_speakerLabel_CollectionType > EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr;
class IEbuCoreaudioBlockFormatType_position_CollectionType;
typedef Dc1Ptr< IEbuCoreaudioBlockFormatType_position_CollectionType > EbuCoreaudioBlockFormatType_position_CollectionPtr;
class IEbuCorematrixType;
typedef Dc1Ptr< IEbuCorematrixType > EbuCorematrixPtr;

/** 
 * Generated interface IEbuCoreaudioBlockFormatType for class EbuCoreaudioBlockFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4217<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaudioBlockFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaudioBlockFormatType();
	virtual ~IEbuCoreaudioBlockFormatType();
	/**
	 * Get audioBlockFormatID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioBlockFormatID() const = 0;

	/**
	 * Get audioBlockFormatID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioBlockFormatID() const = 0;

	/**
	 * Set audioBlockFormatID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioBlockFormatID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate audioBlockFormatID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioBlockFormatID() = 0;

	/**
	 * Get rtime attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getrtime() const = 0;

	/**
	 * Get rtime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrtime() const = 0;

	/**
	 * Set rtime attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setrtime(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate rtime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatertime() = 0;

	/**
	 * Get duration attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getduration() const = 0;

	/**
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const = 0;

	/**
	 * Set duration attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get speakerLabel element.
	 * @return EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr
	 */
	virtual EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr GetspeakerLabel() const = 0;

	/**
	 * Set speakerLabel element.
	 * @param item const EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetspeakerLabel(const EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get position element.
	 * @return EbuCoreaudioBlockFormatType_position_CollectionPtr
	 */
	virtual EbuCoreaudioBlockFormatType_position_CollectionPtr Getposition() const = 0;

	/**
	 * Set position element.
	 * @param item const EbuCoreaudioBlockFormatType_position_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setposition(const EbuCoreaudioBlockFormatType_position_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get matrix element.
	 * @return EbuCorematrixPtr
	 */
	virtual EbuCorematrixPtr Getmatrix() const = 0;

	virtual bool IsValidmatrix() const = 0;

	/**
	 * Set matrix element.
	 * @param item const EbuCorematrixPtr &
	 */
	virtual void Setmatrix(const EbuCorematrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate matrix element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatematrix() = 0;

	/**
	 * Get gain element.
	 * @return float
	 */
	virtual float Getgain() const = 0;

	virtual bool IsValidgain() const = 0;

	/**
	 * Set gain element.
	 * @param item float
	 */
	virtual void Setgain(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate gain element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidategain() = 0;

	/**
	 * Get diffuse element.
	 * @return bool
	 */
	virtual bool Getdiffuse() const = 0;

	virtual bool IsValiddiffuse() const = 0;

	/**
	 * Set diffuse element.
	 * @param item bool
	 */
	virtual void Setdiffuse(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate diffuse element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatediffuse() = 0;

	/**
	 * Get width element.
	 * @return float
	 */
	virtual float Getwidth() const = 0;

	virtual bool IsValidwidth() const = 0;

	/**
	 * Set width element.
	 * @param item float
	 */
	virtual void Setwidth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate width element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatewidth() = 0;

	/**
	 * Get height element.
	 * @return float
	 */
	virtual float Getheight() const = 0;

	virtual bool IsValidheight() const = 0;

	/**
	 * Set height element.
	 * @param item float
	 */
	virtual void Setheight(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate height element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateheight() = 0;

	/**
	 * Get depth element.
	 * @return float
	 */
	virtual float Getdepth() const = 0;

	virtual bool IsValiddepth() const = 0;

	/**
	 * Set depth element.
	 * @param item float
	 */
	virtual void Setdepth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate depth element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepth() = 0;

	/**
	 * Get channelLock element.
	 * @return bool
	 */
	virtual bool GetchannelLock() const = 0;

	virtual bool IsValidchannelLock() const = 0;

	/**
	 * Set channelLock element.
	 * @param item bool
	 */
	virtual void SetchannelLock(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate channelLock element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatechannelLock() = 0;

	/**
	 * Get jumpPosition element.
	 * @return bool
	 */
	virtual bool GetjumpPosition() const = 0;

	virtual bool IsValidjumpPosition() const = 0;

	/**
	 * Set jumpPosition element.
	 * @param item bool
	 */
	virtual void SetjumpPosition(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate jumpPosition element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatejumpPosition() = 0;

	/**
	 * Get equation element.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getequation() const = 0;

	virtual bool IsValidequation() const = 0;

	/**
	 * Set equation element.
	 * @param item XMLCh *
	 */
	virtual void Setequation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate equation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequation() = 0;

	/**
	 * Get degree element.
	 * @return int
	 */
	virtual int Getdegree() const = 0;

	virtual bool IsValiddegree() const = 0;

	/**
	 * Set degree element.
	 * @param item int
	 */
	virtual void Setdegree(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate degree element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedegree() = 0;

	/**
	 * Get order element.
	 * @return int
	 */
	virtual int Getorder() const = 0;

	virtual bool IsValidorder() const = 0;

	/**
	 * Set order element.
	 * @param item int
	 */
	virtual void Setorder(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate order element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateorder() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of audioBlockFormatType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:speakerLabel</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:position</li>
	 * <li>matrix</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaudioBlockFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4217<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4217.<br>
 */
class DC1_EXPORT EbuCoreaudioBlockFormatType :
		public IEbuCoreaudioBlockFormatType
{
	friend class EbuCoreaudioBlockFormatTypeFactory; // constructs objects

public:
	/*
	 * Get audioBlockFormatID attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaudioBlockFormatID() const;

	/*
	 * Get audioBlockFormatID attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistaudioBlockFormatID() const;

	/*
	 * Set audioBlockFormatID attribute.
	 * @param item XMLCh *
	 */
	virtual void SetaudioBlockFormatID(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate audioBlockFormatID attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaudioBlockFormatID();

	/*
	 * Get rtime attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getrtime() const;

	/*
	 * Get rtime attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existrtime() const;

	/*
	 * Set rtime attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setrtime(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate rtime attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatertime();

	/*
	 * Get duration attribute.
	 * @return EbuCoretimecodePtr
	 */
	virtual EbuCoretimecodePtr Getduration() const;

	/*
	 * Get duration attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existduration() const;

	/*
	 * Set duration attribute.
	 * @param item const EbuCoretimecodePtr &
	 */
	virtual void Setduration(const EbuCoretimecodePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate duration attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateduration();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get speakerLabel element.
	 * @return EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr
	 */
	virtual EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr GetspeakerLabel() const;

	/*
	 * Set speakerLabel element.
	 * @param item const EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetspeakerLabel(const EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get position element.
	 * @return EbuCoreaudioBlockFormatType_position_CollectionPtr
	 */
	virtual EbuCoreaudioBlockFormatType_position_CollectionPtr Getposition() const;

	/*
	 * Set position element.
	 * @param item const EbuCoreaudioBlockFormatType_position_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setposition(const EbuCoreaudioBlockFormatType_position_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get matrix element.
	 * @return EbuCorematrixPtr
	 */
	virtual EbuCorematrixPtr Getmatrix() const;

	virtual bool IsValidmatrix() const;

	/*
	 * Set matrix element.
	 * @param item const EbuCorematrixPtr &
	 */
	virtual void Setmatrix(const EbuCorematrixPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate matrix element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatematrix();

	/*
	 * Get gain element.
	 * @return float
	 */
	virtual float Getgain() const;

	virtual bool IsValidgain() const;

	/*
	 * Set gain element.
	 * @param item float
	 */
	virtual void Setgain(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate gain element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidategain();

	/*
	 * Get diffuse element.
	 * @return bool
	 */
	virtual bool Getdiffuse() const;

	virtual bool IsValiddiffuse() const;

	/*
	 * Set diffuse element.
	 * @param item bool
	 */
	virtual void Setdiffuse(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate diffuse element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatediffuse();

	/*
	 * Get width element.
	 * @return float
	 */
	virtual float Getwidth() const;

	virtual bool IsValidwidth() const;

	/*
	 * Set width element.
	 * @param item float
	 */
	virtual void Setwidth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate width element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatewidth();

	/*
	 * Get height element.
	 * @return float
	 */
	virtual float Getheight() const;

	virtual bool IsValidheight() const;

	/*
	 * Set height element.
	 * @param item float
	 */
	virtual void Setheight(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate height element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateheight();

	/*
	 * Get depth element.
	 * @return float
	 */
	virtual float Getdepth() const;

	virtual bool IsValiddepth() const;

	/*
	 * Set depth element.
	 * @param item float
	 */
	virtual void Setdepth(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate depth element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedepth();

	/*
	 * Get channelLock element.
	 * @return bool
	 */
	virtual bool GetchannelLock() const;

	virtual bool IsValidchannelLock() const;

	/*
	 * Set channelLock element.
	 * @param item bool
	 */
	virtual void SetchannelLock(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate channelLock element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatechannelLock();

	/*
	 * Get jumpPosition element.
	 * @return bool
	 */
	virtual bool GetjumpPosition() const;

	virtual bool IsValidjumpPosition() const;

	/*
	 * Set jumpPosition element.
	 * @param item bool
	 */
	virtual void SetjumpPosition(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate jumpPosition element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatejumpPosition();

	/*
	 * Get equation element.
	 * @return XMLCh *
	 */
	virtual XMLCh * Getequation() const;

	virtual bool IsValidequation() const;

	/*
	 * Set equation element.
	 * @param item XMLCh *
	 */
	virtual void Setequation(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate equation element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateequation();

	/*
	 * Get degree element.
	 * @return int
	 */
	virtual int Getdegree() const;

	virtual bool IsValiddegree() const;

	/*
	 * Set degree element.
	 * @param item int
	 */
	virtual void Setdegree(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate degree element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatedegree();

	/*
	 * Get order element.
	 * @return int
	 */
	virtual int Getorder() const;

	virtual bool IsValidorder() const;

	/*
	 * Set order element.
	 * @param item int
	 */
	virtual void Setorder(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate order element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidateorder();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of audioBlockFormatType.
	 * Currently this type contains 3 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:speakerLabel</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:position</li>
	 * <li>matrix</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaudioBlockFormatType();

protected:
	EbuCoreaudioBlockFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_audioBlockFormatID;
	bool m_audioBlockFormatID_Exist;
	EbuCoretimecodePtr m_rtime;
	bool m_rtime_Exist;
	EbuCoretimecodePtr m_duration;
	bool m_duration_Exist;


	EbuCoreaudioBlockFormatType_speakerLabel_CollectionPtr m_speakerLabel;
	EbuCoreaudioBlockFormatType_position_CollectionPtr m_position;
	EbuCorematrixPtr m_matrix;
	bool m_matrix_Exist; // For optional elements 
	float m_gain;
	bool m_gain_Exist; // For optional elements 
	bool m_diffuse;
	bool m_diffuse_Exist; // For optional elements 
	float m_width;
	bool m_width_Exist; // For optional elements 
	float m_height;
	bool m_height_Exist; // For optional elements 
	float m_depth;
	bool m_depth_Exist; // For optional elements 
	bool m_channelLock;
	bool m_channelLock_Exist; // For optional elements 
	bool m_jumpPosition;
	bool m_jumpPosition_Exist; // For optional elements 
	XMLCh * m_equation;
	bool m_equation_Exist; // For optional elements 
	int m_degree;
	bool m_degree_Exist; // For optional elements 
	int m_order;
	bool m_order_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreaudioBlockFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _103EBUCOREAUDIOBLOCKFORMATTYPE_H

