/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _85EBUCOREADDRESSTYPE_H
#define _85EBUCOREADDRESSTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoreaddressTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreaddressType_ExtInclude.h


class IEbuCoreaddressType;
typedef Dc1Ptr< IEbuCoreaddressType> EbuCoreaddressPtr;
class IEbuCoreaddressType_addressLine_CollectionType;
typedef Dc1Ptr< IEbuCoreaddressType_addressLine_CollectionType > EbuCoreaddressType_addressLine_CollectionPtr;
class IEbuCoreelementType;
typedef Dc1Ptr< IEbuCoreelementType > EbuCoreelementPtr;
class IEbuCoreaddressType_country_LocalType;
typedef Dc1Ptr< IEbuCoreaddressType_country_LocalType > EbuCoreaddressType_country_LocalPtr;

/** 
 * Generated interface IEbuCoreaddressType for class EbuCoreaddressType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2415<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreaddressType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreaddressType();
	virtual ~IEbuCoreaddressType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get addressLine element.
	 * @return EbuCoreaddressType_addressLine_CollectionPtr
	 */
	virtual EbuCoreaddressType_addressLine_CollectionPtr GetaddressLine() const = 0;

	/**
	 * Set addressLine element.
	 * @param item const EbuCoreaddressType_addressLine_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaddressLine(const EbuCoreaddressType_addressLine_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get addressTownCity element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetaddressTownCity() const = 0;

	virtual bool IsValidaddressTownCity() const = 0;

	/**
	 * Set addressTownCity element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetaddressTownCity(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate addressTownCity element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressTownCity() = 0;

	/**
	 * Get addressCountyState element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetaddressCountyState() const = 0;

	virtual bool IsValidaddressCountyState() const = 0;

	/**
	 * Set addressCountyState element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetaddressCountyState(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate addressCountyState element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressCountyState() = 0;

	/**
	 * Get addressDeliveryCode element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaddressDeliveryCode() const = 0;

	virtual bool IsValidaddressDeliveryCode() const = 0;

	/**
	 * Set addressDeliveryCode element.
	 * @param item XMLCh *
	 */
	virtual void SetaddressDeliveryCode(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate addressDeliveryCode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressDeliveryCode() = 0;

	/**
	 * Get country element.
	 * @return EbuCoreaddressType_country_LocalPtr
	 */
	virtual EbuCoreaddressType_country_LocalPtr Getcountry() const = 0;

	virtual bool IsValidcountry() const = 0;

	/**
	 * Set country element.
	 * @param item const EbuCoreaddressType_country_LocalPtr &
	 */
	virtual void Setcountry(const EbuCoreaddressType_country_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate country element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecountry() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of addressType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:addressLine</li>
	 * <li>addressTownCity</li>
	 * <li>addressCountyState</li>
	 * <li>country</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreaddressType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreaddressType_ExtPropDef.h

};




/*
 * Generated class EbuCoreaddressType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2415<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 2415.<br>
 */
class DC1_EXPORT EbuCoreaddressType :
		public IEbuCoreaddressType
{
	friend class EbuCoreaddressTypeFactory; // constructs objects

public:
		/* @name Sequence

		 */
		//@{
	/*
	 * Get addressLine element.
	 * @return EbuCoreaddressType_addressLine_CollectionPtr
	 */
	virtual EbuCoreaddressType_addressLine_CollectionPtr GetaddressLine() const;

	/*
	 * Set addressLine element.
	 * @param item const EbuCoreaddressType_addressLine_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetaddressLine(const EbuCoreaddressType_addressLine_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get addressTownCity element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetaddressTownCity() const;

	virtual bool IsValidaddressTownCity() const;

	/*
	 * Set addressTownCity element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetaddressTownCity(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate addressTownCity element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressTownCity();

	/*
	 * Get addressCountyState element.
	 * @return EbuCoreelementPtr
	 */
	virtual EbuCoreelementPtr GetaddressCountyState() const;

	virtual bool IsValidaddressCountyState() const;

	/*
	 * Set addressCountyState element.
	 * @param item const EbuCoreelementPtr &
	 */
	virtual void SetaddressCountyState(const EbuCoreelementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate addressCountyState element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressCountyState();

	/*
	 * Get addressDeliveryCode element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetaddressDeliveryCode() const;

	virtual bool IsValidaddressDeliveryCode() const;

	/*
	 * Set addressDeliveryCode element.
	 * @param item XMLCh *
	 */
	virtual void SetaddressDeliveryCode(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate addressDeliveryCode element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateaddressDeliveryCode();

	/*
	 * Get country element.
	 * @return EbuCoreaddressType_country_LocalPtr
	 */
	virtual EbuCoreaddressType_country_LocalPtr Getcountry() const;

	virtual bool IsValidcountry() const;

	/*
	 * Set country element.
	 * @param item const EbuCoreaddressType_country_LocalPtr &
	 */
	virtual void Setcountry(const EbuCoreaddressType_country_LocalPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate country element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecountry();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of addressType.
	 * Currently this type contains 4 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:addressLine</li>
	 * <li>addressTownCity</li>
	 * <li>addressCountyState</li>
	 * <li>country</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreaddressType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreaddressType();

protected:
	EbuCoreaddressType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:


	EbuCoreaddressType_addressLine_CollectionPtr m_addressLine;
	EbuCoreelementPtr m_addressTownCity;
	bool m_addressTownCity_Exist; // For optional elements 
	EbuCoreelementPtr m_addressCountyState;
	bool m_addressCountyState_Exist; // For optional elements 
	XMLCh * m_addressDeliveryCode;
	bool m_addressDeliveryCode_Exist; // For optional elements 
	EbuCoreaddressType_country_LocalPtr m_country;
	bool m_country_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreaddressType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _85EBUCOREADDRESSTYPE_H

