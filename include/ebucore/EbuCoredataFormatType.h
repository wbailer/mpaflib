/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _319EBUCOREDATAFORMATTYPE_H
#define _319EBUCOREDATAFORMATTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCoredataFormatTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoredataFormatType_ExtInclude.h


class IEbuCoredataFormatType;
typedef Dc1Ptr< IEbuCoredataFormatType> EbuCoredataFormatPtr;
class IEbuCoredataFormatType_captioningFormat_CollectionType;
typedef Dc1Ptr< IEbuCoredataFormatType_captioningFormat_CollectionType > EbuCoredataFormatType_captioningFormat_CollectionPtr;
class IEbuCoredataFormatType_subtitlingFormat_CollectionType;
typedef Dc1Ptr< IEbuCoredataFormatType_subtitlingFormat_CollectionType > EbuCoredataFormatType_subtitlingFormat_CollectionPtr;
class IEbuCoredataFormatType_ancillaryDataFormat_CollectionType;
typedef Dc1Ptr< IEbuCoredataFormatType_ancillaryDataFormat_CollectionType > EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr;
class IEbuCorecodecType;
typedef Dc1Ptr< IEbuCorecodecType > EbuCorecodecPtr;
class IEbuCoretechnicalAttributes;
typedef Dc1Ptr< IEbuCoretechnicalAttributes > EbuCoretechnicalAttributesPtr;
class IEbuCoredataFormatType_comment_CollectionType;
typedef Dc1Ptr< IEbuCoredataFormatType_comment_CollectionType > EbuCoredataFormatType_comment_CollectionPtr;

/** 
 * Generated interface IEbuCoredataFormatType for class EbuCoredataFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3250<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoredataFormatType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoredataFormatType();
	virtual ~IEbuCoredataFormatType();
	/**
	 * Get dataFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatId() const = 0;

	/**
	 * Get dataFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatId() const = 0;

	/**
	 * Set dataFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatId() = 0;

	/**
	 * Get dataFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatVersionId() const = 0;

	/**
	 * Get dataFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatVersionId() const = 0;

	/**
	 * Set dataFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatVersionId() = 0;

	/**
	 * Get dataFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatName() const = 0;

	/**
	 * Get dataFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatName() const = 0;

	/**
	 * Set dataFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatName() = 0;

	/**
	 * Get dataFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatDefinition() const = 0;

	/**
	 * Get dataFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatDefinition() const = 0;

	/**
	 * Set dataFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatDefinition() = 0;

	/**
	 * Get dataTrackId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackId() const = 0;

	/**
	 * Get dataTrackId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackId() const = 0;

	/**
	 * Set dataTrackId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataTrackId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackId() = 0;

	/**
	 * Get dataTrackName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackName() const = 0;

	/**
	 * Get dataTrackName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackName() const = 0;

	/**
	 * Set dataTrackName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataTrackName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackName() = 0;

	/**
	 * Get dataTrackLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackLanguage() const = 0;

	/**
	 * Get dataTrackLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackLanguage() const = 0;

	/**
	 * Set dataTrackLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataTrackLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackLanguage() = 0;

	/**
	 * Get dataPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetdataPresenceFlag() const = 0;

	/**
	 * Get dataPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataPresenceFlag() const = 0;

	/**
	 * Set dataPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetdataPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate dataPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataPresenceFlag() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get captioningFormat element.
	 * @return EbuCoredataFormatType_captioningFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_captioningFormat_CollectionPtr GetcaptioningFormat() const = 0;

	/**
	 * Set captioningFormat element.
	 * @param item const EbuCoredataFormatType_captioningFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetcaptioningFormat(const EbuCoredataFormatType_captioningFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get subtitlingFormat element.
	 * @return EbuCoredataFormatType_subtitlingFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_subtitlingFormat_CollectionPtr GetsubtitlingFormat() const = 0;

	/**
	 * Set subtitlingFormat element.
	 * @param item const EbuCoredataFormatType_subtitlingFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetsubtitlingFormat(const EbuCoredataFormatType_subtitlingFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get ancillaryDataFormat element.
	 * @return EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr GetancillaryDataFormat() const = 0;

	/**
	 * Set ancillaryDataFormat element.
	 * @param item const EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetancillaryDataFormat(const EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const = 0;

	virtual bool IsValidcodec() const = 0;

	/**
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec() = 0;

	/**
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const = 0;

	/**
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get comment element.
	 * @return EbuCoredataFormatType_comment_CollectionPtr
	 */
	virtual EbuCoredataFormatType_comment_CollectionPtr Getcomment() const = 0;

	/**
	 * Set comment element.
	 * @param item const EbuCoredataFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCoredataFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of dataFormatType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:captioningFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormat</li>
	 * <li>codec</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoredataFormatType_ExtPropDef.h

};




/*
 * Generated class EbuCoredataFormatType<br>
 * Located at: EBU_CORE_20140318.xsd, line 3250<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 3250.<br>
 */
class DC1_EXPORT EbuCoredataFormatType :
		public IEbuCoredataFormatType
{
	friend class EbuCoredataFormatTypeFactory; // constructs objects

public:
	/*
	 * Get dataFormatId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatId() const;

	/*
	 * Get dataFormatId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatId() const;

	/*
	 * Set dataFormatId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataFormatId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatId();

	/*
	 * Get dataFormatVersionId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatVersionId() const;

	/*
	 * Get dataFormatVersionId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatVersionId() const;

	/*
	 * Set dataFormatVersionId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatVersionId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataFormatVersionId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatVersionId();

	/*
	 * Get dataFormatName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatName() const;

	/*
	 * Get dataFormatName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatName() const;

	/*
	 * Set dataFormatName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataFormatName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatName();

	/*
	 * Get dataFormatDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataFormatDefinition() const;

	/*
	 * Get dataFormatDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataFormatDefinition() const;

	/*
	 * Set dataFormatDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataFormatDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataFormatDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataFormatDefinition();

	/*
	 * Get dataTrackId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackId() const;

	/*
	 * Get dataTrackId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackId() const;

	/*
	 * Set dataTrackId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataTrackId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackId();

	/*
	 * Get dataTrackName attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackName() const;

	/*
	 * Get dataTrackName attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackName() const;

	/*
	 * Set dataTrackName attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackName(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataTrackName attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackName();

	/*
	 * Get dataTrackLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdataTrackLanguage() const;

	/*
	 * Get dataTrackLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataTrackLanguage() const;

	/*
	 * Set dataTrackLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SetdataTrackLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataTrackLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataTrackLanguage();

	/*
	 * Get dataPresenceFlag attribute.
	 * @return bool
	 */
	virtual bool GetdataPresenceFlag() const;

	/*
	 * Get dataPresenceFlag attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistdataPresenceFlag() const;

	/*
	 * Set dataPresenceFlag attribute.
	 * @param item bool
	 */
	virtual void SetdataPresenceFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate dataPresenceFlag attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedataPresenceFlag();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get captioningFormat element.
	 * @return EbuCoredataFormatType_captioningFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_captioningFormat_CollectionPtr GetcaptioningFormat() const;

	/*
	 * Set captioningFormat element.
	 * @param item const EbuCoredataFormatType_captioningFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetcaptioningFormat(const EbuCoredataFormatType_captioningFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get subtitlingFormat element.
	 * @return EbuCoredataFormatType_subtitlingFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_subtitlingFormat_CollectionPtr GetsubtitlingFormat() const;

	/*
	 * Set subtitlingFormat element.
	 * @param item const EbuCoredataFormatType_subtitlingFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetsubtitlingFormat(const EbuCoredataFormatType_subtitlingFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get ancillaryDataFormat element.
	 * @return EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr
	 */
	virtual EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr GetancillaryDataFormat() const;

	/*
	 * Set ancillaryDataFormat element.
	 * @param item const EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetancillaryDataFormat(const EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get codec element.
	 * @return EbuCorecodecPtr
	 */
	virtual EbuCorecodecPtr Getcodec() const;

	virtual bool IsValidcodec() const;

	/*
	 * Set codec element.
	 * @param item const EbuCorecodecPtr &
	 */
	virtual void Setcodec(const EbuCorecodecPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate codec element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatecodec();

	/*
	 * Get technicalAttributes element.
	 * @return EbuCoretechnicalAttributesPtr
	 */
	virtual EbuCoretechnicalAttributesPtr GettechnicalAttributes() const;

	/*
	 * Set technicalAttributes element.
	 * @param item const EbuCoretechnicalAttributesPtr &
	 */
	// Mandatory			
	virtual void SettechnicalAttributes(const EbuCoretechnicalAttributesPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get comment element.
	 * @return EbuCoredataFormatType_comment_CollectionPtr
	 */
	virtual EbuCoredataFormatType_comment_CollectionPtr Getcomment() const;

	/*
	 * Set comment element.
	 * @param item const EbuCoredataFormatType_comment_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcomment(const EbuCoredataFormatType_comment_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of dataFormatType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:captioningFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:subtitlingFormat</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:ancillaryDataFormat</li>
	 * <li>codec</li>
	 * <li>technicalAttributes</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:comment</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMyMethodDef.h


public:

	virtual ~EbuCoredataFormatType();

protected:
	EbuCoredataFormatType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_dataFormatId;
	bool m_dataFormatId_Exist;
	XMLCh * m_dataFormatVersionId;
	bool m_dataFormatVersionId_Exist;
	XMLCh * m_dataFormatName;
	bool m_dataFormatName_Exist;
	XMLCh * m_dataFormatDefinition;
	bool m_dataFormatDefinition_Exist;
	XMLCh * m_dataTrackId;
	bool m_dataTrackId_Exist;
	XMLCh * m_dataTrackName;
	bool m_dataTrackName_Exist;
	XMLCh * m_dataTrackLanguage;
	bool m_dataTrackLanguage_Exist;
	bool m_dataPresenceFlag;
	bool m_dataPresenceFlag_Exist;


	EbuCoredataFormatType_captioningFormat_CollectionPtr m_captioningFormat;
	EbuCoredataFormatType_subtitlingFormat_CollectionPtr m_subtitlingFormat;
	EbuCoredataFormatType_ancillaryDataFormat_CollectionPtr m_ancillaryDataFormat;
	EbuCorecodecPtr m_codec;
	bool m_codec_Exist; // For optional elements 
	EbuCoretechnicalAttributesPtr m_technicalAttributes;
	EbuCoredataFormatType_comment_CollectionPtr m_comment;

// no includefile for extension defined 
// file EbuCoredataFormatType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _319EBUCOREDATAFORMATTYPE_H

