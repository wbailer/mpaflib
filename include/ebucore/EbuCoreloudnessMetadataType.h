/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _447EBUCORELOUDNESSMETADATATYPE_H
#define _447EBUCORELOUDNESSMETADATATYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"

#include "EbuCoreloudnessMetadataTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreloudnessMetadataType_ExtInclude.h


class IEbuCoreloudnessMetadataType;
typedef Dc1Ptr< IEbuCoreloudnessMetadataType> EbuCoreloudnessMetadataPtr;

/** 
 * Generated interface IEbuCoreloudnessMetadataType for class EbuCoreloudnessMetadataType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4551<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreloudnessMetadataType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreloudnessMetadataType();
	virtual ~IEbuCoreloudnessMetadataType();
	/**
	 * Get loudnessMethod attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetloudnessMethod() const = 0;

	/**
	 * Get loudnessMethod attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistloudnessMethod() const = 0;

	/**
	 * Set loudnessMethod attribute.
	 * @param item XMLCh *
	 */
	virtual void SetloudnessMethod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate loudnessMethod attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateloudnessMethod() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get integratedLoudness element.
	 * @return float
	 */
	virtual float GetintegratedLoudness() const = 0;

	virtual bool IsValidintegratedLoudness() const = 0;

	/**
	 * Set integratedLoudness element.
	 * @param item float
	 */
	virtual void SetintegratedLoudness(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate integratedLoudness element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateintegratedLoudness() = 0;

	/**
	 * Get loudnessRange element.
	 * @return float
	 */
	virtual float GetloudnessRange() const = 0;

	virtual bool IsValidloudnessRange() const = 0;

	/**
	 * Set loudnessRange element.
	 * @param item float
	 */
	virtual void SetloudnessRange(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate loudnessRange element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateloudnessRange() = 0;

	/**
	 * Get maxTruePeak element.
	 * @return float
	 */
	virtual float GetmaxTruePeak() const = 0;

	virtual bool IsValidmaxTruePeak() const = 0;

	/**
	 * Set maxTruePeak element.
	 * @param item float
	 */
	virtual void SetmaxTruePeak(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate maxTruePeak element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxTruePeak() = 0;

	/**
	 * Get maxMomentary element.
	 * @return float
	 */
	virtual float GetmaxMomentary() const = 0;

	virtual bool IsValidmaxMomentary() const = 0;

	/**
	 * Set maxMomentary element.
	 * @param item float
	 */
	virtual void SetmaxMomentary(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate maxMomentary element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxMomentary() = 0;

	/**
	 * Get maxShortTerm element.
	 * @return float
	 */
	virtual float GetmaxShortTerm() const = 0;

	virtual bool IsValidmaxShortTerm() const = 0;

	/**
	 * Set maxShortTerm element.
	 * @param item float
	 */
	virtual void SetmaxShortTerm(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate maxShortTerm element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxShortTerm() = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of loudnessMetadataType.
	 * Currently just supports rudimentary XPath expressions as loudnessMetadataType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreloudnessMetadataType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreloudnessMetadataType_ExtPropDef.h

};




/*
 * Generated class EbuCoreloudnessMetadataType<br>
 * Located at: EBU_CORE_20140318.xsd, line 4551<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 4551.<br>
 */
class DC1_EXPORT EbuCoreloudnessMetadataType :
		public IEbuCoreloudnessMetadataType
{
	friend class EbuCoreloudnessMetadataTypeFactory; // constructs objects

public:
	/*
	 * Get loudnessMethod attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetloudnessMethod() const;

	/*
	 * Get loudnessMethod attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistloudnessMethod() const;

	/*
	 * Set loudnessMethod attribute.
	 * @param item XMLCh *
	 */
	virtual void SetloudnessMethod(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate loudnessMethod attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateloudnessMethod();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get integratedLoudness element.
	 * @return float
	 */
	virtual float GetintegratedLoudness() const;

	virtual bool IsValidintegratedLoudness() const;

	/*
	 * Set integratedLoudness element.
	 * @param item float
	 */
	virtual void SetintegratedLoudness(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate integratedLoudness element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateintegratedLoudness();

	/*
	 * Get loudnessRange element.
	 * @return float
	 */
	virtual float GetloudnessRange() const;

	virtual bool IsValidloudnessRange() const;

	/*
	 * Set loudnessRange element.
	 * @param item float
	 */
	virtual void SetloudnessRange(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate loudnessRange element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateloudnessRange();

	/*
	 * Get maxTruePeak element.
	 * @return float
	 */
	virtual float GetmaxTruePeak() const;

	virtual bool IsValidmaxTruePeak() const;

	/*
	 * Set maxTruePeak element.
	 * @param item float
	 */
	virtual void SetmaxTruePeak(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate maxTruePeak element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxTruePeak();

	/*
	 * Get maxMomentary element.
	 * @return float
	 */
	virtual float GetmaxMomentary() const;

	virtual bool IsValidmaxMomentary() const;

	/*
	 * Set maxMomentary element.
	 * @param item float
	 */
	virtual void SetmaxMomentary(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate maxMomentary element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxMomentary();

	/*
	 * Get maxShortTerm element.
	 * @return float
	 */
	virtual float GetmaxShortTerm() const;

	virtual bool IsValidmaxShortTerm() const;

	/*
	 * Set maxShortTerm element.
	 * @param item float
	 */
	virtual void SetmaxShortTerm(float item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate maxShortTerm element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatemaxShortTerm();

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of loudnessMetadataType.
	 * Currently just supports rudimentary XPath expressions as loudnessMetadataType contains no child elements of type "Dc1NodePtr" at all.

	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreloudnessMetadataType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreloudnessMetadataType();

protected:
	EbuCoreloudnessMetadataType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_loudnessMethod;
	bool m_loudnessMethod_Exist;


	float m_integratedLoudness;
	bool m_integratedLoudness_Exist; // For optional elements 
	float m_loudnessRange;
	bool m_loudnessRange_Exist; // For optional elements 
	float m_maxTruePeak;
	bool m_maxTruePeak_Exist; // For optional elements 
	float m_maxMomentary;
	bool m_maxMomentary_Exist; // For optional elements 
	float m_maxShortTerm;
	bool m_maxShortTerm_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCoreloudnessMetadataType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _447EBUCORELOUDNESSMETADATATYPE_H

