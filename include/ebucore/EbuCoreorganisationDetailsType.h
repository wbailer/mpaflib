/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _455EBUCOREORGANISATIONDETAILSTYPE_H
#define _455EBUCOREORGANISATIONDETAILSTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"
#include "W3CFactoryDefines.h"

#include "EbuCoreorganisationDetailsTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtInclude.h


class IEbuCoreorganisationDetailsType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType> EbuCoreorganisationDetailsPtr;
class IW3Cdate;
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;
class IEbuCoreorganisationDetailsType_organisationName_CollectionType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_organisationName_CollectionType > EbuCoreorganisationDetailsType_organisationName_CollectionPtr;
class IEbuCoreorganisationDetailsType_organisationCode_CollectionType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_organisationCode_CollectionType > EbuCoreorganisationDetailsType_organisationCode_CollectionPtr;
class IEbuCoreorganisationDepartmentType;
typedef Dc1Ptr< IEbuCoreorganisationDepartmentType > EbuCoreorganisationDepartmentPtr;
class IEbuCoreorganisationDetailsType_details_CollectionType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_details_CollectionType > EbuCoreorganisationDetailsType_details_CollectionPtr;
class IEbuCoreorganisationDetailsType_relatedInformationLink_CollectionType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_relatedInformationLink_CollectionType > EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr;
class IEbuCoreorganisationDetailsType_contacts_CollectionType;
typedef Dc1Ptr< IEbuCoreorganisationDetailsType_contacts_CollectionType > EbuCoreorganisationDetailsType_contacts_CollectionPtr;

/** 
 * Generated interface IEbuCoreorganisationDetailsType for class EbuCoreorganisationDetailsType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2278<br>
 * Classified: Class<br>
 */
class EBUCORE_EXPORT IEbuCoreorganisationDetailsType 
 :
		public Dc1Node
{
public:
	// TODO: make these protected?
	IEbuCoreorganisationDetailsType();
	virtual ~IEbuCoreorganisationDetailsType();
	/**
	 * Get organisationId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetorganisationId() const = 0;

	/**
	 * Get organisationId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistorganisationId() const = 0;

	/**
	 * Set organisationId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetorganisationId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate organisationId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorganisationId() = 0;

	/**
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get linkToLogo attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetlinkToLogo() const = 0;

	/**
	 * Get linkToLogo attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlinkToLogo() const = 0;

	/**
	 * Set linkToLogo attribute.
	 * @param item XMLCh *
	 */
	virtual void SetlinkToLogo(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate linkToLogo attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelinkToLogo() = 0;

	/**
	 * Get lastUpdate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetlastUpdate() const = 0;

	/**
	 * Get lastUpdate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastUpdate() const = 0;

	/**
	 * Set lastUpdate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate lastUpdate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastUpdate() = 0;

		/** @name Sequence

		 */
		//@{
	/**
	 * Get organisationName element.
	 * @return EbuCoreorganisationDetailsType_organisationName_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_organisationName_CollectionPtr GetorganisationName() const = 0;

	/**
	 * Set organisationName element.
	 * @param item const EbuCoreorganisationDetailsType_organisationName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetorganisationName(const EbuCoreorganisationDetailsType_organisationName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get organisationCode element.
	 * @return EbuCoreorganisationDetailsType_organisationCode_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_organisationCode_CollectionPtr GetorganisationCode() const = 0;

	/**
	 * Set organisationCode element.
	 * @param item const EbuCoreorganisationDetailsType_organisationCode_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetorganisationCode(const EbuCoreorganisationDetailsType_organisationCode_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get organisationDepartment element.
	 * @return EbuCoreorganisationDepartmentPtr
	 */
	virtual EbuCoreorganisationDepartmentPtr GetorganisationDepartment() const = 0;

	virtual bool IsValidorganisationDepartment() const = 0;

	/**
	 * Set organisationDepartment element.
	 * @param item const EbuCoreorganisationDepartmentPtr &
	 */
	virtual void SetorganisationDepartment(const EbuCoreorganisationDepartmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate organisationDepartment element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorganisationDepartment() = 0;

	/**
	 * Get details element.
	 * @return EbuCoreorganisationDetailsType_details_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_details_CollectionPtr Getdetails() const = 0;

	/**
	 * Set details element.
	 * @param item const EbuCoreorganisationDetailsType_details_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdetails(const EbuCoreorganisationDetailsType_details_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relatedInformationLink element.
	 * @return EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr GetrelatedInformationLink() const = 0;

	/**
	 * Set relatedInformationLink element.
	 * @param item const EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedInformationLink(const EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get contacts element.
	 * @return EbuCoreorganisationDetailsType_contacts_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_contacts_CollectionPtr Getcontacts() const = 0;

	/**
	 * Set contacts element.
	 * @param item const EbuCoreorganisationDetailsType_contacts_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcontacts(const EbuCoreorganisationDetailsType_contacts_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of organisationDetailsType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:organisationName</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:organisationCode</li>
	 * <li>organisationDepartment</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:details</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:contacts</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtPropDef.h

};




/*
 * Generated class EbuCoreorganisationDetailsType<br>
 * Located at: EBU_CORE_20140318.xsd, line 2278<br>
 * Classified: Class<br>
 * Defined in EBU_CORE_20140318.xsd, line 2278.<br>
 */
class DC1_EXPORT EbuCoreorganisationDetailsType :
		public IEbuCoreorganisationDetailsType
{
	friend class EbuCoreorganisationDetailsTypeFactory; // constructs objects

public:
	/*
	 * Get organisationId attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetorganisationId() const;

	/*
	 * Get organisationId attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistorganisationId() const;

	/*
	 * Set organisationId attribute.
	 * @param item XMLCh *
	 */
	virtual void SetorganisationId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate organisationId attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorganisationId();

	/*
	 * Get typeLabel attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get linkToLogo attribute.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetlinkToLogo() const;

	/*
	 * Get linkToLogo attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlinkToLogo() const;

	/*
	 * Set linkToLogo attribute.
	 * @param item XMLCh *
	 */
	virtual void SetlinkToLogo(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate linkToLogo attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelinkToLogo();

	/*
	 * Get lastUpdate attribute.
	 * @return W3CdatePtr
	 */
	virtual W3CdatePtr GetlastUpdate() const;

	/*
	 * Get lastUpdate attribute validity information.
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistlastUpdate() const;

	/*
	 * Set lastUpdate attribute.
	 * @param item const W3CdatePtr &
	 */
	virtual void SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate lastUpdate attribute.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatelastUpdate();

		/* @name Sequence

		 */
		//@{
	/*
	 * Get organisationName element.
	 * @return EbuCoreorganisationDetailsType_organisationName_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_organisationName_CollectionPtr GetorganisationName() const;

	/*
	 * Set organisationName element.
	 * @param item const EbuCoreorganisationDetailsType_organisationName_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetorganisationName(const EbuCoreorganisationDetailsType_organisationName_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get organisationCode element.
	 * @return EbuCoreorganisationDetailsType_organisationCode_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_organisationCode_CollectionPtr GetorganisationCode() const;

	/*
	 * Set organisationCode element.
	 * @param item const EbuCoreorganisationDetailsType_organisationCode_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetorganisationCode(const EbuCoreorganisationDetailsType_organisationCode_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get organisationDepartment element.
	 * @return EbuCoreorganisationDepartmentPtr
	 */
	virtual EbuCoreorganisationDepartmentPtr GetorganisationDepartment() const;

	virtual bool IsValidorganisationDepartment() const;

	/*
	 * Set organisationDepartment element.
	 * @param item const EbuCoreorganisationDepartmentPtr &
	 */
	virtual void SetorganisationDepartment(const EbuCoreorganisationDepartmentPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate organisationDepartment element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorganisationDepartment();

	/*
	 * Get details element.
	 * @return EbuCoreorganisationDetailsType_details_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_details_CollectionPtr Getdetails() const;

	/*
	 * Set details element.
	 * @param item const EbuCoreorganisationDetailsType_details_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setdetails(const EbuCoreorganisationDetailsType_details_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relatedInformationLink element.
	 * @return EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr GetrelatedInformationLink() const;

	/*
	 * Set relatedInformationLink element.
	 * @param item const EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr &
	 */
	// Mandatory collection
	virtual void SetrelatedInformationLink(const EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get contacts element.
	 * @return EbuCoreorganisationDetailsType_contacts_CollectionPtr
	 */
	virtual EbuCoreorganisationDetailsType_contacts_CollectionPtr Getcontacts() const;

	/*
	 * Set contacts element.
	 * @param item const EbuCoreorganisationDetailsType_contacts_CollectionPtr &
	 */
	// Mandatory collection
	virtual void Setcontacts(const EbuCoreorganisationDetailsType_contacts_CollectionPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of organisationDetailsType.
	 * Currently this type contains 6 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:organisationName</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:organisationCode</li>
	 * <li>organisationDepartment</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:details</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink</li>
	 * <li>urn:ebu:metadata-schema:ebuCore_2014:contacts</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMyMethodDef.h


public:

	virtual ~EbuCoreorganisationDetailsType();

protected:
	EbuCoreorganisationDetailsType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	XMLCh * m_organisationId;
	bool m_organisationId_Exist;
	XMLCh * m_typeLabel;
	bool m_typeLabel_Exist;
	XMLCh * m_typeDefinition;
	bool m_typeDefinition_Exist;
	XMLCh * m_typeLink;
	bool m_typeLink_Exist;
	XMLCh * m_typeSource;
	bool m_typeSource_Exist;
	XMLCh * m_typeLanguage;
	bool m_typeLanguage_Exist;
	XMLCh * m_linkToLogo;
	bool m_linkToLogo_Exist;
	W3CdatePtr m_lastUpdate;
	bool m_lastUpdate_Exist;


	EbuCoreorganisationDetailsType_organisationName_CollectionPtr m_organisationName;
	EbuCoreorganisationDetailsType_organisationCode_CollectionPtr m_organisationCode;
	EbuCoreorganisationDepartmentPtr m_organisationDepartment;
	bool m_organisationDepartment_Exist; // For optional elements 
	EbuCoreorganisationDetailsType_details_CollectionPtr m_details;
	EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr m_relatedInformationLink;
	EbuCoreorganisationDetailsType_contacts_CollectionPtr m_contacts;

// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _455EBUCOREORGANISATIONDETAILSTYPE_H

