/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// ClassType_H.template
#ifndef _255EBUCORECOREMETADATATYPE_HASTRACKPART_LOCALTYPE_H
#define _255EBUCORECOREMETADATATYPE_HASTRACKPART_LOCALTYPE_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "EbuCoreDefines.h"
#include "Dc1FactoryDefines.h"
#include "EbuCoreFactoryDefines.h"

#include "EbuCorecoreMetadataType_hasTrackPart_LocalTypeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"


// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtInclude.h


class IEbuCorerelationType;
typedef Dc1Ptr< IEbuCorerelationType > EbuCorerelationPtr;
class IEbuCorecoreMetadataType_hasTrackPart_LocalType;
typedef Dc1Ptr< IEbuCorecoreMetadataType_hasTrackPart_LocalType> EbuCorecoreMetadataType_hasTrackPart_LocalPtr;
class IEbuCorealternativeTitleType;
typedef Dc1Ptr< IEbuCorealternativeTitleType > EbuCorealternativeTitlePtr;
class IEbuCoretimeType;
typedef Dc1Ptr< IEbuCoretimeType > EbuCoretimePtr;
#include "EbuCorerelationType.h"
class IDc1elementType;
typedef Dc1Ptr< IDc1elementType > Dc1elementPtr;
class IEbuCoreidentifierType;
typedef Dc1Ptr< IEbuCoreidentifierType > EbuCoreidentifierPtr;

/** 
 * Generated interface IEbuCorecoreMetadataType_hasTrackPart_LocalType for class EbuCorecoreMetadataType_hasTrackPart_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 412<br>
 * Classified: Class<br>
 * Derived from: relationType (Class)<br>
 */
class EBUCORE_EXPORT IEbuCorecoreMetadataType_hasTrackPart_LocalType 
 :
		public IEbuCorerelationType
{
public:
	// TODO: make these protected?
	IEbuCorecoreMetadataType_hasTrackPart_LocalType();
	virtual ~IEbuCorecoreMetadataType_hasTrackPart_LocalType();
		/** @name Sequence

		 */
		//@{
	/**
	 * Get trackPartTitle element.
	 * @return EbuCorealternativeTitlePtr
	 */
	virtual EbuCorealternativeTitlePtr GettrackPartTitle() const = 0;

	/**
	 * Set trackPartTitle element.
	 * @param item const EbuCorealternativeTitlePtr &
	 */
	// Mandatory			
	virtual void SettrackPartTitle(const EbuCorealternativeTitlePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get destinationId element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdestinationId() const = 0;

	virtual bool IsValiddestinationId() const = 0;

	/**
	 * Set destinationId element.
	 * @param item XMLCh *
	 */
	virtual void SetdestinationId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate destinationId element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationId() = 0;

	/**
	 * Get destinationStart element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetdestinationStart() const = 0;

	virtual bool IsValiddestinationStart() const = 0;

	/**
	 * Set destinationStart element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetdestinationStart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate destinationStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationStart() = 0;

	/**
	 * Get destinationEnd element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetdestinationEnd() const = 0;

	virtual bool IsValiddestinationEnd() const = 0;

	/**
	 * Set destinationEnd element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetdestinationEnd(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate destinationEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationEnd() = 0;

	/**
	 * Get sourceId element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsourceId() const = 0;

	virtual bool IsValidsourceId() const = 0;

	/**
	 * Set sourceId element.
	 * @param item XMLCh *
	 */
	virtual void SetsourceId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sourceId element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceId() = 0;

	/**
	 * Get sourceStart element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetsourceStart() const = 0;

	virtual bool IsValidsourceStart() const = 0;

	/**
	 * Set sourceStart element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetsourceStart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sourceStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceStart() = 0;

	/**
	 * Get sourceEnd element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetsourceEnd() const = 0;

	virtual bool IsValidsourceEnd() const = 0;

	/**
	 * Set sourceEnd element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetsourceEnd(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate sourceEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceEnd() = 0;

		//@}
	/**
	 * Get typeLabel attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const = 0;

	/**
	 * Get typeLabel attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const = 0;

	/**
	 * Set typeLabel attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLabel attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel() = 0;

	/**
	 * Get typeDefinition attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const = 0;

	/**
	 * Get typeDefinition attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const = 0;

	/**
	 * Set typeDefinition attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeDefinition attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition() = 0;

	/**
	 * Get typeLink attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const = 0;

	/**
	 * Get typeLink attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const = 0;

	/**
	 * Set typeLink attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLink attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink() = 0;

	/**
	 * Get typeSource attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const = 0;

	/**
	 * Get typeSource attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const = 0;

	/**
	 * Set typeSource attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeSource attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource() = 0;

	/**
	 * Get typeLanguage attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const = 0;

	/**
	 * Get typeLanguage attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const = 0;

	/**
	 * Set typeLanguage attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate typeLanguage attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage() = 0;

	/**
	 * Get runningOrderNumber attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return int
	 */
	virtual int GetrunningOrderNumber() const = 0;

	/**
	 * Get runningOrderNumber attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistrunningOrderNumber() const = 0;

	/**
	 * Set runningOrderNumber attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item int
	 */
	virtual void SetrunningOrderNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate runningOrderNumber attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidaterunningOrderNumber() = 0;

	/**
	 * Get totalNumberOfGroupMembers attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return int
	 */
	virtual int GettotalNumberOfGroupMembers() const = 0;

	/**
	 * Get totalNumberOfGroupMembers attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttotalNumberOfGroupMembers() const = 0;

	/**
	 * Set totalNumberOfGroupMembers attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item int
	 */
	virtual void SettotalNumberOfGroupMembers(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate totalNumberOfGroupMembers attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetotalNumberOfGroupMembers() = 0;

	/**
	 * Get orderedGroupFlag attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return bool
	 */
	virtual bool GetorderedGroupFlag() const = 0;

	/**
	 * Get orderedGroupFlag attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistorderedGroupFlag() const = 0;

	/**
	 * Set orderedGroupFlag attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item bool
	 */
	virtual void SetorderedGroupFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate orderedGroupFlag attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorderedGroupFlag() = 0;

	/**
	 * Get note attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const = 0;

	/**
	 * Get note attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const = 0;

	/**
	 * Set note attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


	/**
	 * Invalidate note attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote() = 0;

		/** @name Choice

		 */
		//@{
	/**
	 * Get relation element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrelation() const = 0;

	/**
	 * Set relation element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item const Dc1elementPtr &
	 */
	// Mandatory			
	virtual void Setrelation(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relationIdentifier element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return EbuCoreidentifierPtr
	 */
	virtual EbuCoreidentifierPtr GetrelationIdentifier() const = 0;

	/**
	 * Set relationIdentifier element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item const EbuCoreidentifierPtr &
	 */
	// Mandatory			
	virtual void SetrelationIdentifier(const EbuCoreidentifierPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	/**
	 * Get relationLink element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetrelationLink() const = 0;

	/**
	 * Set relationLink element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	// Mandatory			
	virtual void SetrelationLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

		//@}
	/**
	 * Gets or creates Dc1NodePtr child elements of coreMetadataType_hasTrackPart_LocalType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>relation</li>
	 * <li>relationIdentifier</li>
	 * <li>trackPartTitle</li>
	 * <li>destinationStart</li>
	 * <li>destinationEnd</li>
	 * <li>sourceStart</li>
	 * <li>sourceEnd</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL) = 0;

	
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMethodDef.h


// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtPropDef.h

};




/*
 * Generated class EbuCorecoreMetadataType_hasTrackPart_LocalType<br>
 * Located at: EBU_CORE_20140318.xsd, line 412<br>
 * Classified: Class<br>
 * Derived from: relationType (Class)<br>
 * Defined in EBU_CORE_20140318.xsd, line 412.<br>
 */
class DC1_EXPORT EbuCorecoreMetadataType_hasTrackPart_LocalType :
		public IEbuCorecoreMetadataType_hasTrackPart_LocalType
{
	friend class EbuCorecoreMetadataType_hasTrackPart_LocalTypeFactory; // constructs objects

public:
	/*
	 * Retrieves the base class of EbuCorecoreMetadataType_hasTrackPart_LocalType
	 
	 * @return Dc1Ptr< EbuCorerelationType >
	 */
	virtual Dc1Ptr< EbuCorerelationType > GetBase() const;
		/* @name Sequence

		 */
		//@{
	/*
	 * Get trackPartTitle element.
	 * @return EbuCorealternativeTitlePtr
	 */
	virtual EbuCorealternativeTitlePtr GettrackPartTitle() const;

	/*
	 * Set trackPartTitle element.
	 * @param item const EbuCorealternativeTitlePtr &
	 */
	// Mandatory			
	virtual void SettrackPartTitle(const EbuCorealternativeTitlePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get destinationId element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetdestinationId() const;

	virtual bool IsValiddestinationId() const;

	/*
	 * Set destinationId element.
	 * @param item XMLCh *
	 */
	virtual void SetdestinationId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate destinationId element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationId();

	/*
	 * Get destinationStart element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetdestinationStart() const;

	virtual bool IsValiddestinationStart() const;

	/*
	 * Set destinationStart element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetdestinationStart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate destinationStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationStart();

	/*
	 * Get destinationEnd element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetdestinationEnd() const;

	virtual bool IsValiddestinationEnd() const;

	/*
	 * Set destinationEnd element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetdestinationEnd(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate destinationEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatedestinationEnd();

	/*
	 * Get sourceId element.
	 * @return XMLCh *
	 */
	virtual XMLCh * GetsourceId() const;

	virtual bool IsValidsourceId() const;

	/*
	 * Set sourceId element.
	 * @param item XMLCh *
	 */
	virtual void SetsourceId(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sourceId element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceId();

	/*
	 * Get sourceStart element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetsourceStart() const;

	virtual bool IsValidsourceStart() const;

	/*
	 * Set sourceStart element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetsourceStart(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sourceStart element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceStart();

	/*
	 * Get sourceEnd element.
	 * @return EbuCoretimePtr
	 */
	virtual EbuCoretimePtr GetsourceEnd() const;

	virtual bool IsValidsourceEnd() const;

	/*
	 * Set sourceEnd element.
	 * @param item const EbuCoretimePtr &
	 */
	virtual void SetsourceEnd(const EbuCoretimePtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate sourceEnd element.
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatesourceEnd();

		//@}
	/*
	 * Get typeLabel attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLabel() const;

	/*
	 * Get typeLabel attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLabel() const;

	/*
	 * Set typeLabel attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLabel(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLabel attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLabel();

	/*
	 * Get typeDefinition attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeDefinition() const;

	/*
	 * Get typeDefinition attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeDefinition() const;

	/*
	 * Set typeDefinition attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeDefinition(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeDefinition attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeDefinition();

	/*
	 * Get typeLink attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLink() const;

	/*
	 * Get typeLink attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLink() const;

	/*
	 * Set typeLink attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLink attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLink();

	/*
	 * Get typeSource attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeSource() const;

	/*
	 * Get typeSource attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeSource() const;

	/*
	 * Set typeSource attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeSource(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeSource attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeSource();

	/*
	 * Get typeLanguage attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GettypeLanguage() const;

	/*
	 * Get typeLanguage attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttypeLanguage() const;

	/*
	 * Set typeLanguage attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void SettypeLanguage(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate typeLanguage attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetypeLanguage();

	/*
	 * Get runningOrderNumber attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return int
	 */
	virtual int GetrunningOrderNumber() const;

	/*
	 * Get runningOrderNumber attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistrunningOrderNumber() const;

	/*
	 * Set runningOrderNumber attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item int
	 */
	virtual void SetrunningOrderNumber(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate runningOrderNumber attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidaterunningOrderNumber();

	/*
	 * Get totalNumberOfGroupMembers attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return int
	 */
	virtual int GettotalNumberOfGroupMembers() const;

	/*
	 * Get totalNumberOfGroupMembers attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExisttotalNumberOfGroupMembers() const;

	/*
	 * Set totalNumberOfGroupMembers attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item int
	 */
	virtual void SettotalNumberOfGroupMembers(int item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate totalNumberOfGroupMembers attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidatetotalNumberOfGroupMembers();

	/*
	 * Get orderedGroupFlag attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return bool
	 */
	virtual bool GetorderedGroupFlag() const;

	/*
	 * Get orderedGroupFlag attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool ExistorderedGroupFlag() const;

	/*
	 * Set orderedGroupFlag attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item bool
	 */
	virtual void SetorderedGroupFlag(bool item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate orderedGroupFlag attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void InvalidateorderedGroupFlag();

	/*
	 * Get note attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * Getnote() const;

	/*
	 * Get note attribute validity information.
	 * (Inherited from EbuCorerelationType)
	 * @return true if attribute value is valid, false if not.
	 */
	virtual bool Existnote() const;

	/*
	 * Set note attribute.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	virtual void Setnote(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	/*
	 * Invalidate note attribute.
	 * (Inherited from EbuCorerelationType)
	 
	 * (Validate it again by setting a value)
	 */
	virtual void Invalidatenote();

		/* @name Choice

		 */
		//@{
	/*
	 * Get relation element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return Dc1elementPtr
	 */
	virtual Dc1elementPtr Getrelation() const;

	/*
	 * Set relation element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item const Dc1elementPtr &
	 */
	// Mandatory			
	virtual void Setrelation(const Dc1elementPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relationIdentifier element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return EbuCoreidentifierPtr
	 */
	virtual EbuCoreidentifierPtr GetrelationIdentifier() const;

	/*
	 * Set relationIdentifier element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item const EbuCoreidentifierPtr &
	 */
	// Mandatory			
	virtual void SetrelationIdentifier(const EbuCoreidentifierPtr & item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	/*
	 * Get relationLink element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @return XMLCh *
	 */
	virtual XMLCh * GetrelationLink() const;

	/*
	 * Set relationLink element.
<br>
	 * (Inherited from EbuCorerelationType)
	 * @param item XMLCh *
	 */
	// Mandatory			
	virtual void SetrelationLink(XMLCh * item, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

		//@}
	/*
	 * Gets or creates Dc1NodePtr child elements of coreMetadataType_hasTrackPart_LocalType.
	 * Currently this type contains 7 child elements of type "Dc1NodePtr".

	 * <ul>
	 * <li>relation</li>
	 * <li>relationIdentifier</li>
	 * <li>trackPartTitle</li>
	 * <li>destinationStart</li>
	 * <li>destinationEnd</li>
	 * <li>sourceStart</li>
	 * <li>sourceEnd</li>
	 * </ul>
	 * 
	 * Supported XPath expressions for GetOrCreateFromXPath
	 * 
	 * \par<code>/</code>
	 *  - Get the root of this node.
	 * \par<code>..</code>
	 *  - This feature is experimental.
	 *  - Get the ancestor (parent node), or an empty list if the node already is the root node.
	 * 
	 * \par <code>\@A</code>
	 * - Force creation of a child attribute named <code>A</code>.
	 * - This is valid only, if the type of attribute is derived from Dc1NodePtr.
	 * - Evaluating this expression for a plain type like int or string results in an exception.
	 *
	 * \par<code>A</code> 
	 *  - Select all child elements named <code>A</code>.
	 *  - In case of no element, create a new one.
	 *  - For collections: all subsequent XPath expressions are applied on all collection items (e.g. <code>A/B</code> query or create, childnodes <code>B</code> of all <code>A</code>s). 
	 * \par<code>A[i]</code>
	 *  - Select the <code>i</code>th child element of all <code>A</code>.
	 *  - Please note, that XPath indexes start at 1.
	 *  - If there is no such <code>i</code>th element, try to create a new one at index <code>i</code>.
	 *  - Single elements require <code>i</code> to be 1 otherwise an exception is thrown.
	 *  - For collections, <code>i</code> must not exceed the number of existing items +1 (appending is allowed, filling gaps not).
	 *  - If the index is too high, a <code>MissingElementException</code> is thrown, and you can extract the last valid index. (Example: If a collection contains 2 elements and you are requesting the 4th element).
	 *
	 * \par <code>A[\@xsi:type="T"]</code>
	 *  - Select all child elements named <code>A</code> of type <code>T</code>.
	 *  - Currently only typenames without namespace are recognised.
	 *  - If there is no such element, create and append a new one.
	 * \par <code>A[i][\@xsi:type="T"]</code>
	 *  - Select the <code>i</code>th child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - If there is no such element at index <code>i</code>, a new one is created and inserted (appended).
	 * \par <code>A[\@xsi:type="T"][i]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the <code>i</code>th one.
	 *  - If there is no such element at index <code>i</code>, create and append one.
	 *
	 * \par <code>A[last()]</code>
	 *  - Select the last child element named <code>A</code>.
	 *  - In case of no element, create a new one.
	 * \par <code>A[last()][\@xsi:type="T"]</code>
	 *  - Select the last child element named <code>A</code>, which must be of type <code>T</code>.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[\@xsi:type="T"][last()]</code>
	 *  - Out of all child elements named <code>A</code> of type <code>T</code>, take the last one.
	 *  - For collections: in case of no such type, create and append a new one.
	 *  - For single elements: if an element with different type exists, return the empty list.
	 * 
	 * \par <code>A[position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> with its default type.
	 *  - For collections: create and append the default element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[position() \> last()][\@xsi:type="T"]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - For collections: create and append the element.
	 *  - For single elements: if any element exists, an exception is thrown.
	 *
	 * \par <code>A[\@xsi:type="T"][position() \> last()]</code>
	 *  - Force creation of a child element named <code>A</code> of type <code>T</code>.
	 *  - Behaves the same way as the previous expression.
	 * 
	 * Supported XPath expressions for GetFromXPath<br>
	 * The same expressions like GetOrCreateFromXPath plus
	 * \par <code>A[position() \< i] </code>
	 *  - Select all child elements named <code>A</code> at indexes lower than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code>.
	 *  - <code>i</code> must be greater than -1, otherwise an exception is thrown.
	 * \par <code>A[position() \> i | position() \< j] </code>
	 *  - Select all child elements named <code>A</code> at indexes greater than <code>i</code> and lower than <code>j</code>.
	 *  - <code>i</code> and <code>j</code> must be greater than -1, otherwise an exception is thrown.
	 *  - If <code>i</code> is greater than <code>j</code> then an empty list is returned.
	 *
	 * These three expressions can be combined with the <code>\@xsi:type</code> attribute to get a more restricted query.
	 *
	 * @param xpath  the xpath expression to get or create.
	 * @param client the ID in case of locking.
	 * @return the appropriate subelement node list. Can be empty in case of incompatible name/type/index settings.
	 */
	// xpath: mandatory
	// client: optional
	virtual Dc1NodeEnum GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client = DC1_UNDEFINED_CLIENT, Dc1XPathCreation flag = DC1_FORCECREATE, Dc1XPathParseContext * context = NULL);

	

	/* Get the class name.
	 * @return The name of this class.
	 */
	virtual XMLCh * ToText() const;

	/* Parse this type.
	 * @param txt buffer to parse from.
	 * @return true if parsing was successful, false otherwise.
	 */
	virtual bool Parse(const XMLCh * const txt);

	/* Serialialize this object to a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent node in DOM tree.
	 */
	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);

	/* Deserialize this object from a DOM tree.
	 * @param doc DOM document.
	 * @param parent Parent element in DOM tree.
	 * @param current Current element in DOM tree.
	 */
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

	/* Creates and inserts a Childnode of the specified Type
	 * @param type The tag type of the child to be created (tag name).
	 * @param xsitype The xsi type of the child to be created if available.
	 * @return the created child with the correct dynamic type for the specified type.
	 */
	virtual Dc1NodePtr CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype);
		 
// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMyMethodDef.h


public:

	virtual ~EbuCorecoreMetadataType_hasTrackPart_LocalType();

protected:
	EbuCorecoreMetadataType_hasTrackPart_LocalType();
	virtual void DeepCopy(const Dc1NodePtr &original);

	virtual Dc1NodeEnum GetAllChildElements() const;

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:

	// Base Class
	Dc1Ptr< EbuCorerelationType > m_Base;

	EbuCorealternativeTitlePtr m_trackPartTitle;
	XMLCh * m_destinationId;
	bool m_destinationId_Exist; // For optional elements 
	EbuCoretimePtr m_destinationStart;
	bool m_destinationStart_Exist; // For optional elements 
	EbuCoretimePtr m_destinationEnd;
	bool m_destinationEnd_Exist; // For optional elements 
	XMLCh * m_sourceId;
	bool m_sourceId_Exist; // For optional elements 
	EbuCoretimePtr m_sourceStart;
	bool m_sourceStart_Exist; // For optional elements 
	EbuCoretimePtr m_sourceEnd;
	bool m_sourceEnd_Exist; // For optional elements 

// no includefile for extension defined 
// file EbuCorecoreMetadataType_hasTrackPart_LocalType_ExtMyPropDef.h


public:
	virtual void Init(); // Called by inheritees to recursively update m_myself
protected:
	void Cleanup();
};

#endif // _255EBUCORECOREMETADATATYPE_HASTRACKPART_LOCALTYPE_H

