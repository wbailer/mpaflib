/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// PatternType_H

#ifndef _71W3CTIME_H
#define _71W3CTIME_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"



#include "W3CtimeFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"

// no includefile for extension defined 
// file W3Ctime_ExtInclude.h


class IW3Ctime;typedef Dc1Ptr< IW3Ctime> W3CtimePtr;
/**
 * Generated interface IW3Ctime for Pattern W3Ctime
 */
class DC1_EXPORT IW3Ctime :
	public Dc1Node
{
public:
	virtual int GetHour() const = 0;

	virtual void SetHour(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMinute() const = 0;

	virtual void SetMinute(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetSecond() const = 0;

	virtual void SetSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetUTCTimezone_Exist() const = 0;

	virtual void SetUTCTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneSign() const = 0;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneHours() const = 0;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetTimezoneMinutes() const = 0;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual bool GetTimezoneOffset_Exist() const = 0;

	virtual void SetTimezoneOffset_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


// no includefile for extension defined 
// file W3Ctime_ExtMethodDef.h


};

/*
 * Generated pattern W3Ctime<br>
 */
class DC1_EXPORT W3Ctime :
	public IW3Ctime
{
	friend class W3CtimeFactory;
	
public: 
	virtual int GetHour() const;

	virtual void SetHour(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMinute() const;

	virtual void SetMinute(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetSecond() const;

	virtual void SetSecond(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetUTCTimezone_Exist() const;

	virtual void SetUTCTimezone_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneSign() const;

	virtual void SetTimezoneSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneHours() const;

	virtual void SetTimezoneHours(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetTimezoneMinutes() const;

	virtual void SetTimezoneMinutes(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual bool GetTimezoneOffset_Exist() const;

	virtual void SetTimezoneOffset_Exist(bool val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	virtual XMLCh * ToText() const;
	virtual bool Parse(const XMLCh * const txt);

	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file W3Ctime_ExtMyMethodDef.h


	virtual ~W3Ctime();

protected:
	W3Ctime();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_Hour;
	int m_Minute;
	int m_Second;
	bool m_UTCTimezone_Exist;
	int m_TimezoneSign;
	int m_TimezoneHours;
	int m_TimezoneMinutes;
	bool m_TimezoneOffset_Exist;

// no includefile for extension defined 
// file W3Ctime_ExtPropDef.h


protected:
	void Init();
	void Cleanup();
	
protected:
    const XMLCh * const m_nsURI; // For namespace handling
};

#endif // _71W3CTIME_H

