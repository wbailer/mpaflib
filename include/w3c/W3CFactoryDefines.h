/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#ifndef W3CFACTORYDEFINES_H
#define W3CFACTORYDEFINES_H

#include "Dc1Ptr.h"

// Generated creation defines for extension classes

// Note: for destruction use Factory::DeleteObject(Node * item);
// Note: Refcounting pointers don't need to be deleted explicitly

// Note the different semantic for derived types with identical names


#define TypeOfdate (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:date")))
#define Createdate (Dc1Factory::CreateObject(TypeOfdate))
class W3Cdate;
class IW3Cdate;
/** Smart pointer for instance of IW3Cdate */
typedef Dc1Ptr< IW3Cdate > W3CdatePtr;

#define TypeOfdateTime (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:dateTime")))
#define CreatedateTime (Dc1Factory::CreateObject(TypeOfdateTime))
class W3CdateTime;
class IW3CdateTime;
/** Smart pointer for instance of IW3CdateTime */
typedef Dc1Ptr< IW3CdateTime > W3CdateTimePtr;

#define TypeOfduration (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:duration")))
#define Createduration (Dc1Factory::CreateObject(TypeOfduration))
class W3Cduration;
class IW3Cduration;
/** Smart pointer for instance of IW3Cduration */
typedef Dc1Ptr< IW3Cduration > W3CdurationPtr;

#define TypeOfIDREFS (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:IDREFS")))
#define CreateIDREFS (Dc1Factory::CreateObject(TypeOfIDREFS))
class W3CIDREFS;
class IW3CIDREFS;
/** Smart pointer for instance of IW3CIDREFS */
typedef Dc1Ptr< IW3CIDREFS > W3CIDREFSPtr;

#define TypeOfNMTOKENS (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:NMTOKENS")))
#define CreateNMTOKENS (Dc1Factory::CreateObject(TypeOfNMTOKENS))
class W3CNMTOKENS;
class IW3CNMTOKENS;
/** Smart pointer for instance of IW3CNMTOKENS */
typedef Dc1Ptr< IW3CNMTOKENS > W3CNMTOKENSPtr;

#define TypeOftime (Dc1Factory::GetTypeIndex(X("http://www.w3.org/2001/XMLSchema:time")))
#define Createtime (Dc1Factory::CreateObject(TypeOftime))
class W3Ctime;
class IW3Ctime;
/** Smart pointer for instance of IW3Ctime */
typedef Dc1Ptr< IW3Ctime > W3CtimePtr;

#include "Dc1PtrTypes.h"

#endif // W3CFACTORYDEFINES_H
