/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// PatternType_H

#ifndef _33W3CDURATION_H
#define _33W3CDURATION_H

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Defines.h"
#include "Dc1FactoryDefines.h"



#include "W3CdurationFactory.h"
#include "Dc1Node.h"
#include "Dc1Convert.h"

// no includefile for extension defined 
// file W3Cduration_ExtInclude.h


class IW3Cduration;typedef Dc1Ptr< IW3Cduration> W3CdurationPtr;
/**
 * Generated interface IW3Cduration for Pattern W3Cduration
 */
class DC1_EXPORT IW3Cduration :
	public Dc1Node
{
public:
	virtual int GetDurationSign() const = 0;

	virtual void SetDurationSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetYear() const = 0;

	virtual void SetYear(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMonth() const = 0;

	virtual void SetMonth(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetDay() const = 0;

	virtual void SetDay(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetHour() const = 0;

	virtual void SetHour(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual int GetMinute() const = 0;

	virtual void SetMinute(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;

	virtual double GetSecond() const = 0;

	virtual void SetSecond(double val, Dc1ClientID client = DC1_UNDEFINED_CLIENT) = 0;


// no includefile for extension defined 
// file W3Cduration_ExtMethodDef.h


};

/*
 * Generated pattern W3Cduration<br>
 */
class DC1_EXPORT W3Cduration :
	public IW3Cduration
{
	friend class W3CdurationFactory;
	
public: 
	virtual int GetDurationSign() const;

	virtual void SetDurationSign(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetYear() const;

	virtual void SetYear(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMonth() const;

	virtual void SetMonth(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetDay() const;

	virtual void SetDay(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetHour() const;

	virtual void SetHour(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual int GetMinute() const;

	virtual void SetMinute(int val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);

	virtual double GetSecond() const;

	virtual void SetSecond(double val, Dc1ClientID client = DC1_UNDEFINED_CLIENT);


	virtual XMLCh * ToText() const;
	virtual bool Parse(const XMLCh * const txt);

	virtual void Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem = NULL);
	virtual bool Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current);

// no includefile for extension defined 
// file W3Cduration_ExtMyMethodDef.h


	virtual ~W3Cduration();

protected:
	W3Cduration();
	virtual void DeepCopy(const Dc1NodePtr &original);
	virtual Dc1NodeEnum GetAllChildElements() const { return Dc1NodeEnum(); }

	virtual Dc1NodePtr GetBaseRoot();
	virtual const Dc1NodePtr GetBaseRoot() const;

private:
	int m_DurationSign;
	int m_Year;
	int m_Month;
	int m_Day;
	int m_Hour;
	int m_Minute;
	double m_Second;

// no includefile for extension defined 
// file W3Cduration_ExtPropDef.h


protected:
	void Init();
	void Cleanup();
	
protected:
    const XMLCh * const m_nsURI; // For namespace handling
};

#endif // _33W3CDURATION_H

