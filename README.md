# README #

* Quick summary
MP-AF lib is a C++ library generated out of the mpaf XML schema. It allows to create / read / update and delete MP-AF data using C++ classes as frontend and Xerces XML parser as backend.

* Version
1.0.0

* Summary of set up
* A Windows VS-2013 solution allows to build the libraries along with doxygen scripts to generate documentation.

* Dependencies
Xerces 3.1.1

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact